'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');


var browserSync = require('browser-sync');
var browserSyncSpa = require('browser-sync-spa');

var util = require('util');

var proxyMiddleware = require('http-proxy-middleware');

function browserSyncInit(baseDir, browser)
{
    browser = browser === undefined ? 'default' : browser;

    var routes = null;
    if ( baseDir === conf.paths.src || (util.isArray(baseDir) && baseDir.indexOf(conf.paths.src) !== -1) )
    {
        routes = {
            '/bower_components': 'bower_components'
        };
    }

    var server = {
        baseDir: baseDir,
        routes : routes
    };

    /*
     * You can add a proxy to your backend by uncommenting the line below.
     * You just have to configure a context which will we redirected and the target url.
     * Example: $http.get('/users') requests will be automatically proxified.
     *
     * For more details and option, https://github.com/chimurai/http-proxy-middleware/blob/v0.9.0/README.md
     */
    
    // configure proxy middleware options
    var options = {
            target: conf.urlProxy.url + ':' + conf.urlProxy.port, // target host
            changeOrigin: true,               // needed for virtual hosted sites
            ws: true,                         // proxy websockets
            pathRewrite: {
                '^/api' : '/' + conf.urlProxy.contexto      // rewrite paths
            }
        };

 
     //server.middleware = proxyMiddleware('/Finnova', {target: 'http://localhost:8080', changeOrigin: true});
     
     server.middleware = proxyMiddleware('/api', options);

    browserSync.instance = browserSync.init({
        startPath: '/',
        server   : server,
        browser  : browser,
        port: conf.paths.port,
        ui: {
          port: conf.paths.portui,
          weinre: {
                  port: conf.paths.portweinre
                }
            }
    });
}

browserSync.use(browserSyncSpa({
    selector: '[ng-app]'// Only needed for angular apps
}));

gulp.task('serve', ['watch'], function ()
{
    browserSyncInit([path.join(conf.paths.tmp, '/serve'), conf.paths.src]);
});

gulp.task('serve:dist', ['build'], function ()
{
    browserSyncInit(conf.paths.dist);
});

gulp.task('serve:e2e', ['inject'], function ()
{
    browserSyncInit([conf.paths.tmp + '/serve', conf.paths.src], []);
});

gulp.task('serve:e2e-dist', ['build'], function ()
{
    browserSyncInit(conf.paths.dist, []);
});
