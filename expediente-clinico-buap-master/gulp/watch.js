'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var browserSync = require('browser-sync');

function isOnlyChange(event)
{
    return event.type === 'changed';
}

gulp.task('watch', ['inject'], function ()
{
    gulp.watch([path.join(conf.paths.src, '/*.html'), 'bower.json'], ['inject-reload']); // Inyecta en el index html todas las librerias que se encuentren en el archivo bower.json

    gulp.watch([
        path.join(conf.paths.src, '/application/assets/stylesheets/*.scss'),
        path.join(conf.paths.src, '/application/assets/stylesheets/**/*.scss'),
        path.join(conf.paths.src, '/application/assets/stylesheets/**/**/*.scss'),
        path.join(conf.paths.src, '/modules/**/assets/stylesheets/*.css'),
        path.join(conf.paths.src, '/modules/**/assets/stylesheets/*.scss'),
        path.join(conf.paths.src, '/modules/**/**/*.scss'),
        path.join(conf.paths.src, '/modules/**/**/**/*.scss'),
        path.join(conf.paths.src, '/modules/**/**/**/**/*.scss'),
        path.join(conf.paths.src, '/modules/**/**/**/**/**/*.scss')
    ], function (event)
    {
        if ( isOnlyChange(event) )
        {
            gulp.start('styles-reload');
        }
        else
        {
            gulp.start('inject-reload');
        }
    });

    gulp.watch([
        path.join(conf.paths.src, '/application/*.js'),
        path.join(conf.paths.src, '/application/**/*.js'),
        path.join(conf.paths.src, '/application/**/**/*.js'),
        path.join(conf.paths.src, '/application/**/**/**/*.js'),
        path.join(conf.paths.src, '/modules/**/*.js'),
        path.join(conf.paths.src, '/modules/**/**/*.js'),
        path.join(conf.paths.src, '/modules/**/**/**/*.js'),
        path.join(conf.paths.src, '/modules/**/**/**/**/*.js'),
        path.join(conf.paths.src, '/modules/**/**/**/**/**/*.js')
        ], function (event)
    {
        if ( isOnlyChange(event) )
        {
            gulp.start('scripts-reload');
        }
        else
        {
            gulp.start('inject-reload');
        }
    });

    gulp.watch([
        path.join(conf.paths.src, '/application/views/*.json'),
        path.join(conf.paths.src, '/application/views/*.html'),
        path.join(conf.paths.src, '/application/views/**/*.json'),
        path.join(conf.paths.src, '/application/views/**/*.html'),
        path.join(conf.paths.src, '/application/views/**/**/*.json'),
        path.join(conf.paths.src, '/application/views/**/**/*.html'),
        path.join(conf.paths.src, '/application/views/**/**/**/*.json'),
        path.join(conf.paths.src, '/application/views/**/**/**/*.html'),
        path.join(conf.paths.src, '/modules/**/views/*.json'),
        path.join(conf.paths.src, '/modules/**/views/*.html'),
        path.join(conf.paths.src, '/modules/**/views/**/*.json'),
        path.join(conf.paths.src, '/modules/**/views/**/*.html'),
        path.join(conf.paths.src, '/modules/directives/**/*.json'),
        path.join(conf.paths.src, '/modules/directives/**/*.html'),
        path.join(conf.paths.src, '/modules/directives/**/**/*.json'),
        path.join(conf.paths.src, '/modules/directives/**/**/*.html'),
        path.join(conf.paths.src, '/modules/directives/**/**/**/*.json'),
        path.join(conf.paths.src, '/modules/directives/**/**/**/*.html')
    ], function (event)
    {
        browserSync.reload(event.path);
    });
});
