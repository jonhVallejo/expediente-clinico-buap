/**
 *  This file contains the variables used in other gulp files
 *  which defines tasks
 *  By design, we only put there very generic config values
 *  which are used in several places to keep good readability
 *  of the tasks
 */

var gutil = require('gulp-util');

/**
 *  The main paths of your project handle these with care
 */
exports.paths = {
    src : 'src',
    dist: 'dist',
    tmp : '.tmp',
    e2e : 'e2e',
    port : 9007,  //Se debe cambiar el puerto al que se va a conectar, es la variable BaseUrl 
    portui : 3007,
    portweinre : 4007

};

/**
 * [urlBackend Datos de configuración para hacer un proxy en la aplicación,]
 * @type {Object}
 */
exports.urlProxy = {
    url : 'http://148.228.103.28',  //http://localhost
    port : '8082',
    contexto : 'jdbc'
};


/**
 *  Wiredep is the lib which inject bower dependencies in your project
 *  Mainly used to inject script tags in the index.html but also used
 *  to inject css preprocessor deps and js files in karma
 */
exports.wiredep = {
    directory: 'bower_components'
};

/**
 *  Common implementation for an error handler of a Gulp plugin
 */
exports.errorHandler = function (title)
{
    'use strict';

    return function (err)
    {
        gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
        this.emit('end');
    };
};
