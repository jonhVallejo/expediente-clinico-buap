'use strict';

/**
 * @ngdoc directive
 * @name mockupApp.directive:menuLink
 * @description
 * # menuLink
 */
 angular.module('expediente').directive('menuLink', [ '$state', '$location','menu','$rootScope', function ($state, $location, menu, $rootScope) {

  return {
    scope: {
      section: '='
    },
    templateUrl: '/application/views/partials/menu-link.tmpl.html',
    link: function ($scope, $element) {
      var controller = $element.parent().controller();
      //console.log($scope.section)
    //  console.log($element.parent())


      // $scope.listaIcons = [{
      //   cancelarReceta: "fa-ban"
      //   ,"/agenda/consultaPaciente/agendaActualizaPaciente":"fa-pencil-square-o"
      //   ,"agendaCalendarizarCitaEspecial": "fa-calendar-plus-o"
      //   ,"/agenda/consultaPaciente/agendaVerPaciente":"fa-calendar"
      //   ,"/agenda/consultaMedico/consulta": "fa-user-md"
      // }]

      $scope.isSelected = function(section) {
        return controller.isSelected(section);
      };


      $scope.obtenerIcon = function( menuIcon ){
      //  console.log(menuIcon)
        var icon = _.filter(  controller.menu.listaIcons, function(it){
          return it[menuIcon] != undefined
        })
        return _.size(icon) > 0 ? icon[0][menuIcon] : "fa-address-book-o"
      }

      $scope.focusSection = function (section) {
        if(section.url !== undefined){
            $rootScope.menuSelected = section.url;
          if(section.url.indexOf('/') > -1){
            $location.path(section.url);
          }else{
            $state.go(section.url);
          }
  		  // set flag to be used later when
		  // $locationChangeSuccess calls openPage()
		  controller.setPage(section);
		  controller.autoFocusContent = true ;
          }
        };
      }
    };
  }]);

 var _url;
