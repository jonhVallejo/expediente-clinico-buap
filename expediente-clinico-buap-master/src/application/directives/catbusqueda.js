'use strict';



/**
 * @ngdoc directive
 * @name expediente.directive:catbusqueda
 * @description
 * # catbusqueda
 */
angular.module('expediente').run(['$templateCache', function ($templateCache) {
  $templateCache.put('partials/cat-busqueda.tmpl.html',
    '<md-data-table-toolbar ng-show="!selected.length">\
        <md-input-container class="form-item">\
            <label>BÚSQUEDA <md-icon md-svg-src="images/icons/svg/ic_search_black_18px.svg" ng-class="search"></md-icon></label>\
            <input maxlength="60"  ng-model="ngModel" ng-change="ngChange()" >\
        </md-input-container>\
        <md-input-container class="form-item" >\
             <md-button class="md-icon-button md-primary" ng-click="click()" aria-label="Insertar registro">\
              <md-icon md-svg-src="images/icons/svg/ic_add_circle_black_18px.svg" ></md-icon>\
            </md-button>\
        </md-input-container>\
      </md-data-table-toolbar>'
    );
}]).directive('catBusqueda', function ( $window) {


    var directiva={};
    directiva.restrict= 'E';
    directiva.templateUrl= 'partials/cat-busqueda.tmpl.html';
    directiva.scope= {
      ngModel : '=',
      click: '&',
      ngChange: '&'
    };

      return directiva;
  });
