'use strict';

/**
 * @ngdoc directive
 * @name expediente.directive:cardToolbar
 * @description
 * # cardToolbar
 */
angular.module('expediente').run(['$templateCache', function ($templateCache) {
  $templateCache.put('partials/card-toolbar.tmpl.html',
    '<md-toolbar md-scroll-shrink layout="row"> \
<md-button layout-align="start end" class="md-fab md-primary" aria-label="Profile" ng-click="back()" ng-if="canreturn">\
<md-tooltip>\
          Regresar\
        </md-tooltip>\
          <md-icon md-svg-src="images/icons/svg/ic_arrow_back_white_18px.svg"></md-icon>\
        </md-button>\
      <div class="md-toolbar-tools"  layout-align="center center">\
        <h3>\
          {{title}}\
        </h3>\
      </div>\
      </md-toolbar>'
    );
}]).directive('cardToolbar',["$window", "menu","$rootScope", function ( $window, menu, $rootScope) {



    var directiva={};
  	directiva.restrict= 'E';
  	directiva.templateUrl= 'partials/card-toolbar.tmpl.html';
  	directiva.scope= {

      canreturn: '@',
      title: '@'
    };



    directiva.link = function($scope, $element, attrs){
      var controller = $element.parent().controller();

    //  console.log($element.parent())
      //
      // console.log(menu)

      $scope.title = _.isUndefined(menu.getTitle()) ? "" : menu.getTitle();
    //  console.log($scope.title)

      $scope.back = function(){
        $window.history.back();
      }


      $scope.obtenerIcon = function( ){
      //  console.log(menuIcon)
      var menuIcon = $rootScope.menuSelected;
        var icon = _.filter(  menu.listaIcons, function(it){
          return it[menuIcon] != undefined
        })
        return  _.size(icon) > 0 ? icon[0][menuIcon] : "fa-address-book-o"
      }

    }

    	return directiva;
  }]);
