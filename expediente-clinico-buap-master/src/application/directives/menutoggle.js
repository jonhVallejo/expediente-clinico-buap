'use strict';

/**
 * @ngdoc directive
 * @name mockupApp.directive:menuToggle
 * @description
 * # menuToggle
 */
angular.module('expediente').directive('menuToggle', [ '$timeout', function($timeout) {
  return {
    scope: {
      section: '='
    },
    templateUrl: '/application/views/partials/menu-toggle.tmpl.html',
    link: function($scope, $element) {
      var controller = $element.parent().controller();

      $scope.obtenerIcon = function( menuIcon ){
       // console.log(menuIcon, '+++++++++++++++++++++++')

        var icon = _.filter(  controller.menu.listaIcons, function(it){
          return it[menuIcon] != undefined
        })
        return _.size(icon) > 0 ? icon[0][menuIcon] : "fa-check"
      }

      $scope.isOpen = function() {
        return controller.isOpen($scope.section);
      };
      $scope.toggle = function() {
        controller.toggleOpen($scope.section);
      };
    }
  };
}]);
