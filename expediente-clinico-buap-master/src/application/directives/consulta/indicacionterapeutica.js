(function(){
'use strict';


/**
* @ngdoc directive
* @name expediente.directive:consulta/indicacionTerapeutica
* @description
* # consulta/indicacionTerapeutica
*/
angular.module('expediente')
.directive('indicacionTerapeutica', ['indicacionTerapeutica', 'mensajes', '$http', '$timeout', '$q','$localStorage',
  function (indicacionTerapeutica, mensajes, $http, $timeout, $q,$localStorage){
  return {
    templateUrl: '/application/views/consulta/indicacionterapeutica.html',
    restrict: 'E',
    scope: {
      guardaIndicacionTerapeutica : '&',
      anterior: '&',
      esUrgencias : '@',
      esHosp : '@',
      labelButton : '@',
      labelButtonBack: '@',
      idNota      : '@',
      esCancelar : '@',
      esNotaMedica:'@',
      permissionsForSigning: '@'
    },
    link: function postLink(scope, element, attrs) {
      var currentMedicamento = null;
      

      scope.permissionsForSigning = true;
      if(angular.isUndefined(scope.permissionsForSigning))  scope.permissionsForSigning = true;
      // Se envia al servicio los permisos para el usuario actual.
      indicacionTerapeutica.permissionsForSigning = scope.permissionsForSigning;
      // Si no se esta cancelando la nota medica, este valor es indefinido, se asigna,como falso por fines practicos
      //console.log("esCancelar: "+scope.esCancelar);
      scope.esCancelar = angular.isUndefined(scope.esCancelar) ? false: scope.esCancelar;
      
      console.log($localStorage);
      scope.esNotaMedica = angular.isUndefined(scope.esNotaMedica) ? false: scope.esNotaMedica;
      //console.debug('esNotaMedica',scope.esNotaMedica);
      scope.deshabilitaBtnFinalizar=false;
      scope.recetaACancelar= (scope.idNota==-404)? 0 : scope.idNota;
      scope.fe = {FIRMA: {}}
      indicacionTerapeutica.C_TIPO_RECETA = (scope.esUrgencias == 'true') ? 1: 0;
      scope.selectedMedicamentos = [];
      scope.statusRegistro=false;
      scope.medicamentoAnterior=-1;
      scope.permissions = undefined;
      // Numero maximo de dias que se puede surtir un medicamento
      // y este depende si biene de urgencias o no.
      scope.maxMonths   = scope.esUrgencias == 'true' ? 7 : 180;
      scope.minDays     = scope.esUrgencias == 'true' ? 0 : 1;
      scope.labelButton = scope.labelButton === undefined ? "SIGUIENTE" : scope.labelButton;
      scope.duplicates = [];
      indicacionTerapeutica.init(0).then(function(){
        scope.permissions = indicacionTerapeutica.havePermissions;
        if(scope.permissions){
          if(scope.idNota != -404){
            indicacionTerapeutica.recuperaNota(scope.idNota)
            .then(function(nota){
              scope.$evalAsync(function(){
                scope.C_TRATAMIENTO = nota.C_TRATAMIENTO;
                scope.C_PLAN=nota.C_PLAN;
                scope.selectedMedicamentos = nota.medicamentos;
                scope.recetaPrevia = JSON.parse( JSON.stringify(scope.selectedMedicamentos) );
                scope.saveMedicamentosTemporal(scope.C_PLAN,scope.C_TRATAMIENTO);
                //se actualiza la lista campleta de medicamentos
                _.each(indicacionTerapeutica.medicamentos,function(listaOriginal){
                  _.each(scope.selectedMedicamentos,function(recetaClon){ 
                    _.each(listaOriginal.DOSIS,function(presentacionOriginal){
                      if (presentacionOriginal.NP_MEDICAMENTO_CLAVE==recetaClon.selected.NP_MEDICAMENTO_CLAVE) {
                        presentacionOriginal.NF_ESTATUS='';
                        //console.log(presentacionOriginal.DESCRIPCION+": "+presentacionOriginal.NF_ESTATUS);
                        //console.log(recetaClon.selected.DESCRIPCION+": "+recetaClon.selected.NF_ESTATUS);
                      };
                    });
                  });
                });

                //se actualizan medicamentos de la receta  
                _.each(scope.selectedMedicamentos,function(listaClon){
                  _.each(indicacionTerapeutica.medicamentos,function(listaOriginal){
                    _.each(listaClon.DOSIS,function(presentacionClon){
                      _.each(listaOriginal.DOSIS,function(presentacionOriginal){
                        if(presentacionClon.NP_MEDICAMENTO_CLAVE==presentacionOriginal.NP_MEDICAMENTO_CLAVE){
                          
                          presentacionClon.NF_ESTATUS=presentacionOriginal.NF_ESTATUS;
                          //console.log(presentacionClon.NP_MEDICAMENTO_CLAVE+": "+presentacionOriginal.NF_ESTATUS);
                        }
                      });
                    });
                  });
                });
                //console.log(indicacionTerapeutica.medicamentos);
                //console.log(scope.selectedMedicamentos);
                _.each(scope.selectedMedicamentos, function(el){
                  scope.esUrgencias = (scope.esHosp == 'true') ? 'false' : 'true';
                  console.log('el.selected.ANTIBIOTICO: '+el.selected.ANTIBIOTICO);

                  el.max = (scope.esUrgencias == 'true') ? 7 :
                                (el.selected.CBASICO_CLAVE.indexOf("CBMC") >= 0 || el.selected.CATEGORIA_CLAVE == 5) ? 90 : 180;

                  el.fechasSurtimiento = calculateDates(el.meses);
                  el.validar = false;
                });
              });
            });
          }
        }





      }).catch(function(){
        //Error al inicializar el servicio de medicamentos
      });

      scope.ActualizarStatus= function(medicamentoActulizar,status){
                //se actualiza la lista campleta de medicamentos
                _.each(indicacionTerapeutica.medicamentos,function(listaOriginal){
                  _.each(listaOriginal.DOSIS,function(presentacionOriginal){
                    if (presentacionOriginal.NP_MEDICAMENTO_CLAVE==medicamentoActulizar.selected.NP_MEDICAMENTO_CLAVE) {
                      presentacionOriginal.NF_ESTATUS=status;
                     //console.log(presentacionOriginal.NF_ESTATUS);
                    };
                  });
                  
                });

                  //se actualizan medicamentos de la receta  
                _.each(scope.selectedMedicamentos,function(listaClon){
                  _.each(indicacionTerapeutica.medicamentos,function(listaOriginal){
                    _.each(listaClon.DOSIS,function(presentacionClon){
                      _.each(listaOriginal.DOSIS,function(presentacionOriginal){
                        if(presentacionClon.NP_MEDICAMENTO_CLAVE==presentacionOriginal.NP_MEDICAMENTO_CLAVE){
                          presentacionClon.NF_ESTATUS=presentacionOriginal.NF_ESTATUS;
                         // console.log(presentacionClon.NP_MEDICAMENTO_CLAVE+": "+presentacionClon.NF_ESTATUS);
                        }
                      });
                    });
                  });
                });
      }


      /*
      * Función para hacer el autocompletado, se llama cada que se escribe una letra en un md-autocomplete
      */
      scope.querySearchM = function(text){
        var result = indicacionTerapeutica.querySearch(text);
        //result = _.difference(result, scope.selectedMedicamentos);
        return result
      }

      /*
      * Añade un el medicamento que se seleccióno en el autocompletado al arreglo de medicamentos.
      */
      scope.addMedicamento = function(){
        
        // Se agrega el nuevo medicamento.
        // 1. Mandar petición al servidor para ver si este se puede recetar.

        if(currentMedicamento !== null && currentMedicamento !== undefined){
          scope.statusRegistro=true;
          var respaldo='';
          respaldo = JSON.parse( JSON.stringify(currentMedicamento) );
          scope.selectedMedicamentos.unshift(respaldo);

          currentMedicamento.selected='';
          currentMedicamento.indicaciones='';
          currentMedicamento.dias='';
          currentMedicamento.saec='';
          currentMedicamento.meses='';
          //currentMedicamento = null;
          scope.medicamentoText = '';

        }else{
          mensajes.alerta('AVISO','SELECCIONE UN MEDICAMENTO VALIDO','OK');

        }

      }
      /*
      * Esta funcion es llamada cada que se selecciona una forma diferente de medicamento.
      * Busca el medicamento padre e hijo y notifica si se puede recetar o no, de acuerdo
      * a las reglas de negocio.
      */
       scope.cambiarStatus = function(medicamento, $index, folioReceta){
        if (medicamento.selected!==undefined && medicamento.selected!==null) {
          scope.ActualizarStatus(medicamento,'ACTIVO');
          scope.medicamentoAnterior=$index;
          //medicamento.selected.NP_MEDICAMENTO_CLAVE="";
          //scope.selectedMedicamentos.splice($index, 1);

        };
          /*console.log("Presentacion selecionada= "+medicamento.selected.NP_MEDICAMENTO_CLAVE);
         // console.log("-----------------------");
          //console.log("Medtos en Receta: ");
          _.each(scope.selectedMedicamentos,function(MedRec){
            _.each(MedRec.DOSIS,function(Present){
             
              console.log(Present.NP_MEDICAMENTO_CLAVE+"= "+Present.NF_ESTATUS);

            });
              console.log(MedRec);    
          });*/
    

       }

      scope.validarMedicamento=function(medicamento){
        var ban=true;
         _.each(scope.recetaPrevia,function(MedRec){
          if(MedRec.selected.NP_MEDICAMENTO_CLAVE==medicamento.selected.NP_MEDICAMENTO_CLAVE){ban=false;}
         });
         
         return ban;
      }

      scope.changeMedicamento = function(medicamento, $index, folioReceta){
        scope.statusRegistro=false;
        medicamento.validar=scope.validarMedicamento(medicamento);
        
        scope.ActualizarStatus(medicamento,'');
        var forma = medicamento.selected;
          if( angular.isDefined(medicamento.selected) && medicamento.validar) {
            // encontrar el objeto al que se esta apuntando
            //console.log("validando");
            var index = _.findIndex(scope.selectedMedicamentos, function(el){
              return angular.isDefined(el.selected) ? el.selected.DESCRIPCION == forma.DESCRIPCION : false;
            });
            var isValid;
            scope.esCancelar = false;
            if(scope.esCancelar == false){
              indicacionTerapeutica.isValid(forma,scope.recetaACancelar).success(function(data){//Busca duplicidades en la misma nota médica!
                
                var lock = false;
                isValid = true;
                // Debe haber mas de un medicamento seleccionado, para que sea posible la busqueda
                if(scope.selectedMedicamentos.length > 1){
                  // Buscar en los medicamentos que estan seleccionados.
                  // se busca para cada uno, se genera el aviso de duplicidad
                  // con la lista de medicamentos que no puede recetar porque ya
                  // tienen duplicidad
                  var filtered = _.map(data, function(e){ return e.C_MEDICAMENTO_DUPLICADO });
                  // Se hace la busqueda sobre los medicamentos que se estan seleccionando.
                  _.each(scope.selectedMedicamentos, function(e){
                    // Debe tener algun medicamento seleccionado
                    if(angular.isDefined(e.selected)){
                      // Buscamos en la tabla de duplicidad.
                      var duplicates = _.filter(data, function(el){
                        //console.debug("el:",el.C_MEDICAMENTO_DUPLICADO,"-- e:",e.selected.NP_MEDICAMENTO_CLAVE,"-->", forma.NP_MEDICAMENTO_CLAVE);    
                        return (el.C_MEDICAMENTO_DUPLICADO == e.selected.NP_MEDICAMENTO_CLAVE) && (e.selected.NP_MEDICAMENTO_CLAVE!=forma.NP_MEDICAMENTO_CLAVE) ;
                      });
                      // Tiene restricciones sobre el medicamento seleccionado.
                    //console.debug(forma.NP_MEDICAMENTO_CLAVE);
                      //console.debug(duplicates);

                      if(duplicates.length > 0){
                        if(duplicates[0].C_INDICADOR == 'A' && duplicates[0].C_TIPO == 'D'){ // AVISO
                          mensajes.alerta('AVISO','EL PACIENTE TIENE UNA PRESCRIPCIÓN DE '+e.NP_MEDICAMENTO+' EN LA PRESENTE RECETA. EN CASO DE CONTINUAR, SU PRESCRIPCIÓN SERÁ AUDITADA.','OK');

                          //console.log("------"+e.NP_MEDICAMENTO);
                          //console.log(e);
                          medicamento.selected.c_duplicidad =e.selected.NP_MEDICAMENTO_CLAVE; //medicamento.selected.NP_MEDICAMENTO_CLAVE+'';



                        }else if(duplicates[0].C_INDICADOR == 'B' && duplicates[0].C_TIPO == 'D'){ // BLOQUEO
                       
                          mensajes.alerta('AVISO', 'NO PUEDE PRESCRIBIR EL MEDICAMENTO '+medicamento.NP_MEDICAMENTO+" ("+forma.DESCRIPCION+'). <br>EL PACIENTE TIENE TRATAMIENTO CON '+e.NP_MEDICAMENTO+' ('+e.selected.DESCRIPCION+') EN LA PRESENTE RECETA.', 'OK');
                          scope.selectedMedicamentos[index].DOSIS = _.filter(scope.selectedMedicamentos[index].DOSIS, function(val){
                            return val.DESCRIPCION !== forma.DESCRIPCION;
                          });
                          isValid = false;
                        }
                      }
                    }
                  });
                }
              })
              .error(function(err){//Devuelve duplicidades de Recetas anteriores
                if (err.bloqueo == 1) {//Duplicidad Bloqueo
                  isValid = false;
                  scope.selectedMedicamentos[index].DOSIS = _.filter(scope.selectedMedicamentos[index].DOSIS, function(val){
                    return val.DESCRIPCION !== forma.DESCRIPCION;
                  });
                  scope.selectedMedicamentos[index].selected = null;
                  if(scope.selectedMedicamentos[index].DOSIS.length == 0){
                    // Ya no tiene opciones para que se le agregue
                    scope.selectedMedicamentos.splice(index, 1);
                  }
                  // Eliminar el medicamento que no puede recetar
                  mensajes.alerta('AVISO', 'NO PUEDE PRESCRIBIR EL MEDICAMENTO '+forma.DESCRIPCION+'. '+err.error.toUpperCase(), 'ACEPTAR');
                }else {//Duplicidad Aviso
                  isValid = true;
                  var dup= trim(err.error.split(":")[1]);
                  mensajes.alerta('AVISO', err.error.toUpperCase(), 'ACEPTAR');
                  // MEDICAMENTO AL QUE SE LE ENVIA EL AVISO DE DUPLICIDAD
                  medicamento.selected.c_duplicidad =dup;//= medicamento.selected.NP_MEDICAMENTO_CLAVE+'';
                }
              });
            }
            // reglas para el tiempo maximo de medicamentos
            //console.log("esHosp: "+scope.esHosp);
            //console.log(scope.esHosp);
          }  
        
            scope.esUrgencias = (scope.esHosp == 'true') ? 'false' : 'true';
            medicamento.max = scope.esUrgencias == 'true' ? (medicamento.selected.ANTIBIOTICO == 'NO') ? 7: 10  :
            (medicamento.selected.CBASICO_CLAVE.indexOf("CBMC") >= 0 || medicamento.selected.CATEGORIA_CLAVE == 5) ? 90 : 180;
            console.log("medicamento.max: "+ medicamento.max);
        
      };

      /*
      * Elimina un medicamento del arreglo de medicamentos.
      */
      scope.removeMedicamento = function(medicamento){
        scope.statusRegistro=false;
        //console.log(medicamento);
        if (medicamento.selected!==undefined && medicamento.selected!==null) {scope.ActualizarStatus(medicamento,'ACTIVO');};
       ;
        scope.selectedMedicamentos = _.without(scope.selectedMedicamentos, medicamento);
        medicamento.selected='';
          medicamento.indicaciones='';
          medicamento.dias='';
          medicamento.saec='';
          medicamento.meses='';
      };

      /*
      * Cada que se cambia la selección de medicamento, se guarda en una variable que representa el medicamento
      * al que se esta haciendo referencia.
      */
      scope.selectedItemChangeM = function(_medicamento){
        currentMedicamento = _medicamento;
      };

      /*
      * Muestra en un dialogo la información de ayuda para un medicamento en especifico.
      */
      scope.help = function(medicamento){
        mensajes.alerta('', _.filter(medicamento.DOSIS, function(el){
          return el.DESCRIPCION === medicamento.selected.DESCRIPCION;
        })[0].DOSIS
        , 'Aceptar');
      };

      /*
      * Crea el objeto json que se enviara al servidor y envia un evento a la directiva para que
      * se notifique que se va a hacer el guardado.
      */
      scope.finalizar = function(){
       
       // scope.deshabilitaBtnFinalizar= (scope.idNota==-404) ? false : true;// si viene de cancelar reseta o consulta externa
       //console.log("deshabilitaBtnFinalizar"+scope.deshabilitaBtnFinalizar);
       //console.debug("scope.esCancelar",scope.esCancelar);
        scope.deshabilitaBtnFinalizar= scope.esCancelar;
        //console.log("deshabilitaBtnFinalizar"+scope.deshabilitaBtnFinalizar);

        // validar que este la firma electronica, de lo contrario enviar aviso
        if(validaFirmaElectronica() || !scope.permissionsForSigning){
          var jsonResult = {MEDICAMENTOS : _.map(scope.selectedMedicamentos, function(el){
            // RETORNAR LOS MEDICAMENTOS COMO LOS PIDE EL JSON
            var complementObject = {
              C_INDICACIONES:     el.indicaciones,
              C_NOMBRE_MEDICAMENTO:   el.NP_MEDICAMENTO + ' ( ' + el.selected.DESCRIPCION + ' )',
              C_CANTIDAD:       (el.cantidad === undefined || el.cantidad === '') ? '0': el.cantidad+'',
              N_DIAS:         el.dias,
              N_MESES:          el.meses,
              N_SAEC:         el.saec,
              CBASICO_CLAVE: el.selected.CBASICO_CLAVE,
              CATEGORIA_CLAVE: el.selected.CATEGORIA_CLAVE,
              NP_MEDICAMENTO_CLAVE: el.selected.NP_MEDICAMENTO_CLAVE
            };
            if(angular.isDefined(el.selected.c_duplicidad)){
              complementObject.C_DUPLICIDAD  = el.selected.c_duplicidad+'';
            }
            return  complementObject;
          })};
          jsonResult.C_TRATAMIENTO = scope.C_TRATAMIENTO;
          jsonResult.C_JUSTIFICACION_INDICACIONES = '';
          jsonResult.C_PLAN = scope.C_PLAN;

          indicacionTerapeutica.receta = jsonResult;
          // Se injecta la firma electronica en el servicio de indicación terapeutica
          // No es residente, si tiene permisos para firmar la receta
          if(scope.permissionsForSigning) indicacionTerapeutica.setFirmaElectronica(scope.fe);
          indicacionTerapeutica.C_PLAN = scope.C_PLAN;
          //console.log(indicacionTerapeutica);
          
          //scope.deshabilitaBtnFinalizar= scope.guardaIndicacionTerapeutica();
            scope.guardaIndicacionTerapeutica().then(function(data){ //se guarda receta pero tarda mucho porque consulta la bd
            scope.deshabilitaBtnFinalizar=data;//esperamos el resultado 
            console.debug("scope.deshabilitaBtnFinalizar",data);            
          }).catch(function(error){
            //console.error(error);
            scope.deshabilitaBtnFinalizar=error;
            //console.debug("scope.deshabilitaBtnFinalizar",error);            

          });

          
        }else{
          scope.deshabilitaBtnFinalizar=false;
          mensajes.alerta('AVISO', 'TIENE QUE SELECCIONAR SU FIRMA ELECTRONICA PARA PODER CONTINUAR', 'ACEPTAR');
        }
      }

      /*
      * Valida que el medicamento sobre el que se esta editando la información
      * se pueda recetar por mas de 30 dias.
      */
      scope.calculate = function(medicamento){
        //validar el numero de dias que ingreso para poner el calculo
        if(medicamento.dias <= medicamento.max){
          if(medicamento.dias == ''){
            medicamento.meses = undefined;
            medicamento.saec = undefined;
          }else{
            if(medicamento.dias >= 30){
              medicamento.saec = 'S';
            }else{
              medicamento.saec = 'N';
            }
            medicamento.meses = Math.trunc(medicamento.dias / 30) ;
            medicamento.fechasSurtimiento = calculateDates(medicamento.meses);
          }
        }else{
          medicamento.fechasSurtimiento = [];
          medicamento.meses = undefined;
          medicamento.saec = undefined;
        }
      }

      /*
      * Es un handler que recibe el contenido de un archivo de texto
      * para guardar la información en el servicio de la llave
      */
      scope.cargaLlave = function (fileContent){
        scope.fe.FIRMA.keyPriv = fileContent;
      };

      /*
      * Es un handler que recibe el contenido de un archivo de texto
      * para guardar la información en el servicio del certificado
      */
      scope.cargaCertificado = function (fileContent){
        scope.fe.FIRMA.certificado = fileContent;
      };

      /*
      * Lanza un evento para cambiar de tab
      */
      scope.siguiente = function(){
        scope.guardaIndicacionTerapeutica();
      };

      /*
      * Valida que la firma y el cerficado ya exista.
      */
      function validaFirmaElectronica(){
        return scope.fe.FIRMA.keyPriv && scope.fe.FIRMA.certificado;
      }

      /*
      * Calcula la fecha de surtimiento de medicamentos.
      */
      function calculateDates(months){
        var dates = [];

        for(var i = 0; i < months; i++){
          dates.push(moment().add(30 * i, 'days').format('DD/MM/YYYY'));
        }
        return dates;

      }

      scope.saveMedicamentosTemporal=function(plan,tratamiento){
         indicacionTerapeutica.setMedicamentos(scope.selectedMedicamentos,plan,tratamiento);
      }

    }
  };

function ltrim(str, filter){
      filter || ( filter = '\\s|\\&nbsp;' );
      var pattern = new RegExp('^(' + filter + ')*', 'g');
      return str.replace(pattern, "");
    }

    function rtrim(str, filter){
      filter || ( filter = '\\s|\\&nbsp;' );
      var pattern = new RegExp('(' + filter + ')*$', 'g');
      return str.replace(pattern, "");
    }

    function trim(str, filter){
      filter || ( filter = '\\s|\\&nbsp;' );
      return ltrim(rtrim(str, filter), filter);
    }



}]);

})();