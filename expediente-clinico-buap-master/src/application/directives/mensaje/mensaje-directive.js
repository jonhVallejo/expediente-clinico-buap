(function(){

	'use strict';


	angular.module('expediente')
	.directive('mensajeDatos', function(){

		return {
			 restrict: 'E',			
			 templateUrl: '/application/directives/mensaje/mensaje.html',
			 scope:{
			 	mensaje: '=mensaje'
			 }
		}

	});


})();