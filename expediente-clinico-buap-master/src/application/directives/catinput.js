'use strict';

/**
 * @ngdoc directive
 * @name expediente.directive:catinput
 * @description
 * # catinput
 */
angular.module('expediente').run(['$templateCache', function ($templateCache) {
  $templateCache.put('partials/cat-input.tmpl.html',
    '<md-data-table-toolbar>\
      <div>\
  	    <md-input-container flex="33" class="form-item">\
  			 	<label>{{title}}</label>\
  			    <input type="text"  maxlength="30" name="concepto" ng-model="ngModel" required capitalize ng-pattern="/^[a-zA-Z ÑÁÉÍÓÚñáéíóú\s]+$/">\
        </md-input-container>\
      </div>\
		 <div>\
        <md-button class="md-raised md-warning" ng-click="click()">CANCELAR</md-button>\
	      <md-button class="md-raised md-primary" type="submit" ng-disabled="!formulario.$valid">{{name}}</md-button>\
	   </div>\
     </md-data-table-toolbar>'
    );
}]).directive('catInput', function ( $window) {
    var directiva={};
    directiva.restrict= 'E';
    directiva.templateUrl= 'partials/cat-input.tmpl.html';
    directiva.scope= {
      ngModel : '=',
      title: '@',
      name:'@',
      click: '&',
      formulario : "=formulario"
    };
      return directiva;
  });
