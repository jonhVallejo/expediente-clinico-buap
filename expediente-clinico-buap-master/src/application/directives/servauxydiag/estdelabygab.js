'use strict';

/**
 * @ngdoc directive
 * @name expediente.directive:servauxydiag\estdelabygab
 * @description
 * # servauxydiag\estdelabygab
 * ver Ver a l 5/2/2016 7:15
 */
angular.module('expediente').directive('estLab', function () {
    return {
      templateUrl: 'partials/est-lab.tmpl.html',
      restrict: 'E',
      scope : 	{
			      ngModel : '=',
			      title: '@',
			      name:'@',
			      click: '&',
			      isAgenda: '='			      
			    }     
    };
  });
