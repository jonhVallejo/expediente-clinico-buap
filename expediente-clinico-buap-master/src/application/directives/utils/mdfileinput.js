'use strict';

/**
 * @ngdoc directive
 * @name expediente.directive:utils/mdFileInput
 * @description
 * # utils/mdFileInput
 */
 angular.module('expediente')
 .directive('mdFileInput', function ($compile) {
 	return {
 		template: '<div layout="row" layout-align="center center">' +
 		'<input type="file" ng-show="false"/>' +
 		'<md-input-container>' +

 		'<label flex>NOMBRE DEL ARCHIVO</label>' +
 		'<input ng-model="fileName" ng-disabled="true" />' +
 		'</md-input-container>' +
 		'<md-button md-no-ink type="button" class="md-primary firma-documentos" ng-click="show()">{{title}}</md-button></div>',
 		restrict: 'E',
 		replace : true,
 		scope : {
 			items : '='
 		},

 		link: function postLink(scope, element, attrs) {

 			scope.title = attrs['title'];

 			scope.fileName = '';

 			scope.file;

 			var input = element.find('input');

 			input.on('change', function(onChangeEvent){
 				var reader = new FileReader();

 				scope.$apply(function(){
 					scope.file = onChangeEvent.target.files[0];
 					scope.fileName = scope.file.name;

 				});



 			});



 			scope.show = function(){
 				input.click();
 			};


 		}
 	};
 });


 var __element;
