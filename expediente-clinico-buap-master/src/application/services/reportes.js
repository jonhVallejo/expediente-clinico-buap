'use strict';

/**
 * @ngdoc service
 * @name expediente.reportes
 * @description
 * # reportes
 * Service in the expediente.
 */
angular.module('expediente')
  .service('reportes', function ($localStorage , mensajes) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.getReporte = function(url , type , size){
    	if(url!=undefined){
    		if(type === undefined)
	    		type = '_blank';
	    	if(size === undefined)
	    		size = 'width=800, height=640';

          if(url.toLowerCase().indexOf('blob') >= 0){
            window.open(url, type, size);
          }else{
            window.open(url + '?access_token='+ $localStorage.userSecurityInformation.token.value, type, size);
          }
    	}
    	else{
    		mensajes.alerta("ERROR","NO SE PUEDE ACCEDER A LA DIRECCIÓN DEL REPORTE","ACEPTAR");
    	}
	};

  });
