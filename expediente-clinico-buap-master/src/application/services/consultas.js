(function(){
'use strict';

/**
 * @ngdoc service
 * @name expediente.consultas
 * @description
 * # consultas
 * Service in the expediente.
 */
angular.module('expediente').service('consultas', ['$http','$localStorage', function ($http,$localStorage) {
    
	var consulta = baseURL + 'notamedica/';

	this.getNotaMedica = function(idPaciente,idAgenda){
		return $http.get(consulta+idPaciente+'/'+idAgenda);
	};

	this.switchCita = function(idCita){
		return $http.put(consulta+'registro/'+idCita);
	}

	this.guardaNotaMedica = function(nota){
		return $http.post(consulta,nota);
	};

	this.guardaSignosVitales = function(signos){
		return $http.post(consulta,signos);
	};

	this.getIMC = function(){
		return $http.get(consulta+'imc/');
	};

	this.getEstudiosAnteriores = function(idAgenda){
		return $http.get(consulta+'gabinete/'+idAgenda)
	};

	this.startCita = function(datos) {
		return $http.post(consulta+'registroUrgencias/',datos);
	};

	this.getTipoNotas = function(){
		return $http.get(consulta+'tipoNota/');
	};

	this.guardaNotaUrgencias = function(nota){
		return $http.post(consulta + 'urgencias/',nota);
	};

	this.getEstatusNotaMedica = function(idNota){
		return $http.get(consulta + 'estatusNota/' + idNota);
	};

	this.terminarNotaMedicaUrgencias = function(idNota){
		return $http.put(consulta + 'finUrgencias/' + idNota);
	};

	this.getNotaMedicaUrgencias = function(idNota){
		return $http.get(consulta + 'urgencias/' + idNota);
	};

	this.getPacientesUrgencias = function(idMedico,tipo){
		if(tipo != undefined)
			return $http.get(consulta + 'pacientesUrgencias/' + tipo + '/' + idMedico);
		else
			return $http.get(consulta + 'pacientesUrgencias/notas/' + idMedico);
	};

	this.solicitudInterconsulta = function(solicitud){	
		return $http.post(consulta + 'solicitudInterconsulta/',solicitud);
	};

	this.getSolicitudesInterconsulta = function(tipo){
		if(tipo != undefined)
			return $http.get(consulta + 'pacientesInterconsulta/' + tipo);
		else
			return $http.get(consulta + 'pacientesInterconsulta/');
	};

	this.getDiagnosticoIngreso = function(idPaciente){
		return $http.get(consulta + 'diagnosticoIngreso/' + idPaciente);
	};

	this.setFirmaElectronica = function(firma){
		return $http.post(consulta + 'firmar/', firma);
	}

	this.getEvolucionAlta = function(idMedico , tipo){
		if(tipo != undefined)
			return $http.get(consulta + 'pacientesDevolucion/' + tipo + '/' + idMedico);
		else
			return $http.get(consulta + 'pacientesDevolucion/' + idMedico);
	};

	this.cancelarEvolucionAlta = function(idNota){
		return $http.delete(consulta + 'cancelarNota/' + idNota);
	};

	this.getAllNotasMedicas = function(idPaciente){
		return $http.get(consulta + idPaciente);
	};

	this.getTriage = function(idNota){
		return $http.get(consulta + 'triage/' +idNota);
	};

	this.getNotasCIE10 = function(idCie10 , idPaciente){
		return $http.get(consulta + 'cie10/' + idCie10 + '/' + idPaciente);
	};

	this.getNotasUrgencias = function(idCie10 , idPaciente,idProceso){
		return $http.get(consulta + 'cie10urgencias/' + idCie10 + '/' + idPaciente + '/' + idProceso);
	};

	this.getNotasEgresoUrgencias = function(idNota , idPaciente){
		return $http.get(consulta + 'urgenciasegreso/' + idPaciente + '/' + idNota);
	};

	this.getIndicacionTereapeutica = function(idReceta){
		return $http.get(baseURL + 'receta/' + idReceta);
	};

	this.getEstatusHistoriaClinica = function(idNotaMedica,idEspecialidad){
		return $http.get(baseURL + 'cuestionario/estatusHistoria/' + idNotaMedica + '/' + idEspecialidad);	
	};

	this.getDestinoPaciente = function(){
		return $http.get(baseURL + 'notamedica/origenPaciente');
	};

	this.cancelJustificacionUrgencias = function(cancelacion){
		return $http.delete(baseURL + 'notamedica/cancelarNota/', { data: cancelacion});
	};

	this.getCicloUrgencias = function(idPaciente,idNota,idProceso){
		return $http.get(baseURL + 'notamedica/urgenciasciclo/' + idPaciente + '/' + idNota + '/' + idProceso); 
	};

	this.getSignosVitalesAnteriores = function(idPaciente){
		return $http.get(baseURL + 'notamedica/signosvitalesAnterior/' + idPaciente);
	};

	this.getSignosVitalesTriage = function(idTriage){
		return $http.get(baseURL + 'triage/' + idTriage);	
	};

	this.deleteSolicitudEstudio = function(estudio){
		return $http.delete(baseURL + 'notamedica/gabinete/', { data: estudio});
	};

	this.getEstudiosSolicitados = function(idNota){
		return $http.get(baseURL + 'notamedica/gabineteNota/' + idNota);
	};

	this.getValidacionEstudios = function(idPaciente , idEstudio){
		return $http.get(baseURL + 'notamedica/validaestudio/' + idPaciente + '/' + idEstudio);
	};

	this.guardaOrdenMedica = function(ordenMedica){
		return $http.post(baseURL + 'notamedica/ordenmedica/' , ordenMedica);
	};

	this.getAgendaMedicaHoy = function(){
		return $http.get(baseURL + 'agendamedica/today');
	};

	this.getServiciosPaciente = function(idPaciente){
		return $http.get(baseURL + 'notamedica/servicio/' + idPaciente);
	};

	this.getEstatusSignosVitales = function(idCita){
		return $http.get(baseURL + 'notamedica/validasignos/' + idCita);
	};

	this.getPacientesProceso = function (idMedico , triage){
		return $http.get(baseURL + 'notamedica/pacientesPendientes/' + triage + '/' + idMedico);
	};

	this.getIncapacidadTemporal = function (idPaciente){
		return $http.get(baseURL + 'incapacidadTemporal/' + idPaciente);
	}

	this.upgradeEstudios = function (estudios){
		return $http.put(baseURL + 'notamedica/gabinete/' , estudios);
	};

	this.getAmarillosConsulta = function (idMedico){
		return $http.get(baseURL + 'triage/AMARILLOS/' + idMedico);
	};

	this.getEstudiosPaciente = function (idPaciente){
		return $http.get(baseURL + 'notamedica/estudiosPaciente/'+idPaciente);
	};

	this.notasPaciente = function (idPaciente){
		return $http.get(baseURL + 'notaspaciente/' + idPaciente);
	};

  }]);
})();