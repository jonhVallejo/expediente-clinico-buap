'use strict';

/**
 * @ngdoc service
 * @name expediente.mensajes
 * @description
 * # mensajes
 * Service in the expediente.
 */
 angular.module('expediente')
 .service('mensajes', ['$mdDialog', '$q', function ($mdDialog, $q) {

 	this.alerta = function(title,cont,ok){
 		var defered = $q.defer();
 		var promise = defered.promise;
 		$mdDialog.show(
 			$mdDialog.alert()
 			.clickOutsideToClose(true)
 			.title(title)
 			.content(cont)
 			.ok(ok)
 			).then(function(){

 			defered.resolve();
 		}, function(){
 			defered.resolve();
 		});


 		return promise;
 	};

 	this.alertaTimeout = function(_title, cont, _ok, _timeout){
 		
 		

 		var defered = $q.defer();
 		var promise = defered.promise;

 		var alert;

 		alert =  $mdDialog.alert({
        title: _title,
        content: cont,
        ok: _ok,
        clickOutsideToClose:true,
        controller: function($timeout, $mdDialog){
 		 	
 		 		$timeout(function(){
 		 			$mdDialog.hide();
 		 		}, _timeout);
 		 	}
      	});


      	$mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
          defered.resolve();
        });

/*
 		 $mdDialog.show({
 		 	controller: function(){
 		 		console.log('hay putos');
 		 	},
 		 	title: _title,
 		 	content: cont,
 		 	clickOutsideToClose:true
    })
    .then(function(answer) {
      defered.resolve();
    }, function() {
      defered.resolve();
    });

*/


/*
 		$mdDialog.show(
 			$mdDialog.alert()
 			.clickOutsideToClose(true)
 			.title(title)
 			.content(cont)
 			.ok(ok)
 			).then(function(){

 			defered.resolve();
 		}, function(){
 			defered.resolve();
 		});
*/


 		return promise;
 	}

 	this.confirmacion = function(title,cont,ok){
 		var confirm = $mdDialog.confirm()
 		.title(title)
 		.content(cont)
 		.cancel('CANCELAR')
 		.ok(ok)
 		return $mdDialog.show(confirm);
 	};

    this.mConfirmacion = function(title,cont,no,ok){
    var confirm = $mdDialog.confirm()
    .title(title)
    .content(cont)
    .cancel(no)
    .ok(ok)
    return $mdDialog.show(confirm);
  };

  this.mConfirmacionValida = function(title,cont,ok){
    return  $mdDialog.show(
          $mdDialog.alert()
          .clickOutsideToClose(false)
          .title(title)
          .content(cont)
          .ok("Ok")
      );
  };

 	this.errorConexion = function(){
 	  $mdDialog.show(
 			$mdDialog.alert()
 			.clickOutsideToClose(true)
 			.title("ERROR DE CONEXIÓN")
 			.content("ES IMPOSIBLE CONECTARSE CON EL SERVIDOR")
 			.ok("ACEPTAR")
 			);
 	};

 	this.datoNoEncontrado = function(){
 		$mdDialog.show(
 			$mdDialog.alert()
 			.clickOutsideToClose(true)
 			.title("ALERTA")
 			.content("NO SE ENCONTRÓ EL DATO BUSCADO")
 			.ok("ACEPTAR")
 			);
 	};

  this.infoNoCargada = function(){
    $mdDialog.show(
      $mdDialog.alert()
      .clickOutsideToClose(true)
      .title("ALERTA")
      .content("NO SE PUDO CARGAR LA INFORMACIÓN")
      .ok("ACEPTAR")
      );
  };

 	this.errorConexionBD = function(){
 		$mdDialog.show(
 			$mdDialog.alert()
 			.clickOutsideToClose(true)
 			.title("ERROR")
 			.content("NO SE PUEDE CONECTAR A LA BASE DE DATOS")
 			.ok("ACEPTAR")
 			);
 	};


 	this.showReportDialog = function(report){

 		var defered = $q.defer();
 		var promise = defered.promise;

 		var reportDialog = $mdDialog.confirm()
 		.title(report.title)
 		.content('All of the banks have agreed to <span class="debt-be-gone">forgive</span> <h1>dsads</h1> you your debts.')
 		//.content('<object data="'+report.url+'" type="application/pdf" width="300" height="200">')
 		.ariaLabel(report.title)
 		.ok('FINALIZADO');


 		$mdDialog.show(reportDialog).then(function(){
 			defered.resolve();
 		}, function(){
 			defered.resolve();
 		});


 		return promise;
 	};



 }]);
