/**
 * @ngdoc service
 * @name expediente.adquisiciones
 * @description
 * # adquisiciones
 * Service in the expediente.
 */
(function(){
  'use strict';
  angular.module('expediente')
    .service('adquisiciones',['$http', '$q',  function ($http, $q) {
     var self = this;
     var baseUrl="http://148.228.103.38:8080/controller/"

    self.getDatos = function(url) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(baseUrl + url)
            .success(function(data, status, headers, config) {
                defered.resolve({data:data, status: status, headers: headers, config: config});
            })
            .error(function(data, status, headers, config) {
                defered.reject({data:data, status: status, headers: headers, config: config});
            });

        return promise;
    };

    self.postDatos = function(url,data) {
        var defered = $q.defer();
        var promise = defered.promise;
        console.log(baseUrl + url);
        console.log(data);
        $http.post(baseUrl + url,data)
            .success(function(data) {
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);
                //defered.resolve({data:data, status: status, headers: headers, config: config});
            })
            .error(function(data, status, headers, config) {
                //defered.reject({data:data, status: status, headers: headers, config: config});
            });

        return promise;
    };

    // self.postDatos = function(url,data) {
    //     var defered = $q.defer();
    //     var promise = defered.promise;

    //     $http.post(baseUrl + url, data)
    //         .success(function(data) {
    //             defered.resolve(data);
    //         })
    //         .error(function(err) {
    //             defered.reject(err)
    //         });

    //     return promise;
    // };

    self.putDatos = function(url,data) {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.put(baseUrl + url, data)
            .success(function(data) {
                defered.resolve(data);
            })
            .error(function(err) {
                defered.reject(err)
            });

        return promise;
    };

    }]);
})();
