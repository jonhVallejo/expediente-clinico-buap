'use strict';

/**
 * @ngdoc service
 * @name expediente.medicos
 * @description
 * # medicos
 * Service in the expediente.
 */
angular.module('expediente').service('medicos', function ($http, $q,pacientes) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var horarioDia='';
    var nombreMedico='';
    var nombreEspecialidad = '';
    var diaMedico='';
    var idMedico='';
    var idEspecialidad='';
    var idTurno='';
    //var asociarTurno='';

    var cita={};




    var actionURL = 'agendamedica/';
    var verCanceladas = 'canceladas/';

    var catalogoURL = baseURL + 'catalogo/';

    var verDisponibilidad = baseURL + 'agenda/';

    var getListMedicos = baseURL + 'personal/medicos/';
    var getAllListMedicos = baseURL + 'usuarios/2';
    var getAllListMedicosFull = baseURL + 'personal/medicos';
    var getAgendaDia = baseURL +'agendamedica/';

    var setPerfilMedico = baseURL + 'usuarios/';

    var perfilesDisponibles = baseURL + 'paciente/candidatos/';
    var actualizarMedico = baseURL + '';

    this.getEspecialidades = function(){
        return $http.get(catalogoURL+"8");
    };

    this.getTurno = function(){
        return $http.get( catalogoURL+"13");
    };

    this.cargaDia = function(dia){
    	diaMedico=dia.split(" ")[0];
    };

    this.getDia = function(){
    	return diaMedico;
    };

    this.cargaMedico = function(medico){
        nombreMedico=medico.C_PRIMER_APELLIDO+" "+medico.C_SEGUNDO_APELLIDO+" "+medico.C_NOMBRE;
        nombreEspecialidad= medico.ESPECIALIDADES.CF_ESPECIALIDAD;
        idMedico = medico.N_EXPEDIENTE;
        idTurno = medico.TURNOS.NF_TURNO;
    };


    this.getMedico = function(){
        return {'nombreMedico':nombreMedico,'nombreEspecialidad':nombreEspecialidad,'idMedico':idMedico,'idTurno':idTurno};
    };

    this.cargaHorario = function(horario){
        horarioDia=horario.split(" ")[1];
    };

    this.getHorario = function(){
        return horarioDia;
    };

    this.confirmarCita = function(citaC){
        cita.especialidad=citaC.especialidad;
        cita.medico= citaC.medico;
        cita.idMedico = citaC.idMedico;
        cita.FECHA= citaC.dia;
        cita.HORA= citaC.hora;
        cita.paciente = pacientes.obtenerNombrePaciente();
        cita.idPaciente = pacientes.obtenerIdPaciente();
    };

    this.actualizarCita = function(){
        cita.paciente = pacientes.obtenerNombrePaciente();
        cita.idPaciente = pacientes.obtenerIdPaciente();
    };

    this.getCita = function(){
        return cita;
    };

    this.getDiasDisponibles= function(idMedico){
        return $http.get(verDisponibilidad+idMedico);
    };

    this.getHorariosDisponibles= function(idMedico, idTurno, dia){
        return $http.get(verDisponibilidad+idMedico+"/"+idTurno+"/"+dia);
    };

    this.cargaIdEspecialidad = function(id){
        idEspecialidad=id;
    };

    this.getIdEspecialidad = function(){
        return idEspecialidad;
    };

    this.setIdMedico = function(id){
        idMedico=id;
    };

    this.getIdMedico = function(){
        return idMedico;
    };

    this.getMedicos = function(idEspecialidad){
        return $http.get(getListMedicos+idEspecialidad);
    };

    /*this.getMedicos = function(){
        console.log("getListMedicos");
        console.log(getListMedicos);
        return $http.get(getListMedicos);
    };*/

    this.getAllMedicos = function(){
        return $http.get(getAllListMedicos);
    };

    this.getAllMedicosFull = function(){
        return $http.get(getAllListMedicosFull);
    };

    this.getAgenda = function(beginDate, endDate, especialidad, medico){

        var agendaURL = baseURL + actionURL
        + beginDate + '/' + endDate;
        agendaURL += (especialidad !== undefined) ? '/' + especialidad.ID : '';
        agendaURL += (medico !== undefined) ? '/' + medico.N_EXPEDIENTE: '';
        return $http.get(agendaURL);
    }


    this.getCanceladas = function(beginDate, endDate, especialidad, medico){

        var canceladasURL = baseURL + actionURL + verCanceladas
        + beginDate + '/' + endDate;

        canceladasURL += (especialidad !== undefined) ? '/' + especialidad.ID : '';
        canceladasURL += (medico !== undefined) ? '/' + medico.N_EXPEDIENTE: '';

        return $http({
            url : canceladasURL,
            method : 'GET',
            data: '',
            dataType : 'json',
            headers:{
                'Content-Type': 'application/json',
            }
        });

    }

    this.getAgendaDia = function(dia,idEspecialidad,idMedico){
        return $http.get(getAgendaDia+dia+'/'+dia+'/'+idEspecialidad+'/'+idMedico);
    };

    this.getAgendaHoy = function(){
        //caca
         var defered = $q.defer();
        var promise = defered.promise;

        //var _t = baseURL;

        //baseURL = 'http://148.228.103.13:8080/';

        $http.get(baseURL + 'agendamedica/hoy').success(function(data){
            defered.resolve(data);
        }).error(function(err){
            err: err
        });

        //baseURL = _t;

        return promise;
    };

    this.getMedicoActual = function(idMedico){
        return $http.get(getListMedicos+"0/"+idMedico);
    };

    this.setEspecialidad = function(medico){
        return $http.post(setPerfilMedico+'especialidad',medico);
    };

    this.setTurno = function(medico){
        return $http.post(setPerfilMedico+'turno',medico);
    };

    this.actualizaEspecialidad = function(medico){
        return $http.put(setPerfilMedico+'especialidad',medico);
    };

    this.setPerfil = function(medico){
        return $http.post(setPerfilMedico,medico);
    };

    this.getDisponibles = function(query){
        return $http.get(perfilesDisponibles+query);
    };

    this.bajaMedico = function(medico){
        return $http.delete(setPerfilMedico+'especialidad/', { data: {
            'NP_PERSONAL': medico.N_EXPEDIENTE_ANIO.replace("/",""),
            'NP_ESPECIALIDAD': medico.NF_ESPECIALIDAD,
            'NP_TURNO' : medico.NF_TURNO,
            'NP_PERFIL' : medico.N_PERFIL
        }});
    };

    this.actualizaEspecialidad = function(medico){
        return $http.put(setPerfilMedico+'especialidad',medico);
    };

    this.getMedicoInformation = function(idMedico, idEspecialidad){
      return $http.get(getListMedicos + idEspecialidad + '/' + idMedico);
    }

  });
