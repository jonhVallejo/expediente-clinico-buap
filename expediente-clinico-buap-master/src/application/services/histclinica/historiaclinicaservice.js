(function(){
'use strict';

/**
 * @ngdoc service
 * @name expediente.histClinica/historiaClinicaService
 * @description
 * # histClinica/historiaClinicaService
 * Service in the expediente.
 */
angular.module('expediente')
  .service('historiaClinicaService',['$location', function ($location) {
  	var self = this;
    var procedencia=undefined;
  	this.pagina = {
      fichaidentificacion : '/application/views/histclinica/fichaidentificacion.html',
  		fichaident : '/application/views/cuestionarios/cuestionario_contestar.html',
  		heredofamiliares : '/application/views/cuestionarios/cuestionario_contestar.html',
  		nopatologicos : '/application/views/histclinica/nopatologicos.html',
  		patologicos : '/application/views/histclinica/patologicos.html',
  		ginecobstetricos : '/application/views/histclinica/ginecobstetricos.html',
  		padecimientoactual : '/application/views/histclinica/padecimientoactual.html',
  		aparatosysistemas : '/application/views/histclinica/aparatosysistemas.html',
  		exploracionfisica : '/application/views/histclinica/exploracionfisica.html',
  		laboratoriosygabinete : '/application/views/histclinica/laboratoriosygabinete.html',
  		terapeuticaempleada : '/application/views/histclinica/terapeuticaempleada.html',
  		diagnostico : '/application/views/histclinica/diagnostico.html',
  		pronostico : '/application/views/histclinica/pronostico.html',
      notamedica : '/application/views/histclinica/notamedica.html',
      notaegreso : '/application/views/urgencias/notaegreso.html',
      //NOTA PREANESTESICA
      fichaEvaluacionClinica : '/application/views/quirofano/notapreanestesica/evaluacionClinica/evaluacion.html',
      fichaTipoAnestesia : '/application/views/quirofano/notapreanestesica/tipoAnestesia/tipoanestesia.html',
      fichaRiesgoAnestesico : '/application/views/quirofano/notapreanestesica/riesgoAnestesico/riesgoanestesico.html',
      fichaMedicacionPreanestesica : '/application/views/quirofano/notapreanestesica/medicacionPreanestesica/medicacion.html',
      //Riesgo Anestesico
      fichaGoldman : '/application/views/quirofano/notapreanestesica/riesgoAnestesico/goldman.html',
      fichaLangeron : '/application/views/quirofano/notapreanestesica/riesgoAnestesico/langeron.html',
      fichaRiesgoTromboembolico : '/application/views/quirofano/notapreanestesica/riesgoAnestesico/riesgotromboembolico.html',
      //NOTA POSTANESTÉSICA
      medicamentosutilizados : '/application/views/quirofano/medicamentosutilizados.html',
      duracionanestesia : '/application/views/quirofano/duracionanestesia.html',
      cuantificaciones : '/application/views/quirofano/cuantificaciones.html',
      estadoclinico : '/application/views/quirofano/estadoclinico.html',
      padecimientoActual : '/application/views/cuestionarios/cuestionario.html',
      antecedentesPatologicos : '/application/views/cuestionarios/cuestionario.html',
      antecedentesGineco : '/application/views/cuestionarios/cuestionario.html',
      default : '/application/views/histclinica/historiaclinica.html'
  	};
    this.secciones=[{titulo:'FICHA DE IDENTIFICACIÓN',visible: false,id: 1,buscarCuest: false,activo:false},
                    {titulo:'ANTECEDENTES HEREDOFAMILIARES',visible: false,id: 2,buscarCuest: true,activo:false},
                    {titulo:'ANTECEDENTES PERSONALES NO PATOLÓGICOS',visible: false,id: 3,buscarCuest: true,activo:false},
                    {titulo:'ANTECEDENTES PERSONALES PATOLÓGICOS',visible: false,id: 4,buscarCuest: true,activo:false},
                    {titulo:'ANTECEDENTES GINECO-OBSTÉTRICOS',visible: false,id: 5,buscarCuest: true,activo:false},
                    {titulo:'PADECIMIENTO ACTUAL',visible: false,id: 6,buscarCuest: true,activo:false},
                    {titulo:'INTERROGATORIO POR APARATOS Y SISTEMAS',visible: false,id: 7,buscarCuest: true,activo:false},
                    {titulo:'EXPLORACIÓN FÍSICA',visible: false,id: 8,buscarCuest: true,activo:false},
                    {titulo:'RESULTADOS DE LABORATORIO Y GABINETE',visible: false,id: 9,buscarCuest: true,activo:false},
                    {titulo:'TERAPÉUTICA EMPLEADA Y RESULTADOS OBTENIDOS',visible: false,id: 10,buscarCuest: true,activo:false},
                    {titulo:'DIAGNÓSTICO',visible: false,id: 11,buscarCuest: false,activo:false},
                    {titulo:'PRONÓSTICO',visible: false,id: 12,buscarCuest: false,activo:false}
                  ];
    this.getSecciones=function(){
      return this.secciones;
    };

  }]);


})();
