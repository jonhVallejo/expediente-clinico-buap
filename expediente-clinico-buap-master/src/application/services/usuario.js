'use strict';

/**
 * @ngdoc service
 * @name expediente.usuario
 * @description
 * Author: Jonathan Vallejo
 * Modifications:
 * 18-ago-2015
 * 		Creacion del servicio de usuario.
 * # usuario
 * Service in the expediente.
 */


angular.module('expediente').service('usuario', function ($localStorage, $http, $q) {
    // AngularJS will instantiate a singleton by calling "new" on self function

    var setPerfil       = {};
    self.usuario        = undefined;
    self.recuerdame     = false;
    self.perfil         = {};
    self.perfiles       = [];

    var actionURL = "perfilesPersona/";
    var perfil = baseURL+"usuarios/"

    self.setPerfil = function(usuario){
        return $http.post(perfil, usuario);
    };

    self.getPerfiles = function(id){
        if(id != undefined)
            return $http.get(perfil+"perfiles/"+id);
        else
            return $http.get(perfil+"perfiles/");
    };

    self.getUsuarios = function(){
        //return $http.get(perfil+1);
        return $http.get(perfil);
    };

    self.bajaUsuario = function(expediente){
        var exp = {
            IDPERSONAL: expediente.N_EXPEDIENTE_ANIO.replace("/","")+'',
            IDPERFIL: expediente.N_PERFIL+'',
            NF_USUARIO : expediente.NF_USUARIO+''
        };
        return $http.delete(perfil, {
            data: exp
        } );
    };

    self.actualizaPassword = function(usuario){
        return $http.post(perfil+'contrasena/', usuario);
    };


    // Llama al servidor para obtener todos los perfiles del usuario.
    self.obtenPerfiles = function(){
        // Promises para esperar las respuestas del servidor.
        var defered = $q.defer();
        var promise = defered.promise;


        $http.get(baseURL + actionURL + self.usuario.NP_EXPEDIENTE)
        .success(function(data){

            self.perfiles = data;
            _.each(self.perfiles, function( perfil){
                perfil.menuIcon = setIcons(perfil.clave);
            });

            /// COMPROBAR SI LLEGA UN SOLO O VARIOS PERFILES.

            if(angular.isArray(data)){


                defered.resolve({
                result: 'PERFILES CARGADOS CON EXITO.'
            });

            }else{
                defered.reject({
                result: 'USUARIO SIN PRIVILEGIOS'
            });
            }




        }).error(function(data){
            defered.reject({
                result: 'SERVICIO NO DISPONIBLE, CONTACTE AL ADMINISTRADOR.'
            });
        });


        return promise;

    };

    function setIcons( clave ){

        var iconsClave = [{
            "MEDICO":"fa-stethoscope",
            "MANESTESI": "fa-user-md",
            "HOSP": "fa-hospital-o",
            "URGENCIAS": "fa-ambulance",
            "MCIRUJANO": "fa-user-md",
            "ECIRCULAN":"fa-heartbeat"
        }];

        return (iconsClave[0][clave] != undefined) ? iconsClave[0][clave] : "fa-medkit";
    }


    self.printCarnet = function(idUsuario){
        var defered = $q.defer();
        var promise = defered.promise;

         $http.get(baseURL + 'carnet/' + idUsuario)
         .success(function(data){
            if(data.success){
                defered.resolve(data.cadena);
            }else{
                defered.reject(data.cadena);
            }

         }).error(function(err){
            defered.reject(err);

         });

         return promise;
    }


    self.recordado = function(){


        if($localStorage.recuerdame){
          if($localStorage.usuario !== undefined){
              self.usuario = $localStorage.usuario;
              self.perfil = $localStorage.perfil;
          }
        }



        return $localStorage.recuerdame;
    }


    // Retorna el usuario que esta en la sesion actual.
    self.informacion = function(){
    	return self.usuario;
    };


    self.recordar = function(valor){

        self.recuerdame = valor;
    }

    self.modificaPerfil = function(perfil){
        //if(self.recuerdame){
        $localStorage.perfil = perfil;
        //}
        self.perfil = perfil;
    }


    self.modificaUsuario = function(usuario){
        //if(self.recuerdame){
            $localStorage.usuario = usuario;
        //}
    	  self.usuario = usuario;
        $localStorage.recuerdame = self.recuerdame;
    };

    self.eliminaUsuario = function(){
    	self.usuario = undefined;
        $localStorage.usuario = undefined;
        $localStorage.perfil = undefined;
    }

    self.removeLoginInformation = function(){

      $localStorage.usuario = undefined;
      $localStorage.perfil = undefined;
      $localStorage.$reset();
    }


    return self;

  });
