'use strict';

/**
 * @ngdoc service
 * @name expediente.catalogos
 * @description
 * # catalogos
 * Service in the expediente.
 */
angular.module('expediente').service('catalogos', function ($http) {




  var catalogoURL = baseURL + 'catalogo/';

    this.guardaCatalogo = function(urlCat,data){
        return $http.post(catalogoURL+urlCat,data);
    };
     this.modificaCatalogo = function(urlCat,data){
        return $http.put(catalogoURL+urlCat,data);
    };
    this.eliminaCatalogo = function(urlCat){
        return $http.delete(catalogoURL+urlCat);
    };
    this.getCatalogos = function(urlCat){
      return $http.get(catalogoURL+urlCat);
      /*
        return $http({
            url : catalogoURL+urlCat,
            method : 'GET',
            data: '',
            dataType : 'json',
            headers:{
                'Content-Type': 'application/json',
            }
        });
*/
    };
    this.getCatCIE10 = function(){
      return $http.get(baseURL+'notamedica/cie10');

    };


     this.getUsuarios = function(urlUser){
      return $http.get(urlUser);
      /*
      return $http({
      url : urlUser,
      method : 'GET',
      data: '',
      dataType : 'json',
      headers:{
        'Content-Type': 'application/json',
      }
    });
*/
    };


    this.mListaServicios = this.getCatalogos(2).success(function(data){
         return data;
      });


    this.getRegistros = function(urlTabla){
      return $http.get(catalogoURL+urlTabla);
      /*
        return $http({
            url : catalogoURL+urlTabla,
            method : 'GET',
            data: '',
            dataType : 'json',
            headers:{
                'Content-Type': 'application/json',
            }
        });
*/
    };


    this.guardaRegistro = function(urlTabla,registro) {
        var dato = {};
        dato.cDescripcion = registro.cDescripcion;
        return $http.post(catalogoURL+urlTabla, registro);
    };


    this.eliminaRegistro = function(urlTabla,registro){
        return $http.delete(catalogoURL+urlTabla+"/"+registro.ID+"/1");
    };


    this.modificaRegistro = function(urlTabla,registro){

        var dato = {};
        dato.cDescripcion = registro.cDescripcion;
        console.log(dato);
        return $http.put(catalogoURL+urlTabla+"/"+registro.ID, registro);
    };


  });
