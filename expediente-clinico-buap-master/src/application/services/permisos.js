(function(){
'use strict';

/**
 * @ngdoc service
 * @name expediente.permisos
 * @description
 * # permisos
 * Service in the expediente.
 */
angular.module('expediente')
  .service('permisos', function () {

    var permisosMedicosCE = [
            {funcionalidad : 0, read : true, write : true},//Signos vitales
            {funcionalidad : 1, read : true, write : true},//Resumen del Interrogatorio
            {funcionalidad : 2, read : true, write : true},//Exploración Física
            {funcionalidad : 3, read : true, write : true},//Interpretación
            {funcionalidad : 4, read : true, write : true},//Diagnóstico
            {funcionalidad : 5, read : true, write : true},//Solicitud De estudios
            {funcionalidad : 6, read : true, write : true},//Pronostico
            {funcionalidad : 7, read : true, write : true},//Indicacion
            {funcionalidad : 8, read : true, write : true},//Firmar Receta
            {funcionalidad : 9, read : true, write : true},//Firmar Nota
            {funcionalidad : 10, read : false, write : false},//Procedimientos Realizados
            {funcionalidad : 11, read : false, write : false}//Destino del paciente
          ];
    var permisosResidentesCE = [
            {funcionalidad : 0, read : false, write : true},//Signos vitales
            {funcionalidad : 1, read : false, write : true},//Resumen del Interrogatorio
            {funcionalidad : 2, read : false, write : true},//Exploración Física
            {funcionalidad : 3, read : false, write : true},//Interpretación
            {funcionalidad : 4, read : false, write : true},//Diagnóstico
            {funcionalidad : 5, read : false, write : true},//Solicitud De estudios
            {funcionalidad : 6, read : false, write : true},//Pronostico
            {funcionalidad : 7, read : false, write : true},//Indicacion
            {funcionalidad : 8, read : false, write : true},//Firmar Receta
            {funcionalidad : 9, read : false, write : true},//Firmar Nota
            {funcionalidad : 10, read : false, write : false},//Procedimientos Realizados
            {funcionalidad : 11, read : false, write : false}//Destino del paciente
          ];

    var permisosEnfermeriaCE = [
            {funcionalidad : 0, read : true, write : true},//Signos vitales
            {funcionalidad : 1, read : false, write : false},//Resumen del Interrogatorio
            {funcionalidad : 2, read : false, write : false},//Exploración Física
            {funcionalidad : 3, read : false, write : false},//Interpretación
            {funcionalidad : 4, read : false, write : false},//Diagnóstico
            {funcionalidad : 5, read : false, write : false},//Solicitud De estudios
            {funcionalidad : 6, read : false, write : false},//Pronostico
            {funcionalidad : 7, read : false, write : false},//Indicacion
            {funcionalidad : 8, read : false, write : false},//Firmar Receta
            {funcionalidad : 9, read : false, write : false},//Firmar Nota
            {funcionalidad : 10, read : false, write : false},//Procedimientos Realizados
            {funcionalidad : 11, read : false, write : false}//Destino del paciente
          ];

    var noPermisos = [
            {funcionalidad : 0, read : false, write : false},
            {funcionalidad : 1, read : false, write : false},
            {funcionalidad : 2, read : false, write : false},
            {funcionalidad : 3, read : false, write : false},
            {funcionalidad : 4, read : false, write : false},
            {funcionalidad : 5, read : false, write : false},
            {funcionalidad : 6, read : false, write : false},
            {funcionalidad : 7, read : false, write : false},
            {funcionalidad : 8, read : false, write : false},
            {funcionalidad : 9, read : false, write : false},
            {funcionalidad : 10, read : false, write : false},
            {funcionalidad : 11, read : false, write : false}
          ];

    var permisosMedicosUr = [
            {funcionalidad : 0, read : true, write : true},//Signos vitales
            {funcionalidad : 1, read : true, write : true},//Resumen del Interrogatorio
            {funcionalidad : 2, read : true, write : true},//Exploración Física
            {funcionalidad : 3, read : true, write : true},//Interpretación
            {funcionalidad : 4, read : true, write : true},//Diagnóstico
            {funcionalidad : 5, read : true, write : true},//Solicitud De estudios
            {funcionalidad : 6, read : true, write : true},//Pronostico
            {funcionalidad : 7, read : true, write : true},//Indicacion
            {funcionalidad : 8, read : true, write : true},//Firmar Receta
            {funcionalidad : 9, read : true, write : true},//Firmar Nota
            {funcionalidad : 10, read : true, write : true},//Procedimientos Realizados
            {funcionalidad : 11, read : true, write : true}//Destino del paciente
          ];

    var permisosResidentesUr = [
            {funcionalidad : 0, read : true, write : true},//Signos vitales
            {funcionalidad : 1, read : true, write : true},//Resumen del Interrogatorio
            {funcionalidad : 2, read : true, write : true},//Exploración Física
            {funcionalidad : 3, read : true, write : true},//Interpretación
            {funcionalidad : 4, read : true, write : true},//Diagnóstico
            {funcionalidad : 5, read : true, write : true},//Solicitud De estudios
            {funcionalidad : 6, read : true, write : true},//Pronostico
            {funcionalidad : 7, read : true, write : true},//Indicacion
            {funcionalidad : 8, read : false, write : false},//Firmar Receta
            {funcionalidad : 9, read : false, write : false},//Firmar Nota
            {funcionalidad : 10, read : true, write : true},//Procedimientos Realizados
            {funcionalidad : 11, read : true, write : true}
          ];

    var permisosEnfermeriaUr = [
            {funcionalidad : 0, read : false, write : false},//Signos vitales
            {funcionalidad : 1, read : false, write : false},//Resumen del Interrogatorio
            {funcionalidad : 2, read : true, write : true},//Exploración Física
            {funcionalidad : 3, read : false, write : false},//Interpretación
            {funcionalidad : 4, read : false, write : false},//Diagnóstico
            {funcionalidad : 5, read : false, write : false},//Solicitud De estudios
            {funcionalidad : 6, read : false, write : false},//Pronostico
            {funcionalidad : 7, read : false, write : false},//Indicacion
            {funcionalidad : 8, read : false, write : false},//Firmar Receta
            {funcionalidad : 9, read : false, write : false},//Firmar Nota
            {funcionalidad : 10, read : false, write : false},//Procedimientos Realizados
            {funcionalidad : 11, read : false, write : false}//Destino del paciente
          ];

  	this.getPermisosConsultaExterna = function(perfil){
      switch(perfil){
        case 'MEDICO': //PERFIL DE MÉDICO
          return permisosMedicosCE;
          break;
        case 'MGENERAL': //PERFIL DE MÉDICO
          return permisosMedicosCE;
          break;
        case 'RESI': //PERFIL DE RESIDENTE
          return permisosResidentesCE; 
          break;
        case 'ENFER': //PERFIL DE ENFERMERA
          return permisosEnfermeriaCE;
          break;
        case 'ECIRCULAN': //PERFIL DE ENFERMERA
          return permisosEnfermeriaCE;
          break;
        default : noPermisos;
          return 
      };
    };

    this.getPermisosUrgencias = function(perfil){
      switch(perfil){
        case 'URGENCIAS': //PERFIL URGENCIAS
          return permisosMedicosUr;
          break;
        case 'HOSP': //PERFIL HOSPITALIZACION
          return permisosMedicosUr;
          break;
        case 'RESI': //PERFIL DE RESIDENTE
          return permisosResidentesUr; 
          break;
        case 'ENFER': //PERFIL DE ENFERMERA
          return permisosEnfermeriaUr;
          break;
        case 'ECIRCULAN': //PERFIL DE ENFERMERA
          return permisosEnfermeriaUr;
          break;
        default : noPermisos;
          return 
      };
    };

  });
})();