var aux;
(function(){
'use strict';
angular.module('expediente')
  .service('cuestionarioService', ['peticiones', '$filter', function (peticiones, $filter) {

    var self = this;

    var mCuestionario = {
                          C_DESCRIPCION: "",
                          NP_CUESTIONARIO: 0,
                          NF_MEDICO : 1,
                          NF_PACIENTE : 1,
                          NF_TIPOS_CUESTIONARIOS: 0,
                          SECCION: []
                        };


  /**
   * [nPregunta Se utiliza para ir creando las preguntas independientes
   *  para agregarlas posteriormente al cuestionario que se esta creando]
   * @type {Object}
   */
    var nPregunta = {
                        C_PREGUNTA: "",
                        RESPUESTAS: [],
                        TIPO_PREGUNTA: "",
                        OBLIGATORIA: false
                    };
    self.listOpcionesDerivadas = [];

    var editandoPregunta = {
                                pregunta:{},
                                indice_seccion: undefined,
                                indice_pregunta: undefined,
                                HASHKEY: undefined,
                                NUEVA: true
                            } ;


    self.tiposDePregunta = {1: "PREGUNTA ABIERTA",
                                7: "PREGUNTAS DE TIPO NÚMERICO",
                                8: "PREGUNTAS DE TIPO FECHA",
                                2: "PREGUNTA DE OPCIÓN MULTIPLE (CHECK BOX)",
                                3: "PREGUNTA DE UNA OPCIÓN (RADIO BUTTON)",
                                4: "PREGUNTA ABIERTA CON VARIAS OPCIONES Y ETIQUETAS",
                                5: "PREGUNTA DE UNA OPCIÓN (RADIO BUTTON Y ETIQUETAS)",
                                6: "PREGUNTAS SI/NO QUE LANZAN A OTRA PREGUNTA",
                                9: "PREGUNTA DE TIPO CATALOGO (SELECT)"};


/**
 * Esta sección se va a utilizar unicamente para manejar el flujo
 * de  que acción se debe realizar
 */
    var editarCuestionario = undefined;
    var contestarCuestionario = undefined;
    var NP_ID_CUESTIONARIO = undefined;
    var NP_EXPEDIENTE_PACIENTE = 9080;


    self.getNpExpedientePaciente = function(){
        return NP_EXPEDIENTE_PACIENTE;
    };

    self.setNpExpedientePaciente = function(NP_EXPEDIENTE){
        NP_EXPEDIENTE_PACIENTE = NP_EXPEDIENTE;
    };

    self.getEditarCuestionario = function(){
      return editarCuestionario;
    };

    self.setEditarCuestionario = function(bandera){
      editarCuestionario = bandera;
    };


    self.getIdCuestionario = function(){
        return NP_ID_CUESTIONARIO;
    };

    self.setIdCuestionario = function(NP_ID){
        NP_ID_CUESTIONARIO = NP_ID;
    };

    self.getContestarCuestionario = function(){
      return contestarCuestionario;
    }

    self.setContestarCuestionario= function(bandera){
      contestarCuestionario = bandera;
      //console.log(contestarCuestionario);
    }

    self.datosCuestionario = function(nombre, especialidad){
        mCuestionario.C_DESCRIPCION = nombre;
        mCuestionario.NP_ESPECIALIDAD = especialidad;
    };

    self.setEditarPregunta = function(id_seccion,id_pregunta){
        self.resetEditandoPregunta();
        editandoPregunta.pregunta = mCuestionario.SECCION[id_seccion].PREGUNTAS[id_pregunta];
        editandoPregunta.NUEVA = false;
        editandoPregunta.indice_seccion = id_seccion;
        editandoPregunta.indice_pregunta = id_pregunta;
    }
    self.setNuevaPregunta = function(id_seccion){
        editandoPregunta.indice_seccion = id_seccion;
    }

    self.getEditarPregunta = function(){
        return angular.toJson(editandoPregunta);
    };

    self.getOpcionesDerivadas = function(id_seccion){
        var list = [];
        for (var i = 0;  i < self.listOpcionesDerivadas.length; i++) {

          if (self.listOpcionesDerivadas[i].id_seccion == id_seccion){
            list.push(self.listOpcionesDerivadas[i]);
          }
        };
        return list;
    };

    self.reset = function(cuestionario, bandera){
        cuestionario.C_DESCRIPCION   = "";
        cuestionario.NF_TIPOS_CUESTIONARIOS = undefined;
        cuestionario.SECCION.splice(0, mCuestionario.SECCION.length);
        cuestionario.NP_ESPECIALIDAD = undefined;
        cuestionario.NP_CUESTIONARIO = undefined;
        cuestionario.NF_MEDICO = undefined;
        cuestionario.NF_PACIENTE = undefined;
        cuestionario.ESTATUS = undefined;

        if(bandera == undefined){
            editarCuestionario = undefined;
            NP_ID_CUESTIONARIO = undefined;
            self.listOpcionesDerivadas.splice(0,self.listOpcionesDerivadas.length)
        }

    };

    self.resetEditandoPregunta = function(){
        editandoPregunta.indice_seccion = undefined;
        editandoPregunta.indice_pregunta = undefined;
        editandoPregunta.HASHKEY = undefined;
        editandoPregunta.NUEVA = true;
        editandoPregunta.pregunta = {};
    };



    /**
     * [addSeccion Función que se utiliza unicamente para agregar un sección vacia
     * posteriormente se va ir agregando la información]
     */

    self.addSeccion = function(){
        var aux = self.verificaSeccion();
        if(aux.result){mCuestionario.SECCION.push({C_DESCRIPCION:"",ESTATUS:true, NP_SECCION:0, PREGUNTAS:[]});}
        return aux;
    };


     self.cambiarRadio = function(key_seccion, key_pregunta, bandera, cCuestionario){

          console.log( key_seccion, key_pregunta, bandera, cCuestionario )
            var indice_seccion = getIndiceArray(cCuestionario.SECCION, key_seccion);
            var id_pregunta =    getIndiceArray(cCuestionario.SECCION[indice_seccion].PREGUNTAS, key_pregunta);
            var aux =  cCuestionario.SECCION[indice_seccion].PREGUNTAS[id_pregunta];
                for (var i = aux.RESPUESTAS.length - 1; i >= 0; i--) {

                    aux.RESPUESTAS[i].SELECTED = false;//9999999
                    if (bandera){
                        aux.RESPUESTAS[i].RESPUESTA = "";
                    }
                };
                aux.RESPUESTAS[aux.ID_RESPUESTA].SELECTED = true;

            if(aux.TIPO_PREGUNTA==6){
              permitirNodoHijo(aux.NP_PREGUNTA,aux.ID_RESPUESTA, aux.RESPUESTAS[aux.ID_RESPUESTA].ETIQUETA, cCuestionario);
            }
        };


    self.seleccionarRespuesta = function(key_seccion,key_pregunta,key_respuesta,mCuestionarioAuxiliar){
      //var mCuestionarioAuxiliar = (MEDICINA_GENERAL) ? self.mCuestionarioGeneral : self.mCuestionario;
      var id_seccion = getIndiceArray(mCuestionarioAuxiliar.SECCION,key_seccion);
      var id_pregunta = getIndiceArray(mCuestionarioAuxiliar.SECCION[id_seccion].PREGUNTAS, key_pregunta);
      var id_respuesta = (key_respuesta != -1) ? getIndiceArray(mCuestionarioAuxiliar.SECCION[id_seccion].PREGUNTAS[id_pregunta].RESPUESTAS, key_respuesta) : 0;

      mCuestionarioAuxiliar.SECCION[id_seccion].PREGUNTAS[id_pregunta].RESPUESTAS[id_respuesta].SELECTED = false;
      if(mCuestionarioAuxiliar.SECCION[id_seccion].PREGUNTAS[id_pregunta].RESPUESTAS[id_respuesta].RESPUESTA == undefined){
          return;
      }
      if(mCuestionarioAuxiliar.SECCION[id_seccion].PREGUNTAS[id_pregunta].RESPUESTAS[id_respuesta].RESPUESTA.trim().length > 0){
        mCuestionarioAuxiliar.SECCION[id_seccion].PREGUNTAS[id_pregunta].RESPUESTAS[id_respuesta].SELECTED = true;
      }
     };

    var permitirNodoHijo = function(NP_PREGUNTA, TIPO_RESPUESTA, etiqueta, mCuestionarioAuxiliar){
      //var mCuestionarioAuxiliar = (MEDICINA_GENERAL) ? self.mCuestionarioGeneral : self.mCuestionario;
      for (var i = mCuestionarioAuxiliar.SECCION.length - 1; i >= 0; i--) {
          for (var j =  mCuestionarioAuxiliar.SECCION[i].PREGUNTAS.length - 1; j >= 0; j--) {

            if(mCuestionarioAuxiliar.SECCION[i].PREGUNTAS[j].PADRE == NP_PREGUNTA){


                  if(etiqueta == "SI" & mCuestionarioAuxiliar.SECCION[i].PREGUNTAS[j].SN == 1){
                     mCuestionarioAuxiliar.SECCION[i].PREGUNTAS[j].NODO_RAIZ = true;
                  }else{
                    if(etiqueta == "NO" & mCuestionarioAuxiliar.SECCION[i].PREGUNTAS[j].SN == 0){
                       mCuestionarioAuxiliar.SECCION[i].PREGUNTAS[j].NODO_RAIZ = true;
                    }else{
                      mCuestionarioAuxiliar.SECCION[i].PREGUNTAS[j].NODO_RAIZ = false;
                    }
                   // mCuestionarioAuxiliar.SECCION[i].PREGUNTAS[j].NODO_RAIZ = (mCuestionarioAuxiliar.SECCION[i].PREGUNTAS[j].SN == TIPO_RESPUESTA) ? false : true;
                    }






                  }

              }
          };
      };



    self.verificaSeccion = function(){
        var bandera = true;
        var mErroSeccion = 0;
        var mErrorNombre = false;

        if (mCuestionario.SECCION.length > 0) {
            for (var i = mCuestionario.SECCION.length - 1; i >= 0; i--) {
                mErroSeccion = i;
                if(mCuestionario.SECCION[i].PREGUNTAS.length < 1){
                    bandera = false;
                    break;
                }
            };
        }
        return {result: bandera, error: mErroSeccion + 1, errorNombre: mErrorNombre};
    };

    self.numSeccion = function(){
      return mCuestionario.SECCION.length;
    };

    /**
     * [delSeccion Función que se utiliza para eliminar una sección,
     * tambien se va a eliminar toda la información que pertenezca a dicha sección]
     * @param  {[type]} indice [Indice de la sección a eliminar]
     * @return {[type]}        [No hay valor de retorno]
     */
    self.delSeccion = function(indice){
        (mCuestionario.SECCION[indice].NP_SECCION == 0) ? mCuestionario.SECCION.splice(indice,1) : mCuestionario.SECCION[indice].ESTATUS = false;
    };


    var insertChild = function(indice,padre,hijo){
      var a = _.findWhere(mCuestionario.SECCION[indice].PREGUNTAS, {ID: padre});
      if(a.hijos === undefined){
        a.hijos = [];
      }
      a.hijos.push(hijo);
    };

    /**
     * [addPregunta Función que se utiliza unicamente para agregar una pregunta
     * que ya se a creado (verificado) a un cuestionario.]
     * @param {[type]} indice    [Indice de la sección a la que se va a agregar]
     * @param {[type]} mPregunta [Pregunta a agregar]
     */
    self.addPregunta = function(indice,mPregunta, derivada){
        var id = generateID();
        if (mPregunta.mAuxiliar != undefined) {
            mPregunta.derivada = self.listOpcionesDerivadas[parseInt(mPregunta.mAuxiliar)];
        }
         mPregunta.ID = id;
        if(mPregunta.derivada !== undefined){//hija
          insertChild(indice,mPregunta.derivada.ID,id);
          ordenarDerivadas(indice, mPregunta);
        }else{
          mCuestionario.SECCION[indice].PREGUNTAS.push(angular.extend({},mPregunta));
        }
        if(mPregunta.TIPO_PREGUNTA == "6"){
            preguntasDerivadas(undefined,id);
            mCuestionario.SECCION[indice].PREGUNTAS[mCuestionario.SECCION[indice].PREGUNTAS.length-1].hijos = [];
        }
    };


    self.updatePregunta = function(pregunta){
        var listEliminar = [];
        if(editandoPregunta.pregunta.TIPO_PREGUNTA == "6" & pregunta.TIPO_PREGUNTA != "6"){
              buscarPreDependientes(editandoPregunta.indice_seccion, editandoPregunta.indice_pregunta,listEliminar);
              eliminarPreDependientes(listEliminar);
        }
        mCuestionario.SECCION[editandoPregunta.indice_seccion].PREGUNTAS[editandoPregunta.indice_pregunta] = angular.extend({},pregunta);
        //console.log(mCuestionario.SECCION[editandoPregunta.indice_seccion].PREGUNTAS[editandoPregunta.indice_pregunta]);
        if(pregunta.TIPO_PREGUNTA == "6"){
             preguntasDerivadas();
        }
        editandoPregunta.NUEVA = false;
        editandoPregunta.indice_seccion = undefined;
        editandoPregunta.indice_pregunta = undefined;
    };

    /**
     * [delPregunta Función que se utiliza para borrar una pregunta existente del
     *  cuestionario.]
     * @param  {[type]} indice_seccion  [Indice de la sección en la que se encuentra la pregunta]
     * @param  {[type]} indice_pregunta [Indice de la pregunta a eliminar]
     * @return {[type]}                 [description]
     */
    self.delPregunta = function(indice_seccion,indice_pregunta){
        var listEliminar = [];
        if(mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].TIPO_PREGUNTA == "6"){
            buscarPreDependientes(indice_seccion,indice_pregunta,listEliminar);
            eliminarPreDependientes(listEliminar);
        }
        if(mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].NP_PREGUNTA == 0){
           mCuestionario.SECCION[indice_seccion].PREGUNTAS.splice(indice_pregunta,1);
        }else{
          mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].MODIFICADA = true;
          mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].ESTATUS = false;
        }
    };


    var buscarPreDependientes = function(indice_seccion,indice_pregunta,listEliminar){
      var delListLanzadoras = [];
        for (var i = self.listOpcionesDerivadas.length - 1; i >= 0; i--) {
            if(self.listOpcionesDerivadas[i].TIPO_PREGUNTA == mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].C_PREGUNTA +  " / SI"){
                listPreguntasDependientes(i,listEliminar);
                delListLanzadoras.push(i);
            }
                if(self.listOpcionesDerivadas[i].TIPO_PREGUNTA == mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].C_PREGUNTA +  " / NO"){
                    listPreguntasDependientes(i,listEliminar);
                    delListLanzadoras.push(i);
                }
          };
          for (var i = delListLanzadoras.length - 1; i >= 0; i--) {
           self.listOpcionesDerivadas[i].estatus = false;
          };
    }

    var eliminarPreDependientes = function(listEliminar){
        for (var i = 0; i < listEliminar.length; i++) {
             if(mCuestionario.SECCION[listEliminar[i].ID_SECCION].PREGUNTAS[listEliminar[i].ID_PREGUNTA].NP_PREGUNTA == 0){
                mCuestionario.SECCION[listEliminar[i].ID_SECCION].PREGUNTAS.splice(listEliminar[i].ID_PREGUNTA,1);
             }else{
                mCuestionario.SECCION[listEliminar[i].ID_SECCION].PREGUNTAS[listEliminar[i].ID_PREGUNTA].MODIFICADA = true;
                mCuestionario.SECCION[listEliminar[i].ID_SECCION].PREGUNTAS[listEliminar[i].ID_PREGUNTA].ESTATUS = false;
             }
        };
    };

    var listPreguntasDependientes = function(indice,listEliminar){
        for (var i = mCuestionario.SECCION.length - 1; i >= 0; i--) {
            for (var j = mCuestionario.SECCION[i].PREGUNTAS.length - 1; j >= 0; j--) {
                if(mCuestionario.SECCION[i].PREGUNTAS[j].mAuxiliar == indice){
                  listEliminar.push({ID_SECCION: i, ID_PREGUNTA: j});
                }
            };
        };

    };

    /**
     * [delRespuesta Función que se utiliza para eliminar una respuesta de una pregutna que
     * apenas se esta  creando]
     * @param  {[type]} indice [Indice de la respuesta a eliminar]
     * @return {[type]}        [description]
     */
    var delRespuesta = function(indice){
         nPregunta.RESPUESTAS.splice(idice,1);
    };


    /**
     * [reordenarSubArray Función que se utiliza para reordenar los elementos de un array,
     * unicamente sirve para subir una posición con respecto de la posición en la que se
     * encuentra el elemento]
     * @param  {[type]} mArray [Array a ordenar]
     * @param  {[type]} indice [indice del elemento que se debe reordenar]
     * @return {[type]}        [description]
     */
    var reordenarSubArray = function(mArray, indice){
        var aux = mArray[indice - 1];
        var storage =  mArray[indice];
        var ban = false;
        if(indice > 0 ){
          if(mArray[indice].derivada===undefined){
            ban = true;
          }else{
            if(mArray[indice].derivada.ID === aux.ID){
              return false;
            }
          }
          mArray[indice - 1] = mArray[indice];
          mArray[indice] = aux;
          if(ban){
            for(var id in storage.hijos){
              reordenarSubArray(mArray,getIndexByIdKey(storage.hijos[id],mArray));
            }
          }
          return true;
        }
        return false;
    };

    /**
     * [reordenarBajArray Función quye se utiliza para re ordenar los elementos de un array
     * unicamente sirve para bajar un elemento  con respecto de su posición]
     * @param  {[type]} mArray     [Array a ordenar]
     * @param  {String} indice    [Indice del elemento a ordenar]
     * @return {[type]}            [description]
     */
    var reordenarBajArray = function(mArray, indice,ban){
        if(indice < mArray.length - 1){
          if(mArray[indice].derivada === undefined){
            if(mArray[indice].hijos !== undefined){
              var hijos = mArray[indice].hijos.reverse();
              for(var id in hijos){
                reordenarBajArray(mArray,getIndexByIdKey(mArray[indice].hijos[id],mArray),false);
              }
            }
          }else{
            if(ban || isHermano(mArray[indice + 1],mArray[indice])){
              return false;
            }
          }
          var aux = mArray[indice + 1]; //hijo
          //var storage = mArray[indice]; //padre
          mArray[indice + 1] = mArray[indice];
          mArray[indice] = aux;
          return true;
        }
        return false;
    };


    var reordenarSubSeccion = function(mArray, indice){
        var aux = '';
        if(indice > 0 ){
            aux = mArray[indice - 1];
            mArray[indice - 1] = mArray[indice];
            mArray[indice] = aux;
            return true;
        }
        return false;
    };

    var reordenarBajSeccion = function(mArray, indice){
        var aux = '';
        if(indice < mArray.length - 1){
            aux = mArray[indice + 1];
            mArray[indice + 1] = mArray[indice];
            mArray[indice] = aux;
            return true;
        }
        return false;
    };


    /**
     * [getCuestionario Función que se encarga unicamente de retornar el Array donde se tiene
     * almacenado el cuestionario]
     * @return {[type]} [description]
     */
    self.getCuestionario = function(){
        return mCuestionario;
    };

    var buscarIndexmAuxiliar = function(NP_PADRE,TIPO_DE_RESPUESTA){
        for (var i = self.listOpcionesDerivadas.length - 1; i >= 0; i--) {
            if(self.listOpcionesDerivadas[i].NP_PREGUNTA == NP_PADRE & self.listOpcionesDerivadas[i].id_respuesta == TIPO_DE_RESPUESTA){
              return i;
            }
        };

    };

    var convertirFormatosDerivadas = function(){
       for (var indice_seccion = mCuestionario.SECCION.length - 1; indice_seccion >= 0; indice_seccion--) {
            for (var indice_pregunta = mCuestionario.SECCION[indice_seccion].PREGUNTAS.length - 1; indice_pregunta >= 0; indice_pregunta--) {
              preguntaSelected(mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta],true);
              if(mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].PADRE != 0){
                  var index = buscarIndexmAuxiliar(mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].PADRE,
                                      mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].SN);
                  mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].mAuxiliar = index;
              }
            }
        }
    }

    self.setCuestionario = function(mData){
        //self.reset(mCuestionario);
        //var mData  = angular.fromJson(json);

        mCuestionario.C_DESCRIPCION          = mData.C_DESCRIPCION;
        mCuestionario.ESTATUS                = mData.ESTATUS;
        mCuestionario.NP_CUESTIONARIO        = mData.NP_CUESTIONARIO;
        mCuestionario.NP_ESPECIALIDAD        = mData.NP_ESPECIALIDAD;
        mCuestionario.NF_TIPOS_CUESTIONARIOS = mData.NF_TIPOS_CUESTIONARIOS;

        ordenarTiposPreguntas(mData.SECCION);
        mCuestionario.SECCION                = mData.SECCION;
        preguntasDerivadas(true,undefined);
        convertirFormatosDerivadas();

        //return angular.toJson(mCuestionario);

    };


    var ordenarTiposPreguntas = function(mSecciones){
        for (var i = mSecciones.length - 1; i >= 0; i--) {
          for (var j =  mSecciones[i].PREGUNTAS.length - 1; j >= 0; j--) {


            mSecciones[i].PREGUNTAS[j].ID = mSecciones[i].PREGUNTAS[j].NP_PREGUNTA;

            if(mSecciones[i].PREGUNTAS[j].PADRE == 0){
              mSecciones[i].PREGUNTAS[j].NODO_RAIZ = true;
            }else{
              mSecciones[i].PREGUNTAS[j].NODO_RAIZ = false; //99999999999
              var a = _.findWhere(mSecciones[i].PREGUNTAS, {NP_PREGUNTA: mSecciones[i].PREGUNTAS[j].PADRE});
              if(a.hijos === undefined){
                a.hijos = [];
              }
              a.hijos.unshift(mSecciones[i].PREGUNTAS[j].NP_PREGUNTA);
            }
          };
          mSecciones[i].PREGUNTAS = _.sortBy(mSecciones[i].PREGUNTAS, 'ORDEN');
        };
    };

    /**
     * [subirSeccion Función que se utiliza para reordenar (subir) una sección determinada]
     * @param  {[type]} id [el indice del elemento a reordenar]
     * @return {[type]}    [description]
     */
    self.subirSeccion = function(id){
      reordenarSubSeccion(mCuestionario.SECCION, id);

    };

    /**
     * [bajarSeccion Función que se utiliza para reordenar (bajar) una sección determinada]
     * @param  {[type]} id [indice del elemento a reordenar]
     * @return {[type]}    [description]
     */
    self.bajarSeccion = function(id){
      reordenarBajSeccion(mCuestionario.SECCION, id, true);

    };


    self.subirPregunta = function(id_seccion,id_pregunta){
      reordenarSubArray(mCuestionario.SECCION[id_seccion].PREGUNTAS, id_pregunta);
      mCuestionario.SECCION[id_seccion].REORDENAR = true;
    };

    self.bajarPregunta = function(id_seccion,id_pregunta){
      reordenarBajArray(mCuestionario.SECCION[id_seccion].PREGUNTAS, id_pregunta, true);
      mCuestionario.SECCION[id_seccion].REORDENAR = true;
    };


    var preguntasDerivadas = function(modificarCuestionario,id){
        self.listOpcionesDerivadas.splice(0, self.listOpcionesDerivadas.length);
        for (var indice_seccion = 0; indice_seccion < mCuestionario.SECCION.length;  indice_seccion++) {
            for (var indice_pregunta = 0; indice_pregunta < mCuestionario.SECCION[indice_seccion].PREGUNTAS.length; indice_pregunta++) {

                if(mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].TIPO_PREGUNTA == 6){
                    if(mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].RESPUESTAS[1].ETIQUETA == "SI"){ //INverti indices para que funcione
                        self.listOpcionesDerivadas.push(
                                          {
                                            id_seccion: indice_seccion,
                                            id_pregunta: indice_pregunta,
                                            id_respuesta: 1,
                                            NP_PREGUNTA: mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].NP_PREGUNTA,
                                            TIPO_PREGUNTA: mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].C_PREGUNTA +  " / SI",
                                            HASHKEY_PREGUNTA: 'Hola',
                                            ID: mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].ID,
                                            estatus: true
                                          });

                      }
                      if(mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].RESPUESTAS[0].ETIQUETA == "NO"){ //inverti indices para que funciione
                              self.listOpcionesDerivadas.push(
                                          {
                                            id_seccion: indice_seccion,
                                            id_pregunta: indice_pregunta,
                                            id_respuesta: 0,
                                            NP_PREGUNTA: mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].NP_PREGUNTA,
                                            TIPO_PREGUNTA: mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].C_PREGUNTA +  " / NO",
                                            HASHKEY_PREGUNTA: 'Hola',
                                            ID: mCuestionario.SECCION[indice_seccion].PREGUNTAS[indice_pregunta].ID,
                                            estatus: true
                                          });
                      }

                 }
            };
        };


    };

    var generateID = function(){
        var d = new Date().getTime();
        var r = (d + Math.random()*10000)%10000 | 0;
        return r;
    }

        self.crearCuestionarioFinal = function(mData){

            var indicesEliminar = []
            for (var i = 0;  i < mData.SECCION.length;  i++) {
              mData.SECCION[i].MODIFICADA = true;
                for (var j = 0; j < mData.SECCION[i].PREGUNTAS.length; j++) {
                    if(mData.SECCION[i].PREGUNTAS[j].mAuxiliar != undefined){
                        if(agregarSubPregunta( mData,
                                              mData.SECCION[i].PREGUNTAS[j],
                                              self.listOpcionesDerivadas[mData.SECCION[i].PREGUNTAS[j].mAuxiliar].TIPO_PREGUNTA.substring(0,
                                              self.listOpcionesDerivadas[mData.SECCION[i].PREGUNTAS[j].mAuxiliar].TIPO_PREGUNTA.length -  5),
                                              self.listOpcionesDerivadas[mData.SECCION[i].PREGUNTAS[j].mAuxiliar].id_respuesta) == true){
                            indicesEliminar.push({seccion: i, pregunta:j });
                       }
                    }
                };
            };

            for (var i = indicesEliminar.length  - 1; i >= 0; i--) {
                mData.SECCION[indicesEliminar[i].seccion].PREGUNTAS.splice(indicesEliminar[i].pregunta,1);
            };
        return mData;
    };


        var agregarSubPregunta = function(cCuestionario,mPregunta, cPregunta,id_respuesta){
            for (var i = cCuestionario.SECCION.length - 1; i >= 0; i--) {
                for (var j = cCuestionario.SECCION[i].PREGUNTAS.length - 1; j >= 0; j--) {
                    if(cCuestionario.SECCION[i].PREGUNTAS[j].C_PREGUNTA == cPregunta){
                          if(cCuestionario.SECCION[i].PREGUNTAS[j].RESPUESTAS[id_respuesta].PREGUNTA_DERIVADA == undefined){
                             cCuestionario.SECCION[i].PREGUNTAS[j].RESPUESTAS[id_respuesta].PREGUNTA_DERIVADA = [];
                          }
                          cCuestionario.SECCION[i].PREGUNTAS[j].RESPUESTAS[id_respuesta].PREGUNTA_DERIVADA.push(mPregunta);
                          return true;
                    };
                };
            };
            return false;
        };

        var getIndexByIdKey = function(id,array){
          for(var i=0 ; i<array.length ; i++){
            if(id === array[i].ID || id===array[i].$$hashKey){
              return i;
            }
          }
          return -1;
        };

        var isHermano = function(hijo,padre){
          for(var i in padre.hijos){
            if(hijo.ID === padre.hijos[i].ID){
              return true;
            }
          }
          return false;
        };


        self.revisarRespuestas = function(cuestionarioFin){
          var result = {id_seccion: undefined, id_pregunta: undefined, correcta : true, cuestionario: undefined};
            for (var i = cuestionarioFin.SECCION.length - 1; i >= 0; i--) {
                for (var j = cuestionarioFin.SECCION[i].PREGUNTAS.length - 1; j >= 0; j--) {
                    if(cuestionarioFin.SECCION[i].PREGUNTAS[j].TIPO_PREGUNTA == 7){
                      cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[0].RESPUESTA = String(cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[0].RESPUESTA_N);

                      cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[0].SELECTED = (cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[0].RESPUESTA.trim().length > 0) ? true : false;
                    }

                    if(cuestionarioFin.SECCION[i].PREGUNTAS[j].TIPO_PREGUNTA == 8){
                         cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[0].RESPUESTA =  "";
                        if(cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[0].RESPUESTA_F){
                             cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[0].SELECTED = true;
                             cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[0].RESPUESTA = cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[0].RESPUESTA_F;
                        }
                    }
                    if(cuestionarioFin.SECCION[i].PREGUNTAS[j].TIPO_PREGUNTA == 5){
                      for (var y = cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS.length - 1; y >= 0; y--) {
                          if(cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[y].SELECTED == true ){
                              cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTAS[y].RESPUESTA = cuestionarioFin.SECCION[i].PREGUNTAS[j].RESPUESTA;
                          }
                      };
                    }

                    if(cuestionarioFin.SECCION[i].PREGUNTAS[j].OBLIGATORIA){

                        if(estatusRespuestas(cuestionarioFin.SECCION[i].PREGUNTAS[j]) == false){
                          result.id_seccion = i;
                          result.id_pregunta = j;
                          result.correcta = false;
                        };
                    };

                };
            };
            result.cuestionario = angular.toJson(cuestionarioFin);
            return result;
        };

        var preguntaSelected = function(pregunta, contestado){
            var aux = true;

            for (var i = pregunta.RESPUESTAS.length - 1; i >= 0; i--) {
                if(contestado){
                    if(pregunta.RESPUESTAS[i].SELECTED  & (pregunta.TIPO_PREGUNTA == 3 |
                                                          pregunta.TIPO_PREGUNTA == 6 |
                                                          pregunta.TIPO_PREGUNTA == 5 |
                                                          pregunta.TIPO_PREGUNTA == 7 |
                                                          pregunta.TIPO_PREGUNTA == 8 |
                                                          pregunta.TIPO_PREGUNTA == 9)){
                            pregunta.ID_RESPUESTA = i;
                            if(pregunta.TIPO_PREGUNTA == 6){
                              //permitirNodoHijo(aux.NP_PREGUNTA,aux.ID_RESPUESTA, aux.RESPUESTAS[aux.ID_RESPUESTA].ETIQUETA, cCuestionario);
                                var numId = (pregunta.ID_RESPUESTA == 0) ? 1 : 0;
                                self.activarHijosContestados(pregunta.NP_PREGUNTA,numId);
                            }
                            if(pregunta.TIPO_PREGUNTA == 5){

                              pregunta.RESPUESTA = pregunta.RESPUESTAS[i].RESPUESTA;
                            }
                            if(pregunta.TIPO_PREGUNTA == 7){

                                pregunta.RESPUESTAS[i].RESPUESTA_N = (!isNaN(pregunta.RESPUESTAS[i].RESPUESTA)) ? parseInt(pregunta.RESPUESTAS[i].RESPUESTA) : undefined;
                            }
                            if(pregunta.TIPO_PREGUNTA == 8){
                              pregunta.RESPUESTAS[i].RESPUESTA_F = (pregunta.RESPUESTAS[i].RESPUESTA != "") ? new Date(pregunta.RESPUESTAS[i].RESPUESTA) : undefined;
                            }
                        }
                }else{
                  if (pregunta.TIPO_PREGUNTA == 4) {
                      if(pregunta.RESPUESTAS[i].RESPUESTA.trim().length < 1){
                        aux = false;
                        break;
                      }
                  }else{
                        aux = false;
                        if(pregunta.RESPUESTAS[i].SELECTED){
                            return true;
                        }
                  }
                }
            }

            return (aux) ? true : false;
      }


      self.activarHijosContestados = function(NP_PREGUNTA, TIPO_RESPUESTA){
              for (var i = mCuestionario.SECCION.length - 1; i >= 0; i--) {
                  for (var j =  mCuestionario.SECCION[i].PREGUNTAS.length - 1; j >= 0; j--) {
                    //console.log(contestarCuestionario);
                      if (contestarCuestionario){
                      //  console.log(mCuestionario.SECCION[i].PREGUNTAS[j].SN + " == " + TIPO_RESPUESTA);
                           if(mCuestionario.SECCION[i].PREGUNTAS[j].PADRE == NP_PREGUNTA){

                            mCuestionario.SECCION[i].PREGUNTAS[j].NODO_RAIZ = false;
                            mCuestionario.SECCION[i].PREGUNTAS[j].NODO_RAIZ = (mCuestionario.SECCION[i].PREGUNTAS[j].SN == TIPO_RESPUESTA) ? true : false;
                          }
                      }else{
                            mCuestionario.SECCION[i].PREGUNTAS[j].NODO_RAIZ = true;
                      }
                  };
              };
          };

        var estatusRespuestas= function(objPregunta){

             switch(objPregunta.TIPO_PREGUNTA) {
                    case 1:
                        if(objPregunta.RESPUESTAS[0].RESPUESTA == undefined){
                          return false;
                        }
                        if(objPregunta.RESPUESTAS[0].RESPUESTA.trim().length > 0) {
                          objPregunta.RESPUESTAS[0].SELECTED = true;
                          return true;
                        }else{
                          return false;
                        }

                        break;
                    case 2:
                        return preguntaSelected(objPregunta);
                        break;
                    case 3:
                        return preguntaSelected(objPregunta);
                        break;
                    case 4:

                        return preguntaSelected(objPregunta);
                        break;
                    case 5:

                        if(objPregunta.RESPUESTA != undefined){
                            return (preguntaSelected(objPregunta) & objPregunta.RESPUESTA.trim().length > 0) ? true : false;
                        }else{
                          return false;
                        }
                        break;
                    case 6:
                        return preguntaSelected(objPregunta);
                        break;
                    case 7:

                        return (objPregunta.RESPUESTAS[0].RESPUESTA_N != undefined) ? true : false;
                        break;
                    case 8:
                        return (objPregunta.RESPUESTAS[0].RESPUESTA_F) ? true : false;
                        break;
                    case 9:
                        return preguntaSelected(objPregunta);
                        break;
              }
        };




        var ordenarDerivadas = function(indice,mPregunta){
            var pos="";
            var aux=[];
            for (var j =0; j<= mCuestionario.SECCION[indice].PREGUNTAS.length - 1; j++) {
                if(mCuestionario.SECCION[indice].PREGUNTAS[j].ID ==  mPregunta.derivada.ID){
                   pos=j;
                };

                if(pos!==""){
                  if(mCuestionario.SECCION[indice].PREGUNTAS[j].derivada!=undefined){
                     if(mCuestionario.SECCION[indice].PREGUNTAS[j].derivada.ID ==  mPregunta.derivada.ID){
                       pos=j;
                      }
                      else{
                        break;
                      }
                  }
                }
            };
            aux=mCuestionario.SECCION[indice].PREGUNTAS.slice(pos+1,mCuestionario.SECCION[indice].PREGUNTAS.length);
            mCuestionario.SECCION[indice].PREGUNTAS.splice(pos+1,mCuestionario.SECCION[indice].PREGUNTAS.length-1);
            mCuestionario.SECCION[indice].PREGUNTAS.push(angular.extend({},mPregunta));

            for(var x in aux){
                mCuestionario.SECCION[indice].PREGUNTAS.push(angular.extend({},aux[x]));
            }
        };

        var getIndiceArray = function(mArray,key){
          for(var j = mArray.length -1; j >= 0; j--){
              if(mArray[j].$$hashKey == key){
                return j
              }
            }
            return null;
        };

  }]);
})();
