'use strict';

/**
 * @ngdoc service
 * @name expediente.pacientes
 * @description
 * # pacientes
 * Service in the expediente.
 */
angular.module('expediente').service('pacientes',["$http","$localStorage", "$q", function ($http,$localStorage, $q) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    //var baseURL = 'http://148.228.103.13:8080/';
    var registrarPaciente       = baseURL + 'paciente/';
    var consultarPacientesURL   = baseURL + 'paciente/';
    var catalogoURL             = baseURL + 'catalogo/';
    var registrarCita           = baseURL + 'agenda/';
    var getNoDates              = baseURL + 'agendamedica/'
    var registrarReferencia     = baseURL + 'referenciamedica/';
    var consultarPersonalURL    = baseURL + 'personal';
    var serviciosURL            = baseURL + 'catalogo/2/consulta';
    var pendientesURL           = baseURL + 'trabajosocial/medicinalegal/';
    var pendientesDoctorURL     = baseURL + 'trabajosocial/medicinalegal/doctor';
    var notaEgresoURL           = baseURL + 'notaegreso/';
    var reporteMPURL            = baseURL + 'trabajosocial/medicinalegal/';
    var medicinaLegalPaciente   = baseURL + 'trabajosocial/medicinalegal/paciente/'
    var consultarPacientesNotaMedica = baseURL + 'trabajosocial/';

    var perteneceAMedicinaLegalURL = baseURL + 'notamedica/medicinaLegal/';



    var backCita = undefined;


    var nombrePaciente = '';
    var idPaciente = '';
    var mNP_ID = "";
    this.idPac = "";

    var listadoPacientes = []
    var busquedaAnterior = ""




    this.setCurrentPaciente = function(NF_PACIENTE){
      $http.get(baseURL + '')
    };



    this.guardaPaciente = function(objPaciente){
        return $http.post(registrarPaciente, objPaciente);
    };

    this.setBackCita= function(cita){
        backCita = cita;
    }

    this.actualizaPaciente = function(paciente){
        return $http.put(registrarPaciente,paciente);
    };

    this.getCatalogo = function(id){

        return $http.get(catalogoURL+id);

    };

    this.getPaciente = function(){
        var _idPaciente;
        if($localStorage.paciente === undefined && idPaciente !== undefined){
            _idPaciente = idPaciente;
        }else
        if($localStorage.paciente.expediente === undefined ||
            $localStorage.paciente.expediente === null ||
            $localStorage.paciente.expediente === ''){
            _idPaciente = idPaciente;

        }else{
            _idPaciente = $localStorage.paciente.expediente;
        }



        return $http.get(consultarPacientesURL + _idPaciente);
    };


    this.getPacienteTrabajoSocial = function(){
        var _idPaciente;
        if($localStorage.paciente === undefined && idPaciente !== undefined){
            _idPaciente = idPaciente;
        }else
        if($localStorage.paciente.expediente === undefined ||
            $localStorage.paciente.expediente === null ||
            $localStorage.paciente.expediente === ''){
            _idPaciente = idPaciente;

        }else{
            _idPaciente = $localStorage.paciente.expediente;
        }
        return $http.get(baseURL+"trabajosocial/" + _idPaciente );
    };

    this.getPacienteNotaMedica = function(){
        var _idPaciente;
        if($localStorage.paciente === undefined && idPaciente !== undefined){
            _idPaciente = idPaciente;
        }else
        if($localStorage.paciente.expediente === undefined ||
            $localStorage.paciente.expediente === null ||
            $localStorage.paciente.expediente === ''){
            _idPaciente = idPaciente;

        }else{
            _idPaciente = $localStorage.paciente.expediente;
        }
        //console.log(_idPaciente);
        return $http.get(consultarPacientesNotaMedica + _idPaciente);
    };

    this.getPacientes = function(textoBuscado){
        return $http.get(consultarPacientesURL+textoBuscado);
    };

    this.getListadoPacientes = function(textoBuscado){
        var defered = $q.defer();
        if(textoBuscado.length == 4 && textoBuscado != busquedaAnterior){
          this.getPacientes(textoBuscado)
            .success(function(data){
                busquedaAnterior = textoBuscado
                listadoPacientes = cargarServicios(data.data)

                defered.resolve(listadoPacientes);
            })  
        }else{

            defered.resolve(filtrarDatosPac(textoBuscado));
            // if(textoBuscado.length == 4 && textoBuscado == busquedaAnterior){
            //     defered.resolve(listadoPacientes);
            // }else{
            //     if(textoBuscado.length > 4){
            //         defered.resolve(listadoPacientes);
            //     }else{
            //        defered.resolve([]); 
            //     }   
            // }
            
        }
        
         return defered.promise;
    }

    // this.buscarPacientesLocal = function(textoBuscado){
    //     return = _.filter(cargarServicios(result),function(e){
    //                                 var med = $scope.cita.idMedico+'';
    //                                 var pac=e.id;
    //                                 pac=pac.split("/")[0];
    //                                 return pac+''!==med+'';
    //                             });
    // }
    // 
    
    function filtrarDatosPac(textoBuscado){
         return _.filter(listadoPacientes, function(item){
            return item.value.indexOf(textoBuscado.toUpperCase())  != -1
         })
    }

    function cargarServicios(data){
        if(data!==undefined){
            return data.map(function (serv){
              return {
                value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE+" - "+(serv.NP_ID)+" " + (serv.C_CURP ? " - "+serv.C_CURP : '')).toUpperCase(),
                display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE+" - "+(serv.NP_ID)+" " + (serv.C_CURP ? " - "+serv.C_CURP : '')).toUpperCase(),
                nombre: serv.C_NOMBRE,
                id  : serv.NP_EXPEDIENTE,
                expSIU : serv.NP_EXPEDIENTE_SIU
              };
            });
        }
    };

    this.cargaPaciente = function(paciente){

    };

    this.cargaPacienteMinimo = function(paciente){
        nombrePaciente= paciente.NOMBRE;
        mNP_ID = paciente.NP_ID;
    };

    this.cargaNombrePaciente = function(nombre){
        nombrePaciente = nombre;
    };

    this.cargaIdPaciente = function(id){
        idPaciente = id;
    };

    this.obtenerNombrePaciente = function(){
        return nombrePaciente;
    };

    this.obtenerIdPaciente = function(){
        return idPaciente;
    };
    this.obtenerNpIdPaciente = function(){
        return mNP_ID;
    };

    this.guardaCita = function(cita){
        if(backCita !== undefined){
            cita.NF_ORIGEN = backCita.NP_ID;
            cita.NF_ESPECIALIDAD = backCita.NF_ESPECIALIDAD;
            backCita = undefined;
        }
        return $http.post(registrarCita,cita);
    };


    this.calculaEdad = function(fechaNacimiento){

        /*
        var values    = fechaNacimiento.split("/");
        var dia       = values[0];
        var mes       = values[1];
        var ano       = values[2];
        var fecha_hoy = new Date();
        var ahora_ano = fecha_hoy.getYear();
        var ahora_mes = fecha_hoy.getMonth()+1;
        var ahora_dia = fecha_hoy.getDate()-1+1;
        var edad      = (ahora_ano + 1900) - ano;

        if(ahora_mes < mes){
          edad--;
        }
        else if(ahora_mes==mes && ahora_dia<dia){
          edad--;
        }
        */
        if(fechaNacimiento == undefined){
            return ''
        }
        var edad;
        var ahora=moment();
        var n;

        if(moment.isDate(fechaNacimiento)){
            edad = moment(fechaNacimiento);

        }else if(moment.isMoment()){
            edad = fechaNacimiento;
        }else{
            edad = moment(fechaNacimiento,"DD/MM/YYYY");
        }

        /*var dateTemp = moment(edad).format("YYYY-MM-DD");
        dateTemp = dateTemp.split("-");
        var aa = moment().format("YYYY");
        if (moment(aa+"-"+dateTemp[1]+"-"+dateTemp[0]).isBefore(moment().format("YYYY-MM-DD"))) {
            dateTemp[2] = parseInt(dateTemp[2])+1;
            edad = moment(dateTemp[2]+"-"+dateTemp[1]+"-"+dateTemp[0]);             
        }*/

        n=ahora.diff(edad,'months'),'months';
        if (n<12){            
            n +=  (n == 1) ? " MES": " MESES";
        }else{
            n = parseInt(n/12) +" AÑOS";
        }
        return n;
    }


    this.cancelaCita = function(cita){
        return $http.delete(baseURL + 'agenda', { data: {
            'id_cita': '' + [cita.NP_ID],
                'id_motivo': 0
            }});

    }

    this.guardaReferencia = function(referencia){
        return $http.post(registrarReferencia, referencia);
    };

    this.getPersonal = function(){
        return $http.get(consultarPersonalURL);


    };

    this.getNoCitas = function(citaEspecial){
        return $http.get(getNoDates+"contador/"+citaEspecial.NF_ESPECIALIDAD+"/"+citaEspecial.NF_PERSONAL+"/"+citaEspecial.FECHA);

    };

    this.eliminarPaciente = function(paciente){
        return $http.get(registrarPaciente+paciente+"/true");
        /*
        return $http({
            url : registrarPaciente+paciente+"/true",
            method : 'DELETE',
            data: '',
            headers:{
                'Content-Type': 'application/json',
            }
        });
*/
    };

    this.getServicios = function(){

        return $htt.get(serviciosURL);
        /*
        return $http({
            url : serviciosURL,
            method : 'GET',
            data: '',
            dataType : 'json',
            headers:{
                'Content-Type': 'application/json',
            }
        });
*/
    };

    this.getPacientesUrgencias = function(tipo,idMedico){
        return $http.get(baseURL + 'triage/' + tipo + '/' + idMedico);
        //return $http.get('http://148.228.103.13:8080/triage/' + tipo + '/' + idMedico);
    };

    this.getPacientesVerdes = function(){
        //return $http.get('http://148.228.103.13:8080/triage');
        return $http.get(baseURL + 'triage/');
    };

    this.getPendientes = function(){

        return $http.get(pendientesURL);
    };

    this.getPendientesDetalle = function(data){
        return $http.get(pendientesURL+data);
    };

    this.getPacienteMinisterioPublico = function(){
        return $http.get(baseURL+"trabajosocial/medicinalegal/" + $localStorage.paciente.idexpediente); //notaEgresoURL + _idPaciente
    };


    this.getInformacionPacienteMedicinaLegal = function(){

      return $http.get(medicinaLegalPaciente + '/' + $localStorage.paciente.expediente);
    }

     this.getReporteMP = function(){


        var _idPaciente = '';

        try{
            _idPaciente = $localStorage.paciente.idPaciente;
        }catch(Exception){}

        return $http.get(reporteMPURL + _idPaciente);

    };

    this.guardaPendientesDoctor = function(datos){
        return $http.post(pendientesDoctorURL, datos);
    };


    this.perteneceAMedicinaLegal = function(){

      var defered = $q.defer();
      var promise = defered.promise;

      var _idPaciente = '';

      try{

          _idPaciente = $localStorage.paciente.expediente;
      }catch(Exception){
        defered.reject('SUCEDIO UN ERROR MIENTRAS SE RECUPERA EL ID DEL PACIENTE, CONTACTE AL ADMINISTRADOR.');
      }

      $http.get(perteneceAMedicinaLegalURL + _idPaciente)
      .success(function(data){
        if(data.Mensaje.indexOf('CON') > -1){
            defered.resolve(': '+data.Mensaje);
        }
        else{
            defered.reject();
        }
        //defered.resolve(data.Mensaje.indexOf('CON') > -1 ? ': ' + data.Mensaje : '');

      })
      .error(function(err){
        defered.reject('ALGO SALIO MAL');
      });

      return promise;
    }

  }]);
