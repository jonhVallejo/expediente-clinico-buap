'use strict';

/**
 * @ngdoc service
 * @desc expediente.menu
 * @description
 * # menu
 * Service in the expediente.
 */
angular.module('expediente').service('menu', [
    '$location',
    function ($location, $state) {

        var self;


        var listaIcons = [{
          cancelarReceta: "fa-ban"
          ,consultaCancelSAD: "fa-ban"
          ,"/agenda/consultaPaciente/servAuxiliares": "fa-flask"
          ,"/agenda/consultaPaciente/expedienteClinico": "fa-folder-open"
          ,"/consulta/agendaMedica/historiaClinica":"fa-history"
          ,"/agenda/consultaPaciente/consultaReferencia" : "fa-sticky-note"
          ,"/agenda/consultaPaciente/reaccionesAdversasMedicamentos": "fa-tint"
          ,"/agenda/consultaPaciente/quirofano" : "fa-user-circle-o"
          ,"/quirofano/inicioQuirofano": "fa-users"
          ,"/agenda/consultaPaciente/asignacionCama": "fa-check-square"
          ,"/agenda/consultaPaciente/incapacidadTemporal": "fa-id-card-o"
          ,"/hospitalizacion/consultaPaciente": "fa-h-square"
          ,"/interconsultas": "fa-stethoscope"
          ,"/agenda/consultaPaciente/ministerioPublico": "fa-legal"
          ,"/hospitalizacion/cargarCuentaPaciente": "fa-child"
          ,"/paciente/requisicion" : "fa-cubes"
          ,"/agenda/consultaPaciente/consultaContrarreferencia": "fa-bookmark-o"
          ,"/agenda/consultaPaciente/agendaActualizaPaciente":"fa-pencil-square-o"
          ,"agendaCalendarizarCitaEspecial": "fa-calendar-plus-o"
          ,"/agenda/consultaPaciente/agendaVerPaciente":"fa-calendar"
          ,"/agenda/consultaMedico/consulta": "fa-user-md"
          ,"ADMINISTRACION DE CITAS": "fa-address-book-o"
          ,"IMPRIMIR AGENDA": "fa-print"
          ,"/caja/buscarServicio": "fa-window-close-o"
          ,"/agenda/consultaPaciente/ventaDeServicios":"fa-money"
          ,"/agenda/consultaPaciente/detalleConveniosServicios": "fa-universal-access"
          ,"corteDeCaja": "fa-lock"
          ,"listaServicios": "fa-usd"
          ,"buscaPreCitas": "fa-calendar-o"
          ,"reimprimirReciboPago": "fa-files-o"
          ,"/caja/registraPacienteCaja": "fa-id-card-o"
          ,"/agenda/consultaPaciente/crearCuentaPaciente": "fa-id-badge"
          ,"/agenda/consultaPaciente/detalleCuentaPaciente": "fa-list-alt"
          ,"consultarAgenda": "fa-user-md"
          ,"agendaConsultarCitas": "fa-user-o"
          ,"agendaReporteMedicos": "fa-users"
          ,"agendaReporteMedico": "fa-male"
          ,"/agenda/consultaPaciente/hojaDeEnfermeria": "fa-file-text-o"
          ,"/consulta/agendaMedica/notaMedica": "fa-clipboard"
          ,"/consulta/cancelSAD": "fa-window-close-o"
          
        }]





        return self = {
          listaIcons: listaIcons,
			hashValues: [],
			generateHashValues: function (sections) {
				_.each(sections, function (section) {
					if (section.tipo == 'TOOGLE') {
						_.each(section.submenus, function (menu) {
							self.hashValues[menu.url] = section.desc + ': ' + menu.desc;
						});
					} else {
						self.hashValues[section.url] = section.desc;
					}
				});
			},

			sections: [],
			currentPage: undefined,

			setMenus: function (menu) {
				self.sections = menu;
				self.generateHashValues(self.sections);
				return self;
			},

			getTitle: function() {
				return self.hashValues[self.current.name] === undefined ?
					   self.hashValues[$location.path()]: self.hashValues[self.current.name];
			},

			toggleSelectSection: function (section) {
				self.openedSection = (self.openedSection === section ? null : section);
			},

			isSectionSelected: function (section) {
				return self.openedSection === section;
			},

			setPage: function(section) {
				self.currentPage = section;
			},

			isPageSelected: function(page) {
			  return self.currentPage === page;
			},

			selectPage: function (section, page) {
				page && page.url && $location.path(page.url);
				self.currentSection = section;
				self.currentPage = page;
			}
        };
    }])
	// Toma todos los espacios en blanco de una cadena
	.filter('nospace', function () {
		return function (value) {
			return (!value) ? '' : value.replace(/ /g, '');
		};
	})
	//replace uppercase to regular case
	.filter('humanizeDoc', function () {
		return function (doc) {
			if (!doc)
				return;
			if (doc.tipo === 'directive') {
				return doc.desc.replace(/([A-Z])/g, function ($1) {
					return '-' + $1.toLowerCase();
				});
			}
			return doc.label || doc.desc;
		};
	});
