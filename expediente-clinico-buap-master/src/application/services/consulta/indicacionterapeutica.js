(function(){
'use strict';

/**
* @ngdoc service
* @name expediente.consulta/indicacionTerapeutica
* @description
* # consulta/indicacionTerapeutica
* Service in the expediente.
*/
angular.module('expediente')
.service('indicacionTerapeutica', ['$http', '$q', 'usuario', 'medicos', 'pacientes' , 'mensajes',
  function ($http, $q, usuario, medicos, pacientes , mensajes) {
  // AngularJS will instantiate a singleton by calling "new" on this function

  // Obtener catalogo de medicamentos
  var self = this;

  self.C_TIPO_RECETA = 0;

  self.havePermissions = false;

  self.paciente = {};

  self.listaMedicamentos= {};

  self.init = function(){

    var defered = $q.defer();
    var promise = defered.promise;

    self.receta = {};
    self.medicamentos = [];

    // Obtener la información del medicos
    pacientes.getPaciente().success(function(data){


      self.paciente  = data.data[0];

      var edadPaciente = pacientes.calculaEdad(self.paciente.F_FECHA_DE_NACIMIENTO).split(" ")[1]

      self.esMenor =  (edadPaciente == "MESES") || (edadPaciente == "MES");

      //self.esMenor = pacientes.calculaEdad(self.paciente.F_FECHA_DE_NACIMIENTO) <= 1;
      //console.log(self.esMenor);

      medicos.getMedicoInformation(usuario.usuario.NP_EXPEDIENTE, 0)
      .success(function(data){
        self.medico = data;
        // Se valida que tenga permisos para recetar el medico.
        self.havePermissions = self.medico.N_RECETA == 1;

        $http.get(baseURL + 'medicamentos/' + usuario.usuario.NP_EXPEDIENTE + '/' + self.paciente.NP_EXPEDIENTE.split("/")[0] + '/' + self.C_TIPO_RECETA)
        .success(function(data){
          // agrupar los medicamentos por NP_MEDICAMENTO
           //console.log(data);
            self.medicamentos = _.map(_.groupBy(data, 'NP_MEDICAMENTO'), function(el){
                return {
                    NP_MEDICAMENTO: el[0].NP_MEDICAMENTO,
                    DOSIS: _.map(el, function(v){

                                 if(v.DOSIS.length === 0)
                                 v.DOSIS.push({
                                      NP_MEDICAMENTO_CLAVE: v.NP_CVEMED,
                                      DESCRIPCION: '-',
                                      DOSIS: 'DOSIS NO ESPECIFICADA',
                                      LACTEO: 'NO',
                                      NF_ESTATUS: '',
                                      ANTIBIOTICO: 'NO'
                                 });
                                 return {
                                     NP_MEDICAMENTO_CLAVE: v.DOSIS[0].NP_MEDICAMENTO_CLAVE,
                                     CATEGORIA_CLAVE: v.DOSIS[0].CATEGORIA_CLAVE,
                                     CATEGORIA_DESC: v.DOSIS[0].CATEGORIA_DESC,
                                     CBASICO_CLAVE: v.DOSIS[0].CBASICO_CLAVE,
                                     DESCRIPCION: v.DOSIS[0].DESCRIPCION,
                                     DOSIS: v.DOSIS[0].DOSIS,
                                     LACTEO: v.DOSIS[0].LACTEO,
                                     NF_ESTATUS: v.DOSIS[0].NF_ESTATUS,
                                     ANTIBIOTICO: v.DOSIS[0].ANTIBIOTICO

                                 };
                           }).filter(function(a) { return self.esMenor ? a.NP_MEDICAMENTO_CLAVE !== '0000'  : a.NP_MEDICAMENTO_CLAVE !== '0000' && a.LACTEO == "NO"; }) //return a.NP_MEDICAMENTO_CLAVE !== '0000' || (self.esMenor == false && a.LACTEO == 'NO') || self.esMenor ;})
                };

            }).filter(function(v){ return v.DOSIS.length > 0; });

            //console.log("Numero de medicamentos: "+self.medicamentos.length);
            console.log( self.medicamentos );
            defered.resolve();
          }).error(function(err){
            defered.reject();
          });
        }).error(function(err){
          //console.error(err);
        });
      });
      return promise;
    };

    self.isValid = function(medicamento,recetaACancelar){
      var temporal=$http.get(baseURL  + 'receta/medicamento/' + self.paciente.NP_EXPEDIENTE.replace('/', '') + '/' + medicamento.NP_MEDICAMENTO_CLAVE+"/"+recetaACancelar);

      //console.log(medicamento+" receta:"+recetaACancelar);
      //console.log(temporal);
      return temporal;
    };

    self.recuperaNota = function(idNota){
      if(idNota != -404){
      var deferred = $q.defer();
      var promise = deferred.promise;
      if (idNota != undefined) {
        $http.get(baseURL + "receta/" + idNota)
        .success(function(result){
          //console.log(result);// resultado de la recuperacion
          var nota = {};
          nota.C_TRATAMIENTO = result.C_TRATAMIENTO;
          nota.C_PLAN=result.C_PLAN;
          var MEDICAMENTOS_ACTUALES  =  _.map(result.MEDICAMENTOS, function(m){
            if(m.C_NOMBRE_MEDICAMENTO.indexOf('C') < 0){
              _med = self.medicamentos;
              var ff = _.filter(self.medicamentos, function(c){
                return c.NP_MEDICAMENTO == m.C_NOMBRE_MEDICAMENTO;
              });
            }
            var nomb_medicamento = m.C_NOMBRE_MEDICAMENTO.split("(");
            var selc_medicamento =   nomb_medicamento[1].split(")");
            selc_medicamento = trim(selc_medicamento[0]); //presentacion
            var medicamento = trim(nomb_medicamento[0]);
            var xx= JSON.parse( JSON.stringify(_.filter(self.medicamentos, function(el){
                  return el.NP_MEDICAMENTO === medicamento;

            })));

            xx.map(function(a,b){
              a.dias = m.N_DIAS;
              a.selected = _.filter(a.DOSIS, function(val){
                var sinEsp = trim(val.DESCRIPCION);
                return selc_medicamento == sinEsp;
              })[0];
              a.indicaciones = m.C_INDICACIONES;
              a.meses        = m.N_MESES;
              a.saec         = m.N_SAEC == 1 ? 'S' : 'N';
              //console.log(a,+" "+b);
              return a;
            })[0];
            //console.log(xx[0]);
            return xx[0];
          });
          //console.log(self.medicamentos);
          nota.medicamentos = MEDICAMENTOS_ACTUALES;
          deferred.resolve(nota);
        })
        .error(function(data) {
          mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
          deferred.reject();
        });
      }
      return promise;
      }
    }


    function ltrim(str, filter){
      filter || ( filter = '\\s|\\&nbsp;' );
      var pattern = new RegExp('^(' + filter + ')*', 'g');
      return str.replace(pattern, "");
    }

    function rtrim(str, filter){
      filter || ( filter = '\\s|\\&nbsp;' );
      var pattern = new RegExp('(' + filter + ')*$', 'g');
      return str.replace(pattern, "");
    }

    function trim(str, filter){
      filter || ( filter = '\\s|\\&nbsp;' );
      return ltrim(rtrim(str, filter), filter);
    }

    self.querySearch = function(query){
      if(query === null || query === undefined || query === ''){
        return [];
      }else
      {
        return _.filter(self.medicamentos, function(el){
          return el.NP_MEDICAMENTO.toUpperCase().indexOf(query.toUpperCase()) >= 0;
        });
      }
    }
    /*
    self.Actualizar = function(claveMed,status){
      _.each(self.medicamentos,function(medicamento){
        _.each(medicamento.DOSIS,function(presentacion){
          if(presentacion.NP_MEDICAMENTO_CLAVE==claveMed){presentacion.NF_ESTATUS=status;}
          //console.log(medicamento.NP_MEDICAMENTO+"."+presentacion.DESCRIPCION+"="+presentacion.NP_MEDICAMENTO_CLAVE);
        });
      });

    }

    self.retornaMedicamentos = function(){
     return self.medicamentos;
    }

*/



    self.guardaReceta = function(receta){
      //console.log(receta);
      //console.log("guarda receta");
      receta.NF_MEDICO = usuario.usuario.NP_EXPEDIENTE;

      return $http.post(baseURL + 'receta/',receta);
    };

    self.setFirmaElectronica = function(firma){
      self.firma = firma;
      // Se injecta el id  del usuario que esta logueado.
      // Esto se hace desde el servicio para no enviar a
      // la directiva información que no necesita.
      self.firma.FIRMA.id = usuario.usuario.NP_EXPEDIENTE;
    }

    self.getReporteReceta = function(receta){
      var defered = $q.defer();
      var promise = defered.promise;
      // Se le añade el atributo que modifica la receta
      // este puede ser NP_RECETA_CANCELAR o NP_RECETA
      var data = _.extend(self.firma, receta);
      // Validar que ya este la firma electornica
      if(angular.isDefined(self.firma)){
        $http({
          url : baseURL + 'receta/',
          method : 'PUT',
          data: data,
          headers:{
            'Content-Type': 'application/json; charset=utf-8'
          },
          responseType: 'arraybuffer'
        }).success(function(data){
          defered.resolve(data);
        }).error(function(data){


          if (!data.hasOwnProperty('error')) {
                   var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                   //console.log(decodedString);
                   var obj = JSON.parse(decodedString);
                   //mensajes.alerta('ERROR',obj.error,'ACEPTAR');
                 } 
          

          //console.log("validacion de llaves");
          defered.reject({
            type: 'AVISO',
            message: obj.error
            /*function(){
              //console.log(obj.error);
              return  data; //angular.isDefined(!data.hasOwnProperty('error')) ? "obj.error": 'ERROR DEL SERVIDOR'
            }*/
          });
        });
      }else{
        defered.reject({
          type: 'ERROR',
          message: 'NO EXISTE LA FIRMA ELECTRONICA, IMPOSIBLE FIRMAR'
        });
      }
      return promise;
    };

    self.setMedicamentos = function(medicamentos,plan,tratamiento){
         self.listaMedicamentos.medicamentos= {};
         self.listaMedicamentos.medicamentos=medicamentos;
         self.listaMedicamentos.plan=plan;
         self.listaMedicamentos.tratamiento=tratamiento;
         self.listaMedicamentos.band=true;
         //console.log(self.listaMedicamentos);
    }

    self.getMedicamentos = function(){
         return self.listaMedicamentos;
    }

    self.message = '';
    return self;
  }]);


var _med;
})();