(function(){
'use strict';
/**
 * @ngdoc service
 * @name expediente./quirofano/quirofano
 * @description
 * # /quirofano/quirofano
 * Service in the expediente.
 */
angular.module('expediente')
  .service('quirofano', ['$location','peticiones','mensajes','$state','$localStorage', function ($location,peticiones,mensajes,$state,$localStorage) {
  	var self=this;
  	//Variables para una Nota Prequirurgíca

 	  this.idNota=undefined;
    this.tipoNota = undefined;
	  this.notaActual=[];
    this.historicoQuirofano=[];

  	this.setNotaActual= function(nota){
  		this.notaActual=nota;
  	};
  	this.getNotaActual= function(){
  		return this.notaActual;
  	};
   	this.setIdNota=function(id){
  		this.idNota=id;
  	};
  	this.getIdNota=function(){
  		return this.idNota;
  	};

    this.setTipoNota=function(tipoNota){
      this.tipoNota=tipoNota;
    };
    this.getTipoNota=function(){
      return this.tipoNota;
    };

    this.getIdNotaMedica = function(){
      return this.notaActual.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA;
    }

    this.getStatusNotaPost = function(){
      return this.notaActual.NOTAPOSTQUIRURGICA.NF_STATUS;
    }
     this.getNpNotaPost = function(){
      return this.notaActual.NOTAPOSTQUIRURGICA.NP_ID_NOTA_POSTQUIRURGICA;
    }

    this.getNotaPosAnestesica = function(){
       return (this.notaActual.NOTAPOSTANESTESICA) ?  this.notaActual.NOTAPOSTANESTESICA.NP_ID_NOTA_POSTANESTESICA : 0;
    }

    this.getNotaPreanestesica = function(){
      return (this.notaActual.NOTAPREANESTESICA) ? this.notaActual.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA : 0;
    }

  }]);
})();
