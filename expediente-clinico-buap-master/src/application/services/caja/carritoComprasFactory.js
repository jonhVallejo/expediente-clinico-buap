var aux;
'use strict';
(function(){

angular.module('expediente').factory("carritoCompras", ['pacientes','$state','$localStorage',
            function(pacientes,$state,$localStorage){
var desc = {id:'', NP_ID: '', FOLIO: '', DESCUENTO: '', TIPO: '',descripcion:'',PORCENTAJE:''};
var servSeleccionados  = [];
var descuentosAll = [];
var mensaje = '';
var isCancelacion = false;
this.tipoDescuento = '$';

var data = {
              cajero:0,
              paciente:"",
              tipo_pago: "cobro_serv",
              folio_general: 0,
              nomPaciente:"",
              total:0,
              'servicios':[],
               formasDePago: [{}]
               
            }


        data.paciente = pacientes.obtenerNpIdPaciente();
        data.nomPaciente =  pacientes.obtenerNombrePaciente();


  function cambiar(){
    for(var au in data.servicios){
    }
  }



  function calcularCostosSeg( costos_producto ){

    // $localStorage.paciente.seguros = [
    //                  "axa",
    //                  "atlas",
    //                  "qualitas",
    //                  "gnp"
    //               ]

    //console.log( _.indexOf( $localStorage.paciente.seguros, "axa" ) )

    if( _.isUndefined($localStorage.paciente.seguros)){
      $localStorage.paciente.seguros = [];
    }

      var costo_normal_prod = costos_producto['D_PESO']
      var costo_restante = costo_normal_prod;
      var suma_seguros = 0;

      var listado_seguros = [];


      if(_.indexOf( $localStorage.paciente.seguros, "axa" ) != -1){
        if( costos_producto['N_COSTO_AXA'] >= 0 ){

          var m_aportacion = costo_normal_prod - costos_producto['N_COSTO_AXA'];
          
          suma_seguros = m_aportacion;

          listado_seguros.push( { 'aportacion': m_aportacion, 'seguro': 'axa'} )

          if ( costos_producto['N_COSTO_AXA'] == 0 ){
            return listado_seguros ;

          }else{
            
            costo_restante = costos_producto['N_COSTO_AXA']
          }  


        }else{
          console.error('No tiene un costo correcto el producto pero si le agregaron el seguro') 
        }
        
      }

      if(_.indexOf( $localStorage.paciente.seguros, "qualitas" ) != -1){

        if( costos_producto['N_COSTO_QUALITAS'] >= 0 ){

          var m_aportacion = costo_normal_prod - costos_producto['N_COSTO_QUALITAS'];

          console.log(  'qualitas 1' , m_aportacion )

          if( suma_seguros > 0 ){

            suma_seguros += m_aportacion;

            if( costo_restante <= m_aportacion){

              listado_seguros.push( { 'aportacion': costo_restante , 'seguro': 'qualitas'} )

              return listado_seguros ;
            }else{

              console.log(  'qualitas 1' , m_aportacion )
              listado_seguros.push( { 'aportacion': m_aportacion , 'seguro': 'qualitas'} )
              
              costo_restante -= m_aportacion;
            }

          }else{

            suma_seguros += m_aportacion;

            listado_seguros.push( { 'aportacion': m_aportacion, 'seguro': 'qualitas'} )

            if ( costos_producto['N_COSTO_QUALITAS'] == 0 ){
                return listado_seguros ;
              }else{
                costo_restante = costos_producto['N_COSTO_QUALITAS']
              }
          }

        }else{
          console.error('No tiene un costo correcto el producto pero si le agregaron el seguro') 
        }

        //return costos_producto['N_COSTO_QUALITAS'] 
      }




      if(_.indexOf( $localStorage.paciente.seguros, "gnp" ) != -1){

        if( costos_producto['N_COSTO_GNP'] >= 0 ){

          var m_aportacion = costo_normal_prod - costos_producto['N_COSTO_GNP'];

          console.log(  'gnp 2' , m_aportacion )

          if( suma_seguros > 0 ){

            suma_seguros += m_aportacion;


            if( costo_restante <= m_aportacion){

              listado_seguros.push( { 'aportacion': costo_restante , 'seguro': 'gnp'} )

              return listado_seguros ;
            }else{
              listado_seguros.push( { 'aportacion': m_aportacion , 'seguro': 'gnp'} )
              //suma_seguros += m_aportacion;
              costo_restante -= m_aportacion;
            }

          }else{

            suma_seguros += m_aportacion;


            listado_seguros.push( { 'aportacion': m_aportacion, 'seguro': 'gnp'} )

            if ( costos_producto['N_COSTO_GNP'] == 0 ){
                return listado_seguros ;
              }else{
                costo_restante = costos_producto['N_COSTO_GNP']
              }
          }

        }else{
          console.error('No tiene un costo correcto el producto pero si le agregaron el seguro') 
        }

        //return costos_producto['N_COSTO_QUALITAS'] 
      }




      // if(_.indexOf( $localStorage.paciente.seguros, "N_COSTO_GNP" )){
      //  return costos_producto['N_COSTO_QUALITAS'] 
      // }
      // 
      
      if(_.indexOf( $localStorage.paciente.seguros, "atlas" ) != -1){

        if( costos_producto['N_COSTO_ATLAS'] >= 0 ){

          var m_aportacion = costo_normal_prod - costos_producto['N_COSTO_ATLAS'];

          if( suma_seguros > 0 ){

            suma_seguros += m_aportacion;


            if( costo_restante <= m_aportacion){

              listado_seguros.push( { 'aportacion': costo_restante , 'seguro': 'atlas'} )

              return listado_seguros ;
            }else{
              listado_seguros.push( { 'aportacion': m_aportacion , 'seguro': 'atlas'} )
              //suma_seguros += m_aportacion;
              costo_restante -= m_aportacion;
            }

          }else{
            suma_seguros += m_aportacion;


            listado_seguros.push( { 'aportacion': m_aportacion, 'seguro': 'atlas'} )

            if ( costos_producto['N_COSTO_ATLAS'] == 0 ){
                return listado_seguros ;
              }else{
                costo_restante = costos_producto['N_COSTO_ATLAS']
              }
          }

        }else{
          console.error('No tiene un costo correcto el producto pero si le agregaron el seguro') 
        }

        //return costos_producto['N_COSTO_QUALITAS'] 
      }

      // if(_.indexOf( $localStorage.paciente.seguros, "N_COSTO_ATLAS" )){
      //  return costos_producto['N_COSTO_QUALITAS'] 
      // }





      return _.size( listado_seguros ) > 0 ? listado_seguros   : null;



}






  var isBeneficiario = function(){
    // console.debug($localStorage.paciente);
    // return $localStorage.paciente.cuentaPaciente.TIPO_PACIENTE_DERECHO_HABIENTE;
    return $localStorage.paciente.DERECHO_HABIENTE;
  }

  var calculaDescuentos = function(indice){
      var descTemporal = 0;
      for (var aux in  data.servicios[indice].DESCUENTOS) {
        descTemporal += data.servicios[indice].DESCUENTOS[aux].DESCUENTO;
      }
      data.total -= data.servicios[indice].D_PESO_TOTAL;
      data.servicios[indice].D_PESO_TOTAL -= descTemporal;
      data.total += data.servicios[indice].D_PESO_TOTAL;
      return descTemporal;
  };

  var updatePrecio = function(indice){


      // totalDescuentosProductos = (mData.D_PESO * mData.nPorcentajeCondonacion) / 100;
      //     mDescuentos.push({id: 1000, DESCRIPCION: data.paciente.substring(0, 1), DESCUENTO: totalDescuentosProductos, TIPO: 2, "DESC_EXISTENTE":true });

      aux = data.servicios[indice].DESCUENTOS;

      var sumDesc = 0;


     // data.servicios[indice].PRESTACION_DERECHO_HABIENTE = ((data.servicios[indice].D_PESO * data.servicios[indice].N_PORCENTAJE_CONDONACION)/100) * data.servicios[indice].CANTIDAD;


      for (var aux in  data.servicios[indice].DESCUENTOS) {
        if(data.servicios[indice].DESCUENTOS[aux].DESCRIPCION == "D"){
          data.servicios[indice].DESCUENTOS[aux].DESCUENTO = ((data.servicios[indice].D_PESO * data.servicios[indice].N_PORCENTAJE_CONDONACION)/100) * data.servicios[indice].CANTIDAD;
        }
        sumDesc += parseFloat(data.servicios[indice].DESCUENTOS[aux].DESCUENTO);
      }
      var sub = ((data.servicios[indice].D_PESO * data.servicios[indice].CANTIDAD) - sumDesc);
// console.debug("data.servicios[indice]",data.servicios[indice]);
      

      //_.where(listOfPlays, {author: "Shakespeare", year: 1611});




      (sub>0) ? data.servicios[indice].D_PESO_TOTAL = sub : data.servicios[indice].D_PESO_TOTAL=0;

      console.debug(data.servicios[indice]);
      updateTotal();
  };

  var updateTotal = function(){
      var tot = 0;
      for(var i=0 ; i<data.servicios.length ; i++){
        tot += data.servicios[i].D_PESO_TOTAL;
      }
      data.total = tot;
  };


  var eliminarDescuento = function(idx,key){
    var des;
    var total;
    for(var i=descuentosAll.length -1 ; i>=0 ; i--){
      des = descuentosAll[i];
      total = 0;     
      for(var j=des.desc.length-1 ; j>=0 ; j--){
        if(des.desc[j].$$hashKey == key){
          des.desc.splice(j, 1);
        }else{
          total += des.desc[j].D_PESO_TOTAL;
        }
      }
      if(des.desc.length===0){
        descuentosAll.splice(i, 1);
      }else{
        des.DESCUENTO = total;
      }
    }
  };

  var generateUUID = function(){
    var d = new Date().getTime();
    var r = (d + Math.random()*10000)%10000 | 0;
    return r;
  };


  var interfaz = {
    calcularCostosSeg: calcularCostosSeg,
    carrito: data,
    descuento: desc,
    tipo_pago: "", // ver sino le pega
    descuentosAll : descuentosAll,
    servSeleccionados : servSeleccionados,
    mensaje : mensaje,
    isCancelacion : isCancelacion,
    tipoDescuento : this.tipoDescuento,
    setIsCancelacion : function(bool){
      isCancelacion = bool;
    },
    getIndices: function(obj){
      var indices = [];
      for(var j=0 ; j<obj.length ; j++){
        for(var i=0 ; i<data.servicios.length ; i++){
          if(obj[j].$$hashKey === data.servicios[i].$$hashKey){
            indices.push(i);
            break;
          }
        }
      }
      return indices;
    },
    getIndices2: function(obj){
      var indices = [];
      for(var j=0 ; j<obj.length ; j++){
        for(var i=0 ; i<data.servicios.length ; i++){
          if(obj[j].$$hashKey === data.servicios[i].$$hashKey){
            indices.push({ id_selecionado:j, id_servicio: i});
            break;
          }
        }
      }
      return indices;
    },
    getIndiceByKey: function(key){
        for(var i=0 ; i<data.servicios.length ; i++){
          if(key === data.servicios[i].$$hashKey){
            return i;
          }
        }
    },
        getIndiceByKeyCobro: function(key){
        for(var i=0 ; i<data.formasDePago.length ; i++){

          if(key === data.formasDePago[i].$$hashKey){

            return i;

          }
        }
    },

    addServicio: function(mData,nuevo){
      //self.idEditCobros = (data.tipo_pago == "cobro_serv") ? true : false;
      



     var mDescuentos = [];
     var NP_PAGO_SERV = (mData.NP_PAGO_SERV == undefined) ? 0 : mData.NP_PAGO_SERV;


      if(mData.T_FECHA_CMD == undefined){
        mData.T_FECHA_CMD = moment().format('DD/MM/YYYY');
      }
       
      // console.debug("$state.current.name",$state.current.name);
       var totalDescuentosProductos = 0;

      

        if (isBeneficiario()){
          mDescuentos.splice(0,mDescuentos.length);
          totalDescuentosProductos = (mData.D_PESO * mData.nPorcentajeCondonacion) / 100;
          mDescuentos.splice(0,0,{id: 1000, DESCRIPCION: "D", DESCUENTO: totalDescuentosProductos, TIPO: 2, "DESC_EXISTENTE":true });
        }
         var precioTotal = (isBeneficiario()) ?  (mData.D_PESO - totalDescuentosProductos) : mData.D_PESO_TOTAL;

        data.servicios.splice(0,0,{"DESCUENTOS": mDescuentos ,"NP_ID": mData.NP_ID,"C_DESCRIPCIPON": mData.C_DESCRIPCIPON , "CANTIDAD": 1,
                              "D_PESO":  mData.D_PESO, "D_PESO_TOTAL": precioTotal, "cClave":mData.cClave,
                              "cVariable":mData.cVariable,"NUEVO":nuevo,"it":false,"PRESTACION_DERECHO_HABIENTE":totalDescuentosProductos,
                              "N_PORCENTAJE_CONDONACION":mData.nPorcentajeCondonacion,"NP_PAGO_SERV":NP_PAGO_SERV,
                              "T_FECHA_CMD":mData.T_FECHA_CMD,"ORDEN": mData.orden, "listadoSeg": mData.listadoSeg});

              if (isBeneficiario()){
                    calculaDescuentos(data.servicios.length - 1);
                     updatePrecio(data.servicios.length - 1);
                }
        updateTotal();

        // console.debug(data.servicios);
    },

    updateCantidad: function(indice){
      updatePrecio(indice);
    },

    addDescuento: function(mData,indice,id){
          var mFallidos = [];
          var desc = mData.DESCUENTO;
          var total = 0;

          for(var id in indice){
            var datValido = true;
            for (var i = 0; i < data.servicios[indice[id].id_servicio].DESCUENTOS.length; i++) {
              console.debug(mData.TIPO,data.servicios[indice[id].id_servicio].DESCUENTOS[i]);
              if (mData.TIPO == data.servicios[indice[id].id_servicio].DESCUENTOS[i].TIPO){
                datValido = false;
                break;
              }
            }
            if(this.tipoDescuento==='%'){
              mData.DESCUENTO = desc*(data.servicios[indice[id].id_servicio].CANTIDAD*data.servicios[indice[id].id_servicio].D_PESO)/100;
              total += mData.DESCUENTO;
            }
              if((parseFloat(data.servicios[indice[id].id_servicio].D_PESO_TOTAL) < parseFloat(mData.DESCUENTO)) || datValido === false){
                 mFallidos.push({id_servicio: indice[id].id_servicio, id_selecionado: indice[id].id_selecionado,tipoFallo: (datValido) ? 1 : 2});
              }else{
                if (mData.TIPO==2){
                  data.servicios[indice[id].id_servicio].DESCUENTOS.push({id:id, NP_ID: parseInt(mData.NP_ID), FOLIO: mData.FOLIO, DESCUENTO: parseFloat(mData.DESCUENTO), TIPO: parseInt(mData.TIPO),DESCRIPCION:mData.DESCRIPCION,"DESC_EXISTENTE":mData.DESC_EXISTENTE});
                }else{
                 data.servicios[indice[id].id_servicio].DESCUENTOS.push({id:id, NP_ID: parseInt(mData.NP_ID), FOLIO: mData.FOLIO, DESCUENTO: parseFloat(mData.DESCUENTO), TIPO: parseInt(mData.TIPO),DESCRIPCION:mData.descripcion,"DESC_EXISTENTE":mData.DESC_EXISTENTE});
                }
                if (data.servicios.length > 0){
                    calculaDescuentos(indice[id].id_servicio);
                }
                updatePrecio(indice[id].id_servicio);
              }
          }
          if(this.tipoDescuento==='%'){
            mData.DESCUENTO = total;
          }
          return mFallidos;
    },

    delDescuento: function(descObj){
      for(var i=0 ; i<descObj.desc.length ; i++){
        for(var j=0 ; j<data.servicios.length ; j++){
          //Busca el servicio al que se le va a eliminar el descuento
          if(descObj.desc[i].$$hashKey === data.servicios[j].$$hashKey){
            for(var h=0 ; h<data.servicios[j].DESCUENTOS.length ; h++){
              //Busca el descuento que se va a eliminar
              if(descObj.DESCUENTO === data.servicios[j].DESCUENTOS[h].DESCUENTO && descObj.descripcion === data.servicios[j].DESCUENTOS[h].DESCRIPCION && descObj.FOLIO === data.servicios[j].DESCUENTOS[h].FOLIO){
                data.servicios[j].DESCUENTOS.splice(h, 1);
                updatePrecio(j);
                break;
              }
            }
            break;
          }
        }
      }

      for(var i=0 ; i< descuentosAll.length ; i++){
        if(descuentosAll[i].id === descObj.id){
          descuentosAll.splice(i,1);
          break;
        }
      }
    },

    delServicio: function(obj){
      for(var j=0 ; j<obj.length ; j++){
        for(var i=0 ; i<data.servicios.length ; i++){
          if(obj[j].$$hashKey === data.servicios[i].$$hashKey){
            eliminarDescuento(i,obj[j].$$hashKey);
            data.servicios.splice(i, 1);
            break;
          }
        }
      }
      updateTotal();
    },

    reset: function(){
      data.cajero = 0;
      data.paciente = 0;
      data.nomPaciente = ""
      data.total = 0;
      data.efectivo = 0;
      data.tipo_pago = "";
      data.folio_general = 0;
      data.servicios = [];
      data.formasDePago = [{}];
      descuentosAll.splice(0,descuentosAll.length);
    }

    // cargarUser: function(paciente_id, nomPaciente){
    //   data.paciente = paciente_id;
    //   data.nomPaciente = nomPaciente;
    // }
  };


    return interfaz;
}]);


})();
