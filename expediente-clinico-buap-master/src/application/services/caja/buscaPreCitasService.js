(function(){

angular.module('expediente')
    .service('buscaPreCitasService',['$http','peticiones','carritoCompras',
      function ($http,peticiones,carritoCompras) {

        var buscaPreCitasUrl = baseURL + 'buscaPreCitas/';

        var folio              = "";
        var nomPaciente        = "";
        var paciente_id        = "";
        var descripcionService = "";
        var servicio_id        = "";
        var fechaService       = "";
        var importe            = "";
        var nomMedico          = "";
        var claveServicio      = "";
        var mListData          = [];


        this.activaCitaPost = function(mCita){
          return $http.post(buscaPreCitasUrl, mCita);
        };

        this.setDatosPreCita = function(mData){
            folio = mData.NP_ID_CITA;

            nomPaciente = mData.NOMBRE_COMPLETO;
            paciente_id = mData.CP_ID;
            descripcionService = mData.PRODUCTO_DESC;
            console.log("Descripcion del servicio : " + descripcionService);
            fechaService = mData.F_FECHA_HORA;
            importe = mData.D_COSTO;
            nomMedico = mData.nomMedico;
            claveServicio = mData.C_CLAVE;
            servicio_id = mData.NP_ID_PRODUCTO;

            carritoCompras.carrito.paciente = mData.CP_ID;
            carritoCompras.carrito.nomPaciente = mData.NOMBRE_COMPLETO;

        };

        this.getFolio = function(){
          return folio;
        }

        this.getServicio_id = function(){
          return servicio_id;
        }

        this.getClaveServicio = function(){
          return claveServicio;
        }

         this.getNomPaciente = function(){
          return nomPaciente;
        }

         this.getPaciente_id = function(){
          return paciente_id;
        }

         this.getDescripcionService = function(){
          return descripcionService;
        }

         this.getFechaService = function(){
          return fechaService;
        }

         this.getImporte = function(){
          return importe;
        }

        this.getNomMedico = function(){
          return nomMedico;
        }





  }]);

})();
