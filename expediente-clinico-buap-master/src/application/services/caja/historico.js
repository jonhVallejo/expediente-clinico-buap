'use strict';
(function(){

/**
 * Controlador encargado de realizar las operaciones para el carrito de compra
 * este controlador se comunica con la Factoria [carritoCompras] que es donde se
 * tiene toda la información del carrito.
 */
angular.module('expediente')

    .controller('realizarPago',['$scope','$timeout', '$q', '$log','carritoCompras','$mdDialog','catalogos','$http','$state','$window','peticiones','$localStorage','reportes',
      function ($scope,$timeout, $q, $log,carritoCompras,$mdDialog,catalogos,$http,$state,$window,peticiones,$localStorage, reportes) {

    var self = this;
    self.mImporte = 0;
    self.c_operador = ['VISA', 'MASTERCARD'];
    self.banco = [];
    self.formasPago = [];
    self.idFolioRecibo = 0;
    self.tipoOperacionPago = 1;
    self.cantidadAbonar = 0;
    self.cuentaAbono = false;
    self.pagoCuentaPaciente=false;
    verificarProcedencia();

    var tipoDeOperacion = "";
    /**
     * Function Realiza petición al servidor para obtener todo el catalogo de medicamentos.
     * @param  {[type Int Id del catalogo a solicitar ]}
     */
    catalogos.getCatalogos(5).success(function(data){
        self.banco = data;
    });


    /**
     *  Function, Realiza una petición para obtener el catalogo de formas de pago y se valida
     *  si viene de cuenta paciente mostrar tipo de pago Crédito en caso contrario no mostrarlo
     * @param  {[type id del catalgo a solicitar]}
     */
    function getFormasPago(cuentaPaciente){
      catalogos.getCatalogos(3).success(function(data){
          if(!cuentaPaciente){
          self.formasPago=_.filter(data,function(e){
             return e.cDescripcion != "CRÉDITO";
          });
        }else
            self.formasPago = data;
      });
    }
   

    self.pagoTotalServicios = carritoCompras.carrito.total;
    self.mRealizarPago = carritoCompras.carrito.formasDePago;
    /**
     * [mDialog description]
     * @param  {[type]} titulo  [description]
     * @param  {[type]} mensaje [description]
     * @return {[type]}         [description]
     */
    var mDialog = function(titulo,mensaje){
      $mdDialog.show(
          $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(titulo)
            .content(mensaje)
            .ariaLabel('Error 1')
            .ok('Aceptar')
        );
    };



    /**
     * [importeCliente Función que se utiliza para actualizar el pago restante cuando van a pagar servicios]
     * @return {[type]} [Solo se actualiza la varibale mImporte que es la que contiene la suma de los pagos que se han realizado]
     */
    self.importeCliente = function(){
      self.mImporte =0;
      for (var elemento in carritoCompras.carrito.formasDePago){
          if (typeof carritoCompras.carrito.formasDePago[elemento].D_IMPORTE != 'undefined'){
              if(carritoCompras.carrito.formasDePago[elemento].D_IMPORTE != null){
                self.mImporte += parseFloat(carritoCompras.carrito.formasDePago[elemento].D_IMPORTE);

            }
          }
        }
    };



  /**
   *  Funcion Que se utiliza para agregar una forma de pago.
   * @param {[type]}
   * @param {[type]}
   */
  self.addTipo = function(key,keyEvent){
     if (keyEvent.which == 0 | keyEvent.keyCode == 0){
     // return;
     }
      var index = carritoCompras.getIndiceByKeyCobro(key);
      console.debug("Key a buscar ", key);
      console.debug("Elemento encontrado " , index);
      if( !isNaN(carritoCompras.carrito.formasDePago[index].D_IMPORTE) ) {
          if (carritoCompras.carrito.formasDePago[index].D_IMPORTE <= 0){
                  mDialog('ERROR','EL IMPORTE NO PUEDE SER MENOR O IGUAL A CERO');
                  return;
          }else{
              if (typeof carritoCompras.carrito.formasDePago[index].TIPO_PAGO == 'undefined'){
                      mDialog('ERROR','POR FAVOR SELECCIONE EL TIPO DE PAGO');
                      return;
                  }
                carritoCompras.carrito.formasDePago.push({});
          }
      }else{
           mDialog('ERROR','POR FAVOR REVISE EL IMPORTE, CONTIENE UN DATO INCORRECTO');
      }
    };

  self.actualizarTipoPago = function(key){
      var index = carritoCompras.getIndiceByKeyCobro(key);
  }

  /**
   * [verificarPagoTotal Funsión para verificar  el pago total de la cuantea o el servicio,
   * solo se va a utilizar para pagar totales]
   * @param  {[type]} otroTipoPago [parametro para verificar si solo es pago en efectivo o hay diferentes formas de pago]
   * @return {[type]}              [description]
   */
  var verificarPagoTotal = function(otroTipoPago,pagoConEfectivo,mOtraFormaPago,contadorInputEfectivo){
    var bandera = true;
    if(otroTipoPago == false){
      if (parseFloat(pagoConEfectivo) >= parseFloat(carritoCompras.carrito.total)){
          self.cambiCliente =  parseFloat(pagoConEfectivo) - parseFloat(carritoCompras.carrito.total);
          carritoCompras.carrito.formasDePago = [{
          "D_IMPORTE": parseFloat(pagoConEfectivo) - self.cambiCliente,
          "C_OPERADOR": "",
          "N_AUTORIZACION": "0",
          "N_OPERACION": "0",
          "C_REFERENCIA": "",
          "TIPO_PAGO": 1
        }];
      }else{
          bandera = false;
          mDialog('ERROR','EL IMPORTE INGRESADO NO ALCANZA A CUBRIR EL COSTO TOTAL');
         
      }
    }else{
         if ((parseFloat(pagoConEfectivo) + parseFloat(mOtraFormaPago)) >= parseFloat(carritoCompras.carrito.total)){
              if ((parseFloat(pagoConEfectivo) + parseFloat(mOtraFormaPago)) >= parseFloat(carritoCompras.carrito.total)){
                  self.cambiCliente = (parseFloat(pagoConEfectivo) + parseFloat(mOtraFormaPago))  - parseFloat(carritoCompras.carrito.total);
                  if (contadorInputEfectivo > 0){
                    if (self.cambiCliente > pagoConEfectivo){
                        bandera = false
                        mDialog('ERROR','LAS FORMAS DE PAGO QUE SON DIFERENTES A “EFECTIVO” DEBEN SUMAR LA CANTIDAD EXACTA A PAGAR, O EN CASO DE TENER UN MONTO MENOR EL RESTO SERÁ PAGADO CON EFECTIVO');
                    }else{
                        var banC = false;
                        var ids =[];
                          for(var dat = 0; dat < carritoCompras.carrito.formasDePago.length; dat++){
                            if (banC == false && carritoCompras.carrito.formasDePago[dat].TIPO_PAGO == 1 ){
                                carritoCompras.carrito.formasDePago[dat].D_IMPORTE = pagoConEfectivo - self.cambiCliente;
                                banC = true;
                            }else{
                                    if (carritoCompras.carrito.formasDePago[dat].TIPO_PAGO == 1 ){
                                      ids.push(dat);
                                    }
                            }
                          }
                          for (var i = ids.length - 1; i >= 0; i--) {
                            carritoCompras.carrito.formasDePago.splice([ids[i]], 1);
                          };
                    }
                  }else{
                    if(parseFloat(mOtraFormaPago) != parseFloat(carritoCompras.carrito.total)){
                        bandera = false;
                        mDialog('ERROR','LAS FORMAS DE PAGO QUE SON DIFERENTES A “EFECTIVO” DEBEN SUMAR LA CANTIDAD EXACTA A PAGAR');
                    }
                  }
              }
            }else{
                bandera = false;
                mDialog('ERROR','EL IMPORTE INGRESADO NO ALCANZA A CUBRIR EL COSTO TOTAL');
            }
      }
      return bandera;
  }


  /** Función que se utiliza para realizar un pago parcal (abono), esto solo se va a utilizar para cuando se trata de una cuenta paciente. 
   * @param  {[type  Bolean Bandera que especifica si solo es pago en efectivo]}
   * @return {[type]}
   */
  var verificarPagoParcial = function(otroTipoPago){
     if(otroTipoPago == false){
      if (parseFloat(pagoConEfectivo) < parseFloat(carritoCompras.carrito.total & parseFloat(pagoConEfectivo) > 0)){
          self.cambiCliente =  parseFloat(pagoConEfectivo) - parseFloat(carritoCompras.carrito.total);
          carritoCompras.carrito.formasDePago = [{
          "D_IMPORTE": parseFloat(pagoConEfectivo) - self.cambiCliente,
          "C_OPERADOR": "",
          "N_AUTORIZACION": "0",
          "N_OPERACION": "0",
          "C_REFERENCIA": "",
          "TIPO_PAGO": 1
        }];
      }else{
           // if (parseFloat(pagoConEfectivo)  ){

           // }
      }
    }
  }


  /**
   * Funcion principal de la cual derivan todas las validaciones para realizar un pago, sea derecho habiente o no
   * @return {[type]}
   */
  self.verificarTiposDePago_xx = function(){
      console.debug($localStorage);
      if($localStorage.stateAnterior == "detalleCuentaPacienteEspecifica"){
        console.debug("viene del detalle de una cuenta paciente.")

      }else{
          if($localStorage.stateAnterior == "ventaDeServicios"){
            console.debug("viene de la venta de servicios");
            verificarPagoTotal()
         }
      }


  }

  self.verificarTiposDePago = function(){
    var bandera = true;
    var pagos = 0;
    var mensaje ="";
    var pagoConEfectivo = 0;
    var contadorInputEfectivo = 0;
    var otroTipoPago = false;
    self.cambiCliente = 0;

    var mOtraFormaPago = 0;


    console.log(carritoCompras.carrito.tipo_pago );
    if (carritoCompras.carrito.servicios.length<1 & carritoCompras.carrito.tipo_pago  == "cobro_serv"){
       mDialog('ERROR','NO HAY ELEMENTOS EN EL CARRITO DE COMPRAS');
       return;
    }else{
        if (carritoCompras.carrito.tipo_pago  == "abono_cuenta"){
           console.log("Si va a dejhar pagar");
           //return;
        }
    }

  for (var id = 0; id < carritoCompras.carrito.formasDePago.length; id++)
  {

      if(!isNaN(carritoCompras.carrito.formasDePago[id].D_IMPORTE)){
          carritoCompras.carrito.formasDePago[id].D_IMPORTE = parseFloat(carritoCompras.carrito.formasDePago[id].D_IMPORTE);
          if(carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 1){
              pagoConEfectivo += parseFloat(carritoCompras.carrito.formasDePago[id].D_IMPORTE);
              contadorInputEfectivo += 1;
            }else{
                otroTipoPago = true;
                mOtraFormaPago += carritoCompras.carrito.formasDePago[id].D_IMPORTE;
            }
      }else{
          if(carritoCompras.carrito.total == 0){
             // generaPago();
           break;
          }
          mDialog('ERROR','POR FAVOR REVISE EL IMPORTE, CONTIENE UN DATO INCORRECTO');
          return;
      }
    if( typeof carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 'undefined'){
             mDialog('POR FAVOR SELECCIONE EL TIPO DE PAGO');
             return;
    }
    carritoCompras.carrito.formasDePago[id].TIPO_PAGO =parseInt(carritoCompras.carrito.formasDePago[id].TIPO_PAGO);
    if (carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 2 || carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 3){
      if( typeof carritoCompras.carrito.formasDePago[id].NP_ID == 'undefined'){
             mDialog('POR FAVOR SELECCIONE EL TIPO DE BANCO');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].C_OPERADOR == 'undefined'){

             mDialog('POR FAVOR SELECCIONE EL TIPO DE OPERADOR');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].N_AUTORIZACION == 'undefined'){
             mDialog('POR FAVOR INGRESE EL NÚMERO DE AUTORIZACIÓN');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].N_OPERACION == 'undefined'){
             mDialog('POR FAVOR INGRESE EL NÚMERO DE OPERACIÓN');
             return;
      }
        carritoCompras.carrito.formasDePago[id].N_AUTORIZACION = carritoCompras.carrito.formasDePago[id].N_AUTORIZACION;
        carritoCompras.carrito.formasDePago[id].N_OPERACION = carritoCompras.carrito.formasDePago[id].N_OPERACION;

    }

    if (carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 4){
      if( typeof carritoCompras.carrito.formasDePago[id].NP_ID == 'undefined'){
             mDialog('POR FAVOR SELECCIONE EL TIPO DE BANCO');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].C_REFERENCIA == 'undefined'){
             mDialog('POR FAVOR INGRESE EL NÚMERO DE REFERENCIA');
             return;
      }
       if(isNaN(carritoCompras.carrito.formasDePago[id].C_REFERENCIA)){
            mDialog('EL NÚMERO DE REFERENCIA NO ES VALIDO');
            return;
       }
    }
    if (carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 5){
      if( typeof carritoCompras.carrito.formasDePago[id].TIPO_SERVICIO == 'undefined'){
             mDialog('POR FAVOR INGRESE EL TIPO DE SERVICIO');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].FECHA_IMPRESION == 'undefined'){
             mDialog('POR FAVOR INGRESE LA FECHA DE IMPRESION');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].ID_PARENTESCO == 'undefined'){
             mDialog('POR FAVOR SELECCIONE PARENTESCO');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].ADSCRITO == 'undefined'){
             mDialog('POR FAVOR INGRESE ADSCRITO');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].TIPO_CONVENIO == 'undefined'){
             mDialog('POR FAVOR SELECCIONE TIPO DE CONVENIO');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].SEGURO_POPULAR == 'undefined'){
             mDialog('POR FAVOR INGRESE SEGURO POPULAR');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].DOC_REFERENCIA == 'undefined'){
             mDialog('POR FAVOR INGRESE EL DOCUMENTO DE REFERENCIA');
             return;
      }
      // if( typeof carritoCompras.carrito.formasDePago[id].FECHA_DOCUMENTO == 'undefined'){
      //        mDialog('POR FAVOR INGRESE LA FECHA DEL DOCUMENTO');
      //        return;
      // }
     if( typeof carritoCompras.carrito.formasDePago[id].OBSERVACIONES == 'undefined'){
             mDialog('POR FAVOR INGRESE LAS OBSERVACIONES');
             return;
      }
     
    }
    


  }//Termina el for

    if($localStorage.stateAnterior == "detalleCuentaPacienteEspecifica" && carritoCompras.carrito.tipo_pago  == "abono_cuenta"){
        console.debug("viene del detalle de una cuenta paciente.")

      }else{
          if($localStorage.stateAnterior == "ventaDeServicios" && carritoCompras.carrito.tipo_pago  == "cobro_serv"){
            console.debug("viene de la venta de servicios");
            bandera = verificarPagoTotal(otroTipoPago, pagoConEfectivo, mOtraFormaPago,contadorInputEfectivo);
            console.debug("Resultado de verificar el pago: ", bandera);
         }else{
          console.error("No se puede realizar el pago por que la procedencia no esta considerada");
         }
    }

  if (bandera){
    generaPago();
    }
  }

  function limpiarCarrito(){
    carritoCompras.carrito.cajero = 0;
    carritoCompras.carrito.paciente = 0;
    carritoCompras.carrito.total = 0;
    carritoCompras.carrito.efectivo = 0;
    carritoCompras.carrito.servicios = [];
    carritoCompras.carrito.formasDePago =[{}];
  };


  function generaPago(){
      var url = 'pago_serv_productos';
      if (carritoCompras.carrito.servicios.length>0){
          peticiones.postDatos(url,carritoCompras.carrito)
            .then(function(data) {
              //carritoCompras.reset();
              self.idFolioRecibo = parseInt(data.id);
               self.impRecibo = function() {
                  reportes.getReporte(baseURL + 'reporte_ReciboDePago/' +  data.id , '_blank', 'width=1000, height=800');
                   // $window.location.reload();
                   $state.go('bienvenido');
                };
          }).catch(function(response) {
             mDialog('ERROR','OCURRIO UN ERROR AL REALIZAR LA TRANSACCIÓN POR FAVOR CONTACTE AL ADMINISTRADOR');
            });
      }else{
         mDialog('Error','No hay elementos en el carrito de compras');
      }
  }

  self.delTiposDePago = function(key,keyEvent){
     if (keyEvent.which == 0 | keyEvent.keyCode == 0){
    // return;
     }
      var index = carritoCompras.getIndiceByKeyCobro(key);
      carritoCompras.carrito.formasDePago.splice(index,1);
      self.importeCliente();

  }

  self.importeAbonar=function(importe){
    console.log(importe);
    self.aux.D_IMPORTE=importe;
  };


  self.exprs = [''];

  /***************************************TIPO DE PAGO "CUENTAS POR COBRAR"*****************************************************/
   function getCatalogos(){
       peticiones.getDatos("catalogo/"+17)
          .then(function(data) {
            //GETDatos
            console.log(data);
            $scope.catParentesco=data;
          }).catch(function(err) {
              console.log(err);
              mensajes.alertaTimeout('ALERTA','EL FOLIO DE LA RECETA NO  EXISTE','ACEPTAR', 3000);
        });
       peticiones.getDatos("catalogo/"+1)
          .then(function(data) {
            //GETDatos
               console.log(data);
            $scope.catConvenios=data;
          }).catch(function(err) {
              console.log(err);
              mensajes.alertaTimeout('ALERTA','EL FOLIO DE LA RECETA NO  EXISTE','ACEPTAR', 3000);
        });

    };

  getCatalogos();
  /*************************************FUNCIONES PARA CUENTA PACIENTE**********************************************************/
  self.abonar = function(abono){
     self.cantidadAbonar =abono;
     self.importeCliente();
  };

  function verificarProcedencia (){
      if($localStorage.stateAnterior == "detalleDeServicios"){ // viene del detalle de su cuneta paciente, va a pagar o a abonar
           self.pagoCuentaPaciente=true;
          if(carritoCompras.carrito.tipo_pago  == "abono_cuenta"){
            self.cuentaAbono=true;
            console.log("cuenta a abonar");
            console.log(self.cuentaAbono);
          }
      }else{
          if($localStorage.stateAnterior == "ventaDeServicios" ){ // viene del carrito cunado compra los servicios
            self.pagoCuentaPaciente=false;
            console.log("venta de servicios");
            self.cuentaAbono=false;
            console.log(self.cuentaAbono);
          }
      }
      getFormasPago(self.pagoCuentaPaciente);
    }
    verificarProcedencia();

    }]);
  })();
