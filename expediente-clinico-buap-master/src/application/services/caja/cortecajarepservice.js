'use strict';

/**
 * @ngdoc service
 * @name expediente.caja/corteCajaRepService
 * @description
 * # caja/corteCajaRepService
 * Service in the expediente.
 */
angular.module('expediente')
  .service('corteCajaRepService', ['peticiones','mensajes',function (peticiones,mensajes) {
    var self = this;

    //Reporte de Concetrado de Ventas
      self.getDatosRepVentas = function(datos){
        return peticiones.putMethod('reportedeventasdatos/',datos);
    };

    self.getDatosRepVentasPDF = function(datos){
        return peticiones.putMethodPDF('reportedeventaspdf/',datos);
    };


    //reporteCorteDeCaja
    self.getDatosCorteCaja = function(datos){
    	return peticiones.putMethod('reportecortecajadatos/',datos);
    };

    self.getDatosCorteCajaPDF = function(datos){
    	return peticiones.putMethodPDF('reportecortecajapdf/',datos);
    };

    //Reporte de Servicios

    self.getDatosRepServicios = function(datos){
    	return peticiones.putMethod('reportedeserviciosdatos/',datos);
    };

    self.getDatosRepServiciosPDF = function(datos){
    	return peticiones.putMethodPDF('reporteDeServiciospdf/',datos);
    };

    //Reporte de Concetrado de Ventas
    self.getDatosRepConcentradoVentas = function(datos){
        return peticiones.putMethod('reporteconcentradodeventasdatos/',datos);
    };


    self.getDatosRepConcentradoVentasPDF = function(datos){
    	return peticiones.putMethodPDF('reporteConcentradoDeVentaspdf/',datos);
    };

    //Reportes de Cajas
    self.getDatosRepCorteCajaVentas = function(datos){
        return peticiones.getMethodPDF('cajacorte/ventasPDF/'+datos);
    };

    self.getDatosRepCorteCajaConcentrado = function(datos){
        return peticiones.getMethodPDF('cajacorte/concentradoVentasPDF/'+datos);
    };

    self.getDatosRepCorteCajaPDF = function(datos){
        return peticiones.getMethodPDF('cajacorte/PDF/'+datos);
    };
    


    
  }]);
