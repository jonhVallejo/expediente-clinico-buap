(function(){
'use strict';
angular.module('expediente')
  .service('cuentaPacienteService', ['peticiones',function (peticiones) {

      var self = this;
      var NP_CUENTA_PACIENTE = undefined;


      self.setNPCuentaPaciente = function(NP){
        NP_CUENTA_PACIENTE = NP;
      }
      self.getNPCuentaPaciente = function(){
          return NP_CUENTA_PACIENTE;
      }

      // var url = "cuentaPaciente/activa/";

      var T_CUENTA_PACIENTE = '',
          C_CAMAS = '',
          T_PACIENTE  = '',
          C_CONVENIO = '';

       /*
              Retorna la cuenta paciente activa, recibe como parametro el NP_EXPEDIENTE
              del paciente
      */
      self.CuentaNP_EXPEDIENTE_PACIENTE =  function(NP_EXPEDIENTE){
          peticiones.getDatos("cuentaPaciente/activa/" + NP_EXPEDIENTE)
          .then(function(data) {
            console.debug("Funcion que recibe un NP_EXPEDIENTE y retorna una cuneta paciente activa");
              if(data){
                console.debug("***");
                  console.debug(data);
                  console.debug("***");
                  self.fillCuentaPaciente(data);
                  // return true;
              }else{

                  console.debug("*** Sin contenido");
                   // return false;
              }

          }).catch(function(response) {
              mDialog('ERROR','OCURRIO UN ERROR AL REALIZAR LA TRANSACCIÓN POR FAVOR CONTACTE AL ADMINISTRADOR');
          });
        }

      // /*
      //         Retorna la cuenta paciente, recibe como parametro el NP_ID de la cuenta
      //         del paciente
      // */
      self.CuentaNP_ID_CUENTA_PACIENTE =  function(NP_ID){
        peticiones.getDatos("cuentaPaciente/" + NP_ID)
          .then(function(data) {
            console.debug("Funcion que recibe un NP_EXPEDIENTE y retorna una cuneta paciente activa");
              if(data){
                  console.debug("***"  +  data);
                  self.fillCuentaPaciente(data);
                  return true;
              }else{
                  return false;
                  console.debug("*** Sin contenido");
              }

          }).catch(function(response) {
              mDialog('ERROR','OCURRIO UN ERROR AL REALIZAR LA TRANSACCIÓN POR FAVOR CONTACTE AL ADMINISTRADOR');
          });
      }


        self.fillCuentaPaciente = function(mData){
            console.log(mData.T_CUENTA_PACIENTE);
            T_CUENTA_PACIENTE = mData.T_CUENTA_PACIENTE;
            T_PACIENTE = mData.T_PACIENTE;
            C_CONVENIO = mData.C_CONVENIO;
        }

        this.getCuentaPaciente = function(){
          return T_CUENTA_PACIENTE;
        }

        this.getCamas = function(){
            return C_CAMAS;
        }

        this.getPaciente = function(){
            return T_PACIENTE;
        }

        this.getConvenio = function(){
            return C_CONVENIO;
        }







  }]);
})();
