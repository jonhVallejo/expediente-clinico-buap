(function(){

  'use strict';

  /**
   * @ngdoc service
   * @name expediente.caja/cancelarServicio
   * @description
   * # caja/cancelarServicio
   * Service in the expediente.
   */
  angular.module('expediente')
    .service('cancelarServicio',['$state','peticiones','mensajes','catalogos','$location','reportes',
      function ($state,peticiones,mensajes,catalogos,$location,reportes) {



    	var self = this;

      self.index = -1;
      self.parentescos = [];
      self.identificaciones = [];
      self.response = {
                        cajero: 1,
                        paciente: "1",
                        importe_devolver: 0,
                        servicios: [],
                        NOMBRE: "",
                        C_TELEFONO: "",
                        NF_IDENTIFICACION: -1,
                        C_FOLIO_IDENTIFICACION: "",
                        NF_RELACION_PACIENTE: -1
                      };

      self.mensaje = '';
      self.cantidad = 1;

    	self.compra = {};// {"PACIENTE":"ALFREDO VELUETA VIVEROS","I_PAGOS_FORMAS_PAGO":[],"I_PAGOS_SERVICIOS":[{"C_CLAVE":"1","SF_ESTATUS":1,"DESCRIPCION_CONDONACION":"CONDONACION POR BUENA ONDA","I_PAGOS_CONVENIOS":[{"C_DESCRIPCION":"CASA DEL ABUE","D_IMPORTE":50}],"D_IMPORTE_CONDONACION":10,"D_IMPORTE_TOTAL":15.5,"C_DESCRIPCION":"Rayos XXX","ID_SERVICIO_PRODUCTO":1,"N_SERVICIOS":1,"D_COSTO":1000,"T_FECHA_CMD":"04/09/2015","D_IMPORTE":75.5}],"CAJERO":"ALFREDO VELUETA VIVEROS","N_ESTATUS":1,"D_IMPORTE_TOTAL":15.5,"NP_ID":40,"TERMINAL":"CAJA URGENCIAS","T_FECHA_CMD":"08/09/2015"};
    	self.servicio = [];

      if(self.compra.PACIENTE == undefined){
        if($location.path() == "/caja/buscarServicio"){
          $state.transitionTo("buscarServicio");
        }else{
          $state.transitionTo("reimprimirReciboPago");
        }
        
      }

    	self.setCompra = function(compra){
    		self.compra = compra;
        self.response.cajero = compra.ID_CAJERO;
        self.response.paciente = compra.ID_PACIENTE;
        self.response.importe_devolver = 0;
        console.log(self.compra.I_PAGOS_SERVICIOS);
    	};

    	self.setServicio = function(obj){
        self.servicio = [];
    		self.servicio.push(obj);
    	};

      self.siguiente = function(){
        if(self.response.servicios.length > 0){
          getParentesco();
          getIdentificaciones();
          $state.transitionTo("cajaVale");
        }else{
          mensajes.alerta('ADVERTENCIA','NO HA ELIMINADO NINGUN SERVICIO','ACEPTAR');
        }
      };

      self.atras = function(){
        $state.transitionTo("cancelarServicios");
      };

      self.enviar = function(){
        console.log(self.response);
        peticiones.putMethod("cancela_serv_productos/"+self.compra.NP_ID,self.response)
        .success(function(data){
          console.log(data);
          reportes.getReporte(baseURL + 'reporte_cancela_serv_productos/'+data.id_cancelacion, '_blank', 'width=600, height=800,toolbar=no,scrollbars=yes,Location=no');
          reportes.getReporte(baseURL + 'vale_de_Caja/'+data.id_vale, '_blank', 'width=600, height=800,toolbar=no,scrollbars=yes,Location=no');
          $state.transitionTo("bienvenido");
        })
        .error(function(data){
          mensajes.errorConexion();
        });
      }
      self.validar=function(){
          return  mensajes.confirmacion('ALERTA','ESTA SEGURO QUE DESEA CANCELAR LOS SERVICIOS','Aceptar');
      }
      self.eliminarServicio = function(page,limit){
        //Se crea el objeto base
        var desc = {NP_ID: 0,N_CANCELADOS: 0,IMPORTE: 0, key:"",desc:""};
        var pos = self.index + ((page-1)*limit);
        console.log(page+" : "+limit)
        console.log(pos);

        //Variable referenciando a totos los servicios del tiket
        var servicios = self.compra.I_PAGOS_SERVICIOS;
        desc.NP_ID = servicios[pos].ID_SERVICIO_PRODUCTO;
        desc.N_CANCELADOS = parseInt(self.cantidad);
        desc.IMPORTE = parseInt((servicios[pos].D_IMPORTE_TOTAL) - (servicios[pos].D_IMPORTE_TOTAL/servicios[pos].N_SERVICIOS*(servicios[pos].N_SERVICIOS-self.cantidad)));
        desc.key = servicios[pos].$$hashKey;
        desc.desc = servicios[pos].C_DESCRIPCION;
        //self.response.importe_devolver = parseInt(self.response.importe_devolver) +  desc.IMPORTE;

        //self.response.servicios.push(desc);
        insertServ(desc);
 
        servicios[pos].eliminados = parseInt(self.cantidad);
        servicios[pos].colores = Math.round(255*(servicios[pos].N_SERVICIOS-self.cantidad)/servicios[pos].N_SERVICIOS);
        console.log(servicios);

        calcularTotal();

      };

      function insertServ(aux){
        var serv = self.response.servicios;
        for(var i=0 ; i<serv.length ; i++){
          if(serv[i].key === aux.key){
            if(aux.N_CANCELADOS !== 0){
              serv[i].NP_ID = aux.NP_ID;
              serv[i].N_CANCELADOS = aux.N_CANCELADOS;
              serv[i].IMPORTE = aux.IMPORTE;
            }else{
              serv.splice(i, 1);
            }
            return 0;
          }
        }
        if(aux.N_CANCELADOS !== 0){
          serv.push(aux);
        }
      };

      function calcularTotal(){
        var serv = self.response.servicios;
        var cont = 0;
        for(var i=0 ; i<serv.length ; i++){
          cont += serv[i].IMPORTE;
        }
        self.response.importe_devolver = cont;
        self.cantidad = 1;
      };


      //////////////////////////////////////////////////
      /////   CONSULTA DE CATALOGOS
      //////////////////////////////////////////////////
      
      function getParentesco(){
        catalogos.getCatalogos(17)
        .success(function(data){
          console.log(data);
            self.parentescos = (data);
        })
        .error(function(data){
          mensajes.alerta('ERROR','NO SE HA PODIDO RECUPERAR LA LISTA DE PARENTESCOS','Aceptar');
        });
      };

      function getIdentificaciones(){
        catalogos.getCatalogos(18)
        .success(function(data){
          console.log(data);
            self.identificaciones = (data);
        })
        .error(function(data){
          mensajes.alerta('ERROR','NO SE HA PODIDO RECUPERAR LA LISTA DE IDENTIFICACIONES','Aceptar');
        });
      };

      self.existIdCatalogo = function(cat,id){
        console.log(id);
        console.log(cat);
        for(var i=0 ; i<cat.length ; i++){
          if(cat.ID === id){
            console.log("Okas");
            return true;
          }
        }
        console.log("false");
        return false;
      };


    }]);

})();
