(function(){

	'use strict';

	/**
	 * @ngdoc service
	 * @name expediente.peticiones
	 * @description
	 * # peticiones
	 * Service in the expediente.
	 */
	angular.module('expediente')
	  .service('peticiones',['$http', '$q','Upload',  function ($http, $q, Upload) {
	  	var self = this;

	  	self.postMethod = function(url,data){
        return $http.post(baseURL + url, data);
	  	};

      self.postDatos = function(url,data) {
        var defered = $q.defer();
        var promise = defered.promise;

        $http.post(baseURL + url, data)
            .success(function(data) {
                defered.resolve(data);
            })
            .error(function(err) {
                defered.reject(err)
            });

        return promise;
        }


      self.getDatos = function(url) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(baseURL + url)
            .success(function(data, status, headers, config) {
                defered.resolve(data);
            })
            .error(function(data, status, headers, config) {
                defered.reject({data:data, status: status, headers: headers, config: config});
            });

        return promise;
      }


       self.getDatosPruebas = function(url) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(url)
            .success(function(data, status, headers, config) {
                defered.resolve(data);
            })
            .error(function(data, status, headers, config) {
                defered.reject({data:data, status: status, headers: headers, config: config});
            });

        return promise;
      }

      self.getDatosStatus = function(url) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http.get(baseURL + url)
            .success(function(data, status, headers, config) {
                defered.resolve({data:data, status: status, headers: headers, config: config});
            })
            .error(function(data, status, headers, config) {
                defered.reject({data:data, status: status, headers: headers, config: config});
            });

        return promise;
      }

    self.deleteDatos = function(url){
      var defered = $q.defer();
      var promise = defered.promise;
      $http.delete(baseURL + url)
      .success(function (data, status) {
          defered.resolve(data);
      })
      .error(function(err) {
          defered.reject(err)
      });
      return promise;
    };

    self.deleteDatosParametro = function(url,data){
      // var defered = $q.defer();
      // var promise = defered.promise;
      // $http.delete(baseURL + url, datas)
      // .success(function (data, status) {
      //     defered.resolve(data);
      // })
      // .error(function(err) {
      //     defered.reject(err)
      // });
      // return promise;

      return $http.delete(baseURL + url, {"data":data});
    };

  	self.putMethod = function(url,data){
      return $http.put( baseURL + url, data);
  	};

    self.getMethod = function(url){
      return $http.get( baseURL+url);
    };

    self.putMethodPDF = function(url,data){
      return $http({
        url : baseURL + url,
        method : 'PUT',
        data: data,
        headers:{
          'Content-Type': 'application/json; charset=utf-8'
        },
        responseType: 'arraybuffer'
      });
    };

    self.postMethodPDF = function(url,data){
      return $http({
        url : baseURL + url,
        method : 'POST',
        data: data,
        headers:{

          'Content-Type': 'application/json; charset=utf-8'
        },
        responseType: 'arraybuffer'
      });
    };

    self.getMethodPDF = function(url){
      return $http({
        url : baseURL + url,
        method : 'GET',
        headers:{
          'Content-Type': 'application/json; charset=utf-8'
        },
        responseType: 'arraybuffer'
      });
    };

    self.uploadArchivo = function(url, file) {
      return  Upload.upload({
                url: baseURL + url,
                data: {file: file},
                headers: { 'Content-Type': undefined},
              });
    }

    self.filtrar = function(filtro,datos,campos) {
      /*
      * datos: Información que se muestra en la tabla donde desea aplicar el filtro
      *
      * campos: JSON con ls siguiente estructura: [ {descripcion : "especificar el campo de la tabla",
                                                     validar     : "true si desea verificar que el campo exista, false en caso contrario",
                                                     campo       : "campo que se desea verificar" }
      *                                              {}, {}, ...];
      * NOTA: El campo a verificar es por si en el JSON de la tabla, este no se envie por no contar con información.
      */
      var encontrado;
      var result;
      var existe;

      var registros = _.filter(datos, function(registro){
        encontrado = false;

        angular.forEach(campos, function(campo, index) {
          //console.log("registro."+campo.descripcion+".indexOf('"+filtro+"')");
          if (!encontrado) {
            existe = true;
            if (campo.validar)
              existe = eval("registro.hasOwnProperty('"+campo.campo+"')");
            if (existe) {
							try{

								result = eval("registro."+campo.descripcion+".indexOf('"+filtro+"')");
							}catch(Ex){}
              if (result >= 0)
                encontrado= true;
            }
          }
        });

        return  encontrado
      });

      return registros;
    }

	  }]);

})();
