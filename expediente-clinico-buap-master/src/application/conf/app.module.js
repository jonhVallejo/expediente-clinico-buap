(function(){
	'use strict';
	angular.module('expediente', [
        'ngAnimate',
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ui.router',
        'ngSanitize',
        'ngMaterial',
        'md.data.table',
        'ngStorage',
        'blockUI',
      	'textareaChips',
        'ngFileUpload'
        ]);

})();

//var baseURL = 'http://ece.ditco-fs.com/api-ventas/';  //Araceli

var baseURL = 'http://hospital.trafficsdigital.com/api-ventas/';  //Server pruebas
