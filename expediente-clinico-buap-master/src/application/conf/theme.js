'use strict';

angular.module('expediente').config( function($mdThemingProvider){

  // Configuración de la paleta de colores para el tema.
  $mdThemingProvider.definePalette('huPallete', {
     '50': 'E8EAF6',
    '100':'C5CAE9',
    '200':'9FA8DA',
    '300':'4FC3F7',
    '400': '29b6f6',
    '500': '003B5C',
    '600': '039be5',
    '700': '0288d1',
    '800': '0277bd',
    '900': '01579b',
    'A100': '80d8ff',
    'A200': '40c4ff',
    'A400': '00b0ff',
    'A700': '0091ea',

    'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                        // on this palette should be dark or light
    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
     '200', '300', '400', 'A100'],
    'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });
  $mdThemingProvider.theme('default')
    .primaryPalette('huPallete')
    .accentPalette('blue');



  });