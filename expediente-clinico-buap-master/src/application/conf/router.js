'use strict';

var menuView = {
  templateUrl: '/application/views/menu.html',
  controller: 'MenuCtrl as vm'
};

/*
*
* Router configuration, se definen los estados de la aplicación
* para cada ruta.
*
*/

var _provider;
angular.module('expediente').config(['$stateProvider', '$urlRouterProvider', '$logProvider', '$locationProvider',
  function ($stateProvider, $urlRouterProvider, $locationProvider) {

      //  $locationProvider.html5Mode(true);
      $urlRouterProvider.otherwise("/");

      $stateProvider
      .state('home', {
        url: '/',

        views: {
          'contentView':{
            templateUrl: '/application/views/login.html',
            controller: 'LoginCtrl'
          }
          ,
          'menuView':{
            controller: function($scope){
              $scope.hide = true;
            }
          }
          ,
          'footerView': {
            templateUrl : '/application/views/common/footer.html',
          }
        }
      })
      .state('perfil',{
        url: '/perfil',
        views: {
          'contentView': {
            templateUrl: '/application/views/perfil.html',
            controller: 'PerfilCtrl as vm',
          }
        }
      })
      .state('bienvenido',{
        url: '/bienvenido',
        views: {
          'contentView': {
            templateUrl: '/application/views/bienvenido.html',
            controller: 'BienvenidoCtrl'
          },
          'menuView': menuView
        }
      })
      .state('consultaPaciente',{
        url:'/consultaPaciente',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/consultapaciente.html',
            controller: 'ConsultapacienteCtrl'
          },
        }
      })
      .state('buscaDerechoHabiente',{
        url:'/buscaDerechoHabiente',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/buscaderechohabiente.html',
            controller: 'BuscaderechohabienteCtrl'
          },
        }
      })

      /*AGENDA*/

      .state('agendaRegistraPaciente',{
        url:'/agenda/registraPaciente',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/registrapaciente.html',
            controller: 'AgendaRegistrapacienteCtrl'
          },
        }
      })
      .state('agendaConsultaPaciente',{
        url:'/agenda/consultaPaciente/:state',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/consultapaciente.html',
            controller: 'AgendaConsultapacienteCtrl as ctrlPac'
          },
        }
      })
      .state('agendaVerPaciente',{
        url:'/agenda/verPaciente',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/verpaciente.html',
            controller: 'AgendaVerpacienteCtrl'
          },
        }
      })
      .state('agendaConsultaMedico',{
        url:'/agenda/consultaMedico/:state',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/consultamedico.html',
            controller: 'AgendaConsultamedicoCtrl as ctrlMed'
          },
        }
      })
      .state('agendaVerDisponibilidad',{
        url:'/agenda/verDisponibilidad',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/verdisponibilidad.html',
            controller: 'AgendaVerdisponibilidadCtrl as ctrlDisp'
          },
        }
      })
      .state('agendaCalendarizarCita',{
       url:'/agenda/calendarizarCita',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/calendarizarcita.html',
            controller: 'CalendarizarcitaCtrl as ctrlCita'
          },
        }
      })
      .state('agendaReagendarCita',{
       url:'/agenda/reagendarCita',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/calendarizarcita.html',
            controller: 'AgendaReagendarcitaCtrl as ctrlCita'
          },
        }
      })
      .state('agendaCalendarizarCitaEspecial',{
       url:'/agenda/calendarizarCitaEspecial',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/calendarizarcita.html',
            controller: 'AgendaCalendarizarcitaespecialCtrl as ctrlCita'
          },
        }
      })
      .state('agendaCalendarizarPreCita',{
       url:'/agenda/calendarizarPreCita',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/calendarizarcita.html',
            controller: 'AgendaCalendarizarprecitaCtrl as ctrlCita'
          },
        }
      })
      .state('agendaRegistrarReferencia',{
       url:'/agenda/registrarReferencia',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/referenciamedica.html',
            controller: 'AgendaReferenciamedicaCtrl as ctrlReferencia'
          },
        }
      })
      .state('catalogosAgenda',{
        url:'/agenda/catalogos',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/catalogos.html',
            controller: 'AgendaCatalogosCtrl'
          },
        }
      })
      .state('consultarAgenda',{
        url:'/agenda/consultarAgenda',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/consultaragenda.html',
            controller: 'AgendaConsultaragendaCtrl'
          },
        }
      })
      .state('agendaCitaPrimeraVez',{
        url:'/agenda/citaPrimeraVez',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/consultamedico.html',
            controller: 'AgendaPrimeravezCtrl as ctrlMed'
          },
        }
      })
      .state('agendaVerDisponibilidadPV',{
        url:'/agenda/verDisponibilidadPV',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/verdisponibilidad.html',
            controller: 'AgendaDisponibilidadprimeravezCtrl as ctrlDisp'
          },
        }
      })

      .state('agendaConsultarCitas',{
       url:'/agenda/consultarCitas',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/consultarcita.html',
            controller: 'AgendaConsultarcitaCtrl as ctrlPac'
          },
        }
      })

      .state('agendaImprimirTicket',{
       url:'/agenda/reimprimirTicket',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/reimprimirTicket.html',
            controller: 'AgendaReimprimirticketCtrl'
          },
        }
      })
      .state('agendaImprimirCarnet',{
       url:'/agenda/reimprimirCarnet',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/consultapaciente.html',
            controller: 'AgendaImprimircarnetCtrl as ctrlPac'
          },
        }
      })
      .state('agendaConfirmacionPreCitas',{
       url:'/agenda/confirmacionPreCitas',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/confirmacionprecitas.html',
            controller: 'AgendaConfirmacionprecitasCtrl'
          },
        }
      })
      .state('agendaReporteMedicos',{
        url: '/agenda/reportes/agendamedicos',
        views: {
          'menuView': menuView,
          'contentView': {
            templateUrl: '/application/views/agenda/reportes/agenda.html',
            controller: 'AgendaReportesAgendaCtrl'
          }
        }
      })

      .state('agendaReporteMedico',{
        url: '/agenda/reportes/agendamedico',
        views: {
          'menuView': menuView,
          'contentView': {
            templateUrl: '/application/views/agenda/reportes/agendamedico.html',
            controller: 'AgendaReportesAgendamedicoCtrl'
          }
        }
      })

      .state('agendaDiasDeExcepcion',{
          url:'/agenda/diasDeExcepcion',
           views: {
            'menuView': menuView,
             'contentView': {
               templateUrl: '/application/views/agenda/diasdeexcepcion.html',
                 controller: 'AgendaDiasdeexcepcionCtrl as ctrlDiasExc'
             },
           }
      })
      .state('agendaDiasFestivos',{
          url:'/agenda/diasFestivos',
           views: {
            'menuView': menuView,
             'contentView': {
               templateUrl: '/application/views/agenda/diasfestivos.html',
                 controller: 'AgendaDiasfestivosCtrl as ctrlDiasFest'
             },
           }
      })


      .state('agendaAdministracionUsuarios',{
       url:'/agenda/administracionUsuarios',
       views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/administracionusuarios.html',
            controller: 'AgendaAdministracionusuariosCtrl as ctrlPac'
          },
        }
      })
      .state('agendaAdministracionMedicos',{
        url:'/agenda/administracionMedicos',
        views: {
         'menuView': menuView,
          'contentView': {
            templateUrl: '/application/views/agenda/administracionmedicosbase.html',
            controller: 'AgendaAdministracionmedicosCtrl'
          },
        }
      })

      .state('agendaCatCIE10',{
          url:'/agenda/catCIE10',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/agenda/catcie10.html',
              controller: 'AgendaCatcie10Ctrl as ctrl'
            },
          }
        })

      .state('agendaCatalogos',{
        url:'/agenda/catalogos/:idCatalogo',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/caja/catalogos/catalogogeneral.html',
            controller: 'CajaCatalogosCatalogogeneralCtrl'
          },
        }
      })
      .state('agendaActualizaPaciente',{
        url:'/agenda/actualizaPaciente',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/registrapaciente.html',
            controller: 'AgendaActualizapacienteCtrl'
          },
        }
      })
      .state('agendaReactivarCita',{
        url:'/agenda/reactivarCita',
        views: {
         'menuView': menuView,
         'contentView': {
            templateUrl: '/application/views/agenda/reactivarcitas.html',
            controller: 'ReactivarcitasCtrl'
          },
        }
      })
        // .state('buscaPacienteServicios',{
        //     url:'/caja/ventaDeServicios/',
        //     views: {
        //      'menuView': menuView,
        //      'contentView': {
        //       templateUrl: '/application/views/agenda/consultapaciente.html',
        //       controller: 'AgendaConsultapacienteCtrl as ctrlPac',
        //       params: {'new_param': 'ventaDeServicios'}
        //     },
        //   }
        // })
        .state('crearCuentaPaciente',{
            url:'/caja/crearCuentaPaciente/',
            views: {
             'menuView': menuView,
              'contentView': {
                templateUrl: '/application/views/caja/crearCuentaPaciente.html',
                controller: 'crearCuentaPaciente as ctrl'
              },
            }
          })
        .state('buscaPreCitas',{
            url:'/caja/buscaPreCitas',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/buscaPreCitas.html',
              controller: 'buscaPreCitas as ctrl'
            },
          }
        })
          .state('ventaDeServicios',{
            url:'/caja/compraServicios',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/compraservicios.html',
              controller: 'CompraserviciosCtrl as ctrl'
            }
          }
        })
          .state('ventaDeServiciosPrecita',{
            url:'/caja/compraServiciosPrecita',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/compraservicios.html',
              controller: 'CompraserviciosCtrl as ctrl'
            }
          }
        })
          .state('ventaDeServiciosCuentaPaciente',{
            url:'/caja/detalleServiciosCuentaPaciente',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/compraserviciosCuentaPaciente.html',
              controller: 'CompraserviciosCtrl as ctrl'
            }
          }
        })
          .state('ventaDeServiciosCuentaPacienteAdmin',{
            url:'/caja/compraServiciosCuentaPacienteAdmin',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/compraserviciosCuentaPaciente.html',
              controller: 'CompraserviciosCtrl as ctrl'
            }
          }
        })
        .state('detalleDeServicios',{
            url:'/caja/detalleDeServicios',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/compraservicios.html',
              controller: 'CajaDetalleserviciosCtrl as ctrl'
            }
          }
        })
          .state('realizarPago',{
            url:'/caja/realizarPago',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/realizarPago.html',
              controller: 'realizarPago as ctrl'
            }
          },
          from: ['ventaDeServicios','detalleCuentaPacienteEspecifica']
        })
          .state('realizarPagoCuentaPaciente',{
            url:'/caja/realizarPagoCuentaPaciente',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/realizarPagoCuentaPaciente.html',
              controller: 'realizarPago as ctrl'
            }
          },
          from: ['ventaDeServicios','detalleCuentaPacienteEspecifica','crearCuentaPaciente']
        })
          .state('cancelarServicios',{
            url:'/caja/cancelarServicios',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/cancelarServicios.html',
              controller: 'cancelarServicios as ctrl'
            }
            }
          })
          .state('buscarServicio',{
            url:'/caja/buscarServicio',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/buscarservicio.html',
              controller: 'buscarServicio as ctrl'
            },
          }
        })
          .state('reimprimirReciboPago',{
            url:'/caja/reImprimirReciboPago',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/buscarservicio.html',
              controller: 'buscarServicio as ctrl'
            },
          }
        })
          .state('cajaCatalogos',{
            url:'/caja/catalogos/:idCatalogo',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/catalogos/catalogogeneral.html',
              controller: 'CajaCatalogosCatalogogeneralCtrl'
            },
          }
        })

          .state('cajaCatalogosServicios',{
           url:'/caja/catalogosServicios',
           views: {
            'menuView': {
             templateUrl: '/application/views/menu.html',
             controller: 'MenuCtrl as vm'
           },
           'contentView': {
             templateUrl: '/application/views/caja/catalogos/servicios.html',
             controller: 'CajaCatalogosCatalogogeneralCtrl'
           },
         }
       })
          .state('cajaCatalogosTurnos',{
            url:'/caja/catalogosTurnos',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/catalogos/turnos.html',
              controller: 'CajaCatalogosCatalogogeneralCtrl'
            },
          }
        })
          .state('cajaCatalogosSubsecciones',{
            url:'/caja/catalogosSubsecciones',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/catalogos/subsecciones.html',
              controller: 'CajaCatalogosCatalogogeneralCtrl'
            },
          }
        })

          .state('adminCamas',{
            url:'/caja/catalgoAdminCamas',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/catalogos/camas.html',
              controller: 'CajaCatalogosCatalogogeneralCtrl'
            },
          }
        })
          .state('adminSecciones',{
            url:'/caja/catalgoAdminSecciones',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/catalogos/secciones.html',
              controller: 'CajaCatalogosCatalogogeneralCtrl'
            },
          }
        })
          .state('asignacionCama',{
            url:'/caja/asignacionCama',
            views: {
             'menuView': menuView,
              'contentView': {
                templateUrl: '/application/views/caja/asignacioncama.html',
                controller: 'CajaAsignacioncamaCtrl'
              },
            }
          })

      /*    .state('asignarCama',{
            url:'/caja/asignarCama',
            views: {
             'menuView': menuView,
              'contentView': {
                templateUrl: '/application/views/caja/asignarcama.html',
                controller: 'CajaAsignacioncamaCtrl'
              },
            }
          })*/


          .state('registraPacienteCaja',{
            url:'/caja/registraPacienteCaja',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/registrapacientecaja.html',
              controller: 'AgendaRegistrapacienteCtrl'
            },
          }
        })



           .state('reportesDeVentas',{
            url:'/caja/reportesDeVentas',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/reportes/reportesDeVentas.html',
              controller: 'reportesDeCaja as ctrl'
            },
          }
        })




        .state('reportesDeCaja',{
            url:'/caja/reportesDeCaja',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/reportes/reportesDeCaja.html',
              controller: 'reportesDeCaja as ctrl'
            },
          }
        })

          //Reporte de Concentrado de Ventas
          .state('concentradoVentas',{
            url:'/caja/reportesDeConcentradoVentas',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/reportes/reportesDeConcentradoVentas.html',
              controller: 'reportesDeCaja as ctrl'
            },
          }
        })


          //Reporte de Servicios
          .state('reportesDeServicios',{
            url:'/caja/reportesDeServicios',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/reportes/reportesDeServicios.html',
              controller: 'reportesDeCaja as ctrl'
            },
          }
        })

          //Reporte de vales de caja
          .state('reportesDeValesCaja',{
            url:'/caja/reportesDeValesCaja',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/reportes/reportesDeValesCaja.html',
              controller: 'reportesDeVentas as ctrl'
            },
          }
        })

          .state('corteDeCaja',{
            url:'/caja/corteDeCaja',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/reportes/corteDeCaja.html',
              controller: 'reportesDeCaja as ctrl'
            },
          }
        })


          .state('listaServicios',{
            url:'/caja/listaServicios',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/listaServicios.html',
              controller: 'listaServicios as ctrl'
            },
          }
        })


          .state('cajaVale',{
            url:'/caja/cajaVale',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/valecaja.html',
              controller: 'CajaValecajaCtrl'
            },
          }
        })

        .state('detalleCuentaPaciente',{
          url:'/caja/detalleCuentaPaciente',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/caja/detalleCuentasPaciente.html',
              controller: 'cuentaPacienteEspecifica as ctrl'
            },
          }
        })

        .state('causesServicios',{
          url:'/caja/causesServicios',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/caja/correspondenciacausesservicios.html',
              controller: 'CajaCorrespondenciacausesserviciosCtrl as ctrl'
            },
          }
        })

        .state('disponibilidadCamas',{
          url:'/caja/disponibilidadCamas',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/caja/disponibilidadcamas.html',
              controller: 'CajaDisponibilidadcamasCtrl as ctrl'
            },
          }
        })



        /*Consulta Médica*/
        .state('consultaSignosVitales',{
          url:'/consulta/signosVitales',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/signosvitales.html',
              controller: 'ConsultaSignosvitalesCtrl'
            },
          }
        })
        .state('consultaRecetaMedica',{
          url:'/consulta/recetaMedica',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/consulta/recetamedica.html',
              controller: ''
            },
          }
        })
        .state('reimprimirreceta',{
          url:'/reimprimirreceta',
            views: {
             'menuView': menuView,
             'contentView': {
              templateUrl: '/application/views/caja/reimprimirreceta.html',
              controller: 'CajaReimprimirrecetaCtrl'
            },
          }
        })
        .state('consultaNotaMedica',{
          url:'/consulta/notaMedica',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/notamedica.html',
              controller: 'ConsultaNotamedicaCtrl'
            }
          }
        })
        .state('consultaAgendaMedica',{
          url:'/consulta/agendaMedica/:state',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/agendamedica.html',
              controller: 'ConsultaAgendamedicaCtrl'
            },
          }
        })
        /*.state('consultaHistoriaClinica',{
          url:'/consulta/historiaClinica',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/agendamedica.html', //
              controller: 'ConsultaAgendamedicaCtrl'
            },
          }
        })*/
        .state('consultaReferencia',{
          url:'/consulta/referencia',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/referencia.html',
              controller: 'ReferenciactrlCtrl as Ctrl'
            },
          }
        })
        .state('consultaReferenciaList',{
          url:'/consulta/referenciaList',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/referencia.html',
              controller: 'ReferenciactrlCtrl as Ctrl'
            },
          }
        })
        .state('consultaContrarreferencia',{
          url:'/consulta/contrarreferencia',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/contrarreferencia.html',
              controller: 'ContrarreferenciactrlCtrl as ctrl'
            },
          }
        })
        .state('consultaCancelSAD',{
          url:'/consulta/cancelSAD',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/cancelsad.html',
              controller: 'ConsultaCancelsadCtrl'
            },
          }
        })
        .state('consultaIndicacionTerapeutica', {
          url: '/consulta/indicacionTerapeutica',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/indicacionterapeutica.html',
              controller: 'ConsultaIndicacionterapeuticaCtrl'
            }
          }
        })
        .state('detalleCuentaPacienteEspecifica',{
          url:'/caja/cuentaPaciente/detalle',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/caja/cuentaPacienteEspecifica.html',
              controller: 'cuentaPacienteEspecifica as ctrl'
            },
          }
        })

        .state('detalleConveniosServicios',{
          url:'/caja/cuentaPaciente/detalleConvenios',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl:  '/application/views/caja/detalleconvenioservicios.html',
              controller: 'CajaDetalleconvenioserviciosCtrl as ctrl'
            },
          }
        })

        .state('addCuestionario',{
          url:'/add/cuestionario',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/cuestionarios/addCuestionario.html',
              controller: 'addCuestionarioCtrl as ctrl'
            },
          }
        })
        .state('cuestionario',{
          url:'/cuestionario',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/cuestionarios/addCuestionario.html',
              controller: 'addCuestionarioCtrl as ctrl'
            },
          }
        })
        .state('modificarCuestionario',{
          url:'/modificar/cuestionario',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/cuestionarios/addCuestionario.html',
              controller: 'addCuestionarioCtrl as ctrl'
            },
          }
        })
        .state('getCuestionario',{
          url:'/list/cuestionario/:state',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/cuestionarios/getCuestionarios.html',
              controller: 'addCuestionarioCtrl as ctrl'
            },
          }
        })
        .state('contestarCuestionario',{
          url:'/contestar/cuestionario',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/cuestionarios/addCuestionario.html',
              controller: 'addCuestionarioCtrl as ctrl'
            },
          }
        })
      /***** Inician estados d ela historia clinica  **********/
        .state('historiaClinica',{
          url:'/histclinica/historiaClinica',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/histclinica/historiaclinica.html',
              controller: 'HistoriaclinicactrlCtrl as ctrl'
            }
          },
          from: ['consultaAgendaMedica','expedienteClinico','consultaNotaMedica','consultaSignosVitales','notaMedicaIngreso']
        })
         .state('historiaClinicaUrgencias',{
          url:'/histclinica/historiaClinicaUrgencias',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/histclinica/historiaClinicaUrgencias.html',
              controller: 'HistoriaclinicaUrgenciasCtrl as ctrl'
            }
          }
        })
        .state('expedienteClinico',{
          url:'/histclinica/expedienteClinico',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/histclinica/expedienteclinico.html',
              controller: 'HistclinicaExpedienteclinicoCtrl as ctrl'
            }
          }
        })
        .state('histoNotas',{
          url:'/histclinica/notasMedicas',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/histclinica/historiconotas.html',
              controller: 'HistclinicaHistoriconotasCtrl'
            }
          }
        })
      /***** Terminan estados d ela historia clinica  **********/



        //TRABAJO SOCIAL IvanHp

        // .state('trabajoSocial',{
        //   url:'/registroEstudioSocioEconomico',
        //   views: {
        //     'menuView': menuView,
        //     'contentView': {
        //       templateUrl: '/application/views/trabajosocial/registroEstudioSocioEconomico.html',
        //       controller: 'AgendaConsultapacienteCtrl as ctrl'
        //     },
        //   }
        // })


        .state('trabajosocial',{
          url:'/trabajosocial/opciones',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/trabajosocial/opcionesestudiosoceco.html',
              controller: 'TrabajosocialEstudioseCtrl'
            },
          }
        })

        .state('registroESDiagnostico',{
          url:'/trabajosocial/estudioSEDiagSoc',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/trabajosocial/estudiosediagsoc.html',
              controller: 'TrabajosocialEstudioseCtrl'
            },
          }
        })

        .state('registroESNuevo',{
          url:'/trabajosocial/estudioSE',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/trabajosocial/estudiose.html',
              controller: 'TrabajosocialEstudioseCtrl'
            },
          }
        })

        .state('trabajoSocialCuotas',{
          url:'/trabajosocial/sistemascuotas',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/trabajosocial/sistemascuotas.html',
              controller: 'TrabajosocialEstudioseCtrl'
            },
          }
        })

        .state('ministerioPublico', {
          url: '/trabajosocial/ministerioPublico',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/trabajosocial/ministeriopublico.html',
              controller: 'MinisteriopublicoCtrl'
            },
          }
        })

        .state('ministerioPublicoPendientes', {
          url: '/trabajosocial/pendientes',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/trabajosocial/pendientes.html',
              controller: 'TrabajosocialPendientesCtrl'
            },
          }
        })



                //Obtener cuestionarios dependiendo de la especialidad --Luis--

	/* URGENCIAS *************************/
	     .state('notaEgreso', {
        url: '/urgencias/notaegreso',
        views: {
          'menuView': menuView,
          'contentView': {
            templateUrl: '/application/views/urgencias/notaegreso.html',
            controller: 'UrgenciasNotaegresoCtrl'
          },
        }
      })
        .state('notaMedicaIngreso',{
          url:'/urgencias/notaMedicaIngreso',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/consulta/notamedica.html',
              controller: 'ConsultaNotamedicaCtrl'
            },
          }
        })
        .state('solicitudInterconsulta',{
          url:'/urgencias/solicitudInterconsulta',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/urgencias/interconsulta.html',
              controller: 'UrgenciasInterconsultaCtrl'
            },
          }
        })
        .state('listaPacientes',{
          url:'/urgencias/listaPacientes',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/urgencias/listapacientes.html',
              controller: 'UrgenciasListapacientesCtrl'
            },
          }
        })
        .state('interconsultas',{
          url:'/interconsultas',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/urgencias/interconsultas.html',
              controller: 'UrgenciasInterconsultasCtrl'
            },
          }
        })
         /***********************************************
        * QUIROFANO  Notas (pre-post) anestésicas y operatorias
        ***********************************************/

        .state('inicioQuirofano',{
          url:'/quirofano/inicioQuirofano',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/quirofano/inicioquirofano.html',
              controller: 'QuirofanoInicioquirofanoCtrl as ctrl'
            },
          }
        })
        .state('quirofano',{
          url:'/quirofano/quirofano',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/quirofano/quirofano.html',
              controller: 'QuirofanoQuirofanoCtrl as ctrl'
            },
          }
        })
        .state('notaPreOperatoria',{
          url:'/quirofano/notaPreOperatoria',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/quirofano/notaPreOperatoria.html',
              controller: 'notaPreOperatoriaCtrl as ctrl'
            },
          }
        })
        .state('notaPostOperatoria',{
          url:'/quirofano/notaPostOperatoria',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/quirofano/notaPostOperatoria.html',
              controller: 'notaPreOperatoriaCtrl as ctrl'
            },
          }
        })
        .state('consInfoOperatorio',{
          url:'/quirofano/consentimientoInformado/operatorio',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl: '/application/views/quirofano/conInfoOperatorio.html',
              controller: 'notaPreOperatoriaCtrl as ctrl'
            },
          }
        })
        .state('notaPostAnestesica', {
         url: '/quirofano/notaPostAnestesica',
          views: {
           'menuView': menuView,
           'contentView': {
             templateUrl: '/application/views/quirofano/notapostanestesicactrl.html',
             controller: 'NotapostanestesicactrlCtrl as ctrl'
           },
          }
        })
        .state('notaPreAnestesica',{
            url:'/quirofano/notapreanestesica/notaPreAnestesica',
            views: {
              'menuView': menuView,
              'contentView': {
                templateUrl: '/application/views/quirofano/notapreanestesica/notapreanestesica.html',
                controller: 'QuirofanoNotapreanestesicaCtrl as ctrl'
              },
            }
        })
       .state('cirugiaSegura',{
            url:'/quirofano/cirugiaSegura',
            views: {
              'menuView': menuView,
              'contentView': {
                templateUrl: '/application/views/quirofano/cirugiasegura.html',
                controller: 'QuirofanoCirugiaseguraCtrl as ctrl'
              },
            }
        })


        /**********************************************
        * RIESGO OBSTETRICO
        ********************************************** */
        .state('riesgoObstetrico', {
         url: '/consulta/califRiesgoObs',
         views: {
           'menuView': menuView,
           'contentView': {
             templateUrl: '/application/views/consulta/califriesgoobst.html',
             controller: 'ConsultaCalifRiesgoObstCtrl'
           },
         }
       })

        /**********************************************
        * CANCELACIÓN DE RECETA MEDICA
        ***********************************************/
        .state('cancelarReceta', {
         url: '/consulta/cancelarReceta',
         views: {
           'menuView': menuView,
           'contentView': {
             templateUrl: '/application/views/consulta/cancelarreceta.html',
             controller: 'ConsultaCancelarRecetaCtrl'
           },
         }
       })

        /**********************************************
        * ESTUDIOS DE LABORATORIO
        ***********************************************/
        .state('servAuxiliares', {
         url: '/servauxiliares/servAuxiliares',
         views: {
           'menuView': menuView,
           'contentView': {
             templateUrl: '/application/views/servauxydiag/estdelabygab.html',
             controller: 'ServauxydiagEstdelabygabCtrl'
           },
         }
       })




        /***********************************************
        * URGENCIAS
        ***********************************************/


        .state('triage', {
               url: '/urgencias/triage',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/urgencias/triage.html',
                   controller: 'UrgenciasTriageCtrl'
                 },
               }
             })

        /************************************************
        * HOSPITALIZACIÓN
        *************************************************/

        .state('rtus', {
               url: '/hospitalizacion/rtusangre',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/hospitalizacion/rtusangre.html',
                   controller: 'HospitalizacionRtusangreCtrl'
                 },
               }
             })

        .state('hospitalizacionConsultaPaciente',{
        url:'/hospitalizacion/consultaPaciente',
        views: {
           'menuView': menuView,
           'contentView': {
              templateUrl: '/application/views/hospitalizacion/consultapaciente.html',
              controller: 'HospitalizacionConsultapacienteCtrl'
            },
          }
        })

        .state('hojaDeEnfermeria',{
        url:'/enfermeria/hojaDeEnfermeria',
        views: {
           'menuView': menuView,
           'contentView': {
              templateUrl: '/application/views/enfermeria/hojaEnfermeria.html',
              controller: 'hojaEnfermeriaCtrl as ctrl'
            },
          }
        })
        .state('solicitudLentes', {
          url: '/consulta/solicitudLentes',
          views: {
            'menuView': menuView,
            'contentView': {
              templateUrl : '/application/views/consulta/solicitudlentes.html',
              controller: 'ConsultaSolicitudlentesCtrl'
            }
          }
        })

        //GENERAR REQUISICION
       .state('requisicionesCtrl', {
             url: '/paciente/requisicion',
             views: {
               'menuView': menuView,
               'contentView': {
                 templateUrl: '/application/views/hospitalizacion/requisiciones.html',
                 controller: 'requisicionesCtrl as ctrl'
               },
             }
           })

        //GENERAR REQUISICIÓN
        .state('generarRequisicion', {
               url: '/hospitalizacion/generarRequisiciones',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/hospitalizacion/generarrequisicionhospitalizacion.html',
                   controller: 'HospitalizacionGenerarrequisicionhospitalizacionCtrl'
                 },
               }
            })
         //CARGAR A CUENTA PACIENTE
        .state('cargoCuentaPacienteHospitalizacion', {
               url: '/hospitalizacion/cargarCuentaPaciente',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/hospitalizacion/generarrequisicionhospitalizacion.html',
                   controller: 'HospitalizacionGenerarrequisicionhospitalizacionCtrl'
                 },
               }
            })




        /***********************************************
        * Reacciones Adversas a Medicamentos
        ***********************************************/

        .state('reaccionesAdversasMedicamentos', {
               url: '/consulta/reaccionesAdversasMedicamentos',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/consulta/reacadversamedicamentos.html',
                   controller: 'ConsultaReacAdversAMedicamentosCtrl'
                 },
               }
             })

        /***********************************************
        * Incapacidad Temporal
        ***********************************************/

        .state('incapacidadTemporal', {
               url: '/consulta/incapacidadTemporal',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/consulta/incapacidadtemporal.html',
                   controller: 'ConsultaIncapacidadTemporalCtrl'
                 },
               }
             })


        /***********************************************
        *ADQUISICIONES
        ***********************************************/
        //CATÁLOGOS
        //articulos
         .state('articulos', {
               url: '/adquisiciones/catalogos/articulos',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/catalogos/articulos.html',
                   controller: 'AdquisicionesCatalogosCtrl'
                 },
               }
             })

        //grupos
        .state('grupos', {
               url: '/adquisiciones/catalogos/grupos',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/catalogos/grupos.html',
                   controller: 'AdquisicionesCatalogosCtrl'
                 },
               }
             })


         //proveedores
         .state('proveedores', {
               url: '/adquisiciones/catalogos/proveedores',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/catalogos/proveedores.html',
                   controller: 'AdquisicionesCatalogosCtrl'
                 },
               }
             })

         //departamentos
         .state('departamentos', {
               url: '/adquisiciones/catalogos/departamentos',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/catalogos/departamentos.html',
                   controller: 'AdquisicionesCatalogosCtrl'
                 },
               }
             })

          //cuentas
         .state('cuentas', {
               url: '/adquisiciones/catalogos/cuentas',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/catalogos/cuentas.html',
                   controller: 'AdquisicionesCatalogosCtrl'
                 },
               }
             })

         .state('unidadesDeMedida', {
               url: '/adquisiciones/catalogos/:idCatalogo',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/catalogogeneral.html',
                   controller: 'AdquisicionesCatalogogeneralCtrl'
                 },
               }
             })

        //entradas
         .state('entradas', {
               url: '/adquisiciones/entradas',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/entradas.html',
                   controller: 'AdquisicionesEntradasCtrl'
                 },
               }
             })

         //salidas
         .state('salidas', {
               url: '/adquisiciones/salidas',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/salidas.html',
                   controller: 'AdquisicionesSalidasCtrl'
                 },
               }
             })

         //vales
         .state('vales', {
               url: '/adquisiciones/vales/:tipoSolicitud',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/vales.html',
                   controller: 'AdquisicionesValesCtrl'
                 },
               }
             })

         //requisiciones
         .state('requisicionesAlm', {
               url: '/adquisiciones/requisiciones',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/requisiciones.html',
                   controller: 'AdquisicionesRequisicionesCtrl'
                 },
               }
             })

           //cargo a cuenta paciente
         .state('cargoCuentaPaciente', {
               url: '/adquisiciones/cuentapaciente/:idProcedencia',
               views: {
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/adquisiciones/cuentapaciente.html',
                   controller: 'AdquisicionesCuentapacienteCtrl'
                 },
               }
             })


              //cargo a cuenta paciente
         .state('prueba', {
               url: '/prueba/prueba/',
               views:{
                 'menuView': menuView,
                 'contentView': {
                   templateUrl: '/application/views/prueba/prueba.html',
                   controller: 'PruebaPruebaCtrl'
                 },
               }
             })


    }]);
