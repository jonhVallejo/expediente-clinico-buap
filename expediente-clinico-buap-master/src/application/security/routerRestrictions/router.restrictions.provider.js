angular.module('expediente').run(function ($rootScope, $state, usuario, menu, $localStorage, $location) {
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){


    $localStorage.stateAnterior = (fromState.name == "") ? $localStorage.stateAnterior : fromState.name;
    if(toState.from){
      if(toState.from.indexOf(fromState.name) == -1){
        if(fromState.name == ""){
          $state.transitionTo("bienvenido");
          //$state.transitionTo(fromState.name); //POR EL MOMENTO
        }else{
            // if($localStorage.paciente.cuentaPaciente){
            //   ///delete $localStorage.paciente.cuentaPaciente;
            // }
            $state.transitionTo(fromState.name);
        }
        event.preventDefault();
      }
    }

    if(toState.name != 'home'){
        $('html, body').css('font-size', '100%');
    }
    else{
        $('html, body').css('font-size', '69.5%');
    }

    console.debug("Me encuentro en :", $location.$$path, " **** ", $localStorage);
    console.debug("Quiero ir a :", toState.name);

  var encontrado = undefined;
  encontrado  = menu.hashValues[$location.$$path];
  if(encontrado == undefined){
      encontrado  = menu.hashValues[toState.name];
  }

  if(encontrado == undefined){
    var direccion = toState.url.split("/");
    for (var k in menu.hashValues){
      if (menu.hashValues.hasOwnProperty(k)) {
          var aux = k.toString().split("/");
           if(aux[aux.length-1].indexOf(direccion[direccion.length-1]) != -1){
                encontrado = "Petición externa";
           }
        }
      }
  }

  if(menu.sections.length){
    if(toState.name != "home" & toState.name != "bienvenido" & toState.name != undefined  & toState.name != "perfil" ){
      if(encontrado == undefined){
         // $state.transitionTo("bienvenido");
         // event.preventDefault();
      }
    }
  }

  menu.current = toState;
  if(toState.name !== 'home'){
      if ($localStorage.userSecurityInformation === undefined){

        // $state.transitionTo("home");
        // event.preventDefault();
      }
    }else{
      if($localStorage.userSecurityInformation !== undefined){

        $state.transitionTo("perfil");
        event.preventDefault();
      }
    }


  });
});
