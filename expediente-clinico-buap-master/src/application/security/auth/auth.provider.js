'use-strict'

angular.module('expediente')
.factory('LoginServerProvider', function ($http, $localStorage, CLIENT_ID, CLIENT_SECRET){

	return {
		login: function(credentials){


			var loginURL = baseURL + 'oauth/token?grant_type=password';

			loginURL += '&client_id=';
			loginURL += CLIENT_ID;
			loginURL += '&client_secret=';
			loginURL += CLIENT_SECRET;
			loginURL += '&username=';
			loginURL += credentials.name;
			loginURL += '&password=';
			loginURL += credentials.password;

			return $http.get(loginURL).
			success(function(data){

				$localStorage.userSecurityInformation = {};

				$localStorage.userSecurityInformation.token = data;



				return data.additionalInformation;
			});

		},

		logout: function(){

			// Poner peticion al servidor para desligar el token actual
			//$http.post('oauth/logout')
			var logoutURL = baseURL + 'oauth/logout'
			return $http.get(logoutURL).success(function(data){


				$localStorage.userSecurityInformation = undefined;

				return data;
			});

		},

		refreshSession: function(){
			if($localStorage.userSecurityInformation.token.refreshToken.expiration > new Date().getTime()){

				$http.get(baseURL + 'oauth/token?grant_type=refresh_token&client_id=' + CLIENT_ID + '&client_secret=' +
				CLIENT_SECRET + '&refresh_token=' + $localStorage.userSecurityInformation.token.refreshToken.value).success(function(data){

					$localStorage.userSecurityInformation.token = data;

				}).error(function(err){
					$localStorage.$reset();
					window.location = '/';
				});
			}else{
				$localStorage.$reset();
				window.location = '/';
			}


		}
	};
});
