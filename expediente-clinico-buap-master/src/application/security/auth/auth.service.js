
angular.module('expediente').factory('Auth', function Auth(LoginServerProvider, $q){

	return {
		login: function(credentials){

			var deferred = $q.defer();
			var promise = deferred.promise;

			LoginServerProvider.login(credentials).
			then(function(data){

				deferred.resolve(data.data.additionalInformation);
			}).
			catch(function(err){
				var message
				switch(err.status){
					case 404:
					message = 'SERVICIO NO DISPONIBLE, CONTACTE AL ADMINISTRADOR';
					break;
					case 401:
					message = 'USUARIO O CONTRASEÑA INCORRECTOS';
					break;
					default:
					message = 'ERROR INTERNO DEL SERVIDOR, POR FAVOR CONTACTE AL ADMINISTRADOR.';
					break;
				}
				deferred.reject(message);
			});
			return promise;
		},

		logout: function(){

			LoginServerProvider.logout();

		},

		authorize: function(){

			
			
			LoginServerProvider.refreshSession();

		}

	}
});
