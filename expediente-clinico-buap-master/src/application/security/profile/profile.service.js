'use-strict'

angular.module('expediente').factory('ProfileService', function($localStorage, $http, menu){

	console.log('reading profile service..');

	var self = this;

	self.profiles = [];
	self.currentProfile = {};

	return {
		getCurrentProfile: function(){
			return self.currentProfile;
		},
		setCurrentProfile: function(profile){
			self.currentProfile = profile;

			menu.setMenus(self.currentProfile.menu);
		},
		getCurrentMenu: function(){
			return self.currentProfile.menu;
		},
		getProfileList: function(){
			return self.profiles;
		},
		clearCurrentProfile: function(){
			self.currentProfile = {};
		},
		clearCacheProfiles: function(){
			$localStorage.profiles = undefined;
			self.currentProfile = null;
			self.profiles = null;
		},
		setProfiles: function(profiles){
			$localStorage.profiles = profiles;
			self.profiles = profiles;
		},
		retrieveProfiles: function(user){
			if($localStorage.profiles){
				self.profiles = $localStorage.profiles;
			}
			return angular.isDefined($localStorage.profiles);
		}
	};
});
