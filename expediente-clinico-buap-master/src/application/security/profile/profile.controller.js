'use strict';

/**
* @ngdoc function
* @name ECEangular.module('expediente').controller:PerfilCtrl
* @description
* # PerfilCtrl
* Controller of the ECEApp
*/
angular.module('expediente').controller('PerfilCtrl', function ($http,$scope, $state,usuario, menu, mensajes, $timeout, blockUI)
{


  $scope.perfiles = [];
  $scope.perfil = {};
  $scope.loading = true;
  blockUI.start();

  $scope.ingresar = function(){
    usuario.modificaPerfil($scope.perfil);



    menu.setMenus(usuario.perfil.menus);


    $state.go('bienvenido');
  }

  $scope.select = function( itemSelecionado ){
      $scope.perfil = itemSelecionado;
      $scope.ingresar();
  }

  // $scope.$watch(function(){
  //   console.log($scope.perfil);
  // });

  this.selectedMode = 'md-flin';

  usuario.obtenPerfiles()
  .then(function(data){


    if(usuario.perfiles.length == 1){
      $timeout(function(){


        usuario.modificaPerfil(usuario.perfiles[0]);

        menu.setMenus(usuario.perfil.menus);
        blockUI.stop();
        $state.go('bienvenido');
        $scope.loading = false;
      }, 1000);
    }else{
      $scope.perfiles = usuario.perfiles;

      blockUI.stop();
      $scope.loading = false;
    }

  })
  .catch(function(err){


    blockUI.stop();
    mensajes.alerta('ERORR', 'NO CUENTA CON PRIVILEGIOS, CONTACTE AL ADMININSTRADOR.', 'ACEPTAR').then(function(){

      usuario.removeLoginInformation();

      $state.go('home');

    });
  })



});
