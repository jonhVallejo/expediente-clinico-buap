'use strict';

/**
 * @ngdoc service
 * @name expediente.login
 * @description
 * # login
 * Service in the expediente.
 *
 * Este servicio usa una implementación para auth, de forma que si se cambia de metodo de autentificación este lo va a controlar
 */
 angular.module('expediente').service('login',[ 'usuario', '$q', '$http', '$localStorage', 'Auth', '$location', function (usuario, $q, $http, $localStorage, Auth, $location){
  var loginURL = '/login';
  var logoutURL = '/logout';



  this.login = function(userData){

    var defered = $q.defer();
    var promise = defered.promise;

    Auth.login(userData).then(function(res){
      defered.resolve(res);
    }).catch(function(err){
      defered.reject({result: err});
    });


   return promise;

 };


 this.verifyCredentials = function(userData){
  var defered = $q.defer();
  var promise = defered.promise;


  $http.post(baseURL + loginURL, {
    'C_EMAIL': userData.name,
    'C_PASSWORD': userData.password
  }).success(function(data){
    if(data.NP_EXPEDIENTE){

      defered.resolve('USUARIO CON PRIVILEGIOS');

    }else{
     defered.reject('DATOS INCORRECTOS, VERIFIQUE SU INFORMACIÓN');

   }


 }).error(function(err){


  defered.reject('SERVICIO NO DISPONIBLE, CONTACTE AL ADMINISTRADOR');
});

 return promise;


}




this.logout = function(){

  // Limpia toda la información del storage.
  Auth.logout();
  $location.path('/');
  $localStorage.$reset();

  // window.location = '/';

};

  }]);
