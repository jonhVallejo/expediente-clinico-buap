'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the expediente
 */
 angular.module('expediente').controller('LoginCtrl', ['$scope', '$state', 'login', '$mdDialog', 'usuario',
 	function ($scope, $state, login, $mdDialog, usuario) {


 		$scope.user = {
 			recordar: true,
 			name: '',
 			password: ''
 		};

 		$scope.iniciarSesion = function(){
 			login.login($scope.user)
 			.then(function(data){
 				//$('html, body').css('font-size', '100%');
		        usuario.recordar($scope.user.recordar);
		        usuario.modificaUsuario(data);
				$state.transitionTo("perfil");
 			}).catch(function(err){
 				$mdDialog.show(
 					$mdDialog.alert()
 					.clickOutsideToClose(true)
 					.title('ERROR')
 					.content(err.result)
 					.ok('ACEPTAR!')
 				);
 			});
 		};
 	}]);
