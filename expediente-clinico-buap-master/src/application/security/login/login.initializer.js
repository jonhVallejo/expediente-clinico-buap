'use-strict'


///// Perfil is not defined.

angular.module('expediente').run(function(usuario, menu, $state){

  if(usuario.recordado()){

    if(usuario.perfil === null || usuario.perfil === undefined){
      menu.setMenus([]);
    }else{
      menu.setMenus(usuario.perfil.menus);
    }
  }else{
      usuario.removeLoginInformation();
      $state.go('home');
  }
});
