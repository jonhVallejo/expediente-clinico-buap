'use-strict'

angular.module('expediente').config(function($httpProvider){
	$httpProvider.interceptors.push('authInterceptor');
	$httpProvider.interceptors.push('authExpiredInterceptor');

});

angular.module('expediente').factory('authInterceptor', function($localStorage){

	var interceptor = {

		request: function(config){
			if($localStorage.userSecurityInformation && $localStorage.userSecurityInformation.token.expiration > new Date().getTime()){

				config.headers.Authorization= 'Bearer ' + $localStorage.userSecurityInformation.token.value;

			}

			return config;
		}
	};
	return interceptor;
});

angular.module('expediente').factory('authExpiredInterceptor', function ($rootScope, $q, $injector, $localStorage) {
	return {
		responseError: function (response) {

			// token has expired
			// if(response.status === 401){
			//
			//
			// 	//  mensajes.alerta('ALERTA', 'SU SESIÓN A CADUCADO, VUELVA A INICIAR SESIÓN', 'ACEPTAR');
			// 	$localStorage.$reset();
			// 	window.location = '/';
			// }
			if (response.status === 401){// && (response.data.error == 'invalid_token' || response.data.error == 'Unauthorized')) {

				//  localStorageService.remove('token');
				//  var Principal = $injector.get('Principal');
				//  if (Principal.isAuthenticated()) {

				// try{
				//
					var Auth = $injector.get('Auth');
					Auth.authorize();
				// }catch(ex){
				// 	console.log('erorrrrrr');
				// }


			// }
		}
		return $q.reject(response);
	}
};
});
