'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ReactivarcitasCtrl
 * @description
 * # ReactivarcitasCtrl
 * Controller of the expediente
 */
  angular.module('expediente').controller('ReactivarcitasCtrl', function ($scope, $q, $timeout, $http, $mdDialog, medicos, mensajes, $mdToast, $state) {

    $scope.selected = [];
 	$scope.medicos = [];
 	$scope.citas = [];
 	$scope.cita ={};
 	$scope.tablaAgenda = false;

 	$scope.especialidades = [];

 	$scope.minDate = moment().subtract(1, 'days').toDate();


 	$scope.query = {
 		filter: '',
 		order: 'fecha',
 		limit: 5,
 		page: 1
 	};	

 	medicos.getEspecialidades().success(function(data){
 		$scope.especialidades = data;
 	}).error(function(err){
 		mensajes.alerta('AVISO', 'NO SE ENCUENTRAN DISPONIBLES LAS CATEGORIAS', 'ACEPTAR');
 	});	

    $scope.obtenCanceladas = function(){
 		medicos.getCanceladas(moment($scope.cita.fechaInicial).format('DDMMYYYY'), 
 			moment($scope.cita.fechaFinal).format('DDMMYYYY'), $scope.cita.especialidad, $scope.cita.medico)
 		.success(function(data){
 			if(data.length == 0){
 				mensajes.alerta('AVISO', 'NO HAY DATOS EN ESTE RANGO DE FECHAS', 'ACEPTAR');
 			}
 			else{
 				var hora1 = moment();
 				console.log(data);
 				data = _.filter(data,function(att){
 					var hora = moment(att.F_FECHA_HORA,'DD/MM/YYYY HH:mm:ss' );
 					console.log(hora);
 					return hora.diff(hora1)>0;
 				});
 				var grouped = _.groupBy(data, function(el){
 					return el.CF_MEDICO + '#' + moment(el.F_FECHA_HORA, 'DD/MM/YYYY').format('DD/MM/YYYY') + '#' + el.NF_ESTATUS;
 				})

 				$scope.citas = _.map(grouped, function(el, key){
 					return{
 						'medico': key.split('#')[0],
 						'fecha': key.split('#')[1],
 						'estatus':key.split('#')[2],
 						'citas': el

 					}

 				});

 			}

 		}).error(function(err){
 		});


 		$scope.tablaAgenda = true;
 	};	

 	$scope.obtenMedicos = function(){
	medicos.getMedicos($scope.cita.especialidad.ID) 
	.success(function(data){
		$scope.medicos = data;
	}).error(function(err){ mensajes.alerta('AVISO', 'NO SE ENCUENTRA DISPONIBLE LA LISTA DE DOCTORES', 'ACEPTAR');	});
	}

	$scope.clearMedicos = function(){
		$scope.medicos = [];
	};

	$scope.reactivarCanceladas = function(days){
	var temp = [];
	temp = _.map(days, function(el){ return el.citas;});
	temp = _.flatten(temp);

	var result = $scope.reactivar(temp);
	$scope.citas  = _.difference($scope.citas, days);
	}

	$scope.reactivar = function(registros){ 
	  var successRegistros = [];
	  var result = false;

	  var ids = _.map(registros, function(el){ return el.NP_ID; });
	  ids = _.reduce(ids, function(m,p) { return m + ',' + p});
	  console.log(ids);

	    $http({
	      url : baseURL + 'agenda',
	      method : 'PUT',
	      data: {
	        'id_cita': '' + ids,
	      },
	      dataType : 'json',
	      headers:{
	        'Content-Type': 'application/json',
	      }
	    }).success(function(data){
	      $mdToast.show(
	      $mdToast.simple()
	      .content("LA(S) CITA HA SIDO REACTIVADA")
	      .position('top left')
	      .hideDelay(3000)
	      );
	      result = true;
	    }).error(function(err){
	      $mdToast.show(
	      $mdToast.simple()
	      .content('ERROR')
	      .position('top')
	      .hideDelay(3000)
	      );
	    });

  	return result;

  	};


 });
