(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaAdministracionusuariosCtrl
 * @description
 * # AgendaAdministracionusuariosCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaAdministracionusuariosCtrl', ['$scope','pacientes','mensajes','usuario','$localStorage','$mdDialog','$q','candado',
	function ($scope,pacientes,mensajes,usuario,$localStorage,$mdDialog,$q,candado) {

	$scope.datos =[];
	$scope.editar = false;
	$scope.alta = false;
	$scope.busqueda = true;
	$scope.nuevoUsuario=false;
	$scope.usuario={isNuevo:'NO'};
	$scope.paciente = {};
	$scope.folio=usuario.usuario.NP_EXPEDIENTE;
	var isValid=false;
	$scope.canRecord = false;

	$scope.selected = [];
  	$scope.query = {
  		filter: '',
    	order: 'nombre',
    	limit: 15,
    	page: 1
  	};

  	$scope.click = function(){
  		$scope.alta = true;
	  	$scope.busqueda = false;
	}

	var self = this;
	self.simulateQuery = false;
	self.isDisabled    = false;
	self.querySearch   = querySearch;
	self.selectedItemChange = selectedItemChange;
	var textoBuscado='';

	candado.isLock()
	.success(function(data){
		$scope.isLock = (data.Mensaje ==='1') ? true : false ;
	})
	.error(function(data){

	});


	function querySearch(query) {
	  textoBuscado = query;
	  var defered = $q.defer();
	  var promise = defered.promise;
	    if(textoBuscado.indexOf('/') != -1 || textoBuscado.indexOf('-') != -1){
	      textoBuscado= textoBuscado.replace("/","");
	      textoBuscado= textoBuscado.replace("-","");
	    }
	    pacientes.getPacientes(textoBuscado)
	    .success(function(data){
	      $scope.isDisabled = false;
	      $scope.nombres = cargarServicios(data.data);
	      defered.resolve($scope.nombres);
	    })
	    .error(function(data){
	      mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
	      defered.reject({});
	    });
	    return promise;
	 }
  
  	function selectedItemChange(item) {
	    if(item!==undefined){
	      textoBuscado=item.id;
	      $scope.usuario= {
	      	IDPACIENTE: textoBuscado
	      }
	      isValid=true;
	    }
	}

  	function createFilterFor(query) {
	    var consulta = angular.uppercase(query);
	    return function filterFn(state) {
	      return (state.value.search(consulta) !== -1);
	    };
  	};
  	
  	function getServicios(query){
	    pacientes.getPacientes(query)
	    .success(function(data){
	      self.isDisabled = false;
	      self.nombres = cargarServicios(data.data);
	    })
	    .error(function(data){
	      mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
	    });
  	};

  	function cargarServicios(data){
	    if(data!==undefined){
	      return data.map(function (serv){
	        return {
	          value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
	          display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
	          nombre: serv.C_NOMBRE,
	          id  : serv.NP_EXPEDIENTE
	        };
	      });
	    }
  	};

  	$scope.loadPerfiles = function() {
		$scope.catPerfiles = [];
	    usuario.getPerfiles().success(function(data){
			$scope.catPerfiles=_.sortBy(data,'C_DESCRIPCION');
		});
	};

	$scope.addRol = function(){
		if(!isValid){
			mensajes.alerta('ERROR','INGRESA UN USUARIO VÁLIDO','ACEPTAR!');
		}
		else if($scope.usuario.PASSWORD!==$scope.usuario.PASSWORDC){
			mensajes.alerta('ERROR','LAS CONTRASEÑAS NO COINCIDEN','ACEPTAR!');
		}
		else{
			$scope.usuario.IDPACIENTE=$scope.usuario.IDPACIENTE.replace("/","")
			usuario.setPerfil($scope.usuario)
			.success(function(data){
				if(data.success){
					mensajes.alerta('AVISO','EL PERFIL SE HA CREADO CORRECTAMENTE','ACEPTAR!');
					$scope.editar = false;
					$scope.alta = false;
					$scope.busqueda = true;
					$scope.loadTable();
					$scope.usuario = {};
					$scope.ctrlPac.searchText = "";
				}
				else if(!data.success){
					mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
				}
				else{
					mensajes.alerta('AVISO','OCURRIÓ UN ERROR AL ASIGNAR EL PERFIL','ACEPTAR!');
				}
			})
			.error(function(data){
				mensajes.alerta('AVISO','OCURRIÓ UN ERROR AL ASIGNAR EL PERFIL','ACEPTAR!');
			});
		}
	};

	$scope.loadTable = function(){
		usuario.getUsuarios()
		.success(function(data){
			$scope.datos=_.filter(data,function(e){
				return e!==null;
			});
			$scope.datos = _.map($scope.datos, function (el, key) {
	          el.nombreCompleto = el.C_PRIMER_APELLIDO +" " + el.C_SEGUNDO_APELLIDO + " " + el.C_NOMBRE;
	          return el;
	        });
	        $scope.registros = $scope.datos;
			
		})
		.error(function(data){
			mensajes.alerta('AVISO','NO SE PUDO CARGAR LA INFORMACIÓN','ACEPTAR!');
		});
	};

	
	$scope.bajaUsuario = function(dato){
		var confirm = $mdDialog.confirm()
				    .title('AVISO')
				    .content('DESEAS DAR DE BAJA A ESTE USUARIO?')
					.ariaLabel('Lucky day')
				    .targetEvent()
				    .ok('ACEPTAR!')
				    .cancel('CANCELAR');
		$mdDialog.show(confirm).then(function() {
			$scope.editar = false;
			dato.NF_USUARIO=parseInt(usuario.usuario.NP_EXPEDIENTE);
			usuario.bajaUsuario(dato)
			.success(function(data){
				if(data.success){
					mensajes.alerta('AVISO','EL USUARIO SE HA DADO DE BAJA CORRECTAMENTE','ACEPTAR!');
					$scope.loadTable();
				}
				else if(!data.success){
					mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
				}
				else{
					mensajes.alerta('AVISO','NO SE PUDO DAR DE BAJA AL USUARIO','ACEPTAR!');
				}
			})
			.error(function(data){
				mensajes.alerta('AVISO','NO SE PUDO DAR DE BAJA AL USUARIO','ACEPTAR!');
			});
		}, function() {
		});
	};

    $scope.editRow = function(datos){
		$localStorage.paciente={
			expediente: datos.N_EXPEDIENTE_ANIO.replace("/","")
		};
		pacientes.getPaciente()
		.success(function(data){
			if(data.success){
				$scope.paciente=data.data[0];
				$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO  = ($scope.paciente.NF_ENTIDAD_DE_NACIMIENTO_ID=== undefined) ? undefined : $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO_ID;
				$scope.paciente.NF_ENTIDAD_FEDERATIVA = ($scope.paciente.NF_ENTIDAD_FEDERATIVA_ID === undefined) ? undefined : $scope.paciente.NF_ENTIDAD_FEDERATIVA_ID;
				$scope.paciente.NF_ESTADO_CIVIL = ($scope.paciente.NF_ESTADO_CIVIL_ID === undefined) ? undefined : $scope.paciente.NF_ESTADO_CIVIL_ID;
				$scope.paciente.NF_SEXO = ($scope.paciente.NF_SEXO_ID === undefined) ? undefined : $scope.paciente.NF_SEXO_ID;
				if($scope.paciente.F_FECHA_DE_NACIMIENTO != undefined){
					$scope.paciente.FECHA_NACIMIENTO = new Date($scope.paciente.F_FECHA_DE_NACIMIENTO.split("/")[2], 
						(($scope.paciente.F_FECHA_DE_NACIMIENTO.split("/")[1]) - 1) ,
						$scope.paciente.F_FECHA_DE_NACIMIENTO.split("/")[0]);
					$scope.calcularEdad();
				}
				$scope.editar = true;
				$scope.busqueda = false;
				$scope.alta = false;
			}
			else{
				mensajes.alerta('ERROR',data.Mensaje.toUpperCase(),'ACEPTAR!');
				$scope.cancelar();
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
			$scope.cancelar();
		});          
    }; 

    $scope.cancelar = function(){
    	$scope.editar = false;
		$scope.alta = false;
		$scope.busqueda = true;
		isValid=false;
		$scope.nuevoUsuario=false;
    };

    $scope.loadCatalogos = function() {
	    $scope.catEstadoCivil = [];
	    pacientes.getCatalogo("9").success(function(data){
			$scope.catEstadoCivil=data;
		});
		$scope.catSexo = [];
	    pacientes.getCatalogo("12").success(function(data){
			$scope.catSexo=data;
		});
		$scope.catEstado = [];
	    pacientes.getCatalogo("7").success(function(data){
			$scope.catEstado=data;
		});
	};

	$scope.actualizarUsuario = function(){
		if($scope.usuario.isNuevo=='SI'){
			if($scope.usuario.PASSWORD!==$scope.usuario.PASSWORDC){
				mensajes.alerta('ERROR','LAS CONTRASEÑAS NO COINCIDEN','ACEPTAR!');
			}
			else{
				$scope.usuario.IDPERSONAL= $scope.paciente.NP_EXPEDIENTE.replace("/","");
				usuario.actualizaPassword($scope.usuario)
				.success(function(data){
					if(!data.success){
						mensajes.alerta('AVISO','NO SE PUDO ACTUALIZAR LA CONTRASEÑA','ACEPTAR!');	
					}
					else if(data.success){
						$scope.paciente.NP_EXPEDIENTE=$scope.paciente.NP_EXPEDIENTE.replace("/","");
						if($scope.paciente.NF_ESTADO_CIVIL != undefined){
							var estadoCivil = _.filter($scope.catEstadoCivil,function(e){
							    return e.ID+''===$scope.paciente.NF_ESTADO_CIVIL+'';
							});
							$scope.paciente.NF_ESTADO_CIVIL = estadoCivil[0].ID;
						}
						
						if($scope.paciente.NF_ENTIDAD_FEDERATIVA != undefined){
							var entidadFederativa =_.filter($scope.catEstado,function(e){
							    return e.ID+''===$scope.paciente.NF_ENTIDAD_FEDERATIVA+'';
							});
							$scope.paciente.NF_ENTIDAD_FEDERATIVA = entidadFederativa[0].ID;
						}

						if($scope.paciente.NF_ENTIDAD_DE_NACIMIENTO != undefined){
							var entidadNacimiento = _.filter($scope.catEstado,function(e){
							    return e.ID+''===$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO+'';
							});
							$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = entidadNacimiento[0].ID;
						}

						if($scope.paciente.NF_SEXO != undefined){
							var sexo = _.filter($scope.catSexo,function(e){
							    return e.ID+''===$scope.paciente.NF_SEXO+'';
							});
							$scope.paciente.NF_SEXO = sexo[0].ID;
						}
						if($scope.paciente.FECHA_NACIMIENTO != undefined){
							$scope.paciente.F_FECHA_DE_NACIMIENTO = moment($scope.paciente.FECHA_NACIMIENTO).format("DD/MM/YYYY");
						}
						$scope.paciente.NF_USUARIO=parseInt(usuario.usuario.NP_EXPEDIENTE);
						pacientes.actualizaPaciente($scope.paciente)
						.success(function(data){
							if(data.success){
								mensajes.alerta('AVISO','EL USUARIO SE HA ACTUALIZADO CORRECTAMENTE','ACEPTAR!');
								$scope.editar = false;
								$scope.busqueda=true;
								$scope.loadTable();
							}
							else if(!data.success){
								mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');	
							}
							else{
								mensajes.alerta('ERROR','LO SENTIMOS HA OCURRIDO UN ERROR INESPERADO, INTENTE MÁS TARDE','ACEPTAR!');
							}
						})
						.error(function(data){
							mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
						});
					}
					else{
						mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');	
					}
				})
				.error(function(data){
					mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
				})
			}
		}
		else{
			$scope.paciente.NP_EXPEDIENTE=$scope.paciente.NP_EXPEDIENTE.replace("/","");
			if($scope.paciente.NF_ESTADO_CIVIL != undefined){
				var estadoCivil = _.filter($scope.catEstadoCivil,function(e){
				    return e.ID+''===$scope.paciente.NF_ESTADO_CIVIL+'';
				});
				$scope.paciente.NF_ESTADO_CIVIL = estadoCivil[0].ID;
			}
			
			if($scope.paciente.NF_ENTIDAD_FEDERATIVA != undefined){
				var entidadFederativa =_.filter($scope.catEstado,function(e){
				    return e.ID+''===$scope.paciente.NF_ENTIDAD_FEDERATIVA+'';
				});
				$scope.paciente.NF_ENTIDAD_FEDERATIVA = entidadFederativa[0].ID;
			}

			if($scope.paciente.NF_ENTIDAD_DE_NACIMIENTO != undefined){
				var entidadNacimiento = _.filter($scope.catEstado,function(e){
				    return e.ID+''===$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO+'';
				});
				$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = entidadNacimiento[0].ID;
			}

			if($scope.paciente.NF_SEXO != undefined){
				var sexo = _.filter($scope.catSexo,function(e){
				    return e.ID+''===$scope.paciente.NF_SEXO+'';
				});
				$scope.paciente.NF_SEXO = sexo[0].ID;
			}
			if($scope.paciente.FECHA_NACIMIENTO != undefined){
				$scope.paciente.F_FECHA_DE_NACIMIENTO = moment($scope.paciente.FECHA_NACIMIENTO).format("DD/MM/YYYY");
			}
			pacientes.actualizaPaciente($scope.paciente)
			.success(function(data){
				if(data.success){
					mensajes.alerta('AVISO','EL USUARIO SE HA ACTUALIZADO CORRECTAMENTE','ACEPTAR!');
					$scope.editar = false;
					$scope.busqueda=true;
					$scope.loadTable();
				}
				else if(!data.success){
					mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');	
				}
				else{
					mensajes.alerta('ERROR','LO SENTIMOS HA OCURRIDO UN ERROR INESPERADO, INTENTE MÁS TARDE','ACEPTAR!');
				}
			})
			.error(function(data){
				mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
			});
		}
	};

	$scope.clickUsuario = function(){
		$scope.nuevoUsuario=true;
		$scope.editar=true;
		$scope.paciente = {};
	};

	$scope.registrarDatos = function(){
		$scope.paciente.F_FECHA_DE_NACIMIENTO=moment(new Date($scope.paciente.FECHA_NACIMIENTO)).format('DD/MM/YYYY');
		$scope.paciente.NF_USUARIO=parseInt(usuario.usuario.NP_EXPEDIENTE);
		pacientes.guardaPaciente($scope.paciente)
		.success(function(data){
			if(data.success){
				mensajes.alerta('','SE HA REGISTRADO AL USUARIO','ACEPTAR!');
				$localStorage.cita = {
					paciente:{
						expediente:data.Mensaje,
						nombrePaciente: $scope.paciente.C_PRIMER_APELLIDO+" "+$scope.paciente.C_SEGUNDO_APELLIDO+" "+$scope.paciente.C_NOMBRE
					}
				};
				$scope.nuevoUsuario=false;
				$scope.alta=true;
			}
			else if(!data.success){
				mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
			}
			else {
				mensajes.alerta('ERROR','NO SE PUDO REALIZAR EL REGISTRO','ACEPTAR!');	
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

	$scope.calcularEdad =function(){
		$scope.paciente.F_FECHA_DE_NACIMIENTO = moment($scope.paciente.FECHA_NACIMIENTO).format('DD/MM/YYYY');
		var values=$scope.paciente.F_FECHA_DE_NACIMIENTO.split("/");
		var dia = values[0];
	    var mes = values[1];
        var ano = values[2];
        var fecha_hoy = new Date();
        var ahora_ano = fecha_hoy.getYear();
        var ahora_mes = fecha_hoy.getMonth()+1;
        var ahora_dia = fecha_hoy.getDate()-1+1;
        var edad = (ahora_ano + 1900) - ano;
        if(ahora_mes<mes){
        	edad--;
        }
        else if(ahora_mes==mes && ahora_dia<dia){
        	edad--;
        }
        $scope.paciente.EDAD=edad;
        if(edad<0){
        	mensajes.alerta('AVISO','NO PUEDES PONER UNA FECHA MAYOR AL DÍA DE HOY','ACEPTAR!');
        	$scope.paciente.FECHA_NACIMIENTO = undefined;
        	$scope.paciente.EDAD = undefined;
        }
    }

    $scope.parseDate = function (fecha){
    	return (fecha != undefined) ? moment(fecha).format('DD/MM/YYYY HH:mm') : "";
    };

    $scope.printCarne = function(dato){
		usuario.printCarnet(dato.N_EXPEDIENTE).
		then(function(data){
		/*
		var w;
		   	w=window.open();
		w.document.write(data);
		w.print();
		w.close();
		*/
		var fileName = 'carnet_' + $scope.selected[0].C_NOMBRE + ' '  + $scope.selected[0].C_PRIMER_APELLIDO;
		download(fileName + '.txt', data);

		//var command = 'copy "' + fileName + '.txt" \\\\127.0.0.1\\ZebraTLP2844-Z'
		//download(fileName + '.bat',command);
		})
		.catch(function(err){
		});  	
   }

   $scope.buscaUsuario = function(user){
   	if(user != undefined){
   		$scope.datos = _.filter($scope.registros , function(e){
   			var band = false;
   			if(e.nombreCompleto.toUpperCase().indexOf(user.toUpperCase()) >= 0 ) band = true;
   			if(e.C_EMAIL != undefined)
   				if(e.C_EMAIL.toUpperCase().indexOf(user.toUpperCase()) >= 0 ) band = true;
   			if(e.C_PERFIL != undefined)
   				if(e.C_PERFIL.toUpperCase().indexOf(user.toUpperCase()) >= 0 ) band = true;
			if(e.N_EXPEDIENTE != undefined){
				e.N_EXPEDIENTE += '';
   				if(e.N_EXPEDIENTE.indexOf(user) >= 0 ) band = true;
			}
   			return band;
   		});
   	}
   	else{
   		$scope.datos = $scope.registros;
   	}
   };

  }]);
})();