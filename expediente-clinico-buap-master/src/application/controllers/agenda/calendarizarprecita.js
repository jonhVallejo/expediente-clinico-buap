'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaCalendarizarprecitaCtrl
 * @description
 * # AgendaCalendarizarprecitaCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaCalendarizarprecitaCtrl', function ($scope,medicos,pacientes,$http,$state,$mdDialog,mensajes,usuario,$localStorage,blockUI, reportes) {

	$scope.citaEspecial=false;
  $scope.preCita=true;
	$scope.tituloCita="CALENDARIZAR PRE-CITA";
	$scope.cita= {
		precita: true
	};

  $scope.referencia={};


	$scope.calendarizarCita = function(){
    $scope.cita=$localStorage.cita;
  };

  $scope.cancelar = function(){
    var idPac= $localStorage.cita.paciente.expediente;
    pacientes.eliminarPaciente(idPac.replace("/",""))
    .success(function(data){
      mensajes.alerta('AVISO','CANCELACIÓN EXITOSA','ACEPTAR!');
    })
    .error(function(data){
      mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    });
    $localStorage.cita=undefined;
    $localStorage.paciente=undefined;
    $localStorage.medico=undefined;
    $state.go('home');
  };

  $scope.registrarDatos = function(ev) {
    $scope.datosPreCita={NP_ID:'',NF_CAPTURISTA:''};
    var myBlockUI = blockUI.instances.get('myBlockUI');
    myBlockUI.start();
    //REGISTRAR REFERENCIA MÉDICA
    if($scope.cita.referencia=="SI"){
      pacientes.guardaReferencia($scope.referencia)
      .success(function(data){
        if(data.hasOwnProperty('NP_ID')){
          $scope.datosPreCita.NF_REFERENCIA_MEDICA=data.NP_ID;
          $scope.datosPreCita.F_FECHA_HORA = $localStorage.cita.dia+" "+$localStorage.cita.horario;
          $scope.datosPreCita.NF_ESPECIALIDAD = $localStorage.cita.medico.idEspecialidad;
          $scope.datosPreCita.NF_PACIENTE = $localStorage.cita.paciente.expediente;
          $scope.datosPreCita.C_OBSERVACIONES = $scope.cita.C_OBSERVACIONES;
          if($scope.datosPreCita.NF_PACIENTE.indexOf('/') != -1){
            $scope.datosPreCita.NF_PACIENTE= $scope.datosPreCita.NF_PACIENTE.replace("/","");
          }
          $scope.datosPreCita.NF_PERSONAL = $localStorage.cita.medico.idMedico;
          $scope.datosPreCita.NF_CAPTURISTA = usuario.usuario.NP_EXPEDIENTE;
          $scope.datosPreCita.NF_TURNO = $scope.cita.medico.idTurno;

          pacientes.guardaCita($scope.datosPreCita)
          .success(function(data){
            if(data.success===true){
              reportes.getReporte(baseURL + 'ticket_exportar/'+data.Mensaje, '_blank', 'width=500, height=400');
              $localStorage.cita=undefined;
              $localStorage.cita=undefined;
              $localStorage.paciente=undefined;
              $localStorage.medico=undefined;
              $state.go('bienvenido');
              myBlockUI.stop();
            }
            else if(data.success===false){
              myBlockUI.stop();
              mensajes.alerta('ERROR','NO SE REALIZÓ EL REGISTRO','ACEPTAR!');
            }
          })
          .error(function(data){
            myBlockUI.stop();
            mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
          });
        }
        else{
          myBlockUI.stop();
          mensajes.alerta('ERROR','NO SE REALIZÓ EL REGISTRO','ACEPTAR!');
        }
      })
      .error(function(data){
        myBlockUI.stop();
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      });
    }
    else{
      $scope.datosPreCita.F_FECHA_HORA = $localStorage.cita.dia+" "+$localStorage.cita.horario;
      $scope.datosPreCita.NF_ESPECIALIDAD = $localStorage.cita.medico.idEspecialidad;
      $scope.datosPreCita.NF_PACIENTE = $localStorage.cita.paciente.expediente;
      if($scope.datosPreCita.NF_PACIENTE.indexOf('/') != -1){
        $scope.datosPreCita.NF_PACIENTE= $scope.datosPreCita.NF_PACIENTE.replace("/","");
      }
      $scope.datosPreCita.NF_PERSONAL = $localStorage.cita.medico.idMedico;
      $scope.datosPreCita.NF_CAPTURISTA = usuario.usuario.NP_EXPEDIENTE;
      $scope.datosPreCita.NF_TURNO = $scope.cita.medico.idTurno;
      $scope.datosPreCita.C_OBSERVACIONES = $scope.cita.C_OBSERVACIONES;

      pacientes.guardaCita($scope.datosPreCita)
      .success(function(data){
        if(data.success===true){
          myBlockUI.stop();
          reportes.getReporte(baseURL + 'ticket_exportar/'+data.Mensaje, '_blank', 'width=500, height=400');
          $localStorage.cita=undefined;
          $localStorage.paciente=undefined;
          $localStorage.medico=undefined;
          $state.go('bienvenido');
        }
        else if(data.success===false){
          myBlockUI.stop();
          mensajes.alerta('ERROR','NO SE REALIZÓ EL REGISTRO','ACEPTAR!');
        }
      })
      .error(function(data){
        myBlockUI.stop();
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      });
    }
  };

  });

  function addZero(i) {
      if (i < 10) {
          i = "0" + i;
      }
      return i;
  }
