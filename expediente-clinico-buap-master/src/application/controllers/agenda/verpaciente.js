'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaVerpacienteCtrl
 * @description
 * # AgendaVerpacienteCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaVerpacienteCtrl', function ($scope,$http,pacientes,mensajes,$state) {

	$scope.isEdit = false;
	$scope.title="DATOS GENERALES DEL PACIENTE";
	$scope.showDomicilio = false;
	$scope.verPaciente = function(){
		pacientes.getPaciente()
		.success(function(data){
			if(!data.data.hasOwnProperty('NP_ID')){
				$scope.paciente=data.data[0];
			}
			else{
				mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

	$scope.editarPaciente = function(){
		$scope.isEdit = true;
		this.loadCatalogos();
	}

	$scope.loadCatalogos = function() {
	    $scope.catEstadoCivil = [];
	    pacientes.getCatalogo("9").success(function(data){
			$scope.catEstadoCivil=data;
		});
		$scope.catSexo = [];
	    pacientes.getCatalogo("12").success(function(data){
			$scope.catSexo=data;
		});
		$scope.catEstado = [];
	    pacientes.getCatalogo("7").success(function(data){
			$scope.catEstado=data;
		});
	};

	$scope.actualizarDatos = function(){
		var newDate = new Date($scope.paciente.FECHA_NACIMIENTO);
		$scope.paciente.F_FECHA_DE_NACIMIENTO=('0' + newDate.getDate()).slice(-2)+"/"+('0' + (newDate.getMonth() + 1)).slice(-2)+"/"+newDate.getFullYear()
		pacientes.actualizaPaciente($scope.paciente)
		.success(function(data){
			if(data.hasOwnProperty('Mensaje')){
				mensajes.alerta('',data.Mensaje.toUpperCase(),'ACEPTAR!');
				$state.go('agendaConsultaPaciente');
			}
			else{
				mensajes.alerta('ERROR','NO SE REALIZÓ EL REGISTRO','ACEPTAR!');
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

	$scope.consultarMedico = function(){
		$state.go('agendaConsultaMedico');
	}

  });
