'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaCatespecialidadCtrl
 * @description
 * # AgendaCatespecialidadCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('CatespecialidadCtrl',  function ($scope,$timeout, $q, $log, $mdToast, $mdDialog ) {
	 	
		$scope.selected = [];
		$scope.editar = false;
		$scope.alta = false;
		$scope.busqueda = true;
		$scope.data = {};
		$scope.backData = {};


		$scope.query = {
		    filter: '',
		    order: 'id',
		    limit: 5,
		    page: 1
		};			
		
		$scope.registros = [];

    	$scope.registros = [
		  {id: 1,  descripcion: 'Alergología‎'},
		  {id: 2,  descripcion: 'Anatomía patológica‎'},
		  {id: 3,  descripcion: 'Biomedicina‎'},
		  {id: 4,  descripcion: 'Cardiología'},
		  {id: 5,  descripcion: 'Ciencias de la nutrición'},
		  {id: 6,  descripcion: 'Dermatología‎'},
		  {id: 7,  descripcion: 'Endocrinología‎'},
		  {id: 8,  descripcion: 'Farmacología clínica'},
		  {id: 9,  descripcion: 'Geriatría‎'},
		  {id: 10,  descripcion: ' Hematología‎'},

		 /* {id: 5,  descripcion: 'Especialidad 5',estatus: ' ACTIVO'},
		  {id: 6,  descripcion: 'Especialidad 6',estatus: ' ACTIVO'},
		  {id: 7,  descripcion: 'Especialidad 7',estatus: ' ACTIVO'},
		  {id: 8,  descripcion: 'Especialidad 8',estatus: ' ACTIVO'},
		  {id: 9,  descripcion: 'Especialidad 9',estatus: ' ACTIVO'}*/
		 
		];

		$scope.onOrderChange = function(page, limit) {
		    var deferred = $q.defer();
				    
		    $timeout(function () {
		    	deferred.resolve();
		    }, 2000);
				    
		    return deferred.promise;
		};


		$scope.deleteRow = function(){	
			console.log($scope.selected);

	 	 	$scope.selected.forEach(function(seleccion) {
		  		$scope.registros.splice($scope.registros.indexOf(seleccion),1);	
		  	}); 
		};


	  	

		$scope.click = function(){
	  		$scope.alta = true;
	  		$scope.busqueda = false;
        }


				
	  	$scope.loadEstatus = function() {
		    $scope.catEstatus = [];
		    $scope.catEstatus = [{id:1,nombre:'ACTIVO'},{id:2,nombre:'BAJA'}];
		};


		 $scope.editRow = function(){

		  	var alert;		  			  	
		  	if ($scope.selected.length > 1) {
		  		
			  	alert = $mdDialog.alert().title('Información').content('Sólo debe seleccionar un registro').ok('Cerrar');

			    $mdDialog
			        .show( alert )
			        .finally(function() {
			            alert = undefined;
			        }); 	
			    $scope.editar = false; 
			    $scope.busqueda = true;   
		  	} else {
				$scope.backData = $scope.selected[0];
				$scope.data = angular.copy($scope.backData);
				$scope.editar = true;
				$scope.busqueda = false;
		  	}
		  	$scope.selected = [];            
        }; 

        $scope.updateReg = function(){
        	$scope.registros[$scope.registros.indexOf($scope.backData)] = $scope.data;
		    $scope.data = {};
			$scope.editar = false;
			$scope.busqueda = true;      
	    };   


        $scope.addReg = function(registros){       	
		    registros.push($scope.data);
		    $scope.data = {};
			$scope.alta = false;
			$scope.busqueda = true;	      
	    };        
				 						  	

  });

