(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaCalendarizarcitaespecialCtrl
 * @description
 * # AgendaCalendarizarcitaespecialCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaCalendarizarcitaespecialCtrl', ['$scope','medicos','$http','pacientes','$state','$mdDialog','mensajes','usuario','$localStorage','$q','blockUI','reportes',
	function ($scope,medicos,$http,pacientes,$state,$mdDialog,mensajes,usuario,$localStorage,$q,blockUI,reportes) {

	$scope.citaEspecial=true;
	$scope.preCita=false;
	$scope.tituloCita="CALENDARIZAR CITA ESPECIAL";
	$scope.isMedico=false;
	$scope.horaActual="00:00:00";
	$scope.cita = {
        FECHA: new Date(),
    };
    console.log($scope.cita.FECHA.getFullYear());
    console.log($scope.cita.FECHA.getMonth());
    console.log($scope.cita.FECHA.getDate());

    $scope.currentDate = new Date(
    	$scope.cita.FECHA.getFullYear(),
      	$scope.cita.FECHA.getMonth(),
      	$scope.cita.FECHA.getDate());

    $localStorage.medico = {
    	value : '',
	    display: '',
	    nombre: '',
	    id  : '',
	    especialidad: '',
	    idEspecialidad: ''
    };

    $localStorage.paciente = {
    	value : '',
    	display : '',
    	nombre : '',
    	id : ''
    };
    $scope.catMedicos = [];
    $scope.selectedMedico = 0;

    var self = this;
    self.nombres = [];
	self.simulateQuery = false;
	self.isDisabled    = false;
	self.querySearch   = querySearch;
	self.selectedItemChange = selectedItemChange;
	var idMedico='';
    
	$scope.catEspecialidad = [];
    medicos.getEspecialidades().success(function(data){
		$scope.catEspecialidad = data;
	});

    function querySearch (text,paciente) {
    	
		var defered = $q.defer();
    	var promise = defered.promise;

		if(text != undefined || text!= null){
	    	if(text.length>2){
	    		if(text.indexOf('/') != -1 || text.indexOf('-') != -1){
			      text= text.replace("/","");
			      text= text.replace("-","");
			    }
		      	if(hasNumbers(text)){
		      		return self.nombres = [];
		        }
		      	if(paciente===0){ //Buscar Pacientes
		      		$localStorage.paciente = undefined;

		      		console.log('Se va a buscar paciente')
		      		pacientes.getListadoPacientes(text)
		      		.then(function(result){
		      			console.log(result)

		      			defered.resolve(result);
		    				self.isDisabled = false;
		    				// if($localStorage.medico != undefined){
		    				// 	console.log($localStorage)
						    //  //  	self.nombres = _.filter(cargarServicios(result),function(e){
						    // 	// 	var med = $scope.cita.idMedico+'';
						    // 	// 	var pac=e.id;
						    // 	// 	console.log(e)
							   //  // 	pac=pac.split("/")[0];
						    // 	// 	return pac+''!==med+'';
						    // 	// });
						    // }
						    // else{
						    // 	self.nombres = result;
						    // }


		      		})
		      		.catch(function(error){
		      			console.error(error)
		      		})


		      // 		pacientes.getPacientes(text)
	    			// .success(function(data){


	    			// 	if(data.success){
		    		// 		defered.resolve(self.nombres);
		    		// 		self.isDisabled = false;
		    		// 		if($localStorage.medico != undefined){
						  //     	self.nombres = _.filter(cargarServicios(data.data),function(e){
						  //   		var med = $scope.cita.idMedico+'';
						  //   		var pac=e.id;
							 //    	pac=pac.split("/")[0];
						  //   		return pac+''!==med+'';
						  //   	});
						  //   }
						  //   else{
						  //   	self.nombres = data.data;
						  //   }
					   //  }
					   //  else{
					   //  	defered.reject({});
	      	// 			}


	    			// });


				    return promise;
		      	}
		    }
	    }
	    else{
	    	defered.reject();
	    }
	    return promise;
	}

	function hasNumbers(t){
	    return /\d/.test(t);
	}
  
  	function selectedItemChange(item,paciente) {
  		if(item!==undefined){
	  		if(paciente===0){
	    		$localStorage.paciente = item;
	    		$scope.cita.paciente = {
	    			expediente : $localStorage.paciente.id,
	    			expedienteSIU : $localStorage.paciente.expSIU
	    		};
	    		self.nombres = [];
	    		$scope.selectEspecialidad();
	    	}
	    }
  	}

	function createFilterFor(query) {
	    var consulta = angular.uppercase(query);
	    return function filterFn(state) {
	    	return (state.value.search(consulta) !== -1);
	    };
	};

	
	function cargarServicios(data){
	    if(data!==undefined){
		    return data.map(function (serv){
		      return {
		        value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE+" - "+(serv.NP_ID)+" " + (serv.C_CURP ? " - "+serv.C_CURP : '')).toUpperCase(),
          		display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE+" - "+(serv.NP_ID)+" " + (serv.C_CURP ? " - "+serv.C_CURP : '')).toUpperCase(),
		        nombre: serv.C_NOMBRE,
		        id  : serv.NP_EXPEDIENTE,
		        expSIU : serv.NP_EXPEDIENTE_SIU
		      };
		    });
		}
	};

	
	function cargarServiciosMedico(data){
	    return data.map(function (serv){
	      return {
	        value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
	        display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
	        nombre: serv.C_NOMBRE,
	        id  : serv.N_EXPEDIENTE,
	        especialidad: serv.ESPECIALIDADES.CF_ESPECIALIDAD,
	        idEspecialidad: serv.ESPECIALIDADES.NF_ESPECIALIDAD
	      };
	    });
	};

	$scope.cancelar = function(){
		$localStorage.paciente = undefined;
		$localStorage.medico = undefined;
		$state.go("bienvenido");
	};

	$scope.registrarDatos = function(ev) {
		var myBlockUI = blockUI.instances.get('myBlockUI');
    	myBlockUI.start();
		if($scope.cita.idMedico === -1){
			mensajes.alerta('AVISO','INGRESA EL NOMBRE DE UN MÉDICO VÁLIDO','ACEPTAR!');
			myBlockUI.stop();
		}
		else{
	        $scope.citaEspecial={};
	        var expedientePaciente = $localStorage.paciente.id;
	        if(expedientePaciente.indexOf('/') != -1 || expedientePaciente.indexOf('-') != -1){
	      		expedientePaciente= expedientePaciente.replace("/","");
	      		expedientePaciente= expedientePaciente.replace("-","");
	    	}
			$scope.citaEspecial.F_FECHA_HORA = moment(new Date($scope.cita.FECHA)).format('DD/MM/YYYY')
			var newTime = new Date($scope.cita.HORA);
			var horas = addZero(newTime.getHours());
			var minutos = addZero(newTime.getMinutes());
			$scope.citaEspecial.F_FECHA_HORA = $scope.citaEspecial.F_FECHA_HORA + " "+horas+":"+minutos+":00";
			$scope.citaEspecial.NF_ESPECIALIDAD = $scope.cita.especialidad;
			$scope.citaEspecial.NF_PACIENTE = expedientePaciente;
			$scope.citaEspecial.NF_PERSONAL = $scope.cita.idMedico+"";
			$scope.citaEspecial.NF_CAPTURISTA = usuario.usuario.NP_EXPEDIENTE;
			$scope.citaEspecial.CITA_ESPECIAL = "";
			$scope.citaEspecial.C_OBSERVACIONES = $scope.cita.C_OBSERVACIONES;
			$scope.citaEspecial.FECHA = moment($scope.citaEspecial.F_FECHA_HORA,"DD/MM/YYYY HH:mm:SS").format("DDMMYYYY");
			pacientes.getNoCitas($scope.citaEspecial)
			.success(function(data){
				var confirm = $mdDialog.confirm()
				    .title('AVISO')
				    .content('EL MÉDICO YA CUENTA CON '+data.Mensaje.toUpperCase()+' CITA(S), DESEA EMPALMAR?')
					.ariaLabel('Lucky day')
				    .targetEvent(ev)
				    .ok('ACEPTAR!')
				    .cancel('CANCELAR');
				$mdDialog.show(confirm).then(function() {
					pacientes.guardaCita($scope.citaEspecial)
					.success(function(data){
						if(data.success===true){
				            reportes.getReporte(baseURL + 'ticket_exportar/'+data.Mensaje, '_blank', 'width=500, height=400');
				            $localStorage.medico=undefined;
				            $localStorage.paciente = undefined;
				            myBlockUI.stop();
				            $state.go('bienvenido');
				        }
				        else if(data.success===false){
				            myBlockUI.stop();
				            mensajes.alerta('ERROR',data.Mensaje.toUpperCase(),'ACEPTAR!');
				        }
					})
					.error(function(data){
						myBlockUI.stop();
						mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
					});

				}, function() {
					myBlockUI.stop();
				});
			})
			.error(function(data){
				myBlockUI.stop();
				mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
			});
		}
    };

    $scope.getCurrentDate= function(){
    	$scope.fechaActual=new Date(new Date().setDate(new Date().getDate() - 1));
    };

    $scope.getCurrentTime = function(){
    	if($scope.cita.FECHA!==undefined || $scope.cita.HORA){
			var fechaActual = moment(new Date()).format('DD/MM/YYYY');
			var fechaCita = moment(new Date($scope.cita.FECHA)).format('DD/MM/YYYY');
    		if(fechaActual===fechaCita){
    			if($scope.cita.HORA!==undefined){
	    			var horaCita=$scope.cita.HORA+'';
	    			horaCita = horaCita.split(" ")[4];
	    			var horaActual = moment(new Date()).format('HH:mm:ss');
	    			$scope.horaActual=horaActual;
	    		}	
    		}
    		else{
    			$scope.horaActual="00:00:00";
    		}
    	}
    	
    };

    $scope.selectEspecialidad = function(){
    	$scope.cita.idMedico = -1;
    	if($scope.cita.especialidad != undefined){
    		medicos.getMedicos($scope.cita.especialidad).success(function(data){
	    		if(data.length > 0){
	    			$scope.existMedicos = true;
	    			$scope.catMedicos = _.filter(data,function(e){
						return e.N_EXPEDIENTE+'' !== $localStorage.paciente.id.split("/")[0]+'';
					});
	    		}
	    		else{
	    			$scope.existMedicos = false;
	    		}
			});
    	}
    };

  }]);


	function addZero(i) {
	    if (i < 10) {
	        i = "0" + i;
	    }
	    return i;
	}
})();