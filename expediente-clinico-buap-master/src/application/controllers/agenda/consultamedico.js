(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaConsultamedicoCtrl
 * @description
 * # AgendaConsultamedicoCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaConsultamedicoCtrl', ['$scope','$http','$mdDialog','medicos','$log','$state','mensajes','$localStorage','$stateParams', function ($scope,$http,$mdDialog,medicos,$log,$state,mensajes,$localStorage,$stateParams) {

	$scope.selected = [];
	$scope.existMedicos = true;
  	$scope.query = {
   		filter: '',
   		order: 'nombre',
   		limit: 15,
   		page: 1
  	};

	function success(datos) {
		$scope.datos = datos;
	}

	$scope.search = function (predicate) {
	    $scope.filter = predicate;
	    $scope.deferred = $scope.datos.get($scope.query, success).$promise;
	};

	$scope.loadTable = function(){ 
		$scope.existMedicos = true;
		$scope.datos={};
		medicos.getMedicos($scope.medico.especialidad.ID).success(function(data){
			if($stateParams.state !== 'consulta'){
				if($localStorage.paciente != undefined){
					$localStorage.notCita = false;
					$scope.datos=_.filter(data,function(e){
						return e.N_EXPEDIENTE+'' !== $localStorage.paciente.idexpediente+'';
					});
				}
				else{
					$localStorage.notCita = true;
					$scope.datos = data;
				}
			}
			else{
				$localStorage.notCita = true;
				$scope.datos = data;
			}
			if ($scope.datos.length<=0){
				$scope.existMedicos = false;
				mensajes.alerta('AVISO','NO SE ENCONTRARON MÉDICOS DISPONIBLES','ACEPTAR!');
			}
		});
	};

  	$scope.loadEspecialidades = function() {
		$scope.catEspecialidad = [];
	    medicos.getEspecialidades().success(function(data){
			$scope.catEspecialidad=data;
		});
	};

	$scope.verDisponibilidad = function(ev,dato) {
  		$localStorage.medico = {
  			nombreMedico : dato.C_PRIMER_APELLIDO+" "+dato.C_SEGUNDO_APELLIDO+" "+dato.C_NOMBRE,
  			nombreEspecialidad : dato.ESPECIALIDADES.CF_ESPECIALIDAD,
  			idMedico : dato.N_EXPEDIENTE,
  			idTurno: dato.TURNOS.NF_TURNO,
  			idEspecialidad: $scope.medico.especialidad.ID
  		};

  		$state.go('agendaVerDisponibilidad');
	};



  }]);

})();	
