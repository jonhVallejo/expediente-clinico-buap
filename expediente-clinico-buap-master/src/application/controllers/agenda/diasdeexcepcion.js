
(function(){
'use strict';

angular.module('expediente')
    .controller('AgendaDiasdeexcepcionCtrl',['$scope','$timeout', '$q', '$http', 'mensajes', 'pacientes', 'medicos', 'catalogos','$mdDialog', 'blockUI',
      function ($scope,$timeout, $q, $http, mensajes, pacientes, medicos, catalogos, $mdDialog, blockUI) {

  //var baseURL = 'http://148.228.103.13:8080/';
	var actionURL = 'excepciones';
  var id;
  var self = this;
  self.simulateQuery = false;
  self.isDisabled    = false;
  self.querySearch   = querySearch;
  self.selectedItemChange = selectedItemChange;
  self.searchTextChange   = searchTextChange;
  var expediente='';
  var _catalogos;
  $scope.selected = [];
  $scope.diasSinExcepcion = [];
  $scope.diasConExcepcion = [];
  $scope.catDiasExcepcion = [];
  $scope.fechaInicial = new Date();
  $scope.fechaInicial = new Date($scope.fechaInicial.getFullYear(),
                               $scope.fechaInicial.getMonth(),
                               $scope.fechaInicial.getDate());
  $scope.fechaFinal = new Date($scope.fechaInicial.getFullYear(),
                               $scope.fechaInicial.getMonth() + 12,
                               $scope.fechaInicial.getDate());
  $scope.excepcionValida = false;


  
	$scope.buscar = function(){
      blockUI.start();
      $http({
              url : baseURL + actionURL+ '/' + id + '/' + moment($scope.obj.beginDate).format('DDMMYYYY') + '/' + moment($scope.obj.endDate).format('DDMMYYYY'), 
              method : 'GET',
              data: '',
              dataType : 'json',
              headers:{
                  'Content-Type': 'application/json',
              }
          }).success(function(data){  
            blockUI.stop();
            var _diasExcepcion = data.data;  
            if (_diasExcepcion.length > 0) {
              $scope.diasConExcepcion = _.map(_diasExcepcion, function(el){
                    return {
                      'F_FECHA_FINAL': el.F_FECHA_FINAL,
                      'F_FECHA_INICIAL': el.F_FECHA_INICIAL,
                      'FECHA_INICIAL':  el.F_FECHA_INICIAL.substring(6,10)+el.F_FECHA_INICIAL.substring(3,5)+el.F_FECHA_INICIAL.substring(0,2),
                      'NF_EXCEPCION': el.NF_EXCEPCION,
                      'NF_PERSONAL': el.NF_PERSONAL,
                      'NP_ID': el.NP_ID,
                      'DESCRIPCION': _.filter($scope.catDiasExcepcion, function(val){
                                  console.log(val);
                                  return el.NF_EXCEPCION === val.ID;
                                  })[0].cDescripcion
                    };
                    });
              $scope.diasSinExcepcion = [];
            }
            else {
              $scope.diasSinExcepcion = [
                {FECHAINI:moment($scope.obj.beginDate).format('DD/MM/YYYY'),
                FECHAFIN:moment($scope.obj.endDate).format('DD/MM/YYYY')}
              ];
              $scope.diasConExcepcion = [];        
            }
          }).error(function(err){
            blockUI.stop();
          	mensajes.alerta('ERROR', 'ERROR, EL MEDICO ESPECIFICADO NO EXISTE','ACEPTAR!');
          });     
       
	}

  function asigna(idExcepcion){
    var data = {};
    data.NP_PERSONA = id;
    data.NP_EXCEPCION = idExcepcion;
    data.F_FECHA_INICIAL = moment($scope.obj.beginDate).format('DDMMYYYY');
    data.F_FECHA_FINAL = moment($scope.obj.endDate).format('DDMMYYYY');
    blockUI.start();
    $http.post(baseURL+actionURL,data).success(function(){          
          blockUI.stop();
          $scope.diasSinExcepcion = [];
          mensajes.alerta('EXITO', 'ASIGNACION EXITOSA..','CONTINUAR');  
          $scope.buscar();
        }).error(function(err){
          blockUI.stop();
          mensajes.alerta('ERROR', 'ERROR, VERIFICA TU INFORMACIÓN','ACEPTAR!');
        });      
  }  

  function desasigna(){
    var datos = "";
    var coma = "";
    var registros = {};
    $scope.selected.forEach(function(seleccion) {
      datos = datos + coma + seleccion.NP_ID;
      coma = ",";
    });
    
    registros.idAusencia = datos;
    blockUI.start();
    $http.delete(baseURL+actionURL,{data:registros}).success(function(){          
          blockUI.stop();
          $scope.selected.forEach(function(seleccion) {
            $scope.diasConExcepcion.splice($scope.diasConExcepcion.indexOf(seleccion),1);
          }); 
          $scope.selected = [];    
          mensajes.alerta('EXITO', 'DESASIGNACION EXITOSA..','CONTINUAR');          
        })
      .error(function(err){
          blockUI.stop();
          mensajes.alerta('ERROR', 'ERROR, VERIFICA TU INFORMACIÓN','ACEPTAR!');
        })  ;
  }
 

  $scope.query = {
    filter: '',
    order: 'F_FECHA_INICIAL',
    limit: 15,
    page: 1
  };

  $scope.obtenDescripcionTurno = function(idExcepcion){  
    angular.forEach($scope.catDiasExcepcion, function (value, prop, obj) {
      if (value.ID == idExcepcion.NP_EXCEPCION)
         idExcepcion.DESCRIPCION = value.cDescripcion; 
      
    });
      
  };
 

	$scope.onOrderChange = function(page, limit) {
        var deferred = $q.defer();
            
        $timeout(function () {
          deferred.resolve();
        }, 2000);
    return deferred.promise
  };

  $scope.onPaginationChange = function (page, limit) {
      if(typeof success !== 'undefined')
        return $scope.datos.get($scope.query, success).$promise;
  };

  function querySearch (query) {

     var defered = $q.defer();
    var promise = defered.promise;


    if(query !== undefined && query.length>3){

        
        medicos.getMedicos(query)
        .success(function(data){     
          self.isDisabled = false;
          self.nombres = cargarServicios(data);
          defered.resolve(self.nombres);
        })
        .error(function(data){      
          mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
          defered.reject();
        });



    }else{
      defered.reject();
    }
  
    return promise;
  }

  function searchTextChange(text) {
    
  }
  
  function selectedItemChange(item) {
    console.log(item);
    $scope.vaciaTablas();
    id = item.id;
  }

  function createFilterFor(query) {
    var consulta = angular.uppercase(query);
    return function filterFn(state) {
      return (state.value.search(consulta) !== -1);
    };
  };

  function getServicios(query){

   	medicos.getMedicos(query)
    .success(function(data){     
      self.isDisabled = false;
      

      self.nombres = cargarServicios(data);
      console.log(self.nombres);

    })
    .error(function(data){      
      mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
    });
  };

  function cargarServicios(data){



    if(data != undefined){
      return data.map(function (serv){
        return {
                	value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase()+" - "+serv.N_EXPEDIENTE,
        	        display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase()+" - "+serv.N_EXPEDIENTE,
        	        nombre: serv.C_NOMBRE,
        	        id  : serv.N_EXPEDIENTE,
        	        especialidad: serv.ESPECIALIDADES.CF_ESPECIALIDAD,
        	        idEspecialidad: serv.ESPECIALIDADES.NF_ESPECIALIDAD
              };
      });
    }
  };

  $scope.inicializa = function() {
        blockUI.start();
        catalogos.getCatalogos('16')
          .success(function(data){
              blockUI.stop();
              data.push({tFechaCmd:"",ID:-1,sEstatusRegistro:1,cDescripcion:"SELECCIONE"});
              $scope.catDiasExcepcion=data;  
              _catalogos = data;
              
          })
          .error(function(data){
            blockUI.stop();
            mensajes.alerta('ERROR','ERROR AL CARGAR EL CATALOGO DE EXCEPCIONES','ACEPTAR!');
          });

    };

  $scope.validaInformacion = function(){
    if ($scope.diasSinExcepcion.length > 0 ) {
      console.log(idExcepcion);
      return true;
    } else {
        mensajes.alerta('ERROR', 'LA INFORMACIÓN SE ENCUENTRA INCOMPLETA');
        return false;  
    }    
  }

  $scope.showConfirm = function(ev,funcion,idExcepcion) {
    var confirm = $mdDialog.confirm()
          .title('Precaución')
          .content('Desea realizar los cambios')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Aceptar')
          .cancel('Cancelar');
    $mdDialog.show(confirm).then(function() {
      if (funcion == "asigna")
        asigna(idExcepcion);
      else{
        desasigna();
      }
        
    }, function() {
      $scope.nombreFuncion = '';
    });
  };  

  $scope.inicializaBeginDate = function(){
    $scope.obj = {beginDate : moment().toDate() };    
  }

  $scope.vaciaTablas = function(){
    $scope.diasSinExcepcion = [];
    $scope.diasConExcepcion = [];    
    if ($scope.obj.beginDate > $scope.obj.endDate) 
      $scope.obj.endDate = $scope.obj.beginDate;

  }

 }]);
})();

