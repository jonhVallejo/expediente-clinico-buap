'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaCatcie10Ctrl
 * @description
 * # AgendaCatcie10Ctrl
 * Controller of the expediente
 */
angular.module('expediente')
  angular.module('expediente').controller('AgendaCatcie10Ctrl',  function ($scope,catalogos,blockUI) {
	 	
	$scope.selected = [];

	$scope.query = {
	    filter: '',
	    order: 'id',
	    limit: 15,
	    page: 1
	};			
		
	$scope.registros = [];
				 

	$scope.loadTabla = function() {
		var myBlockUI = blockUI.instances.get('myBlockUI');
		myBlockUI.start();
	    catalogos.getCatCIE10().success(function(data){
			$scope.registros=data;	
			myBlockUI.stop();
		});
	}; 		 						  	

  });

