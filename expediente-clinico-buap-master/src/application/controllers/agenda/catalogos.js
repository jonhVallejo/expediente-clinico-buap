'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaCatalogosCtrl
 * @description
 * # AgendaCatalogosCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaCatalogosCtrl',  function ($scope,$timeout, $q, $log, $mdToast, $mdDialog, catalogos ) {
	 	
		$scope.selected = [];
		$scope.editar = false;
		$scope.alta = false;
		$scope.busqueda = true;
		$scope.data = {};
		$scope.backData = {};
		$scope.catEstatus = [];
		$scope.urlTabla = '';
		$scope.isTablaTurnos = false;


		$scope.query = {
		    filter: '',
		    order: 'id',
		    limit: 5,
		    page: 1
		};			
		
		$scope.registros = [];

    	

		$scope.onOrderChange = function(page, limit) {
			var deferred = $q.defer();
				    
		    $timeout(function () {
		    	deferred.resolve();
		    }, 2000);
				    
		    return deferred.promise;


		};


		$scope.deleteRow = function(registros){	
			$scope.editar = false;
			$scope.registros=registros;
	 	 	$scope.selected.forEach(function(seleccion) {
	 	 		catalogos.eliminaRegistro($scope.urlTabla,seleccion)
				.success(function(){
				  /*EXITOSO*/
				  		$scope.registros.splice($scope.registros.indexOf(seleccion),1);
				  		$scope.selected = [];
	  					$scope.busqueda = true;
	  					$scope.alta = false;

				})
				.error(function(data){
					$mdDialog.show(
						$mdDialog.alert()
						.clickOutsideToClose(true)
						.title('ERROR')
						.content('SERVICIO NO DISPONIBLE')
						.ok('ACEPTAR!')
					);
				});	
		  	}); 
		};


		$scope.click = function(){
	  		$scope.alta = true;
	  		$scope.busqueda = false;
        }

			
		 $scope.editRow = function(){

		  	var alert;		  			  	
		  	if ($scope.selected.length > 1) {
		  		
			  	alert = $mdDialog.alert().title('Información').content('Sólo debe seleccionar un registro').ok('Cerrar');

			    $mdDialog
			        .show( alert )
			        .finally(function() {
			            alert = undefined;
			        }); 	
			    $scope.editar = false; 
			    $scope.busqueda = true;   
		  	} else {
				$scope.backData = $scope.selected[0];
				$scope.data = angular.copy($scope.backData);
				$scope.editar = true;
				$scope.busqueda = false;
		  	}
		  	$scope.selected = [];            
        }; 

        $scope.updateReg = function(registros){
            catalogos.modificaRegistro($scope.urlTabla,$scope.data)
            .success(function(data){
                /*EXITOSO*/
                $scope.registros=registros;
            	$scope.registros[$scope.registros.indexOf($scope.backData)] = $scope.data;
                $scope.data = {};
                $scope.editar = false;
                $scope.busqueda = true;
            })
            .error(function(data){
                $mdDialog.show(
	                $mdDialog.alert()
	                .clickOutsideToClose(true)
	                .title('ERROR')
	                .content('SERVICIO NO DISPONIBLE')
	                .ok('ACEPTAR!')
              	);
            });
        };    


        $scope.addReg = function(){    
        	/*$scope.backData.ID = $scope.data.ID;
        	$scope.backData.cDescripcion = $scope.data.cDescripcion; */
        	

        	catalogos.guardaRegistro($scope.urlTabla,$scope.data)
        		.success(function(data){
					/*EXITOSO*/
					$scope.loadTabla($scope.urlTabla);
				    $scope.data={};
				    $scope.backData = {};
				})
				.error(function(data){
						$mdDialog.show(
						$mdDialog.alert()
						.clickOutsideToClose(true)
						.title('ERROR')
						.content('SERVICIO NO DISPONIBLE')
						.ok('ACEPTAR!')
					);
				});		    		    
			$scope.alta = false;
			$scope.busqueda = true;	      
	    };        
				 

		$scope.loadTabla = function(urlTabla) {
			if (urlTabla == 13)
				$scope.isTablaTurnos = true;
		    $scope.registros = [];
		    $scope.urlTabla = urlTabla;
		    catalogos.getRegistros(urlTabla).success(function(data){
					$scope.registros=data;	
			});
		}; 		 						  	

  });

