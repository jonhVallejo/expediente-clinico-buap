(function(){
'use strict';
/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaDiasfestivosCtrl
 * @description
 * # AgendaDiasfestivosCtrl
 * Controller of the expediente
 */
angular.module('expediente')
    .controller('AgendaDiasfestivosCtrl',['$scope','$timeout', '$q', '$http', 'mensajes', 'pacientes','medicos', 'catalogos', '$mdDialog', 'blockUI','peticiones',
      function ($scope,$timeout, $q, $http, mensajes, pacientes, medicos, catalogos, $mdDialog, blockUI,peticiones) {
  
  var actionURL = 'excepciones/todos/';
  var expediente='';
  $scope.selected = [];
  $scope.diasSinExcepcion = [];
  $scope.diasConExcepcion = [];
  $scope.medicos = [];
  $scope.catDiasExcepcion = [];
  $scope.fechaInicial = new Date();
  $scope.fechaInicial = new Date($scope.fechaInicial.getFullYear(),
                               $scope.fechaInicial.getMonth(),
                               $scope.fechaInicial.getDate());
  $scope.fechaFinal = new Date($scope.fechaInicial.getFullYear(),
                               $scope.fechaInicial.getMonth() + 12,
                               $scope.fechaInicial.getDate());
  $scope.excepcionValida = false;
  $scope.registros = [];
  inicializa();

  function inicializa() {
      catalogos.getCatalogos('16')
        .success(function(data){
          $scope.catDiasExcepcion=data;  
        })
        .error(function(data){
          mensajes.alerta('ERROR','ERROR AL CARGAR EL CATALOGO DE EXCEPCIONES','ACEPTAR');
        });

  };

	$scope.buscar = function(){
     peticiones.getDatos(actionURL+ '/' + moment($scope.obj.beginDate).format('DDMMYYYY') + '/' + moment($scope.obj.endDate).format('DDMMYYYY'))  
    .then(function(data) {
      $scope.diasSinExcepcion=data.SIN_EXCEPCION;
      $scope.diasConExcepcion=data.CON_EXCEPCION;
      for(var x in $scope.diasConExcepcion){
        for(var i in $scope.diasConExcepcion[x].DIAS_AUSENCIA){
          $scope.diasConExcepcion[x]={
            DIAS_AUSENCIA: $scope.diasConExcepcion[x].DIAS_AUSENCIA[i],
            DOCTOR: $scope.diasConExcepcion[x].DOCTOR,
            EXCEPCION: $scope.getDescripcion($scope.diasConExcepcion[x].DIAS_AUSENCIA[i].NF_EXCEPCION)
          }
         }
      }
    })
    .catch(function(err) {
       mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
    });
  }

  $scope.asigna= function(registros){
    if($scope.selected.length>0){
      var datos=[];
      var f_inicial=moment($scope.obj.beginDate).format('DDMMYYYY');
      var f_final=moment($scope.obj.endDate).format('DDMMYYYY');
      mensajes.confirmacion('PRECAUCIÓN','LAS CITAS AGENDADAS DE CADA UNO DE LOS MEDICOS PARA ESTA FECHA, PASARAN EN ESTADO DE CANCELADO','OK')
        .then(function() {
          $scope.registros=registros;
          $scope.selected.forEach(function(seleccion) {
            datos.push({
              NP_PERSONA: seleccion.DOCTOR.N_EXPEDIENTE,
              NP_EXCEPCION:$scope.tipoExcepcion,
              F_FECHA_INICIAL: f_inicial,
              F_FECHA_FINAL:f_final
            });
          });
          peticiones.postDatos("excepciones/varias",datos)
          .then(function(data) {
              mensajes.alerta('EXITO', 'ASIGNACION EXITOSA','ACEPTAR');
              $scope.selected = [];
              //usar el front para visualizar información
              $scope.buscar();
            })
            .catch(function(err) {
               mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
            });
        }).catch(function() {
           $scope.selected = [];
        });
    }
    else
       mensajes.alerta('PRECAUCIÓN', 'SELECCIONE UN ELEMENTO','ACEPTAR');
   
  }  
  $scope.desasigna= function(){
    if($scope.selected.length>0){
      $scope.datos = "";
      var coma = "";
      var registros = {};
      $scope.selected.forEach(function(seleccion) {
        $scope.datos=$scope.datos +coma + seleccion.DIAS_AUSENCIA.NP_ID;
        coma = ",";
      });
      registros.idAusencia = $scope.datos;
      blockUI.start();
      $http.delete(baseURL+"excepciones",{data:registros}).success(function(){          
            blockUI.stop();
            $scope.selected.forEach(function(seleccion) {
              $scope.diasConExcepcion.splice($scope.diasConExcepcion.indexOf(seleccion),1);
              $scope.diasSinExcepcion.push({
                  DIAS_AUSENCIA:seleccion.DIAS_AUSENCIA,
                  DOCTOR: seleccion.DOCTOR
               });
              console.log( $scope.diasSinExcepcion);
            }); 
            $scope.selected = [];    
            mensajes.alerta('EXITO', 'DESASIGNACION EXITOSA..','CONTINUAR');  
           // $scope.buscar();        
          })
        .error(function(err){
            blockUI.stop();
            mensajes.alerta('ERROR', 'ERROR, VERIFICA TU INFORMACIÓN','ACEPTAR!');
          })  ;

    }
    else
      mensajes.alerta('PRECAUCIÓN', 'SELECCIONE UN ELEMENTO','ACEPTAR');
  }

  $scope.query = {
    filter: '',
    order: 'F_FECHA_INICIAL',
    limit: 15,
    page: 1
  };
  $scope.queryDos = {
    filter: '',
    order: 'F_FECHA_INICIAL',
    limit: 5,
    page: 1
  };

  $scope.getDescripcion=function(idExcepcion){
    var existe = _.filter($scope.catDiasExcepcion, function(val){ 
        return idExcepcion === val.ID;
    });
    return existe[0].cDescripcion;
  }
 
	$scope.onOrderChange = function(page, limit) {
          var deferred = $q.defer();
          $timeout(function () {
            deferred.resolve();
          }, 2000);
  };
  $scope.onOrderChangeDos = function(page, limit) {
          var deferredDos = $q.defer();
          $timeout(function () {
            deferredDos.resolve();
          }, 2000);
  };

  $scope.change = function(id){
    if(id==1)
      $scope.query.page = 1;
    else
       $scope.queryDos.page = 1;

  };
 
  $scope.validaInformacion = function(){
    if ($scope.diasSinExcepcion.length > 0 ) {
      return true;
    } else {
        mensajes.alerta('ERROR', 'LA INFORMACIÓN SE ENCUENTRA INCOMPLETA');
        return false;  
    }    
  }
  $scope.inicializaBeginDate = function(){
    $scope.obj = {beginDate : moment().toDate() };    
  }

  $scope.vaciaTablas = function(){
    $scope.diasSinExcepcion = [];
    $scope.diasConExcepcion = [];    
    if ($scope.obj.beginDate > $scope.obj.endDate) 
      $scope.obj.endDate = $scope.obj.beginDate;

  }
 }]);
})();


