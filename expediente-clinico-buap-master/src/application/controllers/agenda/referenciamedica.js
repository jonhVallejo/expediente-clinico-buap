'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaReferenciamedicaCtrl
 * @description
 * # AgendaReferenciamedicaCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaReferenciamedicaCtrl', function ($scope,$http,$state,pacientes,mensajes) {

	$scope.registrarDatos = function(){
		var newDate = new Date($scope.paciente.FECHA_NACIMIENTO);
		$scope.paciente.F_FECHA_DE_NACIMIENTO=('0' + newDate.getDate()).slice(-2)+"/"+('0' + (newDate.getMonth() + 1)).slice(-2)+"/"+newDate.getFullYear()
		pacientes.guardaReferencia($scope.paciente)
		.success(function(data){
			if(data.hasOwnProperty('Mensaje')){
				mensajes.alerta('',data.Mensaje.toUpperCase(),'ACEPTAR!');
				$state.go('agendaConsultaPaciente');
			}
			else{
				mensajes.alerta('ERROR','NO SE REALIZÓ EL REGISTRO','ACEPTAR!');
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

	$scope.loadEstadoCivil = function() {
	    $scope.catEstadoCivil = [];
	    pacientes.getCatalogo("9").success(function(data){
			$scope.catEstadoCivil=data;
		});
	};

	$scope.loadSexo = function() {
	    $scope.catSexo = [];
	    pacientes.getCatalogo("12").success(function(data){
			$scope.catSexo=data;
		});
	  };

	$scope.loadEstado = function() {
		$scope.catEstado = [];
	    pacientes.getCatalogo("7").success(function(data){
			$scope.catEstado=data;
		});
	};
  });
