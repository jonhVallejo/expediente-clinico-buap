'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaVerdisponibilidadCtrl
 * @description
 * # AgendaVerdisponibilidadCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaVerdisponibilidadCtrl', function ($scope,$http,$mdDialog,$state,medicos,mensajes,$localStorage) {

	$scope.selected = [];
	$scope.title= "DISPONIBILIDAD DE CONSULTA MÉDICA";
	$scope.existCitas = true;

  	$scope.query = {
   		filter: '',
   		order: '',
   		limit: 10,
   		page: 1
  	};

	function success(datos) {
		$scope.datos = datos;
	}

	$scope.search = function (predicate) {
	    $scope.filter = predicate;
	    $scope.deferred = $scope.datos.get($scope.query, success).$promise;
	};

  	$scope.verHorario = function(ev,dato) {
  		$localStorage.cita={
  			dia: dato.DIA.split(" ")[0],
  			medico : $localStorage.medico,
  			paciente: $localStorage.paciente
  		};
	    $mdDialog.show({
	      templateUrl: '/application/views/agenda/dialog1.tmpl.html',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:true,
	    });
	};

	$scope.calendarizarCita = function(datoHorario){
		$mdDialog.hide();
		$localStorage.cita.horario = datoHorario.FECHA_HORA.split(" ")[1];
		$state.go('agendaCalendarizarCita');
	}

	$scope.hide = function() {
	    $mdDialog.hide();
	};
	  
	$scope.cancel = function() {
	    $mdDialog.cancel();
	};
	
	$scope.close = function() {
	    $mdDialog.hide();
	};

	$scope.datos =[];

	$scope.loadTable = function(){		
		$scope.existCitas=true;
		$scope.datos = [];
	    medicos.getDiasDisponibles($localStorage.medico.idMedico)
	    .success(function(data){
	    	if(data.length <= 0){
	    		$scope.existCitas=false;
				mensajes.alerta('AVISO','LO SENTIMOS, ESTE MÉDICO NO TIENE CITAS DISPONIBLES','ACEPTAR!');
			}
			$scope.datos=data;
		})
		.error(function(data){
			$scope.existCitas=false;
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});

	};

	$scope.loadTableHorario = function(){
		$scope.notShow = $localStorage.notCita;
		$scope.titleDia=$localStorage.cita.dia;
		var diaDisponible= $scope.titleDia.replace("/","").replace("/","");
		$scope.datosHorario = [];
	    medicos.getHorariosDisponibles($localStorage.cita.medico.idMedico,$localStorage.cita.medico.idTurno,diaDisponible)
	    .success(function(data){
			$scope.datosHorario=data;
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

	$scope.loadDatos = function(){
		$scope.consulta={
			MEDICO: $localStorage.medico.nombreMedico,
			ESPECIALIDAD: $localStorage.medico.nombreEspecialidad
		};
	};

  });