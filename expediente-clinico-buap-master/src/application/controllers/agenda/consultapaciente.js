(function(){
'use strict';

/**
* @ngdoc function
* @nombre ECEangular.module('expediente').controller:AgendaConsultapacienteCtrl
* @description
* # AgendaConsultapacienteCtrl
* Controller of the expediente
*/
angular.module('expediente').controller('AgendaConsultapacienteCtrl', ['$scope','$http','pacientes','$log','$state','mensajes', '$location','carritoCompras','$stateParams','$localStorage','consultas','usuario','medicos','$q','$mdDialog','blockUI', 'peticiones', '$window' , 'reportes',
  function ($scope,$http,pacientes,$log,$state,mensajes, $location,carritoCompras,$stateParams,$localStorage,consultas,usuario,medicos,$q,$mdDialog,blockUI, peticiones, $window , reportes) {

  $scope.title="CONSULTAR PACIENTE";
  $scope.busqueda='';
  $localStorage.notCita = false;
  $scope.simulateQuery = false;
  $scope.isDisabled    = false;

  $scope.nombres;
  var textoBuscado='';
  $scope.selected = [];
  $scope.existDatos = true;

  $localStorage.showExpedienteClinico = false;
  if($stateParams.state === 'notaMedicaIngreso'){
    iniciaConcentimientosInformados();
    var myBlockUI = blockUI.instances.get('blockConsulta');
    myBlockUI.start();
    $scope.isTriage = true;
    medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
    .success(function(data){
      if(data === ''){
        mensajes.alerta('ERROR','ERROR INTERNO DEL SISTEMA INTENTE NUEVAMENTE','ACEPTAR!');
      }
      else{
        if(data.ESPECIALIDADES != undefined){
          if(data.ESPECIALIDADES.CF_ESPECIALIDAD.toUpperCase() === "PEDIATRIA"){
            $scope.isPediatria = true;
            $scope.titulo = "PEDIATRICO";
          }
          else if(data.ESPECIALIDADES.CF_ESPECIALIDAD.toUpperCase() === "GINECOLOGIA"){
            $scope.isGinecologia = true;
            $scope.titulo = "OBSTETRICIA";
          }
          else {
            $scope.isAdultos = true;
            $scope.titulo = "ADULTOS";
          }
        }
        else{
          if(data.C_PERFIL.toUpperCase() === "PEDIATRIA"){
            $scope.isPediatria = true;
            $scope.titulo = "PEDIATRICO";
          }
          else if(data.C_PERFIL.toUpperCase() === "GINECOLOGIA"){
            $scope.isGinecologia = true;
            $scope.titulo = "OBSTETRICIA";
          }
          else {
            $scope.isAdultos = true;
            $scope.titulo = "ADULTOS";
          }
        }
      }
    })
    .error(function(data){
      mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
    });

    //Rojos Interconsulta, Evolución y Alta, Triage ,Urgencias
    getRojos();

    //Amarillos
    getAmarillos();
    myBlockUI.stop();

  }

  $scope.loadIni  = function(){
    var myBlockUI = blockUI.instances.get('blockConsulta');
    myBlockUI.start();

    if($stateParams.state === 'notaMedicaIngreso'){
      iniciaConcentimientosInformados();
      $scope.isTriage = true;
      medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
      .success(function(data){
        if(data === ''){
          mensajes.alerta('ERROR','ERROR INTERNO DEL SISTEMA INTENTE NUEVAMENTE','ACEPTAR!');
        }
        else{
          if(data.ESPECIALIDADES != undefined){
            if(data.ESPECIALIDADES.CF_ESPECIALIDAD.toUpperCase() === "PEDIATRIA"){
              $scope.isPediatria = true;
              $scope.titulo = "PEDIATRICO";
            }
            else if(data.ESPECIALIDADES.CF_ESPECIALIDAD.toUpperCase() === "GINECOLOGIA"){
              $scope.isGinecologia = true;
              $scope.titulo = "OBSTETRICIA";
            }
            else {
              $scope.isAdultos = true;
              $scope.titulo = "ADULTOS";
            }
          }
          else{
            if(data.C_PERFIL.toUpperCase() === "PEDIATRIA"){
              $scope.isPediatria = true;
              $scope.titulo = "PEDIATRICO";
            }
            else if(data.C_PERFIL.toUpperCase() === "GINECOLOGIA"){
              $scope.isGinecologia = true;
              $scope.titulo = "OBSTETRICIA";
            }
            else {
              $scope.isAdultos = true;
              $scope.titulo = "ADULTOS";
            }
          }
        }
      })
      .error(function(data){
        mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
      });
      //Rojos Interconsulta, Evolución y Alta, Triage ,Urgencias
      getRojos();
      //Amarillos
      getAmarillos();
      myBlockUI.stop();
    }
  };

  function getRojos(){//URGENCIAS, TRIAGE, SOLICITUDES INTERCONSULTA Y EVOLUCIÓN Y ALTA
    //Rojos de Triage
    var myBlockUI = blockUI.instances.get('myBlockUI');
    myBlockUI.start();
    var defered = $q.defer();
    var promise = defered.promise;
    consultas.getPacientesUrgencias(usuario.usuario.NP_EXPEDIENTE)//ROJOS DE CASO EXTREMO
    .success(function(data){
      if(data.length>0){



/*
        $scope.datos2 = data;
        $scope.datos2 = _.map($scope.datos2, function (el, key) {
          el.PACIENTE = {};
          el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
          el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
          el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
          el.idOrder = 3;
          return el;
        });
        $scope.datos2 = _.filter(data,function(e){
          return e.CF_TIPO_NOTA !== 'URGENCIAS';
        });

       */





      }
      consultas.getPacientesUrgencias(usuario.usuario.NP_EXPEDIENTE , "rojos")//URGENCIAS
      .success(function(data){

     
        $scope.datos2 = _.union($scope.datos2,data);
        $scope.datos2 = _.map($scope.datos2, function (el, key) {
          el.PACIENTE = {};
          el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
          el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
          el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
          el.idOrder = 3;
          return el;
        });


        pacientes.getPacientesUrgencias('rojo' , usuario.usuario.NP_EXPEDIENTE)//TRIAGE
        .success(function(data){
          if(data.data != undefined){
            if (data.data.length > 0){
              var rojosEspera = _.map(data.data, function (el, key) {
                el.CF_TIPO_NOTA = 'ESPERA';
                el.idOrder = 2;
                return el;
              });
              $scope.datos2 = _.union($scope.datos2,rojosEspera);
            }
          }
          /*consultas.getSolicitudesInterconsulta('rojos')//INTERCONSULTA
          .success(function(data){
            var interconsultas = _.map(data, function (el, key) { //SE AGREGA EL TIPO DE NOTA S_INTERCONSULTA = SOLICITUD DE INTERCONSULTA
              el.CF_TIPO_NOTA = 'S_INTERCONSULTA';
              el.idOrder = 1;
              return el;
            });
            interconsultas = _.filter(interconsultas,function(e){ //SE QUITAN LAS SOLICITUDES QUE EL MÉDICO SOLICITÓ
              return e.NF_MEDICO_ATIENDE+'' === usuario.usuario.NP_EXPEDIENTE+'';
            });

            interconsultas = _.map(interconsultas, function (el, key) {
              el.PACIENTE = {};
              el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
              el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
              el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
              return el;
            });
            $scope.datos2 = _.union($scope.datos2,interconsultas);*/
            consultas.getEvolucionAlta(usuario.usuario.NP_EXPEDIENTE , 'rojos')//EVOLUCIÓN Y ALTA
            .success(function(data){
              data = _.map(data, function (el, key) {
                el.PACIENTE = {};
                el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
                el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
                el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
                el.idOrder = 4;
                return el;
              });
              $scope.datos2 = _.union($scope.datos2,data); //Froy eliminado
              




              //ROJOS DE CASO EXTREMO
              /*consultas.getSolicitudesInterconsulta()//INTERCONSULTA
              .success(function(data){
                var interconsultas = _.map(data, function (el, key) { //SE AGREGA EL TIPO DE NOTA S_INTERCONSULTA = SOLICITUD DE INTERCONSULTA
                  el.CF_TIPO_NOTA = 'S_INTERCONSULTA';
                  el.idOrder = 1;
                  return el;
                });
                interconsultas = _.filter(interconsultas,function(e){ //SE QUITAN LAS SOLICITUDES QUE EL MÉDICO SOLICITÓ
                  return e.NF_MEDICO_ATIENDE+'' === usuario.usuario.NP_EXPEDIENTE+'';
                });

                interconsultas = _.map(interconsultas, function (el, key) {
                  el.PACIENTE = {};
                  el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
                  el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
                  el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
                  return el;
                });
                $scope.datos2 = _.union($scope.datos2,interconsultas);*/
                consultas.getEvolucionAlta(usuario.usuario.NP_EXPEDIENTE)//EVOLUCIÓN Y ALTA
                .success(function(data){
                  data = _.map(data, function (el, key) {
                    el.PACIENTE = {};
                    el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
                    el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
                    el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
                    el.idOrder = 4;
                    return el;
                  });
                  $scope.datos2 = _.union($scope.datos2,data);

                  defered.resolve();
                /*})
                .error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');myBlockUI.stop();});
              })
              .error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');myBlockUI.stop();});*/
            })
            .error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');myBlockUI.stop();});
          })
          .error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');myBlockUI.stop();});
        })
        .error(function(data){myBlockUI.stop();});
      })
      .error(function(data){myBlockUI.stop();});
    })
    .error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');defered.reject();myBlockUI.stop();});
    myBlockUI.stop();
    return promise;
  };


  function getAmarillos(){
    consultas.getPacientesUrgencias(usuario.usuario.NP_EXPEDIENTE , "amarillos")//Se traen los pacientes de Urgencias amarillos
    .success(function(data){

      console.debug("Pacientes de urgencias amarillos",data);

      $scope.amarillos = data;
      $scope.amarillos = _.map($scope.amarillos, function (el, key) {
        el.PACIENTE = {};
        el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
        el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
        el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
        el.idOrder = 3;
        console.debug(" Pacientes de urgencias amarillos  -> el", el);
        //el.T_FECHA_CMD = (el.T_FECHA_CMD != " ") ? moment(new Date("02/21/2016 21:54:04")).format('DD/MM/YYYY') : "";
        // el.T_FECHA_CMD = "02/21/2016 21:53:04";
        return el;
      });
     // $scope.$apply();
      pacientes.getPacientesUrgencias('amarillo' , usuario.usuario.NP_EXPEDIENTE)//Se manda a traer los triage amarillos
      .success(function(data){

        console.debug("Pacientes de triage amarillo", data);
        var amarillosEspera = _.map(data.data, function (el, key) {
          el.CF_TIPO_NOTA = 'ESPERA';
          el.idOrder = 2;
          //el.T_FECHA_CMD
           // el.T_FECHA_CMD = (el.T_FECHA_CMD != " ") ? moment(new Date("03/13/2016 21:54:04")).format('DD/MM/YYYY') : "";
            // el.T_FECHA_CMD = "01/13/2016 21:53:04";
          
          return el;
        });
        $scope.amarillos = _.union($scope.amarillos,amarillosEspera);

       // $scope.$apply();
        /*consultas.getSolicitudesInterconsulta('amarillos')//INTERCONSULTA
        .success(function(data){
          var interconsultas = _.map(data, function (el, key) { //SE AGREGA EL TIPO DE NOTA S_INTERCONSULTA = SOLICITUD DE INTERCONSULTA
            el.CF_TIPO_NOTA = 'S_INTERCONSULTA';
            el.idOrder = 1;
            return el;
          });
          interconsultas = _.filter(interconsultas,function(e){ //SE QUITAN LAS SOLICITUDES QUE EL MÉDICO SOLICITÓ
            return e.NF_MEDICO_ATIENDE+'' === usuario.usuario.NP_EXPEDIENTE+'';
          });

          interconsultas = _.map(interconsultas, function (el, key) {
            el.PACIENTE = {};
            el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
            el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
            el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
            return el;
          });
          $scope.amarillos = _.union($scope.amarillos,interconsultas);*/
          consultas.getEvolucionAlta(usuario.usuario.NP_EXPEDIENTE , 'amarillos')//EVOLUCIÓN Y ALTA
          .success(function(data){
            console.debug("Pacientes de evolución y alta" , data);
            data = _.map(data, function (el, key) {
              el.PACIENTE = {};
              el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
              el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
              el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
              el.idOrder = 4;
              //el.T_FECHA_CMD = (el.T_FECHA_CMD != " ") ? moment(new Date("02/15/2016 21:53:04")).format('DD/MM/YYYY') : "";
             //  el.T_FECHA_CMD = "01/13/2016 19:53:04";
              
              return el;
            });
            $scope.amarillos = _.union($scope.amarillos,data);
//$scope.$apply();
          /*})
          .error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');});*/
        })
        .error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');});
      })
      .error(function(data){});
    })
    .error(function(data){});
  };

  $scope.querySearch = function(query) {
    if(query.length != undefined){
      textoBuscado = query;
      if(textoBuscado.indexOf('/') != -1 || textoBuscado.indexOf('-') != -1){
        textoBuscado= textoBuscado.replace("/","");
        textoBuscado= textoBuscado.replace("-","");
      }
      if(textoBuscado.length > 2){
        var defered = $q.defer();
        var promise = defered.promise;
        if(hasNumbers(query)){
          return $scope.nombres = [];
        }
        
        // pacientes.getPacientes(textoBuscado)
        // .success(function(data){
        //   $scope.isDisabled = false;
        //   $scope.nombres = $scope.cargarServicios(data.data);
        //   defered.resolve($scope.nombres);
        // })
        // .error(function(data){
        //   mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
        //   defered.reject({});
        // });


        pacientes.getListadoPacientes(textoBuscado)
              .then(function(result){
                console.log(result)
                defered.resolve(result);
                self.isDisabled = false;
              })
              .catch(function(error){
                console.error(error)
                mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
                defered.reject({});
              })

        return promise
      }
    }
  }

  $scope.selectedItemChange = function(item) {
    if(item!==undefined){
      textoBuscado=item.id;
    }
  }

  $scope.cargarServicios = function(data){
    if(data!==undefined){
      var a = data.map(function (serv){
        return {
          value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE+" - "+(serv.NP_ID)+" " + (serv.C_CURP ? " - "+serv.C_CURP : '')).toUpperCase(),
          display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE+" - "+(serv.NP_ID)+" " + (serv.C_CURP ? " - "+serv.C_CURP : '')).toUpperCase(),
          nombre: serv.C_NOMBRE,
          id  : serv.NP_EXPEDIENTE
        };
      });
      return a;
    }
  };

  $scope.selected = [];

  $scope.query = {
    filter: '',
    order: 'idOrder',
    limit: 15,
    page: 1
  };

  $scope.query2 = {
    filter: '',
    order: 'idOrder',
    limit: 15,
    page: 1
  };

  $scope.query3 = {
    filter: '',
    order: '',
    limit: 15,
    page: 1
  };

  $scope.clear = function(){
    $scope.datos = [];
    $scope.nombres = [];
    $scope.searchText = '';
    $scope.existDatos = true;

  }

  $scope.datos =[];

  $scope.loadTablePacientes = function(){
    $scope.datos = [];
    if(textoBuscado.indexOf('/') != -1 || textoBuscado.indexOf('-') != -1){
      textoBuscado= textoBuscado.replace("/","");
      textoBuscado= textoBuscado.replace("-","");
    }

    pacientes.getPacientes(textoBuscado)
    .success(function(data){
      if(data.success===true){
        if(angular.isArray(data.data)){
          $scope.datos=data.data;
          if($scope.datos.length > 0){
            $scope.existDatos = true;
          }
          else{
            $scope.existDatos = false;
          }
        }
        else{
          $scope.datos = [data.data];
        }
      }
      else if(data.success===false){
        mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
      }
      else{
        mensajes.alerta('AVISO','NO EXISTEN USUARIOS','ACEPTAR!');
      }
      $scope.query.page=1;
    })
    .error(function(data){
      mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    });
  };

  $scope.verPaciente = function(ev,dato) {
    if($localStorage.paciente != undefined){
      if($localStorage.paciente.expediente !== dato.NP_EXPEDIENTE.replace("/","")){
        $localStorage.selectedIndex = 0;
      }
    }
    carritoCompras.reset();
    var bande_derecho = (dato.C_CLAVE_TIPO_PACIENTE == "D"  || dato.C_CLAVE_TIPO_PACIENTE == "B") ? true : false;
    $localStorage.paciente={
      idexpediente: dato.NP_EXPEDIENTE.split("/")[0],
      expediente: dato.NP_EXPEDIENTE.replace("/",""),
      nombrePaciente: dato.C_PRIMER_APELLIDO+" "+dato.C_SEGUNDO_APELLIDO+" "+dato.C_NOMBRE,
      idPaciente : dato.NP_ID,
      curp : dato.C_CURP,
      genero : dato.NF_SEXO,
      f_nacimiento: dato.F_FECHA_DE_NACIMIENTO,
      expedienteSIU : dato.NP_EXPEDIENTE_SIU,
      DERECHO_HABIENTE: bande_derecho
    };
    if($stateParams.state === 'notaMedicaIngreso'){
      $localStorage.paciente.tipo="ingreso";
      $localStorage.notaMedica = undefined;
    }
    $localStorage.paciente.contador = 1;
    $state.go($stateParams.state);
  };

  $scope.abrirEvolutiva = function(paciente){
    if($localStorage.paciente != undefined){
      if($localStorage.paciente.expediente+'' != paciente.expediente+'' || $localStorage.paciente.tipo != 'EVOLUTIVA'){
        $localStorage.selectedIndex = 0;
        $localStorage.notaMedica = undefined;
      }
    }
    if(paciente.NF_ESTATUS === 1){
      $localStorage.notaMedica = {NP_NOTA_MEDICA : paciente.NF_NOTA_MEDICA}
    }
    $localStorage.paciente = paciente;
    $localStorage.paciente.tipo = 'EVOLUTIVA';
    $localStorage.paciente.idexpediente = $localStorage.paciente.NF_PACIENTE.split("/")[0];
    $localStorage.paciente.expediente = $localStorage.paciente.NF_PACIENTE.replace("/","");
    $state.go('notaMedicaIngreso');
  };

  $scope.abrirSolicitarInterconsulta = function(paciente){
    if($localStorage.paciente != undefined){
      if($localStorage.paciente.expediente+'' != paciente.expediente+''){
        $localStorage.selectedIndex = 0;
        $localStorage.notaMedica = undefined;
      }
    }
    $localStorage.paciente = paciente;
    $localStorage.paciente.idexpediente = $localStorage.paciente.NF_PACIENTE.split("/")[0];
    $localStorage.paciente.expediente = $localStorage.paciente.NF_PACIENTE.replace("/","");
    $state.go('solicitudInterconsulta');
  };

  $scope.abrirInterconsulta = function(paciente){
    if($localStorage.paciente != undefined){
      if($localStorage.paciente.expediente+'' != paciente.expediente+'' || $localStorage.paciente.tipo != 'INTERCONSULTA'){
        $localStorage.selectedIndex = 0;
        $localStorage.notaMedica = undefined;
      }
    }
    if(paciente.NF_ESTATUS === 1){
      $localStorage.notaMedica = {NP_NOTA_MEDICA : paciente.NF_NOTA_MEDICA}
    }
    $localStorage.paciente = paciente;
    $localStorage.paciente.tipo = 'INTERCONSULTA';
    $localStorage.paciente.idexpediente = $localStorage.paciente.NF_PACIENTE.split("/")[0];
    $localStorage.paciente.expediente = $localStorage.paciente.NF_PACIENTE.replace("/","");
    $state.go('notaMedicaIngreso');
  };

  $scope.abrirEvolucionAlta = function(paciente){
    if($localStorage.paciente != undefined){
      if($localStorage.paciente.expediente+'' != paciente.expediente+'' || $localStorage.paciente.tipo != 'EVOLUCION Y ALTA'){
        $localStorage.selectedIndex = 0;
        $localStorage.notaMedica = undefined;
      }
    }
    if(paciente.NF_ESTATUS === 1){
      $localStorage.notaMedica = {NP_NOTA_MEDICA : paciente.NF_NOTA_MEDICA}
    }
    $localStorage.paciente = paciente;
    $localStorage.paciente.tipo = 'EVOLUCION Y ALTA';
    $localStorage.paciente.idexpediente = $localStorage.paciente.NF_PACIENTE.split("/")[0];
    $localStorage.paciente.expediente = $localStorage.paciente.NF_PACIENTE.replace("/","");
    $state.go('notaMedicaIngreso');
  };

  $scope.abrirCancelar = function(paciente){
    $localStorage.NOTA_CANCELAR = paciente;
    console.debug("---------------------",paciente);
    $mdDialog.show({
      templateUrl: '/application/views/urgencias/cancelarevolucionalta.html',
      parent: angular.element(document.body),
      clickOutsideToClose:true,
    });
  };

  $scope.abrirAgendaMedicaIngreso = function(dato){
    $localStorage.selectedIndex = 0;
    if(dato.NF_ESTATUS == 1){
      $localStorage.paciente = {
        idexpediente:   dato.NF_PACIENTE,
        expediente:   dato.NF_PACIENTE.replace("/",""),
        nombreCompleto: dato.C_PRIMER_APELLIDO + ' ' + dato.C_SEGUNDO_APELLIDO + ' ' + dato.C_NOMBRE,
        idTriage:     dato.NP_TRIAGE,
        tipo : 'ingresoTriage',
      };
      $localStorage.notaMedica = {NP_NOTA_MEDICA : dato.NF_NOTA_MEDICA}
    }
    else{
      $localStorage.paciente = {
        idexpediente:   dato.PACIENTE.NP_EXPEDIENTE,
        expediente:   dato.PACIENTE.NP_EXPEDIENTE.replace("/",""),
        nombreCompleto: dato.PACIENTE.C_PRIMER_APELLIDO + ' ' + dato.PACIENTE.C_SEGUNDO_APELLIDO + ' ' + dato.PACIENTE.C_NOMBRE,
        idTriage:     dato.NP_TRIAGE,
        tipo : 'ingresoTriage',
      };
      $localStorage.notaMedica = undefined;
    }
    $state.go("consultaNotaMedica");
  };

  function hasNumbers(t){
    return /\d/.test(t);
  }

  $scope.goBack = function (){
    $mdDialog.hide();
    $localStorage.NOTA_CANCELAR = undefined;
  }

  $scope.sendCancelacion = function(){
    $scope.cancelNota = {NP_NOTA_MEDICA:$localStorage.NOTA_CANCELAR.NF_NOTA_MEDICA,C_MOTIVO_CANCELACION:$scope.nota.C_MOTIVO_CANCELACION};
    console.debug($scope.cancelNota);
    consultas.cancelJustificacionUrgencias($scope.cancelNota).success(function(data){
      if (data.success){
        mensajes.alerta("AVISO!","SE HA CANCELADO LA EVOLUCIÓN Y ALTA","ACEPTAR!");
        console.log(data);
        $state.reload();
      }
      else{
        mensajes.alerta("AVISO!",data.Mensaje.toUpperCase(),"ACEPTAR!");
      }
    }).error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');});
     $localStorage.NOTA_CANCELAR = undefined;
  };


  $scope.abrirNotaEgreso = function(dato) {
    $localStorage.paciente = {
      expediente : dato.NF_PACIENTE.replace("/",""),
      NF_NOTA_MEDICA : dato.NF_NOTA_MEDICA
    };
    $localStorage.regresarA = 'U';
    $state.go("notaEgreso");
  }

  $scope.printTriage = function(dato){
    reportes.getReporte(baseURL + 'getpdf/' + dato.C_MODULO_TRIAGE + '/' + dato.C_FILE_TRIAGE , '_blank' , 'width=800, height=640');
  };


  $scope.queryConsentimiento = {
    filter: '',
    order: '',
    limit: 5,
    page: 1
  };

  $scope.consentimientosInformados = [];

  function iniciaConcentimientosInformados(){
    $http.get(baseURL + 'notamedica/pacientesConsentimiento/0/'+usuario.usuario.NP_EXPEDIENTE).success(function(data){


      $scope.consentimientosInformados = data;






    }).error(function(err){

    });
  }

  



  $scope.uploadConsentiminetoInformado = function(consentimientoInformado, ev){
    var registro = {};
    var tipo = {};
    $mdDialog.show({
      controller: uploadConcentimientoCtrl,
      templateUrl: '/application/views/quirofano/subirarchivo.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      locals:{
        registro: consentimientoInformado,
      }
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });





  }

  $scope.downloadConsentiminetoInformado = function(consentimientoInformado){


    var url = consentimientoInformado.C_FILE_CONSENTIMIENTO.toUpperCase().indexOf('PDF') > 0 ? 'downloadpdf/' : 'download/';

    switch (consentimientoInformado.CF_HOSPITALIZACION) {
      case 'HOSPITALIZACION':
      url += 'consentimientoHospitalizacion/'
      break;

      case 'URGENCIAS':
      url += 'consentimientoUrgencias/'

      break;

    }

    url += consentimientoInformado.NF_NOTA_MEDICA;

    if(url.indexOf('pdf') > 0){
      reportes.getReporte(baseURL + url, '_blank', 'width=1000, height=800');
    }else{
      peticiones.getDatos(url)
      .then(function(data) {
        $scope.image=data;
        $scope.ruta=baseURL+url;
        $mdDialog.show({
          controller: DialogControllerDownload,
          templateUrl: '/application/views/quirofano/descargar.html',
          parent: angular.element(document.body),
          clickOutsideToClose:true,
          locals:{
            image: $scope.image,
            ruta:$scope.ruta
          }
        })
        .then(function(answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
          $scope.status = 'You cancelled the dialog.';
        });
      })
      .catch(function(err) {
        mensajes.alerta("ALERTA","NO EXISTE ARCHIVO","OK");
      });
    }


  };

  function DialogControllerDownload($scope, $mdDialog,image,ruta) {
    $scope.cancelar = function() {
      $mdDialog.hide();
    };
    $scope.image=image;
    $scope.ruta=ruta;

  };

  function uploadConcentimientoCtrl($scope, $mdDialog, registro, $timeout, peticiones) {
    var registro=registro;

    $scope.cancelar = function() {
      $mdDialog.hide();
    };
    $scope.guardar = function() {
      $mdDialog.hide();
    };

    $scope.uploadPic = function(file) {

      var urlSave = baseURL + "consentimiento/"
      var url="upload/";

      switch (registro.CF_HOSPITALIZACION) {
        case 'HOSPITALIZACION':
        url += 'consentimientohospitalizacion/'
        urlSave += "hospitalizacion/";
        break;

        case 'URGENCIAS':
        url += 'consentimientourgencias/'
        urlSave += "urgencias/";

        break;

      }

      url += registro.NF_NOTA_MEDICA;
      urlSave += registro.NF_NOTA_MEDICA;

      $http.get(urlSave).success(function(dataD){

        peticiones.uploadArchivo(url, file).then(function (data) {

          iniciaConcentimientosInformados();

          mensajes.alerta("EXITO","SE SUBIO CORRECTAMENTE","OK");



        }).catch(function(err){
          console.error(err);
        });

      }).error(function(err){

      });






    };

  };

}]);
})();