'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaReportesAgendaCtrl
 * @description
 * # AgendaReportesAgendaCtrl
 * Controller of the expediente
 */
 angular.module('expediente')
 .controller('AgendaReportesAgendaCtrl', function ($scope, $http, mensajes,reportes) {



    $scope.generaReporte = function(){
    	var dateRange = moment($scope.agenda.fechaInicial).format('DDMMYYYY') + '/' + 
            moment($scope.agenda.fechaFinal).format('DDMMYYYY');

        $http.get(baseURL + 'agendaTotal/contiene/' + dateRange
            
            ).success(function(data){
                if(data.Mensaje > 0){
                    reportes.getReporte(baseURL + 'agendaTotal/'+ dateRange
                        , '_blank', 'width=900, height=600');
                }else{
                    mensajes.datoNoEncontrado();
                }
 
            });



        };
    });
