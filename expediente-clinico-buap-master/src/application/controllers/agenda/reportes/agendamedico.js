(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaReportesAgendamedicoCtrl
 * @description
 * # AgendaReportesAgendamedicoCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('AgendaReportesAgendamedicoCtrl', ['$scope', 'medicos' , 'reportes', 'blockUI', function ($scope, medicos , reportes, blockUI) {

    $scope.query 		= {
  		filter: '',
  	    order: '',
  	    limit: 15,
  	    page: 1
    	};
      var myBlockUI = blockUI.instances.get('myBlockUI');
      	myBlockUI.start();
  	medicos.getAgendaHoy().then(function(data){
  		console.log(data);
  		$scope.datos = data;
      $scope.registros = data;
      myBlockUI.stop();
  	}).catch(function(err){

      myBlockUI.stop();

    });



    $scope.buscaMedico = function(text){
      if(text != undefined){
          $scope.datos = _.filter($scope.registros , function(e){

            text = text.toUpperCase();
            return e.CF_ESPECIALIDAD.indexOf(text) >= 0 || e.CF_MEDICO.indexOf(text) >= 0 ||
            e.NF_MEDICO_ANIO.indexOf(text) >= 0;
          });
        }
        else{
          $scope.datos = $scope.registros;
        }
    };

     $scope.generaReporte = function(event, medico){


     	// agendaM/{idMedico}/{fechaInicio}

     	reportes.getReporte(baseURL + 'agendaM/' + medico.NF_MEDICO_ANIO.split('/')[0] + '/' + moment().format('DDMMYYYY'), '_blank' , 'width=800, height=640');

     	/*
    	var dateRange = moment($scope.agenda.fechaInicial).format('DDMMYYYY') + '/' +
            moment($scope.agenda.fechaFinal).format('DDMMYYYY');

        $http.get(baseURL + 'agendaTotal/contiene/' + dateRange

            ).success(function(data){
                if(data.Mensaje > 0){
                    reportes.getReporte(baseURL + 'agendaTotal/'+ dateRange
                        , '_blank', 'width=900, height=600');
                }else{
                    mensajes.datoNoEncontrado();
                }

            });


*/
        };

  }]);
})();
