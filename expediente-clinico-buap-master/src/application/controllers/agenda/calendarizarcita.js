'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:CalendarizarcitaCtrl
 * @description
 * # CalendarizarcitaCtrl
 * Controller of the expediente
 */

  angular.module('expediente').controller('CalendarizarcitaCtrl', function ($scope, medicos, $location,pacientes,$mdDialog,mensajes,$state,usuario,$localStorage,blockUI,reportes) {

    $scope.citaEspecial=false;
    $scope.preCita=false;
    $scope.tituloCita="CALENDARIZAR CITA";

    $scope.calendarizarCita = function(){
      $scope.cita=$localStorage.cita;
    };

    
    $scope.cancelar = function(){
      $localStorage.cita=undefined;
      $localStorage.paciente=undefined;
      $localStorage.medico=undefined;
      $state.go('home');
    }

    $scope.registrarDatos = function(ev) {
      var myBlockUI = blockUI.instances.get('myBlockUI');
      myBlockUI.start();
      $scope.cita.F_FECHA_HORA = $localStorage.cita.dia+" "+$localStorage.cita.horario;
      $scope.cita.NF_ESPECIALIDAD = $localStorage.cita.medico.idEspecialidad;
      $scope.cita.NF_PACIENTE = $localStorage.cita.paciente.expediente;
      $scope.cita.NF_PERSONAL= $localStorage.cita.medico.idMedico;
      $scope.cita.NF_CAPTURISTA = usuario.usuario.NP_EXPEDIENTE;
      $scope.cita.NF_TURNO = $scope.cita.medico.idTurno;
      $scope.cita.C_OBSERVACIONES = $scope.cita.C_OBSERVACIONES;
      pacientes.guardaCita($scope.cita)
      .success(function(data){
        if(data.success===true){
          myBlockUI.stop();
          reportes.getReporte(baseURL + 'ticket_exportar/'+data.Mensaje, '_blank', 'width=500, height=400');
          $localStorage.cita=undefined;
          $localStorage.paciente=undefined;
          $localStorage.medico=undefined;
          $state.go('bienvenido');
        }
        else if(data.success===false){
          myBlockUI.stop();
          mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
        }
        else{
          myBlockUI.stop();
          mensajes.alerta('ERROR',"ERROR AL GENERAR EL TICKET",'ACEPTAR!');
        }
      })
      .error(function(data){
        myBlockUI.stop();
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      });
    };


  });

  function addZero(i) {
      if (i < 10) {
          i = "0" + i;
      }
      return i;
  }
