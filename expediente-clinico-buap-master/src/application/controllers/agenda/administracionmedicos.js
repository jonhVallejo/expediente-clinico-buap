(function(){
'use strict';

/**
* @ngdoc function
* @name ECEangular.module('expediente').controller:AgendaAdministracionmedicosCtrl
* @description
* # AgendaAdministracionmedicosCtrl
* Controller of the expediente
*/
angular.module('expediente').controller('AgendaAdministracionmedicosCtrl',['$scope', 'pacientes', 'mensajes', 'usuario', '$localStorage', 'medicos', 'peticiones', '$q', 'blockUI', '$mdDialog', 
  function ($scope, pacientes, mensajes, usuario, $localStorage, medicos, peticiones, $q, blockUI, $mdDialog) {


  $scope.states = {
    TABLA: 			0,
    EDITAR: 		1,
    ALTA: 			2,
    HORARIO: 		3,
    HORARIO_P: 		4,
    BUSQUEDA: 		5,
    NUEVO_USUARIO: 	6,
    UNDEFINED: 		-1
  };


  $scope.categorias = [];

  $scope.categorias.push({descripcion: 'CATEGORIA 1', selected: false});
  $scope.categorias.push({descripcion: 'CATEGORIA 2', selected: false});
  $scope.categorias.push({descripcion: 'CATEGORIA 3', selected: false});
  $scope.categorias.push({descripcion: 'CATEGORIA 4', selected: false});
  $scope.categorias.push({descripcion: 'CATEGORIA 5', selected: false});

  $scope.allCategories = false;

  $scope.buscar = [];

  $scope.state = $scope.states.NUEVO_USUARIO;

  $scope.datos =[];


  $scope.editar = false;
  $scope.alta = false;
  $scope.tabla=true;
  $scope.horario = false;
  $scope.horario_p = false;
  $scope.busqueda = true;
  $scope.nuevoUsuario=false;



  var isValid=false;

  $scope.paciente = {};

  $scope.selected = [];
  $scope.query = {
    filter: '',
    order: 'nombre',
    limit: 15,
    page: 1
  };

  $scope.nombres = [];
  $scope.simulateQuery = false;
  $scope.isDisabled    = false;
  $scope.querySearch   = querySearch;
  $scope.selectedItemChange = selectedItemChange;
  var textoBuscado='';


  $scope.categoriasMedicos = [
    {
      id: 'A',
      descripcion: 'CONSULTA'
    },
    {
      id: 'B',
      descripcion: 'CONSULTA, INTERCONSULTA'
    },
    {
      id: 'C',
      descripcion: 'CONSULTA, INTERCONSULTA Y DIA QUIRURGICO'
    },
    {
      id: 'R',
      descripcion: 'MEDICOS RESIDENTES'
    }];

    $scope.currentItem = $scope.categoriasMedicos[0];

    $scope.toggle = function (item) {
      $scope.currentItem = item;
    };
    $scope.exists = function (item) {
      return $scope.currentItem === item;
    };


  function success(datos) {
    $scope.datos = datos;
  }

  $scope.search = function (predicate) {
    $scope.filter = predicate;
    $scope.deferred = $scope.datos.get($scope.query, success).$promise;
  };

  $scope.click = function(){
    $scope.alta_p = true;
    $scope.busqueda = false;

  }

  $scope.clickMedico = function(){


    $scope.alta_p = false;
    $scope.alta = true;
    $scope.busqueda = false;
    $scope.tabla=false;
    $scope.paciente = {};
  }



  function querySearch (query) {
    if(query!==undefined || query != null){
      var defered = $q.defer();
      var promise = defered.promise;
      if(query.length >= 3){
        isValid=false;
        textoBuscado=query;
        if(textoBuscado.indexOf('/') != -1 || textoBuscado.indexOf('-') != -1){
          textoBuscado= textoBuscado.replace("/","");
          textoBuscado= textoBuscado.replace("-","");
        }
        pacientes.getPacientes(textoBuscado)
        .success(function(data){
          console.log(data);
          $scope.isDisabled = false;
          $scope.nombres = cargarServicios(data.data);
          if($scope.nombres != undefined)
          defered.resolve($scope.nombres);
          else
          defered.resolve([]);

        })
        .error(function(data){
          mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
        });
      }
      else{
        defered.reject({});
        $scope.nombres = [];
      }
    }
    return promise;
  }

  function selectedItemChange(item) {
    if(item!==undefined){
      textoBuscado=item.id;
      $scope.usuario= {
        IDPACIENTE: textoBuscado
      }
      isValid=true;
    }
  }

  function cargarServicios(data){
    if(data!==undefined){
      return data.map(function (serv){
        return {
          value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase() + " "+serv.NP_ID,
          display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase() + " "+serv.NP_ID,
          nombre: serv.C_NOMBRE,
          id  : serv.NP_EXPEDIENTE
        };
      });
    }
  };

  $scope.loadPerfiles = function() {
    $scope.catPerfiles = [];
    usuario.getPerfiles("2").success(function(data){
      $scope.catPerfiles=data;
    });
  };

  $scope.addRol = function(){

    $scope.submiting = true;

    var defered = $q.defer();
    var promise = defered.promise;
    $scope.diasActividad = '';

    if($scope.editing){

      if (regCompleto()) {




        $scope._especialidad = {
          NF_USUARIO  : parseInt(usuario.usuario.NP_EXPEDIENTE),
          NP_PERSONAL : currentRowForEdit.N_EXPEDIENTE_ANIO.replace("/",""),
          NP_ESPECIALIDAD : $scope.medico.especialidad,
          C_CEDULA:$scope.medico.CEDULA_ESP,
          NP_TURNO : $scope.medico.turno,
          C_HORARIO: $scope.medico.HORARIO_LAB,
          N_DURACION : $scope.medico.tiempoSelec,
          N_TOTAL : $scope.medico.consultas,
          C_DIAS_ACTIVIDAD : $scope.diasActividad,
          N_GENERA_INCAPACIDADES: $scope.medico.INCAPACIDADES ? 1 : 0,
          N_ORDENES_ESTUDIO: $scope.medico.ORDEN_ESTUDIOS ? 1 : 0,
          N_RECETA: $scope.medico.N_RECETA ? 1: 0,
          N_HOSPITALIZA : $scope.medico.N_HOSPITALIZA ? 1: 0,
          C_CATEGORIA: $scope.currentItem.id
        };

        if($scope.medico.N_RECETA){
          $scope._especialidad.N_CATEGORIA_1 = $scope.categorias[0].selected ? 1 : 0;
          $scope._especialidad.N_CATEGORIA_2 = $scope.categorias[1].selected ? 1 : 0;
          $scope._especialidad.N_CATEGORIA_3 = $scope.categorias[2].selected ? 1 : 0;
          $scope._especialidad.N_CATEGORIA_4 = $scope.categorias[3].selected ? 1 : 0;
          $scope._especialidad.N_CATEGORIA_5 = $scope.categorias[4].selected ? 1 : 0;

        }




        if(true){



          $scope.submiting = false;

        }
        medicos.actualizaEspecialidad($scope._especialidad)
        .success(function(data){
          if(data.success === true){
            $scope.loadTable();
            mensajes.alerta('AVISO','LOS CAMBIOS HAN SIDO GUARDADOS CON EXITO','ACEPTAR!');
            $scope.inicializaEstado();
            $scope.editing = false;
          }
          else{
            mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
          }
          defered.resolve();
        })
        .error(function(data){
          mensajes.alerta('AVISO','SERVICIO NO DISPONIBLE','ACEPTAR!');
          defered.reject();
        });
      }
    }else{
      /****************************************************
      * se esta editando todo
      ****************************************************/


      if(!isValid){
        mensajes.alerta('ERROR','INGRESA UN USUARIO VÁLIDO','ACEPTAR!');
      }
      else if($scope.usuario.PASSWORD!==$scope.usuario.PASSWORDC){
        mensajes.alerta('ERROR','LAS CONTRASEÑAS NO COINCIDEN','ACEPTAR!');
      }
      else{
        if (regCompleto()) {
          $scope._especialidad = {
            NP_PERSONAL : $scope.usuario.IDPACIENTE.replace("/",""),
            NP_ESPECIALIDAD : $scope.medico.especialidad,
            CEDULA_ESP:$scope.medico.CEDULA_ESP,
            NP_TURNO : $scope.medico.turno,
            C_HORARIO: $scope.medico.HORARIO_LAB,
            N_DURACION : $scope.medico.tiempoSelec,
            N_TOTAL : $scope.medico.consultas,
            C_DIAS_ACTIVIDAD : $scope.diasActividad
          };

          $scope._perfil = {
            NP_PACIENTE : $scope.usuario.IDPACIENTE.replace("/",""),
            C_PASSWORD : $scope.usuario.PASSWORD,
            C_CEDULA : $scope.medico.CEDULA_ESP,
            NP_PERFIL : $scope.usuario.IDPERFIL
          };

          medicos.setPerfil($scope._perfil)
          .success(function(data){
            if(data.success === true){
              medicos.setEspecialidad($scope._especialidad)
              .success(function(data){
                if(data.success === true){
                  $scope.loadTable();
                  mensajes.alerta('AVISO','EL MÉDICO SE HA ASIGNADO CORRECTAMENTE','ACEPTAR!');
                  $scope.cancelar();
                }
                else{
                  mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
                }
                defered.resolve();
              })
              .error(function(data){
                mensajes.alerta('AVISO','SERVICIO NO DISPONIBLE','ACEPTAR!');
                defered.reject();
              });
            }
            else {
              mensajes.alerta('AVISO','ESTE USUARIO YA TIENE EL PERFIL DE '+ data.Mensaje.toUpperCase(),'ACEPTAR!');
            }
          })
          .error(function(data){
            mensajes.alerta('AVISO','SERVICIO NO DISPONIBLE','ACEPTAR!');
          });
        }
      }
    }

    $scope.submiting = false;

    return promise;


  };




  function regCompleto() {

    var dias;
    var primerDia=true;



    dias = _.filter($scope.items,function(e){
      if(e.selec){
        if (primerDia)
        $scope.diasActividad = e.value;
        else
        $scope.diasActividad = $scope.diasActividad+','+ e.value;
        primerDia = false;
      }
      return e!==null;
    })

    var completo = true;

    if (isNaN(parseInt($scope.medico.turno))) {
      mensajes.alerta('AVISO','DEBE ESPECIFICAR EL TURNO','ACEPTAR!');
      completo = false;
    }
    else
    if ($scope.diasActividad.length == 0) {
      mensajes.alerta('AVISO','DEBE ESPECIFICAR LOS DÍAS DE LABORABLES','ACEPTAR!');
      completo = false;
    }
    else
    if (isNaN(parseInt($scope.medico.tiempoSelec))) {
      mensajes.alerta('AVISO','DEBE ESPECIFICAR EL TIEMPO DE CONSULTA','ACEPTAR!');
      completo = false;
    }

    if($scope.medico.N_RECETA){
      var someCategorieSelected = false;

      _.each($scope.categorias, function(el){
        someCategorieSelected = el.selected || someCategorieSelected;
      });

      if(!someCategorieSelected){

        mensajes.alerta('AVISO', 'DEBE SELECCIONAR AL MENOS UNA CATEGORIA PARA RECETAR', 'ACEPTAR!');
      }
      completo = someCategorieSelected;

    }

    return completo;
  }





  $scope.loadTable = function(){
    var defered = $q.defer();
    var promise = defered.promise;
    var myBlockUI = blockUI.instances.get('myBlockUI');
    myBlockUI.start();
    medicos.getAllMedicosFull()
    .success(function(data){
      $scope.datos=_.filter(data,function(e){
        if (e!==null)
        e.NOMBRE_COMPLETO = e.C_PRIMER_APELLIDO + " " + e.C_SEGUNDO_APELLIDO + " " + e.C_NOMBRE;
        return e!==null;
      });
      $scope.registros = $scope.datos;
      $scope.campos = [{descripcion : "ESPECIALIDADES.CF_ESPECIALIDAD", 	validar: false, campo:"" },
      {descripcion : "N_ID", 							validar: false, campo:""  },
      {descripcion : "NOMBRE_COMPLETO", 					validar: false, campo:""  },
      {descripcion : "TURNOS.CF_DESCRIPCION",			validar: true, campo:"TURNOS"  },
      {descripcion : "TURNOS.C_DIAS_ACTIVIDAD", 			validar: true, campo:"TURNOS"  }];
      defered.resolve();
      myBlockUI.stop();
    })
    .error(function(data){
      defered.reject();
      myBlockUI.stop();
      mensajes.alerta('AVISO','NO SE PUDO CARGAR LA INFORMACIÓN','ACEPTAR!');
    });
    return promise;
  };

  $scope.filtraTabla = function(filtro) {
    $scope.registros = peticiones.filtrar(filtro,$scope.datos,$scope.campos);
  }

  $scope.bajaUsuario = function(datos){
    $scope.editar = false;
    $scope.dato=datos;
    $scope.selected.forEach(function(seleccion) {
      seleccion.NF_USUARIO = parseInt(usuario.usuario.NP_EXPEDIENTE);
      medicos.bajaMedico(seleccion)
      .success(function(data){
        if(data.success===true){
          $scope.loadTable();
          mensajes.alerta('AVISO','EL USUARIO SE HA DADO DE BAJA CORRECTAMENTE','ACEPTAR!');
        }
        else if(data.success===false){
          mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
        }
        else{
          mensajes.alerta('AVISO','NO SE PUDO DAR DE BAJA AL USUARIO','ACEPTAR!');
        }
      })
      .error(function(data){
        mensajes.alerta('AVISO','NO SE PUDO DAR DE BAJA AL USUARIO','ACEPTAR!');
      });
    });
  };

  $scope.editing = false;

  var currentRowForEdit;

  $scope.editRow = function(datos){

    isValid = true;


    currentRowForEdit = $scope.selected[0];

    /*
    ******************************************
    */

    $scope.horario_p = true;
    $scope.horario = false;
    $scope.alta_p = false;
    $scope.tabla=false;

    /*
    ******************************************
    */


    //pedir información complementaria al servidor


    medicos.getMedicoInformation(currentRowForEdit.N_EXPEDIENTE, currentRowForEdit.ESPECIALIDADES.NF_ESPECIALIDAD)
    .success(function(data){
      console.log(data);
      $scope.C_NOMBRE_COMPLETO = data.C_PRIMER_APELLIDO + ' ' + data.C_SEGUNDO_APELLIDO + ' ' + data.C_NOMBRE;

      $scope.categorias[0].selected = data.N_CATEGORIA_1 == 1;
      $scope.categorias[1].selected = data.N_CATEGORIA_2 == 1;
      $scope.categorias[2].selected = data.N_CATEGORIA_3 == 1;
      $scope.categorias[3].selected = data.N_CATEGORIA_4 == 1;
      $scope.categorias[4].selected = data.N_CATEGORIA_5 == 1;
      $scope.medico.HORARIO_LAB=data.C_HORARIO;
      $scope.medico.N_RECETA = data.N_RECETA == 1;
      $scope.medico.N_HOSPITALIZA = data.N_HOSPITALIZA == 1;
      $scope.medico.ORDEN_ESTUDIOS = data.N_ORDENES == 1;
      $scope.medico.INCAPACIDADES = data.C_INCAPACIDADES == 1;

      if(data.C_CATEGORIA){

          $scope.currentItem = _.filter($scope.categoriasMedicos, function(el){
            return el.id === data.C_CATEGORIA;
          })[0];
        }

    }).error(function(err){

    });



    var alert;
    if ($scope.selected.length > 1) {
      alert = $mdDialog.alert().title('Información').content('Sólo debe seleccionar un registro').ok('Cerrar');
      $mdDialog
      .show( alert )
      .finally(function() {
        alert = undefined;
      });

      $scope.editar = false;
      $scope.busqueda = true;
      $scope.alta = false;
    } else {


      $scope.backData = $scope.selected[0];
      $scope.data = angular.copy($scope.backData);
      $scope.medico.especialidad = currentRowForEdit.ESPECIALIDADES.NF_ESPECIALIDAD;
      if (currentRowForEdit.hasOwnProperty('TURNOS')) {
        $scope.medico.turno = currentRowForEdit.TURNOS.NF_TURNO;
        /* Activa los días laborables */
        if (currentRowForEdit.hasOwnProperty('TURNOS'))
        var dias = currentRowForEdit.TURNOS.C_DIAS_ACTIVIDAD.split(",");
        dias.forEach(function(element, index, array) {
          element = trim(element);
          angular.forEach($scope.items, function(tabla,index) {
            if (tabla.value == element)
            tabla.selec = true;
          });
        });
        $scope.medico.tiempoSelec = currentRowForEdit.TURNOS.N_DURACION;
      }
      else {
        $scope.medico.turno = "";
        angular.forEach($scope.items, function(tabla,index) {
          tabla.selec = false
        });
      }

      $scope.medico.CEDULA_ESP = currentRowForEdit.C_CEDULA;

      $scope.calcHoras();
      if (currentRowForEdit.N_ORDENES == 1)
      $scope.medico.ORDEN_ESTUDIOS = true;
      else
      $scope.medico.ORDEN_ESTUDIOS = false;

      if (currentRowForEdit.C_INCAPACIDADES == 1)
      $scope.medico.INCAPACIDADES = true;
      else
      $scope.medico.INCAPACIDADES = false;


      $scope.busqueda = false;

      /*
      ******************************************
      */

      $scope.horario_p = true;
      $scope.horario = false;
      $scope.alta_p = false;
      $scope.tabla=false;


      $scope.editing = true;


      /*
      ******************************************
      */


      $scope.selected.forEach(function(seleccion) {
        $localStorage.paciente={
          expediente: seleccion.N_EXPEDIENTE_ANIO.replace("/","")
        };
        pacientes.getPaciente()
        .success(function(data){
          if(!data.data.hasOwnProperty('NP_ID')){
            $scope.paciente=data.data[0];
          }
          else{
            mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
          }
        })
        .error(function(data){
          mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
        });
      });
    }
    $scope.selected = [];
  };

  $scope.cancelar = function(){
    $scope.inicializaEstado();
    if($scope.usuario){

        $scope.usuario.PASSWORD = undefined;
        $scope.usuario.PASSWORDC = undefined;
      }
  };

  $scope.inicializaEstado = function(){
    $scope.editar = false;
    $scope.alta = false;
    $scope.busqueda = true;
    $scope.horario = false;
    $scope.horario_p = false;
    $scope.alta_p = false;
    $scope.tabla=true;
    isValid=false;
  }

  ///PERNSONAL/MEDICOS
  $scope.datosHorario = function(){
    $scope.horario_p = false;
    $scope.horario = true;
    $scope.alta_p = false;
    $scope.alta = false;
    $scope.tabla=false;
  };

  $scope.datosHorario_p = function(){


    if(!isValid){
      mensajes.alerta('ERROR','INGRESA UN USUARIO VÁLIDO','ACEPTAR!');
    }
    else if($scope.usuario.PASSWORD == undefined || $scope.usuario.PASSWORDC == undefined){
      mensajes.alerta('ERROR','INGRESA LA CONTRASEÑA','ACEPTAR!');
    }
    else if($scope.usuario.PASSWORD !=$scope.usuario.PASSWORDC){
      mensajes.alerta('ERROR','LAS CONTRASEÑAS NO COINCIDEN','ACEPTAR!');
    }
    else{
      $scope.horario_p = true;
      $scope.horario = false;
      $scope.alta_p = false;
      $scope.tabla=false;
    }
  };

  $scope.loadCatalogos = function() {
    $scope.catEstadoCivil = [];
    pacientes.getCatalogo("9").success(function(data){
      $scope.catEstadoCivil=data;
    });
    $scope.catSexo = [];
    pacientes.getCatalogo("12").success(function(data){
      $scope.catSexo=data;
    });
    $scope.catEstado = [];
    pacientes.getCatalogo("7").success(function(data){
      $scope.catEstado=data;
    });

    $scope.catEspecialidad = [];
    medicos.getEspecialidades().success(function(data){
      $scope.catEspecialidad=data;
    });

    $scope.catTurno = [];
    medicos.getTurno().success(function(data){
      $scope.catTurno=data;
    });


  };

  $scope.items = [
    {day:'LUN', id: 0, selec: false, value : 'LUNES'},
    {day:'MAR', id: 1, selec: false, value : 'MARTES'},
    {day:'MIE', id: 2, selec: false, value : 'MIERCOLES'},
    {day:'JUV', id: 3, selec: false, value : 'JUEVES'},
    {day:'VIE', id: 4, selec: false, value : 'VIERNES'},
    {day:'SAB', id: 5, selec: false, value : 'SABADO'},
    {day:'DOM', id: 6, selec: false, value : 'DOMINGO'}];
    $scope.allDays= false;

    $localStorage.dias = [];

    $scope.selectAllDays = function(allDays){
      var i;
      for(i = 0; i < $scope.items.length; i++){
        $scope.items[i].selec= allDays;
      }
    };

    $scope.updateDays = function(item){
      var allDays = true;
      $scope.items.forEach(function(currentDay){
        if(currentDay.day === item.day){
          allDays = allDays && item.selec;
        }
        else{
          allDays = allDays && currentDay.selec;
        }
      });

      $scope.allDays = allDays;


    };




  $scope.medico = {turno:"Selecionar",consultas:'',tiempoSelec:'',especialidad:'',CEDULA_ESP:'',INCAPACIDADES:false,ORDEN_ESTUDIOS:false};
  $scope.horas=0;

  $scope.calcHoras = function(){
    if (!isNaN(parseInt($scope.medico.turno))) {
      var existe = _.filter($scope.catTurno, function(val){
        return $scope.medico.turno == val.ID;
      });

      var ini = existe[0].dHoraInicial.split(':')[0];
      var fin = existe[0].dHoraFinal.split(':')[0];
      $scope.horas=fin-ini;
      if($scope.horas<0){
        $scope.horas=24+$scope.horas;
      }
      if (isNaN(parseInt($scope.medico.tiempoSelec)))
      $scope.medico.consultas=0;
      else
      $scope.medico.consultas=$scope.horas*60/$scope.medico.tiempoSelec;
    }
  }

  $scope.calcCitas = function(){
    $scope.medico.consultas=$scope.horas*60/$scope.medico.tiempoSelec;
  }

  $scope.tiempo = [{etiqueta: "5 MINUTOS", num:5},
  {etiqueta: "10 MINUTOS", num:10},
  {etiqueta: "15 MINUTOS", num:15},
  {etiqueta: "20 MINUTOS", num:20},
  {etiqueta: "30 MINUTOS", num:30},
  {etiqueta: "60 MINUTOS", num:60}];

  $scope.registrarDatos = function(){
    $scope.paciente.F_FECHA_DE_NACIMIENTO=moment(new Date($scope.paciente.FECHA_NACIMIENTO)).format('DD/MM/YYYY');
    $scope.paciente.NF_USUARIO = parseInt(usuario.usuario.NP_EXPEDIENTE);
    pacientes.guardaPaciente($scope.paciente)
    .success(function(data){
      if(data.success===true){
        mensajes.alerta('','SE HA REGISTRADO AL USUARIO','ACEPTAR!');

      }
      else if(data.success===false){
        mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
      }
      else {
        mensajes.alerta('ERROR','NO SE PUDO REALIZAR EL REGISTRO','ACEPTAR!');
      }
    })
    .error(function(data){
      mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    });
  };

  $scope.calcularEdad =function(){
    $scope.paciente.F_FECHA_DE_NACIMIENTO = moment($scope.paciente.FECHA_NACIMIENTO).format('DD/MM/YYYY');
    var values=$scope.paciente.F_FECHA_DE_NACIMIENTO.split("/");
    var dia = values[0];
    var mes = values[1];
    var ano = values[2];
    var fecha_hoy = new Date();
    var ahora_ano = fecha_hoy.getYear();
    var ahora_mes = fecha_hoy.getMonth()+1;
    var ahora_dia = fecha_hoy.getDate()-1+1;
    var edad = (ahora_ano + 1900) - ano;
    if(ahora_mes<mes){
      edad--;
    }
    else if(ahora_mes==mes && ahora_dia<dia){
      edad--;
    }
    $scope.paciente.EDAD=edad;
    if(edad<0){
      mensajes.alerta('AVISO','NO PUEDES PONER UNA FECHA MAYOR AL DÍA DE HOY','ACEPTAR!');
      $scope.paciente.FECHA_NACIMIENTO = undefined;
      $scope.paciente.EDAD = undefined;
    }
  };

  $scope.generateElectronicKey = function(ev){
    var selectedUser = $scope.selected[0];



    $mdDialog.show({
      controller: function ($scope, $mdDialog, $q, login, $http) {

        $scope.user = selectedUser;


        $scope.currentState = 0;

        $scope.nextButtonTitle = getButtonTitle();

        $scope.backButtonTitle = getBackButtonTitle();

        $scope.key = {};

        $scope.isKeyDownloaded = false;
        $scope.isCerDownloaded = false;

        $scope.next = function(){


          switch($scope.currentState){
            case 0:
            // hacer peticion de login para ver si lo esta solicitando el usuario correcto.
            $scope.message = '';
            login.verifyCredentials({
              name: $scope.user.C_EMAIL,
              password: $scope.user.password
            }).then(function(data){

              $scope.currentState++;
              $scope.nextButtonTitle = getButtonTitle();
              $scope.backButtonTitle = getBackButtonTitle();


            }).catch(function(msg){
              $scope.message = msg;
            });


            return;
            break;
            case 1:
            // Mostrar la información del usuario para ver con que va a generar la firma

            $http.get(baseURL + 'firmaelectronica/' + $scope.user.N_EXPEDIENTE).success(function(data){
              $scope.key = data;

              $scope.currentState++;



              $scope.nextButtonTitle = getButtonTitle();
              $scope.backButtonTitle = getBackButtonTitle();

            }).error(function(err){
              // Mostrar un mensaje de que sucedio un error e intentarlo de nuevo.
            });
            break;
            case 2:
            // La firma ya debe estar en el scope, ahora descargarla como un .key
            if($scope.isKeyDownloaded && $scope.isCerDownloaded){
              $mdDialog.cancel();
            }else{
              $scope.err = 'POR FAVOR DESCARGUE SUS ARCHIVOS PARA CONTINUAR'
            }
            break;

          }




          $scope.nextButtonTitle = getButtonTitle();
          $scope.backButtonTitle = getBackButtonTitle();

        }

        $scope.getFirmaFile = function(){
          download($scope.user.N_ID + '.key', $scope.key.privateKey);
          $scope.isKeyDownloaded = true;
        };

        $scope.getCertificadoFile = function(){
          download($scope.user.N_ID + '.cer', $scope.key.certificado);
          $scope.isCerDownloaded = true;
        };

        $scope.back = function(){

          switch($scope.currentState){
            case 0:
            // hacer peticion de login para ver si lo esta solicitando el usuario correcto.
            $mdDialog.cancel();
            break;
            case 1:
            // Mostrar la información del usuario para ver con que va a generar la firma
            break;
            case 2:
            // La firma ya debe estar en el scope, ahora descargarla como un .key
            break;

          }
          $scope.currentState--;


          $scope.nextButtonTitle = getButtonTitle();
          $scope.backButtonTitle = getBackButtonTitle();

        }


        $scope.changeTab = function(tabId){
          $scope.currentState = tabId;
        }

        function getButtonTitle(){
          var title;
          switch($scope.currentState){
            case 0: title = 'VALIDAR'; break;
            case 1: title = 'GENERAR'; break;
            case 2: title = 'FINALIZAR'; break;
          }
          return title;
        }

        function getBackButtonTitle(){
          var title;
          switch($scope.currentState){
            case 0: title = 'CANCELAR'; break;
            case 1:case 2: title = 'ANTERIOR'; break;
          }
          return title;
        }




      },
      templateUrl: '/application/views/agenda/dialog1.firmaelectronica.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });

  };



  $scope.selectAllCategories = function(value){
    _.each($scope.categorias, function(el){
      el.selected = value;
    });
  }

  $scope.updateCategories = function(item){
    var allCategories = true;

    _.each($scope.categorias, function(el){
      allCategories = (el.descripcion == item.descripcion) ?
       (allCategories && item.selected) :
       (allCategories && el.selected);
    });

    $scope.allCategories = allCategories;
  }


}]);



function download(filename, text) {
  var pom = document.createElement('a');
  pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  pom.setAttribute('download', filename);

  if (document.createEvent) {
    var event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    pom.dispatchEvent(event);
  }
  else {
    pom.click();
  }
}

function ltrim(str, filter){
      filter || ( filter = '\\s|\\&nbsp;' );
      var pattern = new RegExp('^(' + filter + ')*', 'g');
      return str.replace(pattern, "");
    }

    function rtrim(str, filter){
      filter || ( filter = '\\s|\\&nbsp;' );
      var pattern = new RegExp('(' + filter + ')*$', 'g');
      return str.replace(pattern, "");
    }

    function trim(str, filter){
      filter || ( filter = '\\s|\\&nbsp;' );
      return ltrim(rtrim(str, filter), filter);
    }
})();