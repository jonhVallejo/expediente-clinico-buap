(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaConsultaragendaCtrl
 * @description
 * # AgendaConsultaragendaCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaConsultaragendaCtrl', ['$scope', '$q', '$timeout', '$http', '$mdDialog', 'medicos', 'mensajes', '$mdToast', '$state', 'blockUI', 'usuario' , 'reportes',
    function ($scope, $q, $timeout, $http, $mdDialog, medicos, mensajes, $mdToast, $state, blockUI, usuario , reportes) {

    var actionURL = 'agendamedica/';


    $scope.selected = [];
    $scope.registros = [];
    $scope.agendas = [];
    $scope.agenda = {};
    $scope.medicos = [];
    $scope.tablaAgenda = false;

    $scope.minDate = moment().subtract(91, 'days').toDate();
    $scope.agenda.fechaInicial = $scope.minDate;
    $scope.agenda.fechaFinal = moment().toDate();

    $scope.especialidades = [];

    $scope.clear = function(){
        $scope.agendas = [];
        $scope.agenda = {};

    };

    $scope.query = {
        filter: '',
        order: 'fecha',
        limit: 15,
        page: 1
    };

    console.log(usuario.perfil);

    $scope.usrPerfil = usuario.perfil.clave;

    medicos.getEspecialidades().success(function (data) {
        $scope.especialidades = data;
    }).error(function (err) {
    });

    $scope.onOrderChange = function (page, limit) {
        var deferred = $q.defer();
        $timeout(function () {
            deferred.resolve();
        }, Math.random() * 2000);
        return deferred.promise;
    };

    $scope.close = function () {
        $mdDialog.hide();
    };
     $scope.parseFecha = function(date){
        return (date !== undefined) ? moment(date).format("DD/MM/YYYY") : '';
    };

    $scope.obtenAgendas = function () {

        var myBlockUI = blockUI.instances.get('myBlockUI');

        myBlockUI.start();

        medicos.getAgenda(moment($scope.agenda.fechaInicial).format('DDMMYYYY'),
                moment($scope.agenda.fechaFinal).format('DDMMYYYY'), $scope.agenda.especialidad, $scope.agenda.medico)
                .success(function (data) {
                    if (data.length == 0) {
                        mensajes.alerta('AVISO', 'NO EXISTEN REGISTROS EN ESTE RANGO DE FECHAS', 'ACEPTAR');
                    }
                    else
                    {
                        var grouped = _.groupBy(data, function (el) {
                            return el.CF_MEDICO + '#' + moment(el.F_FECHA_HORA, 'DD/MM/YYYY').format('DD/MM/YYYY');
                        })
                        $scope.agendas = _.map(grouped, function (el, key) {
                            return{
                                'medico': key.split('#')[0],
                                'fecha': key.split('#')[1],
                                'citas': el
                            }
                        });
                        $scope.agendas = _.map($scope.agendas, function (el, key) {
                            el.fecha = new Date(el.fecha.split("/")[2],el.fecha.split("/")[1]-1,el.fecha.split("/")[0]);
                            return el;
                        });
                    }

                    myBlockUI.stop();
                }).error(function (err) {
                    myBlockUI.stop();
        });
        $scope.tablaAgenda = true;
    };




    $scope.verAgenda = function (agenda, evento) {

        var usrPerfil = $scope.usrPerfil;

        var middleForCancel = function (cita) {
            $scope.cancel([cita]);

            $scope.agendas[_.indexOf($scope.agendas, agenda)].citas =
                    _.filter($scope.agendas[_.indexOf($scope.agendas, agenda)].citas,
                            function (el) {
                                return el.NP.ID != cita.NP_ID;
                            });
        }

        $mdDialog.show({
            templateUrl: '/application/views/agenda/dialog.agenda.html',
            controller: function ($scope, $mdDialog, medicos, pacientes, $state, $localStorage) {

              $scope.usrPerfil = usrPerfil;
                $scope.agenda = agenda;
                $scope.hidden = false;
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancelar = function (cita) {
                    middleForCancel(cita);
                }

                $scope.reagendar = function (cita, ev) {

                    pacientes.cargaIdPaciente(cita.NF_EXPEDIENTE_PACIENTE.replace('/', ''));



                    pacientes.getPaciente().success(function (data) {

                        pacientes.setBackCita(cita);


                        var tempPaciente = data.data.pop();

                        $localStorage.paciente = {
                            idexpediente: tempPaciente.NP_EXPEDIENTE.split("/")[0],
                            expediente: tempPaciente.NP_EXPEDIENTE.replace("/", ""),
                            nombrePaciente: tempPaciente.C_PRIMER_APELLIDO + " " + tempPaciente.C_SEGUNDO_APELLIDO + " " + tempPaciente.C_NOMBRE,
                            idPaciente: tempPaciente.NP_ID
                        };

                        pacientes.cargaPaciente(tempPaciente);

                        var confirm = $mdDialog.confirm()
                                .title('AVISO')
                                .content('¿DESEA CAMBIAR DE MEDICO?')
                                .ariaLabel('Lucky day')
                                .targetEvent(ev)
                                .ok('SI')
                                .cancel('NO DESEO CAMBIAR DE MEDICO.');

                        $mdDialog.show(confirm).then(function () {
                            $state.go('agendaConsultaMedico');
                        }, function () {

                            medicos.getMedicoActual(cita.NF_MEDICO).success(function (dato) {





                                medicos.cargaMedico(dato);

                                $localStorage.medico = {
                                    nombreMedico: dato.C_PRIMER_APELLIDO + " " + dato.C_SEGUNDO_APELLIDO + " " + dato.C_NOMBRE,
                                    nombreEspecialidad: dato.ESPECIALIDADES.CF_ESPECIALIDAD,
                                    idMedico: dato.N_EXPEDIENTE,
                                    idTurno: dato.TURNOS.NF_TURNO,
                                    idEspecialidad: cita.NF_ESPECIALIDAD
                                };

                                $state.go('agendaVerDisponibilidad');
                            }).error(function(err){
                                console.log(err);
                            });
                        });

                    });
                }

                $scope.print = function (cita) {
                    reportes.getReporte(baseURL + 'ticketRe/' + cita.NP_ID, '_blank', 'width=330, height=440');
                }

                $scope.formatHour = function (date) {
                    return moment(date, 'DD/MM/YYYY HH:mm:ss').format('HH:mm');
                }
            },
            parent: angular.element(document.body),
            targetEvent: evento,
            clickOutsideToClose: true,
        });
    }


    $scope.obtenMedicos = function () {
        medicos.getMedicos($scope.agenda.especialidad.ID)
                .success(function (data) {
                    $scope.medicos = data;
                }).error(function (err) {
        });
    }


    $scope.clearMedicos = function () {
        $scope.medicos = [];
    };

    $scope.cancelDays = function (days) {
        var temp = [];
        temp = _.map(days, function (el) {
            return el.citas;
        });
        temp = _.flatten(temp);

        var result = $scope.cancel(temp);

        $scope.agendas = _.difference($scope.agendas, days);

    }

    $scope.cancel = function (registros) {

        var successRegistros = [];
        var motivo;
        var result = false;

        var ids = _.map(registros, function (el) {
            return el.NP_ID;
        });
        ids = _.reduce(ids, function (m, p) {
            return m + ',' + p
        });

        $mdDialog.show(
                {
                    controller: function ($scope, $mdDialog, catalogos) {

                        catalogos.getCatalogos(20).success(function (data) {
                            $scope.motivos = data;

                        }).error(function (err) {
                        });

                        $scope.hide = function () {
                            $mdDialog.hide();
                        };
                        $scope.cancel = function () {
                            $mdDialog.cancel();
                        };

                        $scope.answer = function (answer) {
                            $mdDialog.hide(answer);
                        };
                    },
                    templateUrl: 'canceldlg.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true
                })
                .then(function (answer) {

                    if (answer) {
                       /* $http({
                            url: baseURL + 'agenda',
                            method: 'DELETE',
                            data: {
                                'id_cita': '' + ids,
                                'id_motivo': answer
                            },
                            dataType: 'json',
                            headers: {
                                'Content-Type': 'application/json',
                            }
                        })*/
                        $http.delete(baseURL + 'agenda', {
                                data: {
                                    'id_cita': '' + ids,
                                    'id_motivo': answer
                                }
                            }).success(function (data) {
                            $mdToast.show(
                                    $mdToast.simple()
                                    .content(data.Mensaje)
                                    .position('top left')
                                    .hideDelay(3000)
                                    );

                            result = true;
                        }).error(function (err) {
                            $mdToast.show(
                                    $mdToast.simple()
                                    .content('ERROR')
                                    .position('top')
                                    .hideDelay(3000)
                                    );
                        });
                    }
                }, function () {
                });

        return result;

    };

    $scope.print = function (cita) {
        console.log(cita.citas[0].NF_MEDICO);
        reportes.getReporte(baseURL + 'agendaM/' + cita.citas[0].NF_MEDICO + '/' + moment(cita.fecha).format('DDMMYYYY'), '_blank', 'width=800, height=600');
    };

}]);

function CancelarCitaDlgCtrl($scope, $mdDialog) {

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };
}
})();