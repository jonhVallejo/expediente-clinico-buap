'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaRegistrapacienteCtrl
 * @description
 * # AgendaRegistrapacienteCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaRegistrapacienteCtrl', function ($scope,$http,$state,pacientes,mensajes,medicos,$localStorage,candado,reportes) {
	var visible=false;
	$scope.paciente={T_RESPONSABLES:{}};
	$scope.showDomicilio = true;

	$scope.registrarDatos = function(){
		var newDate = new Date($scope.paciente.FECHA_NACIMIENTO);
		$scope.paciente.F_FECHA_DE_NACIMIENTO=('0' + newDate.getDate()).slice(-2)+"/"+('0' + (newDate.getMonth() + 1)).slice(-2)+"/"+newDate.getFullYear()
		pacientes.guardaPaciente($scope.paciente)
		.success(function(data){
			if(data.success===true){
				mensajes.alerta('','SE HA REGISTRADO AL USUARIO','ACEPTAR!');
				$localStorage.cita.paciente = {
					expediente:data.Mensaje,
					nombrePaciente: $scope.paciente.C_PRIMER_APELLIDO+" "+$scope.paciente.C_SEGUNDO_APELLIDO+" "+$scope.paciente.C_NOMBRE
				};
				$state.go('agendaCalendarizarPreCita');
			}
			else if(data.success===false){
				mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
			}
			else {
				mensajes.alerta('ERROR','NO SE PUDO REALIZAR EL REGISTRO','ACEPTAR!');	
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

	$scope.loadCatalogos = function() {
	    $scope.catEstadoCivil = [];
	    pacientes.getCatalogo("9").success(function(data){
			$scope.catEstadoCivil=data;
		})
		$scope.catSexo = [];
	    pacientes.getCatalogo("12").success(function(data){
			$scope.catSexo=data;
		});
		$scope.catEstado = [];
	    pacientes.getCatalogo("7").success(function(data){
			$scope.catEstado=data;
		});
		$scope.catParentesco = [];
	    pacientes.getCatalogo("17").success(function(data){
			$scope.catParentesco=data;
		});
	};

	$scope.loadDatos = function(){
		$scope.consulta=$localStorage.cita;
	};

	$scope.calcularEdad =function(){
		$scope.paciente.F_FECHA_DE_NACIMIENTO = moment($scope.paciente.FECHA_NACIMIENTO).format('DD/MM/YYYY');
		var values=$scope.paciente.F_FECHA_DE_NACIMIENTO.split("/");
		var dia = values[0];
	    var mes = values[1];
        var ano = values[2];
        var fecha_hoy = new Date();
        var ahora_ano = fecha_hoy.getYear();
        var ahora_mes = fecha_hoy.getMonth()+1;
        var ahora_dia = fecha_hoy.getDate()-1+1;
        var edad = (ahora_ano + 1900) - ano;
        if(ahora_mes<mes){
        	edad--;
        }
        else if(ahora_mes==mes && ahora_dia<dia){
        	edad--;
        }
        $scope.paciente.EDAD=edad +' AÑO(S)';
        if(edad<0){
        	mensajes.alerta('AVISO','NO PUEDES PONER UNA FECHA MAYOR AL DÍA DE HOY','ACEPTAR!');
        	$scope.paciente.FECHA_NACIMIENTO = undefined;
        	$scope.paciente.EDAD = undefined;
        }
        else if(edad === 0){
        	$scope.paciente.EDAD = pacientes.calculaEdad($scope.paciente.FECHA_NACIMIENTO);
        	/*if(ahora_dia-dia > 0){
        		$scope.paciente.EDAD = ahora_mes - mes +' MES(ES)';
        	}
        	else{
        		$scope.paciente.EDAD = ahora_mes - mes - 1 +' MES(ES)';	
        	}*/
        }
        else{
        	$scope.paciente.EDAD = pacientes.calculaEdad($scope.paciente.FECHA_NACIMIENTO);
        }
    }
   	
   	$scope.registrarDatosCaja = function(){
	    console.log("AA ",$scope.paciente);
   		
	    if( $scope.paciente.NF_ESTADO_CIVIL==null){
	      $scope.paciente.NF_ESTADO_CIVIL=5;
	    }
	    if( $scope.paciente.FECHA_NACIMIENTO==null){
	      $scope.paciente.F_FECHA_DE_NACIMIENTO="01/02/1900";
	    }
	    else{
	      var newDate = new Date($scope.paciente.FECHA_NACIMIENTO);
	        $scope.paciente.F_FECHA_DE_NACIMIENTO=('0' + newDate.getDate()).slice(-2)+"/"+('0' + (newDate.getMonth() + 1)).slice(-2)+"/"+newDate.getFullYear()
	    }
	  
	    $scope.paciente["NF_ENTIDAD_FEDERATIVA"]=33;
	    $scope.paciente.T_RESPONSABLES["NF_ENTIDAD_FEDERATIVA"]=33;

	    console.log($scope.paciente);
	   	pacientes.guardaPaciente($scope.paciente)
	    .success(function(data){
	      if(data.success===true){
	        $scope.visible=true;
	        $scope.paciente.id=data.id;
	        $scope.paciente.Mensaje=data.Mensaje;
	        $scope.paciente.desconocido=data.desconocido;
	        mensajes.alerta('','SE HA REGISTRADO AL USUARIO','ACEPTAR!');
	      }
	      else if(data.success===false){
	        mensajes.alerta('AVISO',data.Mensaje,'ACEPTAR!');
	      }
	      else {
	        mensajes.alerta('ERROR','NO SE PUDO REALIZAR EL REGISTRO','ACEPTAR!');  
	      }
	    })
	    .error(function(data){
	      mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
	    });
  	};

  	$scope.getReporteCaja = function(id){
   		
   		reportes.getReporte(baseURL+'/ticketCaja/'+id.split('/')[0], '_blank', 'width=600, height=800,toolbar=no,scrollbars=yes,Location=no');
    }

    $scope.validaCURP = function(){
    	
    	if($scope.paciente.C_NOMBRE != undefined && $scope.paciente.C_PRIMER_APELLIDO != undefined && $scope.paciente.C_SEGUNDO_APELLIDO != undefined && $scope.paciente.NF_SEXO != undefined && $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO){
    		
    		var cadenaValida = $scope.paciente.C_PRIMER_APELLIDO[0] + $scope.paciente.C_PRIMER_APELLIDO[1] + $scope.paciente.C_SEGUNDO_APELLIDO[0] +
    			$scope.paciente.C_NOMBRE[0] + moment($scope.paciente.FECHA_NACIMIENTO).format('YYMMDD') ;
			
    	}
    	else {
    		console.log("");
    	}
    };

  });
