'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaReimprimirticketCtrl
 * @description
 * # AgendaReimprimirticketCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaReimprimirticketCtrl', function ($scope , reportes) {

	$scope.reimprimir =function(){
		reportes.getReporte('http://148.228.103.211:8080/ece/ticketRe/'+$scope.paciente.ticket, '_blank', 'width=500, height=400');
	}
	
  });
