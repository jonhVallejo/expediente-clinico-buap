'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaPrimeravezCtrl
 * @description
 * # AgendaPrimeravezCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaPrimeravezCtrl', function ($scope,$http,$mdDialog,medicos,$log,$state,$localStorage,mensajes) {
    $scope.selected = [];
    $scope.existMedicos = true;
	$scope.query = {
		filter: '',
	   	order: 'nombre',
	   	limit: 5,
	   	page: 1
	};

	function success(datos) {
		$scope.datos = datos;
	}

	$scope.search = function (predicate) {
	    $scope.filter = predicate;
	    $scope.deferred = $scope.datos.get($scope.query, success).$promise;
	};

	$scope.loadTable = function(){ 
		$scope.existMedicos = true;
		$scope.datos={};
		medicos.getMedicos($scope.medico.especialidad.ID).success(function(data){
			if(data.length <= 0){
	    		$scope.existMedicos=false;
				mensajes.alerta('AVISO','LO SENTIMOS, NO HAY MÉDICOS PARA ESTA ESPECIALIDAD','ACEPTAR!');
			}
			$scope.datos=data;
		});
	};

  	$scope.loadEspecialidades = function() {
		$scope.catEspecialidad = [];
	    medicos.getEspecialidades().success(function(data){
			$scope.catEspecialidad=data;
		});
	};

	$scope.verDisponibilidad = function(ev,dato) {
  		$localStorage.medico = {
  			nombreMedico : dato.C_PRIMER_APELLIDO+" "+dato.C_SEGUNDO_APELLIDO+" "+dato.C_NOMBRE,
  			nombreEspecialidad : dato.ESPECIALIDADES.CF_ESPECIALIDAD,
  			idMedico : dato.N_EXPEDIENTE,
  			idTurno: dato.TURNOS.NF_TURNO,
  			idEspecialidad: $scope.medico.especialidad.ID
  		};

  		$state.go('agendaVerDisponibilidadPV');
	};
  });
