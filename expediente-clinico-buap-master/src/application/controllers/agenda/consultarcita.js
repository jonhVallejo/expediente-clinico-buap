'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaConsultarcitaCtrl
 * @description
 * # AgendaConsultarcitaCtrl
 * Controller of the expediente
 */
 angular.module('expediente').controller('AgendaConsultarcitaCtrl',
 ['$scope', '$http', 'mensajes','pacientes', 'blockUI', '$q',
 function ($scope, $http, mensajes,pacientes, blockUI, $q) {

   var actionURL = 'paciente/';




   //$scope.minDateForSeach = moment().subtract(1, 'days').toDate();
   $scope.minDateForSeach = moment().subtract(91, 'days').toDate();

   $scope.buscar = function(){



    blockUI.start();

    $scope.hidden = false;
    $scope.isOpen = false;
    $scope.hover = false;





    $http.get(baseURL + actionURL + expediente + '/' +  moment($scope.obj.beginDate).format('DDMMYYYY') + '/' + moment($scope.obj.endDate).format('DDMMYYYY')).success(function(data){
     if(data.success){
      $scope.datos = data.data;
      $scope.expediente = expediente;
    }else{
      mensajes.alerta('ERROR', 'NO EXISTEN CITAS EN EL RANGO SELECCIONADO','ACEPTAR');
      $scope.datos = [];
    }
    blockUI.stop();
  }).error(function(err){
   mensajes.alerta('ERROR', 'ERROR, VERIFICA TU INFORMACIÓN','ACEPTAR!');
   blockUI.stop();
 });
}

$scope.query = {
  filter: '',
  order: 'expediente',
  limit: 5,
  page: 1
};



$scope.onOrderChange = function (order) {
  var deferred = $q.defer();

        $timeout(function () {
          deferred.resolve();
        }, 2000);
    return deferred.promise;
};

$scope.onPaginationChange = function (page, limit) {
  if(typeof success !== 'undefined')

    return $scope.datos.get($scope.query, success).$promise;
};

$scope.simulateQuery = false;
$scope.isDisabled    = false;
$scope.nombres = [];
var expediente='';

$scope.querySearch = function (query) {

  var defered = $q.defer();
  var promise = defered.promise;

  if(query !== undefined){
    if(query.length>3){
      pacientes.getPacientes(query).success(function(data){
      //.isDisabled = false;}
      $scope.isDisabled = false;
      $scope.nombres = cargarServicios(data.data);
      defered.resolve($scope.nombres);
    }).error(function(data){
      mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
      defered.reject();
    });
    }

 }else{
    defered.reject();
 }
 return promise;

}

$scope.searchTextChange = function(text) {

/*
  expediente= (text == undefined) ? "" : text;

  if(expediente.length > 3){

    if(expediente.indexOf('/') != -1 || expediente.indexOf('-') != -1){
      expediente= expediente.replace("/","");
      expediente= expediente.replace("-","");
    }
//    getServicios(expediente);
  }
  */
}

$scope.selectedItemChange = function(item) {

  if(item){
    expediente=item.id;
    if(expediente.indexOf('/') != -1 || expediente.indexOf('-') != -1){
      expediente= expediente.replace("/","");
      expediente= expediente.replace("-","");
    }
  }
}

function createFilterFor(query) {
  var consulta = angular.uppercase(query);
  return function filterFn(state) {
    return (state.value.search(consulta) !== -1);
  };
};

function getServicios(query){
  pacientes.getPacientes(query)
  .success(function(data){

      //.isDisabled = false;
      $scope.nombres = cargarServicios(data.data);

      console.log($scope.nombres);
    })
  .error(function(data){
    mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
  });
};

function cargarServicios(data){

  if(data != undefined){


    return data.map(function (serv){
      return {
        value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
        display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
        nombre: serv.C_NOMBRE,
        id  : serv.NP_EXPEDIENTE
      };
    });
  }
};

$scope.getFormatedDate = function(date){
  return moment(new Date(date)).format('DD/MM/YYYY');
}





}]);
