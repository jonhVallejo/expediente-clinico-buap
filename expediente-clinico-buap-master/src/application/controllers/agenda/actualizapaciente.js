'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaActualizapacienteCtrl
 * @description
 * # AgendaActualizapacienteCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaActualizapacienteCtrl', function ($scope,pacientes,mensajes,$state,$localStorage,candado) {
	$scope.isUpdate = true;
	if($localStorage.paciente === undefined){
		
		$state.go('bienvenido');
	}

	$scope.showDomicilio = true;

	candado.isLock()
	.success(function(data){
		$scope.isLock = (data.Mensaje ==='1') ? true : false ;
	})
	.error(function(data){

	});

	$scope.loadCatalogos = function() {
	    $scope.catEstadoCivil = [];
	    pacientes.getCatalogo("9").success(function(data){
			$scope.catEstadoCivil=data;
		})
		$scope.catSexo = [];
	    pacientes.getCatalogo("12").success(function(data){
			$scope.catSexo=data;
		});
		$scope.catEstado = [];
	    pacientes.getCatalogo("7").success(function(data){
			$scope.catEstado=data;
		});

	};

	$scope.loadDatos = function(){
		$scope.paciente = {expediente: 22015};
		pacientes.getPaciente()
		.success(function(data){
			if(data.success){
				$scope.paciente=data.data[0]; //NF_SEXO: "FEMENINO" NF_SEXO_ID: 1 
				if($scope.paciente.NF_ENTIDAD_DE_NACIMIENTO_ID=== undefined){
					$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = undefined;
				}
				if($scope.paciente.NF_ENTIDAD_FEDERATIVA_ID === undefined){
					$scope.paciente.NF_ENTIDAD_FEDERATIVA = undefined;
				}
				if($scope.paciente.NF_ESTADO_CIVIL_ID === undefined){
					$scope.paciente.NF_ESTADO_CIVIL = undefined;
				}
				if($scope.paciente.NF_SEXO_ID === undefined){
					$scope.paciente.NF_SEXO === undefined;
				}
				if($scope.paciente.F_FECHA_DE_NACIMIENTO != undefined){
					$scope.paciente.FECHA_NACIMIENTO = new Date($scope.paciente.F_FECHA_DE_NACIMIENTO.split("/")[2], ($scope.paciente.F_FECHA_DE_NACIMIENTO.split("/")[1]) - 1 ),$scope.paciente.F_FECHA_DE_NACIMIENTO.split("/")[0];
					$scope.paciente.EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
				}
			}
			else if(!data.success){
				mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
			}
			else{
				mensajes.alerta('ERROR','HA OCURRIDO UN ERROR AL CARGAR LOS DATOS','ACEPTAR!');
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

	$scope.actualizarUsuario = function(){
		var auxEdoCivil = $scope.paciente.NF_ESTADO_CIVIL;
		var auxEntidadF = $scope.paciente.NF_ENTIDAD_FEDERATIVA;
		var auxEntidadN = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO;
		var auxSexo = $scope.paciente.NF_SEXO;

		$scope.paciente.NP_EXPEDIENTE=$scope.paciente.NP_EXPEDIENTE.replace("/","");
		if($scope.paciente.NF_ESTADO_CIVIL != undefined){
			var estadoCivil = _.filter($scope.catEstadoCivil,function(e){
			    return e.cDescripcion+''===$scope.paciente.NF_ESTADO_CIVIL+'';
			});
			$scope.paciente.NF_ESTADO_CIVIL = estadoCivil[0].ID;
		}
		if($scope.paciente.NF_ENTIDAD_FEDERATIVA != undefined){
			var entidadFederativa =_.filter($scope.catEstado,function(e){
			    return e.cDescripcion+''===$scope.paciente.NF_ENTIDAD_FEDERATIVA+'';
			});
			$scope.paciente.NF_ENTIDAD_FEDERATIVA = entidadFederativa[0].ID;
		}
		if($scope.paciente.NF_ENTIDAD_DE_NACIMIENTO != undefined){
			var entidadNacimiento = _.filter($scope.catEstado,function(e){
			    return e.cDescripcion+''===$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO+'';
			});
			$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = entidadNacimiento[0].ID;
		}
		
		if($scope.paciente.NF_SEXO != undefined){
			var sexo = _.filter($scope.catSexo,function(e){
			    return e.cDescripcion+''===$scope.paciente.NF_SEXO+'';
			});
			$scope.paciente.NF_SEXO = sexo[0].ID;
		}
		if($scope.paciente.FECHA_NACIMIENTO != undefined){
			$scope.paciente.F_FECHA_DE_NACIMIENTO = moment($scope.paciente.FECHA_NACIMIENTO).format("DD/MM/YYYY");
		}
		
		pacientes.actualizaPaciente($scope.paciente)
		.success(function(data){
			if(data.success){
				mensajes.alerta('AVISO','EL USUARIO SE HA ACTUALIZADO CORRECTAMENTE','ACEPTAR!');
				$state.go('bienvenido');
				$localStorage.paciente = undefined;
			}
			else if(!data.success){
				mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
				$scope.paciente.NF_ESTADO_CIVIL = auxEdoCivil;
				$scope.paciente.NF_ENTIDAD_FEDERATIVA = auxEntidadF;
				$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = auxEntidadN;
				$scope.paciente.NF_SEXO = auxSexo;
			}
			else{
				mensajes.alerta('ERROR','LO SENTIMOS HA OCURRIDO UN ERROR INESPERADO, INTENTE MÁS TARDE','ACEPTAR!');
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

	$scope.calcularEdad =function(){
		$scope.paciente.F_FECHA_DE_NACIMIENTO = moment($scope.paciente.FECHA_NACIMIENTO).format('DD/MM/YYYY');
		var values=$scope.paciente.F_FECHA_DE_NACIMIENTO.split("/");
		var dia = values[0];
	    var mes = values[1];
        var ano = values[2];
        var fecha_hoy = new Date();
        var ahora_ano = fecha_hoy.getYear();
        var ahora_mes = fecha_hoy.getMonth()+1;
        var ahora_dia = fecha_hoy.getDate()-1+1;
        var edad = (ahora_ano + 1900) - ano;
        if(ahora_mes<mes){
        	edad--;
        }
        else if(ahora_mes==mes && ahora_dia<dia){
        	edad--;
        }
        $scope.paciente.EDAD=edad +' AÑO(S)';
        if(edad<0){
        	mensajes.alerta('AVISO','NO PUEDES PONER UNA FECHA MAYOR AL DÍA DE HOY','ACEPTAR!');
        	$scope.paciente.FECHA_NACIMIENTO = undefined;
        	$scope.paciente.EDAD = undefined;
        }
        else if(edad === 0){
        	if(ahora_dia-dia > 0){
        		$scope.paciente.EDAD = ahora_mes - mes +' MES(ES)';
        	}
        	else{
        		$scope.paciente.EDAD = ahora_mes - mes - 1 +' MES(ES)';	
        	}
        }
    }
  });
