(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AgendaImprimircarnetCtrl
 * @description
 * # AgendaImprimircarnetCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('AgendaImprimircarnetCtrl',
  [ '$scope','$http','pacientes','$log','$state','mensajes' , 'reportes', '$q',
	function ($scope,$http,pacientes,$log,$state,mensajes , reportes, $q) {
	$scope.title="IMPRIMIR CARNET";
	$scope.carnet=true;

	$scope.busqueda='';
	//var self = this;
  $scope.simulateQuery = false;
  $scope.isDisabled    = false;
  // list of `state` value/display objects

  var textoBuscado='';

	$scope.existDatos = true;







  $scope.querySearch = function(query) {

		if(query.length != undefined){
			textoBuscado = query;
			if(textoBuscado.indexOf('/') != -1 || textoBuscado.indexOf('-') != -1){
				textoBuscado= textoBuscado.replace("/","");
				textoBuscado= textoBuscado.replace("-","");
			}
			if(textoBuscado.length > 3){
				var defered = $q.defer();
				var promise = defered.promise;
				if(hasNumbers(query)){
					return $scope.nombres = [];
				}

				pacientes.getPacientes(textoBuscado)
				.success(function(data){
					$scope.isDisabled = false;
					$scope.nombres = $scope.cargarServicios(data.data);
					defered.resolve($scope.nombres);
				})
				.error(function(data){
					mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
					defered.reject({});
				});
				return promise
			}
		}
  }

	function hasNumbers(t){
    return /\d/.test(t);
  }

  $scope.searchTextChange = function(text) {

    textoBuscado=text;
    if(textoBuscado.indexOf('/') != -1 || textoBuscado.indexOf('-') != -1){
      textoBuscado= textoBuscado.replace("/","");
      textoBuscado= textoBuscado.replace("-","");
    }
    if(text.length>1){
      getServicios(textoBuscado);
    }
  }

  $scope.selectedItemChange = function(item) {
    textoBuscado=item.id;
  }

  $scope.createFilterFor = function(query) {
    var consulta = angular.uppercase(query);
    return function filterFn(state) {
      return (state.value.search(consulta) !== -1);
    };
  };

  $scope.getServicios = function(query){


    pacientes.getPacientes(query)
    .success(function(data){

      self.isDisabled = false;
      self.nombres = cargarServicios(data);
    })
    .error(function(data){
      mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
    });
  };

  $scope.cargarServicios = function(data){
    return data.map(function (serv){
      return {
        value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
        display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
        nombre: serv.C_NOMBRE,
        id  : serv.NP_EXPEDIENTE
      };
    });
  };

  $scope.selected = [];

  $scope.query = {
    filter: '',
    order: 'nombre',
    limit: 5,
    page: 1
  };

  function success(datos) {
    $scope.datos = datos;
  }

  $scope.search = function (predicate) {

  };

  $scope.onOrderChange = function (order) {

  };

  $scope.onPaginationChange = function (page, limit) {

  };

  $scope.datos =[];



	$scope.loadTablePacientes = function(){
    $scope.datos = [];
    if(textoBuscado.indexOf('/') != -1 || textoBuscado.indexOf('-') != -1){
      textoBuscado= textoBuscado.replace("/","");
      textoBuscado= textoBuscado.replace("-","");
    }

    pacientes.getPacientes(textoBuscado)
    .success(function(data){
      if(data.success===true){
        if(angular.isArray(data.data)){
          $scope.datos=data.data;
          if($scope.datos.length > 0){
            $scope.existDatos = true;
          }
          else{
            $scope.existDatos = false;
          }
        }
        else{
          $scope.datos = [data.data];
        }
      }
      else if(data.success===false){
        mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
      }
      else{
        mensajes.alerta('AVISO','NO EXISTEN USUARIOS','ACEPTAR!');
      }
      $scope.query.page=1;
    })
    .error(function(data){
      mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    });
  };


  $scope.imprimirCarnet = function(ev,dato){ //Agregar servicio para imprimir carnet


		$http.get(baseURL + 'carnet/' + dato.NP_EXPEDIENTE.split('/')[0]).success(function(data){


			download(dato.NP_ID + '.txt', data.cadena);

      var command = 'copy "' + dato.NP_ID + '.txt" \\\\127.0.0.1\\ZebraTLP2844-Z';

      download(dato.NP_ID + '.bat',command);

		}).error(function(err){
			mensajes.alerta('ERROR', 'ERROR RECUPERANDO CARNET', 'ACEPTAR!');
		});

  };

  }]);


	function download(filename, text) {
	  var pom = document.createElement('a');
	  pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	  pom.setAttribute('download', filename);

	  if (document.createEvent) {
	    var event = document.createEvent('MouseEvents');
	    event.initEvent('click', true, true);
	    pom.dispatchEvent(event);
	  }
	  else {
	    pom.click();
	  }
	}


})();
