(function () {
  'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:QuirofanoNotapostanestesicactrlCtrl
 * @description
 * # QuirofanoNotapostanestesicactrlCtrl
 * Controller of the expediente
 */
  angular.module('expediente')
  .controller('NotapostanestesicactrlCtrl', ['$q','$element','$scope','$localStorage','usuario','mensajes','catalogos','pacientes','peticiones','cuestionarioService','blockUI','$timeout','quirofano','indicacionTerapeutica','$state','$window','reportes',
    function ($q,$element,$scope,$localStorage,usuario,mensajes,catalogos,pacientes,peticiones,cuestionarioService,blockUI,$timeout,quirofano,indicacionTerapeutica, $state,$window,reportes){

    var self                  = this;
    var idEspecialidad        = 10061;//Especialidad Genérica
    var ant                   = -1;

    $scope.medicos            = [];
    $scope.postAnestesicas    = [];
    $scope.preAnestesicas     = [];
    $scope.mEstadoClinico     = [];
    $scope.idNota             = {titulo:'', desc:'',tipo:'', id:0, estatus:-1};
    self.idPostAnestesica     = -1;
    self.mCuestionario        = {SECCION: []};
    self.mCuestionarioGeneral = {SECCION: []};
    indicacionTerapeutica.C_TIPO_RECETA=1;
    indicacionTerapeutica.init();

    $scope.showNota=true;
    $scope.showFirma=false;
    $scope.showButton=true;
    $scope.imprimirNota=false;
    $scope.editable=true;
    self.datosNota = {};

 //DATOS PARA MEDICACIÓN
    $scope.catUnidad=[
        {C_DESCRIPCION:'mg/kg',NP_ID_RESPUESTA:"1"},
        {C_DESCRIPCION:'mcg/kg',NP_ID_RESPUESTA:"2"}
    ];


    var datosEnviarCuantificaciones ={ DATOS :[{
          C_DESCRIPCION : "cuantificaciones",
          C_VALOR: "",
          C_MOTIVO : "  ",
          N_PUNTAJE : 0,
          NP_ID_RESPUESTA: 0
        }]};



/**
 * NOTA POST ANESTESICA   SECCIÓN MEDICAMENTOS
 */

    var currentMedicamento = {};
    $scope.selectedMedicamentos = [];
    $scope.querySearchMedicamento = function(text){
      var result = indicacionTerapeutica.querySearch(text);
      result = _.difference(result, $scope.selectedMedicamentos);
      return result
    }


    function loadViaAdministracion(){
       catalogos.getCatalogos('33').success(function(data){
         $scope.catViaAdministracion=data;
       })
       .error(function(data){
         console.log(data);
       });
   };

   loadViaAdministracion();


    $scope.selectedItemChangeMedicamento = function(_medicamento,get, index){ //get=true si estamos obteniendo los datos, get=false si estamos generando un nuevo medicamento
      if(_medicamento!=undefined){
        currentMedicamento = _medicamento;
        if(get)
           self.selectedMedicamentos=currentMedicamento;
        else
           self.selectedMedicamentos=[currentMedicamento];
        currentMedicamento = {};
        $scope.medicamentoText = '';
        $scope.medicamentos[index].seleccionado = self.selectedMedicamentos[0].NP_MEDICAMENTO;
        $scope.medicamentos[index].catPresentacion =  self.selectedMedicamentos[0].DOSIS;
      }
    };


    $scope.help = function(medicamento){
      mensajes.alerta('', _.filter(medicamento.DOSIS, function(el){
        return el.DESCRIPCION === medicamento.selected;
      })[0].DOSIS
      , 'Aceptar');
    };
    $scope.removeMedicamento = function(medicamento){
      self.selectedMedicamentos = _.without($scope.selectedMedicamentos, medicamento);
      for(var x in self.datos.medicacion){
        if(x>6)
          break;
        else
          self.datos.medicacion[x].C_MOTIVO="";
      }
    };

    $scope.formaFarmaceutica=function(medicamento){
      self.datos.medicacion[1].C_MOTIVO=medicamento;
    };



    var getNotasPostDetail = function(NP_NOTA_POSTQUIRURGICA, seccion){
        var url = "notaPostquirurgica/id/" + NP_NOTA_POSTQUIRURGICA;
        //var url = "notaPostquirurgica/id/" + 2401;
        //var url = "notaPostquirurgica/" + 10860;
        peticiones.getDatos(url).then(function(data)
            {
                blockUI.stop();
                setEditNotasPost(data, seccion);
            }).catch(function(err)
            {
                blockUI.stop();
                console.log(err);
            });
      };



     var setEditNotasPost = function(mDataArray,seccion){
          $scope.diagnosticoCie9                     = undefined;
          $scope.diagnosticoPrincipal                = undefined;
          self.selecItemCie9                               = mDataArray.NF_CIRUGIA_PLANEADA;
          self.cie09Programado                             = mDataArray.C_DESCRIPCION_CIE_9_REALIZADA;
          self.selectedItemCIE10                           = mDataArray.NF_CIE_10;




          self.datosNota.CIRUJANO = mDataArray.CIRUJANOS.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,NOMBRE: aux.NOMBRE,C_CEDULA: aux.C_CEDULA,NF_EXPEDIENTE:aux.NF_EXPEDIENTE, delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

          self.datosNota.AYUDANTE = mDataArray.AYUDANTE.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,NOMBRE: aux.NOMBRE,C_CEDULA: aux.C_CEDULA,NF_EXPEDIENTE:aux.NF_EXPEDIENTE,  delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

          self.datosNota.INSTRUMENTISTA = mDataArray.INSTRUMENTISTA.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,NOMBRE: aux.NOMBRE,C_CEDULA: aux.C_CEDULA,NF_EXPEDIENTE:aux.NF_EXPEDIENTE,  delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

          self.datosNota.ANESTESIOLOGO = mDataArray.ANESTESIOLOGO.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,NOMBRE: aux.NOMBRE,C_CEDULA: aux.C_CEDULA,NF_EXPEDIENTE:aux.NF_EXPEDIENTE,  delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

          self.datosNota.CIRCULANTE = mDataArray.CIRCULANTE.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,NOMBRE: aux.NOMBRE,C_CEDULA: aux.C_CEDULA,NF_EXPEDIENTE:aux.NF_EXPEDIENTE,  delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

          $scope.diagnosticoPrincipalEs = mDataArray.NF_CIE_10;
          $scope.cirugiaRealizadaEs = mDataArray.C_DESCRIPCION_CIE_9_REALIZADA;

          self.contadorMedicos = self.datosNota.CIRUJANO.length + self.datosNota.AYUDANTE.length + self.datosNota.INSTRUMENTISTA.length + self.datosNota.ANESTESIOLOGO.length + self.datosNota.CIRCULANTE.length;

          self.idSeleccion                                 = seccion;
          self.numColsapan = 2;
      };

 

$scope.guardarMedicamentos = function(actualizar){
    var datosEnviar = {

                  DATOS : $scope.medicamentos.map(function(data) {
                    var mArray = {seleccionado: data.seleccionado, canAplicada: data.canAplicada, presentacion: data.presentacion, via: data.via,catPresentacion: data.catPresentacion};
                      return {
                                C_DESCRIPCION : data.desc,
                                C_VALOR: JSON.stringify(mArray),
                                C_MOTIVO : data.udosis,
                                N_PUNTAJE : data.dosis,
                                NP_ID_RESPUESTA : data.NP_ID_RESPUESTA
                              }
                      })
                };
    enviar(datosEnviar,"medicamentosUtilizados",actualizar);
}


/**
 * TERMINA  NOTA POST ANESTESICA   SECCIÓN MEDICAMENTOS ----------------
 */


    self.identificarProcedencia = function(){
      if(quirofano.getNotaActual().length == 0){
        $state.go("quirofano");
      }
      if(quirofano.getNotaPosAnestesica() != 0){
          if(quirofano.getNotaActual().NOTAPOSTANESTESICA.NF_STATUS===1){//PENDIENTE
                $scope.editable=true;
                $scope.showButton=true;
                $scope.showNota=true;
                self.idPostAnestesica = quirofano.getNotaPosAnestesica();
            }else{
            if(quirofano.notaActual.NOTAPOSTANESTESICA.NF_STATUS ===2 || quirofano.notaActual.NOTAPOSTANESTESICA.NF_STATUS ===0){//FIRMADA CANCELADA
                $scope.editable=false;
                $scope.showButton=false;
                $scope.showNota=true;
                self.idPostAnestesica = quirofano.getNotaPosAnestesica();
                $scope.imprimirNota=true;
            }
          }
          self.idPostAnestesica = quirofano.getNotaPosAnestesica();
      }else{
        if(quirofano.getNotaPreanestesica() != 0){


         peticiones.getDatos("notaPostanestesica/registro/"  + $localStorage.paciente.idexpediente + "/" + usuario.usuario.NP_EXPEDIENTE + "/" + quirofano.getNotaPreanestesica())
        .then(function(data){
          console.log("DATOS A ENVIAR.....");
          self.idPostAnestesica = data.id;
          var datosEnviar = {
                  DATOS : $scope.tiempos.map(function(time) {
                              return {
                                C_DESCRIPCION : time.descripcion,
                                C_VALOR: (moment( time.fecha.toString()).format('DD/MM/YYYY, h:mm:ss a')),
                                C_MOTIVO : String(time.idx),
                                N_PUNTAJE : 0
                              }
                            })
                };
                enviar(datosEnviar,"duracionAnestesia", false);
                $scope.guardarCuantificaciones(false);
                $scope.guardarMedicamentos(false);
                $scope.verDatosEstadoCl(false);
        })
        .catch(function(err){
          mensajes.alerta("ERROR","NO SE PUEDE RECUPERAR EL HISTORIAL DE NOTAS POSTANESTÉSICAS","ACEPTAR");
        });
      }else{
        $state.go("quirofano");
        }
      }
    }
    self.identificarProcedencia();


    function enviar(datos,seccion, actualizar){
      console.log("ENVIANDO...");
      console.log(datos);
      console.log(seccion);
        if(actualizar){
           peticiones.putMethod("notaPostanestesica/"+$localStorage.paciente.idexpediente + "/" + self.idPostAnestesica + "/" + seccion,datos)
            .success(function(data){
              mensajes.alerta('OPERACIÓN CORRECTA','SE HA GUARDADO LA INFORMACÓN CORRECTAMENTE','ACEPTAR');
            })
            .error(function(data){
              mensajes.alerta('ERROR','NO SE HA PODIDO ACTUALIZAR EL FORMULARIO','ACEPTAR');
            });
        }else{
            peticiones.postDatos("notaPostanestesica/"+$localStorage.paciente.idexpediente+"/"+ self.idPostAnestesica +"/"+seccion,datos)
            .then(function(data){
            })
            .catch(function(err){
              console.log("Error al guardar los datos (Post)");
            });
        }
    };



    function cargarDatosSecciones(seccion){
      var respuesta = [];
      peticiones.getDatos('notaPostanestesica/'+$localStorage.paciente.idexpediente+'/'+ self.idPostAnestesica + '/' + seccion)
      .then(function(data){
        $scope.mEstadoClinico = data;
        parsearInformacion(seccion,data);
       
      })
      .catch(function(err){
        mensajes.alerta("ERROR","NO SE PUEDEN RECUPERAR LAS RESPUESTAS DE ESTA SECCIÓN","ACEPTAR");
      });
    };


    function parsearInformacion(seccion,respuesta){
      switch(seccion){
        case "medicamentosUtilizados":

       var index = 0;
               $scope.medicamentos = $scope.medicamentos.map(function(data) {
                              var datos =  _.where(respuesta, {C_DESCRIPCION: data.desc});
                              var aux = JSON.parse(datos[0].C_VALOR);
                          aux.catPresentacion = aux.catPresentacion.map(function(data){
                                 return {DESCRIPCION: data.DESCRIPCION} });
                           
               index += 1;
                              return {desc:datos[0].C_DESCRIPCION, dosis: datos[0].N_PUNTAJE,udosis:datos[0].C_MOTIVO, via: aux.via,cantidad:0,Ucantidad:' ',
                                      seleccionado:aux.seleccionado, NP_ID_RESPUESTA: datos[0].NP_ID_RESPUESTA, medicamentoText: aux.seleccionado,
                                      canAplicada: aux.canAplicada, presentacion: aux.presentacion.trim(), catPresentacion: aux.catPresentacion};
                            });
        break;
        case "duracionAnestesia":
                
                  var dataQuirS = _.where(respuesta, {C_DESCRIPCION: "SALIDA DE QUIRÓFANO"});
                  $scope.salidaQuirofano = moment(dataQuirS[0].C_VALOR).format('DD-MM-YYYY, h:mm:ss a');

                  var dataQuirE = _.where(respuesta, {C_DESCRIPCION: "INGRESO A QUIRÓFANO"});
                  $scope.ingresoQuirofano = moment(dataQuirE[0].C_VALOR).format('DD-MM-YYYY, h:mm:ss a');

                  var dataAnE = _.where(respuesta, {C_DESCRIPCION: "INICIO DE ANESTESIA"});
                  $scope.inicioAnestesia = moment(dataAnE[0].C_VALOR).format('DD-MM-YYYY, h:mm:ss a');

                  var dataAnS = _.where(respuesta, {C_DESCRIPCION: "TERMINA ANESTESIA"});
                  $scope.terminaAnestesia = moment(dataAnS[0].C_VALOR).format('DD-MM-YYYY, h:mm:ss a');

                  var dataCirE = _.where(respuesta, {C_DESCRIPCION: "INICIO DE CIRUJÍA"});
                  $scope.inicioCirugia = moment(dataCirE[0].C_VALOR).format('DD-MM-YYYY, h:mm:ss a');

                  var dataCirS = _.where(respuesta, {C_DESCRIPCION: "TERMINA DE CIRUGÍA"});
                  $scope.terminaCirugia =moment(dataCirS[0].C_VALOR).format('DD-MM-YYYY, h:mm:ss a'); 


                 /* $scope.tiempoQuirofano = moment('2012-02-03').diff(moment('2012-02-06'), 'd')
                  $scope.tiempoQuirofano = moment.preciseDiff(moment(dataQuirS[0].C_VALOR).format('YYYY-MM-DD HH:mm:ss'),moment(dataQuirE[0].C_VALOR).format('YYYY-MM-DD HH:mm:ss'));


                  $scope.tiempoAnestesia = moment.preciseDiff(moment(dataAnS[0].C_VALOR).format('YYYY-MM-DD HH:mm:ss'),moment(dataAnE[0].C_VALOR).format('YYYY-MM-DD HH:mm:ss'));


                  $scope.tiempoCirugia = moment.preciseDiff(moment(dataCirS[0].C_VALOR).format('YYYY-MM-DD HH:mm:ss'),moment(dataCirE[0].C_VALOR).format('YYYY-MM-DD HH:mm:ss'));*/
                
                  if(respuesta.length > 0){
                    var horas = [];
                    for(var i=0 ; i<respuesta.length ; i++){
                      var aux = parseInt(respuesta[i].C_MOTIVO);
                      horas[aux] = respuesta[i].C_VALOR;
                      $scope.tiempos[aux].fecha = new Date(respuesta[i].C_VALOR);
                      $scope.tiempos[aux].NP_ID_RESPUESTA = respuesta[i].NP_ID_RESPUESTA;
                    }
                  }
        break;
        case "cuantificaciones":
          datosEnviarCuantificaciones.DATOS[0].NP_ID_RESPUESTA = respuesta[0].NP_ID_RESPUESTA;
          var datCuantificaciones = JSON.parse(respuesta[0].C_VALOR);
          $scope.cuantificaciones = $scope.cuantificaciones.map(function(dat){
            var mDat = _.where(datCuantificaciones, {desc: dat.desc});
              return {desc: mDat[0].desc, hora1: mDat[0].hora1, hora2: mDat[0].hora2, hora3: mDat[0].hora3, hora4: mDat[0].hora4, total: mDat[0].total};
          });
        break;
        case 3:
        break;
        case "estadoClinico":
           var  mAux = _.where(respuesta, {C_DESCRIPCION: "cIngreso"});
            $scope.eClinico.cIngreso = mAux[0].C_VALOR;

            mAux = _.where(respuesta, {C_DESCRIPCION: "sVitales"});
            $scope.eClinico.sVitales = mAux[0].C_VALOR;

            mAux = _.where(respuesta, {C_DESCRIPCION: "venoclisis"});
            $scope.eClinico.venoclisis = mAux[0].C_VALOR;

            mAux = _.where(respuesta, {C_DESCRIPCION: "aldrete"});
            $scope.eClinico.aldrete = JSON.parse(mAux[0].C_VALOR);

            mAux = _.where(respuesta, {C_DESCRIPCION: "bromage"});
            $scope.eClinico.bromage = JSON.parse(mAux[0].C_VALOR);

            mAux = _.where(respuesta, {C_DESCRIPCION: "ramsay"});
            $scope.eClinico.ramsay = JSON.parse(mAux[0].C_VALOR);

             mAux = _.where(respuesta, {C_DESCRIPCION: "dolor"});
            $scope.eClinico.dolor = JSON.parse(mAux[0].C_VALOR);

            getNotasPostDetail(quirofano.getNpNotaPost(), 5);
        break;
        case 5:
        break;
        case 6:
        break;
      }
    };




    $scope.borrarPreAnestesica = function(){
      $scope.idNota.titulo = '';
      $scope.idNota.desc = '';
      $scope.idNota.tipo = '';
      $scope.idNota.id = 0;
      $scope.idNota.estatus=-1;
    };

    /////////////////////////////////
    /////  MENÚS
    /////////////////////////////////

    $scope.secciones =  [
                          {titulo:'REGISTRAR LOS MEDICAMENTOS UTILIZADOS', visible: false, id: 1},
                          {titulo:'REGISTRAR LA DURACIÓN DE LA ANESTESIA', visible: false,id: 2},
                          {titulo:'REGISTRAR INCIDENTES Y ACCIDENTES ATRIBUIBLES A LA ANESTESIA', visible: false, id: 15,buscarCuest: true},
                          {titulo:'CUANTIFICACIONES DE SANGRADO Y SOLUCIONES APLICADAS O TRANSFUSION', visible: false, id: 4},
                          {titulo:'REGISTRO DEL ESTADO CLÍNICO DEL PACIENTE A SU EGRESO', visible: false, id: 31},
                          {titulo:'REGISTRO DEL PLAN, MANEJO Y TRATAMIENTO INMEDIATO', visible: false, id: 16,buscarCuest: true}
                        ];

    /////////////////////////////////
    /////  DURACIÓN DE ANESTESIA
    /////////////////////////////////
    $scope.fechaActual = new Date();
    $scope.tiempos=[
      {descripcion:"INGRESO A QUIRÓFANO", fecha:new Date(),idx:0, minDate: new Date(), maxDate: new Date(),NP_ID_RESPUESTA:0},
      {descripcion:"INICIO DE ANESTESIA", fecha:new Date(),idx:1, minDate: new Date(), maxDate: new Date(),NP_ID_RESPUESTA:0},
      {descripcion:"INICIO DE CIRUJÍA", fecha:new Date(),idx:2, minDate: new Date(), maxDate: new Date("02/05/2016"),NP_ID_RESPUESTA:0},
      {descripcion:"TERMINA DE CIRUGÍA", fecha:new Date(),idx:3, minDate: new Date(), maxDate: new Date(),NP_ID_RESPUESTA:0},
      {descripcion:"TERMINA ANESTESIA", fecha:new Date(),idx:4, minDate: new Date(), maxDate: new Date(),NP_ID_RESPUESTA:0},
      {descripcion:"SALIDA DE QUIRÓFANO", fecha:new Date(),idx:5, minDate: new Date(), maxDate: new Date(),NP_ID_RESPUESTA:0},
    ];

    $scope.tCirujia = "0 HORAS";
    $scope.tQuirofano = "0 HORAS";
    $scope.tAnestesia = "0 HORAS";

    var revisarHorarios = function(index){
     if(moment($scope.tiempos[5].fecha).diff(moment($scope.tiempos[0].fecha)) >0){
        if(moment($scope.tiempos[4].fecha).diff(moment($scope.tiempos[1].fecha)) > 0){
          if(moment($scope.tiempos[3].fecha).diff(moment($scope.tiempos[2].fecha)) > 0){
            if(moment($scope.tiempos[1].fecha).diff(moment($scope.tiempos[0].fecha)) >0){
              if(moment($scope.tiempos[2].fecha).diff(moment($scope.tiempos[1].fecha)) >0){
                return true;
              }else{
                mensajes.alerta("ERROR","POR FAVOR VERIFIQUE EL INICIO DE LA ANESTESIA Y EL INICIO DE LA CIRUGÍA" ,"OK");
              }
            }else{
              mensajes.alerta("ERROR","POR FAVOR VERIFIQUE EL INGRESO A QUIRÓFANO Y EL INICIO DE LA ANESTESIA" ,"OK");
            }
          }else{
             mensajes.alerta("ERROR","POR FAVOR VERIFIQUE EL INICIO Y TERMINO DE LA CIRUGÍA" ,"OK");
          }
        }else{
          mensajes.alerta("ERROR","POR FAVOR VERIFIQUE EL INICIO Y TERMINO DE LA ANESTESIA" ,"OK");
        }
     }else{
       mensajes.alerta("ERROR","POR FAVOR VERIFIQUE LA HORA DE INGRESO Y SALIDA DE QUIROFANO" ,"OK");
     }
     return false;
    };


    $scope.datos ={
        nombre: $localStorage.paciente.nombrePaciente,
        H: $localStorage.paciente.idPaciente,
        id : usuario.usuario.NP_EXPEDIENTE,
        exp: $localStorage.paciente.expediente,
        edad: pacientes.calculaEdad($localStorage.paciente.f_nacimiento),
        fNac: $localStorage.paciente.f_nacimiento,
        genero: $localStorage.paciente.genero,
    };

    $scope.guardar = function(actualizar){
        var datosEnviar = {
                    DATOS : $scope.tiempos.map(function(time) {
                                return {
                                  C_DESCRIPCION : time.descripcion,
                                  C_VALOR: (moment( time.fecha.toString()).format('MM-DD-YYYY, HH:mm:ss')),
                                  C_MOTIVO : String(time.idx),
                                  N_PUNTAJE : 0,
                                  NP_ID_RESPUESTA: time.NP_ID_RESPUESTA
                                }
                              })
                    };
                    if(datosEnviar.DATOS[0].NP_ID_RESPUESTA != 0){
                        if(revisarHorarios()){
                          enviar(datosEnviar,"duracionAnestesia",actualizar);
                        }
                    }else{
                      enviar(datosEnviar,"duracionAnestesia",actualizar);
                    }      
    };


    /////////////////////////////////
    /////  INCIDENTES Y ACCIDENTES
    /////////////////////////////////

    $scope.cuantificaciones = [
      {desc:'HARTMANN (BALANCE PARCIAL EN MILILITROS)',hora1:0,hora2:0,hora3:0,hora4:0,total:0},
      {desc:'SALINA (BALANCE PARCIAL EN MILILITROS)',hora1:0,hora2:0,hora3:0,hora4:0,total:0},
      {desc:'MIXTA (BALANCE PARCIAL EN MILILITROS)',hora1:0,hora2:0,hora3:0,hora4:0,total:0},
      {desc:'PAQUETE GLOBULAR (BALANCE PARCIAL EN MILILITROS)',hora1:0,hora2:0,hora3:0,hora4:0,total:0},
      {desc:'PLASMA Y DERIVADOS (BALANCE PARCIAL EN MILILITROS)',hora1:0,hora2:0,hora3:0,hora4:0,total:0}
    ];

    $scope.calcularCuantificacion = function(obj){
      obj.total = obj.hora1+obj.hora2+obj.hora3+obj.hora4;
    };

    $scope.guardarCuantificaciones = function(actualizar){
      console.log("----------ACTUALIZAR CUANTIFICACIONES--------");
      datosEnviarCuantificaciones.DATOS[0].C_VALOR = JSON.stringify($scope.cuantificaciones);
      console.log( datosEnviarCuantificaciones);
      enviar(datosEnviarCuantificaciones,"cuantificaciones", actualizar);
    };

    /////////////////////////////////
    /////  estado clínico
    /////////////////////////////////

    $scope.catCIE10;
    $scope.searchDiagnostico1;
    $scope.itemDiagnostico1;
    $scope.catCIE9;
    $scope.searchCirugia;
    $scope.itemCirugia;

    $scope.eClinico = {
      cIngreso : '',
      sVitales : '',
      venoclisis : '',
      aldrete: {
        valores : [
          {desc : 'ACTIVIDAD', valores : [0,0,0,0,0,0],auxiliares:['4 EXTREMIDADES','2 EXTREMIDADES','0 EXTREMIDADES']},
          {desc : 'RESPIRACIÓN', valores : [0,0,0,0,0,0],auxiliares:['CORRECTA','DIFICULTOSA','CON AYUDA']},
          {desc : 'CIRCULACIÓN', valores : [0,0,0,0,0,0],auxiliares:['TA +/- 20% BASAL','TA +/- 50% BASAL','TA > 50% DIFIC.']},
          {desc : 'CONCIENCIA', valores : [0,0,0,0,0,0],auxiliares:['TOTALMENTE DESPIERTO','DESPIERTA A LLAMADA','NO RESPONDE']},
          {desc : 'COLORACIÓN', valores : [0,0,0,0,0,0],auxiliares:['ROSADA','PÁLIDA','CIANÓTICA']},
        ],
        total : [0,0,0,0,0,0]
      },
      bromage: [
        {desc : 'COMPLETO: INCAPAZ DE MOVILIZAR PIES Y RODILLAS', valores : [0,0,0,0,0,0]},
        {desc : 'CASI COMPLETO: SOLO CAPAZ DE MOVER PIES', valores : [0,0,0,0,0,0]},
        {desc : 'PARCIAL: CAPAZ DE MOVER RODILLAS', valores : [0,0,0,0,0,0]},
        {desc : 'NULO: FLEXIÓN COMPLETA DE RODILLAS Y PIES', valores : [0,0,0,0,0,0]}
      ],
      ramsay: [
        {desc : 'PACIENTE ANSIOSO Y AGITADO', valores : [0,0,0,0,0,0]},
        {desc : 'PACIENTE COLABORADOR, ORIENTADO Y TRANQUILO', valores : [0,0,0,0,0,0]},
        {desc : 'PACIENTE DORMIDO, QUE OBEDECEA ÓRDENES', valores : [0,0,0,0,0,0]},
        {desc : 'PACIENTE DORMIDO, CON RESPUESTA A ESTÍMULOS AUDITIVOS INTENSOS', valores : [0,0,0,0,0,0]},
        {desc : 'PACIENTE DORMIDO, CON RESPUESTA MÍNIMA A ESTÍMULOS ', valores : [0,0,0,0,0,0]},
        {desc : 'PACIENTE DORMIDO, SIN RESPUESTA A ESTÍMULOS', valores : [0,0,0,0,0,0]}
      ],
      dolor: [
        {valores : [0,0,0,0,0,0]},
      ]
    };

    $scope.selectedItemChangeDiagnostico1 = function(item) {
      item.C_DESCRIPCION = item.C_DESCRIPCION.toUpperCase();
      $scope.diagnosticoPrincipal = item;
      if($scope.diagnosticoPrincipal != undefined){
          $scope.catCIE10 = _.filter($scope.catCIE10, function(e){
          return e.NP_ID !== $scope.diagnosticoPrincipal.NP_ID;
        });
      }
    };

    $scope.querySearchDiagnostico1 = function(query){
      if($scope.diagnosticoPrincipal != undefined){
        $scope.catCIE10.push($scope.diagnosticoPrincipal);
      }
      $scope.diagnosticoPrincipal = undefined;
      if(query.length>3){
        return _.filter($scope.catCIE10,function(e){
          return e.C_DESCRIPCION.toUpperCase().indexOf(query.toUpperCase())>=0;
        });
      }
    };

    var obtenerCie09 = function(url){
      peticiones.getDatos(url)
        .then(function(data)
          {
            self.catalogoCie09List = data;
          }).catch(function(data)
          {
            error = data;
          });
    };

    var getCatalogoCIE09 = function(mSexo){
        var mSexo = ($localStorage.paciente.genero == "MASCULINO") ? "M" : "F";
       if(self.catalogoCie09List == undefined){
          obtenerCie09(urlCie09 + mSexo);
        }else{
          if(sexo != mSexo){
            obtenerCie09(urlCie09 + mSexo);
          }
        }
        return self.catalogoCie09List;
    };

    $scope.sumarPuntuaciones = function(opc) {
      var cont = 0;
      var aux = $scope.eClinico.aldrete.valores;
      for(var i=0 ; i<aux.length ; i++){
        cont += parseInt(aux[i].valores[opc]);
      }
      $scope.eClinico.aldrete.total[opc] = cont;
    };



    $scope.verDatosEstadoCl = function(actualizar){
        var datosEnviar = {
            DATOS : []
        }
        for(var a in $scope.eClinico){
          this.parseData = function(par){
            if (typeof par ==='object') {
              return JSON.stringify(par);
            }
            return par;
          };
          var mAux = _.where($scope.mEstadoClinico, {C_DESCRIPCION: a});

        
              var NP_ID_RESPUESTA1 = ($scope.mEstadoClinico.length > 0) ?  mAux[0].NP_ID_RESPUESTA : 0;
              var aux = {
                          C_DESCRIPCION : a,
                          C_VALOR: this.parseData(($scope.eClinico[a] == "") ? " " : $scope.eClinico[a]),
                          C_MOTIVO : " ",
                          N_PUNTAJE : 0,
                          NP_ID_RESPUESTA: NP_ID_RESPUESTA1
                        }
              datosEnviar.DATOS.push(aux);
          


        }
        enviar(datosEnviar,"estadoClinico", actualizar);
    }


    $scope.guardarEstadoClinico = function(){
        $scope.verDatosEstadoCl(true);
    };



    /////////////////////////////////
    /////  Médicos
    /////////////////////////////////
    $scope.searchText = '';
    $scope.selected = [];
    $scope.eliminados = [];

    $scope.selectedItemChange = function(item) {
      if(item !== undefined){
        var idx = _.findLastIndex($scope.medicos, {N_ID: item.N_ID});
        $scope.selected[idx] = true;
        $scope.searchText = '';
      }
    };

    $scope.querySearch = function(query){
      if(query.length>3){
        return _.filter($scope.medicos,function(e){
          return e.C_NOMBRE.toUpperCase().indexOf(query.toUpperCase())>=0;
        });
      }
    };

    $scope.eliminarMedico = function(obj){
      for(var i=0 ; i<$scope.eliminados.length ; i++){
        var idx = _.findLastIndex($scope.medicos, $scope.eliminados[i]);
        $scope.selected[idx] = false;
      }
      $scope.eliminados = [];
    };

    $scope.query = {
        order: 'clave_servicio',
        limit: 5,
        page: 1
    };

    $scope.columns = [
      {
        name: 'NOMBRE',
        descendFirst: true,
        orderBy: 'C_NOMBRE',
        width : '50%'
      },
      {
        name: 'CÉDULA',
        orderBy: 'N_ID',
        width : '25%'
      },
      {
        name: 'PERFIL',
        width : '25%',
        orderBy : 'C_PERFIL'
      }
    ];

    $scope.onOrderChange = function(page, limit) {
        var deferred = $q.defer();
        $timeout(function () {
          deferred.resolve();
        }, 2000);
        return deferred.promise;
    };

    /////////////////////////////////
    /////  MEDICAMENTOS UTILIZADOS
    /////////////////////////////////

    $scope.seleccionados = [];
    $scope.CBM = [
      {desc : 'PENICILINA', otro:'PENICILINA2',image: '/images/icons/svg/ic_person_black_24px.svg'},
      {desc : 'AAAAAAAAAA', otro:'AAAAAAAAAA2',image: '/images/icons/svg/ic_person_black_24px.svg'},
      {desc : 'BBBBBBBBBB', otro:'BBBBBBBBBB2',image: '/images/icons/svg/ic_person_black_24px.svg'},
      {desc : 'SSSSSSSSSS', otro:'SSSSSSSSSS2',image: '/images/icons/svg/ic_person_black_24px.svg'},
      {desc : 'DDDDDDDDDD', otro:'DDDDDDDDDD2',image: '/images/icons/svg/ic_person_black_24px.svg'},
      {desc : 'WWWWWWWWWW', otro:'WWWWWWWWWW2',image: '/images/icons/svg/ic_person_black_24px.svg'},
      {desc : 'QQQQQQQQQQ', otro:'QQQQQQQQQQ2',image: '/images/icons/svg/ic_person_black_24px.svg'},
      {desc : 'RRRRRRRRRR', otro:'RRRRRRRRRR2',image: '/images/icons/svg/ic_person_black_24px.svg'},
      {desc : 'FFFFFFFFFF', otro:'FFFFFFFFFF2',image: '/images/icons/svg/ic_person_black_24px.svg'}
    ];

//{cantidad: ' ', pres: ' '}
   $scope.medicamentos = [
      {desc:'INDUCTOR',dosis:0,udosis:' ',via:' ',cantidad:0,Ucantidad:' ',seleccionado:' ',NP_ID_RESPUESTA: 0,canAplicada: 0, presentacion: ' ', catPresentacion: []},
      {desc:'ANALGÉSICOS',dosis:0,udosis:' ',via:' ',cantidad:0,Ucantidad:' ',seleccionado:' ',NP_ID_RESPUESTA:0 ,canAplicada: 0, presentacion: ' ', catPresentacion: []},
      {desc:'COADYUVANTES',dosis:0,udosis:' ',via:' ',cantidad:0,Ucantidad:' ',seleccionado:' ',NP_ID_RESPUESTA:0,canAplicada: 0, presentacion: ' ', catPresentacion: []},
      {desc:'RELAJANTES',dosis:0,udosis:' ',via:' ',cantidad:0,Ucantidad:' ',seleccionado:' ',NP_ID_RESPUESTA:0,canAplicada: 0, presentacion: ' ', catPresentacion: []},
      {desc:'INHALADORES',dosis:0,udosis:' ',via:' ',cantidad:0,Ucantidad:' ',seleccionado:' ',NP_ID_RESPUESTA:0,canAplicada: 0, presentacion: ' ', catPresentacion: []}
    ];

    var self = this;
    self.querySearch = querySearch;
    self.filterSelected = true;

    function querySearch (query) {
      var results = query ?
          $scope.CBM.filter(createFilterFor(query)) : [];
      return results;
    };
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(CBM) {
        return (angular.lowercase(CBM.desc).indexOf(lowercaseQuery) != -1);
      };
    };






    /********************************FUNCIONES PARA CUESTIONARIOS**********************************/
    /*
    /*  cuestionario/preanestesica/17/10061/1500
    /*  cuestionario/preanestesica/tipo_cuestionario/especialidad/np_idnotapreanestesica    $scope.idNota*/
    var cuestionarioEspecifico = function(NP_TIPO_CUESTIONARIO, ID_ESPECIALIDAD, NF_PACIENTE, ID_NOTA, MEDICINA_GENERAL){
       // var url = "/cuestionario/" + NP_TIPO_CUESTIONARIO + "/" + NF_PACIENTE + "/";
       var url = "cuestionario/postanestesica/" + NP_TIPO_CUESTIONARIO + "/" + ID_ESPECIALIDAD + "/"+ ID_NOTA;
        peticiones.getDatos(url)
        .then(function(data) {
            if(data.SECCION){
                self.contesCuestionarioGeneral = !$scope.editable;
                self.contesCuestionario = !$scope.editable;
                cuestionarioService.setCuestionario(data);
                if(MEDICINA_GENERAL){
                    self.mCuestionarioGeneral = data
                }else{
                    self.mCuestionario=data;
                }
            }
        })
        .catch(function(err) {
              console.log(err);
        });
    };

   var limpiarCuestionario = function(){
      cuestionarioService.reset(self.mCuestionario);
      cuestionarioService.reset(self.mCuestionarioGeneral);
    }

    self.cambiarRadio = function(key_seccion,key_pregunta,bandera,MEDICINA_GENERAL){
      console.log('es medicna  general ', MEDICINA_GENERAL)
        if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
          console.log('No debe entrar acá',self.mCuestionarioGeneral )
              cuestionarioService.cambiarRadio(key_seccion,key_pregunta, bandera, self.mCuestionarioGeneral,MEDICINA_GENERAL);
            }else{
              console.log('Siiii debe entrar acá',self.mCuestionarioGeneral )
              cuestionarioService.cambiarRadio(key_seccion,key_pregunta, bandera, self.mCuestionario,MEDICINA_GENERAL);
            }
        };

    self.seleccionarRespuesta = function(key_seccion,key_pregunta,key_respuesta,MEDICINA_GENERAL){
        if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
              cuestionarioService.seleccionarRespuesta(key_seccion,key_pregunta, key_respuesta, self.mCuestionarioGeneral);
        }else{
              cuestionarioService.seleccionarRespuesta(key_seccion,key_pregunta, key_respuesta, self.mCuestionario);
        }
      };

    self.guardarRespuestas = function(MEDICINA_GENERAL){
      var cuestionarioFin = {};

      if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
        cuestionarioFin = self.mCuestionarioGeneral;
      }else{
        cuestionarioFin = self.mCuestionario;
      }


     // var cuestionarioFin = (MEDICINA_GENERAL) ? angular.toJson(self.mCuestionarioGeneral) : angular.toJson(self.mCuestionario);




      var result = cuestionarioService.revisarRespuestas(angular.fromJson(cuestionarioFin));
      if(result.correcta){
          var cuestionarioResult = cuestionarioService.crearCuestionarioFinal(angular.fromJson(result.cuestionario));
          cuestionarioResult.NF_MEDICO = usuario.usuario.NP_EXPEDIENTE
          cuestionarioResult.NF_PACIENTE = $localStorage.paciente.expediente.substring(0, $localStorage.paciente.expediente.length - 4);
          cuestionarioResult.NP_NOTA_PREANESTESICA = self.idPostAnestesica;
          postRespuestas(cuestionarioResult,MEDICINA_GENERAL);
      }else{
        mensajes.alerta("ERROR","DEBE CONTESTAR TODAS LAS PREGUNTAS OBLIGATORIAS" ,"OK");
      }
    };

    $scope.guardarObs = function(seccion){
        self.datos[seccion].observaviones.unshift({descripcion:self.observacion,fecha:new Date()});
        self.observacion = '';
    };

   self.guardarRespuestas = function(MEDICINA_GENERAL){
      //cuestionarioService.setContestarCuestionario(!$scope.editable);
      //
      //var cuestionarioFin = (MEDICINA_GENERAL) ? angular.toJson(self.mCuestionarioGeneral) : angular.toJson(self.mCuestionario);

      var cuestionarioFin = {};

      if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
        cuestionarioFin = self.mCuestionarioGeneral;
      }else{
        cuestionarioFin = self.mCuestionario;
      }


      var result = cuestionarioService.revisarRespuestas(angular.fromJson(cuestionarioFin));
      if(result.correcta){
          var cuestionarioResult = cuestionarioService.crearCuestionarioFinal(angular.fromJson(result.cuestionario));
          cuestionarioResult.NF_MEDICO = usuario.usuario.NP_EXPEDIENTE
          cuestionarioResult.NF_PACIENTE = $localStorage.paciente.expediente.substring(0, $localStorage.paciente.expediente.length - 4);
          cuestionarioResult.NP_NOTA_PREANESTESICA = self.idPostAnestesica;
          postRespuestas(cuestionarioResult,MEDICINA_GENERAL);
      }else{
        mensajes.alerta("ERROR","DEBE CONTESTAR TODAS LAS PREGUNTAS OBLIGATORIAS" ,"OK");
      }
    };

    $scope.guardarObs = function(seccion){
        self.datos[seccion].observaviones.unshift({descripcion:self.observacion,fecha:new Date()});
        self.observacion = '';
    };

    var  postRespuestas = function(mData,MEDICINA_GENERAL){
     if(mData.CONTESTADO){//Put
        peticiones.putMethod('cuestionario/respuestas/',mData)
          .success(function(data){
            mensajes.alerta("","DATOS ACTUALIZADOS CORRECTAMENTE" ,"OK");
          })
          .error(function(data){
            mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
          });

      }else{//Post
          peticiones.postDatos('cuestionario/respuestas/',mData)
            .then(function(data) {
              if(MEDICINA_GENERAL){
                self.mCuestionarioGeneral.CONTESTADO = true;//checar tenía diferente de true
              }else{
                self.mCuestionario.CONTESTADO = true;
              }
              mensajes.alerta("","DATOS ACTUALIZADOS CORRECTAMENTE" ,"OK");
            })
            .catch(function(err) {
                console.log(err);
            });
      }
    };

    $scope.seleccionar = function(index,seccion){
      if(index != ant){
          switch(index){
              case 0:
              cargarDatosSecciones("medicamentosUtilizados");
              break;
              case 1:
               cargarDatosSecciones("duracionAnestesia");
              break;
              case 3:
                  cargarDatosSecciones("cuantificaciones");

              break;
              case 4: //estadoClinico
                  cargarDatosSecciones("estadoClinico");
              break;
              case 'TERMINA ANESTESIA':
                $scope.tAnestesia = horas+" "+minutos;
              break;
              case 'SALIDA DE QUIRÓFANO':
                $scope.tQuirofano = horas+" "+minutos;
              break;
          }
      }
      $scope.secciones[index].visible = !$scope.secciones[index].visible;
      if(index!==ant && ant >= 0){
        $scope.secciones[ant].visible = false;
      }
      ant = index ;
      if($scope.secciones[index].buscarCuest && $scope.secciones[index].visible){
         self.mCuestionario.SECCION.splice(0,self.mCuestionario.SECCION.length);
        self.mCuestionarioGeneral.SECCION.splice(0,self.mCuestionarioGeneral.SECCION.length);
        limpiarCuestionario();
        var NP_EXPEDIENTE_PACIENTE =$localStorage.paciente.idexpediente;
        //PETICIÓN PARA EL CUESTIONARIO ESPECIFICO
        cuestionarioEspecifico($scope.secciones[index].id,idEspecialidad,NP_EXPEDIENTE_PACIENTE,self.idPostAnestesica, false);
      }
    };

    var getIndiceArray = function(mArray,key){
      for(var j = mArray.length -1; j >= 0; j--){
          if(mArray[j].$$hashKey == key){
            return j;
          }
      }
      return null;
    };

    $scope.imprimir=function(){
   ///notaPreanestesica/reporte/{idNotaPreanestesica
      var url="notaPostanestesica/reporte/"+ self.idPostAnestesica;
       peticiones.getMethodPDF(url)
         .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          }).error(function(data){
              console.log(data);
          });
    };

    $scope.regresar=function(){
      $state.go($localStorage.stateAnterior);
    };
     //FUNCIONES PARA FIRMA
    $scope.cargaLlave = function (fileContent){
      $scope.keyPriv=fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
      $scope.certificado=fileContent;
    };

    $scope.sellar= function() {
      if( $scope.keyPriv!=undefined && $scope.certificado!=undefined){
        var aux = {};
        aux.FIRMA=[];
        aux.DATOS=[];
        aux.idDoctor = parseInt(usuario.usuario.NP_EXPEDIENTE);
        aux.FIRMA.push({keyPriv:$scope.keyPriv, certificado:$scope.certificado});
        aux.DATOS.push({idNotaPostanestesica:self.idPostAnestesica
        });
        //realizar petición
        var url= "notaPostanestesica/" + $localStorage.paciente.idexpediente +"/"+self.idPostAnestesica;
        peticiones.putMethod(url,aux)
         .success(function(data){
              mensajes.alerta("ALERTA","SE FIRMO CORRECTAMENTE","OK");
              $scope.showFirma=false;
              $scope.showNota=false;
              $scope.keyPriv="";
              $scope.certificado="";
              $state.go("quirofano");
            })
            .error(function(data){
             mensajes.alerta("ALERTA","LLAVE O CLAVE INCORRECTAS","OK");
            });
      }else{
          mensajes.alerta('ALERTA','SELECCIONE LA LLAVE O EL CERTIFICADO','ACEPTAR');
      }
    };

    $scope.cancelar=function(){
        $scope.showFirma=false;
        $scope.showButton=true;
        $scope.showNota=true;
        $scope.imprimirNota=false;
        $scope.keyPriv="";
        $scope.certificado="";
    };
    $scope.showKey=function(){
        $scope.showFirma=true;
        $scope.showButton=false;
        $scope.showNota=false;
        $scope.imprimirNota=false;
    };

    $scope.eliminarNota=function(){
      var url= "notaPostanestesica/" + $localStorage.paciente.idexpediente +"/"+$self.idPostAnestesica;
      peticiones.deleteDatos(url)
        .then(function(data) {
          mensajes.alerta("ALERTA",data.message.toUpperCase(),"OK");
          $scope.showEliminarNota=false;
          $scope.showButton=false;
          $scope.showNota=false;
          $state.go("quirofano");
        })
        .catch(function(err) {
          console.log(err);
        });
    };


    var tipoRespuesta = function(seccion,data){
      var bandera = true;
       switch(seccion){
              case "medicamentosUtilizados":
                  for (var i = data.length - 1; i >= 0; i--) {
                    if(data[i].C_MOTIVO ==" " || data[i].N_PUNTAJE ==" " || data[i].C_VALOR ==" " ){
                        bandera = false;
                    }
                  };
                 
              break;
              case "duracionAnestesia":
                  for (var i = data.length - 1; i >= 0; i--) {
                    if(data[i].C_MOTIVO ==" " || data[i].C_VALOR ==" " ){
                        bandera = false;
                       
                    }
                  };
              
              break;
              case "estadoClinico":
                  for (var i = data.length - 1; i >= 0; i--) {     
                    if(data[i].C_VALOR ==" "){
                        bandera = false;
                    }
                  };
              break;
              case 4: //estadoClinico
              
              break;
              case "cuantificaciones":
                var datCuantificaciones = JSON.parse(data[0].C_VALOR);
                 var mDat = _.where(datCuantificaciones, {total: 0});
                 if(mDat.length > 0){
                    bandera = false;
                 }

              break;
              case 'SALIDA DE QUIRÓFANO':
              
              break;
          }
          return bandera;
    };

    var verificarDatosFirmados = function(seccion){
      var url = 'notaPostanestesica/'+$localStorage.paciente.idexpediente+'/'+ self.idPostAnestesica + '/' + seccion
      var defered = $q.defer();
      var promise = defered.promise;
        peticiones.getDatosStatus(url)
        .then(function(data){
            if(tipoRespuesta(seccion, data.data)){
               defered.resolve(true);
            }else{
              defered.reject(false);
            }
        }).catch(function(error){
            defered.reject(err);
        });

          return promise;
    };


      self.firmarDatos = function(){
        var urlCuestionario = 'cuestionario/estatusPostanestesica/' + $localStorage.paciente.idexpediente + '/' + self.idPostAnestesica;
            verificarDatosFirmados("medicamentosUtilizados")
            .then(function(data){
              verificarDatosFirmados("duracionAnestesia")
                .then(function(data){
                    verificarDatosFirmados("estadoClinico")
                      .then(function(data){    
                       // verificarDatosFirmados("cuantificaciones")
                       // .then(function(data){

                            peticiones.getDatosStatus(urlCuestionario)
                              .then(function(data){
                                  if(data.status == 200 && data.data.estatus){
                                      $scope.showKey();
                                  }else{
                                     if(data.status == 200 && data.data.estatus ==false){
                                        mensajes.alerta("ALERTA","POR FAVOR REGISTRE LAS  LAS PREGUNTAS OBLIGATORIAS EN LA SECCIÓN \n REGISTRA INCIDENTES Y DEL REGISTRO DEL PLAN Y TRATAMIENTO INMEDIATO","OK");
                                     }
                                  }
                              })
                              .catch(function(error){
                                console.log(error);
                            });   
                        //  }).catch(function(error){
                        //     mensajes.alerta("ALERTA","POR FAVOR REGISTRE LAS CUANTIFICACIONES DE SANGRADO Y LAS SOLUCIONES APLICADAS","OK");
                        //  })
                        }).catch(function(error){
                             mensajes.alerta("ALERTA","POR FAVOR REGISTRE EL ESTADO CLINICO DEL PACIENTE A SU EGRESO","OK");
                        })
                  }).catch(function(error){
                       mensajes.alerta("ALERTA","POR FAVOR REGISTRE LA DURACIÓN DE LA ANESTESIA","OK");
                  });
            }).catch(function(error){
                mensajes.alerta("ALERTA","POR FAVOR REGISTRE LOS MEDICAMENTOS UTILIZADOS","OK");
            });
      }

$scope.duraAnestesiaTab = [
    {
      name: 'INGRESO A QUIROFANO',
        orderBy: 'idEspecialidad',
        width : '10%'
    },
    {
        name: 'INICIO DE ANESTESIA',
        orderBy: 'display',
        width : '10%'
    }, {
      descendFirst: true,
      name: 'INICIO DE CIRUGÍA',
        orderBy: 'idEspecialidad',
        width : '10%'
    },
     {
      name: 'TERMINO DE CIRUGÍA',
        orderBy: 'idEspecialidad',
        width : '10%'
  },
  {
      name: 'TERMINO ANESTESIA',
        orderBy: 'idEspecialidad',
        width : '10%'
  },
  {
      name: 'SALIDA DE QUIROFANO',
        orderBy: 'idEspecialidad',
        width : '10%'
  },
     {
      name: 'DURACIÓN DE LA ANESTESIA',
        orderBy: 'idEspecialidad',
        width : '10%'
  },
  {
      name: 'DURACIÓN DEL QUIRÓFANO',
        orderBy: 'idEspecialidad',
        width : '15%'
  },
  {
      name: 'DURACIÓN DE LA CIRUGÍA',
        orderBy: 'idEspecialidad',
        width : '15%'
  }];


  $scope.medicosActividad = [
    {
      name: 'EXPEDIENTE',
        orderBy: 'idEspecialidad',
        width : '15%'
    },
    {
      name: 'CEDULA',
        orderBy: 'idEspecialidad',
        width : '15%'
    },
    {
        name: 'NOMBRE',
        orderBy: 'display',
        width : '55%'
    }, {
      descendFirst: true,
      name: 'ACTIVIDAD REALIZADA',
        orderBy: 'idEspecialidad',
        width : '15%'
    }];

  }]);
})();
