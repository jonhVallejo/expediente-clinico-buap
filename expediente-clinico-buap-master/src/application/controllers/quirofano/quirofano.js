
(function(){
'use strict';
angular.module('expediente')
    .controller('QuirofanoQuirofanoCtrl',['$scope','usuario','$state','peticiones','quirofano','$mdDialog','mensajes','$localStorage','$window','pacientes','reportes',
    	function ($scope,usuario,$state,peticiones,quirofano,$mdDialog,mensajes,$localStorage,$window,pacientes,reportes) {
	  	var self=this;
		self.historicoQuirofano =[];
	    $scope.tipo="";
	    $scope.registro=[];
	    $scope.paciente=$localStorage.paciente;
	    $scope.paciente.edad=pacientes.calculaEdad($localStorage.paciente.f_nacimiento);
	    $scope.perfil=usuario.perfil;
	    getHistoricoNota();
	      //Variables para tabla
	    $scope.query = {
	        filter: '',
	        order: 'NOTAPREQUIRURGICA.T_FECHA_CMD',
	        limit: 5,
	        page: 1
	    };

	    $scope.showCrearNotaPreOpe=validarPerfil(0,false,"") ? true: false;

		function getHistoricoNota(){
			var url="listaNotasQuirofano/"+$localStorage.paciente.idexpediente;
		    peticiones.getDatos(url)
	        .then(function(data) {
	        	if(data.quirofanoNotasVarias.length>0)
	        		self.historicoQuirofano=data.quirofanoNotasVarias;
	        })
	        .catch(function(err) {
	          console.log(err);
	        });
	    };

	    $scope.crearNota=function(){
	    	quirofano.setIdNota(0);
       		quirofano.setTipoNota("PreOperatoria");
	    	$state.go("notaPreOperatoria");
	    };

	    //Estatus 0=CANCELADA  1=PENDIENTE 2=FINALIZADA 3=CREAR
	    $scope.setEstatus=function(estatus,registro,nota){
	    	var texto="";
	     	switch(estatus){
	     		case 0:
	     			if(nota==4)
	     				texto="CIRUGÍA CANCELADA";
	     			else
	  					texto="CANCELADA";
	  				return texto;
	  			case 1:
	  				texto="PENDIENTE";
	  				return texto;
	  			case 2:
	  				texto="FINALIZADA";
	  				return texto;
	  			default:
	  			//Antes de poder "Crear" se requiere validar si se tienen las notas o consentimientos necesarios
	  			  if(validarEstatus(nota,registro)){
	  			   	if(validarPerfil(nota,false,"")){
	  			  		texto="CREAR";
	  					return texto;
	  			  	}
	  			  }
	  				else
	  					return "";
	  			break;
	     	}
        };
       	function validarEstatus(nota,registro){
 			 	switch(nota){
		     		case 0: //NOTA PREOPERATORIA
		  				return true;
		  			case 1: //NOTA PREANESTESICA
		  			    if(registro.NOTAPREQUIRURGICA.N_ANESTESIA==1){//1=SI REQUIERE ANESTESIA, 0=NO REQUIERE ANESTESIA
			  				if(registro.NOTAPREQUIRURGICA.NF_STATUS==2)//Para poder realizar una nota PreAnéstesica se requiere de nota PreOperatoria y que  va requerir anestésia
			  					return true;
			  				else
			  				  return false;
		  				}else{
		  					return false;
		  				}
		  			case 2: //NOTA POSTOPERATORIA
		  				if(registro.CIRUGIASEGURA.NF_STATUS==2)//Para poder realizar una nota PostOperatoria se requiere de Cirugía Segura FINALIZADA
		  					return true;
		  				else
		  					return false;
		  			case 3: //NOTA POST ANESTESICA
		  				if(registro.CIRUGIASEGURA.NF_STATUS==2)//Para poder realizar una nota PostAnéstesica se requiere de Cirugía Segura FINALIZADA
		  					if(registro.NOTAPOSTQUIRURGICA.NF_STATUS==2)//debe estar finalizada la nota postquirurgíca
		  						return true;
		  					else
		  						return false;
		  				else
		  					return false;
		  			case 4: //CIRUGÍA SEGURA   0=no existe consentimiento
		  				if(registro.NOTAPREQUIRURGICA.NF_CONSENTIMIENTO!=0){//Si tengo el consentimiento de nota PreOperatoria
		  					if(registro.NOTAPREQUIRURGICA.N_ANESTESIA==1){//Si requiere anestesiologo se valida consentimiento de PreOperatoria y PreAnéstesica, pero el PreOperatorio ya se valido , solo valido Consentimiento de PreAnéstesico
			  					if(registro.NOTAPREANESTESICA.NF_CONSENTIMIENTO!=0)//Si tengo el consentimiento  de PreAnéstesica
			  						return true;
			  					else
			  						return false;
				  			}
				  			else
				  				return true;
		  				}
		  				else
		  					return false;

		     	}
        };
        function validarPerfil(nota,band,registro){
        	switch(nota){
	     		case 0: //NOTA PREOPERATORIA
	  					return ($scope.perfil.clave=="MCIRUJANO") ? true:false;
	  			case 1: //NOTA PREANESTESICA
	  			   		return ($scope.perfil.clave=="MANESTESI") ? true:false;
	  			case 2: //NOTA POSTOPERATORIA
	  					return ($scope.perfil.clave=="MCIRUJANO") ? true:false;
	  			case 3: //NOTA POST ANESTESICA
	  					return ($scope.perfil.clave=="MANESTESI") ? true:false;
	  			case 4: //CIRUGÍA SEGURA
	  					if(band){//cuando se trata de cirujano o un anestésica	
	  					    if($scope.perfil.clave=="MCIRUJANO" || $scope.perfil.clave=="MANESTESI" ){	//se requiere la firma de enfermera circulante para que firme un cirujano o un anestesiolo	
								if(registro.CIRUGIASEGURA.C_FIRMA_CIRCULANTE=="" || registro.CIRUGIASEGURA.C_FIRMA_CIRCULANTE==undefined)
		  					        return false;
		  					    else
		  					    	return true;
		  					}else
		  						return true;
	  					}
	  					else
	  					 return ($scope.perfil.clave=="ECIRCULAN") ? true:false;
	     	}
        };

        //Notas 0=NOTAPREQUIRURGICA  1=NOTAPREANESTESICA 2=NOTAPOSTQUIRURGICA 3=NOTA_POSTANESTESICA
        $scope.clickNota=function(registro,tipoNota,estatus){
        	quirofano.setNotaActual(registro);
        	if(validarPermiso(estatus,tipoNota,registro)){
            	switch(tipoNota){
    	     		case 0: //NOTA PREOPERATORIA
                		quirofano.setTipoNota("PreOperatoria");
    	     			quirofano.setIdNota(registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA);
    	  				$state.go("notaPreOperatoria");
    	  				break;
    	  			case 1: //NOTA PREANESTESICA
    	  				$state.go("notaPreAnestesica");
    	  				break;
    	  			case 2: //NOTA POSTOPERATORIA
    		            quirofano.setIdNota(0);
    		            quirofano.setTipoNota("PostOperatoria");
    			  		$state.go("notaPostOperatoria");
    	  				break;
    	  			case 3: //NOTA POST ANESTESICA
          				$state.go("notaPostAnestesica");
    	  				break;
    	  			case 4: //CIRUGÍA SEGURA
    	  				$state.go("cirugiaSegura");
    	  				break;
    	     	}
    	     }
        };

        function validarPermiso(estatus,tipoNota,registro){
          	if(estatus!=1){//NO ES UNA NOTA PENDIENTE (NO SE VALIDAN PERMISOS YA QUE ESTARÁ CANCELADA, O FINALIZADA)
        		return true;
        	}else{// SI ESTA PENDIENTE SE VALIDAN PERMISOS YA QUE NO TODOS PODRÁN VISUALIZAR LA NOTA , ÚNICAMENTE VER EN QUE ESTATUS SE ENCUENTRA
        		 if(validarPerfil(tipoNota,true,registro))
        		 	return true;
        		 else
        		 	return false;
        	}
        };
        function download(registro,tipo,ev,url){
 		   	var url=(tipo==0) ? "download/consentimientoCirugia/"+registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA:"download/consentimientoAnestesia/"+registro.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA;
 		   	url += "?access_token=" + $localStorage.userSecurityInformation.token.value;
			 peticiones.getDatos(url)
		        .then(function(data) {
		          	$scope.image=data;
		        	$scope.ruta=baseURL+url;
		        	$mdDialog.show({
                    controller:DialogControllerDownload,
                    templateUrl: '/application/views/quirofano/descargar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                     locals:{
                     	 ev:ev,
                     	 registro: registro,
                     	 tipo:tipo,
                   		 image: $scope.image,
                   		 ruta:$scope.ruta,
                   		 url:url
                	 }
                  })
                  .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                  }, function() {
                    $scope.status = 'You cancelled the dialog.';
                  });
		        })
		        .catch(function(err) {
		          console.log(err);
		          mensajes.alerta("ALERTA","NO EXISTE ARCHIVO","OK");
		        });

 		};
 		 function downloadPDF(registro,tipo,ev,url){
		        	$mdDialog.show({
                    controller:DialogControllerDownload,
                    templateUrl: '/application/views/quirofano/descargar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                     locals:{
                     	 ev:ev,
                     	 registro: registro,
                     	 tipo:tipo,
                   		 image: "",
                   		 ruta:"",
                   		 url:url
                	 }
                  })
                  .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                  }, function() {
                    $scope.status = 'You cancelled the dialog.';
                  });
		      
		      

 		};
 		function load(registro,tipo,ev){
 		  	$mdDialog.show({
                    controller:DialogController,
                    templateUrl: '/application/views/quirofano/subirarchivo.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                     locals:{
                     	 modificar:false,
                   		 registro: registro,
                   		 tipo: tipo
                	 }
                  })
                  .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                  }, function() {
                    $scope.status = 'You cancelled the dialog.';
                  });

                getHistoricoNota();
 		};

        $scope.clickConsentimiento=function(tipo,registro,ev){
        	$scope.tipo=tipo;
        	$scope.registro=registro;
           	// tipo 0=PREQUIRURGICA  1=PREANÉSTESICA
	          var url=(tipo==0) ? "download/consentimientoCirugia/"+registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA:"download/consentimientoAnestesia/"+registro.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA;
	        	if(tipo==0){ //PREQUIRÚRGICA
	        		if($scope.registro.NOTAPREQUIRURGICA.NF_CONSENTIMIENTO!==0){//Tienen consentimiento
	         			 if($scope.registro.NOTAPREQUIRURGICA.C_ARCHIVO.indexOf("|image") > -1){
				 		    	download($scope.registro,tipo,"");
				 		    }else{
						           	// peticiones.getMethodPDF(url)
						            //  .success(function(data){
						            //       var file = new Blob([data], {type: 'application/pdf'});
						            //       var fileURL = URL.createObjectURL(file);
						            //       reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
						            //   }).error(function(data){console.log(fileURL);
						            //       console.log(data);
						            //   });
								 downloadPDF($scope.registro,tipo,ev,url);
				 		    }
	        		}
	    			else{//No tiene Consentimiento
	    				if(validarPerfil(tipo,false,registro))
	    					if(registro.NOTAPREQUIRURGICA.NF_STATUS==2)
	    						load($scope.registro,tipo,ev);
	    			}

	        	}else{//PREANÉSTESICA
	        		if($scope.registro.NOTAPREANESTESICA.NF_CONSENTIMIENTO!==0){//Tienen consentimiento
				 		    if($scope.registro.NOTAPREANESTESICA.C_ARCHIVO.indexOf("|image") > -1){
				 		    	download($scope.registro,tipo);
				 		    }else{
									// var url=(tipo==0) ? "download/consentimientoCirugia/"+registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA:"download/consentimientoAnestesia/"+registro.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA;
						           	peticiones.getMethodPDF(url)
						             .success(function(data){
						                  var file = new Blob([data], {type: 'application/pdf'});
						                  var fileURL = URL.createObjectURL(file);
						                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
						              }).error(function(data){console.log(fileURL);
						                  console.log(data);
						              });
				 		    }
	        		}
	    			else{//No tiene Consentimiento
	    				if(validarPerfil(tipo,false,registro))
	    					if(registro.NOTAPREANESTESICA.NF_STATUS==2)
	    						load($scope.registro,tipo,ev);
	    			}
	        	}
    	

        };

        $scope.getEstatusConsentimiento=function(registro,tipoNota){
          	//  0=PREQUIRURGICA   1=PREANÉSTESICA
        	if(tipoNota==0){//PREQUIRURGICA
        		if(registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA!==0)
        		return (registro.NOTAPREQUIRURGICA.NF_CONSENTIMIENTO==0) ? "SIN CONSENTIMIENTO":"CON CONSENTIMIENTO";
        	}else{//PREANÉSTESICA
        		if(registro.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA!==0)
        			return (registro.NOTAPREANESTESICA.NF_CONSENTIMIENTO==0) ? "SIN CONSENTIMIENTO":"CON CONSENTIMIENTO";
        	}
        };

        function DialogControllerDownload($scope, $mdDialog,image,ruta,registro,tipo,ev,url) {
	        	$scope.registro=registro;
	        	$scope.tipo=tipo;
	        	$scope.cancelar = function() {
			           $mdDialog.hide();
			    };
	        	$scope.image=image;
	        	$scope.ruta=ruta;
	        	
	        	$scope.modificarArchivo=function(){
	        		if(validarPerfil(tipo,false,"")){
						if($scope.registro.CIRUGIASEGURA.NF_STATUS==2){	
		        			 mensajes.alerta("ALERTA","LA NOTA YA FUE FINALIZADA POR LO QUE NO SE PUDE MODIFICAR EL CONSENTIMIENTO","OK");
		        		}else{
		        			$mdDialog.show({
		                    controller: DialogController,
		                    templateUrl: '/application/views/quirofano/subirarchivo.html',
		                    parent: angular.element(document.body),
		                    targetEvent: ev,
		                    clickOutsideToClose:true,
		                    locals:{
		                    	 modificar:true,
		                   		 registro: $scope.registro,
		                   		 tipo: $scope.tipo
		                	}
			                }).then(function(answer) {
			                    $scope.status = 'You said the information was "' + answer + '".';
			                  }, function() {
			                    $scope.status = 'You cancelled the dialog.';
			                  });
			                getHistoricoNota();

		        		}
	        	    }else{
	        	    	 mensajes.alerta("ALERTA","NO TIENE PERMISO","OK");
	        	    }
	 		    };

	 		    $scope.verPdf=function(){
	 		    		peticiones.getMethodPDF(url)
			             .success(function(data){
			                  var file = new Blob([data], {type: 'application/pdf'});
			                  var fileURL = URL.createObjectURL(file);
			                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
			              }).error(function(data){console.log(fileURL);
			                  console.log(data);
			              });
	 		    };
 		};
 		
 		function DialogController($scope, $mdDialog, registro, tipo,$timeout,modificar) { 
	        var registro=registro;
	        var tipo=tipo;
	        var modificar=modificar;
            $scope.cancelar = function() {
                     $mdDialog.hide({result: false});
            };

            $scope.guardar = function(bandera) {
                  $mdDialog.hide({result: bandera});
            };

            $scope.actualizardatos = function(image, formulario){
                $scope.isImage = image;
                $scope.errorMsg = false;
                $scope.picFile = null;
                // formulario.$setPristine();
                  // formulario.$setUntouched();
                    // formulario.fileConsentimiento.$error.maxSize = false;
            };

            $scope.eliminarArchivo = function(formulario){
                $scope.picFile = null;
                // formulario.fileConsentimiento.$error.maxSize = false;
                formulario.$setPristine();
                    formulario.$setUntouched();
            };

            $scope.uploadPic = function(file) {
              if(modificar){
                 $scope.subir(tipo,registro,file);
                 modificar=false;
              }else{
                var url="consentimiento/";
                if(tipo==0){
                  url+="cirugia/"+registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA;
                }else{
                  url+="anestesia/"+registro.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA;
                }
                peticiones.getDatos(url).then(function(data) {
                 
                    $scope.subir(tipo,registro,file);
                  })
                  .catch(function(err) {
                    console.log(err);
                  });
              }

            };

	        $scope.subir=function(tipo,registro,file){
              var url=(tipo==0) ? "upload/consentimientoCirugia/"+registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA:"upload/consentimientoAnestesia/"+registro.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA;
              peticiones.uploadArchivo(url, file)
              .then(function (response) {
                    $timeout(function () {
                      file.result = response.data;
                      mensajes.alerta("EXITO","SE SUBIO CORRECTAMENTE","OK");
                      getHistoricoNota();
                      $state.go("quirofano");
                    });
              }, function (response) {
                if (response.status > 0)
                  $scope.errorMsg = response.status + ': ' + response.data;
                console.log($scope.errorMsg);
              }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
              });
	        }  
        };

	  }]);
})();




