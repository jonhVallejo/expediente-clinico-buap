(function(){
'use strict';
angular.module('expediente')
    .controller('notaPreOperatoriaCtrl',['$scope','$q','$localStorage','usuario','$state','peticiones','$timeout','catalogos','mensajes','blockUI','medicos','$filter','pacientes','$mdDialog','quirofano','$window','reportes',
      function ($scope, $q, $localStorage,usuario,$state,peticiones,$timeout,catalogos,mensajes,blockUI,medicos,$filter,pacientes,$mdDialog,quirofano,$window,reportes) {
      var self                    = this;
      this.idSeleccion            = 1;
      self.firmarDatos            = false;


      $scope.datosSecundarios     = [];
      $scope.catCIE10             = [];
      $scope.duracionCirugia      = new Date();
      
      self.listMedicos            = [];
      self.cambiarDiagnostico     = false;
      self.disabledMedico         = true;
      self.datosNota              = {};
      self.tiposMedico = [{NP_TIPO:0, C_DESCRIPCION: "CIRUJANO"},
                          {NP_TIPO:1, C_DESCRIPCION: "AYUDANTE"},
                          {NP_TIPO:2, C_DESCRIPCION: "INSTRUMENTISTA"},
                          {NP_TIPO:3, C_DESCRIPCION: "ANESTESIOLOGO"},
                          {NP_TIPO:4, C_DESCRIPCION: "CIRCULANTE"}];

      self.tipoContentimiento = [ {id: 1,  description: "HOSPITALIZACIÓN"},
                                  {id: 2, description: "CIRUGÍA"},
                                  {id: 3,  description: "ANESTESIA"},
                                  {id: 4, description: "URGENCIAS"}];
      self.notasPreOperatorias = {};
      self.datosNota.CIRUJANO                    = [];
      self.datosNota.AYUDANTE                    = [];
      self.datosNota.INSTRUMENTISTA              = [];
      self.datosNota.ANESTESIOLOGO               = [];
      self.datosNota.CIRCULANTE                  = [];    
      self.notaPreOperatoriaEdit                 = false;
      self.dataFinal                             = {datos:{}};
      self.dataFinal.datos.N_CONTEO_GASES              = 1;
      self.dataFinal.datos.N_INCIDENTES                = 0;
      self.C_INCIDENTES_DISABLED                 = true;
      self.dataFinal.datos.N_TRANSFUSIONES             = 0;
      self.dataFinal.datos.C_TRANSFUSIONES = "";
      self.N_TRANSFUSIONES_DISABLED              = true;
      self.dataFinal.datos.N_RESULTDS_TRANSQUIRURGICOS = 0;
      self.RESULTDS_TRANSQUIRURGICOS_DISABLED    = true;
      self.c_biopsia_DISABLED                    = true;
      self.dataFinal.datos.N_BIOPSIA                   = 0;
      self.NP_NOTA_PREQUIRURGICA                 = 0;
      self.dataFinal.datos.CIRUJANO                    = [];
      self.dataFinal.datos.AYUDANTE                    = [];
      self.dataFinal.datos.INSTRUMENTISTA              = [];
      self.dataFinal.datos.ANESTESIOLOGO               = [];
      self.dataFinal.datos.CIRCULANTE                  = [];
      self.datosServer                           = {datos:{}};
      self.plan                                  = "";
      self.riesgo                                = "";
      self.cuidados                              = "";
      self.regresarCancelar = "CANCELAR";
      self.editarNotaPost = false;
      self.contadorMedicos = 0;
      self.isEditable = false;
      self.crearNuevaNota = true;
      self.isEditableNotaPost = false;
      self.numColsapan = 1;
      self.NOTA_FIRMADA = false;
      var mNP_NOTA_MEDICA = undefined;
      var idEspecialidad = undefined;

      self.fechaActual = new Date();
      self.fechaMinima = new Date(self.fechaActual.getFullYear(), self.fechaActual.getMonth(), self.fechaActual.getDate() )
      $scope.delNotaPreQuirurgica = "notaPrequirurgica/";
      $scope.delNotaPostQuirurgica = "notaPostquirurgica/";
      var urlNotaIngreso = "consentimiento/notasIngreso/";
      var urlNotaCirugia = "notasPrequirurgicas/";
      var urlCie09 = 'catalogo/cie9/';

      $scope.anestesia=false;

      var mNF_PACIENTE = $localStorage.paciente.expediente.substring(0, $localStorage.paciente.expediente.length - 4);
      var sx = ($localStorage.paciente.genero == "MASCULINO") ? "M" : "F";
      //Datos generales del paciente
      $scope.datos ={
                        nombre: $localStorage.paciente.nombrePaciente,
                        H: $localStorage.paciente.idPaciente,
                        id : usuario.usuario.NP_EXPEDIENTE,
                        exp: $localStorage.paciente.expediente,
                        edad: pacientes.calculaEdad($localStorage.paciente.f_nacimiento),
                        fNac: $localStorage.paciente.f_nacimiento,
                        genero: $localStorage.paciente.genero
        };
      $scope.imprimirNota=false;

    function getEspecialidad(){
       medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
       .success(function(data){
         if(data === ''){
           mensajes.alerta('ERROR','ERROR INTERNO DEL SISTEMA INTENTE NUEVAMENTE','ACEPTAR');
         }
         else{
           idEspecialidad = data.ESPECIALIDADES.NF_ESPECIALIDAD;
         }
       })
       .error(function(data){

         mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR', 3000);
       });

   };
    getEspecialidad();

      /**
       * [datosCie10 Funsión que se encarga de retornar todos los datos del CIE10]
       * @return {[type]} [N/D]
       */
      var datosCie10 = function (){
          catalogos.getCatCIE10().success(function(data){
              $scope.catCIE10 = data;
          })
          .error(function(data){
              mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
          });
      }

      /**
       * [obtenerCie09 Petición al servidor para obtener el catalogo CIE09]
       * @param  {[type]} url [url del servicio]
       * @return {[type]}     [description]
       */
      var obtenerCie09 = function(url){
            peticiones.getDatos(url)
              .then(function(data)
                {
                  self.catalogoCie09List = data;
                }).catch(function(data)
                {
                  error = data;
                });
      };

        /**
         * [cargarServiciosMedico Funsión que se encarga de dar formato para el nombre de los doctores]
         * @param  {[type]} data [lista de doctores]
         * @return {[type]}      [description]
         */
         var cargarServiciosMedico =  function (data){
            return data.map(function (serv){
              var ap_paterno = (serv.C_PRIMER_APELLIDO == undefined) ? "" : serv.C_PRIMER_APELLIDO;
              var ap_materno = (serv.C_SEGUNDO_APELLIDO == undefined) ? "" : serv.C_SEGUNDO_APELLIDO;
              var nombre = (serv.C_NOMBRE== undefined) ? "": serv.C_NOMBRE;
              return {
                value : (ap_paterno +" "+ ap_materno +" "+ nombre).toUpperCase(),
                display: (ap_paterno +" "+ ap_materno +" "+ nombre).toUpperCase(),
                nombre: serv.C_NOMBRE,
                id  : serv.N_EXPEDIENTE,
                especialidad: serv.ESPECIALIDADES.CF_ESPECIALIDAD,
                idEspecialidad: serv.ESPECIALIDADES.NF_ESPECIALIDAD,
                C_CEDULA: serv.C_CEDULA
              };
            });
        };


      /**
       * [getAllMedicos Funsión que se encarga de obtener todos los medicos]
       * @return {[type]} [description]
       */
      var getAllMedicos = function(){
         var url = "personal/medicos";
        peticiones.getDatosStatus(url)
        .then(function(data){
          blockUI.stop();
          if(data.status==200){
              self.listMedicos = cargarServiciosMedico(data.data);
          }else{
            if(data.status==204){
              console.log("No hay datos");
            }
          }
          
        }).catch(function(data){
          console.log(data);
          blockUI.stop();
        });
      }
 


    /**
     * [getEditNotaPreOperatoria Se obtiene todo el detalle de una nota pre operatoria en el apartado "notas pre operatorias"]
     * @param  {[type]} NO_NOTA [id de la nota]
     * @return {[type]}         [No retorna un valor]
     */
    var getEditNotaPreOperatoria = function(NO_NOTA){
        var url = "notaPrequirurgica/id/" + NO_NOTA;
        peticiones.getDatos(url).then(function(data)
            {
                if(quirofano.getTipoNota() == "PreOperatoria"){
                   setNotaPreOperatoria(data);
                 }else{
                  crearNotaPostOperatoria(data);
                 }

                blockUI.stop();
            }).catch(function(data)
            {
                blockUI.stop();

            });
    };

    var getNotasPostDetail = function(NP_NOTA_POSTQUIRURGICA, seccion){
      var url = "notaPostquirurgica/id/" + NP_NOTA_POSTQUIRURGICA;
      //var url = "notaPostquirurgica/id/" + 2401;
      //var url = "notaPostquirurgica/" + 10860;
      peticiones.getDatos(url).then(function(data)
          {
              blockUI.stop();
              setEditNotasPost(data, seccion);
              self.editarNotaPost = (data.NF_ESTATUS == 2) ? true : false;
              self.isEditableNotaPost = (data.NF_ESTATUS == 2) ? true : false;
              getImprimir((data.NF_ESTATUS == 2) ? true : false);
              if(data.C_FIRMA){
                  self.mensajeNota = "NOTA FIRMADA";
              }else{
                self.mensajeNota = "NOTA SIN FIRMAR";
              }

          }).catch(function(err)
          {
              blockUI.stop();
              console.log(err);
          });
    };

    self.editarNotaPostOperatoria = function(NP_NOTA_POSTQUIRURGICA){
          getNotasPostDetail(NP_NOTA_POSTQUIRURGICA, 4);
          self.isNueNotaPos = false;
    };

    var setNotaPreOperatoria = function(data){
      //$scope.diagnosticoCie9
            self.fechaNotapreOperatoria        = new Date(data.FECHA_CIRUGIA.split("/")[2],data.FECHA_CIRUGIA.split("/")[1],data.FECHA_CIRUGIA.split("/")[0]);
            self.NP_DIAGNOSTICO_CIE10          = data.NF_CIE_10;
            self.plan                          = data.C_PLAN_QUIRURGICO;
            self.riesgo                        = data.C_RIESGO_QUIRURGICO;
            self.cuidados                      = data.C_PLAN_TERAPEUTICO;
            self.NP_NOTA_PREQUIRURGICA         = data.NP_NOTA_PREQUIRURGICA
            self.idSeleccion                   = 2;
            $scope.selectedIte                 = data.C_DESCRIPCION_CIE_10;
            $scope.selecItemCie9               = data.C_DESCRIPCION_CIE_9;
            self.notasPreOperatorias.NP_CIE_10 = data.NF_CIE_10;
            self.NP_DIAGNOSTICO_CIE09          = data.NF_CIE_9;
            $scope.diagnosticoCie9 = data.NF_CIE_9;
            self.notasPreOperatorias.NP_NOTA_MEDICA = data.NF_NOTA_MEDICA;
            self.anestesia = (data.N_ANESTESIA == 1) ? true : false;
            self.NOTA_FIRMADA = (data.N_ESTATUS == 2) ? true : false;
            self.regresarCancelar = (data.N_ESTATUS == 2) ? "REGRESAR" : "CANCELAR";
              self.isEditable = (data.N_ESTATUS == 1) ? true : false;
              self.notaPreOperatoriaEdit = (data.N_ESTATUS == 1) ? true : false;
            if(data.N_ESTATUS == 1){
                datosCie10();
                getCatalogoCIE09(sx);
            }
            getImprimir((data.N_ESTATUS == 2) ? true : false);
    };

      /**
       * [getCatalogoCIE09 Se verifica el sexo del paciente para hacer la solicitud
       *                   adecuada por tipo de paciente]
       * @param  {[type]} mSexo [Sexo del paciente]
       * @return {[type]}       [description]
       */
      var getCatalogoCIE09 = function(mSexo){
         if(self.catalogoCie09List == undefined){
            obtenerCie09(urlCie09 + mSexo);
          }else{
            if(sexo != mSexo){
              obtenerCie09(urlCie09 + mSexo);
            }
          }
          return self.catalogoCie09List;
      };


    self.cambiarSolicitud = function(){
      var url = "notaPrequirurgica/" + self.tipoSolicitud + "/" + mNF_PACIENTE + "/" + idEspecialidad;
      //var url = "http://www.json-generator.com/api/json/get/ciJozfDWsy?indent=2";
        peticiones.getDatos(url)
            .then(function(data)
              {
                self.notasMedicasList = data;
              }).catch(function(data)
              {
                error = data;
              });
    }
    /**
     * OBSOLETA
     * [getNotaMedica Funsión qiue se encarga de obtener los datos d ela ultima nota medica
     *                          para crear posteriormente una nota pre operatoria]
     * @param  {[type]} NP_EXPEDIENTE   [Expediente del paciente a consultar]
     * @return {[type]}                 [description]
     */
    var getNotaMedica = function(NP_EXPEDIENTE){
        var url = "notaPrequirurgica/" + NP_EXPEDIENTE;
        peticiones.getDatos(url).then(function(data)
            {
                blockUI.stop();
                self.notasPreOperatorias = data;

                $scope.selectedIte         = data.C_DESCRIPCION_CIE_10;
                self.notaPreOperatoriaEdit = true;
                self.isEditable            = false;
            }).catch(function(err)
            {
                blockUI.stop();
                console.log(err);
            });
    };

     /**
       * [verificarTipo Funsión que se va autilizar para saber a que tipo de nota pertenece así como la acción a realizar]
       * @return {[type]} [description]
       */
    var verificarTipo = function(){
      if(quirofano.getIdNota() != undefined){
        if(quirofano.getTipoNota() == "PreOperatoria"){
            if( quirofano.getIdNota() == 0){
                datosCie10();
                getCatalogoCIE09(sx);

              }else{
                  self.idSeleccion = 1;
                  getEditNotaPreOperatoria(quirofano.getIdNota());
              }
        }else{
            if(quirofano.getTipoNota() == "PostOperatoria"){
              self.isNotaPostFirmada = (quirofano.getStatusNotaPost() == 2) ? true : false;
                          datosCie10();
                          getCatalogoCIE09(sx);
                          if(quirofano.getStatusNotaPost() == 1 || quirofano.getStatusNotaPost() == 2){
                              self.editarNotaPostOperatoria(quirofano.getNpNotaPost());
                              self.isNueNotaPos = false;
                          }else{
                              self.idSeleccion = 4 ;
                              getEditNotaPreOperatoria(quirofano.getIdNotaMedica());
                                     self.isNueNotaPos = true;
                                     self.regresarCancelar = "CANCELAR";
                                     self.mensajeNota = "CREAR NOTA";
                                    getAllMedicos();
                          }
            }
          }
        }else{
         $state.go($localStorage.stateAnterior);
        }
    }
    verificarTipo();

      /**
       * [fillNotaPreOperatoria Llenar datos de una nota pre operatoria]
       * @param  {[type]} firmarNota [Campo de tipo boobleano para saber si se va a firmar o es un guardado parcial]
       * @return {[type]}            [description]
       */
       var fillNotaPreOperatoria = function(firmarNota){
          self.datosServer.datos.FECHA_CIRUGIA         = $filter('date')(self.fechaNotapreOperatoria, 'dd/MM/yyyy');
          self.datosServer.datos.NF_CIE_10             = self.NP_DIAGNOSTICO_CIE10;
          self.datosServer.datos.NF_CIE_9              = (self.notaPreOperatoriaEdit && $scope.diagnosticoCie9 == undefined) ?  self.NP_DIAGNOSTICO_CIE09 : $scope.diagnosticoCie9;
          self.datosServer.datos.C_PLAN_QUIRURGICO     = self.plan;
          self.datosServer.datos.C_RIESGO_QUIRURGICO   = self.riesgo;
          self.datosServer.datos.C_PLAN_TERAPEUTICO    = self.cuidados;
          self.datosServer.datos.NF_NOTA_MEDICA        = self.notasPreOperatorias.NP_NOTA_MEDICA;
          self.datosServer.datos.NF_PACIENTE           = $localStorage.paciente.expediente.substring(0,$localStorage.paciente.expediente.length - 4);
          self.datosServer.datos.NF_MEDICO             = $localStorage.usuario.NP_EXPEDIENTE;
          self.datosServer.id                          = $localStorage.usuario.NP_EXPEDIENTE;
          self.datosServer.datos.N_TIPO_CIRUGIA             = (self.tipoSolicitud == "electiva") ? 1 : 2;
          self.datosServer.datos.NP_NOTA_PREQUIRURGICA = self.NP_NOTA_PREQUIRURGICA;
          self.datosServer.datos.N_ANESTESIA = (self.anestesia) ? 1 : 0;
          if(firmarNota){
              self.datosServer.keyPriv                     = $scope.keyPriv;
              self.datosServer.certificado                 = $scope.certificado;
          }else{
            if(self.datosServer.keyPriv){
              delete self.datosServer["keyPriv"];
            };
            if(self.datosServer.certificado){
              delete self.datosServer["certificado"];
            };
          };
        };

        /**
         * [limpiarDatosPreOperatorios Funsión para resetar los datos de la nota pre operatoria]
         * @param  {[type]} clearAll          [description]
         * @param  {[type]} limpiararrayNotas [description]
         * @return {[type]}                   [description]
         */
        var limpiarDatosPreOperatorios = function(clearAll,limpiararrayNotas){
           self.fechaNotapreOperatoria = undefined;
           self.NP_DIAGNOSTICO_CIE10   = 0;
           self.cambiarDiagnostico     = false;
           self.firmarDatos            = false;
           // self.showCrearNota = true;
           self.plan                   = "";
           self.riesgo                 = "";
           self.cuidados               = "";

           $scope.diagnosticoCie9      = undefined;
           $scope.keyPriv              = "";
           $scope.certificado          = "";
           $scope.selecItemCie9        = ' ';
           //$scope.selectedIte          =  self.notasPreOperatorias.C_DESCRIPCION_CIE_10;
           $scope.key = ' ';
           $scope.cer = ' ';
           if(limpiararrayNotas){
               //self.notasPreOperatorias.NOTAS_PREQUIRURGICAS.splice(0,self.notasPreOperatorias.NOTAS_PREQUIRURGICAS.length);
               self.notasPreOperatorias.NP_NOTA_MEDICA = 0;
               self.notasPreOperatorias.NP_CIE_10 = 0;
               self.notasPreOperatorias.C_DESCRIPCION_CIE_10 = " ";
           };

      };

      /**
       * [changeDiagnostico Funsión que se utiliza para cambiar el diagnostico de una nota, el que viene
       *                     en la nota medica]
       * @return {[type]} [description]
       */
      self.changeDiagnostico = function(){
          self.cambiarDiagnostico = true;
          $scope.selectedIte  = ' ';
      };

      /**
       * [selectedItemChangeDiagnostico1 Seleccionar diagnostico que se cambio (autocompletado)]
       * @param  {[type]} item [Dato a buscar (autocompletado)]
       * @return {[type]}      [description]
       */
       $scope.selectedItemChangeDiagnostico1 = function(item) {
        if(_.isObject(item)){
            $scope.diagnosticoPrincipal = item.NP_ID;
        }
      }

      /**
       * [querySearchDiagnostico1 Funsión que se utiliza para buscar el texto dentro del catalogo
       *                           para realizar el auto completado]
       * @param  {[type]} query [description]
       * @return {[type]}       [description]
       */
      $scope.querySearchDiagnostico1 = function(query){
      if(query.length>2){
          return _.filter($scope.catCIE10,function(e){
            return e.C_DESCRIPCION.toUpperCase().indexOf(query.toUpperCase())>=0;
          });
        }
      };


      /**
       * [selectedItemChangeDiagnostico2 Función que sirve para cambiar el NP_ID de el CIE9 ]
       * @param  {[type]} item [objeto de tipo CIE9]
       * @return {[type]}      [description]
       */
      $scope.selectedItemChangeDiagnostico2 = function(item) {
        if(item != undefined){
          if(_.isObject(item)){
            $scope.diagnosticoCie9 = item.NP_ID;
          }
        }else{
          $scope.diagnosticoCie9 = undefined;
        }
      };


      /**
       * [querySearchDiagnostico2 Funsión que se encarga de realizar la busqueda
       *                          para el CIE9]
       * @param  {[type]} query [Cadeba de texto a buscar en el catalogo]
       * @return {[type]}       [description]
       */
      $scope.querySearchDiagnostico2 = function(query){
        if(query.length>5){
              if(self.catalogoCie09List){
                return _.filter(self.catalogoCie09List.CATALOGO_CIE_9, function(e){
                    return e.C_DESCRIPCION.toUpperCase().indexOf(query.toUpperCase())>=0;
                 });
              }
        }
      };


      /**
       * [verificarParaGuardarPreOpe Funsión que se encarga de verificar los requisitos minimos necesarios
       *                              para guardar una nota, o en su caso para firmarla]
       * @return {[type]} [description]
       */
      var verificarParaGuardarPreOpe = function (){
        var bandera = true;
          if(self.fechaNotapreOperatoria == undefined){
            bandera = false;
          }
          if($scope.diagnosticoCie9 == undefined){
              bandera = false;
            }


          if(self.cambiarDiagnostico){
              if($scope.diagnosticoPrincipal == undefined){
                bandera = false;
              }else{
                self.NP_DIAGNOSTICO_CIE10 = $scope.diagnosticoPrincipal;
              }
            }else{
              self.NP_DIAGNOSTICO_CIE10 = self.notasPreOperatorias.NP_CIE_10;
          }
         if (!bandera){
              mensajes.alerta("ERROR","LOS DATOS MINIMOS REQUERIDOS PARA GUARDAR LA NOTA PRE OPERATORIA SON:  FECHA DE LA CIRUGÍA, DIAGNÓSTICO (CIE10) Y PROCEDIMIENTO QUIRÚRGICO (CIE9).  PARA FIRMAR UNA NOTA TODOS LOS CAMPOS SON OBLIGATORIOS" ,"OK");
            }
        return bandera;
      };

      /**
       * [verificarDatosGuardar Datos obligatorios para guardar poder firmar una nota Pre operatoria]
       * @return {[type]} [description]
       */
       var verificarDatosGuardar = function(){
          var bandera = true;
          if(self.plan == ""){
            bandera = false;
          }
          if(self.riesgo == ""){
            bandera = false;
          }
          if(self.cuidados == ""){
            bandera = false
          }
         // console.debug(quirofano.getNotaActual().NOTAPREQUIRURGICA);
     
          return bandera;
      };



       self.editarNotaPreOperatoria = function(NP_NOTA_MEDICA,C_DESCRIPCION_CIE_10,NP_CIE10,edit){

          mNP_NOTA_MEDICA            = NP_NOTA_MEDICA;
          self.notasPreOperatorias = {};
          //self.notasPreOperatorias.C_DESCRIPCION_CIE_10 = C_DESCRIPCION_CIE_10;
          //self.notasPreOperatorias   = mNotaMedica;
          self.notasPreOperatorias.NP_CIE_10 = NP_CIE10;
          self.notasPreOperatorias.NP_NOTA_MEDICA = NP_NOTA_MEDICA;
          $scope.selectedIte         = C_DESCRIPCION_CIE_10;
          self.notaPreOperatoriaEdit = true;
          self.isEditable            = false;
          self.regresarCancelar = "CANCELAR";
          self.idSeleccion = 2;

      };

      /**
       * [cancelarNota Funsión para cancelar una nota pre operatoria]
       * @param  {[type]} id       [id d ela sección en la que esta]
       * @param  {[type]} form     [formulario de datos]
       * @param  {[type]} clearAll [bandera para limpiar datos]
       * @return {[type]}          [description]
       */
       self.cancelarNota = function(id,form,clearAll){
          self.idSeleccion = id;
          self.firmarDatos = false;
          self.editarNotaPost = false;
          self.regresarCancelar = "CANCELAR";
          self.contadorMedicos = 0;
          self.isEditable = false;

          if(id == 1){
              if(form){
                limpiarDatosPreOperatorios(false);
                form.$setPristine();
                form.$setUntouched();
                if(self.NOTA_FIRMADA == true){
                  $state.go($localStorage.stateAnterior);
                }
              }
          }else{
             if(id == 3){
                 $state.go($localStorage.stateAnterior);
                    limpiarNotaPosOperatoria(form);
                }
          }
      };


      /**
       * [showDialogCIE_09 Funsión que se encarga de mostrr el Show dialog para buscar el CIE9 por categoria]
       * @param  {[type]} ev [description]
       * @return {[type]}    [description]
       */
      self.showDialogCIE_09 = function(ev) {
         $mdDialog.show({
                      controller: DialogController,
                      controllerAs: 'vm',
                      templateUrl: '/application/views/CIE_09.html',
                      parent: angular.element(document.body),
                      targetEvent: ev,
                      clickOutsideToClose: true,
                      locals:{
                          list: self.catalogoCie09List
                      }
                  })
                  .then(function(element) {
                     $scope.selecItemCie9 = element.element.C_DESCRIPCION;
                     $scope.diagnosticoCie9 = element.element.NP_ID;
                  }, function() {
                      $scope.status = 'You cancelled the dialog.';
                  });
        };

      /**
       * [DialogController Lanza el show dialog que permite hacer busquedas de el CIE9 por categoria]
       * @param {[type]} $scope    [description]
       * @param {[type]} $mdDialog [description]
       * @param {[type]} list      [parametro que se le pasa al show dialog]
       */
      function DialogController($scope, $mdDialog, list) {
          $scope.lista = list;
          $scope.enableSeleccionar = true;
          $scope.buscarCie09 = function(tipo, index){
              var id = undefined;
              $scope.enableSeleccionar = true;
              if(tipo ==1){
                  id = $scope.lista.CATALOGO_CAPITULOS[$scope.CapituloId].N_CAPITULO;
                  $scope.CIE_09_seccion = _.where($scope.lista.CATALOGO_SECCIONES, {N_CAPITULO: id});
                  $scope.SeccionId = -1;
                  $scope.categoriaId = -1;
                }else{
                    if(tipo == 2){
                        id = $scope.lista.CATALOGO_SECCIONES[$scope.SeccionId].N_SECCION;
                        $scope.CIE_09_categoria = _.where($scope.lista.CATALOGO_CATEGORIAS, {N_SECCION: id});
                        $scope.categoriaId = -1;
                    }else{
                      if(tipo == 3){
                         id = $scope.lista.CATALOGO_CATEGORIAS[$scope.categoriaId].C_CATEGORIA;
                        $scope.CIE_09_List = _.where($scope.lista.CATALOGO_CIE_9, {C_CATEGORIA: id});
                      }else{
                          $scope.enableSeleccionar = false;
                      }
                    }
                }
          }

          $scope.hide = function() {
            $mdDialog.hide();
          };
          $scope.dialogCancel = function() {
            $mdDialog.cancel();
          };
          $scope.dialogGuardar = function(index) {
            $mdDialog.hide({element: $scope.CIE_09_List[index]});
          };
        }




/**
 * NOTAS POS OPERATORIAS
 */


      /**
       * [limpiarNotaPosOperatoria Función que se utiliza para limpiar todos los datos de el objeto
       *                             (self.datosNota)]
       * @return {[type]} [description]
       */
      var limpiarNotaPosOperatoria = function(form){
          $scope.diagnosticoCie9                     = undefined;
          $scope.diagnosticoPrincipal                = undefined;

          self.dataFinal.datos.NF_CIE_10                   = 0;
          self.dataFinal.datos.NF_CIRUGIA_PLANEADA         = 0;
          self.dataFinal.datos.C_TECNICA_QUIRURGICA        = "";
          self.dataFinal.datos.C_HALLASGOS                 = "";
          self.dataFinal.datos.N_CONTEO_GASES              = 0;
          self.dataFinal.datos.N_INCIDENTES                = 0;
          self.dataFinal.datos.C_INCIDENTES                = "";
          self.dataFinal.datos.N_SANGRADO                  = 0;
          self.dataFinal.datos.N_TRANSFUSIONES             = 0;
          self.dataFinal.datos.N_RESULTDS_TRANSQUIRURGICOS = 0;
          self.dataFinal.datos.C_RESULTDS_TRANSQUIRURGICOS = "";
          self.dataFinal.datos.C_ESTADO_POSTQUIRURGICO     = "";
          self.dataFinal.datos.C_PLAN_POSTQUIRURGICO       = "";
          self.dataFinal.datos.N_BIOPSIA                   = 0;
          self.dataFinal.datos.C_BIOPSIA                   = "";
          self.dataFinal.datos.C_FIRMA                   = false;

          self.dataFinal.datos.CIRUJANO.splice(0, self.dataFinal.datos.CIRUJANO.length);
          self.dataFinal.datos.AYUDANTE.splice(0, self.dataFinal.datos.AYUDANTE.length);
          self.dataFinal.datos.INSTRUMENTISTA.splice(0, self.dataFinal.datos.INSTRUMENTISTA.length);
          self.dataFinal.datos.ANESTESIOLOGO.splice(0, self.dataFinal.datos.ANESTESIOLOGO.length);
          self.dataFinal.datos.CIRCULANTE.splice(0, self.dataFinal.datos.CIRCULANTE.length);


          self.datosNota.CIRUJANO.splice(0, self.datosNota.CIRUJANO.length);
          self.datosNota.AYUDANTE.splice(0, self.datosNota.AYUDANTE.length);
          self.datosNota.INSTRUMENTISTA.splice(0, self.datosNota.INSTRUMENTISTA.length);
          self.datosNota.ANESTESIOLOGO.splice(0, self.datosNota.ANESTESIOLOGO.length);
          self.datosNota.CIRCULANTE.splice(0, self.datosNota.CIRCULANTE.length);

           $scope.keyPriv              = "";
           $scope.certificado          = "";

          form.$setPristine();
          form.$setUntouched();

      };

        /**
       * Funsiones compartidas entre las notas pre y post operatorias
       */

            /**
       * [crearNotaPostOperatoria Función que se utiliza para llenar los valores por dafault que se necesitan al crear una nota
       *                           post opetatoria]
       * @param  {[type]} nota [Objeto de tipo (nota preoperatoria)]
       * @return {[type]}      [description]
       */
       var crearNotaPostOperatoria = function(nota){

          self.cie09Programado                      = nota.C_DESCRIPCION_CIE_9;
          self.selecItemCie9                        = nota.C_DESCRIPCION_CIE_9;
          self.selectedItemCIE10                    = nota.C_DESCRIPCION_CIE_10;
          self.dataFinal.datos.NF_CIE_10            = nota.NF_CIE_10;
          self.dataFinal.datos.NF_CIRUGIA_PLANEADA  = nota.NF_CIE_9;
          self.dataFinal.datos.NF_CIRUGIA_REALIZADA = nota.NF_CIE_9;
          self.dataFinal.datos.NF_PACIENTE          = mNF_PACIENTE;
          self.dataFinal.datos.NF_MEDICO            = $localStorage.usuario.NP_EXPEDIENTE;
          self.dataFinal.datos.NF_NOTA_PRE          = nota.NP_NOTA_PREQUIRURGICA;
          self.dataFinal.datos.NP_NOTA_POSTQUIRURGICA = 0;
          self.dataFinal.id = $localStorage.usuario.NP_EXPEDIENTE;
          self.isEditableNotaPost = false;
          if(quirofano.getNotaActual().NOTAPREQUIRURGICA.N_ANESTESIA!=1){
              $scope.anestesia=true;
          }
      };


      var setEditNotasPost = function(mDataArray,seccion){
          if(mDataArray.T_DURACION!=undefined){
            //Validar si se requiere anestesiologo
            if(quirofano.getNotaActual().NOTAPREQUIRURGICA.N_ANESTESIA!=1){
              $scope.anestesia=true;
              $scope.hora=parseInt(mDataArray.T_DURACION.substring(10,13));
              $scope.minutos=parseInt(mDataArray.T_DURACION.substring(14,16));
              var date=moment(mDataArray.T_DURACION).format('YYYY-MM-DD HH:mm:ss');
              $scope.duracionCirugia                     = new Date(date);
            }
          }
          $scope.diagnosticoCie9                     = undefined;
          $scope.diagnosticoPrincipal                = undefined;
          self.dataFinal.datos.NF_CIE_10                   = mDataArray.NP_CIE_10;
          self.dataFinal.datos.NF_CIRUGIA_PLANEADA         = mDataArray.NP_CIRUGIA_PLANEADA;
          self.dataFinal.datos.C_TECNICA_QUIRURGICA        = mDataArray.C_TECNICA_QUIRURGICA;
          self.dataFinal.datos.C_HALLASGOS                 = mDataArray.C_HALLASGOS;
          self.dataFinal.datos.N_CONTEO_GASES              = mDataArray.N_CONTEO_GASES;
          self.dataFinal.datos.N_INCIDENTES                = mDataArray.N_INCIDENTES;
          self.dataFinal.datos.C_INCIDENTES                = mDataArray.C_INCIDENTES;
          self.dataFinal.datos.N_SANGRADO                  = mDataArray.N_SANGRADO;
          self.dataFinal.datos.N_TRANSFUSIONES             = mDataArray.N_TRANSFUSIONES;
          self.dataFinal.datos.C_TRANSFUSIONES             = mDataArray.C_TRANSFUSIONES;
          self.dataFinal.datos.N_RESULTDS_TRANSQUIRURGICOS = mDataArray.N_RESULTDS_TRANSQUIRURGICOS;
          self.dataFinal.datos.C_RESULTDS_TRANSQUIRURGICOS = mDataArray.C_RESULTDS_TRANSQUIRURGICOS;
          self.dataFinal.datos.C_ESTADO_POSTQUIRURGICO     = mDataArray.C_ESTADO_POSTQUIRURGICO;
          self.dataFinal.datos.C_PLAN_POSTQUIRURGICO       = mDataArray.C_PLAN_POSTQUIRURGICO;
          self.dataFinal.datos.N_BIOPSIA                   = mDataArray.N_BIOPSIA;
          self.dataFinal.datos.C_BIOPSIA                   = mDataArray.C_BIOPSIA;

          self.selecItemCie9                               = mDataArray.NF_CIRUGIA_PLANEADA;
          self.cie09Programado                             = mDataArray.C_DESCRIPCION_CIE_9_REALIZADA;
          self.selectedItemCIE10                           = mDataArray.NF_CIE_10;


          self.datosNota.CIRUJANO                          = mDataArray.CIRUJANOS;
          self.datosNota.AYUDANTE                          = mDataArray.AYUDANTE;
          self.datosNota.INSTRUMENTISTA                    = mDataArray.INSTRUMENTISTA;
          self.datosNota.ANESTESIOLOGO                     = mDataArray.ANESTESIOLOGO;
          self.datosNota.CIRCULANTE                        = mDataArray.CIRCULANTE;
          self.contadorMedicos = self.datosNota.CIRUJANO.length + self.datosNota.AYUDANTE.length + self.datosNota.INSTRUMENTISTA.length + self.datosNota.ANESTESIOLOGO.length + self.datosNota.CIRCULANTE.length;
          self.dataFinal.datos.NP_NOTA_POSTQUIRURGICA = mDataArray.NP_NOTA_POSTQUIRURGICA;

          self.idSeleccion                                 = seccion;

          self.dataFinal.datos.NF_PACIENTE          = $localStorage.paciente.expediente.substring(0,$localStorage.paciente.expediente.length - 4);
          self.dataFinal.datos.NF_MEDICO            = $localStorage.usuario.NP_EXPEDIENTE;
          self.dataFinal.datos.NF_NOTA_PRE          = mDataArray.NF_NOTA_PRE;
          self.dataFinal.datos.NF_CIRUGIA_REALIZADA = mDataArray.NF_CIE_9_REALIZADA;
          self.numColsapan = 2;

          if(seccion == 5 ){
                  copiarTiposMedico();
          }

          if(mDataArray.NF_ESTATUS == 1){
              getAllMedicos();
              self.regresarCancelar = "CANCELAR";
              if($scope.medicosActividad.length == 3){
                  $scope.medicosActividad.push({name: 'ELIMINAR',orderBy: 'idEspecialidad', width : '15%'})
              }
          }else{
              self.regresarCancelar = "REGRESAR";

              $scope.medicosActividad.splice(3,1);
          }

      };

      self.cambiarTipoMedico = function(){
          self.disabledMedico = false;
      };

      $scope.querySearchMedico = function(query){
        if(query.length>2){
          return _.filter(self.listMedicos, function(e){
            return e.value.toUpperCase().indexOf(query.toUpperCase())>=0;
          });
        }
      };

      $scope.selectedItemChangeMedico = function(item) {
        if(item != undefined){
         if(medicoOcupado(item.id) == false){
            addMedico(item);
         }else{
          mensajes.alerta("ERROR", "EL MEDICO YA FUE REGISTRADO EN OTRA ÁREA, NO PUEDE HABER UN MEDICO REGISTRADO 2 VECES" ,"OK");
         }
        };
      }


      var copiarTiposMedico = function(){

          self.dataFinal.datos.CIRUJANO = self.datosNota.CIRUJANO.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,C_CEDULA: aux.C_CEDULA, delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

          self.dataFinal.datos.AYUDANTE = self.datosNota.AYUDANTE.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,C_CEDULA: aux.C_CEDULA, delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

          self.dataFinal.datos.INSTRUMENTISTA = self.datosNota.INSTRUMENTISTA.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,C_CEDULA: aux.C_CEDULA, delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

          self.dataFinal.datos.ANESTESIOLOGO = self.datosNota.ANESTESIOLOGO.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,C_CEDULA: aux.C_CEDULA, delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

          self.dataFinal.datos.CIRCULANTE = self.datosNota.CIRCULANTE.map(function (aux){
            return {exp: aux.NF_EXPEDIENTE,C_CEDULA: aux.C_CEDULA, delete: aux.delete, NP_PERSONAL_CIRUGIA: aux.NP_PERSONAL_CIRUGIA};
          });

      };

      var addMedico = function(data){
        self.contadorMedicos += 1;

             switch(self.idTipoMedico) {
                    case 0:
                      self.datosNota.CIRUJANO.push({NF_EXPEDIENTE: data.id, C_CEDULA: data.C_CEDULA, NOMBRE: data.value, delete: false, NP_PERSONAL_CIRUGIA : "0"});
                      break;
                    case 1:
                      self.datosNota.AYUDANTE.push({NF_EXPEDIENTE: data.id, C_CEDULA: data.C_CEDULA,  NOMBRE: data.value, delete: false, NP_PERSONAL_CIRUGIA : "0"});
                      break;
                    case 2:
                      self.datosNota.INSTRUMENTISTA.push({NF_EXPEDIENTE: data.id, C_CEDULA: data.C_CEDULA,  NOMBRE: data.value, delete: false, NP_PERSONAL_CIRUGIA : "0"});
                      break;
                    case 3:
                      self.datosNota.ANESTESIOLOGO.push({NF_EXPEDIENTE: data.id, C_CEDULA: data.C_CEDULA,  NOMBRE: data.value, delete: false, NP_PERSONAL_CIRUGIA : "0"});
                      break;
                    case 4:
                      self.datosNota.CIRCULANTE.push({NF_EXPEDIENTE: data.id, C_CEDULA: data.C_CEDULA,  NOMBRE: data.value, delete: false, NP_PERSONAL_CIRUGIA : "0"});
                      break;
                    default:
                      break;
                }
        $scope.medicoSelected = ' ';
        self.disabledMedico   = true;
        self.idTipoMedico     = -1;
        var result = revisarDoctoresOperacion();
        self.mostrarMensaje = result.result;
      };


      self.eliminarOpcion = function(indice,idTipo){
        switch(idTipo) {
                    case 0:
                      if(self.editarNotaPost == false && self.datosNota.CIRUJANO[indice].NP_PERSONAL_CIRUGIA != "0"){
                        self.datosNota.CIRUJANO[indice].delete = true;
                      }else{
                        self.datosNota.CIRUJANO.splice(indice,1);
                      }

                      break;
                    case 1:
                      if(self.editarNotaPost == false && self.datosNota.AYUDANTE[indice].NP_PERSONAL_CIRUGIA != "0"){
                          self.datosNota.AYUDANTE[indice].delete = true;
                      }else{
                        self.datosNota.AYUDANTE.splice(indice,1);
                      }

                      break;
                    case 2:
                        if(self.editarNotaPost == false && self.datosNota.INSTRUMENTISTA[indice].NP_PERSONAL_CIRUGIA != "0"){
                           self.datosNota.INSTRUMENTISTA[indice].delete = true;
                        }else{
                            self.datosNota.INSTRUMENTISTA.splice(indice,1);
                        }

                      break;
                    case 3:
                        if(self.editarNotaPost == false && self.datosNota.ANESTESIOLOGO[indice].NP_PERSONAL_CIRUGIA != "0"){
                            self.datosNota.ANESTESIOLOGO[indice].delete = true;
                        }else{
                          self.datosNota.ANESTESIOLOGO.splice(indice,1);
                        }

                      break;
                    case 4:
                        if(self.editarNotaPost == false && self.datosNota.CIRCULANTE[indice].NP_PERSONAL_CIRUGIA != "0"){
                            self.datosNota.CIRCULANTE[indice].delete = true;
                        }else{
                          self.datosNota.CIRCULANTE.splice(indice,1);
                        }

                      break;
                    default:
                      break;
                }

                var result = revisarDoctoresOperacion();
                self.mostrarMensaje = result.result;
          self.contadorMedicos -= 1;
      };


      var medicoOcupado = function(id){
          if(_.isObject(_.find(self.datosNota.CIRUJANO, function(data){ return data.NF_EXPEDIENTE  == id; }))){
            return true;
          }
          if(_.isObject(_.find(self.datosNota.AYUDANTE, function(data){ return data.NF_EXPEDIENTE  == id; }))){
            return true;
          }
          if(_.isObject(_.find(self.datosNota.INSTRUMENTISTA, function(data){ return data.NF_EXPEDIENTE  == id; }))){
            return true;
          }
          if(_.isObject(_.find(self.datosNota.ANESTESIOLOGO, function(data){ return data.NF_EXPEDIENTE  == id; }))){
            return true;
          }
          if(_.isObject(_.find(self.datosNota.CIRCULANTE, function(data){ return data.NF_EXPEDIENTE  == id; }))){
            return true;
          }
          return false;
      };

      // var revisarDoctoresOperacion = function(){
      //     if( _.where(self.datosNota.CIRUJANO, {delete: false}).length == 0 ||
      //         _.where(self.datosNota.AYUDANTE, {delete: false}).length == 0 ||
      //         _.where(self.datosNota.INSTRUMENTISTA, {delete: false}).length == 0 ||
      //         _.where(self.datosNota.ANESTESIOLOGO, {delete: false}).length == 0 ||
      //         _.where(self.datosNota.CIRCULANTE, {delete: false}).length == 0 ){
      //       return {result: false, error: "DEBE AGREGAR POR LO MENOS UN MEDICO PARA CADA TIPO DE ACTIVIDAD"};
      //     }
      //     return  {result: true, error: ""};
      // };

      var revisarDoctoresOperacion = function(){
          if( _.where(self.datosNota.CIRUJANO, {delete: false}).length == 0){
            return {result: false, error: "DEBE AGREGAR POR LO MENOS UN MEDICO PARA CADA TIPO DE ACTIVIDAD"};
          }
          return  {result: true, error: ""};
      };


    self.N_INCIDENTES_CHANGE = function(tipoDato){
       switch(tipoDato) {

                    case 1:
                        self.C_INCIDENTES_DISABLED = (self.dataFinal.datos.N_INCIDENTES == 1) ? false : true;
                        self.dataFinal.datos.C_INCIDENTES = '';
                      break;
                    case 2:
                        self.N_TRANSFUSIONES_DISABLED = (self.dataFinal.datos.N_TRANSFUSIONES == 1) ? false : true;
                        self.dataFinal.datos.C_TRANSFUSIONES = undefined;
                      break;
                    case 3:
                        self.RESULTDS_TRANSQUIRURGICOS_DISABLED = (self.dataFinal.datos.N_RESULTDS_TRANSQUIRURGICOS == 1) ? false : true;
                        self.dataFinal.datos.C_RESULTDS_TRANSQUIRURGICOS = undefined;

                      break;
                    case 4:
                        self.c_biopsia_DISABLED = (self.dataFinal.datos.N_BIOPSIA == 1) ? false : true;
                        self.dataFinal.datos.C_BIOPSIA = undefined;

                      break;
                    default:
                      break;
                }

    };

      /**
       * [verificarNotaPostMin Función que se va a utilizar para verificar que la nota tenga los datos  necesarios
       *                       para guardar y firmar la nota]
       * @return {[type]} [un valor boleano que indica si los datos requeridos son validos]
       */
      var verificarNotaPost = function(){
        var bandera = true;

        if(self.dataFinal.datos.C_TECNICA_QUIRURGICA.trim() == ""){
          return false;
        }
        if(self.dataFinal.datos.C_HALLASGOS.trim() == ""){
            return false;
        }
        if(self.dataFinal.datos.N_INCIDENTES == 1){
          if(self.dataFinal.datos.C_INCIDENTES == ""){
            return false;
          }
        }
         if(self.dataFinal.datos.N_TRANSFUSIONES == 1){
          if(self.dataFinal.datos.C_TRANSFUSIONES == ""){
           returnfalse;
          }
        }
        if(self.dataFinal.datos.N_RESULTDS_TRANSQUIRURGICOS == 1){
          if(self.dataFinal.datos.C_RESULTDS_TRANSQUIRURGICOS == ""){
            return false;
          }
        }
        if(self.dataFinal.datos.C_ESTADO_POSTQUIRURGICO.trim() == ""){
          return false;
        }

        if(self.dataFinal.datos.C_PLAN_POSTQUIRURGICO.trim() == ""){
            return false;
        }

        if(self.dataFinal.datos.N_BIOPSIA == 1){
          if(self.dataFinal.datos.C_BIOPSIA == ""){
            return false;
          }
        }
        if(quirofano.getNotaActual().NOTAPREQUIRURGICA.N_ANESTESIA!=1){
            if($scope.hora == ""){
              bandera = false
            }
            if($scope.minutos == ""){
              bandera = false
            }
          }
        return bandera;
      };

          /**
       * [sellarDatosPostOperatorios Función que se utiliza para guardar y firmar las notas pos operatorias]
       * @return {[type]} [description]
       */
      self.sellarDatosPostOperatorios = function(form){
        if($scope.keyPriv != "" && $scope.certificado != ""){
            self.dataFinal.keyPriv     = $scope.keyPriv;
            self.dataFinal.certificado = $scope.certificado;
            guardarInformacion(self.dataFinal, true, false, form);
            self.dataFinal.id= self.dataFinal.datos.NF_MEDICO;
        }else{
          mensajes.alerta("ERROR", "POR FAVOR SELECCIONE SU CLAVE PRIVADA Y SU CERTIFICADO" ,"OK");
        }
      };



      /**
       * [guardarDatos Funsión para verificar y guardar las notas pre y post quirurgicas]
       * @param  {[type]} id   [description]
       * @param  {[type]} form [description]
       * @return {[type]}      [description]
       */
   self.guardarDatos = function(id,form){
          if(id == -1){
              if(verificarParaGuardarPreOpe()){
                   if(verificarDatosGuardar()){
                      fillNotaPreOperatoria(false);
                      guardarInformacion(self.datosServer,false,true, undefined,true);
                      self.idSeleccion = id;
                      self.firmarDatos = true;
                    }else{
                        mensajes.mConfirmacion("PRECAUCIÓN","PARA FIRMAR ESTA NOTA DEBE COMPLETAR TODOS LOS CAMPOS, SI GUARDA ASÍ SOLO SE HARÁ UN GUARDADO PARCIAL DE LA INFORMACIÓN PERO NO PODRA FIRMAR LA NOTA. ¿DESEA CONTINUAR CON EL GUARDADO SIN FIRMAR? ",
                                        "SI","NO")
                          .then(function() {

                          }).catch(function() {
                              self.firmarDatos = false;
                              fillNotaPreOperatoria(false);
                              guardarInformacion(self.datosServer,false,true);
                          });
                    }
              }

          }else{

           // self.dataFinal.datos.T_DURACION = moment($scope.duracionCirugia).format('DD/MM/YYYY HH:mm:ss');
            if($scope.hora!=undefined && $scope.minutos!=undefined){
              var h=($scope.hora.toString().length==1)? "0"+$scope.hora : $scope.hora;
              var m=($scope.minutos.toString().length==1)? "0"+$scope.minutos : $scope.minutos;
              self.dataFinal.datos.T_DURACION = "10/03/2016 " +h+":"+m+":00";
              console.log( self.dataFinal.datos.T_DURACION);
            }

            if(id == 1){
              if($scope.keyPriv == undefined || $scope.certificado == undefined){
                mensajes.alerta("ERROR","LA CLAVE PRIVADA Y EL CERTIFICADO SON OBLIGATORIOS" ,"OK");
                return
              }
              fillNotaPreOperatoria(true);
             guardarInformacion(self.datosServer,true,true);
             //guardarInformacion(self.datosServer, true, true,form,true);
            }else{
              if(id == 3){
                /**
                 * Este es el caso para crear una nota post operatoria
                 */

                  copiarTiposMedico();
                  if($scope.diagnosticoCie9 != undefined){
                      self.dataFinal.datos.NF_CIRUGIA_REALIZADA = $scope.diagnosticoCie9;
                  }

                  if($scope.diagnosticoPrincipal != undefined){
                    self.dataFinal.datos.NF_CIE_10 = $scope.diagnosticoPrincipal;
                  }

                  var result = revisarDoctoresOperacion();  
                  //  if(!result.result || verificarNotaPost() == false){
                  if( !result.result || verificarNotaPost() == false){
                        mensajes.mConfirmacion("PRECAUCIÓN","PARA FIRMAR ESTA NOTA DEBE COMPLETAR TODOS LOS CAMPOS, SI GUARDA ASÍ SOLO SE HARÁ UN GUARDADO PARCIAL DE LA INFORMACIÓN PERO NO PODRA FIRMAR LA NOTA. ¿DESEA CONTINUAR CON EL GUARDADO SIN FIRMAR? ",
                                        "SI","NO")
                          .then(function() {

                          }).catch(function() {
                               guardarInformacion(self.dataFinal, false, false,form,true,true);
                          });

                  }else{
                    self.idSeleccion = 5;
                    self.firmarDatos = true;
                     guardarInformacion(self.dataFinal, false, false,form,true);
                  }
              }
            }
          }
      };


      /**
       * [eliminarNota Funsión para eliminar una nota pre y post quirurgica ]
       * @param  {[type]} id   [tipo de nota]
       * @param  {[type]} form [formulario de la nota]
       * @return {[type]}      [description]
       */
      self.eliminarNota = function(id,form){
        if(id == 1){
           mensajes.mConfirmacion("PRECAUCIÓN","¿DESEA ELIMINAR ESTA NOTA PRE QUIRÚRGICA? ",
                                        "SI","NO")
                          .then(function() {

                          }).catch(function() {
                              eliminarNotaServer(true, $scope.delNotaPreQuirurgica, self.NP_NOTA_PREQUIRURGICA, form);
                          });

        }else{
          if(id == 2){
             mensajes.mConfirmacion("PRECAUCIÓN","¿DESEA ELIMINAR ESTA NOTA POST QUIRÚRGICA? ",
                                        "SI","NO")
                          .then(function() {

                          }).catch(function() {
                              eliminarNotaServer(false, $scope.delNotaPostQuirurgica, self.dataFinal.datos.NP_NOTA_POSTQUIRURGICA, form);
                          });
          }
        }

      };



      var eliminarNotaServer = function (TIPO_PREO_OPERATORIA,  URL, NP_NOTA_MEDICA, form){
        var mensaje = "LA NOTA SE ELIMINO DE FORMA EXITOSA" ;
        peticiones.deleteDatos(URL + NP_NOTA_MEDICA)
        .then(function(data) {
             if(TIPO_PREO_OPERATORIA){
                limpiarDatosPreOperatorios(true,true);

                mensajes.alerta("",mensaje ,"OK");
              $state.go($localStorage.stateAnterior);
                  self.idSeleccion = 1;
             }else{
               limpiarNotaPosOperatoria(form);
               mensajes.alerta("",mensaje ,"OK");
             $state.go($localStorage.stateAnterior);

             }
        })
        .catch(function(err) {
            console.log(err);
        });
      }




              /**
       * [guardarInformacion Función que se utiliza parsa guardar las notas (pre operatorias ) y (post operatorias)
       *                      ]
       * @param  {[type]} mData             [datos de la nota a guardar]
       * @param  {[type]} firmarNota        [Indica si la nota se guarda parcialmente o completa (con firma)]
       * @param  {[type]} notaPrequirurgica [Indica el tipo de nota a guardar (pre operatoria), (post operatoria)]
       * @return {[type]}                   [description]
       */

      // guardarInformacion(self.dataFinal, false, false,form,true);
      //
  var guardarInformacion = function(mData, firmarNota,notaPrequirurgica, form, soloActualizar,destinoQuirofano){
    var url = (notaPrequirurgica) ? "notaPrequirurgica/" : "notaPostquirurgica/";
    var mensaje = (firmarNota) ? "LA NOTA SE HA FIRMADO DE FORMA EXITOSA" : "LOS DATOS SE HAN GUARDADO DE FORMA EXITOSA";
    //var mensajeNotaPost = (firmarNota) ? "LA NOTA SE HA FIRMADO DE FORMA EXITOSA" : "LOS DATOS SE HAN GUARDADO DE FORMA EXITOSA";
        peticiones.postDatos(url,mData)
            .then(function(data) {
              self.editarNotaPost = false;
              if(notaPrequirurgica ){
                if(soloActualizar){
                  self.NP_NOTA_PREQUIRURGICA = data.NP_NOTA_PREQUIRURGICA;
                  self.isEditable = true;
                  self.notaPreOperatoriaEdit = true;
                }else{
                  limpiarDatosPreOperatorios(true,true);
                  // getNotasPreOperatorias($localStorage.paciente.expediente.substring(
                  //                         0, $localStorage.paciente.expediente.length - 4));
                  //mensajes.alerta("",mensaje ,"OK");


                   mensajes.mConfirmacion("",mensaje,"","OK")
                          .then(function() {
                              $state.go($localStorage.stateAnterior);
                          }).catch(function() {
                              $state.go($localStorage.stateAnterior);
                          });

                  self.idSeleccion = 1;
                }

                }else{ // Notas post operatorias
                  if(soloActualizar){

                        limpiarNotaPosOperatoria(form);


                        if(destinoQuirofano == true){
                            mensajes.mConfirmacion("",mensaje,"","OK")
                              .then(function() {
                                 $state.go($localStorage.stateAnterior);
                              }).catch(function() {
                                 $state.go($localStorage.stateAnterior);
                              });
                        }else{
                          blockUI.start();
                          getNotasPostDetail(data.NP_NOTA_POSTQUIRURGICA, 5);

                        }



                  }else{
                      limpiarNotaPosOperatoria(form);
                      mensajes.mConfirmacion("",mensaje,"","OK")
                          .then(function() {
                              $state.go($localStorage.stateAnterior);
                          }).catch(function() {
                              $state.go($localStorage.stateAnterior);
                          });
                  }

                }


            })
            .catch(function(err) {
              mensajes.alerta("ERROR", "OCURRIO UN ERROR AL GUARDAR LOS DATOS" ,"OK");
                console.log(err);
            });
    };

    $scope.cargaLlave = function (fileContent){
      $scope.keyPriv = fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
      $scope.certificado = fileContent;
    };

    $scope.imprimir=function(tipo){
      if(tipo==0)//PREOPERATORIA
         var url="notaPrequirurgica/reporte/"+ self.NP_NOTA_PREQUIRURGICA;
      else
        if(tipo==2)//POST OPERATORIA
            var url="notaPostquirurgica/reporte/"+ quirofano.getNpNotaPost();
       peticiones.getMethodPDF(url)
         .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          }).error(function(data){
              console.log(data);
          });

    };

    function getImprimir(bandFirmada){
      if(bandFirmada)
        $scope.imprimirNota=true;
    };
    $scope.regresar=function(){
       $state.go($localStorage.stateAnterior);
    };




    /**
     * [columns Sección para las cabeceras de las tablas]
     * @type {Array}
     */
      $scope.query = {
        order: 'cFecha',
        limit: 5,
        page: 1
      };

      $scope.columns = [ {
            name: 'FECHA',
            orderBy: 'cFecha',
            width : '20%'
        },{
          name: 'FOLIO NOTA',
            orderBy: 'NP_FOLIO',
            width : '20%'
      },{
          descendFirst: true,
          name: 'DIAGNÓSTICO PRE-OPERATORIO',
            orderBy: 'cDescripcion',
            width : '50%'
        },

      {
          name: 'VER/EDITAR',
            orderBy: 'NP_FOLIO',
            width : '10%'
      }];

  $scope.medicosActividad = [
    {
      name: 'EXPEDIENTE',
        orderBy: 'idEspecialidad',
        width : '15%'
    },
    {
      name: 'CEDULA',
        orderBy: 'idEspecialidad',
        width : '15%'
    },
    {
        name: 'NOMBRE',
        orderBy: 'display',
        width : '40%'
    }, {
      descendFirst: true,
      name: 'ACTIVIDAD REALIZADA',
        orderBy: 'idEspecialidad',
        width : '15%'
    },
     {
      name: 'ELIMINAR',
        orderBy: 'idEspecialidad',
        width : '15%'
  }];





      }]);
})();




