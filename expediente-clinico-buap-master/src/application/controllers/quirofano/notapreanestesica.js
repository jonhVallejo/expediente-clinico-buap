(function () {
  'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:QuirofanoNotapreanestesicaCtrl
 * @description
 * # QuirofanoNotapreanestesicaCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('QuirofanoNotapreanestesicaCtrl', ['$scope','$location','historiaClinicaService','cuestionarioService','$localStorage','usuario','medicos','blockUI','peticiones','mensajes','pacientes','$q','indicacionTerapeutica','$timeout','quirofano','$state','catalogos','$window','reportes',
    function ($scope,$location,historiaClinicaService,cuestionarioService,$localStorage,usuario,medicos,blockUI,peticiones,mensajes,pacientes,$q,indicacionTerapeutica,$timeout,quirofano,$state,catalogos,$window,reportes){
    
    $scope.total=0;
  	$scope.clasificacionGoldman="";
    $scope.tabGeneral=false;
    $scope.idNota=0;
    //Variables para usar cuestionarios
    var self = this;
    self.observacion        = '';
    self.contesCuestionario = true;   
    self.oCuestionarios     = [];
    self.mCuestionario      = {SECCION: []};
    self.mCuestionarioGeneral = {SECCION: []};
    var idEspecialidad = 10061;//Especialidad Genérica
    var ant = 0;
    var antAux = 0;
    //Variables para firma
    $scope.keyPriv=undefined;
    $scope.certificado=undefined;
    $scope.showFirma=false;
    $scope.showButton=true;
    //Variables para nota
    $scope.showNota=false;
    $scope.showEliminarNota=true;
    $scope.imprimirNota=false;
    $scope.registrosHistorico=[];
    $scope.editable=true;
    $scope.paciente={};
    self.datos={};
    var aux={DATOS:[]};
    var goldmanIde="goldman";
    var langeronIde="langeron";
    var riesgotromboemlicoIde="riesgotromboemlico";
    var medicacionIde="medicacion";

    var langeron=false;
    var medicacion=false;
    var estusCuestionario=false;
    //variable usada para mostrar en que rango pertenece goldamn, riesgo trombolico
    var antValoracion=undefined;
    //variables para medicamentos
    self.selectedMedicamentos="";
    //Variables para tabla
    $scope.query = {
        filter: '',
        order: '!NP_ID',
        limit: 5,
        page: 1
    };

    $scope.seccionesFaltantes="";
    //Variables para medicamento
    indicacionTerapeutica.C_TIPO_RECETA=1;
    indicacionTerapeutica.init();
    loadViaAdministracion();

    $scope.grupoUno = 0;
    $scope.notaActual=undefined;
    self.datos ={
      goldman:[
        {C_DESCRIPCION: 'EDAD > 70', N_PUNTAJE:5,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'CARDIOINFARTO < 6m', N_PUNTAJE:1,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'ESTENOSIS AÓRTICA', N_PUNTAJE:3,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: '3R C/GALOPE O IVY', N_PUNTAJE:11,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'ESTENOSIS AÓRTICA SEVERA', N_PUNTAJE:3,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'RITMO NO SINUSAL O EV ULTIMO EKG', N_PUNTAJE:7,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: '>5 EV/min EN CUALQUIER MOMENTO', N_PUNTAJE:7,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'p02 < 60 mmHg o Pc02 > 50 mmHg K < 3mEq/Ltro o HC03<20 mEq/Ltro BUN>50 o CREATINA > 3m/DI SIGNOS DE ENFERMEDAD HEPÁTICA, PACIENTE POSTRADO POR CAUSA NO CARDIACA', N_PUNTAJE:3,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'CIRUGÍA INTRAPERIOTENAL, INTRATORÁCICA O AÓRTICA', N_PUNTAJE:3,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'CIRUGÍA DE EMERGENCIA', N_PUNTAJE:4,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: '0-5 COMP. CARD. 1', N_PUNTAJE:0,C_VALOR:true,C_MOTIVO:"I",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: '6-12 COMP. CARD. 7', N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"II",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: '13-25 COMP. CARD. 13', N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"III",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: '>26 COMP. CARD. 7', N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"IV",NP_ID_RESPUESTA:0}
      ],
      langeron:[
        {C_DESCRIPCION: '<=55 AÑOS ', N_PUNTAJE:"puntaje",C_VALOR:false,C_MOTIVO:"EDAD",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: '>55 AÑOS', N_PUNTAJE:"puntaje",C_VALOR:false,C_MOTIVO:"EDAD",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: '<26 kg/m2 ', N_PUNTAJE:"puntaje",C_VALOR:false,C_MOTIVO:"IMC",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: '>26 kg/m2', N_PUNTAJE:"puntaje",C_VALOR:false,C_MOTIVO:"IMC",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'NO', N_PUNTAJE:"puntaje", C_VALOR:false,C_MOTIVO:"BARBA",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'SI ',N_PUNTAJE:"puntaje", C_VALOR:false,C_MOTIVO:"BARBA",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'COMPLETA', N_PUNTAJE:"puntaje", C_VALOR:false,C_MOTIVO:"DENTICIÓN",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'INCOMPLETA ',N_PUNTAJE:"puntaje", C_VALOR:false,C_MOTIVO:"DENTICIÓN",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'NO', N_PUNTAJE:"puntaje", C_VALOR:false,C_MOTIVO:"RONCOPATÍA",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'SI ', N_PUNTAJE:"puntaje", C_VALOR:false,C_MOTIVO:"RONCOPATÍA",NP_ID_RESPUESTA:0}
      ],
      riesgoTromboembolico:[
        {C_DESCRIPCION: 'SEXO FEMENINO',N_PUNTAJE:1,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'IGUAL O > 50 AÑOS', N_PUNTAJE:1,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'SOBREPESO DE > 20%',N_PUNTAJE:1,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'NEUROPATÍA', N_PUNTAJE:1,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'DIABETES MELITUS',N_PUNTAJE:1,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'Tx CON ESTRÓGENOS/PROGESTÁGENOS', N_PUNTAJE:1,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'REPOSO PROLONGADO', N_PUNTAJE:1, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'CIRUGÍA DE DURACIÓN <3 hrs', N_PUNTAJE:1, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'CRECIMIENTO CARDIACO Y/O FA ', N_PUNTAJE:5, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'ARTERITIS ', N_PUNTAJE:5, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'FLEBITIS ', N_PUNTAJE:5, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'VÁRICES DE MS PÉLVICOS ', N_PUNTAJE:5, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'NEOPLASIA MALIGNA ', N_PUNTAJE:5, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'CIRUGÍA CON DURACIÓN > 3h ', N_PUNTAJE:5, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'ANTECENDENTES DE TEP PREVÍA ', N_PUNTAJE:5, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'CIRUGÍA DE CADERA, FÉMUR O PRÓSTATA ', N_PUNTAJE:15, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'RIESGO MÍNIMO < 5 PUNTOS ', N_PUNTAJE:0, C_VALOR:true,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'RIESGO MODERADO 5-14 PUNTOS ', N_PUNTAJE:0, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'RIESGO ELEVADO >= 15 PUNTOS ', N_PUNTAJE:0, C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0}
      ],
      medicacion:[
       //0 MEDICAMENTO
        {C_DESCRIPCION: 'NOMBRE MEDICAMENTO', N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        //1 PRESENTACIÓN DE MEDICAMENTO
        {C_DESCRIPCION: 'PRESENTACION DE MEDICAMENTO',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        //2 DOSIS
        {C_DESCRIPCION: 'DOSIS', N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        //3 PRESENTACIÓN DE DOSIS
        {C_DESCRIPCION: 'UNIDAD DE DOSIS',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        //4 CANTIDAD APLICADA
        {C_DESCRIPCION: 'CANTIDAD APLICADA', N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        //5 VÍA DE ADMINISTRACIÓN
        {C_DESCRIPCION: 'VIA DE ADMINISTRACION',N_PUNTAJE:-1,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        //6 PESO
        {C_DESCRIPCION: 'PESO',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        //PREGUNTAS ABIERTAS
        {C_DESCRIPCION: 'MEDICAMENTO',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'SEDACIÓN',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'ANSIEDAD',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'ALUCINACIONES',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'BOCA SECA',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0}

      ]
    };
   
    //DATOS PARA MEDICACIÓN
    $scope.catUnidad=[
        {C_DESCRIPCION:'mg/kg',NP_ID_RESPUESTA:"1"},
        {C_DESCRIPCION:'mcg/kg',NP_ID_RESPUESTA:"2"}
    ];

    function loadViaAdministracion(){
        catalogos.getCatalogos('33').success(function(data){
          $scope.catViaAdministracion=data;
        })
        .error(function(data){
          //console.log(data);
        });
    };
   
    function getDatosPaciente(){
      $scope.notaActual=quirofano.getNotaActual();
      if($scope.notaActual!==undefined){
        $scope.paciente=$localStorage.paciente;
        $scope.paciente.edad=pacientes.calculaEdad($localStorage.paciente.f_nacimiento);
        activarCrearNota($scope.notaActual);
      }else 
          $state.go("quirofano");
    };
    getDatosPaciente();
    loadViaAdministracion();

    function activarCrearNota(notaActual){
      var defered = $q.defer();
      var promise = defered.promise;
      if(notaActual.length!==0){
      //CREAR NOTA SE VA ACTIVAR SI ESTA FINALIZADA LA NOTA PREQUIRURGÍCA
      if(notaActual.NOTAPREQUIRURGICA.NF_STATUS===2){
          //SI ESTUVO FINALIZADA LA NOTA PREQUIRURGICA VALIDAR EN QUE ESTATUS SE ENCUENTRA LA NOTA PREANESTÉSICA 
          //PENDIENTE
          //FIRMADA
          //CANCELADA
          if(notaActual.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA===0){
            crearNota(notaActual.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA);
          }
         else{
          if(notaActual.NOTAPREANESTESICA.NF_STATUS===1){//PENDIENTE
                $scope.editable=true;
                $scope.showButton=true;
                $scope.showEliminarNota=true;
                $scope.showNota=true;
                $scope.idNota=notaActual.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA;
                defered.resolve();
                return promise;
            }else{
            if(notaActual.NOTAPREANESTESICA.NF_STATUS ===2 || notaActual.NOTAPREANESTESICA.NF_STATUS ===0){//FIRMADA CANCELADA
                $scope.editable=false;
                $scope.showButton=false;
                $scope.showEliminarNota=false;
                $scope.showNota=true;
                $scope.idNota=notaActual.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA;
                $scope.imprimirNota=true;
                defered.resolve();
                return promise;
            }
          }
        }
              
      }else
           mensajes.alerta("ALERTA","SE REQUIERE QUE LA NOTA PREQUIRÚRGICA SE FINALICE","OK");
      }else 
          $state.go("quirofano");
    };

    function crearNota(idNotaPrequirurgica){
      aux={DATOS:[]};
      //NOTA PREOPERATORIA
      var url = "notaPreanestesica/registro/" + $localStorage.paciente.idexpediente +"/"+usuario.usuario.NP_EXPEDIENTE+ "/"+idNotaPrequirurgica;
      peticiones.getDatos(url)
        .then(function(data) {
          $scope.showNota=true;
          $scope.showButton=true;
          $scope.showEliminarNota=true;
          $scope.idNota=data.id;
            aux.DATOS=self.datos.goldman;
            post(aux,goldmanIde).then(function(){
              aux.DATOS=self.datos.langeron;
              post(aux,langeronIde).then(function(){
                aux.DATOS=self.datos.riesgoTromboembolico;
                post(aux,riesgotromboemlicoIde).then(function(){
                  aux.DATOS=self.datos.medicacion;
                  post(aux,medicacionIde);
                //  mensajes.alerta("ALERTA","NOTA CREADA EXITOSAMENTE","OK");
                });
              });
            });
        })
        .catch(function(err) {
             // //console.log(err);
              mensajes.alerta("ALERTA",err.error.toUpperCase(),"OK");
        });
    };
    $scope.guardarDatos=function(tipo){
      if(tipo===goldmanIde)
        put(self.datos.goldman,goldmanIde);
      else
        if(tipo===langeronIde)
          put(self.datos.langeron,langeronIde);
        else
         if(tipo===riesgotromboemlicoIde)
          put(self.datos.riesgoTromboembolico,riesgotromboemlicoIde);
         else
          put(self.datos.medicacion,medicacionIde);
    };


    $scope.eliminarNota=function(){
      var url= "notaPreanestesica/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota;
      peticiones.deleteDatos(url)
        .then(function(data) {
          mensajes.alerta("ALERTA",data.message.toUpperCase(),"OK");
          $scope.showEliminarNota=false;
          $scope.showButton=false;
          $scope.showNota=false;
          $state.go("quirofano");
        })
        .catch(function(err) {
          //console.log(err);
        });
    };

    function post(datos,tipo) {
      var defered = $q.defer();
      var promise = defered.promise;
      var url= "notaPreanestesica/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota +"/"+tipo;
      peticiones.postDatos(url,datos)
        .then(function(data) {
          defered.resolve();
        })
        .catch(function(err) {
              //console.log(err);
              defered.reject();
        });
      return promise;
    };

    function put(datos,tipo) {
      aux={DATOS:[]};
      aux.DATOS=datos;
      var url= "notaPreanestesica/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota +"/"+tipo;
      peticiones.putMethod(url,aux)
       .success(function(data){
            mensajes.alerta("ALERTA","RESPUESTA MODIFICADAS CORRECTAMENTE","OK");
          })
          .error(function(data){
            mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
          });
    };

    function getDatosNota(tipo) {
      var defered = $q.defer();
      var promise = defered.promise;

      var url= "notaPreanestesica/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota +"/"+tipo;
      peticiones.getDatos(url)
        .then(function(data) {
          asignarDatos(data,tipo);
          defered.resolve();
        })
        .catch(function(err) {
          //console.log(err);
          defered.reject();
        });

      return promise;
    };

    function asignarDatos(data,tipo) {
      if(tipo===goldmanIde)
        getDatosValor(goldmanIde,data,self.datos.goldman);
      else
        if(tipo===langeronIde)
          getDatosValor(langeronIde,data, self.datos.langeron);
        else
          if(tipo===riesgotromboemlicoIde)
            getDatosValor(riesgotromboemlicoIde,data, self.datos.riesgoTromboembolico);
          else
            getDatosValor(medicacionIde,data, self.datos.medicacion);
    };

    function getDatosValor(tipo,data,datos){//data=lo que recibo de servicio, datos=lo que tengo fijo
      var d=[{}];
      var index=0;
       _.each(data, function(i) {
         index=getIndice(datos,i.C_DESCRIPCION,i.C_MOTIVO,tipo);
         datos[index].C_VALOR=i.C_VALOR;
         datos[index].NP_ID_RESPUESTA=i.NP_ID_RESPUESTA;
         datos[index].N_PUNTAJE=i.N_PUNTAJE;
         if(tipo===medicacionIde)
            datos[index].C_MOTIVO=i.C_MOTIVO;
        });
      if(tipo!==medicacionIde)
       getRecuperarTotal(datos);
       if(tipo===goldmanIde){
          self.datos.goldman=datos;
          getDatosValoracionGoldman();
          $scope.calculaTotalGoldman(self.total,tipo);
        }
        else
          if(tipo===langeronIde)
           self.datos.langeron=datos;
          else
            if(tipo===riesgotromboemlicoIde){
              self.datos.riesgoTromboembolico=datos;
              getDatosValoracionTromboembolico();
              $scope.calculaValoracionTromboembolico(self.total,tipo);
            }
            else{
              self.datos.medicacion=datos;
              $scope.selectedItemChangeMedicamento($scope.querySearchMedicamento(self.datos.medicacion[0].C_MOTIVO),true);
            }
    };

    function getDatosValoracionGoldman() {
      self.datos.goldmanValoracion = _.where(self.datos.goldman, {N_PUNTAJE:0});
    };

    function getDatosValoracionTromboembolico() {
      self.datos.tromboembolicoValoracion = _.where(self.datos.riesgoTromboembolico, {N_PUNTAJE:0});
    };

    function getIndice (datos,descripcion,motivo,tipo) {
      var indx=0;
      _.find(datos, function(item, index) {
        if(tipo==langeronIde){
           if (item.C_DESCRIPCION === descripcion && item.C_MOTIVO === motivo) {
              indx=index;
          }
        }else{
           if (item.C_DESCRIPCION === descripcion) {
              indx=index;
          }
        }
         
        });
      return indx;
    };

    //FUNCIONES PARA OBTENER TOTALES
     $scope.setEstatusLangeron=function(pos,valor){
      self.datos.langeron[pos].C_VALOR=true;
      self.total=0;
      for(var i=0;i< self.datos.langeron.length;i++){
        if(self.datos.langeron[i].N_PUNTAJE==1)
          self.total+=parseInt(self.datos.langeron[i].N_PUNTAJE);
        i=i+1;
      }
    };
   
    function getRecuperarTotal(data) {
      self.total=0;
       _.each(data, function(i) {
          if(i.C_VALOR)
            self.total+=i.N_PUNTAJE;
        });
    };
    $scope.getTotal = function (item,tipo) {
      if (!item.C_VALOR)
        self.total += parseInt(item.N_PUNTAJE);
      else
        self.total-= parseInt(item.N_PUNTAJE);
      if(tipo===goldmanIde)
        $scope.calculaTotalGoldman(self.total,tipo);
      if(tipo===riesgotromboemlicoIde)
        $scope.calculaValoracionTromboembolico(self.total,tipo);
    };

    $scope.calculaTotalGoldman = function(total,tipo) {
      limpiarValoracion(tipo);
        if (total>=0 && total<=5){
          antValoracion=0;
          self.datos.goldmanValoracion[0].C_VALOR="true";
        }
      else if (total>=6 && total<=12){
            antValoracion=1;
            self.datos.goldmanValoracion[1].C_VALOR="true";
          }
      else if (total>=3 && total<=25){
            antValoracion=2;
            self.datos.goldmanValoracion[2].C_VALOR="true";
          }
        else if (total>=26){
              antValoracion=3;
              self.datos.goldmanValoracion[3].C_VALOR="true";
            }
    };

     $scope.calculaValoracionTromboembolico = function(total,tipo) {
      limpiarValoracion(tipo);
        if (total<5){
          antValoracion=0;
          self.datos.tromboembolicoValoracion[0].C_VALOR="true";
        }
      else if (total>=5 && total<=14){
            antValoracion=1;
            self.datos.tromboembolicoValoracion[1].C_VALOR="true";
          }
      else if (total>=15){
            antValoracion=2;
            self.datos.tromboembolicoValoracion[2].C_VALOR="true";
          }
    };

    function limpiarValoracion(tipo){
      if(antValoracion!=undefined){
        if(tipo===goldmanIde)
           self.datos.goldmanValoracion[antValoracion].C_VALOR=false;
        else
        if(tipo===riesgotromboemlicoIde){
          self.datos.tromboembolicoValoracion[antValoracion].C_VALOR=false;
        } 
      }
    };

    $scope.showKey=function(){
      $scope.seccionesFaltantes="";
      //SE VALIDA QUE LOS DATOS FIJOS Y LOS CUESTIONARIOS ESTEN LLENOS ANTES DE FIRMAR LA NOTA PRE ANÉSTESICA
      getValidarDatosFijos().then(function(data){
          getValidarDatosCuestionario().then(function(data){

            if(langeron)
              $scope.seccionesFaltantes="--LANGERON--"
            if(medicacion)
              $scope.seccionesFaltantes+="--MEDICACIÓN--"
            if(estusCuestionario)
                $scope.seccionesFaltantes+="--CUESTIONARIOS--"

              if($scope.seccionesFaltantes!==""){
                 mensajes.alerta('ALERTA','VERIFIQUE QUE EL APARTADO: '+$scope.seccionesFaltantes+' ESTEN LLENOS','ACEPTAR');
              }else{
                  mensajes.confirmacion('PRECAUCIÓN','UNA VEZ FIRMADA LA NOTA PRE-ANESTÉSICA NO PODRÁ EDITARLA','OK')
                  .then(function() {
                    $scope.showFirma=true;
                    $scope.showButton=false;
                    $scope.showNota=false;
                    $scope.showEliminarNota=false;
                    $scope.imprimirNota=false;
                  }).catch(function() {
                    $scope.showFirma=false;
                  });
              }
          }).catch(function(error){
            //console.log(error);
         });

     }).catch(function(error){
        //console.log(error);
     });

    };
    //FUNCIONES PARA FIRMA
    $scope.cargaLlave = function (fileContent){
      $scope.keyPriv=fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
      $scope.certificado=fileContent;
    };

    $scope.sellar= function() {
      if( $scope.keyPriv!=undefined && $scope.certificado!=undefined){
        var aux = {};
        aux.FIRMA=[];
        aux.DATOS=[];
        aux.idDoctor = parseInt(usuario.usuario.NP_EXPEDIENTE);
        aux.FIRMA.push({keyPriv:$scope.keyPriv, certificado:$scope.certificado});
        aux.DATOS.push({idNotaPreanestesica:$scope.idNota
        });
        //realizar petición
        var url= "notaPreanestesica/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota;
        peticiones.putMethod(url,aux)
         .success(function(data){
              mensajes.alerta("ALERTA","SE FIRMO CORRECTAMENTE","OK");
              $scope.showFirma=false;
              $scope.showNota=false;
              $scope.keyPriv="";
              $scope.certificado="";
              $state.go("quirofano");
            })
            .error(function(data){
             mensajes.alerta("ALERTA",data.error.toUpperCase(),"OK");
            });
      }else{
          mensajes.alerta('ALERTA','SELECCIONE LA LLAVE O EL CERTIFICADO','ACEPTAR');
      }
    };

    $scope.cancelar=function(){
        $scope.showFirma=false;
        $scope.showButton=true;
        $scope.showNota=true;
        $scope.showEliminarNota=true;
        $scope.imprimirNota=false;
        $scope.keyPriv="";
        $scope.certificado="";
    };

    function getValidarDatosFijos(){
      langeron=false;
      medicacion=false;
      var defered = $q.defer();
      var promise = defered.promise;
        getDatosNota(langeronIde).then(function(data){
        getDatosNota(medicacionIde).then(function(data){
              for(var i=0 ; i<5; i++){
                  if(self.datos.langeron[i].N_PUNTAJE =="puntaje" && self.datos.langeron[i+1].N_PUNTAJE =="puntaje"){
                      langeron=true;
                      defered.resolve();
                    break;
                  }
                defered.resolve();
                } 
                if( self.datos.medicacion[5].N_PUNTAJE=="puntaje"){
                      medicacion=true;
                      defered.resolve();
                }
                for(var i=0 ; i<5; i++){
                  if( self.datos.medicacion[i].C_MOTIVO==""){
                      medicacion=true;
                      defered.resolve();
                   break;
                  }
                  defered.resolve();
                } 

           });
        });
        return promise;
    };
    function getValidarDatosCuestionario(){
      var defered = $q.defer();
      var promise = defered.promise;
      var url="cuestionario/estatusPreanestesica/"+$localStorage.paciente.idexpediente+"/"+ $scope.idNota;
      peticiones.getDatos(url)
        .then(function(data) {

            if(!data.estatus)
              estusCuestionario=true;
            defered.resolve();
        }).catch(function(err) {
              //console.log(err);
              estusCuestionario=false;
              defered.reject();
        });
        return promise;
    };


    /*********************************FUNCIONES PARA TABLA*************************************************/
    $scope.onOrderChange = function(page, limit) {
      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve();
      }, 2000);

      return deferred.promise;
    };
    /******************FUNCIONES PARA LA BÚSQUEDA DE UN MEDICAMENTO******************************************/
    var currentMedicamento = {};
    $scope.selectedMedicamentos = [];
    $scope.querySearchMedicamento = function(text){
      if(text!=undefined){
        var result = indicacionTerapeutica.querySearch(text);
        result = _.difference(result, $scope.selectedMedicamentos);
        return result
      }
    }
    $scope.selectedItemChangeMedicamento = function(_medicamento,get){ //get=true si estamos obteniendo los datos, get=false si estamos generando un nuevo medicamento
      if(_medicamento!=undefined){
        currentMedicamento = _medicamento;
        if(get)
           self.selectedMedicamentos=currentMedicamento;
        else
           self.selectedMedicamentos=[currentMedicamento];
        currentMedicamento = {};
        $scope.medicamentoText = '';
      }
    };
    $scope.help = function(medicamento){
      mensajes.alerta('', _.filter(medicamento.DOSIS, function(el){
        return el.DESCRIPCION === medicamento.selected;
      })[0].DOSIS
      , 'Aceptar');
    };
    $scope.removeMedicamento = function(medicamento){
      self.selectedMedicamentos = _.without($scope.selectedMedicamentos, medicamento);
      for(var x in self.datos.medicacion){
        if(x>6)
          break;
        else
          self.datos.medicacion[x].C_MOTIVO="";
      }
    };

    $scope.formaFarmaceutica=function(medicamento){
      self.datos.medicacion[1].C_MOTIVO=medicamento;
    };

    $scope.guardaMedicamento=function(tipo){
      var jsonResult = {MEDICAMENTOS : _.map(self.selectedMedicamentos, function(el){
        return {
              C_NOMBRE_MEDICAMENTO:   el.NP_MEDICAMENTO,
        };
      })};
      if(jsonResult.MEDICAMENTOS[0]!=undefined){
        self.datos.medicacion[0].C_MOTIVO=jsonResult.MEDICAMENTOS[0].C_NOMBRE_MEDICAMENTO;
        $scope.guardarDatos(tipo);
      }
    };


    $scope.secciones =[
    /***********************MENÚ PRINCIPAL DE NOTA PREANÉSTESICA*******************************/
      { titulo:'TIPO DE ANESTESIA',visible: false, id: 29,buscarCuest: true},
      { titulo:'EVALUACIÓN CLÍNICA DEL PACIENTE',visible: false,buscarCuest: false},
      { titulo:'RIESGO ANÉSTESICO',visible: false,buscarCuest: false},
      { titulo:'MEDICACIÓN PRE-ANESTÉSICA',visible: false,id:-30,buscarCuest: false},
    /******************EVALUACIÓN ClÍNICA*****************************/
      { titulo:'ANTECEDENTES PERSONALES PATOLÓGICOS',visible: false,id:4,buscarCuest: true},
      { titulo:'ANTECEDENTES GINECO-OBSTÉTRICOS',visible: false, id: 18,buscarCuest: true},
      { titulo:'ANTECEDENTES PERINATALES',visible: false,id: 19,buscarCuest: true},
      { titulo:'ANTECEDENTES ANESTÉSICOS',visible: false,id: 20,buscarCuest: true},
      { titulo:'EXPLORACIÓN FÍSICA POR APARATOS SISTEMAS',visible: false,id: 21,buscarCuest: true},
    /******************RIESGO ANESTÉSICO *****************************/
      { titulo:'GOLDMAN',visible: false,id: -1,buscarCuest: false},
      { titulo:'NYHA',visible: false,id: 22,buscarCuest: true},
      { titulo:'LANGERON',visible: false,id: -1,buscarCuest: false},
      { titulo:'PATIL-ALDRETI',visible: false,id: 23,buscarCuest: true},
      { titulo:'MALLAMPATI',visible: false,id: 24,buscarCuest: true},
      { titulo:'ASA',visible: false,id: 25,buscarCuest: true},
      { titulo:'RIESGO TROMBOEMBÓLICO',visible: false,id: -1,buscarCuest: false}
    ];


    /********************************FUNCIONES PARA CUESTIONARIOS**********************************/

    var cuestionarioEspecifico = function(NP_TIPO_CUESTIONARIO, ID_ESPECIALIDAD, NF_PACIENTE, ID_NOTA, MEDICINA_GENERAL){
       var url = "cuestionario/preanestesica/" + NP_TIPO_CUESTIONARIO + "/" + ID_ESPECIALIDAD + "/"+ ID_NOTA ;
        peticiones.getDatos(url)
        .then(function(data) {
            if(data.SECCION){
                self.contesCuestionarioGeneral = !$scope.editable;
                self.contesCuestionario = !$scope.editable;
                cuestionarioService.setCuestionario(data);
                //console.log('obteniendo el serviciopooo')
                if(MEDICINA_GENERAL){
                  //console.log('es medicina general')
                    self.mCuestionarioGeneral = data
                }else{
                  //console.log('No es medicina general')
                  //console.log( 'Los datos que trajo el servicio', data)
                    self.mCuestionario=data;
                }
            }
        })
        .catch(function(err) {
              //console.log(err);
        });
    };

    var limpiarCuestionario = function(){
      //console.log('se va a limpiar el cuestionario')
      cuestionarioService.reset(self.mCuestionario);
      cuestionarioService.reset(self.mCuestionarioGeneral);
    }

    self.cambiarRadio = function(key_seccion,key_pregunta,bandera,MEDICINA_GENERAL){
      //console.log('111')
        //if(MEDICINA_GENERAL){
          if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
          //console.log('Cuestionario general', self.mCuestionarioGeneral)

              cuestionarioService.cambiarRadio(key_seccion,key_pregunta, bandera, self.mCuestionarioGeneral, MEDICINA_GENERAL);
            }else{
             //console.log('Cuestionario no general', self.mCuestionario)

              cuestionarioService.cambiarRadio(key_seccion,key_pregunta, bandera, self.mCuestionario,MEDICINA_GENERAL);
            }
        };

    self.seleccionarRespuesta = function(key_seccion,key_pregunta,key_respuesta,MEDICINA_GENERAL){
      //console.log('222')
        if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
              cuestionarioService.seleccionarRespuesta(key_seccion,key_pregunta, key_respuesta, self.mCuestionarioGeneral);
        }else{
              cuestionarioService.seleccionarRespuesta(key_seccion,key_pregunta, key_respuesta, self.mCuestionario);
        }
      };

    self.guardarRespuestas = function(MEDICINA_GENERAL){
      //cuestionarioService.setContestarCuestionario(!$scope.editable);
    //  var cuestionarioFin = (MEDICINA_GENERAL) ? angular.toJson(self.mCuestionarioGeneral) : angular.toJson(self.mCuestionario);
      var cuestionarioFin = {};

      if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
        cuestionarioFin = angular.toJson(self.mCuestionarioGeneral);
      }else{
        cuestionarioFin = angular.toJson(self.mCuestionario);
      }

      var result = cuestionarioService.revisarRespuestas(angular.fromJson(cuestionarioFin));
      if(result.correcta){
          var cuestionarioResult = cuestionarioService.crearCuestionarioFinal(angular.fromJson(result.cuestionario));
          cuestionarioResult.NF_MEDICO = usuario.usuario.NP_EXPEDIENTE
          cuestionarioResult.NF_PACIENTE = $localStorage.paciente.expediente.substring(0, $localStorage.paciente.expediente.length - 4);
          cuestionarioResult.NP_NOTA_PREANESTESICA = $scope.idNota;
          postRespuestas(cuestionarioResult,MEDICINA_GENERAL);
      }else{
        mensajes.alerta("ERROR","DEBE CONTESTAR TODAS LAS PREGUNTAS OBLIGATORIAS" ,"OK");
      }
    };

    $scope.guardarObs = function(seccion){
        self.datos[seccion].observaviones.unshift({descripcion:self.observacion,fecha:new Date()});
        self.observacion = '';
    };

    var  postRespuestas = function(mData,MEDICINA_GENERAL){
     if(mData.CONTESTADO){//Put
        peticiones.putMethod('cuestionario/respuestas/',mData)
          .success(function(data){
            mensajes.alerta("","DATOS ACTUALIZADOS CORRECTAMENTE" ,"OK");
          })
          .error(function(data){
            mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
          });

      }else{//Post
          peticiones.postDatos('cuestionario/respuestas/',mData)
            .then(function(data) {
              if(MEDICINA_GENERAL){
                self.mCuestionarioGeneral.CONTESTADO = true;//checar tenía diferente de true
              }else{
                self.mCuestionario.CONTESTADO = true;
              }
              mensajes.alerta("","DATOS ACTUALIZADOS CORRECTAMENTE" ,"OK");
            })
            .catch(function(err) {
                //console.log(err);
            });
      }
    };

    $scope.seleccionar = function(index){
      antValoracion=undefined;
      $scope.secciones[index].visible = !$scope.secciones[index].visible;
      if(index <= 3){
        if(index!==antAux){
           $scope.secciones[antAux].visible = false;
           $scope.secciones[ant].visible = false;
           antAux=index;
        }
      }else{
          if(ant!=index)
          $scope.secciones[ant].visible = false;
          ant=index;
      }
      if(!$scope.secciones[index].buscarCuest){
        //NO ES CUESTIONARIO
        if($scope.secciones[index].id!==undefined && $scope.secciones[index].visible){
          if($scope.secciones[index].id<0)
            if($scope.secciones[index].titulo==="GOLDMAN"){
              getDatosNota(goldmanIde);
            } 
            else
              if ($scope.secciones[index].titulo==="LANGERON")
                getDatosNota(langeronIde);
              else
                if ($scope.secciones[index].titulo==="RIESGO TROMBOEMBÓLICO"){
                  getDatosNota(riesgotromboemlicoIde);
                }
                else
                   getDatosNota(medicacionIde);
        }
      }
      else{
            if($scope.secciones[index].visible){
              self.mCuestionario.SECCION.splice(0,self.mCuestionario.SECCION.length);
              self.mCuestionarioGeneral.SECCION.splice(0,self.mCuestionarioGeneral.SECCION.length);
              //console.log('No se por que se manda a liumpiar')
              limpiarCuestionario();
              var NP_EXPEDIENTE_PACIENTE =$localStorage.paciente.idexpediente;
              //PETICIÓN PARA EL CUESTIONARIO ESPECIFICO
              cuestionarioEspecifico($scope.secciones[index].id,idEspecialidad,NP_EXPEDIENTE_PACIENTE,$scope.idNota, false);
            }
      }

    };

    var getIndiceArray = function(mArray,key){
      for(var j = mArray.length -1; j >= 0; j--){
          if(mArray[j].$$hashKey == key){
            return j
          }
      }
      return null;
    };

    $scope.imprimir=function(){
      var url="notaPreanestesica/reporte/"+ $scope.idNota;
       peticiones.getMethodPDF(url)
         .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          }).error(function(data){
              //console.log(data);
          });
    };
    
    $scope.regresar=function(){
      $state.go($localStorage.stateAnterior);
    };

 }]);
})();
