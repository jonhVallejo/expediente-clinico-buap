/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:QuirofanoInicioquirofanoCtrl
 * @description
 * # QuirofanoInicioquirofanoCtrl
 * Controller of the expediente
 */
(function(){
'use strict';
angular.module('expediente')
    .controller('QuirofanoInicioquirofanoCtrl',['$scope','$state','peticiones','quirofano','$localStorage','$q','$timeout',
    	function ($scope,$state,peticiones,quirofano,$localStorage,$q,$timeout) {
    		//getHistoricoNotasQuirurgicas();
    		$scope.query = {
		        order: 'fecha',
		        limit: 5,
		        page: 1
		    };

		    $scope.onOrderChange = function(page, limit){
		        var deferred = $q.defer();
		        $timeout(function () {
		          deferred.resolve();
		        }, 2000); 
		        return deferred.promise;
		    };

		    $scope.change = function(){
		      $scope.query.page = 1;
		    };

		    function getHistoricoNotasQuirurgicas(){ 
		    	//http://localhost:8080/ece/notaPrequirurgica/pacientes/
				var url="notaPrequirurgica/pacientes/";
			    peticiones.getDatos(url)
		        .then(function(data) {
		        	if(data.length>0)
		        		$scope.historicoNotas=data;
		        })
		        .catch(function(err) {
		          console.log(err);
		        });
	        };
	        
	        getHistoricoNotasQuirurgicas();
	        $scope.verNotaQuirurgica=function(registro){ 
		         $localStorage.paciente={
				      idexpediente: registro.NP_EXPEDIENTE.split("/")[0],
				      expediente: registro.NP_EXPEDIENTE.replace("/",""),
				      nombrePaciente: registro.C_PRIMER_APELLIDO+" "+registro.C_SEGUNDO_APELLIDO+" "+registro.C_NOMBRE,
				      idPaciente : registro.NP_ID,
				      curp : registro.C_CURP,
				      genero : registro.NF_SEXO,
				      f_nacimiento: registro.F_FECHA_DE_NACIMIENTO,
				      expedienteSIU : registro.NP_EXPEDIENTE_SIU
			     };
			    $state.go("quirofano");
	        };



	  }]);
})();



