/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:QuirofanoCirugiaseguraCtrl
 * @description
 * # QuirofanoCirugiaseguraCtrl
 * Controller of the expediente
 */

(function(){
'use strict';
angular.module('expediente')
    .controller('QuirofanoCirugiaseguraCtrl',['$scope','usuario','$state','peticiones','quirofano','$mdDialog','mensajes','$localStorage','pacientes','$q','$window','reportes',
    function ($scope,usuario,$state,peticiones,quirofano,$mdDialog,mensajes,$localStorage,pacientes,$q,$window,reportes) {

//**************************************************************
    //Variables para usar cuestionarios
    var self = this;
    var idEspecialidad = 10061;//Especialidad Genérica 
  
    //Variables para firma
    $scope.keyPriv=undefined;
    $scope.certificado=undefined;
    $scope.showFirma=false;
    $scope.showButton=true;
     //Variables para nota
    $scope.showCirugiaSegura=false;
    $scope.showEliminarNota=true;
    $scope.editable=true;
    $scope.paciente={};
    $scope.idNota=undefined;
    self.datos={};
    var aux={DATOS:[]};
    var ingreso="ingreso";
    var antesCirugia="antesCirugia";
    var salgaOperaciones="salgaOperaciones";
    //Variable para pregunta lanzada
    $scope.showPregunta= false;
    //Variables para cirugía Segura
    $scope.showLanzada1=false;
    $scope.showLanzada2=false;
    $scope.showCirugiaSegura=true;
    $scope.cancelar=false;
    $scope.firmas=undefined;//electiva, urgencias
    $scope.perfil=usuario.perfil;
    $scope.imprimirNota=false;
    $scope.mensajeCirculante="ENFERMERA CIRCULANTE ";
    $scope.mensajeCirujano="CIRUJANO ";
    $scope.mensajeAnestesiologo="";
    $scope.motivoCancelacion="";
    $scope.cancelada=false;

  

    self.datos ={
      ingreso:[
          {C_DESCRIPCION: '¿EL PACIENTE CONOCE A SU CÍRUJANO?',N_PUNTAJE:1,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},
          {C_DESCRIPCION: '¿HA CONFIRMADO EL PACIENTE SU IDENTIDAD,EL SITIO QUIRÚRGICO, EL PROCEDIMIENTO Y SU CONSENTIMIENTO?',N_PUNTAJE:1,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},
          {C_DESCRIPCION: '¿SE HA MARCADO EL SITIO QUIRÚRGICO?',N_PUNTAJE:0,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},
          {C_DESCRIPCION: '¿SE HA COMPLETADO LA COMPROBACIÓN DE LOS APARTADOS DE ANESTESÍA Y LA MEDICACIÓN ANESTÉSICA?',N_PUNTAJE:1,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},
          {C_DESCRIPCION: '¿SE HA COLOCADO EL PULSIOXIMETRO AL PACIENTE Y FUNCIONA?',N_PUNTAJE:1,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},
          {C_DESCRIPCION: '¿TIENE EL PACIENTE...ALERGIAS CONOCIDAS?',N_PUNTAJE:0,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},

          {C_DESCRIPCION: '¿VÍA AÉREA DIFÍCIL / RIESGO DE ASPIRACIÓN?',N_PUNTAJE:0,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},  //1
          {C_DESCRIPCION: '¿HAY MATERIALES Y EQUIPOS / AYUDA DISPONIBLE?',N_PUNTAJE:1,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},
        
          {C_DESCRIPCION: '¿RIESGO DE HEMORRAGÍA 500ml (7ml / kg EN NIÑOS)?',N_PUNTAJE:0,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},  //2
          {C_DESCRIPCION: '¿SE HA PREVISTO LA DISPONIBILIDAD DE LÍQUIDOS Y DOS VÍAS IV O CENTRALES?',N_PUNTAJE:1,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},
        ],
      antesCirugia:[
          {C_DESCRIPCION: '¿SE CONFIRMÓ QUE TODOS LOS MIEMBROS DEL EQUIPO SE HAYAN PRESENTADO POR SU NOMBRE Y FUNCIÓN?',N_PUNTAJE:0,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},
          {C_DESCRIPCION: '¿CONFIRMÓ LA IDENTIDAD DEL PACIENTE, EL SITIO QUIRÚRGICO Y EL PROCEDIMIENTO?',N_PUNTAJE:1,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0},   
          {C_DESCRIPCION: '¿SE HA ADMINISTRADO PROFILAXIS ANTIBIÓTICA EN LOS ÚLTIMOS 60 MINUTOS?',N_PUNTAJE:0,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0} , 
          
          {C_DESCRIPCION: 'CIRUJANO: ¿CUÁLES SERÁN LOS PASOS CRÍTICOS O NO SISTEMATIZADOS?',N_PUNTAJE:0,C_VALOR:0,C_MOTIVO:"",NP_ID_RESPUESTA:0},     
          {C_DESCRIPCION: 'CIRUJANO: ¿CUÁNTO DURARÁ LA OPERACIÓN?',N_PUNTAJE:0,C_VALOR:0,C_MOTIVO:"",NP_ID_RESPUESTA:0},     
          {C_DESCRIPCION: 'CIRUJANO: ¿CUÁL ES LA PÉRDIDA DE SANGRE PREVISTA?',N_PUNTAJE:0,C_VALOR:0,C_MOTIVO:"",NP_ID_RESPUESTA:0},
          {C_DESCRIPCION: 'ANESTESIÓLOGO (A):¿PRESENTA EL PACIENTE ALGÚN PROBLEMA ESPECÍFICO?',N_PUNTAJE:0,C_VALOR:0,C_MOTIVO:"",NP_ID_RESPUESTA:0},     
          {C_DESCRIPCION: 'ENFERMERÍA: ¿SE HA CONFIRMADO LA ESTERILIDAD (CON RESULTADO DE LOS INDICADORES)?',N_PUNTAJE:0,C_VALOR:0,C_MOTIVO:"",NP_ID_RESPUESTA:0},     
          {C_DESCRIPCION: 'ENFERMERÍA: ¿HAY DUDAS O PROBLEMAS RELACIONADOS CON EL INSTRUMENTAL Y LOS EQUIPOS?',N_PUNTAJE:0,C_VALOR:0,C_MOTIVO:"",NP_ID_RESPUESTA:0},     
         
          {C_DESCRIPCION: '¿PUEDEN VISUALIZARSE LAS IMÁGENES DIAGNÓSTICAS ESENCIALES?',N_PUNTAJE:0,C_VALOR:"puntaje",C_MOTIVO:"",NP_ID_RESPUESTA:0}     
          ],
      salgaOperaciones:[
        {C_DESCRIPCION: 'EL NOMBRE DEL PROCEDIMIENTO',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'EL RECUENTO DE INSTRUMENTOS, GASAS Y AGUJAS',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'EL ETIQUETADO DE LAS MUESTRAS(LECTURA DE LA ETIQUETA EN VOZ, INCLUIDO EL NOMBRE DEL PACIENTE)',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'SI HAY PROBLEMAS QUE RESOLVER REALACIONADOS CON EL INSTRUMENTO Y LOS EQUIPOS',N_PUNTAJE:0,C_VALOR:false,C_MOTIVO:"",NP_ID_RESPUESTA:0},
      ]
     
    };
    self.respuestas=[
        {ETIQUETA:'SI',VALUE:"1"},
        {ETIQUETA:'NO',VALUE:"0"}
    ];
    
    $scope.clickLanzada= function(){
     if(self.datos.ingreso[6].C_VALOR ==1){
        $scope.showLanzada1 = true; }
        else
        $scope.showLanzada1 = false;

      if(self.datos.ingreso[8].C_VALOR==1){
         $scope.showLanzada2 = true;}
         else
         $scope.showLanzada2 = false;
    };

    $scope.getDatosNota=function(tipo){
      var defered = $q.defer();
      var promise = defered.promise;
      var url= "cirugiaSegura/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota +"/"+tipo;
      peticiones.getDatos(url)
        .then(function(data) {
          asignarDatos(data,tipo);
          defered.resolve();
        })
        .catch(function(err) {
          console.log(err);
          defered.reject();
        });
      return promise;
    };

    function showLanzados(){
    if(self.datos.ingreso[6].C_VALOR == 1)
         $scope.showLanzada1=true;          
         else
         $scope.showLanzada1=false; 
      if(self.datos.ingreso[8].C_VALOR == 1)
         $scope.showLanzada2=true;          
         else
         $scope.showLanzada2=false; 

    };

    function asignarDatos(data,tipo) {
      if(tipo===ingreso){
        getDatosValor(ingreso,data,self.datos.ingreso);
        showLanzados();
      } 
      else
        if(tipo===antesCirugia)
          getDatosValor(antesCirugia,data, self.datos.antesCirugia);
        else
          if(tipo===salgaOperaciones)
            getDatosValor(salgaOperaciones,data, self.datos.salgaOperaciones);
    };

    function getIndice (datos,descripcion) {
      var indx=0;
      _.find(datos, function(item, index) {
          if (item.C_DESCRIPCION === descripcion) {
              indx=index;
          }
        });
      return indx;
    };

    function getDatosValor(tipo,data,datos){//data=lo que recibo de servicio, datos=lo que tengo fijo
      var d=[{}];
      var index=0;
       _.each(data, function(i) {
         index=getIndice(datos,i.C_DESCRIPCION);
         datos[index].C_VALOR=i.C_VALOR;
         datos[index].NP_ID_RESPUESTA=i.NP_ID_RESPUESTA;
         datos[index].C_MOTIVO=i.C_MOTIVO;
        });
        if(tipo===ingreso)
          self.datos.ingreso=datos;
        else
          if(tipo===antesCirugia)
           self.datos.antesCirugia=datos;
          else
            if(tipo===salgaOperaciones)
              self.datos.salgaOperaciones=datos;
    };

    function permisoCirugiaSegura(firmas){
      $scope.mensajeCirculante='';
      $scope.mensajeCirujano='';
      $scope.mensajeAnestesiologo='';
       var i=0;
      if(firmas.C_FIRMA_CIRCULANTE=="")
        $scope.mensajeCirculante=" ENFERMERA CIRCULANTE";
        else
          i=1;
      if(firmas.C_FIRMA_CIRUJANO=="")
            $scope.mensajeCirujano=" CIRUJANO";
        else
         i+=1;

     if(quirofano.getNotaActual().NOTAPREQUIRURGICA.N_ANESTESIA==1){
      if(firmas.C_FIRMA_ANESTESIOLOGO=="")
            $scope.mensajeAnestesiologo="ANESTESIÓLOGO";
        else
         i+=1;
      if(i==3) 
        $scope.showButton=true;
     }
      
      if(i==2) 
        $scope.showButton=false;

      $scope.showCirugiaSegura=true;

      if($scope.perfil.clave=="ECIRCULAN"){
         //SI YA FIRMO CIRUJANO YA NO SE PODRÍA NI EDITAR NI FIRMAR
        if(firmas.C_FIRMA_CIRCULANTE!==""){ //Ya firmo el circulante no podra editar ni firmar
              $scope.editable=false;
              $scope.showButton=false;
              $scope.showEliminarNota=false;
          }else{//No ha firmado el circulante
              $scope.editable=true;
              $scope.showButton=true;
              $scope.showEliminarNota=true;
          }
      }else{ //EN CASO DE QUE SEA CIRUJANO O ANÉSTESIOLOGO
             //PERMITIR FIRMAR PERO NO EDITAR
              $scope.editable=false;
              $scope.showButton=true;
              $scope.showEliminarNota=false;
      }
    };


    function getDatosCirugia(){
      var defered = $q.defer();
      var promise = defered.promise;
      var url="cirugiaSegura/"+$scope.idNota;
      peticiones.getDatos(url)
        .then(function(data) {
          if(data.C_TIPO_CIRUGIA=="PROGRAMADA"){
              $scope.C_TIPO_CIRUGIA="ELECTIVA";
          }
          else
            $scope.C_TIPO_CIRUGIA=data.C_TIPO_CIRUGIA;

          $scope.procedimiento=data.C_PROCEDIMIENTO_QUIRURGICO.toUpperCase();
          $scope.firmas=data;
          defered.resolve();
        })
        .catch(function(err) {
          console.log(err);
          defered.reject();
        });
      return promise;
    };

    function getAllDatos(){
        $scope.getDatosNota(ingreso);
        $scope.getDatosNota(antesCirugia);
        $scope.getDatosNota(salgaOperaciones);
    };

    function getEstatusNota(notaActual){
      var defered = $q.defer();
      var promise = defered.promise;
      if(notaActual.length!==0){
        if(notaActual.CIRUGIASEGURA.NP_ID_CIRUGIA_SEGURA==0){ //NO EXISTE ID, ENTONCES SE VA CREAR LA NOTA
           $scope.mensajeAnestesiologo="ANESTESIÓLOGO";
           crearNota(notaActual);
        }else{//EXISTE ID VALIDAR EL ESTATUS EN EL QUE SE ENCUENTRA 
            $scope.idNota=notaActual.CIRUGIASEGURA.NP_ID_CIRUGIA_SEGURA;
            if(notaActual.CIRUGIASEGURA.NF_STATUS===1){//PENDIENTE
                getAllDatos();
                getDatosCirugia() .then(function(data) {
                   permisoCirugiaSegura($scope.firmas);
                });
                defered.resolve();
                return promise;

            }else{
              if(notaActual.CIRUGIASEGURA.NF_STATUS ===2 || notaActual.CIRUGIASEGURA.NF_STATUS ===0){//FIRMADA CANCELADA
                  $scope.editable=false;
                  $scope.showButton=false;
                  $scope.showEliminarNota=false;
                  $scope.showCirugiaSegura=true;
                  getAllDatos();
                  $scope.imprimirNota=true;
                  getDatosCirugia() .then(function(data) {
                   if($scope.C_TIPO_CIRUGIA=="PROGRAMADA")
                      getEstatusCancelado();
                  });
                  permisoCirugiaSegura(notaActual.CIRUGIASEGURA);
                  defered.resolve();
                  return promise;
              }
            }
        }
      }else $state.go("quirofano");
    };
 
    function getDatosPaciente(){
      $scope.notaActual=quirofano.getNotaActual();
      if($scope.notaActual!==undefined){
        $scope.paciente=$localStorage.paciente;
        $scope.paciente.edad=pacientes.calculaEdad($localStorage.paciente.f_nacimiento);
         getEstatusNota($scope.notaActual);
      }else {
        $state.go("quirofano");
      }   
    };
    getDatosPaciente();
    
    function crearNota(notaActual){
    if($scope.perfil.clave=="ECIRCULAN"){
      aux={DATOS:[]};
      ///cirugiaSegura/registro/{idPaciente}/{idMedico}/{idNotaPrequirurgica}
      var url = "cirugiaSegura/registro/" + $localStorage.paciente.idexpediente +"/"+usuario.usuario.NP_EXPEDIENTE+"/"+notaActual.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA;
      peticiones.getDatos(url)
        .then(function(data) {
          $scope.showCirugiaSegura=true;
          $scope.showButton=true;
          $scope.showEliminarNota=true;
          $scope.idNota=data.id;
            aux.DATOS=self.datos.ingreso;
            post(aux,ingreso).then(function(){
              aux.DATOS=self.datos.antesCirugia;
              post(aux,antesCirugia).then(function(){
                aux.DATOS=self.datos.salgaOperaciones;
                  post(aux,salgaOperaciones);
                  getDatosCirugia();
                 // mensajes.alerta("ALERTA","NOTA CREADA EXITOSAMENTE","OK");
              });
            });
        })
        .catch(function(err) {
             console.log(err);
              mensajes.alerta("ALERTA",err.error.toUpperCase(),"OK");
        });
      }else{
          mensajes.confirmacion("ALERTA","NO TIENE PERMISO","OK");
          $state.go("quirofano");
      }
    };

    $scope.guardarDatos=function(tipo,mensaje){
      if(tipo===ingreso){
        put(self.datos.ingreso,mensaje,tipo);
      }
      else
        if(tipo===antesCirugia){
         put(self.datos.antesCirugia,mensaje,tipo);
        }
        else
         if(tipo===salgaOperaciones){
            put(self.datos.salgaOperaciones,mensaje,tipo);
          }
    };

    $scope.validarGuardar=function(tipo){
      var mensajeTexto="";
      if($scope.C_TIPO_CIRUGIA=="ELECTIVA"){
        var aux=undefined;
        $scope.cancelar=false;
        if(tipo==ingreso){
             aux = _.where(self.datos.ingreso, {N_PUNTAJE:1});
        }else{
          if(tipo==antesCirugia){
             aux = _.where(self.datos.antesCirugia, {N_PUNTAJE:1});
          }
          else{
             $scope.cancelar=false;
          }
        }
         if(aux!==undefined){
          for(var x in aux){
            if(aux[x].C_VALOR ==0){//NO  
                $scope.cancelar= true;
                break;
            }else{
              if(aux[x].C_VALOR ==1)//SI
                  $scope.cancelar=false;
            }
          }
        }
          if($scope.cancelar){
             mensajeTexto='DE ACUERDO A LOS VALORES SELECCIONADOS LA CIRUGÍA NO DEBE LLEVARSE A CABO'; 
             //pasar a la siguiente pestaña
             $scope.guardarDatos(tipo,mensajeTexto);
          }else
               $scope.guardarDatos(tipo,mensajeTexto);
      }else{
              console.log($scope.C_TIPO_CIRUGIA);
           $scope.guardarDatos(tipo," ");
      }
       
    };

    function validarCamposCirugia(){
      var defered = $q.defer();
      var promise = defered.promise;

      var antesCirugia=false;
      var ingreso=false;
      var despuesCirugia=false;
       $scope.getDatosNota(ingreso).then(function(data){
         $scope.getDatosNota(antesCirugia).then(function(data){
           $scope.getDatosNota(salgaOperaciones).then(function(data){

            for(var i=0 ; i<5; i++){
              if(self.datos.ingreso[i].C_VALOR=="puntaje" ){
                return defered.resolve(false);
              }
            } 

            for(var i=0 ; i<2; i++){
              if(self.datos.antesCirugia[i].C_VALOR =="puntaje" ){
                 return defered.resolve(false);
              }
            } 
            for(var i=3 ; i<8; i++){
              if(self.datos.antesCirugia[i].C_MOTIVO =="" ){
               return defered.resolve(false);
              } 
            } 
            if(self.datos.antesCirugia[9].C_VALOR =="puntaje"){
                return defered.resolve(false);
              }

            for(var x in self.datos.salgaOperaciones){
              if(self.datos.salgaOperaciones[x].C_VALOR==true)
                return defered.resolve(true);
            }


           //true=despuesCirugia almenos tiene un valor true
           // var aux=false;
           // if(ingreso==true && antesCirugia==true && despuesCirugia!==true)
           //    aux=true;
           //  else
           //    aux=false;
          defered.resolve(false);
          });
        });
      });
      return promise;
    };
   


     $scope.cancelarCirugiaSegura=function(){
      ///cirugiaSegura/{idPaciente}/{idNotaCirugiaSegura}
      var url= "cirugiaSegura/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota;
      peticiones.deleteDatos(url)
        .then(function(data) {
          $state.go("quirofano");
        })
        .catch(function(err) {
          console.log(err);
        });
    };

    function getEstatusCancelado(){
     $scope.motivoIngreso=[];
     $scope.motivoAntesCirugia=[];
      var defered = $q.defer();
      var promise = defered.promise;
      var url="cirugiaSegura/verifica/"+$scope.idNota;
      peticiones.getDatos(url)
        .then(function(data) {
         if(data.length>0){
           $scope.motivoIngreso= _.filter(data, function(val){ 
              return val.C_MOTIVO == "ingreso";       
            });
            $scope.motivoAntesCirugia= _.filter(data, function(val){ 
                return val.C_MOTIVO=="antesCirugia";
            });

            $scope.motivoIngreso = _.map($scope.motivoIngreso , function(el , key){
              el.C_DESCRIPCION = el.C_DESCRIPCION.replace("ingreso_","");
              return el;
            });
            $scope.motivoAntesCirugia = _.map($scope.motivoAntesCirugia , function(el , key){
              el.C_DESCRIPCION = el.C_DESCRIPCION.replace("antesCirugia_","");
              return el;
            });

            if($scope.motivoIngreso.length>0 || $scope.motivoAntesCirugia.length>0 )
                $scope.cancelada=true;
            else 
                $scope.cancelada=false;
            defered.resolve();
         }else{
           $scope.cancelada=false;
           defered.resolve();
         }
        })
        .catch(function(err) {
             console.log(err);
              mensajes.alerta("ALERTA",err.error.toUpperCase(),"OK");
               $scope.cancelar=false;   
              defered.reject();
        });
       
        return promise;
    };

    function mostrarMensaje(tipo,mensaje){
      if(tipo!="salgaOperaciones")
         mensajes.alerta("ALERTA","RESPUESTA MODIFICADAS CORRECTAMENTE."+mensaje,"OK",3000);
       else{
            getEstatusCancelado().then(function() {
                if($scope.C_TIPO_CIRUGIA=="URGENCIAS"){
                   mensajes.alerta("ALERTA","RESPUESTA MODIFICADAS CORRECTAMENTE.","OK",3000);
                }else{
                     if(!$scope.cancelada)
                       mensajes.alerta("ALERTA","RESPUESTA MODIFICADAS CORRECTAMENTE.","OK",3000);
                     else{
                        mensajes.confirmacion("ALERTA","RESPUESTA MODIFICADAS CORRECTAMENTE. ¿ DESEA FIRMAR PARA PROCEDER CON LA CANCELACIÓN?","OK")
                          .then(function() {
                              $scope.showButton=false;
                              $scope.showEliminarNota=false;
                              $scope.showCirugiaSegura=false;
                              $scope.imprimirNota=false;
                              $scope.showFirma=true;
                              $scope.motivoIngreso=[];
                              $scope.motivoAntesCirugia=[];
                          }).catch(function() {
                          });
                      }
                }
               

            }).catch(function() { });
          
       }
    };

    function post(datos,tipo) {
      var defered = $q.defer();
      var promise = defered.promise;
      var url= "cirugiaSegura/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota +"/"+tipo;
      peticiones.postDatos(url,datos)
        .then(function(data) {
          defered.resolve();
          $scope.getDatosNota(ingreso);
        })
        .catch(function(err) {
              console.log(err);
              defered.reject();
        });
      return promise;
    };

    function put(datos,mensaje,tipo){   
      aux={DATOS:[]};
      aux.DATOS=datos;
      ///cirugiaSegura/{idPaciente}/{idNotaCirugiaSegura}/{seccion}
      var url= "cirugiaSegura/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota +"/"+tipo;
      peticiones.putMethod(url,aux)
       .success(function(data){
           mostrarMensaje(tipo,mensaje);
          })
          .error(function(data){
            mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
          });
    };

   
    $scope.showKey=function(){
      validarCamposCirugia().then(function(data){
      if(!data){
           mensajes.alerta("ALERTA","PARA FIRMAR SE REQUIEREN TODOS LOS APARTADOS","OK");
      }else{
         $scope.showFirma=true;
          $scope.showButton=false;
          $scope.showCirugiaSegura=false;
      }
      }).catch(function() {
          $scope.showFirma=false;
        });
    };
    //FUNCIONES PARA FIRMA
    $scope.cargaLlave = function (fileContent){
      $scope.keyPriv=fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
      $scope.certificado=fileContent;
    };

    function firmando(){
      var aux = {};
      aux.FIRMA=[];
      aux.DATOS=[];
      aux.idDoctor = parseInt(usuario.usuario.NP_EXPEDIENTE);
      aux.FIRMA.push({keyPriv:$scope.keyPriv, certificado:$scope.certificado});
      aux.DATOS.push({idNotaPreanestesica:$scope.idNota});
      //realizar petición
      ///cirugiaSegura/{idPaciente}/{idNotaCirugiaSegura}/{tipoUsuarioFirma}
      var tipoUsuario=undefined;
      var url= "cirugiaSegura/firma/" + $localStorage.paciente.idexpediente +"/"+$scope.idNota;
      if($scope.perfil!=undefined){
         if($scope.perfil.clave=="ECIRCULAN")
            tipoUsuario="circulante";
          else
            if($scope.perfil.clave=="MANESTESI")
                 tipoUsuario="anestisiologo";
               else
                if($scope.perfil.clave=="MCIRUJANO")
                 tipoUsuario="cirujano";

          if(tipoUsuario!=undefined){
            url+="/"+tipoUsuario;
            peticiones.putMethod(url,aux)
             .success(function(data){
                  mensajes.alerta("ALERTA","SE FIRMO CORRECTAMENTE","OK");
                  $scope.showFirma=false;
                  $scope.showCirugiaSegura=false;
                  $scope.keyPriv="";
                  $scope.certificado="";
                  $state.go("quirofano");
                })
                .error(function(data){
                 mensajes.alerta("ALERTA",data.error.toUpperCase(),"OK");
                });
          }
      }
    };

    $scope.sellar= function() {
      if( $scope.keyPriv!=undefined && $scope.certificado!=undefined){
        getEstatusCancelado().then(function() {  
        if($scope.cancelar){
          mensajes.confirmacion("ALERTA","LA CIRUGÍA FUE CANCELADA, PROCEDER CON LA FIRMA","OK")
          .then(function() {
            $scope.cancelarCirugiaSegura();
            firmando();
          }).catch(function() {  });
        }else{
              firmando();
              $state.go("quirofano");
              }
        }).catch(function() { });
      }
      else{
          mensajes.alerta('ALERTA','SELECCIONE LA LLAVE O EL CERTIFICADO','ACEPTAR');
      }
    };

   

    $scope.cancelarFirma=function(){
        $scope.showFirma=false;
        $scope.showButton=true;
        $scope.keyPriv="";
        $scope.certificado="";
        $scope.showCirugiaSegura=true;
    };

    $scope.imprimir=function(){
   ///cirugiaSegura/reportePdf/{idCirugiaSegura}
      var url="cirugiaSegura/reportePdf/"+ $scope.idNota;
       peticiones.getMethodPDF(url)
         .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          }).error(function(data){
              console.log(data);
          });
    };

    $scope.regresar=function(){
     $state.go($localStorage.stateAnterior);
    };

    }]);
})();

