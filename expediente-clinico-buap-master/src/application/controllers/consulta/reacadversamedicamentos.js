'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultaReacadversamedicamentosCtrl
 * @description
 * # ConsultaReacadversamedicamentosCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('ConsultaReacAdversAMedicamentosCtrl',["$scope", "$http", "mensajes", "pacientes", "medicos", "$state", "peticiones", "$window" ,"reportes", function ($scope, $http, mensajes, pacientes, medicos, $state, peticiones, $window ,reportes) {
  
    $scope.tabConcecuencias=[
                {descripcion: "RECUPERADO SIN SECUELA",                     estatus: false, parametro: "RECUPERADO_SIN_SECUELA"},
                {descripcion: "RECUPERADO CON SECUELA",                     estatus: false, parametro: "RECUPERADO_CON_SECUELA"},
                {descripcion: "NO RECUPERADO",                              estatus: false, parametro: "NO_RECUPERADO"},
                {descripcion: "MUERTE - DEBIDO A LA REACCION ADVERSA",      estatus: false, parametro: "MUERTE_REACCION_ADVERSA"},
                {descripcion: "MUERTE - EL FARMACO PUDO HABER CONTRIBUIDO", estatus: false, parametro: "MUERTE_FARMACO"},
                {descripcion: "MUERTE - NO RELACIONADA AL MEDICAMENTO",     estatus: false, parametro: "MUERTE_RELACIONADA_AL_MEDICAMENTO"},
                {descripcion: "NO SE SABE",                                 estatus: false, parametro: "NO_SE_SABE"}
      ];  

    $scope.tabFarmacoterapia=[];
    $scope.F_ACTUAL = moment().toDate();
    $scope.fe = {
      FIRMA : {},
    };

    $scope.addReg = function() {

      if ($scope.tabFarmacoterapia.length > 0){
        var i = $scope.tabFarmacoterapia.length - 1;
        console.log($scope.tabFarmacoterapia);
        console.log(i);
        console.log($scope.tabFarmacoterapia[i]);
        if ($scope.tabFarmacoterapia[i].MEDICAMENTO != undefined && $scope.tabFarmacoterapia[i].DOSIS != undefined && $scope.tabFarmacoterapia[i].VIA_ADMON != undefined &&
            $scope.tabFarmacoterapia[i].F_INICIO != undefined && $scope.tabFarmacoterapia[i].F_TERMINO != undefined && $scope.tabFarmacoterapia[i].MOTIVO != undefined) 
              $scope.tabFarmacoterapia.push({MEDICAMENTO: undefined, DOSIS: undefined, VIA_ADMON: undefined, F_INICIO: undefined, F_TERMINO: undefined, MOTIVO: undefined}); 
        else
          mensajes.alerta('ERROR','EL ÚLTIMO REGISTRO NO ESTA COMPLETO','ACEPTAR!');       
      }
      else 
        $scope.tabFarmacoterapia.push({MEDICAMENTO: undefined, DOSIS: undefined, VIA_ADMON: undefined, F_INICIO: undefined, F_TERMINO: undefined, MOTIVO: undefined}); 
     // var i = $scope.tabFarmacoterapia.length - 1;
     //   console.log(i);
    }

    $scope.deleteReg = function(reg) {
      if ($scope.tabFarmacoterapia[reg].MEDICAMENTO == undefined && $scope.tabFarmacoterapia[reg].DOSIS == undefined && $scope.tabFarmacoterapia[reg].VIA_ADMON == undefined &&
            $scope.tabFarmacoterapia[reg].F_INICIO == undefined && $scope.tabFarmacoterapia[reg].F_TERMINO == undefined && $scope.tabFarmacoterapia[reg].MOTIVO == undefined) 
        $scope.tabFarmacoterapia.splice(reg,1);
      else
        mensajes.confirmacion('ADVERTENCIA','DESEA ELIMINAR EL REGISTRO','SI'). then(function() {
          $scope.tabFarmacoterapia.splice(reg,1);
        });      
    }

    $scope.obtenInformacion = function () {
      pacientes.getPaciente()
        .success(function(data){
          if(!data.data.hasOwnProperty('NP_ID')){
              //console.log(data);    
              $scope.paciente=data.data[0];
              console.log($scope.paciente);
              $scope.paciente.C_NOMBREPACIENTE = $scope.paciente.C_PRIMER_APELLIDO+" "+$scope.paciente.C_SEGUNDO_APELLIDO+" "+$scope.paciente.C_NOMBRE;
              //$scope.paciente.EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
              //$scope.paciente.F_FECHA_DE_NACIMIENTO = "01/04/2000";
              medicos.getMedicoActual(usuario.NP_EXPEDIENTE)
              .success(function(medico) {
                //console.log(medico);
                $scope.medico = medico;
                $scope.medico.NOMBRE_MEDICO = medico.C_PRIMER_APELLIDO+" "+medico.C_SEGUNDO_APELLIDO+" "+medico.C_NOMBRE;
                if ($scope.paciente.hasOwnProperty('C_EXPLORACION_FISICA')) {
                   var temp = $scope.paciente.C_EXPLORACION_FISICA.split(",");

                   $scope.paciente.ESTATURA = _.filter(temp, function(val){
                                  var dato = val.split(":")
                                  return dato[0] === "TALLA";
                                  })[0].split(":")[1];

                   $scope.paciente.PESO = _.filter(temp, function(val){
                                  var dato = val.split(":")
                                  return dato[0] === "PESO";
                                  })[0].split(":")[1];                  
                }

                if ($scope.paciente.hasOwnProperty('F_FECHA_DE_NACIMIENTO')) {
                  $scope.calcularEdad();             
                }

                /*$scope.catServicios = [];
                pacientes.getCatalogo("32").success(function(data){
                  $scope.catServicioso=data;
                });*/

                $scope.catSexo = [];
                  pacientes.getCatalogo("12").success(function(data){
                  $scope.catSexo=data;
                });

              })
              .error(function(data){
                mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
              });              
          }
        else
            mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');        
        })
        .error(function(data){
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      });

    }

    $scope.calcularEdad = function() {
     var dateTemp = $scope.paciente.F_FECHA_DE_NACIMIENTO.substr(0,10).split("/");

     var aa = moment().format("YYYY");
     var FechaNac = moment(aa+"-"+dateTemp[1]+"-"+dateTemp[0]);
     
     $scope.paciente.MESES = pacientes.calculaEdad(FechaNac);
     $scope.paciente.EDAD  = pacientes.calculaEdad(dateTemp);  
     if ($scope.paciente.EDAD.split(" ")[1] == "MESES") {
       $scope.paciente.EDAD = "0 AÑOS";
       $scope.paciente.MESES = pacientes.calculaEdad(dateTemp);  
     }
      
   }

    $scope.actFechaTermino = function(act) {
      act.T.F_LIMITE_INI = act.T.F_INICIO;
      act.T.F_LIMITE_TER = act.T.F_INICIO;
      act.T.F_TERMINO = undefined
    } 

    $scope.registra = function() {
      $scope.btnRegistro = true;
      if ($scope.paciente.C_INICIALES == undefined)
        $scope.paciente.C_INICIALES = " ";
      if ($scope.paciente.NF_SEXO_ID == undefined) {
        mensajes.alerta('ERROR','INGRESE EL GENERO','ACEPTAR!');
        document.getElementById("NF_SEXO_ID").focus();
        $scope.btnRegistro = false;
      }
      else
        if ($scope.paciente.ESTATURA == undefined) {
          mensajes.alerta('ERROR','INGRESE LA ESTATURA O TIENE UN ERROR','ACEPTAR!');
          document.getElementById("ESTATURA").focus();
          $scope.btnRegistro = false;
        }
        else
          if ($scope.paciente.PESO == undefined) {
          mensajes.alerta('ERROR','INGRESE EL PESO O TIENE UN ERROR','ACEPTAR!');
          document.getElementById("PESO").focus();
          $scope.btnRegistro = false;
        }
        else
        if ($scope.paciente.F_INICIO_REACCION == undefined){
          mensajes.alerta('ERROR','INGRESE LA FECHA DE INICIO DE LA REACCIÓN','ACEPTAR!');
          document.getElementById("F_INICIO_REACCION").focus();
          $scope.btnRegistro = false;
        }
        else
          if ($scope.paciente.F_CADUCIDAD == undefined) {
            mensajes.alerta('ERROR','LA FECHA DE CADUCIDAD ES ERRONEA','ACEPTAR!');
            document.getElementById("F_CADUCIDAD").focus();
            $scope.btnRegistro = false;
          }
          else
            if ($scope.paciente.F_INICIO_ADMINISTRACION == undefined) {
              mensajes.alerta('ERROR','INGRESE LA FECHA DE INICIO DE LA ADMINISTRACIÓN','ACEPTAR!');
              document.getElementById("F_INICIO_ADMINISTRACION").focus();
              $scope.btnRegistro = false;
            }
            else
              if ($scope.paciente.F_TERMINO_ADMINISTRACION == undefined) {
                mensajes.alerta('ERROR','INGRESE LA FECHA DE TERMINO DE LA ADMINISTRACIÓN','ACEPTAR!');
                document.getElementById("F_TERMINO_ADMINISTRACION").focus();
                $scope.btnRegistro = false;
              }
              else                         
                if ($scope.paciente.RETIRO_MEDICAMENTO_SOSPECHOSO == undefined){
                  mensajes.alerta('ERROR','ESPECIFIQUE SI SE RETIRO EL MEDICAMENTO SOSPECHOSO','ACEPTAR!');  
                  document.getElementById("F_TERMINO_ADMINISTRACION").focus();
                  $scope.btnRegistro = false;
                }
                else
                  if ($scope.paciente.T_CAMBIO_FARMACOTERAPIA == undefined){
                    mensajes.alerta('ERROR','ESPECIFIQUE SI SE CAMBIO LA FARMACOTERAPIA','ACEPTAR!');  
                    document.getElementById("T_CAMBIO_FARMACOTERAPIA").focus();
                    $scope.btnRegistro = false;
                  }
                  else
                    if ($scope.paciente.T_DESAPARECIO_REACCION == undefined){
                      mensajes.alerta('ERROR','ESPECIFIQUE SI DESAPARECIO LA REACCIÓN AL SUSPENDER EL MEDICAMENTO','ACEPTAR!');  
                      document.getElementById("T_DESAPARECIO_REACCION").focus();
                      $scope.btnRegistro = false;
                    }
                    else
                      if ($scope.paciente.T_REAPARECIO_REACCION == undefined) {
                        mensajes.alerta('ERROR','ESPECIFIQUE SI REAPARECIÓ LA REACCIÓN AL READMINSITRAR EL MEDICMENTO','ACEPTAR!');  
                        document.getElementById("T_REAPARECIO_REACCION").focus();
                        $scope.btnRegistro = false;
                      }
                      else
                        if ($scope.paciente.T_DISMINUYO_LA_DOSIS == undefined) {
                          mensajes.alerta('ERROR','ESPECIFIQUE SI SE DISMINUYO LA DOSIS','ACEPTAR!'); 
                          document.getElementById("T_DISMINUYO_LA_DOSIS").focus();
                          $scope.btnRegistro = false;
                        }
                        else 
                          if ($scope.paciente.T_RETIRO_MEDICAMENTO == undefined) {
                            mensajes.alerta('ERROR','ESPECIFIQUE SI NO SE RETIRÓ EL MEDICAMENTO','ACEPTAR!'); 
                            document.getElementById("T_RETIRO_MEDICAMENTO").focus();
                            $scope.btnRegistro = false;
                          }
                          else 
                            if ($scope.medico.SERVICIO == undefined) {
                              mensajes.alerta('ERROR','ESPECIFIQUE EL SERVICIO','ACEPTAR!'); 
                              document.getElementById("SERVICIO").focus();
                              $scope.btnRegistro = false;
                            }
                            else 
                            {
                              var completo = true;
                              angular.forEach($scope.tabFarmacoterapia, function(reg) {
                                if (reg.F_INICIO == undefined || reg.F_TERMINO == undefined)
                                  completo = false;
                              });
                              if (completo) {
                                if($scope.fe.FIRMA.keyPriv != undefined && $scope.fe.FIRMA.certificado != undefined) {

                                    console.log("Registrando...");
                                    
                                    $scope.INICIALES = [];
                                    creaNodo("C_INICIALES",$scope.paciente.C_INICIALES,1,"","C_INICIALES",$scope.INICIALES);
                                    creaNodo("F_FECHA_DE_NACIMIENTO",$scope.paciente.F_FECHA_DE_NACIMIENTO,84,"","F_FECHA_DE_NACIMIENTO",$scope.INICIALES);
                                    creaNodo("EDAD",$scope.paciente.EDAD+ " "+$scope.paciente.MESES,82,"","EDAD",$scope.INICIALES);
                                    //creaNodo("MESES",$scope.paciente.MESES,82,"","MESES",$scope.INICIALES);

                                    var C_SEXO_ID =  _.filter($scope.catSexo, function(val){
                                                            return $scope.paciente.NF_SEXO_ID === val.ID;
                                                          })[0].cDescripcion
                                    creaNodo("GENERO",C_SEXO_ID,83,"","GENERO",$scope.INICIALES);
                                    creaNodo("ESTATURA",$scope.paciente.ESTATURA,2,"","ESTATURA",$scope.INICIALES);
                                    creaNodo("PESO",$scope.paciente.PESO,3,"","PESO",$scope.INICIALES);                                    

                                    $scope.REACCION_ADVERSA = [];
                                    creaNodo("F_INICIO_REACCION",moment($scope.paciente.F_INICIO_REACCION).format("DD/MM/YYYY"),4,"","F_INICIO_REACCION",$scope.REACCION_ADVERSA);
                                    creaNodo("T_DESCRIPCION","",5,$scope.paciente.T_DESCRIPCION,"T_DESCRIPCION",$scope.REACCION_ADVERSA);
                                    //console.log($scope.tabConcecuencias);
                                    angular.forEach($scope.tabConcecuencias, function(tabla, index) {
                                      creaNodo(tabla.descripcion,tabla.estatus,6+index,"",tabla.parametro,$scope.REACCION_ADVERSA);
                                    });

                                    $scope.MEDICAMENTO_SOSPECHOSO = [];
                                    creaNodo("T_NOMBRE_GENERICO",$scope.paciente.T_NOMBRE_GENERICO,13,"","T_NOMBRE_GENERICO",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_DENOMINACION",$scope.paciente.T_DENOMINACION,14,"","T_DENOMINACION",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_LABORATORIO",$scope.paciente.T_LABORATORIO,15,"","T_LABORATORIO",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_NUMERO_LOTE",$scope.paciente.T_NUMERO_LOTE,16,"","T_NUMERO_LOTE",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("F_CADUCIDAD",$scope.paciente.F_CADUCIDAD,17,"","F_CADUCIDAD",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_DOSIS",$scope.paciente.T_DOSIS,18,"","T_DOSIS",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_VIA_ADMINISTRACION",$scope.paciente.T_VIA_ADMINISTRACION,19,"","T_VIA_ADMINISTRACION",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("F_INICIO_ADMINISTRACION",moment($scope.paciente.F_INICIO_ADMINISTRACION).format("DD/MM/YYYY"),0,"","F_INICIO_ADMINISTRACION",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("F_TERMINO_ADMINISTRACION",moment($scope.paciente.F_TERMINO_ADMINISTRACION).format("DD/MM/YYYY"),0,"","F_TERMINO_ADMINISTRACION",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("F_ADMINISTRACION",moment($scope.paciente.F_INICIO_ADMINISTRACION).format("DD/MM/YYYY")+ " - " +moment($scope.paciente.F_TERMINO_ADMINISTRACION).format("DD/MM/YYYY"),20,"","F_ADMINISTRACION",$scope.MEDICAMENTO_SOSPECHOSO);

                                    creaNodo("T_MOTIVO_PRESCIPCION",$scope.paciente.T_MOTIVO_PRESCIPCION,21,"","T_MOTIVO_PRESCIPCION",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("RETIRO_MEDICAMENTO_SOSPECHOSO",$scope.paciente.RETIRO_MEDICAMENTO_SOSPECHOSO,22,"","RETIRO_MEDICAMENTO_SOSPECHOSO",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_CAMBIO_FARMACOTERAPIA",$scope.paciente.T_CAMBIO_FARMACOTERAPIA,25,$scope.paciente.C_DESCRIP_CAMBIO_FARMACOTERAPIA,"T_CAMBIO_FARMACOTERAPIA",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_DESAPARECIO_REACCION",$scope.paciente.T_DESAPARECIO_REACCION,23,"","T_DESAPARECIO_REACCION",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_REAPARECIO_REACCION",$scope.paciente.T_REAPARECIO_REACCION,26,"","T_REAPARECIO_REACCION",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_DISMINUYO_LA_DOSIS",$scope.paciente.T_DISMINUYO_LA_DOSIS,24,$scope.paciente.C_DESCRIP_DISMINUYO_DOSIS,"T_DISMINUYO_LA_DOSIS",$scope.MEDICAMENTO_SOSPECHOSO);
                                    creaNodo("T_RETIRO_MEDICAMENTO",$scope.paciente.T_RETIRO_MEDICAMENTO,27,"","T_RETIRO_MEDICAMENTO",$scope.MEDICAMENTO_SOSPECHOSO);

                                    $scope.FARMACOTERAPIA_CONCOMITANTE = [];
                                    var inc = 28;
                                    angular.forEach($scope.tabFarmacoterapia, function(tabla, index) {
                                      creaNodo("MEDICAMENTO",tabla.MEDICAMENTO,inc,"","MEDICAMENTO",$scope.FARMACOTERAPIA_CONCOMITANTE); inc++;                              
                                      creaNodo("DOSIS",tabla.DOSIS,inc,"","DOSIS",$scope.FARMACOTERAPIA_CONCOMITANTE); inc++;                              
                                      creaNodo("VIA_ADMON",tabla.VIA_ADMON,inc,"","VIA_ADMON",$scope.FARMACOTERAPIA_CONCOMITANTE); inc++;                                   
                                      creaNodo("F_INICIO",moment(tabla.F_INICIO).format("DD/MM/YYYY"),0,"","F_INICIO",$scope.FARMACOTERAPIA_CONCOMITANTE);                                                  
                                      creaNodo("F_TERMINO",moment(tabla.F_TERMINO).format("DD/MM/YYYY"),0,"","F_TERMINO",$scope.FARMACOTERAPIA_CONCOMITANTE);
                                      creaNodo("FECHAS",moment(tabla.F_INICIO).format("DD/MM/YYYY") + " - " + moment(tabla.F_TERMINO).format("DD/MM/YYYY"),inc,"","FECHAS",$scope.FARMACOTERAPIA_CONCOMITANTE);
                                      inc++;
                                      creaNodo("MOTIVO",tabla.MOTIVO,inc,"","MOTIVO",$scope.FARMACOTERAPIA_CONCOMITANTE); inc++;                              
                                    });
                                    
                                    for (var i = (inc-28)/5; i < 10; i++) {
                                      creaNodo("MEDICAMENTO"," ",inc,"","MEDICAMENTO",$scope.FARMACOTERAPIA_CONCOMITANTE); inc++;                              
                                      creaNodo("DOSIS"," ",inc,"","DOSIS",$scope.FARMACOTERAPIA_CONCOMITANTE); inc++;                              
                                      creaNodo("VIA_ADMON"," ",inc,"","VIA_ADMON",$scope.FARMACOTERAPIA_CONCOMITANTE); inc++;                                   
                                      creaNodo("FECHAS"," " ,inc,"","FECHAS",$scope.FARMACOTERAPIA_CONCOMITANTE); inc++;
                                      creaNodo("MOTIVO"," ",inc,"","MOTIVO",$scope.FARMACOTERAPIA_CONCOMITANTE); inc++;                                                                    
                                    };

                                    $scope.HISTORIA_CLINICA = [];
                                    creaNodo("C_DATOS_IMPORTANTES_HC","",78,$scope.paciente.C_DATOS_IMPORTANTES_HC,"C_DATOS_IMPORTANTES_HC",$scope.HISTORIA_CLINICA);

                                    $scope.NOTIFICADOR_INICIAL = [];
                                    creaNodo("NOMBRE_MEDICO",$scope.medico.NOMBRE_MEDICO,0,"","NOMBRE_MEDICO",$scope.NOTIFICADOR_INICIAL);
                                    creaNodo("ESPECIALIDAD",$scope.medico.ESPECIALIDADES.CF_ESPECIALIDAD,0,"","ESPECIALIDAD",$scope.NOTIFICADOR_INICIAL);
                                    creaNodo("MEDICO",$scope.medico.NOMBRE_MEDICO + " - " + $scope.medico.ESPECIALIDADES.CF_ESPECIALIDAD,85,"","MEDICO",$scope.NOTIFICADOR_INICIAL);
                                    //creaNodo("C_NO_TELEFONO",$scope.paciente.C_NO_TELEFONO_UNO,0,"","C_NO_TELEFONO",$scope.NOTIFICADOR_INICIAL);
                                    creaNodo("CONSULTA_MEDICA",$scope.medico.SERVICIO,86,"","CONSULTA_MEDICA",$scope.NOTIFICADOR_INICIAL);
                                    creaNodo("C_NO_TELEFONO_UNO",$scope.paciente.C_NO_TELEFONO_UNO,87,"","TELEFONO_UNO",$scope.NOTIFICADOR_INICIAL);

                                    var datos = {RESPUESTAS : [ {PACIENTE                    : $scope.INICIALES},
                                                                {REACCION_ADVERSA            : $scope.REACCION_ADVERSA}, 
                                                                {MEDICAMENTO_SOSPECHOSO      : $scope.MEDICAMENTO_SOSPECHOSO},
                                                                
                                                                {HISTORIA_CLINICA            : $scope.HISTORIA_CLINICA},
                                                                {NOTIFICADOR_INICIAL         : $scope.NOTIFICADOR_INICIAL}],
                                                 FIRMA        : { keyPriv     : $scope.fe.FIRMA.keyPriv,
                                                                  certificado : $scope.fe.FIRMA.certificado,
                                                                },
                                                 FARMACOTERAPIA_CONCOMITANTE : $scope.FARMACOTERAPIA_CONCOMITANTE,               
                                                 ID_MEDICO   : usuario.NP_EXPEDIENTE,
                                                 ID_PACIENTE : $scope.paciente.NP_EXPEDIENTE.split("/")[0]
                                                 //ID_PACIENTE : $scope.paciente.NP_EXPEDIENTE
                                               }                                         

                                    /*console.log("Datos a Enviar..");
                                    console.log(datos); 
                                    $scope.btnRegistro = false;
                                    console.log(JSON.stringify( datos ));*/
                                    peticiones.postMethodPDF("reaccionesAdversas/" + datos.ID_PACIENTE + "/" + datos.ID_MEDICO, datos)
                                    .success(function(data){
                                        var file = new Blob([data], {type: 'application/pdf'});
                                        var fileURL = URL.createObjectURL(file);
                                        reportes.getReporte(fileURL, '_blank', 'width=1000, height=800'); 
                                        $scope.btnRegistro = false;
                                        $state.go('agendaConsultaPaciente',{state: 'reaccionesAdversasMedicamentos'});
                                    })
                                    .error(function(data){    
                                        //console.log(data);              
                                        if (!data.hasOwnProperty('error')) {
                                          var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                                          var obj = JSON.parse(decodedString);
                                          mensajes.alerta('ERROR',obj.error,'ACEPTAR');
                                        }
                                        else
                                          mensajes.alerta('ERROR','NO SE PUDO GENERAR EL DOCUMENTO','ACEPTAR');
                                        $scope.btnRegistro = false;
                                    });
                                }
                                else 
                                  if ($scope.fe.FIRMA.keyPriv == undefined) {
                                    $scope.btnRegistro = false;
                                    mensajes.alerta('AVISO','DEBES SELECCIONAR EL ARCHIVO .KEY PARA GENERAR LA NOTA DE EGRESO','ACEPTAR!');  
                                  }
                                  else if ($scope.fe.FIRMA.certificado == undefined) {
                                        $scope.btnRegistro = false;
                                        mensajes.alerta('AVISO','DEBES SELECCIONAR EL ARCHIVO .CER PARA GENERAR LA NOTA DE EGRESO','ACEPTAR!');  
                                  }
                              }
                              else {
                                mensajes.alerta('ERROR','EXISTEN FECHAS NO REGISTRADAS EN LA TABLA DEL APARTADO FARMACOTERAPIA CONCOMITANTE','ACEPTAR!');                        
                                $scope.btnRegistro = false;
                              }
                            }                      
    }

    function validaCampo(campo, nombCampo,letrero) {
      console.log("validando.."+nombCampo);
      var valido = true;
      if (campo == undefined) {
        mensajes.alerta('ERROR',letrero,'ACEPTAR!');
        document.getElementById(nombCampo).focus();
        $scope.btnRegistro = false;
        valido = false;
      }
      return valido;
    }

    $scope.cancelar = function() {
      $state.go('agendaConsultaPaciente',{state: 'reaccionesAdversasMedicamentos'});
    }

    function creaNodo(C_DESCRIPCION, C_VALOR, N_PUNTAJE, C_MOTIVO, C_PARAMETRO, APARTADO) {
      var nodo = {};      
        
      nodo.C_DESCRIPCION = C_DESCRIPCION;
      nodo.C_VALOR = C_VALOR;
      nodo.N_PUNTAJE = N_PUNTAJE;
      nodo.C_MOTIVO = C_MOTIVO;
      nodo.C_PARAMETRO = C_PARAMETRO;

      APARTADO.push(nodo);
    }

    $scope.cargaLlave = function (fileContent){
      $scope.fe.FIRMA.keyPriv = fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
      $scope.fe.FIRMA.certificado = fileContent;
    };

    $scope.toggle = function (item) {
      item.estatus = !item.estatus;
    }

  }]);
