(function(){
'use strict';

/**
* @ngdoc function
* @name ECEangular.module('expediente').controller:ConsultaSolicitudlentesCtrl
* @description
* # ConsultaSolicitudlentesCtrl
* Controller of the expediente
*/
angular.module('expediente')
.controller('ConsultaSolicitudlentesCtrl', ['$scope', '$http', 'mensajes', '$window', 'peticiones', '$state','reportes', function ($scope, $http, mensajes, $window, peticiones, $state,reportes) {



  $scope.imagePath = 'images/sl.jpg';
  $scope.showHints = true;


  $scope.submit = function(){


    $scope.saving = true;
    var finalObj = {
      NF_PACIENTE: $scope.receta.DATOS_PACIENTE.NP_EXPEDIENTE.replace('/', ''),
      NF_MEDICO: $scope.receta.DATOS_MEDICO.N_EXPEDIENTE,
      NF_RECETA: $scope.receta.NP_RECETA,
      F_FECHA_ENTREGA_LENTES : $scope.receta.F_FECHA_ENTREGA_LENTES,
      C_LEYENDA : $scope.receta.C_LEYENDA
    }

    finalObj = _.extend(finalObj, $scope.sl2);



    finalObj.OPTICA = createOptica($scope.sl);
    finalObj.OPTICA = _.union(finalObj.OPTICA, createOptica($scope.sl1));



    peticiones.postMethodPDF('optica', finalObj).success(function(data){
      $scope.saving = false;
      var file = new Blob([data], {type: 'application/pdf'});
      var fileURL = URL.createObjectURL(file);
      reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');

      $state.go('bienvenido');

    }).error(function(err){
      $scope.saving = false;

      mensajes.alerta('ERROR', 'ERROR INTERNO DEL SERVIDOR, CONTACTE AL ADMINISTRADOR', 'OK');
    });
  };

  $scope.cancel = function(){
    $scope.receta = undefined;
  }

  $scope.obtenReceta = function(folio) {
    //$http.get(baseURL + "receta/" + folio)
    $http.get(baseURL + "optica/receta/" + folio)
    .success(function(result){

      $scope.receta = result;
      if (!result.hasOwnProperty('B_CANCELAR'))
        mensajes.alerta('ERROR','NO EXISTE UNA RECETA CON EL FOLIO ESPECIFICADO','ACEPTAR!');
      else {
        $scope.receta.C_LEYENDA = " ";
        if (result.hasOwnProperty('F_FECHA_ENTREGA_LENTES') || true) {
          var dateTemp = result.F_FECHA_ENTREGA_LENTES.substr(0,10).split("/");
          var fechaReg = Date.parse(moment(dateTemp[2]+"-"+dateTemp[1]+"-"+dateTemp[0]).format("YYYY-MM-DD"));
          var fechaAct = Date.parse(moment().format("YYYY-MM-DD"));
          var numDias = (fechaAct-fechaReg)/86400000;
      

          if (numDias < 365) {
            $scope.receta.C_LEYENDA = "SÓLO SE PUEDEN ENTREGAR MICAS";
          }
        }
      }
    })
    .error(function(data) {

      mensajes.alerta('ERROR',data.error,'ACEPTAR!');
    });

  }


  function createOptica(obj){

    return _.map(_.pairs(obj), function(el){

      return {
        D_PUNTAJE: el[1],
        C_PARAMETRO: el[0].toLowerCase(),
        C_DESCRIPCION: el[0].toLowerCase()
      }
    });



  }


}]);

})();
