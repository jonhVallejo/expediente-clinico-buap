(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultaSignosvitalesCtrl
 * @description
 * # ConsultaSignosvitalesCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('ConsultaSignosvitalesCtrl', ['$scope','$localStorage','pacientes','consultas','mensajes','$state','usuario',
    function ($scope,$localStorage,pacientes,consultas,mensajes,$state,usuario) {

	$scope.nuevo = {
		'NOTAS_MEDICAS':{
			'NF_AGENDA':''
		},
		'EXPLORACION_FISICA' : {}
	}

	$scope.cargarDatos = function(){
		$scope.consulta = $localStorage.paciente;
        consultas.getEstatusSignosVitales($localStorage.paciente.idCita)
        .success(function(data){
            if(!data.success){
                mensajes.confirmacion('AVISO','YA SE HAN REGISTRADO LOS SIGNOS VITALES','ACEPTAR')
                .then(function() {
                    $state.go('bienvenido');
                }).catch(function() {
                    $state.go('bienvenido');
                });
            }
        }).error(function(data){});
		pacientes.getPaciente()
		.success(function(data){
			if(data.success===true){
				$scope.paciente=data.data[0];
				$scope.paciente.nombreCompleto = $scope.paciente.C_PRIMER_APELLIDO+' '+$scope.paciente.C_SEGUNDO_APELLIDO+' '+$scope.paciente.C_NOMBRE;
				$scope.paciente.direccion = $scope.paciente.C_CALLE+' '+$scope.paciente.C_NO_EXTERIOR+' '+$scope.paciente.C_COLONIA+' '+$scope.paciente.C_LOCALIDAD+' '+$scope.paciente.C_MUNICIPIO+' '+$scope.paciente.NF_ENTIDAD_FEDERATIVA;
				$scope.paciente.N_EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
				$scope.paciente.idExpediente = $localStorage.paciente.idexpediente;
			}
			else if(data.success===false){
				mensajes.alerta('AVISO',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
			}
			else{
				mensajes.alerta('ERROR','LO SENTIMOS, OCURRIÓ UN PROBLEMA','ACEPTAR!');
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});

		consultas.getIMC()
    	.success(function(data){
    		$scope.clasificacionIMC = data;
    	})
    	.error(function(data){
    		mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    	});
	};

    $scope.calcularIMC = function(){
    	if(($scope.nuevo.EXPLORACION_FISICA.N_PESO != undefined && $scope.nuevo.EXPLORACION_FISICA.N_TALLA != undefined) && ($scope.nuevo.EXPLORACION_FISICA.N_PESO != '' && $scope.nuevo.EXPLORACION_FISICA.N_TALLA != '')){
    		$scope.nuevo.EXPLORACION_FISICA.N_IMC = ($scope.nuevo.EXPLORACION_FISICA.N_PESO / ($scope.nuevo.EXPLORACION_FISICA.N_TALLA * $scope.nuevo.EXPLORACION_FISICA.N_TALLA)).toFixed(2);
    		$scope.nuevo.EXPLORACION_FISICA.C_CLASIFICACION = _.filter($scope.clasificacionIMC ,function(e){
	    		return $scope.nuevo.EXPLORACION_FISICA.N_IMC >= parseFloat(e.LIMITE_INFERIOR) && $scope.nuevo.EXPLORACION_FISICA.N_IMC <= parseFloat(e.LIMITE_SUPERIOR);
	    	});
	   
	    	if($scope.nuevo.EXPLORACION_FISICA.C_CLASIFICACION.length>0){
	    		$localStorage.C_CLASIFICACIONID = $scope.nuevo.EXPLORACION_FISICA.C_CLASIFICACION[$scope.nuevo.EXPLORACION_FISICA.C_CLASIFICACION.length-1].NP_ID;
	    		$scope.nuevo.EXPLORACION_FISICA.C_CLASIFICACION = $scope.nuevo.EXPLORACION_FISICA.C_CLASIFICACION[$scope.nuevo.EXPLORACION_FISICA.C_CLASIFICACION.length-1].C_DESCRIPCION;
	    	};
    	}
    };

    $scope.guardaSignos = function(){
    	$localStorage.signosVitales = {
    		'EXPLORACION_FISICA': $scope.nuevo.EXPLORACION_FISICA,
    		'NOTAS_MEDICAS': {
    			'NF_AGENDA':$localStorage.paciente.idCita
    		}
    	};
    	$localStorage.signosVitales.EXPLORACION_FISICA.C_CLASIFICACION = $localStorage.C_CLASIFICACIONID;
    	consultas.guardaSignosVitales($localStorage.signosVitales)
    	.success(function(data){
    		if(data.success === true){
    			mensajes.alerta('AVISO','DATOS GUARDADOS CORRECTAMENTE','ACEPTAR!');
    			//Habilitar solo para los médicos de Consulta Externa y Residentes
                if(usuario.perfil.clave === 'MEDICO' || usuario.perfil.clave === 'MGENERAL' || usuario.perfil.clave === 'RESI'){
                    if ($localStorage.paciente.page !== undefined){
                        if($localStorage.paciente.page === 'historiaClinica'){
                            $localStorage.procedenciaHistoria=true;
                            $localStorage.procedenciaExpediente=false;
                            $localStorage.procedenciaHistoriaClinica=true;
                            $state.go("historiaClinica");
                        }
                    }
                    else{
                        $localStorage.procedenciaHistoriaClinica=true;
                        $localStorage.procedenciaConsulExterna=true;///////////////////////////////////////////
                        $state.go("consultaNotaMedica");
                    }
                }
				else
					$state.go('consultaAgendaMedica');

				$localStorage.signosVitales = undefined;
				$localStorage.C_CLASIFICACIONID = undefined;
    		}
    		else if(data.success === false){
    			mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
    		}
    		else{
				mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    		}
    	})
    	.error(function(data){
    		mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    	});
    };

  }]);
})();