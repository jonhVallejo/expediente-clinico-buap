(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultaAgendamedicaCtrl
 * @description
 * # ConsultaAgendamedicaCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('ConsultaAgendamedicaCtrl', ['$scope','usuario','medicos','$state','pacientes','mensajes','$localStorage', '$timeout', 'blockUI','$stateParams','consultas',
	function ($scope,usuario,medicos,$state,pacientes,mensajes,$localStorage, $timeout, blockUI,$stateParams,consultas) {
	$scope.title 		= 'AGENDA MÉDICA';
	$scope.existCitas	= false;
	$scope.valor 		= {
		bool: true
	};
	$scope.isAsistente 	= false;
	$scope.query 		= {
		filter: '',
	    order: 'F_FECHA_HORA',
	    limit: 15,
	    page: 1
  	};
  	$scope.FECHA 		= new Date();

  	$scope.currentDate = new Date(
    	$scope.FECHA.getFullYear(),
      	$scope.FECHA.getMonth(),
      	$scope.FECHA.getDate());

	$scope.datos = [];

	if($stateParams.state === 'notaMedica'){
		$scope.isNotaMedica = true;
	}
	else if($stateParams.state === 'historiaClinica'){
		$scope.isNotaMedica = false;
	}
	else {
		$state.go("bienvenido");
	}


	$scope.cargaDatos = function(){
		var myBlockUI = blockUI.instances.get('myBlockUI');
    	myBlockUI.start();
		$scope.medico = {};
		medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
		.success(function(data){
			$scope.isResidente = false;
			$scope.isAsistente = false;
			if(usuario.perfil.clave === 'ENFER' || usuario.perfil.clave === 'ECIRCULAN') //Se compara el perfil del usuario para verificar si es ENFERMERA
				$scope.isAsistente = true;
			else if(usuario.perfil.clave == 'RESI'){
				$scope.isResidente = true;
			}
			else{
				$scope.isResidente = false;
				$scope.isAsistente = false;
			}
			if(data === ''){
				mensajes.alerta('ERROR','NO SE HA PODIDO ENCONTRAR INFORMACIÓN DE ESTE USUARIO','ACEPTAR!');
			}
			else{
				$scope.medico 				 = data;
				$scope.medico.FECHA 		 = new Date();
				$scope.medico.nombreCompleto = $scope.medico.C_PRIMER_APELLIDO +' '+$scope.medico.C_SEGUNDO_APELLIDO+' '+$scope.medico.C_NOMBRE;
			}
			loadAgendaDia();
			myBlockUI.stop();
			$scope.reload();
		})
		.error(function(data){
			myBlockUI.stop();
			mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
		});



	};

	function loadAgendaDia(){
		//if($scope.isAsistente || $scope.isResidente){
		if(usuario.perfil.clave === 'ENFER' || usuario.perfil.clave === 'ECIRCULAN' || usuario.perfil.clave == 'RESI'){
			$scope.getAgendaNotMedico();
		}
		else{
			var especialidadAgenda = $scope.medico.ESPECIALIDADES.NF_ESPECIALIDAD;
			var usuarioAgenda = usuario.usuario.NP_EXPEDIENTE;
			$scope.getAgendaMedico();
		}
	}

	var timer;
	$scope.reload = function(){
		timer = $timeout(function() {
			loadAgendaDia();
			$scope.reload();
		}, 120000);
	}


	$scope.$on("$destroy",
		function( event ) {
			$timeout.cancel( timer );
		});

	$scope.abrirAgendaMedica = function(dato){


		$localStorage.notaMedica = undefined;
		if($localStorage.paciente != undefined){
			if($localStorage.paciente.expediente !== dato.NF_EXPEDIENTE_PACIENTE.replace("/","")){
				$localStorage.selectedIndex = 0;
			}
		}
		$localStorage.paciente = {
			idexpediente: 	dato.NF_EXPEDIENTE_PACIENTE,
			expediente: 	dato.NF_EXPEDIENTE_PACIENTE.replace("/",""),
			nombreCompleto: dato.CF_PACIENTE,
			hora: 			dato.F_FECHA_HORA.split(" ")[1],
			fecha: 			dato.F_FECHA_HORA.split(" ")[0],
			idCita: 		dato.NP_ID,
			idPaciente : dato.NF_ID_PACIENTE
		};

		if(dato.NF_ESTATUS === 'ACTIVO'){
			$state.go("consultaSignosVitales");
		}
		else{
			
			$localStorage.procedenciaHistoria=false;
			$localStorage.procedenciaExpediente=false;
			$localStorage.procedenciaConsulExterna=true;///////////////////////////////////////////
			$state.go("consultaNotaMedica");
		}
	};

	$scope.parseTime = function (fecha){
    	return moment(fecha,'DD/MM/YYYY HH:mm:ss').format('HH:mm');
    };

    $scope.buscaAgenda = function(){
    	$scope.existCitas = false;
    	$scope.datos      = [];
    	if(!$scope.isResidente && !$scope.isAsistente){//Obtiene la agenda de los médicos
    		$scope.getAgendaMedico();
    	}
    	else{
    		if(moment($scope.medico.FECHA).format('DDMMYYYY') == moment(new Date()).format('DDMMYYYY')){
    			$scope.getAgendaNotMedico();
    		}
    	}
    };

	$scope.isToday = function(dato){
		if($scope.isAsistente){
			return (dato.NF_ESTATUS === 'ACTIVO') ? true : false
		}
		else{
			return (dato.NF_ESTATUS !== 'REALIZADA' && moment(dato.F_FECHA_HORA,'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY') === moment(new Date()).format('DD/MM/YYYY')) ? true : false;
		}
	};

	$scope.abrirHistoriaClinica = function(dato){
		$localStorage.notaMedica = undefined;
		if($localStorage.paciente != undefined){
			if($localStorage.paciente.expediente !== dato.NF_EXPEDIENTE_PACIENTE.replace("/","")){
				$localStorage.selectedIndex = 0;
			}
		}
		$localStorage.paciente = {
			idexpediente: 	dato.NF_EXPEDIENTE_PACIENTE,
			expediente: 	dato.NF_EXPEDIENTE_PACIENTE.replace("/",""),
			nombreCompleto: dato.CF_PACIENTE,
			hora: 			dato.F_FECHA_HORA.split(" ")[1],
			fecha: 			dato.F_FECHA_HORA.split(" ")[0],
			idCita: 		dato.NP_ID,
			idPaciente : dato.NF_ID_PACIENTE
		};

		if(dato.NF_ESTATUS === 'ACTIVO'){
			$localStorage.paciente.page = 'historiaClinica';
			$localStorage.procedenciaHistoriaClinica=true;
			$state.go("consultaSignosVitales");
		}
		else{
			$localStorage.procedenciaHistoria=true;
			$localStorage.procedenciaExpediente=false;
			$localStorage.procedenciaHistoriaClinica=true;
			

			$state.go("historiaClinica");
		}
	};

	$scope.buscaPaciente = function(text){
		if(text != undefined){
	   		$scope.datos = _.filter($scope.registros , function(e){
	   			var band = false;
	   			var fecha = moment(e.F_FECHA_HORA,'DD/MM/YYYY HH:mm:ss').format('HH:mm');
	   			if(e.CF_PACIENTE.toUpperCase().indexOf(text.toUpperCase()) >= 0 ) band = true;
	   			if(e.NF_ID_PACIENTE != undefined)
	   				if(e.NF_ID_PACIENTE.indexOf(text.toUpperCase()) >= 0 ) band = true;
	   			if(e.NF_EXPEDIENTE_PACIENTE_SIU != undefined)
	   				if(e.NF_EXPEDIENTE_PACIENTE_SIU.indexOf(text.toUpperCase()) >= 0 ) band = true;
	   			if(fecha != undefined)
	   				if(fecha.indexOf(text.toUpperCase()) >= 0 ) band = true;
	   			if(e.CF_ESPECIALIDAD != undefined)
	   				if(e.CF_ESPECIALIDAD.toUpperCase().indexOf(text.toUpperCase()) >= 0 ) band = true;
	   			return band;
	   		});
	   	}
	   	else{
	   		$scope.datos = $scope.registros;
	   	}
	};

	$scope.getAgendaNotMedico = function(){
		consultas.getAgendaMedicaHoy()
		.success(function(data){
			if(data.length <= 0){
				$scope.existCitas = true;
			}
			else{
				$scope.existCitas = false;
				$scope.datos      = data;
				$scope.registros = $scope.datos;
			}
		})
		.error(function(data){
			$scope.existCitas = true;
			mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
		});
	};

	$scope.getAgendaMedico = function(){
		medicos.getAgendaDia(moment($scope.medico.FECHA).format('DDMMYYYY'),$scope.medico.ESPECIALIDADES.NF_ESPECIALIDAD,usuario.usuario.NP_EXPEDIENTE)
		.success(function(data){
			if(data.length <= 0){
				$scope.existCitas = true;
				//mensajes.alerta('AVISO','NO EXISTEN CITAS AGENDADAS','ACEPTAR!');
			}
			else{
				$scope.existCitas = false;
				$scope.datos      = data;
				$scope.registros = $scope.datos;
			}
		})
		.error(function(data){
			$scope.existCitas = true;
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

  }]);
})();