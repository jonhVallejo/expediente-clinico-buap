'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultaCalifRiesgoObstCtrl
 * @description
 * # ConsultaCalifRiesgoObstCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('ConsultaCalifRiesgoObstCtrl', function ( $scope, $http, mensajes, pacientes, catalogos, peticiones, $window, usuario, medicos, $state ,reportes) {

  	//var baseURL = 'http://148.228.103.13:8080/';
  	var actionURL = 'riesgo';
  	$scope.N_CALIFICACION = 0;
  	
    

    $scope.C_ESTATUS_ALTO_RIESGO = "";
	$scope.N_CLASIFICACION_ALTO_RIESGO = "";
	$scope.C_ESTATUS_BAJO_RIESGO = "X";
	$scope.N_CLASIFICACION_BAJO_RIESGO = "yellow";
	$scope.DESCIPCION_RIEGO = "BAJO RIESGO";

  	$scope.inicializaPagina = function() {
  		
	    $scope.catSexo = [];

	    pacientes.getCatalogo("12").success(function(data){
	      $scope.catSexo=data;
	    });

	    $scope.tabHistorialClinica = [ 	{ opciones :  [	{descripcion: "EDAD (<15 AÑOS Ó >40 AÑOS)", 	estatus: false, puntaje: 1,		calificacion : 0, id: "HC_EDAD"},
	    												{descripcion: "ANALFABETA", 					estatus: false, puntaje: 0.5,	calificacion : 0, id: "HC_ANALF"} ] } ];



	    $scope.tabAntecedentesPersonales = [ 	{ opciones :  [	{descripcion: "TB PULMONAR", 			estatus: false, puntaje: 2, calificacion : 0, id: "APP_TB"},
	    													 	{descripcion: "CIRUGÍA PÉLVICA/UTERINA",estatus: false, puntaje: 4, calificacion : 0, id: "APP_CPU"} ] },
	    										{ opciones :  [	{descripcion: "DIABETES MELLITUS", 		estatus: false, puntaje: 3, calificacion : 0, id: "APP_DM"},
	    													 	{descripcion: "ENFERMEDADES INMUNOLÓGICAS", 	estatus: false, puntaje: 4, calificacion : 0, id: "APP_EI"} ] },
	    										{ opciones :  [	{descripcion: "HAS CRÓNICA", 			estatus: false, puntaje: 3, calificacion : 0, id: "APP_HAS"}] }	    										
	    									];

	    $scope.tabAntecedentesGinecoObst = [ 	{ opciones :  [{descripcion: "INFERTILIDAD", 					estatus: false, puntaje: 2, calificacion : 0, id: "AGO_INFER"},
	    													 	{descripcion: "ÓBITOS (UNO)", 					estatus: false, puntaje: 2, calificacion : 0, id: "AGO_OBIT"} ] },
	    										{ opciones :  [{descripcion: "PRIMIGESTA O MULTIGESTA", 		estatus: false, puntaje: 3, calificacion : 0, id: "AGO_PRIM_MULT"},
	    													 	{descripcion: "PÉRDIDA GESTACIONAL RECURRENTE", estatus: false, puntaje: 3, calificacion : 0, id: "AGO_PGR"} ] },
	    										{ opciones :  [{descripcion: "ABORTO PREVIO", 					estatus: false, puntaje: 2, calificacion : 0, id: "AGO_ABORTO"},
	    													 	{descripcion: "RN MUERTE EN 1er SEMANA DE VIDA EU",estatus: false, puntaje: 2, calificacion : 0, id: "AGO_RN"} ] },
	    										{ opciones :  [{descripcion: "CESÁREAS PREVIA", 					estatus: false, puntaje: 3, calificacion : 0, id: "AGO_CESAREA"},
	    													 	{descripcion: "RN CON PESO <2500 gr.", 			estatus: false, puntaje: 2, calificacion : 0, id: "AGO_PESO_MENOR"} ] },
	    										{ opciones :  [{descripcion: "2 Ó MÁS CESARIAS", 				estatus: false, puntaje: 4, calificacion : 0, id: "AGO_2_O_MASC"},
	    													 	{descripcion: "RN CON PESO >4000 gr.", 			estatus: false, puntaje: 2, calificacion : 0, id: "AGO_PESO_MAYOR"} ] },
	    										{ opciones :  [{descripcion: "PERIODO INTERGENÉSICO CORTO", 	estatus: false, puntaje: 3, calificacion : 0, id: "AGO_PIC"} ] },			 	 
	    									];

	    $scope.tabEmbarazoActual = [ 	{ opciones :  [	{descripcion: "PREECLAMSIA EN EMBARAZO ANTERIOR",estatus: false, puntaje: 4,   calificacion : 0, id: "EA_PREC"},
													 	{descripcion: "GRUPO SANGUÍNEO RH NEGATIVO",	 estatus: false, puntaje: 6,   calificacion : 0, id: "EA_GPO_SANG"} ] },
										{ opciones :  [	{descripcion: "PESO >80 kg.", 					 estatus: false, puntaje: 2,   calificacion : 0, id: "EA_PESO_MAYOR"},
													 	{descripcion: "HB<10mg/DL ", 					 estatus: false, puntaje: 3,   calificacion : 0, id: "EA_HB"} ] },
										{ opciones :  [	{descripcion: "PESO <40 kg.", 					 estatus: false, puntaje: 3,   calificacion : 0, id: "EA_PESO_MENOR"},
													 	{descripcion: "VDRL POSITIVO",					 estatus: false, puntaje: 3,   calificacion : 0, id: "EA_VDRL"} ] },
										{ opciones :  [	{descripcion: "TALLA <150 cm.", 				 estatus: false, puntaje: 0.5, calificacion : 0, id: "EA_TALLA_MENOR"},
													 	{descripcion: "VIH POSITIVO", 					 estatus: false, puntaje: 6,   calificacion : 0, id: "EA_VIH"} ] },
										{ opciones :  [	{descripcion: "FUM INCIERTA", 					 estatus: false, puntaje: 0.5, calificacion : 0, id: "EA_FUM"},
													 	{descripcion: "IVU", 							 estatus: false, puntaje: 3,   calificacion : 0, id: "EA_IVU"} ] },
										{ opciones :  [	{descripcion: "SIN INMUNIZACIÓN ANTITETÁNICA", 	 estatus: false, puntaje: 0.5, calificacion : 0, id: "EA_SIA"},
														{descripcion: "EMBARAZO MÚLTIPLE", 				 estatus: false, puntaje: 6,   calificacion : 0, id: "EA_EMB_MULT"} ] },			 	 
									];

	    $scope.tabExamenClinico = [ 	{ opciones :  [	{descripcion: "EXPLORACIÓN DE MAMAS ANORMAL", estatus: false, puntaje: 3, calificacion : 0, id: "EC_EXP_MAMAS"},
	    												{descripcion: "EXPLORACIÓN DE CÉRVIX ANORMAL",estatus: false, puntaje: 4, calificacion : 0, id: "EC_EXP_CERVIX"},
	    												{descripcion: "ALTURA UTERINA DISCORDANTE",	  estatus: false, puntaje: 4, calificacion : 0, id: "EC_AUD"},
	    												{descripcion: "FCF>160X'", 					  estatus: false, puntaje: 6, calificacion : 0, id: "EC_FCC_MAYOR"}
														 ] },

										{ opciones :  [	{descripcion: "EXPLORACIÓN DE PELVIS ANORMAL",estatus: false, puntaje: 6, calificacion : 0, id: "EC_EXP_LEVIS"},
														{descripcion: "T/A>130/90 mmhg", 			  estatus: false, puntaje: 6, calificacion : 0, id: "EC_TA"},
													 	{descripcion: "FCF<120X'", 					  estatus: false, puntaje: 6, calificacion : 0, id: "EC_FCC_MENOR"}																								
													 	] }										
									];
		
	    
	    /*console.log("Id del medico");
	    console.log(usuario.usuario.NP_EXPEDIENTE);*/
	    medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE) //
	    .success(function(data){
	    	$scope.medico = data;
	    	$scope.medico.NOMBRE = $scope.medico.C_PRIMER_APELLIDO + " " + $scope.medico.C_SEGUNDO_APELLIDO + " " + $scope.medico.C_NOMBRE;
	    	/*console.log("Datos del medico..");
	    	console.log($scope.medico);*/
	    })
	    .error(function(data){
        	mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      	});

  		pacientes.getPacienteNotaMedica()
      	.success(function(data){	        
	          $scope.paciente=data.DATOS_PACIENTE;
	          console.log("Datos del paciente");
	          console.log($scope.paciente);
	          var temp = $scope.paciente.NF_SEXO;
	          $scope.paciente.NF_SEXO = $scope.paciente.NF_SEXO_ID;
	          $scope.paciente.NF_SEXO_ID = temp;

	          temp = $scope.paciente.NF_ENTIDAD_FEDERATIVA;
	          $scope.paciente.NF_ENTIDAD_FEDERATIVA = $scope.paciente.NF_ENTIDAD_FEDERATIVA_ID;
	          $scope.paciente.CF_ENTIDAD_FEDERATIVA = temp;

	          temp = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO;
	          $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO_ID;
	          $scope.paciente.CF_ENTIDAD_DE_NACIMIENTO = temp;

	          temp = $scope.paciente.NF_ESTADO_CIVIL;
	          $scope.paciente.NF_ESTADO_CIVIL = $scope.paciente.NF_ESTADO_CIVIL_ID;
	          $scope.paciente.CF_ESTADO_CIVIL = temp;
	          $scope.paciente.C_NOMBRE = $scope.paciente.C_NOMBRE + " " + $scope.paciente.C_PRIMER_APELLIDO + " " + $scope.paciente.C_SEGUNDO_APELLIDO;

	          $scope.paciente.FECHAREGISTRO = moment().toDate();
	          $scope.paciente.EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
	          $scope.pacienteConNotaMedica = true;
	    })
      	.error(function(data){
      		if (data.hasOwnProperty('error')) {
      			mensajes.alerta('ERROR',data.error,'ACEPTAR!');
				$state.go('agendaConsultaPaciente',{state: 'riesgoObstetrico'});	
      		}
      		else		
        		mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');

        	$scope.pacienteConNotaMedica = false;
        	
      	}); 
    }    

	$scope.toggle = function (item) {
  		item.estatus = !item.estatus;
        if (item.estatus) {
        	$scope.N_CALIFICACION += parseFloat(item.puntaje);
        	item.calificacion = item.puntaje;
        }        	
        else {
        	$scope.N_CALIFICACION -= parseFloat(item.puntaje);  
        	item.calificacion = 0;
        }
        	
        
        if ($scope.N_CALIFICACION > 6) {
        	$scope.C_ESTATUS_ALTO_RIESGO = "X";
        	$scope.N_CLASIFICACION_ALTO_RIESGO = "yellow";
        	$scope.C_ESTATUS_BAJO_RIESGO = "";
        	$scope.N_CLASIFICACION_BAJO_RIESGO = "";
        	$scope.DESCIPCION_RIEGO = "ALTO RIESGO";
        } else {
        	$scope.C_ESTATUS_ALTO_RIESGO = "";
        	$scope.N_CLASIFICACION_ALTO_RIESGO = "";
        	$scope.C_ESTATUS_BAJO_RIESGO = "X";
        	$scope.N_CLASIFICACION_BAJO_RIESGO = "yellow";
        	$scope.DESCIPCION_RIEGO = "BAJO RIESGO";
        } 

    }

    $scope.registrar = function () {
    	$scope.data = {};
    	
    	$scope.data = _.extend({},
    		{ NF_PACIENTE 		: parseInt($scope.paciente.NP_EXPEDIENTE.substring(0,$scope.paciente.NP_EXPEDIENTE.length-5)),
    		 NF_PACIENTE_SIU 	: $scope.paciente.NP_EXPEDIENTE_SIU,
    		 NF_MEDICO   		: $scope.medico.N_EXPEDIENTE,
    		 N_PUNTAJE_TOTAL	: $scope.N_CALIFICACION,
    		 C_CALIFICACION		: $scope.DESCIPCION_RIEGO,
    		 C_ALTO_RIESGO      : $scope.C_ESTATUS_ALTO_RIESGO,
    		 C_BAJO_RIESGO      : $scope.C_ESTATUS_BAJO_RIESGO,
    		 HISTORIA_CLINICA   : [] });
    	
    	obtenOpciones($scope.tabHistorialClinica);
    	obtenOpciones($scope.tabAntecedentesPersonales);
    	obtenOpciones($scope.tabAntecedentesGinecoObst);
    	obtenOpciones($scope.tabEmbarazoActual);
    	obtenOpciones($scope.tabExamenClinico);    	
	    console.log("Datos a enviar..")
	    console.log($scope.data);    		    
	    peticiones.postMethodPDF(actionURL,$scope.data)
        .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800'); 
              $state.go('agendaConsultaPaciente',{state: 'riesgoObstetrico'});
        })
        .error(function(data){
              mensajes.alerta('ERROR','NO SE PUDO GENERAR EL DOCUMENTO','ACEPTAR');
        });
    }

    function obtenOpciones(tablaDeOpciones) {
    	var temp;
    	angular.forEach(tablaDeOpciones, function(tabla, index) {
    		angular.forEach(tabla.opciones, function(opciones, index) {
	    		$scope.data.HISTORIA_CLINICA.push({C_DESCRIPCION : opciones.descripcion, N_PUNTAJE : opciones.puntaje, N_CALIFICACION : opciones.calificacion,C_PARAMETRO : opciones.id});
	    	});	
	    });
    }

});
