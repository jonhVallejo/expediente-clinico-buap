(function(){
'use strict';  
/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultaReferenciactrlCtrl
 * @description
 * # ConsultaReferenciactrlCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('ReferenciactrlCtrl', ['$window','$localStorage','$scope','mensajes','$http','usuario','peticiones','$state','$q','$timeout','medicos','pacientes','reportes',
    function ($window,$localStorage,$scope,mensajes,$http,usuario,peticiones,$state,$q,$timeout,medicos,pacientes , reportes) {
   
    $scope.keyPriv=undefined;
    $scope.certificado=undefined;
    $scope.disabled = false;
    $scope.interconsulta=undefined;
    var idEspecialidad=undefined;
    var nomEspecialidad=undefined;
    $scope.showHistorico=false;
     $scope.estaFirmada=false;

    inicializar();
   
    $scope.showHistorico=true;
    $scope.historicoReferencia=[];
    $scope.sellar=false;
    $scope.listadoReferencias = false;

    /**********************INICIALIZAR VALORES PARA REFERENCIA*************************************/
      $scope.datos ={
        MOTIVO_ESPECIALIDAD:undefined,
        MOTIVO_SUPERVISOR:undefined,
        paciente:{
        nombre:undefined,
        ID: undefined,
        exp: undefined,
        edad: undefined,
        fNac: undefined,
        genero: undefined,
        curp:undefined,
        idexpediente:undefined,
        fSolicitaInter : new Date()
      },
      motivos:[
        {C_DESCRIPCION: 'SIN RESPUESTA AL TRATAMIENTO', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'COMPLICACIÓN', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'AUXILIARES DE DIAGNÓSTICO', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'RIESGO DE SECUELAS', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'COMPLEMENTACIÓN DIAGNÓSTICA', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'TRATAMIENTO ESPECIALIZADO', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'ESPECIALIDAD', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'UNIDAD QUE SOLICITA', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'TRATAMIENTO UTILIZADO', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'TIPO ORDINARIO-URGENTE', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'OTROS', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'INGESTIÓN CRÓNICA DE MEDICAMENTOS', C_VALOR:'',NP_ID_RESPUESTA:0}
      ],
      antecedentes:{
        C_ALERGIA: "",
        C_CANCER: "",
        C_CARDIOPATIA_ISQUEMICA: "",
        C_DIABETES: "",
        C_HIPERTENCION_ARTERIAL: "",
        C_OCUPACION: "",
        INGESTION:"",
        OTROS:""
      }
    };

    

    function inicializarRespuesta(){
       $scope.datos.motivos=[
        {C_DESCRIPCION: 'SIN RESPUESTA AL TRATAMIENTO', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'COMPLICACIÓN', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'AUXILIARES DE DIAGNÓSTICO', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'RIESGO DE SECUELAS', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'COMPLEMENTACIÓN DIAGNÓSTICA', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'TRATAMIENTO ESPECIALIZADO', C_VALOR:false,NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'ESPECIALIDAD', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'UNIDAD QUE SOLICITA', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'TRATAMIENTO UTILIZADO', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'TIPO ORDINARIO-URGENTE', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'OTROS', C_VALOR:'',NP_ID_RESPUESTA:0},
        {C_DESCRIPCION: 'INGESTIÓN CRÓNICA DE MEDICAMENTOS', C_VALOR:'',NP_ID_RESPUESTA:0}
      ];
      return $scope.datos.motivos;
    };


    function inicializar(){
      $scope.isFirmar=false;
      $scope.isEditar=false;
      $scope.isGuardar=false;
      $scope.isRechazar=false;
      $scope.isMotivoHU=false;
      $scope.isMotivoUMF=false;
      $scope.isUMF=false;// SI ES DE UMF COMO MÉDICO
      $scope.isSUMF=false;//SI ES SUPERVISOR DE UMF
      $scope.isHU=false;//SI ES UN ESPECIALISTA DE HU
    };



    function setDatosPaciente(nombre, id, expediente,f_nacimiento,genero,curp,idexpediente,expedienteSIU){
      console.log(f_nacimiento);
      $scope.datos.paciente.nombre=nombre;
      $scope.datos.paciente.ID=id;
      $scope.datos.paciente.exp=expediente;
      $scope.datos.paciente.edad=pacientes.calculaEdad(f_nacimiento);
      $scope.datos.paciente.fNac= f_nacimiento;
      if(f_nacimiento!=undefined)
          $scope.calcularEdad();
      $scope.datos.paciente.genero=genero;
      $scope.datos.paciente.curp=curp;
      $scope.datos.idexpediente=idexpediente;
      $scope.datos.expedienteSIU=expedienteSIU;       
    };
    $scope.calcularEdad =function(){
  //  $scope.datos.paciente.fNac = moment($scope.datos.paciente.fNac).format('DD/MM/YYYY');
        var values=$scope.datos.paciente.fNac.split("/");
        var dia = values[0];
        var mes = values[1];
        var ano = values[2];
        var fecha_hoy = new Date();
        var ahora_ano = fecha_hoy.getYear();
        var ahora_mes = fecha_hoy.getMonth()+1;
        var ahora_dia = fecha_hoy.getDate()-1+1;
        var edad = (ahora_ano + 1900) - ano;
        if(ahora_mes<mes){
          edad--;
        }
        else if(ahora_mes==mes && ahora_dia<dia){
          edad--;
        }
        $scope.datos.paciente.edad=edad +' AÑO(S)';
        if(edad<0){
         // mensajes.alerta('AVISO','NO PUEDES PONER UNA FECHA MAYOR AL DÍA DE HOY','ACEPTAR!');
          $scope.datos.paciente.fNac = undefined;
          $scope.datos.paciente.edad = undefined;
        }
        else if(edad === 0){
          if(ahora_dia-dia > 0){
            $scope.datos.paciente.edad =  mes-ahora_mes +' MES(ES)';
          }
          else{
            $scope.datos.paciente.edad =mes- ahora_mes - 1 +' MES(ES)'; 
          }
        }
    }

    var notaReferenciaList = function(){
      var url = "referenciaNotaMedica/0/0";
      peticiones.getDatos(url).then(function(data){
          $scope.referenciasList = data;
      }).catch(function(data){
        console.log(data);
      })
    };


    function verificaProcedencia(){
      if($state.current.name =="consultaReferenciaList"){
         getEspecialidad().then(function(data){
            getPerfiles().then(function(data){
             notaReferenciaList();
            });
         });
        
      }else{ 
        console.log($localStorage);
        setDatosPaciente($localStorage.paciente.nombrePaciente,
                          $localStorage.paciente.idPaciente,//H O D
                          $localStorage.paciente.expediente,//con el año
                          $localStorage.paciente.f_nacimiento,
                          $localStorage.paciente.genero,
                          $localStorage.paciente.curp,
                          $localStorage.paciente.idexpediente,//sin el año
                          $localStorage.paciente.expedienteSIU
                          );
        $scope.listadoReferencias = true;

         getEspecialidad().then(function(data){
            getPerfiles().then(function(data){
              obtenerHistorico();
            });
         });
      }
    };

    verificaProcedencia();

    $scope.onOrderChange = function(page, limit) {
        var deferred = $q.defer();
        $timeout(function () {
          deferred.resolve();
        }, 2000); 
        return deferred.promise;
    };

     $scope.query = {
        order: 'fecha',
        limit: 15,
        page: 1
    };
    $scope.queryHistorico = {
        order: 'fecha',
        limit: 15,
        page: 1
    };
    $scope.queryInterpretacion = {
        order: 'fecha',
        limit: 5,
        page: 1
    };
    $scope.change = function(){
          $scope.query.page = 1;
    };
    $scope.ngChangeReferencias = function(){
          $scope.queryHistorico.page = 1;
    };

    $scope.onOrderChangeHistorico = function(page, limit) {
        var deferred = $q.defer();
        $timeout(function () {
          deferred.resolve();
        }, 2000); 
        return deferred.promise;
    };
     $scope.onOrderInterpretacion = function(page, limit) {
        var deferred = $q.defer();
        $timeout(function () {
          deferred.resolve();
        }, 2000); 
        return deferred.promise;
    };

    /*********************************FUNCIONES NECESARIAS PARA OBTENER REFERENCIA****************************/
    medicos.getEspecialidades().success(function(data){
       $scope.interconsulta = data;
    }).catch(function(err){
      mensajes.alerta("ERROR","NO SE PUEDEN RECUPERAR LOS DATOS DE INTERCONSULTA","ACEPTAR");
    });

    function getEspecialidad(){
      var defered = $q.defer();
      var promise = defered.promise;
        medicos.getMedicoActual($localStorage.usuario.NP_EXPEDIENTE)
       .success(function(data){
           if(data.hasOwnProperty("ESPECIALIDADES")){
             idEspecialidad = data.ESPECIALIDADES.NF_ESPECIALIDAD;
             nomEspecialidad=data.ESPECIALIDADES.CF_ESPECIALIDAD;
             defered.resolve();
           }
       })
       .error(function(data){
          console.log(data);
          defered.reject();
       });

      return promise;
   };


   function getPerfiles(){
    var defered = $q.defer();
    var promise = defered.promise;
    var perfil=undefined;
    var nombreEspecialidad= (nomEspecialidad=="UNIDAD MEDICO FAMILIAR 1 (UMF)" || nomEspecialidad=="UNIDAD MEDICO FAMILIAR 2 (UMF)");
    if(usuario.perfil.clave=="MEDICO" && nombreEspecialidad==true){
        perfil="UMF";
    }else{
      perfil=usuario.perfil.clave;
    }
     switch(perfil){
          case "UMF": 
                $scope.isEditar = true;
                $scope.isGuardar=true;
                $scope.nomPerfil="UMF";
                $scope.isMotivo = true;
                $scope.isMotivoUMF=true;
                $scope.isUMF=true;
                defered.resolve();
                break;
          case "SUMF": 
                $scope.isEditar = false;
                $scope.isGuardar=false;
                $scope.isFirmar=true;
                $scope.isRechazar=true;
                $scope.isMotivo = false;
                $scope.nomPerfil="SUMF";
                $scope.isMotivoUMF=true;
                $scope.isSUMF=true;
                defered.resolve();
                break;
          case "MEDICO": 
                $scope.isEditar = true;
                $scope.isGuardar=true;
                $scope.isFirmar=false;
                $scope.nomPerfil="ESPECIALIDAD";
                $scope.isMotivoHU=true;
                $scope.isHU=true;
                defered.resolve();
                break;
          // case "MEDICO": 
          //       $scope.isEditar = true;
          //       $scope.isGuardar=true;
          //       $scope.isFirmar=false;
          //       $scope.isMotivo = true;
          //       $scope.isUMF=true;
          //       $scope.isSUMF=true;
          //       $scope.isMotivo = false;
          //       $scope.nomPerfil="ESPECIALIDAD";
          //       $scope.isMotivoHU=true;
          //       $scope.isHU=true;
          //       defered.resolve();
          //       break;

      } 
    return promise;
   };

  
 
    function obtenerHistorico(){
      var nombreEspecialidad= (nomEspecialidad=="UNIDAD MEDICO FAMILIAR 1 (UMF)" || nomEspecialidad=="UNIDAD MEDICO FAMILIAR 2 (UMF)");
      if((usuario.perfil.clave=="MEDICO" && nombreEspecialidad)   || usuario.perfil.clave=="SUMF")
          var url="referenciaNotaMedica/"+ $scope.datos.idexpediente+"/"+0;
      else
         if(usuario.perfil.clave=="MEDICO")
         // var url="referenciaNotaMedica/"+0+"/"+idEspecialidad;
          //SI SE REQUIERE USAR UMF DESCOMENTAR LA SIGUIENTE LÍNEA y por parte del back modificar esta petición
          var url="referenciaNotaMedica/"+ $scope.datos.idexpediente+"/"+idEspecialidad;
         else{
           mensajes.alerta('ALERTA','VERIFIQUE SU PERFIL','ACEPTAR');
           return;
         }
      console.log(url);
      console.log($scope.datos.idexpediente);
      console.log($scope.datos.exp);
       peticiones.getDatos(url)
            .then(function(data) {
              $scope.showHistorico=true;
              $scope.historicoReferencia=data;
            })
            .catch(function(err) {
              console.log(err);
            });
        
    };
    
    function getIndice (datos,descripcion) {
      var indx=0;
      _.find(datos, function(item, index) {
          if (item.C_DESCRIPCION === descripcion) {
              indx=index;
          }
        });
      return indx;
    };

    function getDatosFijos(data){
      var f = data.DIAGNOSTICO.T_FECHA_CMD_NOTAMEDICA.split(" ")[0].split("/");
              $scope.datos.notamedica = data.FOLIO_NOTAMEDICA.NP_ID_NOTAMEDICA;
              $scope.datos.estableceDiagnostico = new Date(f[1]+"/"+f[0]+"/"+f[2]);
              $scope.datos.diagnostico = data.DIAGNOSTICO.CDESCRIPCION_CIE10.toUpperCase();
              $scope.datos.estadoActual = data.ANTECEDENTES_HEREDOFAMILIARES;
              $scope.datos.estadoActual = data.ESTADO_ACTUAL_PACIENTE;
              $scope.datos.auxiliares = data.AUXILIARES_DIAGNOSTICO;
              $scope.datos.C_PADECIMIENTO_ACTUAL = data.PADECIMIENTO_ACTUAL.C_PADECIMIENTO_ACTUAL;
              $scope.datos.NP_ID_REFERENCIA_NOTAMEDICA = data.FOLIO_REFERENCIA_NOTAMEDICA.NP_ID_REFERENCIA_NOTAMEDICA;
              $scope.datos.antecedentes.C_ALERGIA = data.ANTECEDENTES_HEREDOFAMILIARES.C_ALERGIA;
              $scope.datos.antecedentes.C_CANCER = data.ANTECEDENTES_HEREDOFAMILIARES.C_CANCER;
              $scope.datos.antecedentes.C_CARDIOPATIA_ISQUEMICA = data.ANTECEDENTES_HEREDOFAMILIARES.C_CARDIOPATIA_ISQUEMICA;
              $scope.datos.antecedentes.C_DIABETES = data.ANTECEDENTES_HEREDOFAMILIARES.C_DIABETES;
              $scope.datos.antecedentes.C_HIPERTENCION_ARTERIAL = data.ANTECEDENTES_HEREDOFAMILIARES.C_HIPERTENCION_ARTERIAL;
              $scope.datos.antecedentes.C_OCUPACION = data.ANTECEDENTES_HEREDOFAMILIARES.C_OCUPACION;
              $scope.datos.antecedentes.INGESTION = data.INGESTION;
    };

    function getNota(registro){

      var url='referenciaNotaMedica/' + registro.NP_ID_REFERENCIA;
       peticiones.getDatos(url)
            .then(function(data) {
              $scope.showHistorico=false;
               getDatosFijos(data);
                $scope.datos.MOTIVO_ESPECIALIDAD=registro.C_MOTIVO_ESP;
                $scope.datos.MOTIVO_SUPERVISOR=registro.C_MOTIVO_SUPER;
                if(data.FOLIO_REFERENCIA_NOTAMEDICA.NP_ID_REFERENCIA_NOTAMEDICA !== 0){ 
                  var index=0;
                   _.each(data.RESPUESTAS, function(i) {
                      index=getIndice($scope.datos.motivos,i.C_DESCRIPCION);
                      $scope.datos.motivos[index].C_VALOR=i.C_VALOR;
                      $scope.datos.motivos[index].NP_ID_RESPUESTA=i.NP_ID_RESPUESTA;
                    });
                    //Validar que estatus tiene la Referencia
                  $scope.datos.NP_ID_REFERENCIA_NOTAMEDICA=registro.NP_ID_REFERENCIA;
                }
            })
            .catch(function(err) {
              console.log(err);
              mensajes.infoNoCargada();
            });
    };

    /*********************FUNCIONES PARA REFERENCIA********************************/
    $scope.cancelar = function(){
      peticiones.deleteDatos('referenciaNotaMedica/'+$scope.datos.NP_ID_REFERENCIA_NOTAMEDICA)
      .then(function(data) {
        mensajes.alerta('OPERACIÓN EXITOSA','LA NOTA DE REFERENCIA HA SIDO CANCELADA','ACEPTAR');
      })
      .catch(function(err) {
        mensajes.alerta('ERROR','SE HA PRODUCIDO UN ERROR AL TRATAR DE CANCELAR LA NOTA DE REFERENCIA','ACEPTAR');
      });
    };

    $scope.clickNota=function(registro){;
      if(registro.C_FIRMA!==""){
          $scope.estaFirmada=true;
          $scope.isFirmar=false;
          $scope.isEditar=false;
      }
      if($state.current.name =="consultaReferenciaList"){
        //cargar datos con el expediente
         pacientes.getPacientes(registro.N_EXPEDIENTE)
           .success(function(data){
            console.log(data);
            setDatosPaciente(
              data.data[0].C_NOMBRE+""+data.data[0].C_PRIMER_APELLIDO + " " + data.data[0].C_SEGUNDO_APELLIDO,
              data.data[0].NP_ID,
              data.data[0].NP_EXPEDIENTE,
              data.data[0].F_FECHA_DE_NACIMIENTO,
              data.data[0].NF_SEXO,
              data.data[0].C_CURP,
              data.data[0].NP_EXPEDIENTE.indexOf('/') ? data.data[0].NP_EXPEDIENTE.split("/")[0]: data.data[0].NP_EXPEDIENTE,
              data.data[0].NP_EXPEDIENTE_SIU
            )
            getNota(registro);
           }).catch(function(error){

           });

      }else{
         getNota(registro);
      }
      $scope.listadoReferencias=true;

    };
    $scope.creatNota=function(){
        $scope.showHistorico=false;
        $scope.isGuardar=true;
        post();
    };

    function post(idReferencia){
      var datos = {};
      datos.respuestas = inicializarRespuesta();
      datos.idDoctor =parseInt(usuario.usuario.NP_EXPEDIENTE);
      datos.clavePerfil=$scope.nomPerfil;
      datos.ingestion=$scope.datos.antecedentes.INGESTION;
      datos.otros=$scope.datos.antecedentes.OTROS;
      ///referenciaNotaMedica/{idPaciente}/{idEspecialidad}
      var url='referenciaNotaMedica/'+$localStorage.paciente.idexpediente+"/"+6;
      peticiones.postDatos(url,datos)
        .then(function(data) {
          var index=0;
         _.each(data.RESPUESTAS, function(i) {
                  index=getIndice($scope.datos.motivos,i.C_DESCRIPCION);
                  $scope.datos.motivos[index].NP_ID_RESPUESTA=i.NP_ID_RESPUESTA;
                });
          getDatosFijos(data);
        })
        .catch(function(err) {
          console.log(err);
          mensajes.alerta('ALERTA',err.error.toUpperCase(),'ACEPTAR');
          $scope.regresar();

        });  
    };

    $scope.rechazar=function(){
      if($scope.datos.motivos[6].C_VALOR==''){
        mensajes.alerta('ALERTA','NO SE PUEDE RECHAZAR YA QUE AÚN NO SE CUENTA CON LA ESPECIALIDAD','ACEPTAR');
           return;
      }
       if($scope.datos.MOTIVO_SUPERVISOR==undefined || $scope.datos.MOTIVO_SUPERVISOR==""){
           mensajes.confirmacion('ALERTA','INGRESE EL MOTIVO POR EL CUAL LA REFERENCIA HA SIDO RECHAZADA','ACEPTAR')
                .then(function() {
                  document.getElementById("MOTIVO_SUPERVISOR").focus();
                }).catch(function() {
                  
                });
          
           return;
      }
      var datos = {};
      datos.MOTIVO_SUPERVISOR=  $scope.datos.MOTIVO_SUPERVISOR;
      var url='referenciaNotaMedica/'+$scope.datos.NP_ID_REFERENCIA_NOTAMEDICA+"/"+$scope.datos.motivos[6].C_VALOR;
       peticiones.putMethod(url,datos)
       .success(function(data){
             mensajes.alerta('ALERTA','LA REFERENCIA HA SIDO RECHAZADA CORRECTAMENTE','ACEPTAR');
             $state.go("bienvenido");
          })
          .error(function(data){
            console.log(data);
          });

    };
  
    $scope.guardar = function(){
      //DESCOMENTAR CUANDO SE USE UMF
      if($scope.isSUMF){
          if($scope.datos.MOTIVO_SUPERVISOR==undefined)
              mensajes.alerta('ALERTA','INGRESE EL MOTIVO DE RECHAZO','ACEPTAR');
              return;
      }
      var datos = {};
      for(var i=6 ; i<$scope.datos.motivos.length ; i++){
        if(($scope.datos.motivos[i].C_VALOR === '')){
          mensajes.alerta('ALERTA','POR FAVOR VERIFICA EL CAMPO "'+$scope.datos.motivos[i].C_DESCRIPCION+'"','ACEPTAR');
          return;
        }
      }
      datos.respuestas = $scope.datos.motivos;
      datos.idDoctor = parseInt(usuario.usuario.NP_EXPEDIENTE);
      datos.clavePerfil=$scope.nomPerfil;
      datos.MOTIVO_ESPECIALIDAD =  $scope.datos.MOTIVO_ESPECIALIDAD;
      datos.MOTIVO_SUPERVISOR =  $scope.datos.MOTIVO_SUPERVISOR;
      datos.ID_ESPECIALIDAD = idEspecialidad;
    //  datos.ingestion=$scope.datos.antecedentes.INGESTION;
         // /referenciaNotaMedica/{idReferenciaMedica}/{idEspecialidad}
     var url='referenciaNotaMedica/'+$scope.datos.NP_ID_REFERENCIA_NOTAMEDICA+"/"+$scope.datos.motivos[6].C_VALOR;
     peticiones.putMethod(url,datos)
     .success(function(data){
         mensajes.alerta('ALERTA','NOTA DE REFERENCIA GUARDADA EN ESPERA DE QUE EL SUPERVISOR LO AUTORICE','ACEPTAR');
         //descomentar cuando se usa UMF
        // $scope.regresar();
        //comentar cuando se use UMF
        //si ya esta firmada ya no mandar el showFirma
         if($scope.nomPerfil=="UMF"){
            $scope.regresar();
         }
         if($scope.nomPerfil=="ESPECIALIDAD"){
            if(!$scope.estaFirmada){
             $scope.showFirma();
             $scope.isFirmar=true;
            }
         }
        })
        .error(function(data){
          console.log(data);
           mensajes.alerta('ALERTA',data.error.toUpperCase(),'ACEPTAR');
        });
    };

    $scope.getEstatusReferencia=function(estatus){
      var texto=" ";
        switch(estatus){
          case 0:
              texto="CANCELADA";
              return texto;
          case 1:
              texto="PENDIENTE";
              return texto;
          case 2:
              texto="FINALIZADA";
              return texto;
          case 3:
              texto="RECHAZADA";
              return texto;
          default:
             return " ";
        }

    };

    /****************FUNCIONES PARA LA FIRMA********************************/
    $scope.sellar=function(){
      var datos = {};
      datos.firma=[];
      datos.firma.push({keyPriv:$scope.keyPriv, certificado:$scope.certificado});
      datos.idDoctor = parseInt(usuario.usuario.NP_EXPEDIENTE);
      datos.respuestas = $scope.datos.motivos;
      datos.paciente=$scope.datos.paciente;
      //VALIDAR QUE SEA EL SUMF
      if($scope.isSUMF || $scope.isHU){
      if($scope.keyPriv!==undefined && $scope.certificado!==undefined){
          peticiones.putMethodPDF('referenciaNotaMedica/firmar/'+$scope.datos.NP_ID_REFERENCIA_NOTAMEDICA,datos)
          .success(function(data) {
            $scope.firmar=false;
            var file = new Blob([data], {type: 'application/pdf'});
            var fileURL = URL.createObjectURL(file);
            reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
            mensajes.alerta('ALERTA','SE FIRMO CORRECTAMENTE','ACEPTAR');
            $scope.keyPriv="";
            $scope.certificado="";
            $scope.regresar();
          })
          .error(function(err) {
            mensajes.alerta('ERROR','VERIFIQUE EL CERTIFICADO Y LA LLAVE YA QUE NO SE PUDO FIRMAR LA NOTA DE REFERENCIA','ACEPTAR');
          });
        } else{
           mensajes.alerta('PRECAUCIÓN','INGRESE LA LLAVE Y CERTIFICADO','ACEPTAR');
        }
      }
      else{
        mensajes.alerta('ERROR','NO TIENE PERMISO PARA FIRMAR','ACEPTAR');
      }
    };

    $scope.cargaLlave = function (fileContent){
      $scope.keyPriv=fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
      $scope.certificado=fileContent;
    };

    $scope.showFirma=function(){
      $scope.firmar=true;
    };

    $scope.cancelar=function(){
       $scope.firmar=false;
       $scope.showHistorico=false;
       $scope.datos.MOTIVO_ESPECIALIDAD=undefined;
       $scope.datos.MOTIVO_SUPERVISOR=undefined;
    };

    $scope.regresar=function(){
       inicializar();
       $scope.firmar=false;
       $scope.keyPriv="";
       $scope.certificado="";
       $scope.datos.MOTIVO_ESPECIALIDAD=undefined;
       $scope.datos.MOTIVO_SUPERVISOR=undefined;
      if($state.current.name =="consultaReferenciaList"){
        notaReferenciaList();
        $scope.listadoReferencias = false;
      }else{
        $scope.listadoReferencias = true;
        getEspecialidad();
        $scope.showHistorico=true;
        obtenerHistorico();
      }
      
       getPerfiles();
       $state.go("bienvenido");

    };

  }]);

})();