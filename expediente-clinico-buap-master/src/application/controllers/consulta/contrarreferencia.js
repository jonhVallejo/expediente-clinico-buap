(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultaContrarreferenciactrlCtrl
 * @description
 * # ConsultaContrarreferenciactrlCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('ContrarreferenciactrlCtrl',['$scope','$log','$localStorage','$http','mensajes','peticiones','usuario','$window','pacientes','reportes',
    function ($scope,$log,$localStorage,$http,mensajes,peticiones,usuario,$window,pacientes,reportes) {
    var tabs = [
          { title: 'One', content: "Tabs will become paginated if there isn't enough room for them."},
          { title: 'Two', content: "You can swipe left and right on a mobile device to change tabs."},
          { title: 'Three', content: "You can bind the selected tab via the selected attribute on the md-tabs element."},
        ],
        selected = null,
        previous = null;
    var self = this;
    $scope.tabs = tabs;
    $scope.selectedIndex = 0;
    $scope.datosSecundarios = [];
    $scope.datos = '';
    $scope.selectedMedicamentos = [];
    $scope.minDateAgenda = new Date();
    /*$scope.datos ={
      nombre: $localStorage.paciente.nombrePaciente,

      exp: $localStorage.paciente.expediente,
      fNac: $localStorage.paciente.f_nacimiento,
      genero: $localStorage.paciente.genero,
      edad: calcularEdad(),
      certificado : undefined,
      keyPriv : undefined,
      H: $localStorage.paciente.idPaciente,
      id : $localStorage.paciente.idexpediente
    };*/



    self.getNotaReferenciaList = function(url){
        peticiones.getDatos(url)
          .then(function(data) {
            console.log(data);
          })
          .catch(function(err) {
            console.log(err);
          });
    };

   // self.getNotaReferenciaList();




    $scope.$watch('selectedIndex', function(current, old){
      previous = selected;
      selected = tabs[current];
    });

    // var calcularEdad = function(){
    //   var nac = $localStorage.paciente.f_nacimiento;
    //   var actual = moment(new Date()).format('DD/MM/YYYY');
    //   var anhos = actual.split("/")[2] - nac.split("/")[2] -1;
    //   var complement = " A\u00D1OS";
    //   if((actual.split("/")[1] > nac.split("/")[1])){
    //     anhos += 1;
    //   }
    //   if(anhos<1){
    //     complement = " MESES";
    //     anhos = actual.split("/")[0] - nac.split("/")[0] -1;
    //     if((actual.split("/")[0] > nac.split("/")[0])){
    //       anhos += 1;
    //     }
    //     if(anhos<1){
    //       complement = " D\u00CDAS";
    //       anhos = actual.split("/")[0] - nac.split("/")[0];
    //       if(anhos===1){
    //         complement = " D\u00CDA";
    //       }
    //     }else{
    //       if(anhos===1){
    //         complement = " MES";
    //        }
    //     }
    //   }else{
    //      if(anhos===1){
    //       complement = " A\u00D1O";
    //      }
    //   }
    //   return anhos+complement;
    // };

    




    $scope.datos ={
      REFERENCIA : [
        {
          C_VALOR: $localStorage.paciente.f_nacimiento,
          C_PARAMETRO: "fechaNacimientoPaciente"
        },
        {
          C_VALOR: $localStorage.paciente.idPaciente,
          C_PARAMETRO: "idPaciente"
        },
        {
          C_VALOR: "",
          C_PARAMETRO: "diagPrinciCie10"
        },
        {
          C_VALOR: $localStorage.paciente.nombrePaciente,
          C_PARAMETRO: "nombrePaciente"
        },
        {
          C_VALOR: $localStorage.paciente.idexpediente,
          C_PARAMETRO: "noExpediente"
        },
        {
          C_VALOR: $localStorage.paciente.genero,
          C_PARAMETRO: "genero"
        },
        {
          C_VALOR: pacientes.calculaEdad($localStorage.paciente.f_nacimiento),
          C_PARAMETRO: "edad"
        },
        {
          C_VALOR: $localStorage.paciente.curp,
          otro: $localStorage.paciente.curp,
          C_PARAMETRO: "curp"
        },
        {
          C_VALOR: usuario.usuario.NOMBRE_COMPLETO,
          C_PARAMETRO: "nombreMedico"
        },
        {
          C_VALOR: usuario.usuario.CEDULA,
          C_PARAMETRO: "cedula"
        },
        {
          C_VALOR: usuario.usuario.NP_EXPEDIENTE,
          C_PARAMETRO: "especialidad"
        }
      ],
      id : usuario.usuario.NP_EXPEDIENTE,
      folioUltimaNotaMedica : $scope.datos.notamedica,
      certificado : undefined,
      keyPriv : undefined,
      OBSERVACIONES:[
        {
          C_VALOR: 0,
          C_PARAMETRO: "numDias"
        },
        {
          C_VALOR: "",
          C_PARAMETRO: "valoracionSeguimiento"
        },
        {
          C_VALOR: 0,
          C_PARAMETRO: "folioIncapacidad"
        },
        {
          C_VALOR: "",
          C_PARAMETRO: "observacionesRecomendaciones"
        },
        {
          C_VALOR: "",
          C_PARAMETRO: "nuevaCita"
        }
      ],
      DIAGNOSTICO_SECUNDARIO : [],
      interrogatorio: [
        {descripcion:"ALGO", valor:'ALGO 2'},
        {descripcion:"OTRA COSA", valor:'OTRA COSA 2'},
      ]
    };

    console.log($localStorage.usuario.NP_EXPEDIENTE);

    $scope.getUltimaNotaMedica = function(){
      peticiones.getDatosStatus('contraReferenciaNotaMedica/' + $localStorage.paciente.idexpediente + "/" + $localStorage.usuario.NP_EXPEDIENTE)
      .then(function(data) {
console.log("********");
console.log(data)

        if(data.status == 200){
            $scope.datos.notamedica = data.data.NP_NOTA_MEDICA;
            //$scope.datos.datos.NP_NOTA_MEDICA = data.NP_NOTA_MEDICA;
            //$scope.datos.diagnosticoPrincipal = data.DIAGNOSTICO_PRINCIPAL.toUpperCase();
            $scope.datos.REFERENCIA[2].C_VALOR = data.data.DIAGNOSTICO_PRINCIPAL.toUpperCase();
            for(var i=0 ; i<data.data.DIAGNOSTICO_SECUNDARIO.length ; i++){
              $scope.datos.DIAGNOSTICO_SECUNDARIO.push(data.data.DIAGNOSTICO_SECUNDARIO[i].C_DESCRIPCION.toUpperCase());
            }
            getReceta(data.data.NP_NOTA_MEDICA);
        }else{
          if(data.status == 204){
              mensajes.alerta("AVISO","EL PACIENTE NO CUENTA CON NOTAS MEDICAS","ACEPTAR");
          }
        }
      })
      .catch(function(err) {
        console.log(err);
        mensajes.alerta("ERROR","OCURRIO UN ERROR INTERNO EN EL SISTEMA, POR FAVOR CONTACTE AL ADMINISTRADOR","ACEPTAR");
      });
    };



    function getReceta (id){
      peticiones.getDatos('receta/notaMedica/' + id)
        .then(function(data){
          $scope.selectedMedicamentos = data.MEDICAMENTOS;
          $scope.datos.TRATAMIENTO_FARMACOLOGICO = [];
          for(var j=0 ; j<$scope.selectedMedicamentos.length ; j++){
            var dates = [];
            var aux = [];
            var cadenaDates="";
            for(var i = 0; i < $scope.selectedMedicamentos[j].N_MESES ; i++){
              var auxDate = moment().add(30 * i, 'days').format('DD/MM/YYYY');
              cadenaDates += "1("+auxDate+") ";
              dates.push(auxDate);
            }
            aux.push({C_VALOR: $scope.selectedMedicamentos[j].C_INDICACIONES,C_PARAMETRO: "indicaciones"});
            aux.push({C_VALOR: $scope.selectedMedicamentos[j].C_NOMBRE_MEDICAMENTO,C_PARAMETRO: "nombreGenerico"});
            aux.push({C_VALOR: String($scope.selectedMedicamentos[j].N_DIAS),C_PARAMETRO: "dias"});
            aux.push({C_VALOR: String($scope.selectedMedicamentos[j].N_SAEC),C_PARAMETRO: "saec"});
            aux.push({C_VALOR: cadenaDates,C_PARAMETRO: "fechas"});
            aux.push({C_VALOR: String($scope.selectedMedicamentos[j].C_CANTIDAD),C_PARAMETRO: "cantidad"});
            aux.push({C_VALOR: String($scope.selectedMedicamentos[j].N_MESES),C_PARAMETRO: "meses"});
            $scope.selectedMedicamentos[j].fechasSurtimiento = dates;
            $scope.datos.TRATAMIENTO_FARMACOLOGICO.push(aux);
          }
        })
        .catch(function(data){
          mensajes.alerta("ERROR","NO SE PUEDEN OBTENER LOS MEDICAMENTOS RECETADOS","ACEPTAR");
        });
    };

    $scope.siguiente = function(index){
      $scope.selectedIndex = index;
    };

    $scope.cargaLlave = function (fileContent){
      $scope.datos.keyPriv = fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
      $scope.datos.certificado = fileContent;
    };

    $scope.enviar = function(){
      var aux = JSON.parse(JSON.stringify($scope.datos));
      //var aux = angular.extend({},$scope.datos);
      for(var i=0 ; i<aux.OBSERVACIONES.length-1 ; i++){
        aux.OBSERVACIONES[i].C_VALOR = String(aux.OBSERVACIONES[i].C_VALOR);
      }
      var date = moment(aux.OBSERVACIONES[4].C_VALOR).format('L').split("/");
      aux.OBSERVACIONES[4].C_VALOR = date[1]+"/"+date[0]+"/"+date[2];
      if($scope.datos.keyPriv!==undefined && $scope.datos.certificado!==undefined){
        peticiones.postMethodPDF("contraReferenciaNotaMedica/",aux)
        .success(function(data) {
          var file = new Blob([data], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
        })
        .error(function(data) {
          var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
          console.log(decodedString);
           if (!data.hasOwnProperty('error')) {
                var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                var obj = JSON.parse(decodedString);
                console.log(obj);
                if(obj.error == "FALTA_REFERENCIA"){
                    mensajes.alerta('ERROR',"EL PACIENTE NO CUENTA CON UNA REFERENCIA",'ACEPTAR');
                }
                
              } else
                mensajes.alerta('ERROR','NO SE PUDO GENERAR EL DOCUMENTO','ACEPTAR');
        });
      } else{
         mensajes.alerta('ALERTA','INGRESE LA LLAVE Y CERTIFICADO','ACEPTAR');
      }
    };


/**
 * Inicio de la nueva parte para contra referencia
 */


    self.crearContraReferencia = function(){

    };




    /**
     * [columns Sección para las cabeceras de las tablas]
     * @type {Array}
     */

      $scope.query = {
        order: 'cFecha',
        limit: 5,
        page: 1
      };

      $scope.columns = [ {
            name: 'FECHA',
            orderBy: 'cFecha',
            width : '20%'
        },{
          name: 'FOLIO NOTA',
            orderBy: 'NP_FOLIO',
            width : '20%'
      },{
          descendFirst: true,
          name: 'DIAGNÓSTICO PRE-OPERATORIO',
            orderBy: 'cDescripcion',
            width : '50%'
        },

      {
          name: 'VER/EDITAR',
            orderBy: 'NP_FOLIO',
            width : '10%'
      }];





  }]);







})();
