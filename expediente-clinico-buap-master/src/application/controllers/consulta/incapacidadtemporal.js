(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultaIncapacidadtemporalCtrl
 * @description
 * # ConsultaIncapacidadtemporalCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('ConsultaIncapacidadTemporalCtrl', ['$scope', 'pacientes', 'medicos', 'mensajes', 'peticiones', '$window', '$state' , 'consultas', '$localStorage','reportes', function ($scope, pacientes, medicos, mensajes, peticiones, $window, $state , consultas, $localStorage,reportes) {
    $scope.F_ACTUAL = moment().toDate();
    
    $scope.F_INFERIOR =  ((moment().subtract(7,'days').calendar()));
    var fecha = $scope.F_INFERIOR.split("/");
    $scope.F_INFERIOR = new Date(fecha[2],fecha[1]-1,fecha[0]);
    
    $scope.fe = {
      FIRMA : {},
    };


  	
    $scope.obtenInformacion = function () {
      consultas.getIncapacidadTemporal($localStorage.paciente.expediente)
      .success(function(data){
        pacientes.getPaciente()
        .success(function(data){
          if(!data.data.hasOwnProperty('NP_ID')){        
              $scope.paciente=data.data[0];
              console.log($scope.paciente);
              $scope.paciente.C_NOMBREPACIENTE = $scope.paciente.C_PRIMER_APELLIDO+" "+$scope.paciente.C_SEGUNDO_APELLIDO+" "+$scope.paciente.C_NOMBRE;
              $scope.paciente.NP_PLAZA = $scope.paciente.NP_ID;
              
              if ($scope.paciente.hasOwnProperty('C_DIAGNOSTICO_MEDICO')) {
                $scope.medico = usuario;
                $scope.catTipoIncapacidad = [];
                $scope.paciente.F_EXPEDICION = $scope.F_ACTUAL;

                pacientes.getCatalogo("37").success(function(data){
                  $scope.catTipoIncapacidad=data;
                  $scope.catTipoIncapacidad2=data;
                  console.log("Catalogo de Tipos de Incapacidad");
                  console.log(data);
                });                
              }
              else {
                mensajes.alerta('ERROR','EL PACIENTE NO TIENE NOTA MEDICA O NO ESTA FIRMADA','ACEPTAR!');            
                $state.go('agendaConsultaPaciente',{state: 'incapacidadTemporal'});
              }

          }
          else
            mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');        
          })
        .error(function(data){
          mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
        });
      })
      .error(function(err){
        mensajes.confirmacion("ERROR",err.error.toUpperCase(),"ACEPTAR")
        .then(function() {
          $state.go('bienvenido');
        }).catch(function() {
          $state.go('bienvenido');
        });
      });
    	
    }

    $scope.dias = function() {
      $scope.paciente.C_DIAS_INCAPACIDAD = "";
      
      if ($scope.paciente.N_DIAS_INCAPACIDAD != undefined) {
        var numDias = parseInt($scope.paciente.N_DIAS_INCAPACIDAD);

        if (numDias>=0 && numDias<11)  
          $scope.paciente.C_DIAS_INCAPACIDAD = unidades(numDias);
        else        
            if (numDias<100 && numDias<=48)
              $scope.paciente.C_DIAS_INCAPACIDAD = decenas(numDias);              
            else {
              //$scope.paciente.C_DIAS_INCAPACIDAD = centenas(numDias);        
              mensajes.confirmacion("ERROR","EL NÚMERO DE DÍAS NO PUEDE SER MAYOR A 48","ACEPTAR")
              $scope.paciente.N_DIAS_INCAPACIDAD = "";
            }
      }
    }       

    function unidades(n) {
      var unidad = ["CERO", "UNO", "DOS" ,"TRES" ,"CUATRO" ,"CINCO" ,"SEIS" ,"SIETE" ,"OCHO" ,"NUEVE", "DIEZ"];  
      if (n == 0)
        return "";
      else
        return unidad[n];
    }

    function decenas(n) {
      var decena     = ["VEINTE", "TREINTA","CUARENTA","CINCUENTA", "SESENTA","SETENTA", "OCHENTA", "NOVENTA"]; 
      var especiales = ["ONCE", "DOCE","TRECE","CATORCE", "QUINCE", "DIEZCISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE"];  
      
      if (n < 20)
        return especiales[n-11];
      else {
        var unid = n % 10;  
        var dec = parseInt(n/10);  

        if(unid == 0)  
           return decena[dec-2];                  
        else  
          if (dec == 2)
            return "VEINTI" + unidades(unid);
          else
            return decena[dec-2] + " Y " + unidades(unid);        
      }
    }   

    function centenas(n) {
      var dec = n % 100;
      var cen = parseInt(n/100);
      
      if (dec == 0 && cen == 1)
        return "CIEN";
      else {
        var centena = ["CIENTO", "DOCIENTOS", "TRECIENTOS","CUATROCIENTOS","QUINIENTOS","SEISCIENTOS","SETECIENTOS","OCHOCIENTOS", "NOVECIENTOS"];
        if (dec < 11)
          return centena[cen-1] + " " + unidades(dec);  
        else
          return centena[cen-1] + " " + decenas(dec);
      }
    }      

    $scope.cargaLlave = function (fileContent){
      $scope.fe.FIRMA.keyPriv = fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
      $scope.fe.FIRMA.certificado = fileContent;
    };

    $scope.registra = function() {
    $scope.btnRegistro = true;
    if ($scope.paciente.N_DIAS_INCAPACIDAD == undefined) {
      mensajes.alerta('ERROR','ESPECIFIQUE EL NÚMERO DE DÍAS DE INCAPACIDAD','ACEPTAR!');
      document.getElementById("N_DIAS_INCAPACIDAD").focus();
      $scope.btnRegistro = false;
    }
    else
      if ($scope.paciente.TIPO_INCAPACIDAD == undefined) {
        mensajes.alerta('ERROR','ESPECIFIQUE EL TIPO DE LA INCAPACIDAD','ACEPTAR!');
        document.getElementById("TIPO_INCAPACIDAD").focus();
        $scope.btnRegistro = false;
      }
      else
        if (($scope.paciente.TIPO_INCAPACIDAD == 3 || $scope.paciente.TIPO_INCAPACIDAD == 4) && $scope.paciente.F_NACIMIENTO == undefined) {                            
          mensajes.alerta('ERROR','INGRESE LA FECHA DE NACIMIENTO','ACEPTAR!');
          document.getElementById("F_NACIMIENTO").focus();
          $scope.btnRegistro = false;
        }
        else
          if ($scope.paciente.TIPO_INCAPACIDAD_COMP == undefined) {          
              mensajes.alerta('ERROR','ESPECIFIQUE SI ES POR CONSULTA EXTERNA U HOSPITALIZACIÓN','ACEPTAR!');
              document.getElementById("TIPO_INCAPACIDAD_COMP").focus();
              $scope.btnRegistro = false;            
          }
          else {
            if ($scope.paciente.F_EXPEDICION == undefined) {
              mensajes.alerta('ERROR','INGRESE LA FECHA DE EXPEDICIÓN DE LA INCAPACIDAD','ACEPTAR!');
              document.getElementById("F_EXPEDICION").focus();
              $scope.btnRegistro = false;
            }
            else 
              if ($scope.paciente.F_APARTIR_DE == undefined) {
                mensajes.alerta('ERROR','INGRESE LA FECHA DE INICIO DE LA INCAPACIDAD','ACEPTAR!');
                document.getElementById("F_APARTIR_DE").focus();
                $scope.btnRegistro = false;
              }
              else {
                if($scope.fe.FIRMA.keyPriv != undefined && $scope.fe.FIRMA.certificado != undefined) {
                  
                  
                  var datos = {
                                C_NOMBRE                :  $scope.paciente.C_NOMBREPACIENTE,
                                NP_PLAZA                :  $scope.paciente.NP_ID,
                                CP_ID_SIU               :  $scope.paciente.CP_ID_SIU,
                                NP_EXPEDIENTE_SIU       :  $scope.paciente.NP_EXPEDIENTE_SIU,
                                UNIDAD_ADSCRIPCION      :  $scope.paciente.C_ADSCRIPCION_DESC,
                                F_NACIMIENTO            :  moment($scope.paciente.F_NACIMIENTO).format("DD/MM/YYYY"),
                                TIPO_INCAPACIDAD        :  $scope.paciente.TIPO_INCAPACIDAD,  
                                TIPO_INCAPACIDAD_COMP   :  $scope.paciente.TIPO_INCAPACIDAD_COMP,                            
                                N_DIAS_INCAPACIDAD      :  $scope.paciente.N_DIAS_INCAPACIDAD,
                                C_DIAS_INCAPACIDAD      :  $scope.paciente.C_DIAS_INCAPACIDAD,
                                F_EXPEDICION            :  moment($scope.paciente.F_EXPEDICION).format("DD/MM/YYYY"),
                                F_APARTIR_DE            :  moment($scope.paciente.F_APARTIR_DE).format("DD/MM/YYYY"),
                                C_DIAGNOSTICO_MEDICO    :  $scope.paciente.C_DIAGNOSTICO_MEDICO,
                                NOMBRE_COMPLETO         :  $scope.medico.NOMBRE_COMPLETO,
                                CEDULA                  :  $scope.medico.CEDULA,
                                NP_EXPEDIENTE           :  $scope.paciente.NP_EXPEDIENTE,
                                ID_MEDICO               :  usuario.NP_EXPEDIENTE,
                                ID_PACIENTE             :  $scope.paciente.NP_EXPEDIENTE.split("/")[0],
                                FIRMA                   :  { keyPriv     : $scope.fe.FIRMA.keyPriv,
                                                            certificado : $scope.fe.FIRMA.certificado,
                                                           }
                  };
                  /*console.log("Datos a enviar");
                  console.log(datos);
                  console.log(JSON.stringify( datos ))*/

                  $scope.btnRegistro = false;

                  peticiones.postMethodPDF("incapacidadTemporal/" + datos.ID_PACIENTE + "/" + datos.ID_MEDICO, datos)
                  .success(function(data){
                      var file = new Blob([data], {type: 'application/pdf'});
                      var fileURL = URL.createObjectURL(file);
                      reportes.getReporte(fileURL, '_blank', 'width=1000, height=800'); 
                      $scope.btnRegistro = false;
                      $state.go('agendaConsultaPaciente',{state: 'incapacidadTemporal'});
                  })
                  .error(function(data){                         
                      if (!data.hasOwnProperty('error')) {
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        var obj = JSON.parse(decodedString);
                        mensajes.alerta('ERROR',obj.error,'ACEPTAR');
                      } else
                        mensajes.alerta('ERROR','NO SE PUDO GENERAR EL DOCUMENTO','ACEPTAR');
                      $scope.btnRegistro = false;
                  });

                }
                else
                  if ($scope.fe.FIRMA.keyPriv == undefined) {
                    $scope.btnRegistro = false;
                    mensajes.alerta('AVISO','DEBES SELECCIONAR EL ARCHIVO .KEY PARA GENERAR LA NOTA DE EGRESO','ACEPTAR!');  
                  }
                  else if ($scope.fe.FIRMA.certificado == undefined) {
                        $scope.btnRegistro = false;
                        mensajes.alerta('AVISO','DEBES SELECCIONAR EL ARCHIVO .CER PARA GENERAR LA NOTA DE EGRESO','ACEPTAR!');  
                  }
                
              }
           }  
    }  

    function creaNodo(C_DESCRIPCION, C_VALOR, N_PUNTAJE, C_MOTIVO, C_PARAMETRO, APARTADO) {
      var nodo = {};      
        
      nodo.C_DESCRIPCION = C_DESCRIPCION;
      nodo.C_VALOR = C_VALOR;
      nodo.N_PUNTAJE = N_PUNTAJE;
      nodo.C_MOTIVO = C_MOTIVO;
      nodo.C_PARAMETRO = C_PARAMETRO;

      APARTADO.push(nodo);
    }

    /*function createObjResp(obj){

    return _.map(_.pairs(obj), function(el){

      return {
        C_DESCRIPCION: el[0]
        C_VALOR: el[1],
        C_PARAMETRO: el[0],
      }
    });*/



  }]);


})();