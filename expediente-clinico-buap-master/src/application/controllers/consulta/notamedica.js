(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultaNotamedicaCtrl
 * @description
 * # ConsultaNotamedicaCtrl
 * Controller of the expediente
 *	Estados: edoNota1 : Ingreso, Evolutiva, Evolución y alta
 *	Estados: edoNota2 : Interconsulta
 */
angular.module('expediente').controller('ConsultaNotamedicaCtrl', ['$scope','$mdDialog','$mdUtil','$mdSidenav','$localStorage','pacientes','consultas','mensajes','$state','catalogos','$q','usuario','peticiones','indicacionTerapeutica','$window','$mdToast','medicos','permisos','reportes',
	function ($scope,$mdDialog,$mdUtil,$mdSidenav,$localStorage,pacientes,consultas,mensajes,$state,catalogos,$q,usuario,peticiones,indicacionTerapeutica,$window,$mdToast,medicos,permisos,reportes) {
	$scope.isBegin = false;
	$scope.isNotaMedica=true;
	$scope.existAnteriores = false;
	$scope.editExploracion = false;
	$scope.selectedIndex = $localStorage.selectedIndex;
	$scope.activeSelected = $scope.selectedIndex;
	$scope.nombres;
	$scope.datosEstudios = [];
	$scope.datosSecundarios = [];
	$scope.fe = {
		FIRMA : {},
		NOTAS_MEDICAS : {}
	};
	$scope.existEstudios = false;
	$scope.recetaUrgencias = false;
	$scope.signosAnteriores = "";
	$scope.lengthAnterior = -1;
	$scope.nMedicamentos = -1;
	$scope.tituloIndicacion = "INDICACIÓN TERAPÉUTICA";
	$scope.reload = false;
	$scope.idReceta = -404;
	$scope.canStart = false;
	$scope.nuevo={
		'ANAMNESIS':{
			'C_ANT_HRD_FAMILIARES':'',
			'C_ANT_PRS_PATOLOGICOS':'',
			'C_ANT_PRS_NO_PATOLOGICOS':'',
			'C_ANT_GINECOBTRETICIOS':'',
			'C_PADECIMIENTO_ACTUAL':''
		},
		'EXPLORACION_FSC':{
			'N_TSN_ART_SISTOLICA':'',
			'N_TSN_ART_DIASTOLICA':'',
			'N_FRC_CARDIACA':'',
			'N_FRC_RESPIRATORIA':'',
			'N_TEMPERATURA':'',
			'N_PESO':'',
			'N_TALLA':'',
			'C_DESCRIPCION':''
		},
		'INTERROGATORIO':{
			'GENERAL':'',
			'ESPECIFICO':''
		},
		'NOTAS_MEDICAS':{
			'NF_AGENDA':'',
			'C_DIAGNOSTICO':'',
			'C_DSC_LABORATORIO':'',
			'C_PRONOSTICO':'',
			'C_IND_TERAPEUTICA':''
		}
	};
	//console.log($localStorage);
	//console.log($scope);

	$scope.habilitaMedicinaLegal = false;
	$scope._urgencias = false;
	$scope.noSePresento = false;


	consultas.getDestinoPaciente().success(function(data){
		$scope.catDestinoPaciente = data;
	});

	$scope.setPermisosCE = function(){
		$scope.permisos = permisos.getPermisosConsultaExterna(usuario.perfil.clave);
		if(usuario.perfil.clave === 'RESI' && $scope.permisos[5].read){
			$scope.canRequest = true;
		}
	};

	$scope.setPermisosUr = function(){
		$scope.permisos = permisos.getPermisosUrgencias(usuario.perfil.clave);
		if(usuario.perfil.clave === 'RESI' && $scope.permisos[5].read){
			$scope.canRequest = true;
		}
	};

	$scope.tituloNota = 'NOTA MÉDICA';

	if($localStorage.paciente !== undefined){
		if($localStorage.paciente.tipo === "ingreso"){
			$scope.tituloNota = 'NOTA MÉDICA INGRESO';
			$scope.isIngreso = true;
			$scope.isMedicinaLegal = false;
			$scope.hasMedicinaLegal = false;
			$scope.isTriage = false;
			$scope.ableDLG = true;
			$scope.isUrgencias = true;
			$scope.tituloIndicacion = "ORDEN MÉDICA";
		}
		else if($localStorage.paciente.tipo === 'EVOLUTIVA'){
			$scope.tituloNota = 'NOTA MÉDICA EVOLUTIVA';
			$scope.isEvolutiva = true;
			$scope.isMedicinaLegal = false;
			$scope.hasMedicinaLegal = false;
			$scope.isTriage = false;
			$scope.ableDLG = true;
			$scope.isUrgencias = true;
			$scope.tituloIndicacion = "ORDEN MÉDICA";
		}
		else if($localStorage.paciente.tipo === 'INTERCONSULTA'){
			$scope.tituloNota = 'NOTA MÉDICA INTERCONSULTA';
			$scope.isInterconsulta = true;
			$scope.isMedicinaLegal = false;
			$scope.hasMedicinaLegal = false;
			$scope.isTriage = false;
			$scope.ableDLG = false;
			$scope.isUrgencias = true;
			$scope.tituloIndicacion = "ORDEN MÉDICA";
		}
		else if($localStorage.paciente.tipo === 'EVOLUCION Y ALTA'){
			$scope.tituloNota = 'NOTA MÉDICA EVOLUCIÓN ALTA';
			$scope.isEvolucionAlta = true;
			$scope.isMedicinaLegal = false
			$scope.hasMedicinaLegal = false;
			$scope.isTriage = false;
			$scope.ableDLG = false;
			$scope.isUrgencias = true;
			$scope.tituloIndicacion = "INDICACIÓN TERAPÉUTICA";
		}
		else if($localStorage.paciente.tipo === 'ingresoTriage'){
			$scope.tituloNota = 'NOTA MÉDICA';
			$scope.isTriage = true;
			$scope.isEvolucionAlta = false;
			$scope.isMedicinaLegal = false;
			$scope.hasMedicinaLegal = false;
			$localStorage.paciente.tipo = "ingreso";
			$scope.ableDLG = true;
			$scope.isUrgencias = true;
			$scope.tituloIndicacion = "INDICACIÓN TERAPÉUTICA";
		}
		else if($localStorage.paciente.tipo === 'TRIAGE'){
			$scope.tituloNota = 'NOTA MÉDICA';
			$scope.isTriage = false;
			$scope.isEvolucionAlta = false;
			$scope.isMedicinaLegal = false
			$scope.hasMedicinaLegal = false;
			$scope.ableDLG = false;
			$scope.isVerde = true;
			$scope.isUrgencias = true;
			$scope.tituloIndicacion = "INDICACIÓN TERAPÉUTICA";
		}
		if($localStorage.paciente.tipo !== undefined){
			$scope.setPermisosUr();
			if($localStorage.paciente.tipo === 'ingreso' || $localStorage.paciente.tipo === 'EVOLUTIVA' || $localStorage.paciente.tipo === 'INTERCONSULTA' || $localStorage.paciente.tipo === 'EVOLUCION Y ALTA' || $localStorage.paciente.tipo === 'TRIAGE'){// Valida si es NOTA MÉDICA DE INGRESO O EVOLUTIVA
				$localStorage.paciente.idCita = usuario.usuario.NP_EXPEDIENTE;
				$localStorage.paciente.fecha = moment(new Date()).format('DD/MM/YYYY');
				$localStorage.paciente.hora = moment(new Date()).format('HH:mm');
				$scope.editExploracion = true;
				if($localStorage.notaMedica !== undefined){
					consultas.getEstatusNotaMedica($localStorage.notaMedica.NP_NOTA_MEDICA)//OBTIENE EL ESTATUS DE LA NOTA MÉDICA ACTUAL
					.success(function(data){
						if(data.Mensaje === 'NOTA MEDICA EN PROCESO'){
							$scope.isBegin = true;
						}
						else if(data.Mensaje === 'NOTA MEDICA TERMINADA'){
							$localStorage.paciente = undefined;
							$localStorage.notaMedica = undefined;
							$state.go('bienvenido');
						}
						else{
							$scope.isBegin = false;
						}
					})
					.error(function(data){
						mensajes.alerta('ERROR','SERVICIO OBTENER ESTATUS NO DISPONIBLE','ACEPTAR!');
					});;
				}
				$scope.consulta = {fecha : moment(new Date()).format('DD/MM/YYYY') , hora : moment(new Date()).format('HH:mm')};
			}
			else{
				//console.log("-");
			}
		}

		if(!$scope.editExploracion){//VALIDACIÓN DE MOSTRAR INGRESO DE ID PACIENTE PARA COMENZAR CITA CUANDO VIENE DE CONSULTA EXTERNA
			$scope.setPermisosCE();
			$scope.consulta = {fecha : $localStorage.paciente.fecha, hora : $localStorage.paciente.hora}
			consultas.getNotaMedica($localStorage.paciente.expediente,$localStorage.paciente.idCita)
			.success(function(data){
				$scope.nota=data[0];
				if(data.length>0){
					if($scope.nota.nota.NF_ESTATUS === 'SIGNOS VITALES'){
						$localStorage.selectedIndex = 0;
						$scope.activeSelected = $scope.selectedIndex;
					}else if($scope.nota.nota.NF_ESTATUS === 'REALIZADA'){
						mensajes.alertaTimeout('ERROR','LA CITA YA HA SIDO REALIZADA','ACEPTAR!', 2000).then(function(){
							$state.go('consultaAgendaMedica');
						});
					}else if($scope.nota.nota.NF_ESTATUS === 'EN CONSULTA'){
						$scope.isBegin=true;
					}
				}
				else {
					$scope.isBegin = false;
				}
			})
			.error(function(data){
				mensajes.alerta('ERROR','SERVICIO OBTENER NOTA MÉDICA NO DISPONIBLE','ACEPTAR!');
			});
		}

		consultas.getIMC()
    	.success(function(data){
    		$scope.clasificacionIMC = data;
    	})
    	.error(function(data){
    		mensajes.alerta('ERROR','SERVICIO DE CÁLCULO DE IMC NO DISPONIBLE','ACEPTAR!');
    	});

    	consultas.getTipoNotas()
    	.success(function(data){
    		$scope.tipoNotas = data;
    	})
    	.error(function (data){
    		mensajes.alerta('ERROR','SERVICIO DE INGRESOS NO DISPONIBLE','ACEPTAR!');
    	});
	}

	$scope.cargarDatos = function(){
		
		catalogos.getCatalogos(2)
		.success(function(data){
			$scope.catDatosLaboratorio = data;
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});

		catalogos.getCatCIE10().success(function(data){
			$scope.catCIE10 = data;
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
		
		pacientes.getPaciente()
		.success(function(data){
			if(data.success===true){
				//console.log(data);
				if(data.data[0].C_HOSPITALIZACION != undefined){
					if(data.data[0].C_HOSPITALIZACION == "HOSPITALIZACION" || $localStorage.procedenciaConsulExterna || $localStorage.procedenciaHistoriaClinica){
						$scope.isHospitalizacion = true;
					}
					else{
						$scope.isHospitalizacion = false;
					}
				}
				//console.log("notamedica HOSPITALIZACION");
				//console.log($scope.isHospitalizacion);
      			pacientes.cargaPacienteMinimo(data.data[0]);
				$scope.paciente=data.data[0];
				$scope.paciente.nombreCompleto = $scope.paciente.C_PRIMER_APELLIDO+' '+$scope.paciente.C_SEGUNDO_APELLIDO+' '+$scope.paciente.C_NOMBRE;
				$scope.paciente.direccion = $scope.paciente.C_CALLE+' '+$scope.paciente.C_NO_EXTERIOR+' '+$scope.paciente.C_COLONIA+' '+$scope.paciente.C_LOCALIDAD+' '+$scope.paciente.C_MUNICIPIO+' '+$scope.paciente.NF_ENTIDAD_FEDERATIVA;
				$scope.paciente.N_EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
				$scope.paciente.NP_ID_C = $scope.paciente.NP_ID;

				var url = "fotoPaciente/"+$scope.paciente.NP_EXPEDIENTE.split("/")[0];
				peticiones.getDatos(url)
		        .then(function(data) {

		        	$scope.image=data;
	        		$scope.ruta=baseURL+url+"?access_token="+$localStorage.userSecurityInformation.token.value;
	        		console.log($scope.ruta);
	        	})	
		        .catch(function(err) {
		          //console.log(err);
		          $scope.ruta="images/imagen_no_encontrada.png";
		          //mensajes.alerta("ALERTA","NO EXISTE ARCHIVO","OK");
		        });

				if($localStorage.paciente.tipo !== undefined){
					if($localStorage.paciente.tipo === 'ingreso' || $localStorage.paciente.tipo === 'EVOLUTIVA' || $localStorage.paciente.tipo === 'INTERCONSULTA' || $localStorage.paciente.tipo === 'EVOLUCION Y ALTA' || $localStorage.paciente.tipo === 'TRIAGE'){

						$scope.cargaDatosUrgencias();
						consultas.getServiciosPaciente($localStorage.paciente.expediente).success(function(data){
							$scope.paciente.SERVICIO = (data.C_SERVICIO != undefined) ? data.C_SERVICIO : "URGENCIAS"
							if(data.C_CAMA != undefined){
								$scope.C_CAMA = data.C_CAMA;
							}
						})
						.error(function(data){

						});
						$scope.setPermisosUr();

					}
				}
				else{
					$scope.cargaDatos();
					$scope.paciente.SERVICIO = "CONSULTA EXTERNA";
					$scope.setPermisosCE();
				}
			}
			else if(data.success===false){
				mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
			}
			else{
				mensajes.alerta('ERROR','LO SENTIMOS, OCURRIÓ UN PROBLEMA','ACEPTAR!');
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
	};

	$scope.addSolicitudEstudio = function(estudio){
		if(estudio != undefined){
			consultas.getValidacionEstudios($localStorage.paciente.expediente , estudio.ID)
			.success(function(data){
				if(data.success){
					if(data.Mensaje == 1){ //ES PRIMERA VEZ
						$scope.isEstudio=true;
						$scope.datosEstudios.push(estudio);
						$scope.catDatosLaboratorio = _.difference($scope.catDatosLaboratorio,$scope.datosEstudios);
					}
					else if(data.Mensaje == 2){//
						mensajes.alerta("AVISO","ESTE ESTUDIO YA HA SIDO SOLICITADO EL DÍA DE HOY","ACEPTAR");
					}
				}
			})
			.error(function(data){

			});
		}
	};

	$scope.addDiagnosticoSecundario = function(diagnostico){
		if(diagnostico != undefined){
			$scope.isDiagnostico=true;
			$scope.datosSecundarios.push(diagnostico);
			$scope.catCIE10 = _.filter($scope.catCIE10, function(e){
				return e.NP_ID !== diagnostico.NP_ID;
			});
		}
	};

	$scope.deleteSolicitudEstudio = function(ev,dato){
		if(dato.idEstudio != undefined){ //VALIDA SI EL ESTUDIO YA ESTÁ REGISTRADO EN BD Y LO ELIMINA
			$scope.NOTA = {
				"DATOS_DE_LABORATORIO_Y_GABINETE":
					{"C_SERVICIOS_PRODUCTOS":dato.idEstudio+''},
					"NOTAS_MEDICAS":{"NF_AGENDA":'',NF_MEDICO : usuario.usuario.NP_EXPEDIENTE
				}
			};
			if($localStorage.notaMedica !== undefined){
				$scope.NOTA.NF_AGENDA = $localStorage.notaMedica.NP_NOTA_MEDICA;
				$scope.NOTA.NOTAS_MEDICAS.NF_AGENDA = $localStorage.notaMedica.NP_NOTA_MEDICA;
			}
			else{
				$scope.NOTA.NF_AGENDA = $localStorage.idNota;
				$scope.NOTA.NOTAS_MEDICAS.NF_AGENDA = $localStorage.idNota;
			}
			consultas.deleteSolicitudEstudio($scope.NOTA).success(function(data){
				if(data.success){
					$scope.datosEstudios = _.filter($scope.datosEstudios,function(e){
				    	return e.ID+''!==dato.ID+'';//DEVUELVE EL ESTUDIO A LA LISTA DE ESTUDIOS DISPONIBLES
				    });
				    $scope.catDatosLaboratorio.push(dato);
				}
				else{
					mensajes.alerta("AVISO",data.Mensaje.toUpperCase(),"ACEPTAR");
				}
			}).error(function(data){});
		}
		else{
			$scope.datosEstudios = _.filter($scope.datosEstudios,function(e){
		    	return e.ID+''!==dato.ID+'';//DEVUELVE EL ESTUDIO A LA LISTA DE ESTUDIOS DISPONIBLES
		    });
		    $scope.catDatosLaboratorio.push(dato);
		}
	};

	$scope.deleteDiagnosticoSecundario = function(ev,dato){
		$scope.datosSecundarios = _.filter($scope.datosSecundarios,function(e){
	    	return e.NP_ID+''!==dato.NP_ID+'';
	    });
	    $scope.catCIE10.push(dato);
	};

	$scope.comenzarCita= function(){
		if ($scope.noSePresento) {
			mensajes.mConfirmacion("ALERTA","ESTA SEGURO QUE DESEA CONCLUIR LA NOTA MEDICA","NO","SI")
			.then(function() {
          		console.log("CANCELAR LA NOTA..");
          		$state.go('bienvenido');
          	}).catch(function() {
          		$scope.noSePresento = false;
          	});;
		}
		else {
			$scope.canStart = true;
			$localStorage.selectedMedicamentos = undefined;
			if($scope.paciente.NP_ID_C===$scope.paciente.NP_ID){
				$scope.isBegin=true;
				if(!$scope.isIngreso && !$scope.isInterconsulta && !$scope.isEvolucionAlta && !$scope.isTriage && !$scope.isVerde && !$scope.isEvolutiva){//ENTRA CUANDO VIENE DE AGENDA
					consultas.getNotaMedica($localStorage.paciente.expediente,$localStorage.paciente.idCita)
					.success(function(data){
						$scope.nota=data[0];
						consultas.switchCita($localStorage.paciente.idCita)
						.success(function(data){
							$mdToast.show(
	                            $mdToast.simple()
	                            .content('LA CITA HA COMENZADO')
	                            .position('top left')
	                            .hideDelay(3000)
	                        );
						})
						.error(function(data){
							mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
						});

					})
					.error(function(data){
						$scope.isBegin=false;
						$scope.canStart = false;
						mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
					});
				}
				else{//ENTRA SI VIENE DE NOTA MÉDICA URGENCIAS(INGRESO, EVOLUTIVA, INTERCONSULTA)
					if(!$scope.isTriage && !$scope.isVerde){
						var tipoNota = _.filter($scope.tipoNotas,function(e){
		  					return e.C_DESCRIPCION === $localStorage.paciente.tipo.toUpperCase();
		  				});
						$localStorage.registroUrgencias = {
							NF_PACIENTE : $localStorage.paciente.expediente,
							NF_MEDICO : $localStorage.paciente.idCita,
							NF_TIPO_NOTA: tipoNota[0].NP_ID
						}
						if($localStorage.paciente.tipo === 'INTERCONSULTA'){
				    		$localStorage.registroUrgencias.NF_INTERCONSULTA = $localStorage.paciente.NF_INTERCONSULTAS;
				    	}
				    	if(usuario.perfil.clave === 'HOSP'){
							$localStorage.registroUrgencias.N_TIPO_HOSPITALIZACION = 1;
						}
						else if(usuario.perfil.clave === 'URGENCIAS'){
							$localStorage.registroUrgencias.N_TIPO_HOSPITALIZACION = 2;
						}
						if($localStorage.paciente.NF_NOTA_MEDICA != undefined){
							consultas.getTriage($localStorage.paciente.NF_NOTA_MEDICA)
							.success(function(data){
								if (data.success){
									$localStorage.registroUrgencias.NF_TRIAGE = data.Mensaje;
									$scope.comenzarCitaUrgencias();
								}
								else{
									$scope.comenzarCitaUrgencias();
								}

							})
							.error(function(data){

							});
						}
						else{
							$scope.comenzarCitaUrgencias();
						}
					}
					else{ //TRIAGE
						if(!$scope.isVerde){
							var tipoNota = _.filter($scope.tipoNotas,function(e){
		  					return e.C_DESCRIPCION === $localStorage.paciente.tipo.toUpperCase();
		  					});
		  					$localStorage.paciente.tipoNota = tipoNota[0].NP_ID;
						}
						$localStorage.registroUrgencias = {
							NF_PACIENTE : $localStorage.paciente.expediente,
							NF_MEDICO : usuario.usuario.NP_EXPEDIENTE,
							NF_TRIAGE: $localStorage.paciente.idTriage,
							NF_TIPO_NOTA: $localStorage.paciente.tipoNota
						}
						if(usuario.perfil.clave === 'HOSP'){
							$localStorage.registroUrgencias.N_TIPO_HOSPITALIZACION = 1;
						}
						else if(usuario.perfil.clave === 'URGENCIAS'){
							$localStorage.registroUrgencias.N_TIPO_HOSPITALIZACION = 2;
						}
						if($localStorage.paciente.tipo === 'INTERCONSULTA'){
				    		$localStorage.registroUrgencias.NF_INTERCONSULTA = $localStorage.paciente.NF_INTERCONSULTAS;
				    	}
						$scope.comenzarCitaUrgencias();
					}
				}
			}
			else{
				mensajes.alerta('ERROR','EL ID DEL PACIENTE NO COINCIDE CON LA CONSULTA','ACEPTAR!');
				$scope.canStart = false;
			}
		}
	};

	$scope.terminar = function(){
		if(!$scope.permisos[9].read){
			mensajes.confirmacion('AVISO','NO CUENTAS CON PERMISOS PARA FIRMAR LA NOTA MÉDICA','ACEPTAR')
          	.then(function() {
          		$state.go('bienvenido');
          	}).catch(function() {
          		$state.go('bienvenido');
          	});
		}
		else{
			medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
		    .success(function(data){
		       	if(data === ''){
		       		mensajes.alerta('ERROR','ERROR INTERNO DEL SISTEMA INTENTE NUEVAMENTE','ACEPTAR!');
		       	}
		       	else{
		       		if(usuario.perfil.clave == 'RESI'){
		       			$state.go('bienvenido');		
		       		}
		       		var expediente=$localStorage.paciente.idexpediente.indexOf('/')?$localStorage.paciente.idexpediente.split("/")[0]:$localStorage.paciente.idexpediente;
		         	var idEspecialidad = data.ESPECIALIDADES.NF_ESPECIALIDAD;
					consultas.getEstatusHistoriaClinica(expediente , idEspecialidad)
					.success(function(data){
						if(data.estatus){ // Si la Historia Clínica está terminada estatus == true
					    if($localStorage.procedenciaConsulExterna){
							$scope.confirmacion();
					    }else{
					    	$scope.confirmacion();
					    }
					}
						else if(!data.estatus){ //Si la nota médica no está terminada estatus == false
							mensajes.alerta("AVISO","HISTORIA CLÍNICA INCOMPLETA, POR FAVOR COMPLETARLA","ACEPTAR");
							$localStorage.procedenciaHistoria=false;
							if($localStorage.paciente.page !== undefined){
								if($localStorage.paciente.page === 'historiaClinica'){
									$localStorage.procedenciaHistoria=true;
								}
								else{
									$localStorage.procedenciaHistoria=false;
								}
							}
							$localStorage.procedenciaExpediente=false;
							$state.go("historiaClinica");
						}
					})
					.error(function(data){
						defered.reject();
					});
		       	}
		    })
		    .error(function(data){
		    	mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
		    });
		}
	};

	$scope.terminarUrgencias = function(){
		if($scope.isMedicinaLegal && $scope.hasMedicinaLegal){
			mensajes.alerta('AVISO','DEBES REGISTRAR MEDICINA LEGAL','ACEPTAR!');
			return null;
		}
		else{
			var confirm = $mdDialog.confirm()
				    .title('AVISO')
				    .content('¿DESEAS TERMINAR LA CONSULTA?')
					.ariaLabel('Lucky day')
				    .targetEvent()
				    .ok('ACEPTAR!')
				    .cancel('CANCELAR');
			$mdDialog.show(confirm).then(function() {
				if($scope.fe.FIRMA.keyPriv != undefined && $scope.fe.FIRMA.certificado != undefined){
					$scope.fe.NOTAS_MEDICAS.NP_NOTA_MEDICA = $localStorage.notaMedica.NP_NOTA_MEDICA;
					$scope.fe.FIRMA.id = usuario.usuario.NP_EXPEDIENTE;
					consultas.setFirmaElectronica($scope.fe)
					.success(function(data){
						if (data.success){
							consultas.terminarNotaMedicaUrgencias($localStorage.notaMedica.NP_NOTA_MEDICA)// Termina Nota Médica de Urgencias
							.success(function(data){
								if(data.success){
									mensajes.alerta('AVISO','LA CITA HA TERMINADO','ACEPTAR!');
									reportes.getReporte(baseURL + 'reportenotasmedicas/' + $localStorage.notaMedica.NP_NOTA_MEDICA, '_blank', 'width=800, height=640');
									if($localStorage.paciente.tipo === 'EVOLUCION Y ALTA' || $localStorage.paciente.tipo === 'TRIAGE' || $scope.isVerde){
			  							$scope.generaReporteReceta($scope.idReceta); //Genera Reporte de Receta Médica
			  						}
			  						$localStorage.paciente = undefined;
									$localStorage.notaMedica = undefined;
									$localStorage.selectedIndex = 0;
									$state.go('bienvenido');
								}
								else{
									mensajes.alerta('AVISO',data.Mensaje,'ACEPTAR!');
									return null;
								}
							})
							.error(function(data){});
						}
						else {
							mensajes.alerta('ERROR',data.Mensaje.toUpperCase(),'ACEPTAR!');
							return null;
						}
					})
					.error(function(data){
						mensajes.alerta('ERROR','SERVICIO FIRMA ELECTRÓNICA NO DISPONIBLE','ACEPTAR!');
					});
				}
				else if(usuario.perfil.clave == 'RESI'){
					$localStorage.paciente = undefined;
					$localStorage.notaMedica = undefined;
					$localStorage.selectedIndex = 0;
					$state.go('bienvenido');
				}
				else {
					mensajes.alerta('AVISO','DEBES SELECCIONAR EL ARCHIVO .KEY Y .CER PARA TERMINAR LA CONSULTA','ACEPTAR!');
				}
			}, function() {
			});
		}
	};

    $scope.citaPrimeraVez=function(idCita,NF_CIE10){
      $localStorage.procedenciaUrgencias = ($localStorage.notaMedica !== undefined) ? true : false;
    	var url = ($localStorage.notaMedica !== undefined) ? ("notamedica/tipoDiagnosticoUrgencias/"+$localStorage.notaMedica.NP_NOTA_MEDICA+"/"+NF_CIE10) : ("notamedica/tipoDiagnostico/" + idCita + "/" + NF_CIE10);
      	peticiones.getDatos(url)
			.then(function(data) {
				$scope.esPrimeraVez = (data.Mensaje == 1) ? true : false; //1 Primera vez, 2 No es Primera Vez
			})
        .catch(function(err) {
            $scope.esPrimeraVez = false;
        });
    }

  	$scope.siguiente = function(indexTab){
  		if(indexTab === -1){
  			$scope.selectedIndex += 1
			$localStorage.selectedIndex = $scope.selectedIndex;
			return false;
  		}
  		switch(indexTab){
  			case 0: //RESUMEN INTERROGATORIO
  				if($scope.nuevo.RESUMEN_DEL_INTERROGATORIO.C_MOTIVO_DE_CONSULTA == undefined){
  					mensajes.alerta('AVISO','DEBES COMPLEMENTAR EL CAMPO DE MOTIVO DE CONSULTA','ACEPTAR!');
  					return null;
  				}
  				if($localStorage.paciente.tipo != "EVOLUTIVA" && $localStorage.paciente.tipo != "INTERCONSULTA"){
					if($scope.nuevo.RESUMEN_DEL_INTERROGATORIO.C_ANAMNESIS == undefined){
						mensajes.alerta('AVISO','DEBES COMPLEMENTAR EL CAMPO DE RESUMEN DEL INTERROGATORIO','ACEPTAR!');
  						return null;
					}
  				}
  				$scope.NOTA = {'RESUMEN_DEL_INTERROGATORIO':{'C_ANAMNESIS' : $scope.nuevo.RESUMEN_DEL_INTERROGATORIO.C_ANAMNESIS,'C_MOTIVO_DE_CONSULTA': $scope.nuevo.RESUMEN_DEL_INTERROGATORIO.C_MOTIVO_DE_CONSULTA}};
        		if($localStorage.paciente.tipo === "ingreso"){
        			if($scope.isHospitalizacion != undefined)
        				if(!$scope.isHospitalizacion)
            				$state.go("historiaClinicaUrgencias");
        		}
  				break;
  			case 1: //EXPLORACIÓN FÍSICA
  				if($localStorage.notaMedica !== undefined){//GUARDA NOTA MÉDICA DE URGENCIAS
  					if($scope.nuevo.EXPLORACION_FSC.C_ESTADO_MENTAL == undefined){
	  					mensajes.alerta('AVISO','DEBES COMPLEMENTAR EL CAMPO DE ESTADO MENTAL DEL PACIENTE','ACEPTAR!');
	  					return null;
	  				}
	  				if($localStorage.paciente.tipo != "EVOLUTIVA" && $localStorage.paciente.tipo != "INTERCONSULTA"){
						if($scope.nuevo.EXPLORACION_FSC.EXPLORACION == undefined){
							mensajes.alerta('AVISO','DEBES COMPLEMENTAR EL CAMPO DE EXPLORACIÓN FÍSICA','ACEPTAR!');
	  						return null;
						}
	  				}

  					$scope.NOTA = {'EXPLORACION_FISICA': $scope.nuevo.EXPLORACION_FSC};
  					$scope.NOTA.EXPLORACION_FISICA.C_DESCRIPCION = $scope.NOTA.EXPLORACION_FISICA.EXPLORACION;
  					$scope.NOTA.EXPLORACION_FISICA.C_CLASIFICACION = $localStorage.C_CLASIFICACIONID;
  					$scope.NOTA.EXPLORACION_FISICA.C_ESTADO_MENTAL = $scope.nuevo.EXPLORACION_FSC.C_ESTADO_MENTAL;
  				}
  				else{ //GUARDA NOTA MÉDICA DE AGENDA
  					$scope.NOTA = {'EXPLORACION_FISICA':{'C_DESCRIPCION' : $scope.nuevo.EXPLORACION_FSC.EXPLORACION}};

  					$localStorage.signosVitales = {
			    		'EXPLORACION_FISICA': $scope.nuevo.EXPLORACION_FSC,
			    		'NOTAS_MEDICAS': {
			    			'NF_AGENDA':$localStorage.paciente.idCita
			    		}
			    	};
			    	if($scope.nuevo.EXPLORACION_FSC.EXPLORACION != undefined){
			    		$localStorage.signosVitales.EXPLORACION_FISICA.C_DESCRIPCION = $scope.nuevo.EXPLORACION_FSC.EXPLORACION;
			    	}
			    	$localStorage.signosVitales.EXPLORACION_FISICA.C_CLASIFICACION = $localStorage.C_CLASIFICACIONID;
					consultas.guardaSignosVitales($localStorage.signosVitales)
			    	.success(function(data){
			    		$scope.calcularIMC();
			    		if(data.success){
			    			$mdToast.show(
		                        $mdToast.simple()
		                        .content('DATOS GUARDADOS')
		                        .position('top left')
		                        .hideDelay(3000)
		                    );
		  					$scope.selectedIndex += 1;
			  				$scope.activeSelected = $scope.selectedIndex;
			  				$localStorage.selectedIndex = $scope.selectedIndex;
			  				return null;
			    		}
			    		else{
			    			mensajes.alerta("ERROR",data.Mensaje.toUpperCase(),"ACEPTAR");
			    			return null;
			    		}
			    		
			    	})
			    	.error(function(err){
			    		$scope.calcularIMC();
			    		mensajes.alerta("ERROR","OCURRIÓ UN PROBLEMA AL GUARDAR LOS DATOS","ACEPTAR");
			    		return null;
			    	});
  				}
  				break;
  			case 2: //INTERPRETACIÓN DE ESTUDIOS DE LABORATORIO Y GABINETE

				$scope.habilitaMedicinaLegal = true;

				/*Aquí se va a guardar la interpretación de estudios de laboratorio y gabinete*/
				$scope.NOTA = {INTERPRETACION_DE_LABORATORIO_Y_GABINETE : {'C_INTERPRETACION_LABS' : $scope.nuevo.C_INTERPRETACION}};
  				break;
  			case 3: //DIAGNÓSTICO
  				if($scope.diagnosticoPrincipal != undefined){
          			$scope.citaPrimeraVez($localStorage.paciente.idCita,$scope.diagnosticoPrincipal.NP_ID);
  					var diagnostico = $scope.diagnosticoPrincipal.NP_ID+'';
  					if($scope.datosSecundarios != undefined || $scope.datosSecundarios.length>0){
	  					$scope.serv = _.filter($scope.datosSecundarios,function(e){
		  					diagnostico = diagnostico+','+e.NP_ID;
		  					return true;
		  				});
	  				}
  					$scope.NOTA = {"DIAGNOSTICO":{"CIE_10":diagnostico}};
	  			}
  				else{
  					mensajes.alerta('AVISO','SELECCIONA UN DIAGNÓSTIO PRINCIPAL','ACEPTAR!');
  					return true;
  				}
  				$scope.existDiagnostico = true
  				break;
  			case 4: //SOLICITUD DE LABORATORIO Y GABINETE

				if($scope.datosEstudios.length > 0){
					if($scope.lengthAnterior != $scope.datosEstudios.length){
	  					$scope.existDiagnostico = true;
	  				}
	  				else{
	  					$scope.isCambio = false;
	  					_.filter($scope.datosEstudios , function(e){
	  						if(_.findIndex($scope.datosEstudiosAnteriores , {cDescripcion : e.cDescripcion}) == -1){
	  							$scope.isCambio = true;
	  						}
	  						return false;
	  					});
	  					if ($scope.isCambio){
	  						$scope.existDiagnostico = true;
	  					}
	  					else{
	  						$scope.cambiaMotivo = false;
	  						_.filter($scope.datosEstudios , function(e){
		  						if(_.findIndex($scope.datosEstudiosAnteriores , {C_MOTIVO_SOLICITUD : e.C_MOTIVO_SOLICITUD}) == -1){
		  							$scope.cambiaMotivo = true;
		  						}
		  						return false;
		  					});
		  					$scope.existDiagnostico = ($scope.cambiaMotivo) ? true : false;
	  					}
	  				}
					if($scope.existDiagnostico)
						$scope.existEstudios = true;
				}

  				var SERVICIOS_PRODUCTOS = _.map($scope.datosEstudios, function (el, key) {
                    return{
                    	'NP_SRV_PRODUCTO' : el.ID,
                    	'C_MOTIVO_SOLICITUD' : el.C_MOTIVO_SOLICITUD
                    }
                });
  				$scope.NOTA = {'DATOS_DE_LABORATORIO_Y_GABINETE':
  									{'NOTA_MEDICA_ACTUAL':{
  										"C_SERVICIOS_PRODUCTOS" : SERVICIOS_PRODUCTOS
  									}
  								}
  							};
	  			//if($localStorage.notaMedica !== undefined){//GUARDA NOTA MÉDICA DE URGENCIAS
  					$scope.NOTA.DATOS_DE_LABORATORIO_Y_GABINETE.NOTA_MEDICA_ACTUAL = {C_ANALISIS:$scope.nuevo.EXPLORACION_FSC.C_ANALISIS};
	  				$scope.NOTA.DATOS_DE_LABORATORIO_Y_GABINETE.NOTA_MEDICA_ACTUAL.C_SERVICIOS_PRODUCTOS = SERVICIOS_PRODUCTOS;
	  				$scope.NOTA.DATOS_DE_LABORATORIO_Y_GABINETE.NOTA_MEDICA_ACTUAL.C_ANALISIS = $scope.nuevo.EXPLORACION_FSC.C_ANALISIS;
	  			//}
	  			$scope.lengthAnterior = $scope.datosEstudios.length;
	  			$scope.datosEstudiosAnteriores = JSON.parse( JSON.stringify( $scope.datosEstudios ) );
  				break;
  			case 5: //PRONÓSTICO
  				var valor = 0;
  				if($scope.NF_ESTATUS1 && $scope.NF_ESTATUS3) valor = 1;
  				else if($scope.NF_ESTATUS1 && $scope.NF_ESTATUS4) valor = 2;
  				else if($scope.NF_ESTATUS2 && $scope.NF_ESTATUS3) valor = 3;
  				else if($scope.NF_ESTATUS2 && $scope.NF_ESTATUS4) valor = 4;
  				else {
  					mensajes.alerta("AVISO","DEBES SELECCIONAR EL PRONÓSTICO","ACEPTAR");
  					return null;
  				};
  				if($scope.nuevo.NOTAS_MEDICAS !== undefined){
	  				if($scope.nuevo.NOTAS_MEDICAS.C_PRONOSTICO === undefined){
	  					mensajes.alerta("AVISO","COMPLEMENTA EL CAMPO DE COMENTARIOS","OK");
	  					return null;
	  				}
  				}
  				else{
  					mensajes.alerta("AVISO","COMPLEMENTA EL CAMPO DE COMENTARIOS","OK");
	  				return null;
  				}
  				if(valor != 0){
  					$scope.NOTA = {"PRONOSTICO":{"C_PRONOSTICO":$scope.nuevo.NOTAS_MEDICAS.C_PRONOSTICO,"C_ESTATUS":valor}};
  				}
  				else {
  					mensajes.alerta('AVISO','SELECCIONA UN PRONÓSTICO','ACEPTAR!');
  					return true;
  				}
  				break;
  			case 6://INDICACIÓN TERAPÉUTICA
				var indService = indicacionTerapeutica.receta;
				console.debug( "indicacionTerapeutica", indicacionTerapeutica);
				if($localStorage.notaMedica !== undefined){ //Urgencias
					if(indService.MEDICAMENTOS != undefined){
						if($scope.nMedicamentos != indService.MEDICAMENTOS.length){
							indService.NF_NOTA_MEDICA = $localStorage.notaMedica.NP_NOTA_MEDICA;
							guardaIndicacion(indService); //Se guarda la indicación terapéutica
							if(indicacionTerapeutica.C_PLAN != undefined){ //Validación para guardar el PLAN en la nota médica
								$scope.NOTA= {INDICACION_TERAPEUTICA :{C_PLAN : indicacionTerapeutica.C_PLAN,C_IND_TERAPEUTICA:''}
								};
								$scope.NOTA.NOTAS_MEDICAS = $localStorage.notaMedica;
								$scope.NOTA.NF_MEDICO=usuario.usuario.NP_EXPEDIENTE;
								console.log($scope.NOTA);
								consultas.guardaNotaUrgencias($scope.NOTA).success(function(data){//Guarda Nota Médica
					  				if(data.success){
					  					$localStorage.notaMedica.NP_NOTA_MEDICA = data.Mensaje;
					  					$mdToast.show(
					                        $mdToast.simple()
					                        .content('DATOS GUARDADOS')
					                        .position('top left')
					                        .hideDelay(3000)
					                    );
					  				}
					  			});
							}
							$scope.selectedIndex += 1;
			  				$scope.activeSelected = $scope.selectedIndex;
			  				$localStorage.selectedIndex = $scope.selectedIndex;
			  				$scope.nMedicamentos = indService.MEDICAMENTOS.length;
						}
						else{
							$scope.selectedIndex += 1;
			  				$scope.activeSelected = $scope.selectedIndex;
			  				$localStorage.selectedIndex = $scope.selectedIndex;
			  				$scope.nMedicamentos = indService.MEDICAMENTOS.length;
						}
					}
					else{
						$scope.selectedIndex += 1;
		  				$scope.activeSelected = $scope.selectedIndex;
		  				$localStorage.selectedIndex = $scope.selectedIndex;
					}
				}
				else{ //Consulta Externa
					if(!indicacionTerapeutica.havePermissions){
						$scope.terminar();
					}
					else{
						consultas.getNotaMedica($localStorage.paciente.expediente , $localStorage.paciente.idCita)
				        .success(function(data){
				        	$localStorage.idNota = data[0].nota.NF_NOTA_MEDICA;
				        	indService.NF_NOTA_MEDICA = $localStorage.idNota;
				        	guardaIndicacion(indService); //Guarda la indicación terapéutica
				        })
				        .error(function(data){
				          	mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
				        });
					}
				}
				return null;
  				break;
  			case 7://PROCEDIMIENTOS
  				if($localStorage.paciente.tipo == "EVOLUTIVA" || $localStorage.paciente.tipo == "INTERCONSULTA"){
  					$scope.NOTA = {PROCEDIMIENTOS_REALIZADOS:{'C_PROCEDIMIENTOS_REALIZADOS':$scope.nuevo.PROCEDIMIENTOS.C_PROCEDIMIENTOS_REALIZADOS}};
  				}
  				else{
  					if($scope.nuevo.PROCEDIMIENTOS.C_PROCEDIMIENTOS_REALIZADOS != undefined)
  						$scope.NOTA = {PROCEDIMIENTOS_REALIZADOS:{'C_PROCEDIMIENTOS_REALIZADOS':$scope.nuevo.PROCEDIMIENTOS.C_PROCEDIMIENTOS_REALIZADOS}};
  					else{
  						mensajes.alerta("AVISO","DEBES COMPLEMENTAR EL CAMPO DE PROCEDIMIENTOS REALIZADOS","ACEPTAR");
  						return null;
  					}
  				}

  				break;
  			case 8://DESTINO DEL PACIENTE
  				if($scope.nuevo.C_DESTINO_PACIENTE == undefined && $scope.nuevo.NF_C_ORIGEN_PACIENTE == undefined ){
  					mensajes.alerta("AVISO","DEBES COMPLEMENTAR AL MENOS EL DESTINO DEL PACIENTE O LA DESCRIPCIÓN","ACEPTAR");
  					return null;
  				}
  				else{
  					$scope.NOTA = {DESTINO_PACIENTE:{'C_DESTINO_PACIENTE':$scope.nuevo.C_DESTINO_PACIENTE}};
	  				if($scope.nuevo.NF_C_ORIGEN_PACIENTE){
	  					$scope.NOTA.DESTINO_PACIENTE.NF_C_ORIGEN_PACIENTE = $scope.nuevo.NF_C_ORIGEN_PACIENTE;
	  				}
  				}
  				break;
  		};
  		if($localStorage.notaMedica !== undefined){//GUARDAR NOTA MÉDICA DE URGENCIAS
  			$scope.NOTA.NOTAS_MEDICAS = $localStorage.notaMedica;
  			$scope.NOTA.NF_MEDICO=usuario.usuario.NP_EXPEDIENTE;
  			console.log($scope.NOTA);
  			consultas.guardaNotaUrgencias($scope.NOTA).success(function(data){
  				if(data.success){
  					$localStorage.notaMedica.NP_NOTA_MEDICA = data.Mensaje;
  					$mdToast.show(
                        $mdToast.simple()
                        .content('DATOS GUARDADOS')
                        .position('top left')
                        .hideDelay(3000)
                    );
  					//Genera reporte de Estudios de Laboratorio y Gabinete
	  				if($scope.existEstudios){
	  					reportes.getReporte(baseURL + 'solicitudGabinete/' + $localStorage.notaMedica.NP_NOTA_MEDICA, '_blank', 'width=1000, height=800');
	  					$scope.existDiagnostico = false;
	  				}
	  				$scope.existEstudios = false;
	  				if(indexTab == 1) $scope.calcularIMC();
	  				if(indexTab === 8){
						if($scope.fe.FIRMA.keyPriv != undefined && $scope.fe.FIRMA.certificado != undefined){
	  						if($scope.esPrimeraVez){
	  							mensajes.confirmacion('AVISO','¿DESEAS ACTUALIZAR LA HISTORIA CLÍNICA?','OK')
				              	.then(function() {
				              		$localStorage.procedenciaHistoria=false;
			              			$localStorage.procedenciaExpediente=false;
			              			$state.go("historiaClinica");
				              	}).catch(function() {
				              		$scope.terminarUrgencias();
				              	});
	  						}
			              	else {
			              		$scope.terminarUrgencias();
			              	}
	  					}
	  					else if(usuario.perfil.clave == 'RESI'){
							$localStorage.paciente = undefined;
							$localStorage.notaMedica = undefined;
							$localStorage.selectedIndex = 0;
							$state.go('bienvenido');
						}
			  			else{
	  						mensajes.alerta('AVISO','DEBES SELECCIONAR EL ARCHIVO .KEY Y .CER PARA TERMINAR LA CONSULTA','ACEPTAR!');
	  						return null;
	  					}
		  			}
		  			else{
		  				$scope.selectedIndex += 1;
		  				$scope.activeSelected = $scope.selectedIndex;
		  				$localStorage.selectedIndex = $scope.selectedIndex;
		  			}
	  			}
  				else{
  					mensajes.alerta('ERROR',data.Mensaje.toUpperCase(),'ACEPTAR!');
  				}
  			})
  			.error(function(data){
	  			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE - GUARDA NOTA MÉDICA','ACEPTAR!');
  			});
  		}
  		else {//GUARDA NOTA MÉDICA DE CONSULTA EXTERNA
  			if(indexTab == 1) return null;
  			$scope.NOTA.NOTAS_MEDICAS = {'NF_AGENDA' : $localStorage.paciente.idCita};
  			consultas.guardaNotaMedica($scope.NOTA)
	  		.success(function(data){
	  			if(data.success){
	  				$mdToast.show(
                        $mdToast.simple()
                        .content('DATOS GUARDADOS')
                        .position('top left')
                        .hideDelay(3000)
                    );
	  				//Genera reporte de Estudios de Laboratorio y Gabinete
	  				if($scope.existEstudios)
	  					reportes.getReporte(baseURL + 'solicitudGabinete/' + $localStorage.idNota, '_blank', 'width=1000, height=800');
	  				$scope.existEstudios = false;
	  				$localStorage.idNota = data.Mensaje;
	  				if(indexTab < 6){ //Valida que no esté en Indicación Terapéutica
	  					$scope.selectedIndex += 1;
		  				$scope.activeSelected = $scope.selectedIndex;
		  				$localStorage.selectedIndex = $scope.selectedIndex;
	  				}
		  			if($localStorage.notaMedica === undefined && indexTab === 5){//Abre Historia Clínica después de Pronóstico
	  					var defered = $q.defer();
 						var promise = defered.promise;
 						if(!$localStorage.procedenciaHistoria){//Si entra por Agenda Médica de Consulta Externa
 							medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
						    .success(function(data){
						       	if(data === ''){
						       		mensajes.alerta('ERROR','ERROR INTERNO DEL SISTEMA INTENTE NUEVAMENTE','ACEPTAR!');
						       	}
						       	else{
						       		if(usuario.perfil.clave != 'RESI'){
						       			var idEspecialidad = data.ESPECIALIDADES.NF_ESPECIALIDAD;
									    var expediente=$localStorage.paciente.idexpediente.indexOf('/')?$localStorage.paciente.idexpediente.split("/")[0]:$localStorage.paciente.idexpediente;
			 							consultas.getEstatusHistoriaClinica(expediente , idEspecialidad)//Se valida si ya está termina la historia
					 					.success(function(data){
					 						if(!data.estatus){
					 							mensajes.alerta('AVISO','HISTORIA CLÍNICA INCOMPLETA','ACEPTAR');
						 						$localStorage.procedenciaHistoria=false;
							              		$localStorage.procedenciaExpediente=false;
					 							$state.go("historiaClinica");
								              	defered.resolve();
								            }
								            else{
								            	defered.resolve();
								            }
					 					});
						       		}
				 				}
				 			});

 						}
 						else if($localStorage.procedenciaHistoria){//Si entra por Historia Clínica
 							$localStorage.procedenciaHistoria=false;
			              	$localStorage.procedenciaExpediente=false;
			              	medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
						    .success(function(data){
						       	if(data === ''){
						       		mensajes.alerta('ERROR','ERROR INTERNO DEL SISTEMA INTENTE NUEVAMENTE','ACEPTAR!');
						       	}
						       	else{
						         	var idEspecialidad = data.ESPECIALIDADES.NF_ESPECIALIDAD;
						         	var expediente=$localStorage.paciente.idexpediente.indexOf('/')?$localStorage.paciente.idexpediente.split("/")[0]:$localStorage.paciente.idexpediente;
		 							consultas.getEstatusHistoriaClinica(expediente , idEspecialidad)
		 							.success(function(data){
		 								if(data.estatus){ // Si la Historia Clínica está terminada estatus == true
		 									mensajes.confirmacion('AVISO','¿DESEAS ACTUALIZAR LA HISTORIA CLÍNICA?','OK')
							              	.then(function() {
						              			$state.go("historiaClinica");
						              			defered.resolve();
							              	}).catch(function() {
							                	defered.reject();
							              	});
		 								}
		 								else if(!data.estatus){ //Si la nota médica no está terminada estatus == false
		 									$state.go("historiaClinica");
		 								}
		 							})
		 							.error(function(data){
		 								defered.reject();
		 							});
						       	}
						    })
						    .error(function(data){
						    	mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
						    });
 						}
		              	return promise;
	  				}//Historia Clínica
	  			}
	  			else if(!data.success){
	  				mensajes.alerta('ERROR',data.Mensaje.toUpperCase(),'ACEPTAR!');
	  			}
	  			else {
	  				mensajes.alerta('ERROR','LO SENTIMOS, HA OCURRIDO UN ERROR INESPERADO','ACEPTAR!');
	  			}
	  		})
	  		.error(function(data){
	  			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
	  		});
  		}
  	};

  	$scope.cargaDatos = function(){ // Función para recuperar la información registrada en BD de Consulta Externa
  		consultas.getNotaMedica($localStorage.paciente.expediente,$localStorage.paciente.idCita)
  		.success(function(data){
  			$scope.loadData(data,0);
  		})
  		.error(function(data){
  			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
  		});
  	};

  	$scope.cargaDatosUrgencias = function(){ // Función para recuperar la información registrada en BD de Urgencias
  		if($localStorage.notaMedica !== undefined){
	  		if($localStorage.notaMedica.NP_NOTA_MEDICA !== undefined){
	  			if($localStorage.paciente.tipo === 'ingreso' || $localStorage.paciente.tipo === 'EVOLUTIVA' || $localStorage.paciente.tipo === 'INTERCONSULTA' || $localStorage.paciente.tipo === 'EVOLUCION Y ALTA' || $localStorage.paciente.tipo === "TRIAGE"){
			  		consultas.getNotaMedicaUrgencias($localStorage.notaMedica.NP_NOTA_MEDICA)
			  		.success(function(data){
			  			$scope.loadData(data,1);
			  		})
			  		.error(function(data){
			  			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
			  		});
			  	}
		  	}
	  	}
  	};

  	$scope.loadData = function(data,procedencia){
  		var defered = $q.defer();
 		var promise = defered.promise;
		if(data.length>0){
			if(data[0].fisicas != undefined){
				$scope.nuevo={
				'EXPLORACION_FSC':{
					'N_TSN_ART_SISTOLICA':data[0].fisicas.N_TSN_ART_SISTOLICA,
					'N_TSN_ART_DIASTOLICA':data[0].fisicas.N_TSN_ART_DIASTOLICA,
					'N_FRC_CARDIACA':data[0].fisicas.N_FRC_CARDIACA,
					'N_FRC_RESPIRATORIA':data[0].fisicas.N_FRC_RESPIRATORIA,
					'N_TEMPERATURA':data[0].fisicas.N_TEMPERATURA,
					'N_PESO':data[0].fisicas.N_PESO,
					'N_TALLA':data[0].fisicas.N_TALLA,
					'C_DESCRIPCION':data[0].fisicas.C_DESCRIPCION,
					'N_SATURACION':Number(data[0].fisicas.N_SATURACION),
					'N_IMC':data[0].fisicas.N_IMC,
					'C_CLASIFICACION':data[0].fisicas.C_CLASIFICACION,
					'C_OBS_ENFERMERIA':data[0].fisicas.C_OBS_ENFERMERIA,
					'EXPLORACION':data[0].fisicas.C_DESCRIPCION
					}
				};
				$scope.calcularIMC();
				$scope.signosAnteriores = "";
			}
			else{
				if(procedencia == 1){ //Si viene de Urgencias
					if($localStorage.paciente.tipo === "ingreso" || $localStorage.paciente.tipo === 'TRIAGE'){
						if($localStorage.paciente.idTriage != undefined){
							consultas.getSignosVitalesTriage($localStorage.paciente.idTriage).success(function(da){
								if(da.N_TSN_ART_SISTOLICA != undefined){
									$scope.nuevo={
										'EXPLORACION_FSC':{
											'N_TSN_ART_SISTOLICA':da.N_TSN_ART_SISTOLICA,
											'N_TSN_ART_DIASTOLICA':da.N_TSN_ART_DIASTOLICA,
											'N_FRC_CARDIACA':da.N_FRC_CARDIACA,
											'N_FRC_RESPIRATORIA':da.N_FRC_RESPIRATORIA,
											'N_TEMPERATURA':da.N_TEMPERATURA,
											'N_PESO':da.N_PESO,
											'N_TALLA':da.N_TALLA,
											'N_SATURACION':Number(da.N_SATURACION)
										}
									}
									$scope.calcularIMC();
									$scope.signosAnteriores = " ** SIGNOS VITALES TRIAGE";
								}

							}).error(function(da){});

						}
					}
					else{
						consultas.getSignosVitalesAnteriores($localStorage.paciente.expediente)
						.success(function(d){
							$scope.nuevo={
								'EXPLORACION_FSC':{
									'N_TSN_ART_SISTOLICA':d[0].N_TSN_ART_SISTOLICA,
									'N_TSN_ART_DIASTOLICA':d[0].N_TSN_ART_DIASTOLICA,
									'N_FRC_CARDIACA':d[0].N_FRC_CARDIACA,
									'N_FRC_RESPIRATORIA':d[0].N_FRC_RESPIRATORIA,
									'N_TEMPERATURA':d[0].N_TEMPERATURA,
									'N_PESO':d[0].N_PESO,
									'N_TALLA':d[0].N_TALLA,
									'C_DESCRIPCION':d[0].C_DESCRIPCION,
									'N_SATURACION': Number(d[0].N_SATURACION),
									'N_IMC':d[0].N_IMC,
									'C_CLASIFICACION':d[0].C_CLASIFICACION,
									'C_OBS_ENFERMERIA':d[0].C_OBS_ENFERMERIA,
									'EXPLORACION':d[0].C_DESCRIPCION
								}
							}
							$scope.signosAnteriores = " ** SIGNOS VITALES ANTERIORES";
						})
		 				.error(function(d){});
					}
				}
				$scope.signosAnteriores = "";
			}
			$scope.nuevo.C_INTERPRETACION = data[0].nota.C_INTERPRETACION_LABS;
			$scope.nuevo.RESUMEN_DEL_INTERROGATORIO = {'C_ANAMNESIS':data[0].nota.C_ANAMNESIS, C_MOTIVO_DE_CONSULTA:data[0].nota.C_MOTIVO_DE_CONSULTA};
			$scope.nuevo.NOTAS_MEDICAS = {'C_PRONOSTICO':data[0].nota.C_PRONOSTICO};
			$scope.nuevo.PROCEDIMIENTOS = {C_PROCEDIMIENTOS_REALIZADOS : data[0].nota.C_PROCEDIMIENTOS_REALIZADOS};
			$scope.nuevo.C_PLAN = data[0].nota.C_PLAN;
			$scope.nuevo.C_DESTINO_PACIENTE = data[0].nota.C_DESTINO_PACIENTE;
			if(data[0].fisicas != undefined)
				$scope.nuevo.EXPLORACION_FSC.C_ESTADO_MENTAL = data[0].fisicas.N_ESTADO_MENTAL;
			$scope.nuevo.EXPLORACION_FSC.C_ANALISIS = data[0].nota.C_ANALISIS;
			if(data[0].laboratorio.length>0){ //Datos de laboratorio
				var estudiosL = _.map(data[0].laboratorio, function (el, key) {
	                return{
	                	'cDescripcion' : el.C_DESCRIPCION,
	                	'ID' : el.NP_SRV_PRODUCTO,
	                	'C_MOTIVO_SOLICITUD' : el.C_MOTIVO_SOLICITUD,
	                	'dCosto' : el.D_COSTO,
	                	'idEstudio' : el.NP_ID
	                }
	            });
				$scope.datosEstudios = estudiosL;
				$scope.datosEstudiosAnteriores = JSON.parse( JSON.stringify( $scope.datosEstudios ) );
				$scope.lengthAnterior = $scope.datosEstudios.length;
				$scope.catDatosLaboratorio = _.map($scope.catDatosLaboratorio, function (el, key) {
	                return{
	                	'cDescripcion' : el.cDescripcion,
	                	'ID' : el.ID
	                }
	            });
				$scope.catDatosLaboratorio = _.difference($scope.catDatosLaboratorio,$scope.datosEstudios);
				$scope.isEstudio = true;

			}
			//DIAGNOSTICO PRIMARIO
			if(data[0].nota.NF_CIE_10_C_DESCRIPCION != undefined){
				if(data[0].nota.NF_CIE_10_C_DESCRIPCION !== "SIN DIAGNOSTICO" && data[0].nota.NF_CIE_10_C_DESCRIPCION !== "NO DEFINIDO" && data[0].nota.NF_CIE_10 > 0){
					$scope.itemDiagnostico1 = {'C_DESCRIPCION':data[0].nota.NF_CIE_10_C_DESCRIPCION,'NP_ID':data[0].nota.NF_CIE_10};
					$scope.selectedItemDiagnostico1  = $scope.itemDiagnostico1;
					$scope.selectedItemChangeDiagnostico1($scope.selectedItemDiagnostico1);
				}
			}

			//Pronóstico
			$scope.NF_ESTATUS1 = false;
			$scope.NF_ESTATUS2 = false;
			$scope.NF_ESTATUS3 = false;
			$scope.NF_ESTATUS4 = false;
			if(data[0].nota.N_ESTATUS_PRONOSTICO != undefined){
				if(data[0].nota.N_ESTATUS_PRONOSTICO === 1){$scope.NF_ESTATUS1 = true; $scope.NF_ESTATUS3 = true;}
				else if(data[0].nota.N_ESTATUS_PRONOSTICO === 2){$scope.NF_ESTATUS1 = true; $scope.NF_ESTATUS4 = true;}
				else if(data[0].nota.N_ESTATUS_PRONOSTICO === 3){$scope.NF_ESTATUS2 = true; $scope.NF_ESTATUS3 = true;}
				else if(data[0].nota.N_ESTATUS_PRONOSTICO === 4){$scope.NF_ESTATUS2 = true; $scope.NF_ESTATUS4 = true;}
			}

			//ORDEN MÉDICA
			$scope.C_DIETA = "";
			$scope.C_SOLUCIONES = "";
			$scope.C_MEDIDAS_GENERALES = "";
			if(data[0].nota.C_DIETA != undefined){
				$scope.C_DIETA = data[0].nota.C_DIETA;
			}
			if(data[0].nota.C_SOLUCIONES != undefined){
				$scope.C_SOLUCIONES = data[0].nota.C_SOLUCIONES;
			}
			if(data[0].nota.C_MEDIDAS_GENERALES != undefined){
				$scope.C_MEDIDAS_GENERALES = data[0].nota.C_MEDIDAS_GENERALES;
			}

			consultas.getEstudiosAnteriores($localStorage.paciente.idCita)
			.success(function(data){
				if(data.length>0){
					$scope.estudiosAnteriores = data;
					$scope.existAnteriores = true;
				}
			})
			.error(function(data){
				mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
			});

			if(data[0].cie.length > 0){
				$scope.datosSecundarios = _.map(data[0].cie, function (el, key) {
	                return{
	                	'C_DESCRIPCION' : el.C_DESCRIPCION,
	                	'NP_ID' : el.NF_CIE10
	                }
	            });
				$scope.isDiagnostico = true;
			}
			if($localStorage.paciente.tipo === 'EVOLUCION Y ALTA'){
				consultas.getDiagnosticoIngreso($localStorage.paciente.expediente)
				.success(function(data){
					if(data.length>0){
						$scope.C_DIAGNOSTICO_INGRESO = data[0].C_ANAMNESIS;
					}
					else{
						$scope.C_DIAGNOSTICO_INGRESO = "*** NO SE TIENE UN DIAGNÓSTICO DE INGRESO ***";
					}

				})
				.error(function(data){
					mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
				});
			}
			//Carga el id de la receta
			$scope.reload = false;
			if(data[0].nota.NF_RECETA != undefined){
				$scope.reload = true;
				$scope.idReceta = data[0].nota.NF_RECETA;
			}
			defered.resolve();
		}
		else{
			defered.reject();
		}
		return promise;
  	};

	$scope.querySearch = function(query){
		if(query != undefined){
			if(query.length>3){
				return _.filter($scope.catDatosLaboratorio,function(e){
					return e.cDescripcion.toUpperCase().indexOf(query.toUpperCase())>=0;
				});
			}
		}
	};

	$scope.querySearchDiagnostico1 = function(query){
		if($scope.diagnosticoPrincipal != undefined){
			$scope.catCIE10.push($scope.diagnosticoPrincipal);
		}
		$scope.diagnosticoPrincipal = undefined;
		if(query.length>0){
			var CIE10_descr = _.filter($scope.catCIE10,function(e){
				return e.C_DESCRIPCION.toUpperCase().indexOf(query.toUpperCase())>=0;
			});
			var CIE10_clave = _.filter($scope.catCIE10,function(e){
				if(e.C_CLAVE != undefined)
					return e.C_CLAVE.toUpperCase().indexOf(query.toUpperCase())>=0;
			});
			return _.union(CIE10_descr,CIE10_clave);
		}
	};

	$scope.querySearchDiagnostico2 = function(query){
		if(query.length>0){
			var CIE10_descr = _.filter($scope.catCIE10,function(e){
				return e.C_DESCRIPCION.toUpperCase().indexOf(query.toUpperCase())>=0;
			});
			var CIE10_clave = _.filter($scope.catCIE10,function(e){
				if(e.C_CLAVE != undefined)
					return e.C_CLAVE.toUpperCase().indexOf(query.toUpperCase())>=0;
			});
			return _.union(CIE10_descr,CIE10_clave);
		}
	};

  	$scope.selectedItemChange = function(item) {
  		$scope.addSolicitudEstudio(item);
  		$scope.searchText = ' ';
	};

	$scope.selectedItemChangeDiagnostico1 = function(item) {
		if(item === undefined && $scope.diagnosticoPrincipal != undefined){
			$scope.catCIE10.push($scope.diagnosticoPrincipal);
		}
		$scope.diagnosticoPrincipal = item;
		if($scope.diagnosticoPrincipal != undefined){
		  	$scope.catCIE10 = _.filter($scope.catCIE10, function(e){
				return e.NP_ID !== $scope.diagnosticoPrincipal.NP_ID;
			});
			$scope.citaPrimeraVez($localStorage.paciente.idCita,$scope.diagnosticoPrincipal.NP_ID);
		}
	};

	$scope.selectedItemChangeDiagnostico2 = function(item) {
		$scope.addDiagnosticoSecundario(item);
		$scope.searchDiagnostico2 = ' ';
	};

	$scope.changeEstatus = function(tipo){
		switch(tipo){
			case 1: $scope.NF_ESTATUS2 = false;
				break;
			case 2: $scope.NF_ESTATUS1 = false;
				break;
			case 3: $scope.NF_ESTATUS4 = false;
				break;
			case 4: $scope.NF_ESTATUS3 = false;
				break;
		};
	};

	$scope.medicinaLegal = function(){
		$state.go("ministerioPublico");
	};

	$scope.calcularIMC = function(){
    	if(($scope.nuevo.EXPLORACION_FSC.N_PESO != undefined && $scope.nuevo.EXPLORACION_FSC.N_TALLA != undefined) && ($scope.nuevo.EXPLORACION_FSC.N_PESO != '' && $scope.nuevo.EXPLORACION_FSC.N_TALLA != '')){
    		$scope.nuevo.EXPLORACION_FSC.N_IMC = ($scope.nuevo.EXPLORACION_FSC.N_PESO / ($scope.nuevo.EXPLORACION_FSC.N_TALLA * $scope.nuevo.EXPLORACION_FSC.N_TALLA)).toFixed(2);
    		$scope.nuevo.EXPLORACION_FSC.C_CLASIFICACION = _.filter($scope.clasificacionIMC ,function(e){
	    		return $scope.nuevo.EXPLORACION_FSC.N_IMC >= parseFloat(e.LIMITE_INFERIOR) && $scope.nuevo.EXPLORACION_FSC.N_IMC <= parseFloat(e.LIMITE_SUPERIOR);
	    	});

	    	if($scope.nuevo.EXPLORACION_FSC.C_CLASIFICACION.length>0){
	    		$localStorage.C_CLASIFICACIONID = $scope.nuevo.EXPLORACION_FSC.C_CLASIFICACION[$scope.nuevo.EXPLORACION_FSC.C_CLASIFICACION.length-1].NP_ID;
	    		$scope.nuevo.EXPLORACION_FSC.C_CLASIFICACION = $scope.nuevo.EXPLORACION_FSC.C_CLASIFICACION[$scope.nuevo.EXPLORACION_FSC.C_CLASIFICACION.length-1].C_DESCRIPCION;
	    	};
    	}
    };

    $scope.cargaLlave = function (fileContent){
		$scope.fe.FIRMA.keyPriv = fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
		$scope.fe.FIRMA.certificado = fileContent;
    };

    $scope.generaReporteReceta = function(idReceta){

			indicacionTerapeutica.getReporteReceta({NP_RECETA : idReceta})
			.then(function(data){
				var file = new Blob([data], {type: 'application/pdf'});
            	var fileURL = URL.createObjectURL(file);
            	reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
			})
			.catch(function(data){
			});


    };

    $scope.comenzarCitaUrgencias = function(){
		$localStorage.selectedMedicamentos = undefined;
    	consultas.startCita($localStorage.registroUrgencias)
		.success(function(data){
			if(data.success){
				mensajes.alerta('AVISO','LA CITA HA COMENZADO','ACEPTAR!');
				$localStorage.notaMedica = {
					NP_NOTA_MEDICA : data.Mensaje
				};
				if($localStorage.paciente.tipo === 'EVOLUCION Y ALTA'){ //CARGA EL DIAGNÓSTICO DE INGRESO
					consultas.getDiagnosticoIngreso($localStorage.paciente.expediente)
					.success(function(data){
						if(data.length>0){
							$scope.C_DIAGNOSTICO_INGRESO = data[0].C_ANAMNESIS;
						}
						else{
							$scope.C_DIAGNOSTICO_INGRESO = "*** NO SE TIENE UN DIAGNÓSTICO DE INGRESO ***";
						}
					})
					.error(function(data){
						mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
					});
				}

				if($localStorage.paciente.tipo === "ingreso" || $localStorage.paciente.tipo === 'TRIAGE'){
					if($localStorage.paciente.idTriage != undefined){
						consultas.getSignosVitalesTriage($localStorage.paciente.idTriage).success(function(da){
							if(da.N_TSN_ART_SISTOLICA != undefined){
								$scope.nuevo={
									'EXPLORACION_FSC':{
										'N_TSN_ART_SISTOLICA':da.N_TSN_ART_SISTOLICA,
										'N_TSN_ART_DIASTOLICA':da.N_TSN_ART_DIASTOLICA,
										'N_FRC_CARDIACA':da.N_FRC_CARDIACA,
										'N_FRC_RESPIRATORIA':da.N_FRC_RESPIRATORIA,
										'N_TEMPERATURA':da.N_TEMPERATURA,
										'N_PESO':da.N_PESO,
										'N_TALLA':da.N_TALLA,
										'N_SATURACION': Number(da.N_SATURACION)
									}
								}
								$scope.calcularIMC();
								$scope.signosAnteriores = " ** SIGNOS VITALES TRIAGE";
							}

						}).error(function(da){});

					}
				}
				else{
					consultas.getSignosVitalesAnteriores($localStorage.paciente.expediente)
					.success(function(d){
						$scope.nuevo={
							'EXPLORACION_FSC':{
								'N_TSN_ART_SISTOLICA':d[0].N_TSN_ART_SISTOLICA,
								'N_TSN_ART_DIASTOLICA':d[0].N_TSN_ART_DIASTOLICA,
								'N_FRC_CARDIACA':d[0].N_FRC_CARDIACA,
								'N_FRC_RESPIRATORIA':d[0].N_FRC_RESPIRATORIA,
								'N_TEMPERATURA':d[0].N_TEMPERATURA,
								'N_PESO':d[0].N_PESO,
								'N_TALLA':d[0].N_TALLA,
								'C_DESCRIPCION':d[0].C_DESCRIPCION,
								'N_SATURACION': Number(d[0].N_SATURACION),
								'N_IMC':d[0].N_IMC,
								'C_CLASIFICACION':d[0].C_CLASIFICACION,
								'C_OBS_ENFERMERIA':d[0].C_OBS_ENFERMERIA,
								'EXPLORACION':d[0].C_DESCRIPCION
							}
						}
						$scope.calcularIMC();
						$scope.signosAnteriores = " ** SIGNOS VITALES ANTERIORES";
					})
	 				.error(function(d){});
				}
			}
			else if(!data.success){
				$scope.isBegin = false;
				mensajes.alerta('ERROR',data.Mensaje,'ACEPTAR!');
			}
		})
		.error(function(data){
			$scope.isBegin=false;
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
    };

    function guardaIndicacion(indService){
		indicacionTerapeutica.guardaReceta(indService)
		.success(function(data){
			$scope.idReceta = data.NP_RECETA;
			if($localStorage.notaMedica !== undefined){ //Cuando sea de Urgencias
				$scope.recetaUrgencias = true;
				guardaOrdenMedica();
			}
			else{ //Termina si es de Consulta Externa
				$scope.terminar();

			}
		})
		.error(function(data){

		});
    };

    function guardaOrdenMedica(){
    	if($localStorage.paciente.tipo !== 'EVOLUCION Y ALTA' && $localStorage.paciente.tipo !== 'TRIAGE' && !$scope.isVerde){//Entra para la orden médica
			if($scope.C_DIETA == undefined || $scope.C_SOLUCIONES == undefined){
				mensajes.alerta("AVISO","DEBES COMPLEMENTAR EL CAMPO DE DIETA Y SOLUCIONES","ACEPTAR");
				return null;
			}
			else{
				$scope.ORDENMED= {ORDEN_MEDICA :{C_DIETA : $scope.C_DIETA ,
					C_SOLUCIONES : $scope.C_SOLUCIONES ,
					C_MEDIDAS_GENERALES : $scope.C_MEDIDAS_GENERALES},
					NF_RECETA : $scope.idReceta
				};
				$scope.ORDENMED.NOTAS_MEDICAS = $localStorage.notaMedica;
				consultas.guardaOrdenMedica($scope.ORDENMED).success(function(data){//Guarda Nota Médica
	  				if(data.success){
	  					$localStorage.notaMedica.NP_NOTA_MEDICA = data.Mensaje;
	  					reportes.getReporte(baseURL + 'ordenmedica/' + $localStorage.notaMedica.NP_NOTA_MEDICA, '_blank', 'width=1000, height=800');
	  				}
	  			});
			}
		}
    };


	// ***************************************************************************
	// Verificar si es medicina legal y mostrar leyenda "PERTENECE A MEDICINA LEGAL"
	// ***************************************************************************

	pacientes.perteneceAMedicinaLegal()
	.then(function(res){
		if($localStorage.paciente.tipo === 'EVOLUCION Y ALTA'){
			/*VALIDAR SI EL PACIENTE A REALIZAR EVOLUCIÓN Y ALTA TIENE INTERCONSULTA*/
			var msgInterconsulta = " Y ES NECESARIO REPORTAR AL MINISTERIO PÚBLICO EL ALTA DEL PACIENTE";
			$scope.medLegal = true;
			validaInterconsulta(msgInterconsulta);
		}
		$scope.tituloNota += res;
	})
	.catch(function(err){
		if($localStorage.paciente.tipo === 'EVOLUCION Y ALTA'){
			$scope.medLegal = false;
			validaInterconsulta('');
		}
	});

	// *************************************************************************

	function validaInterconsulta(msg){
		consultas.getSolicitudesInterconsulta('amarillos')//INTERCONSULTA
    	.success(function(data){
    		var go1 = true;
    		_.filter(data, function(e){
    			var exp = e.NF_PACIENTE.replace("/","")
    			if($localStorage.paciente.expediente+'' === exp){
    				//mensajes.alerta("AVISO",'EL PACIENTE TIENE UNA INTERCONSULTA SIN REALIZAR'+msg,"ACEPTAR");
    				mensajes.confirmacion("AVISO",'no se ha realizado la Interconsulta ¿Deseas continuar?'.toUpperCase()+msg,"ACEPTAR")
	              	.then(function() {

	              	}).catch(function() {
	              		$state.go('bienvenido');
	              	});
    				go1=false;
    			}
				return '';
			});
			if(go1){
				consultas.getSolicitudesInterconsulta()//INTERCONSULTA
              	.success(function(data){
              		var go2 = true;
              		_.filter(data, function(e){
              			var exp2 = e.NF_PACIENTE.replace("/","");
	        			if($localStorage.paciente.expediente+'' === exp2){
	        				mensajes.confirmacion("AVISO",'no se ha realizado la Interconsulta ¿Deseas continuar?'.toUpperCase()+msg,"ACEPTAR")
			              	.then(function() {

			              	}).catch(function() {
			              		$state.go('bienvenido');
			              	});
	        				go2=false;
	        			}
						return '';
					});
					if(go2){
						consultas.getSolicitudesInterconsulta('rojos')//INTERCONSULTA
	          			.success(function(data){
	          				var go3 = true;
	          				_.filter(data, function(e){
	          					var exp3 = e.NF_PACIENTE.replace("/","");
			        			if($localStorage.paciente.expediente+'' === exp3){
			        				mensajes.confirmacion("AVISO",'no se ha realizado la Interconsulta ¿Deseas continuar?'.toUpperCase()+msg,"ACEPTAR")
					              	.then(function() {

					              	}).catch(function() {
					              		$state.go('bienvenido');
					              	});
					        		go3 = false;
			        			}
								return '';
							});
	          				if(go3 && $scope.medLegal){
	          					mensajes.alerta("AVISO",'ES NECESARIO REPORTAR AL MINISTERIO PÚBLICO EL ALTA DEL PACIENTE',"ACEPTAR");
	          				}
	          			});
					}
				});
			}
    	});
	};

	$scope.anterior = function(){
		$scope.selectedIndex -= 1
	};

	
	medicos.getMedicoInformation(usuario.usuario.NP_EXPEDIENTE, 0).success(function(data){
		$scope.medicoPermissions = data;
		$scope.canRequest = false;
		if($scope.medicoPermissions.N_ORDENES === 1){
			$scope.canRequest = true;
		}
		else{
			$scope.canRequest = false;
		}
   	}).error(function(err){
     	console.error(err);
   	});

	$scope.saltar = function(){
		$scope.selectedIndex += 1;
		$scope.activeSelected = $scope.selectedIndex;
		$localStorage.selectedIndex = $scope.selectedIndex;
	};

	$scope.salir = function(){
		$state.go('bienvenido');
	};

	$scope.confirmacion=function(){
		var confirm = $mdDialog.confirm()
		    .title('AVISO')
		    .content('¿DESEAS TERMINAR LA CONSULTA?')
			.ariaLabel('Lucky day')
		    .targetEvent()
		    .ok('ACEPTAR!')
		    .cancel('CANCELAR');
			$mdDialog.show(confirm).then(function() {
				if(indicacionTerapeutica.firma.FIRMA.keyPriv != undefined && indicacionTerapeutica.firma.FIRMA.certificado != undefined){
					var firma = indicacionTerapeutica.firma;
					firma.NOTAS_MEDICAS = {NP_NOTA_MEDICA : $localStorage.idNota};
					consultas.setFirmaElectronica(firma)
					.success(function(data){
						if (data.success){
			 				if(data.success===true){
			 					reportes.getReporte(baseURL + 'reporteNota/'+data.Mensaje, '_blank', 'width=800, height=640');
								consultas.switchCita($localStorage.paciente.idCita)
								.success(function(data){
									$state.go('consultaAgendaMedica');
									$localStorage.paciente = undefined;
									$localStorage.selectedIndex = 0;
									if(indicacionTerapeutica.havePermissions)
										$scope.generaReporteReceta($scope.idReceta);
								})
								.error(function(data){
									mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
								});
							}
							else if(data.success===false){
								mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR');
							}
				 		}
				 		else{
				 			mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR');
				 		}
			 		})
			 		.error(function(data){
						mensajes.alerta('ERROR','SERVICIO FIRMA ELECTRÓNICA NO DISPONIBLE','ACEPTAR');
					});
			 	}
			}, function() {
			});
    }

	//Ver nota antes de imprimir
	$scope.verNota=function(){
		var defered = $q.defer();
        var promise = defered.promise;
		$mdDialog.show({
	            controller:DialogControllerPrevisualizacionNota,
	            templateUrl: '/application/views/consulta/previsualizacionNota.html',
	            parent: angular.element(document.body),
	            clickOutsideToClose:true,
	             locals:{
	           		 datos: $scope.nuevo,
	           		 scopes: $scope,
	        	 },
	        	resolve : {
		              val : function() {
		                  return $scope.val;
		              }
		        }
	          })
	          .then(function(answer) {
	            $scope.status = 'You said the information was "' + answer + '".';
	            if(answer.aux==true){
	            	mensajes.alerta('ALERTA','SE REQUIERE DE LA FIRMA PARA FINALIZAR','ACEPTAR');
	            }
	            defered.resolve(answer.aux);
	          }, function() {
	            $scope.status = 'You cancelled the dialog.';
	            console.log("aqui");
	             defered.resolve(false);
	          });
	      return promise; 
	}
	 function DialogControllerPrevisualizacionNota($scope, $mdDialog,datos,scopes) {
	 	$scope.val={aux:false};
	 	$scope.esPrimeraVez=scopes.esPrimeraVez;
	 	$scope.diagnosticoPrincipal=scopes.diagnosticoPrincipal;
	 	$scope.datosEstudios=scopes.datosEstudios;
	 	$scope.isEstudio=scopes.isEstudio;
	 	$scope.NF_ESTATUS1=scopes.NF_ESTATUS1;
	 	$scope.NF_ESTATUS2=scopes.NF_ESTATUS2;
	 	$scope.NF_ESTATUS3=scopes.NF_ESTATUS3;
	 	$scope.NF_ESTATUS4=scopes.NF_ESTATUS4;
	 	$scope.existAnteriores=scopes.existAnteriores;
	 	$scope.isDiagnostico=scopes.isDiagnostico;
	 	$scope.datosSecundarios=scopes.datosSecundarios;
	    $scope.nuevo=datos;	  
	    $scope.paciente=$localStorage.paciente;
	    var band=indicacionTerapeutica.getMedicamentos().band;
	    if(band){
	    	$scope.listaMedicamentos=indicacionTerapeutica.getMedicamentos().medicamentos;
            $scope.C_PLAN=indicacionTerapeutica.getMedicamentos().plan;
            $scope.C_TRATAMIENTO=indicacionTerapeutica.getMedicamentos().tratamiento;
	    }else{
	    	consultas.getIndicacionTereapeutica(scopes.nota.nota.NF_RECETA)
		    	.success(function(data){
		    		if(data != undefined){
		    			$scope.medicamentos = data;
		    			console.log(data);
		    			console.log($scope.medicamentos.MEDICAMENTOS);
		    			$scope.listaMedicamentos=_.map($scope.medicamentos.MEDICAMENTOS,function(a){
			             return{
			              'NP_MEDICAMENTO' : a.C_NOMBRE_MEDICAMENTO,
			              'indicaciones': a.C_INDICACIONES,
			              'meses'        :a.N_MESES,
			              'dias': a.N_DIAS,
			              'saec':$scope.parseSAEC(a.N_SAEC),
			             } 
			            });
						console.debug("VIENE recuperado");
		    			console.log($scope.listaMedicamentos);
		    		}
	    	})
	    	.error(function(data){
	    	});
	    }

    	$scope.cancelar = function() {
    		console.log("cancelando");
	           $mdDialog.hide($scope.val);
	    };
	    $scope.canceladoAutomatico = function() {
	          $mdDialog.hide($scope.val);
	    };
		$scope.finalizar=function(){
			$scope.val.aux=true;
			$scope.canceladoAutomatico();
		}

		$scope.queryMedicamentos = {
		    filter: '',
		    order: '',
		    limit: 15,
		    page: 1
    	};
    	$scope.queryEstudios={
		    filter: '',
		    order: '',
		    limit: 15,
		    page: 1
    	};
    	$scope.queryDiagnostico={
		    filter: '',
		    order: '',
		    limit: 15,
		    page: 1
    	};

    	$scope.parseSAEC = function(SAEC){
    		return SAEC === 1 ? 'S' : 'N';
        };
    
 	};

  }]);
})();
