(function(){
'use strict';

/**
* @ngdoc function
* @name ECEangular.module('expediente').controller:ConsultaCancelarRecetaCtrl
* @description
* # ConsultaCancelarRecetaCtrl
* Controller of the expediente
*/
angular.module('expediente')
.controller('ConsultaCancelarRecetaCtrl',['$scope', '$http','$q','mensajes', '$mdDialog', '$mdMedia', 'indicacionTerapeutica', 'peticiones', '$window', '$state', '$localStorage', 'usuario', 'medicos','reportes',
function ($scope, $http, $q, mensajes, $mdDialog, $mdMedia, indicacionTerapeutica, peticiones, $window, $state, $localStorage, usuario, medicos,reportes) {
    $scope.isEditable = false;
    $scope.showBtnBuscar = true;
    $scope.btnCancelarReceta = true;
    $scope.nuevaIndicacion = false;
    $scope.cancelar = false;
    $scope.generaReceta = false;
    $scope.tipo = 'RECETA';
    $scope.notaMedicaJ={};


    $scope.fe = {
      FIRMA : {},
    };
    $scope.inicializaPagina= function(){
         $scope.tipo = 'RECETA';      
    }

    $scope.limpiar = function(){
      $scope.isEditable=false;
      $scope.C_FOLIO_RECETA='';
      $scope.cancelar = false;
       $scope.generaReceta = false;
       $scope.nuevaIndicacion = false;

    };
    $scope.opcion = function(folio){
      var statusReceta='';
      var folioReceta='';
      if ($scope.tipo=='RECETA'){
        $scope.obtenReceta(folio);
        $scope.esReceta = true;
      }else{
          if($scope.C_FOLIO_RECETA!=''){
            $http.get(baseURL + "notamedica/urgencias/" + $scope.C_FOLIO_RECETA)
            .success(function(data){
              //console.log(data);
              if(data.length>0){
                statusReceta=data[0].nota.NF_ESTATUS_RECETA;
                folioReceta=data[0].nota.NF_RECETA;
               
                if (statusReceta==2) {
                  $scope.generaReceta = true;
                  $scope.nuevaIndicacion = false;
                  $scope.cancelar = true;
                  $scope.isEditable = true;
                  $scope.showBtnBuscar = false;
                  $scope.esReceta = false;
                  $scope.receta={};
                  
                }else{
                  mensajes.alerta('AVISO','EXISTE RECETA : '+folioReceta,'ACEPTAR!');
                  $scope.tipo = 'RECETA';
                  $scope.C_FOLIO_RECETA=folioReceta;
                }
              }else{
                mensajes.alerta('AVISO','EL FOLIO NO EXISTE','ACEPTAR!');
              }
             
            }).error(function(){
              mensajes.alerta('AVISO','EL FOLIO NO EXISTE','ACEPTAR!');
              });

            
            

          }else{
            mensajes.alerta('AVISO','AGREGUE FOLIO','ACEPTAR!');
          }
      }


    };

    $scope.obtenReceta = function(folio) {


      medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE).success(function(data){

        $http.get(baseURL + "receta/" + folio)
        .success(function(result){
          //console.log(result);

          if (!result.hasOwnProperty('B_CANCELAR'))
          mensajes.alerta('ERROR','NO EXISTE UNA RECETA CON EL FOLIO ESPECIFICADO','ACEPTAR!');
          else
          if(data.ESPECIALIDADES.NF_ESPECIALIDAD === result.DATOS_MEDICO.ESPECIALIDADES.NF_ESPECIALIDAD){


            /// LA RECETA AUN ESTA ABIERTA
            if (result.B_CANCELAR){

              //Verificar vigencia de la receta
              var fechaCreacion = moment(result.F_FECHA, 'DD/MM/YYYY HH:mm:ss');

              if( !moment().isAfter(moment(fechaCreacion.add(2, 'days').toString())) ){

                $scope.receta = result;

                  if (result.N_HOSPITALIZACION==2){
                    //console.log("result.N_HOSPITALIZACION==2");
                    $scope.isUrgencias = 'true' ;
                    $scope.isHospitalizacion='false';
                  }else
                    { //console.log("result.N_HOSPITALIZACION=!=2");
                      $scope.isUrgencias = 'false';
                      $scope.isHospitalizacion='true';
                    }

                //console.log("urgencias:"+$scope.isUrgencias);

                $localStorage.paciente = {
                  expediente: result.DATOS_PACIENTE.NP_EXPEDIENTE.replace("/", "")
                }


                indicacionTerapeutica.C_TIPO_RECETA = $scope.receta.C_TIPO_RECETA;
                $scope.isEditable = true;
                $scope.showBtnBuscar = false;
              }else {
                mensajes.alerta('LA VIGENCIA ES DE 24 HRS.','LA RECETA SÓLO SE PODIA CANCELAR ANTES DE: ' + fechaCreacion.subtract(1, 'days').format('DD/MM/YYYY'),'ACEPTAR!');
              }
            }
            else
            mensajes.alerta('ERROR', 'ESTA RECETA YA A SIDO CANCELADA, NO ES POSIBLE CANCELAR DE NUEVO.','ACEPTAR!');
          }else{
            mensajes.alerta('ERROR', 'NO TIENE AUTORIZACIÓN PARA CANCELAR ESTA RECETA', 'ACEPTAR');
          }
          //console.log("$scope.isUrgencias "+$scope.isUrgencias);
          //console.log("$scope.isHospitalizacion: "+$scope.isHospitalizacion);

        })
        .error(function(data) {

          mensajes.alerta('ERROR',data.error,'ACEPTAR!');
        });

      }).error(function(){

      });



    }

    $scope.guardaNotaMedica = function() {

      var indService = indicacionTerapeutica.receta;
      var bandera;
      
      indService.NF_MEDICO = usuario.NP_EXPEDIENTE;
      $scope.receta.MEDICAMENTOS = indService.MEDICAMENTOS;
      $scope.receta.C_TRATAMIENTO = indService.C_TRATAMIENTO;
      

      if ($scope.esReceta) {
        $scope.receta.NP_RECETA_CANCELAR = $scope.receta.NP_RECETA;
          
      }else{
        $scope.receta.NF_NOTA_MEDICA = $scope.C_FOLIO_RECETA;
        $scope.receta.NF_MEDICO = usuario.usuario.NP_EXPEDIENTE;
      }





      
      var defered = $q.defer();
      var promise = defered.promise;

    


      peticiones.postDatos("receta",$scope.receta).then(function(data){
        //console.log("entro a post receta");        
        indicacionTerapeutica.getReporteReceta({
          NP_RECETA_CANCELAR: $scope.receta.NP_RECETA_CANCELAR,
          NP_RECETA: data.NP_RECETA
        }).then(function(data){
          //console.log("entro a getReporte");
          var file = new Blob([data], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          $state.go('bienvenido');
          defered.resolve(true);

        }).catch(function(err){
          //console.log(err);
          mensajes.alerta(err.type,err.message, 'ACEPTAR');          
          defered.reject(false);
        });
        //console.debug("data",data)
      }).catch(function(error){console.error("error",error)})

        return defered.promise;
      }

      $scope.showIndicacionTerapeutica = function(ev) {
        if ($scope.receta.C_MOTIVO_CANCELACION == undefined){
          mensajes.alerta('ERROR','DEBE ESPECIFICAR EL MOTIVO DE LA CANCELACIÓN','ACEPTAR!');
        }
        else{
          $scope.generaReceta = true;
          $scope.nuevaIndicacion = true;
          $scope.cancelar = true;
        }

      };

      $scope.hidenIndicacionTerapeutica = function(ev) {
        $scope.nuevaIndicacion = false;
        $scope.cancelar = false;
      }

      function trim(cadena){
        var retorno=cadena.replace(/^\s+/g,'');
        retorno=retorno.replace(/\s+$/g,'');
        return retorno;
      }

      $scope.parseSAEC = function(SAEC){
        return SAEC === 1 ? 'S' : 'N';
      };


      $scope.cargaLlave = function (fileContent){

        $scope.fe.FIRMA.keyPriv = fileContent;
      };

      $scope.cargaCertificado = function (fileContent){
        $scope.fe.FIRMA.certificado = fileContent;
      };

    }]);
})();
