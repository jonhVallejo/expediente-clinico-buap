'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultaCancelsadCtrl
 * @description
 * # ConsultaCancelsadCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('ConsultaCancelsadCtrl', function ($scope,consultas,mensajes , pacientes , catalogos , $state , reportes , medicos, usuario) {
    
    $scope.first = true;
    $scope.existEstudios = false;
    $scope.isNewEstudio = false;

  	$scope.checkEstudio = function(notaMedica){
      if(!isNaN(notaMedica)){
        $scope.notaMedica = notaMedica;
        consultas.getEstudiosSolicitados($scope.notaMedica)
        .success(function(data){
          $scope.first = false;
          $scope.existEstudios = false;
          $scope.estudiosSolicitados = [];
          $scope.canDo = false;
          if(data.estudios != undefined){
            if(data.estudios.length > 0){
              $scope.estudiosSolicitados = data.estudios;
              $scope.paciente = data.paciente;
              $scope.paciente.nombreCompleto = $scope.paciente.C_PRIMER_APELLIDO + " " + $scope.paciente.C_SEGUNDO_APELLIDO + " " + $scope.paciente.C_NOMBRE;
              $scope.paciente.N_EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
              $scope.existEstudios = true;
              $scope.canDo = (moment(new Date()).format("DD/MM/YYYY HH:mm:SS") > $scope.paciente.T_FECHA_LIMITE) ? false : true;
              $scope.paciente.expediente = $scope.paciente.NP_EXPEDIENTE.replace("/","");
            }
          }
          //Verificar que el Médico logueado pertenezca a la misma especialidad que el que asignó los estudios
          medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
          .success(function(data){
            $scope.canDo = false;
            if(data.ESPECIALIDADES != undefined)
              if(data.ESPECIALIDADES.CF_ESPECIALIDAD != undefined)
                if(data.ESPECIALIDADES.CF_ESPECIALIDAD == $scope.paciente.C_ESPECIALIDAD)
                  $scope.canDo = true;
          })
          .error(function(err){});
        })
        .error(function(data){});
      }
      else{
        mensajes.alerta("AVISO","SOLO PUEDES INGRESAR VALORES NUMÉRICOS","ACEPTAR");
      }
  	};

  	$scope.deleteEstudioSolicitado = function(estudio){ //delete studio that has been requested and storaged in db
      $scope.NOTA = {
        "DATOS_DE_LABORATORIO_Y_GABINETE":
          {"C_SERVICIOS_PRODUCTOS":estudio.NP_ID+''},
          "NOTAS_MEDICAS":{"NF_AGENDA":$scope.notaMedica,
                            NF_MEDICO : usuario.usuario.NP_EXPEDIENTE
        }
      };
  		mensajes.confirmacion('AVISO','¿DESEAS ELIMINAR EL ESTUDIO SOLICITADO?','OK')
    	.then(function() {//true
        consultas.deleteSolicitudEstudio($scope.NOTA)
  		  .success(function(data){
				  if(data.success){
					  $scope.checkEstudio($scope.notaMedica);
				  }
          else{
            mensajes.alerta("AVISO",data.Mensaje.toUpperCase(),"ACEPTAR");
          }
		    }).error(function(data){});
    	}).catch(function(){});
  	};

    $scope.query = {
      filter: '',
      order: '',
      limit: 5,
      page: 1
    };

    $scope.addEstudio = function(){ //enabling form to add new studios
      $scope.isNewEstudio = true;
      catalogos.getCatalogos(2)
      .success(function(data){
        $scope.catDatosLaboratorio = data;
        $scope.catDatosLaboratorio = _.filter($scope.catDatosLaboratorio , function(a){
          var coincidence = false;
          _.filter($scope.estudiosSolicitados , function(b){
            if(a.ID == b.NP_SRV_PRODUCTO){
              coincidence = true;
            }
            return false;
          });
          return !coincidence;
        });
      });
      $scope.datosEstudios = [];
      $scope.isEstudio=false;      
    };

    $scope.addSolicitudEstudio = function(estudio){ //add studio to JSon for request studios
      if(estudio != undefined){
        //Validación de Estudios solicitados durante las últimas 24 horas
        consultas.getValidacionEstudios($scope.paciente.expediente , estudio.ID)
        .success(function(data){
          if(data.success){
            if(data.Mensaje == 1){ //ES PRIMERA VEZ
              $scope.isEstudio=true;
              $scope.datosEstudios.push(estudio);
              $scope.catDatosLaboratorio = _.difference($scope.catDatosLaboratorio,$scope.datosEstudios);
            }
            else if(data.Mensaje == 2){//
              mensajes.alerta("AVISO","ESTE ESTUDIO YA HA SIDO SOLICITADO EL DÍA DE HOY","ACEPTAR");
            }
          }
        })
        .error(function(data){

        });
      }
    };

    $scope.deleteSolicitudEstudio = function(ev,dato){ //deleting studios not requested
      $scope.datosEstudios = _.filter($scope.datosEstudios,function(e){
        return e.ID+''!==dato.ID+'';//DEVUELVE EL ESTUDIO A LA LISTA DE ESTUDIOS DISPONIBLES
      });
      $scope.catDatosLaboratorio.push(dato);
    };

    $scope.querySearch = function(query){
      if(query != undefined){
        if(query.length>3){
          return _.filter($scope.catDatosLaboratorio,function(e){
            return e.cDescripcion.toUpperCase().indexOf(query.toUpperCase())>=0;
          });
        }
      }
    };

    $scope.selectedItemChange = function(item) {
      $scope.addSolicitudEstudio(item);
      $scope.searchText = ' ';
    };

    $scope.saveEstudios = function(){ //storing new studios in db
      $scope.datosEstudios = _.map($scope.datosEstudios , function (el , key){
        el = {
          NP_SRV_PRODUCTO : el.ID,
          C_MOTIVO_SOLICITUD : el.C_MOTIVO_SOLICITUD};
        return el;

      })
      $scope.registro = {DATOS_DE_LABORATORIO_Y_GABINETE: {C_SERVICIOS_PRODUCTOS : $scope.datosEstudios} , 
        NOTAS_MEDICAS : {NP_NOTA_MEDICA: $scope.notaMedica,NF_MEDICO:usuario.usuario.NP_EXPEDIENTE}};
      consultas.upgradeEstudios($scope.registro)
      .success(function(data){
        if(data.success){
          reportes.getReporte(baseURL + 'solicitudGabinete/' + $scope.notaMedica , '_blank' , 'width=1000, height=800');
          mensajes.alerta("AVISO","ESTUDIOS AGREGADOS CORRECTAMENTE","ACEPTAR");
          $state.go('bienvenido');
        }
        else{
          mensajes.alerta("AVISO",data.Mensaje.toUpperCase(),"ACEPTAR");
        }
      })
      .error(function(err){console.log(err);});
    };

  });
