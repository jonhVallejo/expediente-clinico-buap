'use strict';

/**
* @ngdoc function
* @name ECEangular.module('expediente').controller:MinisteriopublicoCtrl
* @description
* # MinisteriopublicoCtrl
* Controller of the expediente
*/
var st;
angular.module('expediente')
.controller('MinisteriopublicoCtrl', function ($scope, pacientes, medicos, catalogos, usuario, $localStorage, mensajes, $location, $timeout, $http, peticiones, $window , reportes) {

  $scope.horaActual         = moment().format("hh:mm");
  $scope.fechaIngreso       = {};
  $scope.fechaNotificacion  = {};
  $scope.perfil             = {
    id: usuario.perfil.id,
    nombre:usuario.usuario.NOMBRE_COMPLETO,
    cedula:usuario.usuario.CEDULA
  };



  $scope.ts = {};

  if($scope.perfil.id === 10){
    $scope.ts.NOTIFICACION = {
      NF_AGENCIA: 0,
      CF_AGENCIA: '',
      CF_TRABAJO_SOCIAL: usuario.usuario.NOMBRE_COMPLETO,
      NF_TRABAJO_SOCIAL: usuario.usuario.NP_EXPEDIENTE
    };

    initTrabajoSocial();
  }

  $scope.datosDoctor={};

  $scope.datosTrabajoSocial={};

  $scope.agenciaMP = {};
  $scope.agenciasMP = [];

  catalogos.getCatalogos(29).success(function(data){


    $scope.agenciasMP = data;



  }).error(function(err){

  });


function initTrabajoSocial(){

  pacientes.getReporteMP().success(function(data){

    $scope.datosDoctor = data;

  }).error(function(err, response){
    console.log(response);
  });
};

$scope.verPaciente = function(){


  /*
  * PEDIR LA INFORMACION DEL PACIENTE Y SI NO TIENE NOTA DE EGRESO U OTRA INFORMACIÓN, REBOTARLO
  * PORQUE NO PODRIA ENTRAR EN ESTA ETAPA.
  */

  if($scope.perfil.id === 10){
    pacientes.getPacienteMinisterioPublico()
    .success(function(data){
      if(data.hasOwnProperty('NP_ID')){
        $scope.paciente=data;

        $scope.edadActual = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);

        $scope.fechaIngreso = moment($scope.paciente.F_FECHA_INGRESO, 'DD/MM/YYYY HH:mm:ss').format("D/M/YYYY");
        $scope.fechaNotificacion = moment().format('D/M/YYYY'); //$scope.paciente.NF_NOTA_EVOLUTIVA.F_FECHA, 'DD/MM/YYYY HH:mm:ss').format("D/M/YYYY");
      }
      else{
        mensajes.alerta('ERROR',data.Mensaje.toUpperCase(),'ACEPTAR!');
      }
    })
    .error(function(data, response){



      mensajes.alertaTimeout('ERROR',data.error,'ACEPTAR', 3000);


    });

  }else{
    pacientes.getInformacionPacienteMedicinaLegal().success(function(data){

      $scope.paciente=data;
      $scope.edadActual = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);

      $scope.fechaIngreso = moment($scope.paciente.F_FECHA_INGRESO, 'DD/MM/YYYY HH:mm:ss').format("D/M/YYYY");
      $scope.fechaNotificacion = moment().format('D/M/YYYY'); //$scope.paciente.NF_NOTA_EVOLUTIVA.F_FECHA, 'DD/MM/YYYY HH:mm:ss').format("D/M/YYYY");
    }).error(function(err){

      $timeout(function(){
        window.history.back();
      }, 1000);

      mensajes.alertaTimeout('ERROR',err.error,'ACEPTAR', 3000);

    });
  }

};


$scope.verDatosMedico = function(){

  /**
  sacar los datos del medico.
  **/




};


$scope.registrarDatos=function(){



  switch ($scope.perfil.id) {

    case 8: case 9: case 11:
    $scope.datosDoctor.NF_NOTA_MEDICA=$scope.paciente.NF_NOTA_INGRESO;
    $scope.datosDoctor.NF_MEDICO= parseInt(usuario.usuario.NP_EXPEDIENTE);
    $scope.datosDoctor.NF_PACIENTE= $scope.paciente.NP_EXPEDIENTE.substring(0,$scope.paciente.NP_EXPEDIENTE.length-5);

    pacientes.guardaPendientesDoctor($scope.datosDoctor)
    .success(function(data){
      if(data.success===true){


        mensajes.alertaTimeout('',data.message,'ACEPTAR!', 2500);

        $timeout(function(){
          window.history.back();
        }, 1000);
        // $location.path('/agenda/consultaPaciente/ministerioPublico');
      }
      else if(data.success===false){
        mensajes.alerta('AVISO',data.message.toUpperCase(),'ACEPTAR!');
      }
      else {
        mensajes.alerta('ERROR','NO SE PUDO REALIZAR EL REGISTRO','ACEPTAR!');
      }
    })
    .error(function(data){
      mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    });
    break;

    //
    // case 9:
    //
    // $scope.datosDoctor.NF_NOTA_MEDICA=$scope.paciente.NF_NOTA_INGRESO;
    // $scope.datosDoctor.NF_MEDICO= parseInt(usuario.usuario.NP_EXPEDIENTE);
    // $scope.datosDoctor.NF_PACIENTE= $scope.paciente.NP_EXPEDIENTE.substring(0,$scope.paciente.NP_EXPEDIENTE.length-5);
    //
    // pacientes.guardaPendientesDoctor($scope.datosDoctor)
    // .success(function(data){
    //   if(data.success===true){
    //     mensajes.alerta('','SE HAN REGISTRADO LOS DATOS','ACEPTAR!');
    //   }
    //   else if(data.success===false){
    //     mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
    //   }
    //   else {
    //     mensajes.alerta('ERROR','NO SE PUDO REALIZAR EL REGISTRO','ACEPTAR!');
    //   }
    // })
    // .error(function(data){
    //   mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    // });
    // break;



    case 10:



    $scope.ts.NOTIFICACION.NF_AGENCIA = $scope.agenciaMP.ID;
    $scope.ts.CF_AGENCIA = $scope.agenciaMP.cDescripcion;
    $scope.ts.NOTIFICACION.CF_AGENCIA = $scope.agenciaMP.cDescripcion;



    var jsonForPost = _.extend($scope.paciente, $scope.datosDoctor);
    jsonForPost = _.extend(jsonForPost, $scope.trabajoSocial);

    jsonForPost.NOTIFICACION = $scope.ts.NOTIFICACION;




    peticiones.putMethodPDF('trabajosocial/medicinalegal',jsonForPost)
    .success(function(data){



      var file = new Blob([data], {type: 'application/pdf'});
      var fileURL = URL.createObjectURL(file);
      reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');

    $timeout(function () {
        $location.path('/bienvenido');
    }, 1000);

    })
    .error(function(data){
      mensajes.alerta('ERROR','NO SE PUDO GENERAR EL DOCUMENTO','ACEPTAR');
    });





    break;
    default:

  }

};


});
