(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:TrabajosocialPendientesCtrl
 * @description
 * # TrabajosocialPendientesCtrl
 * Controller of the expediente
 */
  angular.module('expediente')
    .controller('TrabajosocialPendientesCtrl', ['$scope', '$http', 'mensajes', 'pacientes', '$state', '$localStorage', function ($scope, $http, mensajes, pacientes, $state, $localStorage) {

    $scope.selected = [];

    $scope.query = {
      filter: '',
      order: 'nombre',
      limit: 15,
      page: 1
    };

    $scope.datos =[];

    $scope.loadTablePacientes = function(){
      $scope.datos = [];

      pacientes.getPendientes()
      .success(function(data){
       	$scope.datos=data;
        console.log(data);
        	$scope.query.page=1;
      })
      .error(function(data){
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      });
    };

    $scope.completar = function(current){
      $localStorage.paciente={
        // idexpediente: current.NF_PACIENTE_ID,
        idexpediente: current.NP_ID,
        expediente: current.NF_PACIENTE_ID_ANIO.replace("/",""),
        nombrePaciente: current.NF_PACIENTE_NOMBRE_COMPLETO,
        idPaciente : current.NP_ID
      };
      console.log($localStorage.paciente.idPaciente);
      /*$localStorage.paciente=pacientes.getPendientesDetalle(current.NP_ID);*/

    	$state.go("ministerioPublico");
    };


  }]);

})(); 
