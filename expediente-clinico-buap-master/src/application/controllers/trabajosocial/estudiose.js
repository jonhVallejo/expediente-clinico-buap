(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:TrabajosocialEstudioseCtrl
 * @description
 * # TrabajosocialEstudioseCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('TrabajosocialEstudioseCtrl' , [ '$scope', '$timeout', '$http', 'mensajes', 'pacientes', 'medicos', 'catalogos', '$mdDialog', '$mdMedia', 'blockUI', '$localStorage', '$state', 'peticiones', '$window' , 'reportes',
    function ($scope, $timeout, $http, mensajes, pacientes, medicos, catalogos, $mdDialog, $mdMedia, blockUI, $localStorage, $state, peticiones, $window , reportes) {


  $scope.isDerechoHabiente = false;

  var actionURL = 'trabajoSocial';

  $scope.T_RESPONSABLES = {};

  $scope.ESTUDIO_SOCIAL = {
    C_HOSPITAL: 'HOSPITAL UNIVERSITARIO DE PUEBLA',
    F_FECHAREGISTRO: moment().toDate()
  };

  $scope.fechaRegistro = "";
  $scope.fechaInicial = moment().subtract(1, 'days').toDate();

  $scope.verificaPaciente = function() {
    pacientes.getPacienteTrabajoSocial()
      .success(function(data){
        $localStorage.NF_TRABAJO_SOCIAL = usuario.NP_EXPEDIENTE;
        $localStorage.CF_TRABAJO_SOCIAL = usuario.NOMBRE_COMPLETO;
        if (data.DATOS_PACIENTE.C_CODIGO == "H")
          $scope.esDerechoHabiente = false;
        else
          $scope.esDerechoHabiente = true;

        console.log($scope.esDerechoHabiente);

        $scope.esValido = true;
      })
      .error(function(data){
        mensajes.alerta('ERROR',"EL PACIENTE NO TIENE NOTA MEDICA O NO ESTA FIRMADA",'ACEPTAR!');
        $scope.esValido = false;
        $scope.regresaAlState('I');
      });
  }

  $scope.registar = function(opcion) {
    $scope.estado = opcion;
    $state.go($scope.estado);
  }

  $scope.inicializaPagina = function(validarVigencia) {

    // Catálogos
      $scope.catEstadoCivil = [];
      pacientes.getCatalogo("9").success(function(data){
      $scope.catEstadoCivil=data;
    })
    $scope.catSexo = [];
      pacientes.getCatalogo("12").success(function(data){
      $scope.catSexo=data;
    });
    $scope.catEstado = [];
      pacientes.getCatalogo("7").success(function(data){
      $scope.catEstado=data;
    });
    $scope.catParentesco = [];
      pacientes.getCatalogo("17").success(function(data){
      $scope.catParentesco=data;
    });

    $scope.catServicios = [];
    pacientes.getCatalogo("2").success(function(data){
      $scope.catServicios=data;
    });

    $scope.catHospitalizacion = [];
    pacientes.getCatalogo("24").success(function(data){
      $scope.catHospitalizacion=data;
    });
    
    if ($localStorage.paciente.idPaciente != undefined) {


      $scope.isDerechoHabiente = $localStorage.paciente.idPaciente[0].toUpperCase()  !== 'H';

      // Tablas para la captura de Cuotas de Recuperación

      $scope.grupoFam = [ {etiqueta: "10 O MAS",  num:0},
                          {etiqueta: "7 a 9 ",    num:1},
                          {etiqueta: "4 a 6 ",    num:2},
                          {etiqueta: "1 a 3 ",    num:3}];

     $scope.ingresoFa= [{etiqueta: "JEFE DE FAMILIA",     valor:0, N_ORDEN : 0},
                        {etiqueta: "ESPOSO",              valor:0, N_ORDEN : 1},
                        {etiqueta: "HIJO(A)",             valor:0, N_ORDEN : 2},
                        {etiqueta: "OTROS",               valor:0, N_ORDEN : 3}];

     $scope.egresoFa = [{etiqueta: "ALIMENTACION",        valor:0, N_ORDEN :0},
                        {etiqueta: "RENTA O PREDIO",      valor:0, N_ORDEN :1},
                        {etiqueta: "AGUA",                valor:0, N_ORDEN :2},
                        {etiqueta: "LUZ",                 valor:0, N_ORDEN :3},
                        {etiqueta: "TRANSPORTE",          valor:0, N_ORDEN :4},
                        {etiqueta: "COMBUSTIBLE",         valor:0, N_ORDEN :5},
                        {etiqueta: "EDUCACION",           valor:0, N_ORDEN :6},
                        {etiqueta: "CONSUMOS ADICIONALES",valor:0, N_ORDEN :7},
                        {etiqueta: "OTROS",               valor:0, N_ORDEN :8}];

     $scope.ocupacion= [
                         {etiqueta: "DESEMPLEADOS", num:0},
                         {etiqueta: "SUBEMPLEADOS", num:1},
                         {etiqueta: "OBREROS ",     num:2},
                         {etiqueta: "EMPLEADOS",    num:3},
                         {etiqueta: "TECNICOS ",    num:4},
                         {etiqueta: "PROFECIONISTAS, EMPLEADOS Y EJECUTIVOS", num:5}];

    $scope.salario = [
                         {etiqueta: "SIN SALARIO",                      num:0},
                         {etiqueta: "MENOS DEL SALARIO MINIMO",         num:1},
                         {etiqueta: "SALARIO MINIMO",                   num:2},
                         {etiqueta: "MAS DEL SALARIO MINIMO",           num:3},
                         {etiqueta: "DE 2 A 3 VECES EL SALARIO MINIMO", num:4},
                         {etiqueta: "DE 4 A 5 VECES EL SALARIO MINIMO", num:5}];

     $scope.vivienda = [
                        {etiqueta: "SIN VIVIENDA",                   num:0},
                        {etiqueta: "JACAL/CHOZA ",                   num:1},
                        {etiqueta: "VECINDAD/CUARTOS IMPROVIZADOS ", num:2},
                        {etiqueta: "CASA/DEPTO. POPULAR",            num:3},
                        {etiqueta: "CASA/DEPTO. RECIDENCIAL",        num:4}];

     $scope.dormitorio=[{etiqueta: " DE UN CUARTO",                 num:0},
                        {etiqueta: " DE UN DORMITORIO",             num:1},
                        {etiqueta: " CON DOS DORMITORIOS",          num:2},
                        {etiqueta: " CON TRES DORMITORIOS",         num:3},
                        {etiqueta: " CON CUATRO O MAS DORMITORIOS", num:4}];

     $scope.situacionE=[{etiqueta: "INDIGENA",   num:0},
                        {etiqueta: "DEFICIT",    num:1},
                        {etiqueta: "EQUILIBRIO", num:2},
                        {etiqueta: "SOLVENTE",   num:3},
                        {etiqueta: "EXCEDENTE",  num:4}];

     $scope.sumCalif= [{etiqueta: "GRUPO FAMILIAR",       valor:""},
                       {etiqueta: "OCUPACION",            valor:""},
                       {etiqueta: "SALARIO",              valor:""},
                       {etiqueta: "TIPO DE VIVIENDA",     valor:""},
                       {etiqueta: "NUMERO DE DORMITORIOS",valor:""},
                       {etiqueta: "SITUACION ECONOMICA",  valor:""}];

      $scope.escala=[{num:"1", porcentaje:"30 %",      rango:"0-6"},
                     {num:"2", porcentaje:"20 %",      rango:"7-12"},
                     {num:"3", porcentaje:"10 %",      rango:"13-21"},
                     {num:"4", porcentaje:"SIN APOYO", rango:"22-25"}];

      $localStorage.grupoFam   = $scope.grupoFam;
      $localStorage.ocupacion  = $scope.ocupacion;
      $localStorage.ingresoFa  = $scope.ingresoFa;
      $localStorage.salario    = $scope.salario;
      $localStorage.vivienda   = $scope.vivienda;
      $localStorage.egresoFa   = $scope.egresoFa;
      $localStorage.dormitorio = $scope.dormitorio;
      $localStorage.situacionE = $scope.situacionE;
      $localStorage.sumCalif   = $scope.sumCalif;
      $localStorage.escala     = $scope.escala;
      $localStorage.total      = $scope.total;
      $localStorage.ingreso    = $scope.ingreso;
      $localStorage.egreso     = $scope.egreso;

      $scope.registroCompleto  = false;

      pacientes.getPacienteTrabajoSocial()
      .success(function(data){
        if(data.DATOS_PACIENTE.hasOwnProperty('NP_ID')){
          console.log("Datos del paciente");
          console.log(data);

          if (validarVigencia) {
            if (data.hasOwnProperty('ESTUDIO_SOCIAL')){
              var dateTemp = data.ESTUDIO_SOCIAL.F_FECHA.substr(0,10).split("/");
              var fechaReg = Date.parse(moment(dateTemp[2]+"-"+dateTemp[1]+"-"+dateTemp[0]).format("YYYY-MM-DD"));
              var fechaAct = Date.parse(moment().format("YYYY-MM-DD"));
              var numDias = (fechaAct-fechaReg)/86400000;
              
              if (numDias < 90){
                mensajes.alerta('ERROR',"LA VIGENCIA DEL ESTUDIO SOCIO-ECONOMICO ES DE 90 DÍAS (" +data.ESTUDIO_SOCIAL.F_FECHA.substr(0,10)+")",'ACEPTAR!');
                $state.go('trabajosocial');
              }
            }
          }
          else {
            if (!(data.hasOwnProperty('ESTUDIO_SOCIAL'))) {
              mensajes.alerta('ERROR',"EL PACIENTE NO CUENTA CON UN ESTUDIO SOCIO-ECONOMICO",'ACEPTAR!');
              $state.go('trabajosocial');
            }
            else {              

              console.log("Recuperación de ESTUDIO_SOCIAL ");
              $scope.ESTUDIO_SOCIAL.NP_ID = data.ESTUDIO_SOCIAL.NP_ID;

              $scope.ESTUDIO_SOCIAL.C_HOSPITAL = data.ESTUDIO_SOCIAL.C_HOSPITAL;
              $scope.ESTUDIO_SOCIAL.C_REFERENCIA = data.ESTUDIO_SOCIAL.C_REFERENCIA;
              $scope.ESTUDIO_SOCIAL.C_CONTRAREFERENCIA = data.ESTUDIO_SOCIAL.C_CONTRAREFERENCIA;
              $scope.ESTUDIO_SOCIAL.CF_TRABAJO_SOCIAL = $localStorage.CF_TRABAJO_SOCIAL;
              $scope.ESTUDIO_SOCIAL.NF_TRABAJO_SOCIAL = $localStorage.NF_TRABAJO_SOCIAL;
              $scope.ESTUDIO_SOCIAL.F_FECHA = data.ESTUDIO_SOCIAL.F_FECHA;

              $scope.sumCalif[0].valor = parseInt(data.ESTUDIO_SOCIAL.N_GRUPO_FAMILIAR);
              $scope.sumCalif[1].valor = parseInt(data.ESTUDIO_SOCIAL.N_OCUPACION);
              $scope.sumCalif[2].valor = parseInt(data.ESTUDIO_SOCIAL.N_SALARIO);
              $scope.sumCalif[3].valor = parseInt(data.ESTUDIO_SOCIAL.N_TIPO_VIVIENDA);
              $scope.sumCalif[4].valor = parseInt(data.ESTUDIO_SOCIAL.N_DORMITORIOS);
              $scope.sumCalif[5].valor = parseInt(data.ESTUDIO_SOCIAL.N_SITUACION_ECONOMICA);
              $scope.calculaSuma();

              angular.forEach(data.ESTUDIO_SOCIAL.INGRESO_FAMILIAR, function(tabla, index) {
                $scope.ingresoFa[tabla.N_ORDEN].valor = tabla.CANTIDAD;
              });

              angular.forEach(data.ESTUDIO_SOCIAL.EGRESO_FAMILIAR, function(tabla, index) {
                $scope.egresoFa[tabla.N_ORDEN].valor = tabla.CANTIDAD;
              });

              $scope.calculaIngreso();
              $scope.calculaEgreso();
              
              if (!validarVigencia)               
                if ($scope.total >=0 && $scope.total <=6) $scope.N_NIVEL_SOCIECONOMICO = 30;
                else if ($scope.total>=7 && $scope.total<=12) $scope.N_NIVEL_SOCIECONOMICO = 20;
                     else if ($scope.total>=13 && $scope.total<=21) $scope.N_NIVEL_SOCIECONOMICO = 10;
                          else $scope.N_NIVEL_SOCIECONOMICO = 0;           
              $http.get(baseURL+"trabajosocial/"+data.DATOS_PACIENTE.NP_EXPEDIENTE.replace("/","")+"/estudiosocioeconomico")
              .success(function(data){
                $localStorage.historico = data;
              });
              console.log($scope.N_NIVEL_SOCIECONOMICO);
            }

          }

          $scope.paciente = data.DATOS_PACIENTE;

          $scope.paciente.C_DIAGNOSTICO_SOCIAL = "";
          var temp = $scope.paciente.NF_SEXO;
          $scope.paciente.NF_SEXO = $scope.paciente.NF_SEXO_ID;
          $scope.paciente.NF_SEXO_ID = temp;

          temp = $scope.paciente.NF_ENTIDAD_FEDERATIVA;
          $scope.paciente.NF_ENTIDAD_FEDERATIVA = $scope.paciente.NF_ENTIDAD_FEDERATIVA_ID;
          $scope.paciente.CF_ENTIDAD_FEDERATIVA = temp;

          temp = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO;
          $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO_ID;
          $scope.paciente.CF_ENTIDAD_DE_NACIMIENTO = temp;

          temp = $scope.paciente.NF_ESTADO_CIVIL;
          $scope.paciente.NF_ESTADO_CIVIL = $scope.paciente.NF_ESTADO_CIVIL_ID;
          $scope.paciente.CF_ESTADO_CIVIL = temp;

          $scope.paciente.FECHAREGISTRO = moment().toDate();

          $scope.calcularEdad();
          
          $scope.paciente.MEDICINA = "1";
          $scope.paciente.INGRESOHOSP = "1";
          
          if(data.DATOS_PACIENTE.hasOwnProperty('T_RESPONSABLES'))
            $scope.T_RESPONSABLES = data.DATOS_PACIENTE.T_RESPONSABLES;
          else
            $scope.T_RESPONSABLES = {}; 
        }
        else{
          mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
        } 
      })
      .error(function(data){
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      });
    }
    else {
      $scope.paciente              = $localStorage.paciente;
      $scope.ESTUDIO_SOCIAL        = $localStorage.ESTUDIO_SOCIAL;

      $scope.ESTUDIO_SOCIAL.F_FECHAREGISTRO = moment().toDate();

      $scope.T_RESPONSABLES        = $localStorage.T_RESPONSABLES;
      $scope.N_NIVEL_SOCIECONOMICO = $localStorage.N_NIVEL_SOCIECONOMICO;
      //$scope.paciente.C_DIAGNOSTICO_SOCIAL   = $localStorage.C_DIAGNOSTICO_SOCIAL;
      $scope.sumCalif              = $localStorage.sumCalif;
      $scope.ingresoFa             = $localStorage.ingresoFa;
      $scope.egresoFa              = $localStorage.egresoFa;
      $scope.total                 = $localStorage.total;
      $scope.ingreso               = $localStorage.ingreso;
      $scope.egreso                = $localStorage.egreso;
    }
  };

  $scope.calcularEdad = function(){
    $scope.paciente.EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
  }

  $scope.capturaCuotas = function(ev,ID,EXPEDIENTE) {

      $localStorage.N_NIVEL_SOCIECONOMICO = -1;
      $localStorage.paciente = $scope.paciente;
      $localStorage.ESTUDIO_SOCIAL = $scope.ESTUDIO_SOCIAL;
      $localStorage.T_RESPONSABLES = $scope.T_RESPONSABLES;

      $state.go('trabajoSocialCuotas');
  };

  $scope.registrarDatos = function(tipo) {
    $scope.btnRegistro = true;

    if ($scope.ESTUDIO_SOCIAL.C_HOSPITAL == undefined || 
        $scope.T_RESPONSABLES.C_NOMBRE == undefined || $scope.T_RESPONSABLES.C_PRIMER_APELLIDO == undefined || 
        $scope.T_RESPONSABLES.C_SEGUNDO_APELLIDO == undefined || $scope.T_RESPONSABLES.NF_ENTIDAD_FEDERATIVA == undefined ||
        $scope. T_RESPONSABLES.C_MUNICIPIO == undefined || $scope.T_RESPONSABLES.C_LOCALIDAD == undefined)
      $scope.btnRegistro = false;
    else {      
      if ($scope.N_NIVEL_SOCIECONOMICO == null && !$scope.isDerechoHabiente) {
        mensajes.alerta('ERROR','DEBE REGISTAR LA INFORMCIÓN DE CUOTAS DE RECUPERACIÓN','ACEPTAR');
        $scope.btnRegistro = false;
      }
      else {
        $scope.data = {};

        console.log($scope.paciente.C_DIAGNOSTICO_SOCIAL);
        $scope.data.ESTUDIO_SOCIAL = {
          C_HOSPITALIZACION : ($scope.paciente.NP_HOSPITALIZACION === null || $scope.paciente.NP_HOSPITALIZACION === undefined || $scope.paciente.NP_HOSPITALIZACION === 0) ? '' : _.filter($scope.catHospitalizacion, function(val){
                                      return $scope.paciente.NP_HOSPITALIZACION === val.ID;
                                      })[0].cDescripcion,
          C_REFERENCIA : $scope.ESTUDIO_SOCIAL.C_REFERENCIA,
          C_CONTRAREFERENCIA : $scope.ESTUDIO_SOCIAL.C_CONTRAREFERENCIA,
          C_HOSPITAL : $scope.ESTUDIO_SOCIAL.C_HOSPITAL,
          NP_CAMA : $scope.paciente.NP_CAMA,
          NP_HOSPITALIZACION : $scope.paciente.NP_HOSPITALIZACION,
          NP_EXPEDIENTE : $scope.paciente.NP_EXPEDIENTE.replace("/",""),
          NF_TRABAJO_SOCIAL: $localStorage.NF_TRABAJO_SOCIAL,
          CF_TRABAJO_SOCIAL: $localStorage.CF_TRABAJO_SOCIAL,
          //C_DIAGNOSTICO_SOCIAL : "  "
          C_DIAGNOSTICO_SOCIAL : $scope.paciente.C_DIAGNOSTICO_SOCIAL
        };

        /***************************************************************
        * SI NO ES DERECHOHABIENTE SE INSERTAN ESTOS DATOS
        ****************************************************************/
        if(!$scope.isDerechoHabiente){

          $scope.data.ESTUDIO_SOCIAL.N_GRUPO_FAMILIAR = $scope.sumCalif[0].valor;
          $scope.data.ESTUDIO_SOCIAL.N_OCUPACION = $scope.sumCalif[1].valor;
          $scope.data.ESTUDIO_SOCIAL.N_SALARIO = $scope.sumCalif[2].valor;
          $scope.data.ESTUDIO_SOCIAL.N_DORMITORIOS = $scope.sumCalif[4].valor;
          $scope.data.ESTUDIO_SOCIAL.N_TIPO_VIVIENDA = $scope.sumCalif[3].valor;
          $scope.data.ESTUDIO_SOCIAL.N_SITUACION_ECONOMICA = $scope.sumCalif[5].valor;
          $scope.data.ESTUDIO_SOCIAL.C_DIAGNOSTICO_SOCIAL = $scope.paciente.C_DIAGNOSTICO_SOCIAL;
          if (!($scope.ESTUDIO_SOCIAL.NP_ID == undefined) && tipo=='ANEXO')
            $scope.data.ESTUDIO_SOCIAL.NP_ID = $scope.ESTUDIO_SOCIAL.NP_ID;
          $scope.data.ESTUDIO_SOCIAL.N_NIVEL_SOCIECONOMICO = $scope.N_NIVEL_SOCIECONOMICO;
          $scope.data.ESTUDIO_SOCIAL.INGRESO_FAMILIAR = [];
          creaNodo($scope.ingresoFa,$scope.data.ESTUDIO_SOCIAL.INGRESO_FAMILIAR);
          $scope.data.ESTUDIO_SOCIAL.EGRESO_FAMILIAR = [];
          creaNodo($scope.egresoFa,$scope.data.ESTUDIO_SOCIAL.EGRESO_FAMILIAR);
        }
        
        var T_RESPONSABLES = {
          C_NOMBRE : $scope.T_RESPONSABLES.C_NOMBRE,
          C_PRIMER_APELLIDO : $scope.T_RESPONSABLES.C_PRIMER_APELLIDO,
          C_SEGUNDO_APELLIDO : $scope.T_RESPONSABLES.C_SEGUNDO_APELLIDO,

          NF_RELACION_PACIENTE : $scope.T_RESPONSABLES.NF_RELACION_PACIENTE,

          CF_RELACION_PACIENTE : ($scope.T_RESPONSABLES.NF_RELACION_PACIENTE == undefined) ? undefined: _.filter($scope.catParentesco, function(val){
                                      return $scope.T_RESPONSABLES.NF_RELACION_PACIENTE === val.ID;
                                      })[0].cDescripcion,

          C_CALLE : $scope.T_RESPONSABLES.C_CALLE,
          C_NO_EXTERIOR : $scope.T_RESPONSABLES.C_NO_EXTERIOR,
          C_NO_INTERIOR : $scope.T_RESPONSABLES.C_NO_INTERIOR,
          C_COLONIA : $scope.T_RESPONSABLES.C_COLONIA,
          C_MUNICIPIO : $scope.T_RESPONSABLES.C_MUNICIPIO,
          C_LOCALIDAD : $scope.T_RESPONSABLES.C_LOCALIDAD,

          NF_ENTIDAD_FEDERATIVA : $scope.T_RESPONSABLES.NF_ENTIDAD_FEDERATIVA,
          CF_ENTIDAD_FEDERATIVA : ($scope.T_RESPONSABLES.NF_ENTIDAD_FEDERATIVA == undefined) ? undefined : _.filter($scope.catEstado, function(val){
                                      return $scope.T_RESPONSABLES.NF_ENTIDAD_FEDERATIVA === val.ID;
                                      })[0].cDescripcion,

          C_TELEFONO_RESPONSABLE : $scope.T_RESPONSABLES.C_TELEFONO_RESPONSABLE
        };

        // DATOS_PACIENTE
        var temp = $scope.paciente.NP_EXPEDIENTE;
        $scope.data.DATOS_PACIENTE = {
          NP_EXPEDIENTE : $scope.paciente.NP_EXPEDIENTE.replace("/",""),
          C_NOMBRE : $scope.paciente.C_NOMBRE,
          C_PRIMER_APELLIDO : $scope.paciente.C_PRIMER_APELLIDO,
          C_SEGUNDO_APELLIDO : $scope.paciente.C_SEGUNDO_APELLIDO,
          F_FECHA_DE_NACIMIENTO : $scope.paciente.F_FECHA_DE_NACIMIENTO,

          CF_SEXO : ($scope.paciente.NF_SEXO == undefined) ? undefined:_.filter($scope.catSexo, function(val){
                                      return $scope.paciente.NF_SEXO === val.ID;
                                      })[0].cDescripcion,
          NF_SEXO : $scope.paciente.NF_SEXO,

          CF_ESTADO_CIVIL : ($scope.paciente.NF_ESTADO_CIVIL == undefined) ? undefined: _.filter($scope.catEstadoCivil, function(val){
                                      return $scope.paciente.NF_ESTADO_CIVIL === val.ID;
                                      })[0].cDescripcion,
          NF_ESTADO_CIVIL : $scope.paciente.NF_ESTADO_CIVIL,

          C_CURP : $scope.paciente.C_CURP,
          C_ESCOLARIDAD : $scope.paciente.C_ESCOLARIDAD,
          N_ESCOLARIDAD : $scope.paciente.N_ESCOLARIDAD,
          C_OCUPACION : $scope.paciente.C_OCUPACION,
          C_RELIGION : $scope.paciente.C_RELIGION,
          N_CODIGO_POSTAL : $scope.paciente.N_CODIGO_POSTAL,
          NP_ID : $scope.paciente.NP_ID,

          CF_ENTIDAD_DE_NACIMIENTO : ($scope.paciente.NF_ENTIDAD_DE_NACIMIENTO == undefined) ? undefined : _.filter($scope.catEstado, function(val){
                                      return $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO === val.ID;
                                      })[0].cDescripcion,
          NF_ENTIDAD_DE_NACIMIENTO : $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO,

          C_CALLE : $scope.paciente.C_CALLE,
          C_NO_EXTERIOR : $scope.paciente.C_NO_EXTERIOR,
          C_NO_INTERIOR : $scope.paciente.C_NO_INTERIOR,
          C_COLONIA : $scope.paciente.C_COLONIA,
          C_MUNICIPIO : $scope.paciente.C_MUNICIPIO,
          C_LOCALIDAD : $scope.paciente.C_LOCALIDAD,

          CF_ENTIDAD_FEDERATIVA : ($scope.paciente.NF_ENTIDAD_FEDERATIVA == undefined) ? undefined : _.filter($scope.catEstado, function(val){
                                      return $scope.paciente.NF_ENTIDAD_FEDERATIVA === val.ID;
                                      })[0].cDescripcion,
          NF_ENTIDAD_FEDERATIVA : $scope.paciente.NF_ENTIDAD_FEDERATIVA,

          C_NO_TELEFONO_UNO : $scope.paciente.C_NO_TELEFONO_UNO,
          C_NO_TELEFONO_DOS : $scope.paciente.C_NO_TELEFONO_DOS,
          T_RESPONSABLES : T_RESPONSABLES
        };

        /*console.log("Datos a guardar..");
        console.log($scope.data);*/

        peticiones.putMethodPDF('trabajosocial',$scope.data)
          .success(function(data){
                var file = new Blob([data], {type: 'application/pdf'});
                var fileURL = URL.createObjectURL(file);
                reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
                $localStorage.C_DIAGNOSTICOSOCIAL = '';
                $scope.regresaAlState('I')
          })
          .error(function(data){
                $scope.btnRegistro = false;
                if (!data.hasOwnProperty('error')) {
                  var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                  var obj = JSON.parse(decodedString);
                  mensajes.alerta('ERROR',obj.error,'ACEPTAR');
                } else
                  mensajes.alerta('ERROR','NO SE PUDO GENERAR EL DOCUMENTO','ACEPTAR');
          });
      }
    }      

  }

  $scope.anexaDiagnostico = function (hayCambios) {
    var data = {
                  ESTUDIO_SOCIAL : {C_DIAGNOSTICO_SOCIAL  : $scope.paciente.C_DIAGNOSTICO_SOCIAL,
                                    NP_ID                 : $localStorage.data.ESTUDIO_SOCIAL.NP_ID,
                                    CF_TRABAJO_SOCIAL     : $localStorage.CF_TRABAJO_SOCIAL,
                                    NF_TRABAJO_SOCIAL     : $localStorage.NF_TRABAJO_SOCIAL }
    };

    console.log("Datos a Anexar..");
    console.log(data);
    $http.put( baseURL + "trabajosocial", data);
  }

  function creaNodo(origen,destino) {
    var nodo;
    angular.forEach(origen, function (tabla, index) {
      nodo = {};
      nodo.DESCRIPCION = tabla.etiqueta;
      nodo.CANTIDAD = parseFloat(tabla.valor);
      nodo.N_ORDEN = tabla.N_ORDEN;
      destino.push(nodo);
    });
  }

  $scope.loadInfoCuotas = function() {
    $scope.paciente = {};
    $scope.grupoFam = $localStorage.grupoFam;
    $scope.ocupacion = $localStorage.ocupacion;
    $scope.salario = $localStorage.salario;
    $scope.vivienda = $localStorage.vivienda;
    $scope.dormitorio = $localStorage.dormitorio;
    $scope.situacionE = $localStorage.situacionE;
    $scope.escala = $localStorage.escala;

    $scope.total = $localStorage.total;
    $scope.ingreso = $localStorage.ingreso;
    $scope.egreso = $localStorage.egreso;

    $scope.ingresoFa = $localStorage.ingresoFa;
    $scope.egresoFa = $localStorage.egresoFa;
    $scope.sumCalif = $localStorage.sumCalif;
    $scope.paciente.C_DIAGNOSTICO_SOCIAL = $localStorage.paciente.C_DIAGNOSTICO_SOCIAL;
    $scope.N_NIVEL_SOCIECONOMICO = $localStorage.N_NIVEL_SOCIECONOMICO;
  }

  $scope.calculaSuma = function() {
    $scope.total = 0;
    var temp;
    angular.forEach($scope.sumCalif,function(tabla,index) {
        temp = parseInt(tabla.valor);
        if (!isNaN(temp))
          $scope.total += temp
    });
  }


  $scope.calculaIngreso = function() {
    $scope.ingreso=0;

    var temp;
    angular.forEach($scope.ingresoFa,function(tabla,index) {
        temp = parseInt(tabla.valor);
        if (!isNaN(temp))
          $scope.ingreso += temp
    });
  }

  $scope.calculaEgreso = function() {
    $scope.egreso=0;
    var temp;
    angular.forEach($scope.egresoFa,function(tabla,index) {
        temp = parseInt(tabla.valor);
        if (!isNaN(temp))
          $scope.egreso += temp
    });
  }

  $scope.guardaInfoCuotas= function(){
    $localStorage.grupoFam    = $scope.grupoFam;
    $localStorage.ocupacion   = $scope.ocupacion;
    $localStorage.ingresoFa   = $scope.ingresoFa;
    $localStorage.salario     = $scope.salario;
    $localStorage.vivienda    = $scope.vivienda;
    $localStorage.egresoFa    = $scope.egresoFa;
    $localStorage.dormitorio  = $scope.dormitorio;
    $localStorage.situacionE  = $scope.situacionE;
    $localStorage.sumCalif    = $scope.sumCalif;
    $localStorage.escala      = $scope.escala;
    $localStorage.paciente.C_DIAGNOSTICO_SOCIAL = $scope.paciente.C_DIAGNOSTICO_SOCIAL;
    $localStorage.total       = $scope.total;

    $scope.calculaIngreso();
    $scope.calculaEgreso();
    $localStorage.ingreso     = $scope.ingreso;
    $localStorage.egreso      = $scope.egreso;

    if ($scope.ingreso > 0 && $scope.egreso > 0) {
      if ($scope.total >=0 && $scope.total <=6) $localStorage.N_NIVEL_SOCIECONOMICO = 30;
      else if ($scope.total>=7 && $scope.total<=12) $localStorage.N_NIVEL_SOCIECONOMICO = 20;
           else if ($scope.total>=13 && $scope.total<=21) $localStorage.N_NIVEL_SOCIECONOMICO = 10;
                else $localStorage.N_NIVEL_SOCIECONOMICO = 0;
      //$state.go('registroESNuevo');
      $scope.regresaAlState('A')
    } else {
      mensajes.alerta('ERROR','EL INGRESO O EL EGRESO FAMILIAR NO SE HAN REGISTRADO','ACEPTAR');
      $localStorage.N_NIVEL_SOCIECONOMICO = null;
    }
  }

  $scope.regresaAlState = function(stateGo) {
    if (stateGo == 'A')
      $state.go('registroESNuevo');
    else
      $state.go('agendaConsultaPaciente',{state: 'trabajosocial'});
  }

  $scope.muestraHistorico = function(ev){
      $mdDialog.show({
      controller: function ($scope, $mdDialog, $localStorage) {
            $localStorage.historico.C_RUTA = baseURL+"getpdf/"+$localStorage.historico.C_RUTA+"/";
            angular.forEach($localStorage.historico.ESTUDIOS_ECONOMICOS,function(tabla,index) {
              angular.forEach(tabla.DIAGNOSTICOS_SOCIALES, function(subTabla,index) {
                subTabla.F_FECHA = subTabla.F_FECHA.substr(0,2)+"/"+subTabla.F_FECHA.substr(2,2)+"/"+subTabla.F_FECHA.substr(4,7);
              });
            });
            $scope.historico = $localStorage.historico;
      },
      templateUrl: '/application/views/trabajosocial/historico.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: true
    })
  };

}]);

})(); 


var err;
