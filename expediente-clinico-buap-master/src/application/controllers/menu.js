'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:MenuCtrl
 * @description
 * # MenuCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('MenuCtrl', [
  '$rootScope',
  '$log',
  '$state',
  '$timeout',
  '$location',
  'menu',
  function ($rootScope, $log, $state, $timeout, $location, menu) {

    var vm = this;

	//functions for menu-link and menu-toggle
    vm.isOpen = isOpen;
    vm.toggleOpen = toggleOpen;
    vm.isSectionSelected = isSectionSelected;
    vm.isSelected = isSelected;
    vm.setPage = setPage;
    vm.autoFocusContent = false;
    $rootScope.$watch(function(){
      vm.menu = menu;
    });

    vm.status = {
      isFirstOpen: true,
      isFirstDisabled: false
    };

    function isOpen(section) {
      return menu.isSectionSelected(section);
    }

    function toggleOpen(section) {
      menu.toggleSelectSection(section);
    }
    
    function setPage(section) {
		return menu.setPage(section);
	}

	function isSelected(page) {
		return menu.isPageSelected(page);
	}
	  
    function isSectionSelected(section) {
      var selected = false;
      var openedSection = menu.openedSection;
      if(openedSection === section){
        selected = true;
      }
      else if(section.children) {
        section.children.forEach(function(childSection) {
          if(childSection === openedSection){
            selected = true;
          }
        });
      }
      return selected;
    }
  }]);
