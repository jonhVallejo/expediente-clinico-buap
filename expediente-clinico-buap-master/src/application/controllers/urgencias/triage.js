(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:UrgenciasTriageCtrl
 * @descripcion
 * # UrgenciasTriageCtrl
 * Controller of the expediente
 */
angular.module('expediente')
	.controller('UrgenciasTriageCtrl', ['$scope', '$timeout', '$q', '$http', 'mensajes', 'pacientes', 'catalogos', '$mdDialog', 'blockUI', '$localStorage', 'peticiones', '$window', '$state', 'usuario' ,'reportes', 
    function ($scope, $timeout, $q, $http, mensajes, pacientes, catalogos, $mdDialog, blockUI, $localStorage, peticiones, $window, $state, usuario ,reportes) {

	  //var baseURL = 'http://148.228.103.13:8080/';
  	var actionURL = 'triage';
    var validarInfo = true;
  	$scope.expRegNumInt = "/^([A-Za-z]|\\d|\\s|\\+|\\-)+$/";
  	$scope.expRegNum = "/^\d+$/";
    $scope.TIPO_TRIAGE = 2;
    $scope.REGISTRO_VALIDO = true;
    $scope.TRIAGE_ADULTOS = true;
    $scope.valoresTabIIObs0 = 2;
    $scope.valoresTabIIObs1 = 2;
    $scope.valoresTabIIObs2 = 2;
    $scope.valoresTabIIObs3 = 2;

    $scope.valoresTabIIIObs0 = 0;
    $scope.valoresTabIIIObs1 = 0;
    $scope.valoresTabIIIObs2 = 0;
    $scope.valoresTabIIIObs3 = 0;

     $scope.triage = {
      selectedIndex : 0
    };

    /*$scope.ESTATUSFC = 0;
    $scope.ESTATUSFR = 0;
    $scope.ESTATUSTA = 0;
    $scope.ESTATUSTEMP = 0;
    $scope.ESTATUSSAT = 0;*/
    $scope.tabEstatus = [0,0,0,0,0];
    $scope.paciente = {};
    $scope.paciente.tabEstatus0 = 0;
    $scope.tabEstatusObstetricia = [0,0,0,0,0];
    $scope.tabEstatusPediatria = [0,0,0,0,0];


    $scope.T_CALIFADULTOS5 = 0;
    $scope.N_CLASIFICADULTOS = "green";
    $scope.N_TOTALADULTOS = 0;
    $scope.N_TOTALADULTOSXOPCION = 0;
    $scope.N_TOTALADULTOSXTABLA = 0;
    $scope.N_TOTALPEDIATRIAXOPCION = 0;
    $scope.N_TOTALOBSTETRICIAXOPCION = 0;

  	$scope.inicializaPagina = function() {

  		$scope.catEstadoCivil = [];
	      pacientes.getCatalogo("9").success(function(data){
	      $scope.catEstadoCivil=data;
	    })

	    $scope.catSexo = [];
	      pacientes.getCatalogo("12").success(function(data){
	      $scope.catSexo=data;
	    });

		$scope.catConvenio = [];
	      pacientes.getCatalogo("1").success(function(data){
	      $scope.catConvenio=data;
	    });

	    $scope.catTipoEvolucion = [{cDescripcion:"NORMAL", ID:0}, {cDescripcion:"ALTERADO", ID:1}];

  		$scope.tabPuntajePediatria= [
  							{afectacion: 	"APARIENCIA",
  							leve:     		"LACTANTE CONSOLABLE. HISTORIA DE ALTERACIÓN DEL COMPORTAMIENTO",
  							moderado: 	 	"LACTANTE INCONSOLABLE. ALTERACIÓN DEL COMPORTAMIENTO",
  							intenso:  	 	"ALTERACIÓN NIVEL CONCIENCIA LETARGIA",
  							calificacion: 	1},

  							{afectacion: 	"RESPIRATORIO",
  							leve:     		"FR NORMAL PARA EDAD",
  							moderado: 	 	"DIFICULTAD RESPIRATORIA LEVE ESTRIDOR FR ANORMAL",
  							intenso:  	 	"DIFICULTAD RESPIRATORIA MODERADA A GRAVE >3 SILVERMAN. DIFICULTAD RESPIRATORIA MODERADA SAT 02<90%. ESTRIDOR SEVERO FR ANORMAL",
  							calificacion: 	1},

  							{afectacion: 	"CARDIOVASCULAR",
  							leve:     		"FC NORMAL PARA EDAD",
  							moderado: 	 	"RELLENO CAPILLAR > A 2 SEGUNDOS FC ANORMAL",
  							intenso:  	 	"RELLENO CAPILLAR > DE 4 SEGUNDOS FC ANORMAL",
  							calificacion: 	1},

  							{afectacion: 	"EXPRESIÓN FACIAL",
  							leve:     		"MENOS MARCADO. INTERMITENTE CORTA MUECA DE DISGUSTO",
  							moderado: 	 	"MARCADO CONSTANTE LARGA MUECA DE DISGUSTO",
  							intenso:  	 	"EXPRESIÓN DE DOLOR",
  							calificacion: 	1},

  							{afectacion: 	"POSTURA (COMPORTAMIENTO DEL NIÑO EN REALCIÓN A ZONA DOLOROSA)",
  							leve:     		"PIERNSA MUSLOS FLEXIONADOS. TOCÁNDOSE, FRICCIONÁNDOSE COMEDIDAMENTE",
  							moderado: 	 	"AGARRADO A LA ZONA DE DOLOR",
  							intenso:  	 	"A LA DEFENSIVA TENSO",
  							calificacion: 	1},

  							{afectacion: 	"MOVIMIENTO (CÓMO SE MUEVE TODO EL CUERPO)",
  							leve:     		"REDUCIDO O INQUIETO. AGITACIÓN MODERADA O ACTIVIADAD DISMINUIDA",
  							moderado: 	 	"INMÓVIL O HIPOACTIVO. AGITACIÓN INCESANTE",
  							intenso:  	 	"COMBATIVO. NINGUNA ACTIVIDAD",
  							calificacion: 	1},

  							{afectacion: 	"COLOR",
  							leve:     		"NORMAL",
  							moderado: 	 	"PÁLIDO",
  							intenso:  	 	"COLORACIÓN CIANOGENA",
  							calificacion: 	1},

  							{afectacion: 	"HIDRATACIÓN",
  							leve:     		"LLORA CON LÁGRIMAS Y BOCA HUMEDA",
  							moderado: 	 	"BOCA SECA, SALIVA FILANTE, LLANTO SIN LAGRIMAS",
  							intenso:  	 	"BOCA SECA, OJOS HUNDIDOS Y SIGNO DEL LIENZO POSITIVO",
  							calificacion: 	1},

  							{afectacion: 	"HEMORRAGIA",
  							leve:     		"AUSENTE",
  							moderado: 	 	"ABIERTA SIN RIESGO PARA LA VIDA",
  							intenso:  	 	"ABIERTA O CERRADA CON RIESGO PARA LA VIDA",
  							calificacion: 	1}
		];

		$scope.tabParamPediatria= [
  							{edad: 	"1 MES - 1 AÑO",
  							FCMin:  "90",
  							FCMax: 	"150",
  							FRMin:  "24",
  							FRMax: 	"40",
  							TASMin: "60",
  							TASMax: "80",},

  							{edad: 	"1 A 6 AÑOS",
  							FCMin:  "90",
  							FCMax: 	"130",
  							FRMin:  "22",
  							FRMax: 	"30",
  							TASMin: "90",
  							TASMax: "100",},

  							{edad: 	">6 AÑOS",
  							FCMin:  "60",
  							FCMax: 	"120",
  							FRMin:  "18",
  							FRMax: 	"25",
  							TASMin: "95",
  							TASMax: "110"}

  		];

  		$scope.tabGravedadObstetricia=[
  							{descripcion: "PÉRDIDAD SUBITA DEL ESTADO DE ALERTA",	 estatus: false, puntos: 20},
  							{descripcion: "PARO CARDIORESPIRATORIO",				       estatus: false, puntos: 20},
  							{descripcion: "EMERGENCIA HIPERTENSIVA",				       estatus: false, puntos: 20},
  							{descripcion: "DOLOR TIPO INFARTO DEL MIOCARDIO",		   estatus: false, puntos: 20},
  							{descripcion: "HEMORRAGIA GRAVE",						           estatus: false, puntos: 20},
  							{descripcion: "DISNEA GRAVE",							             estatus: false, puntos: 20}
  		];

  		$scope.tabFCObstetricia = [
  							{descripcion: "<40",	opcion: 0, puntos: 5},
  							{descripcion: "40-59",	opcion: 1, puntos: 3},
  							{descripcion: "60-100",	opcion: 2, puntos: 0},
  							{descripcion: "101-140",opcion: 3, puntos: 3},
  							{descripcion: ">140",	opcion: 4, puntos: 5}
  		];

  		$scope.tabTEMPObstetricia = [
  							{descripcion: "<34.5",		opcion: 0, puntos: 5},
  							{descripcion: "34.5-35.9",	opcion: 1, puntos: 3},
  							{descripcion: "36.37",		opcion: 2, puntos: 0},
  							{descripcion: "37.1-39",	opcion: 3, puntos: 3},
  							{descripcion: ">39",		opcion: 4, puntos: 5}
  		];

  		$scope.tabFRObstetricia = [
  							{descripcion: "<8",		opcion: 0, puntos: 5},
  							{descripcion: "8-12",	opcion: 1, puntos: 3},
  							{descripcion: "13-18",	opcion: 2, puntos: 0},
  							{descripcion: "19-25",	opcion: 3, puntos: 3},
  							{descripcion: ">25",	opcion: 4, puntos: 5}
  		];

  		$scope.tabFAObstetricia = [
  							{descripcion: "<70/50",			opcion: 0, puntos: 5},
  							{descripcion: "70/50-90/60",	opcion: 1, puntos: 3},
  							{descripcion: "91/61-120/80",	opcion: 2, puntos: 0},
  							{descripcion: "121/81-160/110",	opcion: 3, puntos: 3},
  							{descripcion: "DIAST.>110",		opcion: 4, puntos: 5}
  		];

  		$scope.tabGlassgowObstetricia = [
  							{descripcion: "15",		opcion: 0, puntos: 0},
  							{descripcion: "12-14",	opcion: 1, puntos: 1},
  							{descripcion: "9-11",	opcion: 2, puntos: 3},
  							{descripcion: "<8",		opcion: 3, puntos: 5},
  		];

  		$scope.tabNeurologiaObstetricia = [
  							{descripcion: "NORMAL",				opcion: 0, puntos: 0},
  							{descripcion: "ORDENES SENCILLAS",	opcion: 1, puntos: 1},
  							{descripcion: "DÉFICIT FOCAL",		opcion: 2, puntos: 3},
  							{descripcion: "MUERTE CEREBRAL",		opcion: 3, puntos: 5},
  		];

  		$scope.tabConvulcionesObstetricia = [
  							{descripcion: "NO",				opcion: 0, puntos: 0},
  							{descripcion: "ANTECEDENTES",	opcion: 1, puntos: 1},
  							{descripcion: "REMITIDOS",		opcion: 2, puntos: 3},
  							{descripcion: "PRESENTES",		opcion: 3, puntos: 5},
  		];

  		$scope.tabTraumaObstetricia = [
  							{descripcion: "NO",			opcion: 0, puntos: 0},
  							{descripcion: "LEVE",		opcion: 1, puntos: 1},
  							{descripcion: "MODERADO",	opcion: 2, puntos: 3},
  							{descripcion: "SEVERO",		opcion: 3, puntos: 5},
  		];

  		$scope.tabAnexoObstetricia = [
  							{encabezado : 	"BH",
  							 opcion		: 	0,
  							 contenido  :  	[ {descripcion: "H.b.",			  opcion: 0, valor: "", encabezado: ""},
            											{descripcion: "Hto.",			  opcion: 1, valor: "", encabezado: ""},
            											{descripcion: "Plaquetas",	opcion: 2, valor: "", encabezado: ""},
            											{descripcion: "Leucocitos",	opcion: 3, valor: "", encabezado: ""},
            											/*{descripcion: "HEMOTIPO",		opcion: 4, valor: "", encabezado ""}*/ ],
  							},

  							{encabezado : 	"PRUEBAS DE COAGULACIÓN",
  							 opcion		: 	1,
  							 contenido  :  	[ {descripcion: "T.P.",		    opcion: 0, valor: "", encabezado: ""},
            											{descripcion: "%",			    opcion: 1, valor: "", encabezado: ""},
            											{descripcion: "I.N.R.",		  opcion: 2, valor: "", encabezado: ""},
            											{descripcion: "T.T.P.",		  opcion: 3, valor: "", encabezado: ""},
            											{descripcion: "Gpo. S/Rh",	opcion: 4, valor: "", encabezado: "HEMOTIPO"} ]
  							},

  							{encabezado : 	"QS",
  							 opcion		: 	2,
  							 contenido  :  	[ {descripcion: "Glucosa",	   opcion: 0, valor: "", encabezado: ""},
            											{descripcion: "Urea",		     opcion: 1, valor: "", encabezado: ""},
            											{descripcion: "Creatinina",	 opcion: 2, valor: "", encabezado: ""},
            											{descripcion: "Ac.U.",		   opcion: 3, valor: "", encabezado: ""}
            											/*{descripcion: "",			       opcion: 4, valor: 0, encabezado: ""} */]
  							},

  							{encabezado : 	"ELECTROLITOS",
  							 opcion		: 	3,
  							 contenido  :  	[ {descripcion: "N.a.",	  opcion: 0, valor: "", encabezado: ""},
            											{descripcion: "K",			opcion: 1, valor: "", encabezado: ""},
            											{descripcion: "CI",			opcion: 2, valor: "", encabezado: ""},
            											{descripcion: "Ca",			opcion: 3, valor: "", encabezado: ""},
            											{descripcion: "Mg",			opcion: 4, valor: "", encabezado: ""} ]
  							},

  							{encabezado : 	"GASOMETRÍA ARTERIAL",
  							 opcion		: 	4,
  							 contenido  :  	[ {descripcion: "Ph",		  opcion: 0, valor: "", encabezado: ""},
            											{descripcion: "PaCo2",	opcion: 1, valor: "", encabezado: ""},
            											{descripcion: "PaO2",		opcion: 2, valor: "", encabezado: ""},
            											{descripcion: "HCO3",		opcion: 3, valor: "", encabezado: ""},
            											{descripcion: "EB",			opcion: 3, valor: "", encabezado: ""} ]
  							},

  							{encabezado : 	"NO. TRÁNSFUSIONES",
  							 opcion		: 	5,
  							 contenido  :  	[ {descripcion: "C.E.",			  opcion: 0, valor: "", encabezado: ""},
            											{descripcion: "P.F.C.",			opcion: 1, valor: "", encabezado: ""},
            											{descripcion: "C.P.",			  opcion: 2, valor: "", encabezado: ""},
            											{descripcion: "CREIOS",			opcion: 3, valor: "", encabezado: ""} ]
  							}
  		];


  		$scope.valoresTabIIObs  = [{puntos:0},{puntos:0},{puntos:0},{puntos:0}];
  		$scope.valoresTabIIIObs = [{puntos:0},{puntos:0},{puntos:0},{puntos:0}];

  		$scope.totalDatosGravedad = 0;
  		$scope.calculaPuntajePediatria();
  		$scope.calulaSumaTIIObs(-1,-1);
  		$scope.calulaSumaTIIIObs(-1,-1);


		/* Angel */

    $scope.tabAdultosTiempo= [
                          { encabezado :  "TIEMPO DE EVOLUCION",
                            puntaje    : 0,
                            contenido  : [{descripcion:">48 HRS",   opcion: 0, puntos: 0},
                                          {descripcion:"24-48 HRS", opcion: 1, puntos: 1},
                                          {descripcion:"<24 HRS",   opcion: 2, puntos: 2},]
                          }
                        ];

    $scope.tabAdultos= [

                          { encabezado :  "COMPROMISO RESPIRATORIO",
                            puntaje    : 0,
                            contenido  : [{descripcion:"NORMAL",                  opcion: 0, puntos: 0},
                                          {descripcion:"DIFICULTAD RESPIRATORIA", opcion: 1, puntos: 1},
                                          {descripcion:"TIRAJE ALETEO NASAL",     opcion: 2, puntos: 2},
                                          {descripcion:"CIANOSIS SIBILANCIAS",    opcion: 3, puntos: 3}]
                          },

                          { encabezado :  "COMPROMISO ABDOMINAL",
                            puntaje    : 0,
                            contenido  : [{descripcion:"NORMAL",              opcion: 0, puntos: 0},
                                          {descripcion:"RIGIDO O CON DOLOR",  opcion: 1, puntos: 1},
                                          {descripcion:"TRAUMA CERRADO",      opcion: 2, puntos: 2},
                                          {descripcion:"HERIDA PENETRANTE",   opcion: 3, puntos: 3}]
                          },

                          { encabezado :  "COMPROMISO NEUROLOGICO",
                            puntaje    : 0,
                            contenido  : [{descripcion:"NORMAL",              opcion: 0, puntos: 0},
                                          {descripcion:"CONCIENCIA ALTERADA", opcion: 1, puntos: 1},
                                          {descripcion:"OBNUBILADO",          opcion: 2, puntos: 2},
                                          {descripcion:"COMATOSO",            opcion: 3, puntos: 3}]
                          },

                          { encabezado :  "CAPACIDAD DE VERBALIZACION",
                            puntaje    : 0,
                            contenido  : [{descripcion:"NORMAL",              opcion: 0, puntos: 0},
                                          {descripcion:"CONFUSO",            opcion: 1, puntos: 1},
                                          {descripcion:"INCOHERENTE",         opcion: 2, puntos: 2},
                                          {descripcion:"AUSENCIA DE PALABRA", opcion: 3, puntos: 3}]
                          },

                          { encabezado :  "DOLOR",
                            puntaje    : 0,
                            contenido  : [{descripcion:"LEVE 0 A 2",      opcion: 0, puntos: 0},
                                          {descripcion:"MODERADO 4 A 6",  opcion: 1, puntos: 1},
                                          {descripcion:"INTENSO 8 a 10",  opcion: 2, puntos: 2},
                                          {descripcion:"MUY INTENSO >10", opcion: 3, puntos: 3}]
                          }
                        ];



                 /*colm2:">48 HRS",
                 colm3:"24-48 HRS",
                 colm4:"<24 HRS",
                 colm5:"",
                 colm6:0},

                {colm1:"COMPROMISO RESPIRATORIO",
                 colm2:"NORMAL",
                 colm3:"DIFICULTAD RESPIRATORIA",
                 colm4:"TIRAJE ALETEO NASAL",
                 colm5:"CIANOSIS SIBILANCIAS",
                 colm6:0},

                {colm1:"COMPROMISO ABDOMINAL",
                 colm2:"NORMAL",
                 colm3:"RIGIDO O CON DOLOR",
                 colm4:"TRAUMA CERRADO",
                 colm5:"HERIDA PENETRANTE",
                 colm6:0},

                {colm1:"COMPROMISO NEUROLOGICO",
                 colm2:"NORMAL",
                 colm3:"CONCIENCIA ALTERADA",
                 colm4:"OBNUBILADO",
                 colm5:"COMATOSO",
                 colm6:0},

                {colm1:"CAPACIDAD DE VERBALIZACION",
                 colm2:"NORMAL",
                 colm3:"ICONFUSO",
                 colm4:"INCOHERENTE",
                 colm5:"AUSENCIA DE PALABRA",
                 colm6:0},

                 ]; */

      pacientes.getPaciente()
      	.success(function(data){
	        if(!data.data.hasOwnProperty('NP_ID')){
	          //console.log("Datos del Paciente");
	          //console.log(data.data[0]);
	          $scope.paciente=data.data[0];
	          var temp = $scope.paciente.NF_SEXO;
	          $scope.paciente.NF_SEXO = $scope.paciente.NF_SEXO_ID;
	          $scope.paciente.NF_SEXO_ID = temp;

	          temp = $scope.paciente.NF_ENTIDAD_FEDERATIVA;
	          $scope.paciente.NF_ENTIDAD_FEDERATIVA = $scope.paciente.NF_ENTIDAD_FEDERATIVA_ID;
	          $scope.paciente.CF_ENTIDAD_FEDERATIVA = temp;

	          temp = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO;
	          $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO_ID;
	          $scope.paciente.CF_ENTIDAD_DE_NACIMIENTO = temp;

	          temp = $scope.paciente.NF_ESTADO_CIVIL;
	          $scope.paciente.NF_ESTADO_CIVIL = $scope.paciente.NF_ESTADO_CIVIL_ID;
	          $scope.paciente.CF_ESTADO_CIVIL = temp;
            $scope.paciente.C_NOMBRE = $scope.paciente.C_NOMBRE + " " + $scope.paciente.C_PRIMER_APELLIDO + " " + $scope.paciente.C_SEGUNDO_APELLIDO;

	          $scope.paciente.FECHAREGISTRO = moment().toDate();
	          $scope.paciente.EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
            /*if (parseInt($scope.paciente.EDAD.split(" ")[0]) <= 18)
              habilitaTRIAGE(1);
            else
              if ($scope.paciente.NF_SEXO_ID == "FEMENINO")
                habilitaTRIAGE(3);
  	          else
                habilitaTRIAGE(2) */
          }
	        else{
	          mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
	        }
	    })
      	.error(function(data){
        	mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      	});
  	}



  $scope.calculaTotalTransfuciones = function() {
    var temp;
      $scope.totalTransfuciones = 0;
      angular.forEach($scope.tabAnexoObstetricia[5].contenido, function(tabla, index) {
        //console.log(tabla);
        temp = parseFloat(tabla.valor);
        if (!isNaN(temp))
          $scope.totalTransfuciones += temp;
      });
  }

  $scope.actualizaPuntajeAdultos = function(valor){
    if (parseInt(valor) == 1)
      $scope.N_TOTALADULTOSXOPCION++;
    else
      $scope.N_TOTALADULTOSXOPCION--;
    actuallizaColorAdultos();

  }

  $scope.calculaPuntajeAdultos = function() {
      var temp;
      $scope.N_TOTALADULTOSXTABLA = 0;
      angular.forEach($scope.tabAdultos, function(registro) {
        $scope.N_TOTALADULTOSXTABLA += parseInt(registro.puntaje);
      });
      $scope.N_TOTALADULTOSXTABLA += parseInt($scope.tabAdultosTiempo[0].puntaje);
      actuallizaColorAdultos();
    }

    function actuallizaColorAdultos() {
      $scope.N_TOTALADULTOS = $scope.N_TOTALADULTOSXTABLA + $scope.N_TOTALADULTOSXOPCION;
      if ($scope.N_TOTALADULTOS<5)
        $scope.N_CLASIFICADULTOS = "green";
      else if ($scope.N_TOTALADULTOS>=5 && $scope.N_TOTALADULTOS<=7)
          $scope.N_CLASIFICADULTOS = "yellow";
        else if ($scope.N_TOTALADULTOS>=8)
            $scope.N_CLASIFICADULTOS = "red";
          else
              $scope.N_CLASIFICADULTOS = "white";
    }

    $scope.actualizaPuntajePediatria = function(valor){
      if (parseInt(valor) == 1)
        $scope.N_TOTALPEDIATRIAXOPCION++;
      else
        $scope.N_TOTALPEDIATRIAXOPCION--;
      actuallizaColorPediatria();
    }

  	$scope.calculaPuntajePediatria = function() {
    	$scope.N_TOTALPEDIATRIATABLA = 0;
      var temp;
    	angular.forEach($scope.tabPuntajePediatria, function(tablaPuntaje, index) {
        temp = parseInt(tablaPuntaje.calificacion);
        if (!isNaN(temp))
    		  $scope.N_TOTALPEDIATRIATABLA += temp;
      	});
       actuallizaColorPediatria();
    }

    function actuallizaColorPediatria() {
      $scope.N_TOTALPEDIATRIA = $scope.N_TOTALPEDIATRIATABLA + $scope.N_TOTALPEDIATRIAXOPCION;
      if ($scope.N_TOTALPEDIATRIA>=6 && $scope.N_TOTALPEDIATRIA<=10)
        $scope.N_CLASIFICPEDIATRIA = "green";
      else if ($scope.N_TOTALPEDIATRIA>=11 && $scope.N_TOTALPEDIATRIA<=18)
          $scope.N_CLASIFICPEDIATRIA = "yellow";
        else if ($scope.N_TOTALPEDIATRIA>18)
            $scope.N_CLASIFICPEDIATRIA = "red";
          else
              $scope.N_CLASIFICPEDIATRIA = "white";
    }

    $scope.actualizaPuntajeObstetricia = function(valor){
      if (parseInt(valor) == 1)
        $scope.N_TOTALOBSTETRICIAXOPCION++;
      else
        $scope.N_TOTALOBSTETRICIAXOPCION--;
      $scope.calculaPuntajeObstetricia();

    }

  	$scope.calculaPuntajeObstetricia = function() {
  		$scope.N_TOTALOBSTETRICIA = $scope.totalDatosGravedad + $scope.N_TOTALSIGNOSVITALES + $scope.N_TOTALNEUROLOGICO + $scope.N_TOTALOBSTETRICIAXOPCION;
      //$scope.N_TOTALOBSTETRICIA = $scope.totalDatosGravedad + $scope.N_TOTALNEUROLOGICO + $scope.N_TOTALOBSTETRICIAXOPCION;
  		if ($scope.N_TOTALOBSTETRICIA>=0 && $scope.N_TOTALOBSTETRICIA<=14)
    		$scope.N_CLASIFICOBSTETRICIA = "green";
    	else if ($scope.N_TOTALOBSTETRICIA>=15 && $scope.N_TOTALOBSTETRICIA<=19)
    			$scope.N_CLASIFICOBSTETRICIA = "yellow";
    		else if ($scope.N_TOTALOBSTETRICIA>19)
    				$scope.N_CLASIFICOBSTETRICIA = "red";
    			else
  	  				$scope.N_CLASIFICOBSTETRICIA = "white";
  	}

  	$scope.toggle = function (item) {
  		item.estatus = !item.estatus;
        if (item.estatus)
        	$scope.totalDatosGravedad += parseInt(item.puntos);
        else
        	$scope.totalDatosGravedad -= parseInt(item.puntos);
        $scope.calculaPuntajeObstetricia();
      }

    $scope.calulaSumaTIIObs = function(opcion,indice) {
    	if (opcion >= 0)
    		$scope.valoresTabIIObs[indice].puntos = $scope.tabFCObstetricia[opcion].puntos;
    	$scope.N_TOTALSIGNOSVITALES = 0;

    	angular.forEach($scope.valoresTabIIObs, function(tabla, index) {
    		$scope.N_TOTALSIGNOSVITALES += parseInt(tabla.puntos);
      	});
      	$scope.calculaPuntajeObstetricia();
    }

    $scope.calulaSumaTIIIObs = function(opcion,indice) {
    	if (opcion >= 0)
    		$scope.valoresTabIIIObs[indice].puntos = $scope.tabGlassgowObstetricia[opcion].puntos;
    	$scope.N_TOTALNEUROLOGICO = 0;

    	angular.forEach($scope.valoresTabIIIObs, function(tabla, index) {
    		$scope.N_TOTALNEUROLOGICO += parseInt(tabla.puntos);
      });
      $scope.calculaPuntajeObstetricia();
    }

    $scope.registraTriage = function() {
      var temp;
      $scope.mensaje = "";
      $scope.btnRegistro = true;
      switch ($scope.triage.selectedIndex) {
        case 0: $scope.TIPO_TRIAGE = 2; break;
        case 1: $scope.TIPO_TRIAGE = 3; break;
        case 2: $scope.TIPO_TRIAGE = 1; break;
      }
      switch ($scope.TIPO_TRIAGE) {
            // TRIAGE de Pedriatria
            case 1: $scope.REGISTRO_VALIDO = true;
                    angular.forEach($scope.tabPuntajePediatria, function(tabla, index) {
                      temp = parseInt(tabla.calificacion);
                      if (isNaN(temp)) {
                        $scope.REGISTRO_VALIDO = false;
                        $scope.mensaje = "EL(LOS) PUNTAJE(S) INTODUCIDO(S) NO ES(SON) VALIDO(S)";
                        $scope.btnRegistro = false;
                      }
                    });
                    if ($scope.REGISTRO_VALIDO) {
                      $scope.REGISTRO_VALIDO =  !$scope.registraForm.N_FCPED.$invalid &&
                                                !$scope.registraForm.N_FRPED.$invalid &&
                                                !$scope.registraForm.N_TAPED.$invalid &&
                                                !$scope.registraForm.N_TPED.$invalid &&
                                                !$scope.registraForm.N_SATPED.$invalid;
                      if (!($scope.REGISTRO_VALIDO)) {
                        $scope.mensaje = "LA INFORMACIÓN DE FC, FR, TA, T O SAT ES INCORRECTA";
                        $scope.btnRegistro = false;
                      }

                    }

                    break;
            // TRIAGE de Adultos
            case 2: $scope.REGISTRO_VALIDO =  !$scope.registraForm.FCADULTO.$invalid &&
                                              !$scope.registraForm.FRADULTO.$invalid &&
                                              !$scope.registraForm.TAADULTO.$invalid &&
                                              !$scope.registraForm.TEMPADULTO.$invalid &&
                                              !$scope.registraForm.SATADULTO.$invalid;
                      /*if ($scope.REGISTRO_VALIDO){
                        angular.forEach($scope.tabAdultos, function(tabla, index) {
                          temp = parseInt(tabla.colm6);
                          if (isNaN(temp)) {
                            $scope.REGISTRO_VALIDO = false;
                            $scope.mensaje = "EL(LOS) PUNTAJE(S) INTODUCIDO(S) NO ES(SON) VALIDO(S)";
                            $scope.btnRegistro = false;
                          }
                        });
                        $scope.REGISTRO_VALIDO = $scope.REGISTRO_VALIDO &&
                                               !$scope.registraForm.T_CALIFADULTOS5.$invalid;*/
                        if ($scope.REGISTRO_VALIDO){
                          $scope.REGISTRO_VALIDO = $scope.REGISTRO_VALIDO &&
                                               !$scope.registraForm.N_TALLA.$invalid &&
                                               !$scope.registraForm.N_PESO.$invalid;
                           if (!$scope.REGISTRO_VALIDO) {
                              $scope.mensaje = "EL PESO O LA TALLA ES INCORRECTO";
                              $scope.btnRegistro = false;
                           }

                        }
                      //}
                      else {
                        $scope.btnRegistro = false;
                        $scope.mensaje = "LA INFORMACIÓN DE FC, FR, TA, TEMP O SAT ES INCORRECTA";
                      }


                      break;
            // TRIAGE de Obstetricia
            case 3: $scope.REGISTRO_VALIDO =  !($scope.registraForm.N_TAOBS.$invalid) &&
                                               !($scope.registraForm.N_FCOBS.$invalid) &&
                                               !($scope.registraForm.N_FCFOBS.$invalid) &&
                                               !($scope.registraForm.N_SATOBS.$invalid) &&
                                               !($scope.registraForm.N_TOBS.$invalid);
                    if ($scope.REGISTRO_VALIDO) {
                        $scope.REGISTRO_VALIDO =  !($scope.registraForm.N_SANGRADOOBS.$invalid) &&
                                                  !($scope.registraForm.N_URESISOBS.$invalid) &&
                                                  !($scope.registraForm.N_BLIQUIDOSOBS.$invalid);
                        if ($scope.REGISTRO_VALIDO) {
                          angular.forEach($scope.tabAnexoObstetricia, function(tabla, indexTab) {
                            angular.forEach(tabla.contenido, function(subTabla, indexSubtab) {
                              if (subTabla.valor != "")
                                if (subTabla.descripcion != "Gpo. S/Rh") {
                                  temp = parseInt(subTabla.valor);
                                  if (isNaN(temp)) {
                                    $scope.REGISTRO_VALIDO = false;
                                    $scope.mensaje = "EXISTE INFORMACIÓN EN EL ANEXO INVALIDA";
                                    $scope.btnRegistro = false;
                                  }
                                }
                                else
                                  if (!/^[a|A|b|B|o|O][\+|-]$/.test(subTabla.valor)) {
                                    $scope.REGISTRO_VALIDO = false;
                                    $scope.mensaje = "LA INFORMACIÓN DEL Gpo. S/Rh ES INVALIDA";
                                    $scope.btnRegistro = false;
                                  }
                            });
                          });
                        }
                        else {
                          $scope.btnRegistro = false;
                          $scope.mensaje = "LA INFORMACIÓN DEL SANGRADO CUANTIFICADO, URESIS O EL BALANCE DE LIQUIDOS ES INVALIDA";
                        }
                    }
                    else {
                      $scope.btnRegistro = false;
                      $scope.mensaje = "LA INFORMACIÓN DE TA, FCF, STA O T ES INCORRECTA";
                    }
                    break;

      }
      if (!($scope.REGISTRO_VALIDO)){
        mensajes.alerta('ERROR',$scope.mensaje,'ACEPTAR!');
        $scope.btnRegistro = false;
      }
      else
        if ($scope.paciente.VALORACIONPSICO == undefined && $scope.TIPO_TRIAGE == 2) {
          mensajes.alerta('ERROR',"DEBE ESPECIFICAR EL ESTADO DEL PACIENTE ",'ACEPTAR!');
          $scope.btnRegistro = false;
        }
        else
          if ($scope.paciente.VALOR_NUTRICION == undefined && $scope.TIPO_TRIAGE == 2) {
            mensajes.alerta('ERROR',"DEBE ESPECIFICAR EL ESTADO NUTRICIONAL DEL PACIENTE ",'ACEPTAR!');
            $scope.btnRegistro = false;
          }
          else {
            var data = {};
            data.TIPO_TRIAGE = $scope.TIPO_TRIAGE;
            data.NF_PACIENTE = $scope.paciente.NP_EXPEDIENTE.substring(0,$scope.paciente.NP_EXPEDIENTE.length-5);
            data.NF_MEDICO   = usuario.usuario.NP_EXPEDIENTE;
            if ($scope.paciente.C_CONVENIO != undefined)
              data.C_CONVENIO  = _.filter($scope.catConvenio, function(val){
                                return $scope.paciente.C_CONVENIO === val.ID;
                                })[0].cDescripcion;
            else
              data.C_CONVENIO = " ";

            $scope.TRIAGES = [];
            creaNodoTriage("EDAD",$scope.paciente.EDAD,0,"","edad",!validarInfo);
            creaNodoTriage("CONVENIO",$scope.paciente.C_CONVENIO,0,"","convenio",validarInfo);
            if ($scope.TIPO_TRIAGE == 3)
              creaNodoTriage("MOTIVO DE LA CONSULTA","",0," ","motivoConsulta",!validarInfo);
            else
              creaNodoTriage("MOTIVO DE LA CONSULTA","",0,$scope.paciente.C_MOTIVO_CONSULTA,"motivoConsulta",!validarInfo);

            switch ($scope.TIPO_TRIAGE) {
              case 1: data.C_CALIFICACION = $scope.N_CLASIFICPEDIATRIA;   data.N_PUNTAJE_TOTAL = $scope.N_TOTALPEDIATRIA; break;
              case 2: data.C_CALIFICACION = $scope.N_CLASIFICADULTOS;     data.N_PUNTAJE_TOTAL = $scope.N_TOTALADULTOS; break;
              case 3: data.C_CALIFICACION = $scope.N_CLASIFICOBSTETRICIA; data.N_PUNTAJE_TOTAL = $scope.N_TOTALOBSTETRICIA;break;
            }

            switch (data.C_CALIFICACION) {
              case "green" :  data.C_CALIFICACION = "VERDE";     break;
              case "yellow":  data.C_CALIFICACION = "AMARILLO";  break;
              case "red"   :  data.C_CALIFICACION = "ROJO";      break;
            }

            switch ($scope.TIPO_TRIAGE) {
              // TRIAGE de Pedriatria
              case 1:   creaNodoTriage("¿PRESENTA ALGUNA ALERGIA?",$scope.paciente.ALERGIA,0,$scope.paciente.T_TIPOALERGIA,"presentaAlgunaAlergia",validarInfo);
                        angular.forEach($scope.tabPuntajePediatria, function(tabla, index) {
                          creaNodoTriage(tabla.afectacion, tabla.calificacion, tabla.calificacion,"","afectacion"+index,!validarInfo);
                        });
                        creaNodoTriage("FC",$scope.paciente.N_FCPED,$scope.tabEstatusPediatria[0],"","fc",!validarInfo);
                        creaNodoTriage("FR",$scope.paciente.N_FRPED,$scope.tabEstatusPediatria[1],"","fr",!validarInfo);
                        creaNodoTriage("TA",$scope.paciente.N_TAPED,$scope.tabEstatusPediatria[2],"","ta",!validarInfo);
                        creaNodoTriage("T",$scope.paciente.N_TPED,$scope.tabEstatusPediatria[3],"","t",!validarInfo);
                        creaNodoTriage("SAT",$scope.paciente.N_SATPED,$scope.tabEstatusPediatria[4],"","sat",!validarInfo);
                        break;
              // TRIAGE de Adultos
              case 2:   creaNodoTriage("FC",$scope.paciente.FCADULTO,$scope.tabEstatus[0],"","fc",!validarInfo);
                        creaNodoTriage("FR",$scope.paciente.FRADULTO,$scope.tabEstatus[1],"","fr",!validarInfo);
                        creaNodoTriage("T-A",$scope.paciente.TAADULTO,$scope.tabEstatus[2],"","ta",!validarInfo);
                        creaNodoTriage("TEMP",$scope.paciente.TEMPADULTO,$scope.tabEstatus[3],"","temp",!validarInfo);
                        creaNodoTriage("SAT",$scope.paciente.SATADULTO,$scope.tabEstatus[4],"","sat",!validarInfo);
                        creaNodoTriage("SINTOMATOLOGIA","",0,$scope.paciente.SINTOMATOLOGIA,"sintomatologia",!validarInfo);
                        creaNodoTriage("¿ES DIABETICO?",$scope.paciente.DIABETICO, 0,$scope.paciente.C_DESCRIP_DIABETICO,"esDiabetico",validarInfo);
                        creaNodoTriage("¿ES HIPERTENSO?",$scope.paciente.HIPERTENSO,0,$scope.paciente.C_DESCRIP_HIPERTENSO,"esHipertenso",validarInfo);
                        creaNodoTriage("¿PRESENTA ALGUNA ALERGIA?",$scope.paciente.ALERGIA, 0,$scope.paciente.C_DESCRIP_ALERGIA,"presentaAlgunaAlergia",validarInfo);
                        creaNodoTriage("OTRO",$scope.paciente.OTRO,0,$scope.paciente.C_DESCRIP_OTRO,"otro",validarInfo);
                        creaNodoTriage($scope.tabAdultosTiempo[0].encabezado, $scope.tabAdultosTiempo[0].puntaje, $scope.tabAdultosTiempo[0].puntaje,"","afectacion0",!validarInfo);
                        angular.forEach($scope.tabAdultos, function(tabla, index) {
                          var i = index+1
                          creaNodoTriage(tabla.encabezado, tabla.puntaje, tabla.puntaje,"","afectacion"+i,!validarInfo);
                        });
                        creaNodoTriage("DOLOR", $scope.T_CALIFADULTOS5, $scope.T_CALIFADULTOS5,"","dolor",!validarInfo);
                        creaNodoTriage("¿PACIENTE INDEPENDIENTE?",$scope.paciente.INDEPENDIENTE, 0,"","pacienteIndependiente",validarInfo);
                        creaNodoTriage("BAÑO?",$scope.paciente.BAGNO, 0,"","bagno",validarInfo);
                        creaNodoTriage("VESTIDO",$scope.paciente.VESTIDO, 0,"","vestido",validarInfo);
                        creaNodoTriage("AMBULACIÓN",$scope.paciente.AMBULACION, 0,"","ambulacion",validarInfo);
                        creaNodoTriage("ALIMENTACIÓN",$scope.paciente.ALIMENTACION, 0,"","alimentacion",validarInfo);
                        creaNodoTriage("ESTADO DEL PACIENTE:",$scope.paciente.VALORACIONPSICO, 0,"","estadoPaciente",!validarInfo);
                        creaNodoTriage("PUDIERA HACERCE DAÑO:",         $scope.paciente.HACERSEDAYNO, 0,"","pudieraHacerseDagno",validarInfo);
                        creaNodoTriage("PUDIERA HACER DAÑO A LOS DEMAS",$scope.paciente.DAYNOALOSDEMAS,    0,"","pudieraHacerDagnoALosDemas",validarInfo);
                        creaNodoTriage("TALLA",$scope.paciente.N_TALLA, 0,"","talla",!validarInfo);
                        creaNodoTriage("PESO",$scope.paciente.N_PESO, 0,"","peso",!validarInfo);
                        creaNodoTriage("IMC",$scope.paciente.N_IMC, 0,"","imc",!validarInfo);
                        creaNodoTriage("ESTADO NUTRICIONAL",$scope.paciente.VALOR_NUTRICION, 0,"","estadoNutricional",!validarInfo);
                        creaNodoTriage("POSTQUIRURGICO:",$scope.paciente.POSTQUIRURGICO, 0,"","postquirurgico",validarInfo);
                        creaNodoTriage("DISMINUCIÓN DEL APETITO MAYOR AL 5%:",$scope.paciente.DISMIN_APETITO, 0,"","disminucionApetito",validarInfo);
                        creaNodoTriage("FALTA DE PIEZAS DENTARIAS:",$scope.paciente.FALTA_PZAS_DENTALES, 0,"","faltaPiezasDentales",validarInfo);
                        creaNodoTriage("AYUNO PROLONGADO MAS DE 5 DÍAS:",$scope.paciente.AYUNO_PROLONG, 0,"","ayunoProlongado",validarInfo);
                        creaNodoTriage("TOLERANCIA A LA VIA ORAL:",$scope.paciente.TOL_VIA_ORAL, 0,"","toleranciaVialOral",validarInfo);
                        creaNodoTriage("CAPACIDADES DIFERENTES:",$scope.paciente.CAPACIDADES_DIF, 0,"","capacidadesDiferentes",validarInfo);
                        break;
              // TRIAGE de Obstetricia
              case 3:   if ($scope.paciente.C_LUGARREF == undefined)
                          $scope.paciente.C_LUGARREF = " ";
                        creaNodoTriage("LUGAR DE REFERENCIA","", 0,$scope.paciente.C_LUGARREF,"lugarReferencia",!validarInfo);
                        if ($scope.paciente.C_MOTIVOENVIO == undefined)
                          $scope.paciente.C_MOTIVOENVIO = " ";
                        creaNodoTriage("MOTIVO DE ENVIO","", 0,$scope.paciente.C_MOTIVOENVIO,"motiviEnvio",!validarInfo);
                        if ($scope.paciente.C_MEDICOQUEENVIA == undefined)
                          $scope.paciente.C_MEDICOQUEENVIA = " ";
                        creaNodoTriage("MÉDICO QUE ENVIA","", 0,$scope.paciente.C_MEDICOQUEENVIA,"medicoQueEnvia",!validarInfo);
                        creaNodoTriage("¿CUENTA CON SEGURO POPULAR?",$scope.paciente.SEGURO_POPULAR, 0,"","cuentaConSeguroPopular",validarInfo);
                        creaNodoTriage("HORA DE AUTORIZACIÓN",moment($scope.paciente.DHORAAUTORIZACION).format().substring(11, 19), 0,"","horaAutorizacion",!validarInfo);
                        if ($scope.paciente.C_MEDICOQUEAUTORIZA == undefined)
                          $scope.paciente.C_MEDICOQUEAUTORIZA = " ";
                        creaNodoTriage("MÉDICO QUE AUTORIZA","", 0,$scope.paciente.C_MEDICOQUEAUTORIZA,"medicoQueAutoriza",!validarInfo);
                        if ($scope.paciente.C_MEDICOQUERECIBE == undefined)
                          $scope.paciente.C_MEDICOQUERECIBE = " ";
                        creaNodoTriage("MÉDICO QUE RECIBE","", 0,$scope.paciente.C_MEDICOQUERECIBE,"medicoQueRecibe",!validarInfo);
                        creaNodoTriage("TA",$scope.paciente.N_TAOBS,$scope.tabEstatusObstetricia[0],"","ta",!validarInfo);
                        creaNodoTriage("FC",$scope.paciente.N_FCOBS,$scope.tabEstatusObstetricia[1],"","fc",!validarInfo);
                        creaNodoTriage("FCF",$scope.paciente.N_FCFOBS,$scope.tabEstatusObstetricia[2],"","fcf",!validarInfo);
                        creaNodoTriage("SAT",$scope.paciente.N_SATOBS,$scope.tabEstatusObstetricia[3],"","sat",!validarInfo);
                        creaNodoTriage("T",$scope.paciente.N_TOBS, $scope.tabEstatusObstetricia[4],"","t",!validarInfo);;
                        creaNodoTriage("DM",$scope.paciente.N_DMOBS, 0,"","dm",validarInfo);
                        creaNodoTriage("HAS",$scope.paciente.N_HASOBS, 0,"","has",validarInfo);
                        creaNodoTriage("Angina",$scope.paciente.N_ANGINAOBS, 0,"","angina",validarInfo);
                        creaNodoTriage("IRC",$scope.paciente.N_IRCOBS, 0,"","irc",validarInfo);
                        creaNodoTriage("EVC",$scope.paciente.N_EVCOBS, 0,"","evc",validarInfo);
                        creaNodoTriage("Pre eclampsia",$scope.paciente.N_PRECLAOBS, 0,"","preclampsia",validarInfo);
                        creaNodoTriage("Convulsivos",$scope.paciente.N_CONVULOBS, 0,"","convulsivos",validarInfo);
                        creaNodoTriage("ALÉRGICOS",$scope.paciente.N_ALERGOBS, 0,"","alegicos",validarInfo);
                        creaNodoTriage("TRANSFUCIONALES",$scope.paciente.N_TRANSFOBS, 0,"","trasfucionales",validarInfo);
                        creaNodoTriage("QX",$scope.paciente.N_QXOBS, 0,"","qx",validarInfo);
                        creaNodoTriage("HEPÁTICOS",$scope.paciente.N_HEPATOBS, 0,"","hepaticos",validarInfo);
                        creaNodoTriage("ONCOL",$scope.paciente.N_ONCOLOBS, 0,"","oncol",validarInfo);
                        creaNodoTriage("OTROS",$scope.paciente.N_OTROBS, 0,"","otros",!validarInfo);
                        creaNodoTriage("¿PACIENTE ENTUBADO?",$scope.paciente.ENTUBADO, 0,"","pacienteEntubado",validarInfo);
                        creaNodoTriage("¿OPERADO?",$scope.paciente.OPERADO, 0,$scope.paciente.T_TIPOCOBS,"operado",validarInfo);
                        creaNodoTriage("SANGRADO CUANTIFICADO (ml)",$scope.paciente.N_SANGRADOOBS, 0,"","sangradoCuantificado",!validarInfo);
                        creaNodoTriage("URESIS",$scope.paciente.N_URESISOBS, 0,"","uresis",!validarInfo);
                        creaNodoTriage("BALANCE DE LIQUIDOS",$scope.paciente.N_BLIQUIDOSOBS, 0,"","balanceLiquidos",!validarInfo);

                        angular.forEach($scope.tabGravedadObstetricia, function(tabla, index) {
                          if (tabla.estatus)
                            creaNodoTriage(tabla.descripcion, tabla.estatus, tabla.puntos,"","gravedad"+index,!validarInfo);
                          else
                            creaNodoTriage(tabla.descripcion, tabla.estatus, 0,"","gravedad"+index,!validarInfo);
                        });
                        creaNodoTriage("FC",$scope.valoresTabIIObs0,$scope.valoresTabIIObs[0].puntos,"","svfc",!validarInfo);
                        creaNodoTriage("TEMP",$scope.valoresTabIIObs1,$scope.valoresTabIIObs[1].puntos,"","svtemp",!validarInfo);
                        creaNodoTriage("FR",$scope.valoresTabIIObs2,$scope.valoresTabIIObs[2].puntos,"","svfr",!validarInfo);
                        creaNodoTriage("TA",$scope.valoresTabIIObs3,$scope.valoresTabIIObs[3].puntos,"","svta",!validarInfo);
                        creaNodoTriage("GLASSGOW",$scope.valoresTabIIIObs0,$scope.valoresTabIIIObs[0].puntos,"","neroglassgow",!validarInfo);
                        creaNodoTriage("NEUROLÓGICO",$scope.valoresTabIIIObs1,$scope.valoresTabIIIObs[1].puntos,"","neronerologicos",!validarInfo);
                        creaNodoTriage("CONVULCIONES",$scope.valoresTabIIIObs2,$scope.valoresTabIIIObs[2].puntos,"","neroconvulciones",!validarInfo);
                        creaNodoTriage("TRAUMA",$scope.valoresTabIIIObs3,$scope.valoresTabIIIObs[3].puntos,"","nerotrauma",!validarInfo);
                        angular.forEach($scope.tabAnexoObstetricia, function(tabla, index) {
                          angular.forEach(tabla.contenido, function(subTabla, index) {
                            creaNodoTriage(subTabla.descripcion, subTabla.valor, 0,"","anexo"+tabla.opcion+index,!validarInfo);
                          });
                        });
                        creaNodoTriage("TOTAL TRANSFUCIONES",$scope.totalTransfuciones,0,"","anexo54",!validarInfo);
                        creaNodoTriage("OBSERVACIONES","",0,$scope.paciente.T_OBSERVOBSTETRICIA,"observaciones",!validarInfo);
                        break;

            }
            data.TRIAGES = $scope.TRIAGES;
            if(data.C_CALIFICACION == "AMARILLO"){
              if($scope.DESTINO_PACIENTE == undefined){
                mensajes.alerta("AVISO","DEBES SELECCIONAR UN DESTINO DEL PACIENTE","ACEPTAR");
                $scope.btnRegistro = false;
                return '';
              }
              else if($scope.DESTINO_PACIENTE == 0){
                data.CONSULTA = true;
              }
            }
            
            peticiones.postMethodPDF(actionURL,data)
            .success(function(data){
                var file = new Blob([data], {type: 'application/pdf'});
                var fileURL = URL.createObjectURL(file);
                reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
                $state.go('agendaConsultaPaciente',{state: 'triage'});
            })
            .error(function(data){
                mensajes.alerta('ERROR','NO SE PUDO GENERAR EL DOCUMENTO','ACEPTAR');
                $scope.btnRegistro = false;
            });
          }
    }



    function creaNodoTriage(C_DESCRIPCION, C_VALOR, N_PUNTAJE, C_MOTIVO, C_PARAMETRO, VALIDAR) {
      var nodo = {};

      if (VALIDAR) {
        if (C_VALOR == undefined)
          C_VALOR = false;
        if (!C_VALOR)
          C_MOTIVO = "";
      }

      nodo.C_DESCRIPCION = C_DESCRIPCION;
      nodo.C_VALOR = C_VALOR;
      nodo.N_PUNTAJE = N_PUNTAJE;
      nodo.C_MOTIVO = C_MOTIVO;
      nodo.C_PARAMETRO = C_PARAMETRO;

      $scope.TRIAGES.push(nodo);

    }

    function habilitaTRIAGE(tipoTRIAGE) {
      $scope.TIPO_TRIAGE = tipoTRIAGE;
      $scope.TRIAGE_ADULTOS     = false;
      $scope.TRIAGE_OBSTETRICIA = false;
      $scope.TRIAGE_PEDIATRIA   = false;

      switch (tipoTRIAGE) {
        case 1: $scope.TRIAGE_PEDIATRIA   = true; break;
        case 2: $scope.TRIAGE_ADULTOS     = true; break;
        case 3: $scope.TRIAGE_OBSTETRICIA = true; break;
      }

      console.log($scope.TRIAGE.PEDIATRIA);
      console.log($scope.TRIAGE.ADULTOS);
      console.log($scope.TRIAGE.OBSTETRICIA);
    }

    $scope.cancelar = function() {
      $state.go('agendaConsultaPaciente',{state: 'triage'});
    }

    $scope.calculaIMC = function() {
      var tempTalla = parseFloat($scope.paciente.N_TALLA);
      var tempPeso  = parseFloat($scope.paciente.N_PESO);
      if (!isNaN(tempTalla) && !isNaN(tempPeso))
        if (tempTalla > 0) {
          $scope.paciente.N_IMC = (tempPeso/(tempTalla*tempTalla)).toFixed(2);
          if ($scope.paciente.N_IMC <= 18.49)
            $scope.paciente.VALOR_NUTRICION = "DESNUTRIDO";
          else
            if ($scope.paciente.N_IMC >=18.50 && $scope.paciente.N_IMC <= 24.99)
              $scope.paciente.VALOR_NUTRICION = "NORMAL";
            else
              $scope.paciente.VALOR_NUTRICION = "SOBREPESO";
        } else {
            $scope.paciente.VALOR_NUTRICION = "";
            $scope.N_IMC = "";
        }
    }

    $scope.validaFC = function(tipo,indice,valor) {
      var valorNum = parseFloat(valor);
      var valorAnterior;
      switch (tipo) {
        case 'A': valorAnterior = $scope.tabEstatus[indice];
                  $scope.tabEstatus[indice] = (valorNum>=30 && valorNum<=100) ? 0 : 1;
                  if (valorAnterior != $scope.tabEstatus[indice])
                    $scope.actualizaPuntajeAdultos($scope.tabEstatus[indice]);
                  break;
        case 'O': valorAnterior = $scope.tabEstatusObstetricia[indice];
                  $scope.tabEstatusObstetricia[indice] = (valorNum>=100 && valorNum<=150) ? 0 : 1;
                  /*if (valorAnterior != $scope.tabEstatusObstetricia[indice])
                    $scope.actualizaPuntajeObstetricia($scope.tabEstatusObstetricia[indice]);*/
                  break;
        case 'P': valorAnterior = $scope.tabEstatusPediatria[indice];
                  $scope.tabEstatusPediatria[indice] = (valorNum>=55 && valorNum<=190) ? 0 : 1;
                  /*if (valorAnterior != $scope.tabEstatusPediatria[indice])
                    $scope.actualizaPuntajePediatria($scope.tabEstatusPediatria[indice]);*/
                  break;
      }
    }

    $scope.validaFR = function(tipo,indice,valor) {
      var valorNum = parseFloat(valor);
      var valorAnterior;
      switch (tipo) {
        case 'A': valorAnterior = $scope.tabEstatus[indice];
                  $scope.tabEstatus[indice] = (valorNum>=12 && valorNum<=30) ? 0 : 1;
                  if (valorAnterior != $scope.tabEstatus[indice])
                    $scope.actualizaPuntajeAdultos($scope.tabEstatus[indice]);
                  break;
        case 'O': valorAnterior = $scope.tabEstatusObstetricia[indice];
                  $scope.tabEstatusObstetricia[indice] = (valorNum>=12 && valorNum<=20) ? 0 : 1;
                  /*if (valorAnterior != $scope.tabEstatusObstetricia[indice])
                    $scope.actualizaPuntajeObstetricia($scope.tabEstatusObstetricia[indice]);*/
                  break;
        case 'P': valorAnterior = $scope.tabEstatusPediatria[indice];
                  $scope.tabEstatusPediatria[indice] = (valorNum>=12 && valorNum<=20) ? 0 : 1;
                  /*if (valorAnterior != $scope.tabEstatusPediatria[indice])
                    $scope.actualizaPuntajePediatria($scope.tabEstatusPediatria[indice]);*/
                  break;
      }
    }

    $scope.validaFCF = function(indice,valor) {
      var valorNum = parseFloat(valor);
      var valorAnterior = $scope.tabEstatusObstetricia[indice];
      $scope.tabEstatusObstetricia[indice] = (valorNum>=55 && valorNum<=190) ? 0 : 1;
      /*if (valorAnterior != $scope.tabEstatusObstetricia[indice])
        $scope.actualizaPuntajeObstetricia($scope.tabEstatusObstetricia[indice]);*/
    }

    $scope.validaSAT = function(tipo,indice,valor) {
      var valorNum = parseFloat(valor);
      var valorAnterior;
      switch (tipo) {
        case 'A': valorAnterior = $scope.tabEstatus[indice];
                  $scope.tabEstatus[indice] = (valorNum>=88 && valorNum<=100) ? 0 : 1;
                  if (valorAnterior != $scope.tabEstatus[indice])
                    $scope.actualizaPuntajeAdultos($scope.tabEstatus[indice]);
                  break;
        case 'O': valorAnterior = $scope.tabEstatusObstetricia[indice];
                  $scope.tabEstatusObstetricia[indice] = (valorNum>=88 && valorNum<=92) ? 0 : 1;
                  /*if (valorAnterior != $scope.tabEstatusObstetricia[indice])
                    $scope.actualizaPuntajeObstetricia($scope.tabEstatusObstetricia[indice]);*/
                  break;
        case 'P': valorAnterior = $scope.tabEstatusPediatria[indice];
                  $scope.tabEstatusPediatria[indice] = (valorNum>=88 && valorNum<=92) ? 0 : 1;
                  /*if (valorAnterior != $scope.tabEstatusPediatria[indice])
                    $scope.actualizaPuntajePediatria($scope.tabEstatusPediatria[indice]);*/
                  break;
      }
    }

    $scope.validaTEMP = function(tipo,indice,valor) {
      var valorNum = parseFloat(valor);
      var valorAnterior;
      switch (tipo) {
        case 'A': valorAnterior = $scope.tabEstatus[indice];
                  $scope.tabEstatus[indice] = (valorNum>=35 && valorNum<=38) ? 0 : 1;
                  if (valorAnterior != $scope.tabEstatus[indice])
                    $scope.actualizaPuntajeAdultos($scope.tabEstatus[indice]);
                  break;
        case 'O': valorAnterior = $scope.tabEstatusObstetricia[indice];
                  $scope.tabEstatusObstetricia[indice] = (valorNum>=36 && valorNum<=37.7) ? 0 : 1;
                  /*if (valorAnterior != $scope.tabEstatusObstetricia[indice])
                    $scope.actualizaPuntajeObstetricia($scope.tabEstatusObstetricia[indice]);*/
                  break;
        case 'P': valorAnterior = $scope.tabEstatusPediatria[indice];
                  $scope.tabEstatusPediatria[indice] = (valorNum>=36 && valorNum<=37.7) ? 0 : 1;
                  /*if (valorAnterior != $scope.tabEstatusPediatria[indice])
                    $scope.actualizaPuntajePediatria($scope.tabEstatusPediatria[indice]);*/
                  break;
      }
    }

    $scope.validaTA = function(tipo,indice,valor) {
      if (valor != undefined) {
        var valores = valor.split("/");
        var sistolica   = parseFloat(valores[0]);
        var diastolica  = parseFloat(valores[1]);
        var valorAnterior;
        switch (tipo) {
          case 'A': valorAnterior = $scope.tabEstatus[indice];
                    $scope.tabEstatus[indice] = (sistolica>=120 && sistolica<=129 && diastolica>=80 && diastolica<=84) ? 0 : 1;
                    if (valorAnterior != $scope.tabEstatus[indice])
                      $scope.actualizaPuntajeAdultos($scope.tabEstatus[indice]);
                    break;
          case 'O': valorAnterior = $scope.tabEstatusObstetricia[indice];
                    $scope.tabEstatusObstetricia[indice] = (sistolica>=100 && sistolica<=120 && diastolica>=60 && diastolica<=80) ? 0 : 1;
                    /*if (valorAnterior != $scope.tabEstatusObstetricia[indice])
                      $scope.actualizaPuntajeObstetricia($scope.tabEstatusObstetricia[indice]);*/
                    break;
          case 'P': valorAnterior = $scope.tabEstatusPediatria[indice];
                    $scope.tabEstatusPediatria[indice] = (sistolica>=100 && sistolica<=120 && diastolica>=60 && diastolica<=80) ? 0 : 1;
                    /*if (valorAnterior != $scope.tabEstatusPediatria[indice])
                      $scope.actualizaPuntajePediatria($scope.tabEstatusPediatria[indice]);*/
                    break;
        }
      }
    }

    $scope.catDestinoPaciente = [{C_DESCRIPCION : 'CONSULTA URGENCIAS' , NP_ID : 0},{C_DESCRIPCION : 'OBSERVACIÓN URGENCIAS', NP_ID : 1}];
    
  }]);


})();
