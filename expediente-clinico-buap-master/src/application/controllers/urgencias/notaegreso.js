'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:UrgenciasNotaegresoCtrl
 * @description
 * # UrgenciasNotaegresoCtrl
 * Controller of the expediente
 */

var _p;
 angular.module('expediente')
  .controller('UrgenciasNotaegresoCtrl', function ($scope, pacientes, $http, $q, catalogos, mensajes, peticiones, $state, $window, $localStorage, $timeout , reportes) {
  $scope.fe = {
      FIRMA : {},
    };    

  $scope.ESTANCIA_EN_HOSPITAL = "";  
  
  $scope.inicializaPagina = function() {
    pacientes.getPaciente()
      .success(function(data){

        
        $scope.paciente = data.data[0];
        
        $scope.paciente.NF_NOTA_MEDICA = $localStorage.paciente.NF_NOTA_MEDICA;
        $scope.paciente.NOMBRE_COMPLETO = $scope.paciente.C_NOMBRE + ' ' + 
                                          $scope.paciente.C_PRIMER_APELLIDO + ' ' +
                                          $scope.paciente.C_SEGUNDO_APELLIDO;

        $scope.paciente.EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
        $scope.paciente.F_EGRESO = moment().toDate();
        $scope.paciente.H_ELABORACION = moment().format('HH:mm');
        if (!$scope.paciente.hasOwnProperty("C_CURP"))
          $scope.paciente.C_CURP = " ";        
        
        console.log("Datos del paciente");
        console.log($scope.paciente);
        
        $http.get(baseURL + 'notaegreso/'+ $scope.paciente.NP_EXPEDIENTE.replace('/', ''))
        .success(function(result){
          console.log("Informacion relacionada..");
          console.log(result);
          $scope.isConsulta = false;
          $scope.paciente.DIAGNOSTICO_INGRESO_PRINCIPAL = result.NF_NOTA_INGRESO.DIAGNOSTICO_PRINCIPAL;
          $scope.paciente.DIAGNOSTICO_INGRESO_SECUNDARIO = ""
          angular.forEach(result.NF_NOTA_INGRESO.DIAGNOSTICO_SECUNDARIO, function(diagnostico, index) {
            $scope.paciente.DIAGNOSTICO_INGRESO_SECUNDARIO += diagnostico.C_DESCRIPCION + "; ";
          });


          $scope.paciente.DIAGNOSTICO_EGRESO_PRINCIPAL  = result.NF_NOTA_EVOLUTIVA.DIAGNOSTICO_PRINCIPAL;
          $scope.paciente.DIAGNOSTICO_EGRESO_SECUNDARIO = ""
          angular.forEach(result.NF_NOTA_EVOLUTIVA.DIAGNOSTICO_SECUNDARIO, function(diagnostico, index) {
            $scope.paciente.DIAGNOSTICO_EGRESO_SECUNDARIO += diagnostico.C_DESCRIPCION + "; ";
          });

          $scope.alta = {};
          $scope.alta.RESUMEN_EVOLUCION = result.NF_NOTA_EVOLUTIVA.C_EXPLORACION_FISICA;
          $scope.alta.PROC_REALIZADOS = "";
          if (result.hasOwnProperty("LABORATORIO")){
            angular.forEach(result.LABORATORIO.NOTAS_MEDICAS, function(tabla, index) {
              angular.forEach(tabla.SERVICIOS, function(servicio, index)  {
                $scope.alta.PROC_REALIZADOS += servicio.T_FECHA_CMD + " " +servicio.C_DESCRIPCION;  
              });
            });    
          }
          
          $scope.alta.C_PRONOSTICO = result.NF_NOTA_EVOLUTIVA.C_PRONOSTICO;
          
          if(result.NF_NOTA_EVOLUTIVA.N_ESTATUS_PRONOSTICO != undefined){
            $scope.DESCRIP_FUNCION = (result.NF_NOTA_EVOLUTIVA.N_ESTATUS_PRONOSTICO === 1 || result.NF_NOTA_EVOLUTIVA.N_ESTATUS_PRONOSTICO === 2 ) ? "BUENO PARA LA FUNCIÓN" : "MALO PARA LA FUNCIÓN";              
            $scope.DESCRIP_VIDA    = (result.NF_NOTA_EVOLUTIVA.N_ESTATUS_PRONOSTICO === 1 || result.NF_NOTA_EVOLUTIVA.N_ESTATUS_PRONOSTICO === 3 ) ? "BUENO PARA LA VIDA" : "MALO PARA LA VIDA";
            $scope.alta.C_PRONOSTICO = $scope.DESCRIP_FUNCION+", "+$scope.DESCRIP_VIDA+". " + $scope.alta.C_PRONOSTICO;
            console.log($scope.DESCRIP_FUNCION+", "+$scope.DESCRIP_VIDA+". ");
          }
          
          $scope.NF_NOTA_EVOLUTIVA = result.NF_NOTA_EVOLUTIVA.NP_ID;
          $scope.NF_NOTA_INGRESO = result.NF_NOTA_INGRESO.NP_ID;
          $scope.alta.FACTORES_RIESGO = false;
          $scope.alta.DESCRIP_FACTORES_RIESGO = "";          
          $scope.alta.PROBL_CLINICOS = false;
          $scope.alta.DESCRIP_PROBL_CLINICOS = "";
          $scope.alta.F_EGRESO = moment().toDate();
          $scope.F_MAX = moment().toDate();
          $scope.paciente.F_FECHA_INGRESO = result.NF_NOTA_INGRESO.F_FECHA; 
          $scope.paciente.F_FECHA_INGRESO = $scope.paciente.F_FECHA_INGRESO.substring(0,10);

          //console.log("medico:");
          //console.log(usuario);
          $scope.MEDICO = usuario.NOMBRE_COMPLETO;          
          $scope.ID     = usuario.NP_EXPEDIENTE;
          $scope.CEDULA = usuario.CEDULA;
        })
        .error(function(err){
          mensajes.alerta('ERROR',err.error,'ACEPTAR!');  
          if ($localStorage.regresarA == 'U') 
            $state.go('agendaConsultaPaciente');
          else 
            $state.go('hospitalizacionConsultaPaciente');
          console.log(err);
        });

        // Catalogo de Motivos
        catalogos.getCatalogos(28)
        .success(function(data){
          $scope.motivos = data;
        })
        .error(function(err){
          mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
        }); 

      })  
      .error(function(err){
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');        
      });
  } 
    
    
    $scope.registraNota = function() {
      //console.log($scope.alta.ANEXO);
      $scope.btnRegistro = true;
      if($scope.fe.FIRMA.keyPriv != undefined && $scope.fe.FIRMA.certificado != undefined) {  
      
        var data = {};
        data.DATOS_PACIENTE  = $scope.paciente;

        data.FIRMA           = { keyPriv     : $scope.fe.FIRMA.keyPriv,
                                 id          : $scope.ID,
                                 CEDULA      : $scope.CEDULA,
                                 NOMBRE      : $scope.MEDICO,
                                 certificado : $scope.fe.FIRMA.certificado };

        /* if ($scope.motivo == 1 || $scope.motivo == 4 || $scope.motivo == 5) { */
          data.NOTA_EGRESO  = { C_ESTANCIA_HOSPITALARIA : $scope.alta.ESTANCIA_EN_HOSPITAL,                                  
                                C_EVOLUCION_Y_ESTADO    : $scope.alta.RESUMEN_EVOLUCION,
                                F_FECHA_INGRESO         : $scope.paciente.F_FECHA_INGRESO,
                                DIAGNOSTICO_INGRESO_PRINCIPAL   : $scope.paciente.DIAGNOSTICO_INGRESO_PRINCIPAL,
                                DIAGNOSTICO_INGRESO_SECUNDARIO   : $scope.paciente.DIAGNOSTICO_INGRESO_SECUNDARIO,

                                DIAGNOSTICO_EGRESO_PRINCIPAL   : $scope.paciente.DIAGNOSTICO_EGRESO_PRINCIPAL,
                                DIAGNOSTICO_EGRESO_SECUNDARIO   : $scope.paciente.DIAGNOSTICO_EGRESO_SECUNDARIO,

                                //C_DIAGNOSTICO_EGRESO    : $scope.paciente.DIAGNOSTICO_EGRESO,

                                TIPO_NOTA_EGRESO        : $scope.motivo,
                                C_NOTA_EGRESO           : _.filter($scope.motivos, function(val){
                                                            return $scope.motivo === val.ID;
                                                          })[0].cDescripcion,
                                F_FECHA_EGRESO          : moment($scope.alta.F_EGRESO).format("DD/MM/YYYY"),
                              };  
          if ($scope.motivo == 1 || $scope.motivo == 4)             
            _.extend(data.NOTA_EGRESO, {  C_PLAN_TRATAMIENTO      : $scope.alta.MANEJO_TRATAMIENTO,
                                          N_PROBLEMAS             : $scope.alta.PROBL_CLINICOS,
                                          C_PROBLEMAS             : $scope.alta.DESCRIP_PROBL_CLINICOS,
                                          C_VIGILANCIA_AMBULATORIA: $scope.alta.RECOMENDACIONES,                                          
                                          N_FACTORES_RIESGO       : $scope.alta.FACTORES_RIESGO,
                                          C_FACTORES_RIESGO       : $scope.alta.DESCRIP_FACTORES_RIESGO,
                                          NF_NOTA_INGRESO         : $scope.NF_NOTA_INGRESO,
                                          NF_NOTA_EVOLUTIVA       : $scope.NF_NOTA_EVOLUTIVA,
                                          C_PROC_REALIZADOS       : $scope.alta.PROC_REALIZADOS,
                                          C_PRONOSTICO            : $scope.alta.C_PRONOSTICO
                                        });            
          else
            _.extend(data.NOTA_EGRESO, {  C_PLAN_TRATAMIENTO      : " ",
                                          N_PROBLEMAS             : false,
                                          C_PROBLEMAS             : " ",
                                          C_VIGILANCIA_AMBULATORIA: " ",   
                                          N_FACTORES_RIESGO       : false,
                                          C_FACTORES_RIESGO       : " ",
                                          NF_NOTA_INGRESO         : $scope.NF_NOTA_INGRESO,
                                          NF_NOTA_EVOLUTIVA       : $scope.NF_NOTA_EVOLUTIVA,
                                          C_PROC_REALIZADOS       : " ",
                                          C_PRONOSTICO            : " "
                                       }); 
          
          console.log("Datos a enviar..");
          console.log(data);

          if ($scope.motivo == 1 || $scope.motivo == 4 || $scope.motivo == 5) {
            peticiones.putMethodPDF('notaegreso',data)
            .success(function(data){

                  var file = new Blob([data], {type: 'application/pdf'});
                  var fileURL = URL.createObjectURL(file);
                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
                  $scope.btnRegistro = false;
                  if ($localStorage.regresarA == 'U') 
                    $state.go('agendaConsultaPaciente');
                  else 
                    $state.go('hospitalizacionConsultaPaciente');
            })
            .error(function(data){            
                  console.log(data);
                  $scope.btnRegistro = false;
                  if (!data.hasOwnProperty('error')) {
                    var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                    console.log(decodedString);
                    var obj = JSON.parse(decodedString);
                    mensajes.alerta('ERROR',obj.error,'ACEPTAR');
                  } 
                  else
                    mensajes.alerta('ERROR','NO SE PUDO GENERAR EL DOCUMENTO','ACEPTAR');
            });  
          }
          else {
            if($scope.fileSeleccionado != undefined) {

              peticiones.postMethod('notaegreso',data)
              .success(function(data) {
                console.log(data);
                var url = "upload/notaegreso/"+data;

                console.log("URL: "+url);
                console.log("file: ");
                console.log($scope.fileSeleccionado);
                peticiones.uploadArchivo(url, $scope.fileSeleccionado)
                .then(function (response) { 
                        $timeout(function () {
                          console.log(response.data);
                          $scope.progressVisible = response.data.estatus;
                           mensajes.alerta('INFORMACION','SE GENERO LA NOTA DE EGRESO','ACEPTAR');
                           if ($localStorage.regresarA == 'U') 
                              $state.go('agendaConsultaPaciente');
                            else 
                              $state.go('hospitalizacionConsultaPaciente');
                        });
                      },
                      function (response) {
                        //console.log(response);
                        if (response.status > 0)
                          $scope.errorMsg = response.status + ': ' + response.data.error;
                        //console.log($scope.errorMsg);
                        //console.log(response.data);
                      },
                      function (evt) {
                        // Math.min is to fix IE which reports 200% sometimes
                        //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));

                        
                            if (evt.lengthComputable) {
                                $scope.progress = Math.round(evt.loaded * 100 / evt.total)
                            } else {
                                $scope.progress = 'unable to compute'
                            }
                        
                      });              
              })
              .error(function(data){
                mensajes.alerta('ERROR','NO SE PUDO GENERAR LA NOTA DE EGRESO','ACEPTAR');
                $scope.btnRegistro = false;
              });  
            }
            else {
              $scope.btnRegistro = false;
              mensajes.alerta('AVISO','DEBES SELECCIONAR EL ANEXO PARA GENERAR LA NOTA DE EGRESO','ACEPTAR!');
            }
          }                
      }
      else
        if ($scope.fe.FIRMA.keyPriv == undefined) {
          $scope.btnRegistro = false;
          mensajes.alerta('AVISO','DEBES SELECCIONAR EL ARCHIVO .KEY PARA GENERAR LA NOTA DE EGRESO','ACEPTAR!');  
        }
        else if ($scope.fe.FIRMA.certificado == undefined) {
              $scope.btnRegistro = false;
              mensajes.alerta('AVISO','DEBES SELECCIONAR EL ARCHIVO .CER PARA GENERAR LA NOTA DE EGRESO','ACEPTAR!');  
        }
    }            
    
 

    $scope.cancelar = function() {
      if ($localStorage.regresarA == 'U') 
        $state.go('agendaConsultaPaciente');
      else 
        $state.go('hospitalizacionConsultaPaciente');
      $scope.btnRegistro = false;     
      //window.history.back();
    }

    $scope.cargaLlave = function (fileContent){
      $scope.fe.FIRMA.keyPriv = fileContent;
    };

    $scope.cargaCertificado = function (fileContent){
      $scope.fe.FIRMA.certificado = fileContent;
    };

    /* $scope.uploadFile = function() {
      console.log($scope.fileSeleccionado);
      uploadFile($scope.fileSeleccionado).then(function(res) {
        console.log(res);
      })

        var fd = new FormData()
        for (var i in $scope.files) {
            fd.append("uploadedFile", $scope.files[i]);
        }
        var xhr = new XMLHttpRequest();
        
        xhr.upload.addEventListener("progress", uploadProgress, false);
        xhr.addEventListener("load", uploadComplete, false);
        xhr.addEventListener("error", uploadFailed, false);
        xhr.addEventListener("abort", uploadCanceled, false);
        xhr.open("POST", "/fileupload");
        console.log(xhr);
        $scope.progressVisible = true;
        xhr.send(fd);
    }; */

    $scope.setFiles = function(element) {    
    $scope.$apply(function(scope) {
      
      // Turn the FileList object into an Array
        $scope.files = [];
        for (var i = 0; i < element.files.length; i++) {
          $scope.files.push(element.files[i]);
        }
      $scope.fileSeleccionado = element.files[0];
      reportes.getReporte( window.URL.createObjectURL( $scope.fileSeleccionado), '_blank' , 'width=800, height=640');
      if ($scope.fileSeleccionado.type != "application/pdf") {
        mensajes.alerta('AVISO','SOLO SE PERMITEN SUBIR ARCHIVOS .PDF','ACEPTAR!');             
      }         
      else
        if (parseInt($scope.fileSeleccionado.size) > 2097152)
          mensajes.alerta('AVISO','EL DOCUMENTO NO PUEDE SER MAYOR A 2 MEGABYTES','ACEPTAR!');
      $scope.progressVisible = false;
      });
    };

    /*function uploadProgress(evt) {
        $scope.$apply(function(){
            if (evt.lengthComputable) {
                $scope.progress = Math.round(evt.loaded * 100 / evt.total)
            } else {
                $scope.progress = 'unable to compute'
            }
        })
    }

    function uploadComplete(evt) {
        
        alert(evt.target.responseText)
    }

    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.")
    }

    function uploadCanceled(evt) {
        scope.$apply(function(){
            scope.progressVisible = false
        })
        alert("The upload has been canceled by the user or the browser dropped the connection.")
    }

    function uploadFile(file) {
      var deferred = $q.defer();
      var formData = new FormData();
      formData.append("name",'prueba');
      formData.append("file",file);
      return $http.post("#/urgencias/server.php", formData, {
        headers: {
          "Content-type": undefined
        },
        transformRequest: formData
      })
      .success(function(res){
        deferred.resolve(res);
      })
      .error(function(msg, code) {
        deferred.reject(msg);
      })
      return deferred.promise;  
    } */

    $scope.uploadPic = function(file) {
      
    }

  });
