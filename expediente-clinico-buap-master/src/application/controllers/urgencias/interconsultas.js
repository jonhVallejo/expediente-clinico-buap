(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:UrgenciasInterconsultasCtrl
 * @description
 * # UrgenciasInterconsultasCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('UrgenciasInterconsultasCtrl', ['consultas','mensajes','$scope','usuario','$localStorage','$state', 'peticiones',
	function (consultas,mensajes,$scope,usuario,$localStorage,$state, peticiones) {
    $scope.datos={};

    $scope.query = {
        filter: '',
        order: '',
        limit: 15,
        page: 1
      };

	
	function getAmarillos(){
		consultas.getSolicitudesInterconsulta('amarillos')//INTERCONSULTA
    .success(function(data){
      var interconsultas = _.map(data, function (el, key) { //SE AGREGA EL TIPO DE NOTA S_INTERCONSULTA = SOLICITUD DE INTERCONSULTA
      	el.CF_TIPO_NOTA = 'INTERCONSULTA';
      	el.idOrder = 1;
      	return el;
    	});
    	interconsultas = _.filter(interconsultas,function(e){ //SE QUITAN LAS SOLICITUDES QUE EL MÉDICO SOLICITÓ
        return e.NF_MEDICO_ATIENDE+'' == usuario.usuario.NP_EXPEDIENTE+'';
      });

      $scope.amarillos = _.map(interconsultas, function (el, key) {
        el.PACIENTE = {};
        el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
        el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
        el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
        return el;
      });
		})
		.error(function(err){
			mensajes.alerta("ERROR","SERVICIO NO DISPONIBLE","ACEPTAR");
		});
	};

	$scope.getInterconsultas = function(){
    consultas.getSolicitudesInterconsulta('rojos')//INTERCONSULTA
    .success(function(data){
      var interconsultas = _.map(data, function (el, key) { //SE AGREGA EL TIPO DE NOTA S_INTERCONSULTA = SOLICITUD DE INTERCONSULTA
      	el.CF_TIPO_NOTA = 'S_INTERCONSULTA';
      	el.idOrder = 1;
      	return el;
    	});

      interconsultas = _.filter(interconsultas,function(e){ //SE QUITAN LAS SOLICITUDES QUE EL MÉDICO SOLICITÓ
        return e.NF_MEDICO_ATIENDE+'' == usuario.usuario.NP_EXPEDIENTE+'';
      });

    	$scope.rojos = _.map(interconsultas, function (el, key) {
      		el.PACIENTE = {};
      		el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
      		el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
      		el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
      		return el;
    	});

    	consultas.getSolicitudesInterconsulta()//INTERCONSULTA
      .success(function(data){
       	var interconsultas = _.map(data, function (el, key) { //SE AGREGA EL TIPO DE NOTA S_INTERCONSULTA = SOLICITUD DE INTERCONSULTA
       		el.CF_TIPO_NOTA = 'S_INTERCONSULTA';
         	el.idOrder = 1;
         	return el;
        });

        interconsultas = _.filter(interconsultas,function(e){ //SE QUITAN LAS SOLICITUDES QUE EL MÉDICO SOLICITÓ
         	return e.NF_MEDICO_ATIENDE+'' == usuario.usuario.NP_EXPEDIENTE+'';
        });

        interconsultas = _.map(interconsultas, function (el, key) {
         	el.PACIENTE = {};
         	el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
          el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
          el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
          return el;
        });

        $scope.rojos = _.union($scope.rojos,interconsultas);
        $scope.datos = $scope.rojos;    
        $scope.datosFiltrados = $scope.datos;

        consultas.getSolicitudesInterconsulta('amarillos')//INTERCONSULTA
        .success(function(data){

          var interconsultas = _.map(data, function (el, key) { //SE AGREGA EL TIPO DE NOTA S_INTERCONSULTA = SOLICITUD DE INTERCONSULTA
            el.CF_TIPO_NOTA = 'INTERCONSULTA';
            el.idOrder = 1;
            return el;
          });

          interconsultas = _.filter(interconsultas,function(e){ //SE QUITAN LAS SOLICITUDES QUE EL MÉDICO SOLICITÓ
            return e.NF_MEDICO_ATIENDE+'' == usuario.usuario.NP_EXPEDIENTE+'';
          });

          $scope.amarillos = _.map(interconsultas, function (el, key) {
            el.PACIENTE = {};
            el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
            el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
            el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
            return el;
          });

          $scope.amarillos = _.union($scope.rojos,$scope.amarillos);
          $scope.datos = $scope.amarillos;
          $scope.datosFiltrados = $scope.datos;
        })
        .error(function(err){
          mensajes.alerta("ERROR","SERVICIO NO DISPONIBLE","ACEPTAR");
        });
      })
      .error(function(err){
				mensajes.alerta("ERROR","SERVICIO NO DISPONIBLE","ACEPTAR");
			});
    })
    .error(function(err){
			mensajes.alerta("ERROR","SERVICIO NO DISPONIBLE","ACEPTAR");
		});
    
  };

  $scope.abrirNota = function(paciente){
    if($localStorage.paciente != undefined){
      if($localStorage.paciente.expediente+'' != paciente.expediente+'' || $localStorage.paciente.tipo != 'INTERCONSULTA'){
        $localStorage.selectedIndex = 0;
        $localStorage.notaMedica = undefined;
      }
    }
    if(paciente.NF_ESTATUS === 1){
      $localStorage.notaMedica = {NP_NOTA_MEDICA : paciente.NF_NOTA_MEDICA}
    }
    $localStorage.paciente = paciente;
    $localStorage.paciente.tipo = 'INTERCONSULTA';
    $localStorage.paciente.idexpediente = $localStorage.paciente.NF_PACIENTE.split("/")[0];
    $localStorage.paciente.expediente = $localStorage.paciente.NF_PACIENTE.replace("/","");
    $state.go('notaMedicaIngreso');
  };


  $scope.buscaPaciente = function(text){
    if(text != undefined)
      
    $scope.datosFiltrados = peticiones.filtrar(text.toUpperCase(), $scope.datos, [{descripcion:"PACIENTE.C_PRIMER_APELLIDO", validar:false, campo:""}, {descripcion:"PACIENTE.C_SEGUNDO_APELLIDO", validar:false, campo:""}]);
      else{
        $scope.datosFiltrados = $scope.datos;
      }
    };





  }]);
})();