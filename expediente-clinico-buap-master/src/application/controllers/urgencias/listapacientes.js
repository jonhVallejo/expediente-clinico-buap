(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:UrgenciasListapacientesCtrl
 * @description
 * # UrgenciasListapacientesCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('UrgenciasListapacientesCtrl', ['$scope','pacientes','medicos','usuario','$state','$localStorage','mensajes','consultas',
	function ($scope,pacientes,medicos,usuario,$state,$localStorage,mensajes,consultas) {

    $scope.datos = [];
    $scope.query 		= {
		filter: '',
	    order: 'order',
	    limit: 15,
	    page: 1
  	};

  	$scope.isPediatria = false;
  	$scope.isGinecologia = false;
  	$scope.isAdultos = false;

    $scope.cargaDatos = function(){
		if(usuario.perfil.clave == 'RESI'){
			$scope.isResidente = true;
			$state.go('bienvenido');
		}
		else{ // es médico
			$scope.isResidente = false;
			$scope.medico = {};
			medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
			.success(function(data){
				if(data.ESPECIALIDADES.CF_ESPECIALIDAD === "PEDIATRIA"){
		    		$scope.isPediatria = true;
		    		$scope.titulo = "PEDIATRICO";
		    	}
		    	else if(data.ESPECIALIDADES.CF_ESPECIALIDAD === "GINECOLOGIA"){
		    		$scope.isGinecologia = true;
		    		$scope.titulo = "OBSTETRICIA";
		    	}
		    	else {
		    		$scope.isAdultos = true;
		    		$scope.titulo = "ADULTOS"
		    	}
				if(data === ''){
					mensajes.alerta('ERROR','NO SE HA PODIDO ENCONTRAR INFORMACIÓN DE ESTE USUARIO','ACEPTAR!');
				}
				else{
					$scope.medico 				 = data;
					$scope.medico.FECHA 		 = new Date();
					$scope.medico.nombreCompleto = $scope.medico.C_PRIMER_APELLIDO +' '+$scope.medico.C_SEGUNDO_APELLIDO+' '+$scope.medico.C_NOMBRE;
				}
			})
			.error(function(data){
				mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
			});

			pacientes.getPacientesUrgencias('verde' , usuario.usuario.NP_EXPEDIENTE)
		    .success(function(data){
		    	$scope.datos = data.data;
		    	getAmarillos();
		    	if($scope.datos != undefined){
		    		$scope.datos = _.map($scope.datos, function (el, key) {
		    			el.order = 1;
		    			el.colorText = 0;
			          	return el;
			        });
		    		$scope.existDatos = ($scope.datos.length == 0) ? true : false;
		    	}

		    })
		    .error(function(data){
		    	getAmarillos();
		    });

		}
	};

	function cargaVerdesProceso(){
		consultas.getPacientesProceso(usuario.usuario.NP_EXPEDIENTE , 'VERDE')
    	.success(function(data){
    		if(data.length > 0){
    			$scope.existDatos = false;
    			$scope.enProceso = _.filter(data , function(el){
    				if($scope.isPediatria) return el.C_CLAVE == "P";
    				else if($scope.isGinecologia) return el.C_CLAVE == "O";
    				else if($scope.isAdultos) return el.C_CLAVE == "A";
    				else return false;
	    		});
	    		
	    		$scope.enProceso = _.map($scope.enProceso, function (el, key) {
	    			el.order = 1;
	    			el.colorText = 0;
	    			el.nombreCompleto = el.PACIENTE.C_PRIMER_APELLIDO +" " + el.PACIENTE.C_SEGUNDO_APELLIDO + " " + el.PACIENTE.C_NOMBRE;
		          	return el;
		        });
		        $scope.datos = _.union($scope.datos , $scope.enProceso);
	    		$scope.registros = $scope.datos;
    		}
    		cargaAmarillosProceso();
    	})
    	.error(function(err){cargaAmarillosProceso();});
    };

	function cargaAmarillosProceso(){
		consultas.getPacientesProceso(usuario.usuario.NP_EXPEDIENTE , 'AMARILLO')
    	.success(function(data){
    		if(data.length > 0){
    			$scope.existDatos = false;
    			$scope.enProceso = _.filter(data , function(el){
    				if($scope.isPediatria) return el.C_CLAVE == "P";
    				else if($scope.isGinecologia) return el.C_CLAVE == "O";
    				else if($scope.isAdultos) return el.C_CLAVE == "A";
    				else return false;
	    		});
	    		
	    		$scope.enProceso = _.map($scope.enProceso, function (el, key) {
	    			el.order = 0;
	    			el.colorText = 1;
	    			el.nombreCompleto = el.PACIENTE.C_PRIMER_APELLIDO +" " + el.PACIENTE.C_SEGUNDO_APELLIDO + " " + el.PACIENTE.C_NOMBRE;
		          	return el;
		        });
		        $scope.datos = _.union($scope.datos , $scope.enProceso);
	    		$scope.registros = $scope.datos;
    		}	    		
    	})
    	.error(function(err){});
    	if($scope.datos != undefined){
    		$scope.datos = _.map($scope.datos, function (el, key) {
    			el.nombreCompleto = el.PACIENTE.C_PRIMER_APELLIDO +" " + el.PACIENTE.C_SEGUNDO_APELLIDO + " " + el.PACIENTE.C_NOMBRE;
	          	return el;
	        });
    		$scope.existDatos = ($scope.datos.length == 0) ? true : false;
    		$scope.registros = $scope.datos;
    	}
	};

	function getAmarillos(){
		//pacientes.getPacientesUrgencias('amarillo' , usuario.usuario.NP_EXPEDIENTE)
		consultas.getAmarillosConsulta(usuario.usuario.NP_EXPEDIENTE)
	    .success(function(data){
	    	console.log(data);
	    	$scope.amarillos = data.data;
	    	if($scope.datos != undefined)
	    		if ($scope.amarillos.length > 0){
					$scope.existDatos = false;
					$scope.amarillos = _.map($scope.amarillos, function (el, key) {
		    			el.order = 0;
		    			el.colorText = 1;
		    			el.nombreCompleto = el.PACIENTE.C_PRIMER_APELLIDO +" " + el.PACIENTE.C_SEGUNDO_APELLIDO + " " + el.PACIENTE.C_NOMBRE;
			          	return el;
			        });
					$scope.datos = _.union($scope.datos , $scope.amarillos);
					$scope.registros = $scope.datos;
	    		}
	    	cargaVerdesProceso();

	    })
	    .error(function(err){cargaVerdesProceso();});
	};

    $scope.parseFechaNac = function(fechaNac){
    	if(fechaNac != undefined)
    		return moment(fechaNac,'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY');
    	return '';
    };

    $scope.abrirAgendaMedica = function(dato){
    	if($localStorage.paciente != undefined){
			if($localStorage.paciente.expediente !== dato.PACIENTE.NP_EXPEDIENTE.replace("/","")){
				$localStorage.selectedIndex = 0;
				$localStorage.notaMedica = undefined;
			}
		}
		if(dato.NF_ESTATUS === 1){
	      $localStorage.notaMedica = {NP_NOTA_MEDICA : dato.NF_NOTA_MEDICA}
	    }
	    if(dato.NP_TRIAGE == undefined){
	    	dato.NP_TRIAGE = dato.NF_TRIAGE;
	    }
    	$localStorage.paciente = {
			idexpediente: 	dato.PACIENTE.NP_EXPEDIENTE,
			expediente: 	dato.PACIENTE.NP_EXPEDIENTE.replace("/",""),
			nombreCompleto: dato.PACIENTE.C_PRIMER_APELLIDO + ' ' + dato.PACIENTE.C_SEGUNDO_APELLIDO + ' ' + dato.PACIENTE.C_NOMBRE,
			idTriage: 		dato.NP_TRIAGE,
			tipo : 'TRIAGE',
			tipoNota : '5' //TRIAGE,
		};
		$state.go("consultaNotaMedica");
    };

    $scope.buscaPaciente = function(text){
    	if(text != undefined){
    		$scope.query.page = 1;
	   		$scope.datos = _.filter($scope.registros , function(e){
	   			var band = false;
	   			if(e.nombreCompleto.toUpperCase().indexOf(text.toUpperCase()) >= 0 ) band = true;
	   			if(e.PACIENTE.NP_EXPEDIENTE != undefined)
	   				if(e.PACIENTE.NP_EXPEDIENTE.indexOf(text.toUpperCase()) >= 0 ) band = true;
	   			if(e.PACIENTE.F_FECHA_DE_NACIMIENTO != undefined)
	   				if(e.PACIENTE.F_FECHA_DE_NACIMIENTO.indexOf(text.toUpperCase()) >= 0 ) band = true;
	   			return band;
	   		});
	   	}
	   	else{
	   		$scope.datos = $scope.registros;
	   	}
    };

  }]);
})();