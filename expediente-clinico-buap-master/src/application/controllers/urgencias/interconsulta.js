'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:UrgenciasInterconsultaCtrl
 * @description
 * # UrgenciasInterconsultaCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('UrgenciasInterconsultaCtrl', function ($scope,mensajes,pacientes,medicos,usuario,$q,consultas,$localStorage,$state , reportes) {

	$scope.consulta = {
		fecha: moment(new Date()).format('DD/MM/YYYY'),
		hora : moment(new Date()).format('HH:mm')
	};

	$scope.cargarDatos = function(){
		pacientes.getPaciente()
		.success(function(data){
			if(data.success){
				$scope.paciente=data.data[0];
				$scope.paciente.nombreCompleto = $scope.paciente.C_PRIMER_APELLIDO+' '+$scope.paciente.C_SEGUNDO_APELLIDO+' '+$scope.paciente.C_NOMBRE;
				$scope.paciente.direccion = $scope.paciente.C_CALLE+' '+$scope.paciente.C_NO_EXTERIOR+' '+$scope.paciente.C_COLONIA+' '+$scope.paciente.C_LOCALIDAD+' '+$scope.paciente.C_MUNICIPIO+' '+$scope.paciente.NF_ENTIDAD_FEDERATIVA;
				$scope.paciente.N_EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
				$scope.paciente.SERVICIO = "URGENCIAS";
				$scope.paciente.SOLICITUD = "SOLICITUD DE INTERCONSULTA"
			}
			else if(!data.success){
				mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
				$state.go('bienvenido');
			}
			else{
				mensajes.alerta('ERROR','LO SENTIMOS, OCURRIÓ UN PROBLEMA','ACEPTAR!');
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});

		medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
		.success(function(data){
			if(data === ''){
				mensajes.alerta('ERROR','NO SE HA PODIDO ENCONTRAR INFORMACIÓN DE ESTE USUARIO','ACEPTAR!');
			}
			else{
				$scope.INTERCONSULTA 				 = data;
				$scope.INTERCONSULTA.NOMBRE_COMPLETO_ACTUAL = $scope.INTERCONSULTA.C_PRIMER_APELLIDO +' '+$scope.INTERCONSULTA.C_SEGUNDO_APELLIDO+' '+$scope.INTERCONSULTA.C_NOMBRE;
				$scope.INTERCONSULTA.C_CARGO_ACTUAL = $scope.INTERCONSULTA.ESPECIALIDADES.CF_ESPECIALIDAD;
				$scope.INTERCONSULTA.C_CARGO_ACTUALID = $scope.INTERCONSULTA.ESPECIALIDADES.NF_ESPECIALIDAD;
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
		});

	}

    $scope.querySearch = function(query){
    	var defered = $q.defer();
    	var promise = defered.promise;
    	$scope.INTERCONSULTA.C_CARGO_SOLICITADO = undefined;
    	if(query != undefined){
			if(query.length>2){
				medicos.getMedicos(query)
			    .success(function(data){
			    	if(data != undefined){
				    	if(data.length>0){
				    		defered.resolve(self.nombres);
					      	self.isDisabled = false;
					      	self.nombres = cargarServiciosMedico(data);
					    }
					    else{
					    	defered.reject({});
					    }
					}
			    })
			    .error(function(data){
			      mensajes.alerta('ERROR','ERROR AL CARGAR LOS DATOS','ACEPTAR!');
			    });
			}
		}
		return promise;
	};

	function cargarServiciosMedico(data){
	    return data.map(function (serv){
	      return {
	        value : (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
	        display: (serv.C_PRIMER_APELLIDO+" "+serv.C_SEGUNDO_APELLIDO+" "+serv.C_NOMBRE).toUpperCase(),
	        nombre: serv.C_NOMBRE,
	        id  : serv.N_EXPEDIENTE,
	        especialidad: serv.ESPECIALIDADES.CF_ESPECIALIDAD,
	        idEspecialidad: serv.ESPECIALIDADES.NF_ESPECIALIDAD
	      };
	    });
	};

	$scope.selectedItemChange= function(item) {
		if(item !== undefined){
			$scope.INTERCONSULTA.C_CARGO_SOLICITADO = item.especialidad;
			$scope.INTERCONSULTA.MEDICO_SOLICITADOID = item.id;
		}
		else{
			$scope.INTERCONSULTA.C_CARGO_SOLICITADO = undefined;
		}
	};

	$scope.solicitar = function(){
		if($scope.INTERCONSULTA.idMedico === -1){
			mensajes.alerta("AVISO","DEBES SELECCIONAR UN MÉDICO","ACEPTAR");
			return null
		}else{
			$scope.solicitud = {
				NF_PACIENTE : $localStorage.paciente.expediente,
				NF_MEDICO_SOLICITA : $scope.INTERCONSULTA.N_EXPEDIENTE,
				NF_MEDICO_ATIENDE : $scope.INTERCONSULTA.idMedico,
				C_MOTIVO : $scope.INTERCONSULTA.C_MOTIVO,
				NF_NOTA_MEDICA : $localStorage.paciente.NF_NOTA_MEDICA
			};
			consultas.solicitudInterconsulta($scope.solicitud)
			.success(function(data){
				if(data.success){
					mensajes.alerta('AVISO','SOLICITUD ENVIADA','ACEPTAR!');
					reportes.getReporte(baseURL + 'solicitudInterconsulta/'+data.Mensaje, '_blank', 'width=800, height=640');
					$state.go('bienvenido');
				}
				else{
					mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
				}
			})
			.error(function(data){
				mensajes.alerta('ERROR','SERVICIO DE SOLICITUD DE INTERCONSULTA NO DISPONIBLE','ACEPTAR!');
			});
		}
	};

	$scope.catMedicos = [];
    $scope.selectedMedico = 0;

	$scope.catEspecialidad = [];
    medicos.getEspecialidades().success(function(data){
		$scope.catEspecialidad = data;
	});

	$scope.selectEspecialidad = function(){
    	$scope.INTERCONSULTA.idMedico = -1;
    	if($scope.INTERCONSULTA.especialidad != undefined){
    		medicos.getMedicos($scope.INTERCONSULTA.especialidad).success(function(data){
	    		if(data.length > 0){
	    			$scope.existMedicos = true;
	    			$scope.catMedicos = _.filter(data,function(e){
						return e.N_EXPEDIENTE+'' !== usuario.usuario.NP_EXPEDIENTE+'';
					});
	    		}
	    		else{
	    			$scope.existMedicos = false;
	    		}
			});
    	}
    };


  });
