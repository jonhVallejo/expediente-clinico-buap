(function () {
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:RootCtrl
 * @description
 * # RootCtrl
 * Controller of the expediente
 */

 angular.module('expediente')
 .controller('RootCtrl', ['$scope', '$state', 'usuario', 'menu', '$mdMedia','$mdDialog','ECE_CONSTANTS', 'login',
  function ($scope, $state, usuario, menu, $mdMedia, $mdDialog,ECE_CONSTANTS, login) {
  $scope.version = ECE_CONSTANTS.VERSION;

  $scope.$watch(function(){
    $scope.usr = usuario.usuario;
  });



  $scope.$watch(function(){
    $scope.state = $state.$current;
  });


  $scope.logout = function(){

    login.logout();
  }

  var originatorEv;
  $scope.openMenu = function($mdOpenMenu, ev) {
    originatorEv = ev;
    $mdOpenMenu(ev);
  };

  $scope.acercaDe = function(ev){
    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
    $mdDialog.show({
      controller: DialogController,
      templateUrl: '/application/views/about.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    })
    .then(function(answer) {
      //$scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      //$scope.status = 'You cancelled the dialog.';
    });
    $scope.$watch(function() {
      return $mdMedia('xs') || $mdMedia('sm');
    }, function(wantsFullScreen) {
      $scope.customFullscreen = (wantsFullScreen === true);
    });
  };

}]);
function DialogController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}
})();
