(function(){
'use strict';

angular.module('expediente')
    .controller('cuestionarioCtrl',['$scope','$q','$http','$state','peticiones','cuestionarioService','mensajes',
      function ($scope, $q,$http,$state,peticiones,cuestionarioService,mensajes) {

        var self = this;
        self.mEspecialidades = [];
        self.mCuestionario = {};
        self.mNuevaOpcionValid = true;
        self.preguntasDependientes = [];
        self.preguntasAuxDep = [];
        var completarPreguntaError = "DEBE COMPLETAR LA PREGUNTA ";
        var descPreguntaError = "DEBE AGREGAR LA DESCRIPCIÓN PARA LA PREGUNTA ";
        var descSeecionError =  "DEBE AGREGAR LA DESCRIPCIÓN PARA LA SECCIÓN ";
        var agregarOpcionPregunta = "DEBE AGREGAR POR LO MENOS UNA OPCIÓN EN LA PREGUNTA ";

        self.tiposDePregunta = {1: "PREGUNTA ABIERTA",
                                7: "PREGUNTAS DE TIPO NÚMERICO",
                                8: "PREGUNTAS DE TIPO FECHA",
                                2: "PREGUNTA DE OPCIÓN MULTIPLE (CHECK BOX)",
                                3: "PREGONTA DE UNA OPCIÓN (RADIO BUTTON)",
                                4: "PREGUNTA ABIERTA CON VARIAS OPCIONES Y ETIQUETAS",
                                5: "PREGUNTA DE UNA OPCIÓN (RADIO BUTTON Y ETIQUETAS)",
                                6: "PREGUNTAS SI/NO QUE LANZAN A OTRA PREGUNTA"}


        // peticiones.getDatos("/catalogo/8")
        //   .then(function(data) {
        //       self.mEspecialidades = data;
        //   })
        //   .catch(function(err) {
        //       console.log(err);
        //   });



 /**
           * [mCuestionario Variable que se utiliza para crear la referencia con el
           * Array en el cual se va a ir creando todo el cuestionario]
           * @type {[type]}
           */
           self.mCuestionario = cuestionarioService.getCuestionario();

          self.mEspecialidades = [{ID: 1, cDescripcion: "ESPECIALIDAD 1"},{ID: 2, cDescripcion: "ESPECIALIDAD 2"},
                        {ID: 3, cDescripcion: "ESPECIALIDAD 3"},{ID: 4, cDescripcion: "ESPECIALIDAD 4"}]


      }]);
})();
