(function(){
'use strict';

angular.module('expediente')
    .controller('addCuestionarioCtrl',['$scope','$q','$http','$state','peticiones','cuestionarioService','mensajes','$mdDialog','$stateParams','catalogos', '$location', '$anchorScroll',
      function ($scope, $q,$http,$state,peticiones,cuestionarioService,mensajes,$mdDialog,$stateParams,catalogos, $location, $anchorScroll) {

        var self = this;
        self.mEspecialidades = [];

        //datos oCuestionarios de los cuestionarios
        self.oCuestionarios = [];
        self.listPregunta = [];
        self.contestarCuestionario = cuestionarioService.getContestarCuestionario();
        self.inhabilitarFormulario = true;
        self.mCuestionario = cuestionarioService.getCuestionario();
        self.preguntasDependientes = [];
        self.preguntasAuxDep = [];
        self.verTipoCuestionario = false;
        self.eliminarCuestionarioBtn = false;
        self.activarEspecialidades = false;

        var completarPreguntaError = "DEBE COMPLETAR LA PREGUNTA ";
        var descPreguntaError = "DEBE AGREGAR LA DESCRIPCIÓN PARA LA PREGUNTA ";
        var descSeecionError =  "DEBE AGREGAR LA DESCRIPCIÓN PARA LA SECCIÓN ";
        var agregarOpcionPregunta = "DEBE AGREGAR POR LO MENOS UNA OPCIÓN EN LA PREGUNTA ";

        self.tiposDePregunta = cuestionarioService.tiposDePregunta;
        self.listOpcionesDerivadas = cuestionarioService.listOpcionesDerivadas;


         $scope.query = {
            order: 'C_DESCRIPCION',
            limit: 5,
            page: 1
        };

        var activarAcciones = function(bandera){
            self.verBotonesAccion       = bandera;
            self.verGuardarCuestionario = bandera;
            self.verTipoCuestionario    = bandera;
        };

        activarAcciones(false);

        /**
         * [cuestionarioEspecifico Función que se encarga de recuperar un cuestionario
         * para su posterior uso (Modificar o Contestar)]
         * @param  {[Int]} ID_CUESTIONARIO [pk del cuestionario]
         * @return {[undefined]}                 [No retorna valor]
         */
        var cuestionarioEspecifico = function(ID_CUESTIONARIO){

          var url = "cuestionario/" + ID_CUESTIONARIO;
                 peticiones.getDatos(url)
                  .then(function(data) {
                      if(data.SECCION){
                          self.activarEspecialidades = true;
                          self.verTipoCuestionario = false;
                          self.verGuardarCuestionario = true;
                          cuestionarioService.setCuestionario(data);
                          todosTiposCuestionario();
                         cuestionarioService.activarHijosContestados(undefined,undefined);
                         cuestionarioService.setContestarCuestionario(false);
                       }else{
                          console.log("Error, no trajo datos.");
                       }
                  })
                  .catch(function(err) {
                      console.log(err);
                  });
          };

          var todosTiposCuestionario = function(){
             catalogos.getCatalogos(26).success(function(data){
                  self.nomTiposCuestionarios = data;
              });
          }

          self.buscarTiposDeCuestionario = function(){
              var NP_ESPECIALIDAD = self.mCuestionario.NP_ESPECIALIDAD;
              var url = "cuestionario/tiposCuestionarios/" + NP_ESPECIALIDAD;

              peticiones.getDatos(url)
                  .then(function(data) {
                      self.verTipoCuestionario = true;
                      self.nomTiposCuestionarios = data;
              })
                  .catch(function(err) {
                      console.log(err);
              });
          }

          self.cambiarTipoCuestionario = function(){
              if(self.mCuestionario.NF_TIPOS_CUESTIONARIOS != undefined){
                  self.verBotonesAccion = true;
              }
          };


      /**
       *  Función que se encarga de almacenar el NP_ID_CUESTIONARIO cunado se vaya a editar
       *  contestar un cuestionario
       * @param  {[type]} NP_ID_CUESTIONARIO [Int]
       * @return {[type]}                    [No retorna ningun dato]
       */
      self.editarCuestionario = function(NP_ID_CUESTIONARIO){
        cuestionarioService.setIdCuestionario(NP_ID_CUESTIONARIO);
        $state.go($stateParams.state);
    };

        self.delCuestionario = function(){
            self.mCuestionario.ESTATUS = 0;
            self.mCuestionario.MODIFICADA = true;
            mensajes.mConfirmacion("PRECAUCIÓN","¿DESEA ELIMINAR EL CUESTIONARIO?",
                                        "SI","NO")
                          .then(function() {

                          }).catch(function() {
                              var cuestionarioFin = angular.toJson(self.mCuestionario);
                                var cuestionarioResult = cuestionarioService.crearCuestionarioFinal(angular.fromJson(cuestionarioFin));
                                editarCuestionarioServer(cuestionarioResult,true);
                          });

        }



        var identificarOpcion = function(){

            if ($state.current.name == "addCuestionario"){
                self.inhabilitarFormulario = false;
                self.contesCuestionario = false;
                cuestionarioService.setContestarCuestionario(undefined);
                cuestionarioService.setEditarCuestionario(undefined);
                self.eliminarCuestionarioBtn = false;
                cuestionarioService.reset(self.mCuestionario);

            }else{
              if ($state.current.name == "modificarCuestionario" & cuestionarioService.getIdCuestionario() != undefined){
                  cuestionarioService.reset(self.mCuestionario,true);
                 self.inhabilitarFormulario = false;
                 self.contesCuestionario = false;
                 cuestionarioService.setContestarCuestionario(undefined);
                 cuestionarioService.setEditarCuestionario(true);
                 self.eliminarCuestionarioBtn = true;
                 //obtenerCuestionarioEdit();
                 cuestionarioEspecifico(cuestionarioService.getIdCuestionario());
              }else{
                if ($state.current.name == "contestarCuestionario" & cuestionarioService.getIdCuestionario() != undefined) {
                    self.inhabilitarFormulario = true;
                    self.contesCuestionario = true;
                    cuestionarioService.setEditarCuestionario(undefined);
                    cuestionarioService.setContestarCuestionario(true);
                    cuestionarioEspecifico(cuestionarioService.getIdCuestionario());
                };
              }
            }
        }

        identificarOpcion();

        peticiones.getDatos("catalogo/8")
          .then(function(data) {
              self.mEspecialidades = data;
          })
          .catch(function(err) {
              console.log(err);
          });





          /**
           * [addSeccion Función que se encarga unicamente de crear una nueva posición en el array
           *  donde se esta creando el cuestionario, esto para que se cree una nueva estructura de
           *  posición en la vista ]
           */


            self.nuevaPregunta = function(ev,key,editar) {
                if(!editar){
                  cuestionarioService.setNuevaPregunta(getIndiceArray(self.mCuestionario.SECCION,key));
                }

                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: '/application/views/cuestionarios/nuevaPregunta.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true
                  })
                  .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                  }, function() {
                    $scope.status = 'You cancelled the dialog.';
                  });
            };


          self.addSeccion = function(){
              //self.verGuardarCuestionario = true;
              var aux = cuestionarioService.addSeccion();
              if(!aux.result){
                  if(aux.errorNombre){
                      mensajes.alerta("ERROR","POR FAVOR INGRESE EL NOMBRE DE LA SECCIÓN, " + aux.error,"OK");
                  }else{
                      mensajes.alerta("ERROR","ANTES DE AGREGAR OTRA SECCIÓN DEBE AGREGAR POR LO MENOS UNA PREGUTNA EN LA SECIÓN: " + aux.error,"OK");
                  }


              }else{
                // $window.location.href = '/#/modificar/cuestionario#finCuestionario';
                // document.location.href = "/#/modificar/cuestionario#finCuestionario";
                //  $location.hash(id);

    // $anchorScroll();


                $location.hash("finCuestionario");
                $anchorScroll();
              }
          };


          self.btnEliminar =  function(key_seccion,key_pregunta){
              var indice_seccion = getIndiceArray(self.mCuestionario.SECCION, key_seccion);
              var id_pregunta = getIndiceArray(self.mCuestionario.SECCION[indice_seccion].PREGUNTAS, key_pregunta);

              if(self.mCuestionario.SECCION[indice_seccion].PREGUNTAS[id_pregunta].TIPO_PREGUNTA =="6"){
                      mensajes.mConfirmacion("PRECAUCIÓN",
                                            "SI ELIMINA ESTA PREGUNTA SE ELIMINARAN TODAS LAS PREGUNTAS DEPENDIENTES DE ESTA, ¿DESEA CONTINUAR?",
                                            "NO","SI")
                      .then(function() {
                         cuestionarioService.delPregunta(indice_seccion,id_pregunta);
                         self.verGuardarCuestionario = cantidadPreguntas();
                      }).catch(function() {
                          console.error('Error');
                      });
                  }else{
                    cuestionarioService.delPregunta(indice_seccion,id_pregunta);
                    self.verGuardarCuestionario = cantidadPreguntas();
                  }

          };



          var cantidadPreguntas = function(){
            if (self.mCuestionario.SECCION.length > 0){
              if(self.mCuestionario.SECCION[0].PREGUNTAS.length > 0){
                return true;
              }
            }
            return false;

          };

          self.cambiarRadio = function(key_seccion,key_pregunta,bandera){
              var indice_seccion = getIndiceArray(self.mCuestionario.SECCION, key_seccion);
              var id_pregunta = getIndiceArray(self.mCuestionario.SECCION[indice_seccion].PREGUNTAS, key_pregunta);
              var aux = self.mCuestionario.SECCION[indice_seccion].PREGUNTAS[id_pregunta];
              if(!self.inhabilitarFormulario && aux.TIPO_PREGUNTA=="6"){
                cuestionarioService.permitirNodoHijo(aux.NP_PREGUNTA,aux.ID_RESPUESTA);
              }else{
                  for (var i = aux.RESPUESTAS.length - 1; i >= 0; i--) {
                      aux.RESPUESTAS[i].SELECTED = false;
                      if (bandera){
                          aux.RESPUESTAS[i].RESPUESTA = "";
                      }
                  };
                  aux.RESPUESTAS[aux.ID_RESPUESTA].SELECTED = true;

                  if (bandera){
                      aux.RESPUESTAS[aux.ID_RESPUESTA].RESPUESTA = self.mCuestionario.SECCION[indice_seccion].PREGUNTAS[id_pregunta].RESPUESTA;
                  }
              };
          };


          self.actualizarRespuesta = function(key_seccion,key_pregunta,bandera){
              var indice_seccion = getIndiceArray(self.mCuestionario.SECCION, key_seccion);
              var id_pregunta = getIndiceArray(self.mCuestionario.SECCION[indice_seccion].PREGUNTAS, key_pregunta);
              var aux = self.mCuestionario.SECCION[indice_seccion].PREGUNTAS[id_pregunta];
              if (bandera){
                  aux.RESPUESTAS[aux.ID_RESPUESTA].RESPUESTA = self.mCuestionario.SECCION[indice_seccion].PREGUNTAS[id_pregunta].RESPUESTA;
              }
          };


          var verificaEspecialidad = function(){
                if (self.mCuestionario.NP_ESPECIALIDAD == undefined){
                    mensajes.alerta("ERROR","POR FAVOR SELECCIONE UNA ESPECIALIDAD PARA EL CUESTIONARIO","OK");
                    return false;
                }
                return true;
          };


          /**
           * [delSeccion Función que se encarga de eliminar una sección, recibe como parametro el
           * $$haskey de un elemento que existe en un array y invoca a diferentes funciones para realizar la
           * operación]
           * @param  {[type]} key [$$haskey del elmento que se desea eliminar]
           * @return {[type]}     [No hay elemento retornado]
           */
          self.delSeccion = function(key){
              var id = getIndiceArray(self.mCuestionario.SECCION,key);
              mensajes.mConfirmacion("PRECAUCIÓN", "ESTA SEGURO QUE DESEA ELIMINAR ESTA SECCIÓN", "NO","SI")
                      .then(function() {
                          cuestionarioService.delSeccion(id);
                          self.verGuardarCuestionario = (self.mCuestionario.SECCION.length > 0) ? true : false;
                      }).catch(function() {
                          console.error('Error');
                      });
          };


          /**
           * [subirSeccion Función que se encarga unicamente de cambiar una sección de posiscion (subir)
           * con respecto de su posición en la  vista]
           * @param  {[type]} key [.$$haskey del elemento a reordenar]
           * @return {[type]}     [description]
           */
          self.subirSeccion =  function(key){
              var id = getIndiceArray(self.mCuestionario.SECCION,key);
              var aux = (id != null) ? cuestionarioService.subirSeccion(id) : false;
          };

          self.subirPregunta =  function(key_seccion,key_pregunta){
              var id_seccion = getIndiceArray(self.mCuestionario.SECCION,key_seccion);
              var id_pregunta = getIndiceArray(self.mCuestionario.SECCION[id_seccion].PREGUNTAS, key_pregunta);
              var aux = (id_seccion != null & id_pregunta != null) ? cuestionarioService.subirPregunta(id_seccion,id_pregunta) : false;
          };


          /**
           * [bajarSeccion Función que se encarga unicamente de cambiar una sección de posición (bajar)
           * con respecto de su posición en la vista ]
           * @param  {[type]} key [.$$haskey del elmento a reordenar]
           * @return {[type]}     [description]
           */
          self.bajarSeccion =  function(key){
              var id = getIndiceArray(self.mCuestionario.SECCION,key);
              var aux = (id != null) ? cuestionarioService.bajarSeccion(id) : false;
          };
          /*[obtenerCuestionarios] Funcion que se encarga de obtener los cuestionarios de acuerdo a la especialidad seleccionada*/
          self.obtenerCuestionarios = function(){
            //var url="http://www.json-generator.com/api/json/get/ceTePeMZCG?indent=2";
            var url="cuestionario/especialidad/"+self.especialidadId;


              peticiones.getDatos(url)
                .then(function(data) {
                self.oCuestionarios = data;
              })
              .catch(function(err) {
                  console.log(err);
              });
          };

          self.bajarPregunta =  function(key_seccion,key_pregunta){
              var id_seccion = getIndiceArray(self.mCuestionario.SECCION,key_seccion);
              var id_pregunta = getIndiceArray(self.mCuestionario.SECCION[id_seccion].PREGUNTAS, key_pregunta);
              var aux = (id_seccion != null & id_pregunta != null) ? cuestionarioService.bajarPregunta(id_seccion,id_pregunta) : false;

          };


          self.editarPregunta = function(ev,key_seccion,key_pregunta,bandera){
              var id_seccion = getIndiceArray(self.mCuestionario.SECCION,key_seccion);
              var id_pregunta = getIndiceArray(self.mCuestionario.SECCION[id_seccion].PREGUNTAS, key_pregunta);
              cuestionarioService.setEditarPregunta(id_seccion,id_pregunta);
              self.nuevaPregunta(ev,0,true);

          };

          /**
           * [verificarContenidoSeccion Función que se va a utilizar para verificar el contenido de una sección
           * esto vara validar cunado se agregan o eliminan secciones]
           * @return {[type]} [valor boleano]
           */
          var verificarNomCuestionario = function(){
                if(self.mCuestionario.NF_TIPOS_CUESTIONARIOS == undefined){
                  mensajes.alerta("ERROR","POR FAVOR SELECCIONE EL TIPO DE CUESTIONARIO","Ok");
                  return false;
                }else{
                  return true;
                }
          };

          var verificaNombreSeccion = function(){
            if(self.mCuestionario.SECCION.length == 0){
                mensajes.alerta("ERROR","PARA GUARDAR EL CUESTIONARIO DEBE CREAR POR LO MENOS UNA SECCIÓN CON UNA PREGUNTA","Ok");
                return false;
            }
            for (var i = self.mCuestionario.SECCION.length - 1; i >= 0; i--) {
                if(self.mCuestionario.SECCION[i].C_DESCRIPCION == ""){
                  mensajes.alerta("ERROR","DEBE AGREGAR UN NOMBRE A TODAS LAS SECCIONES","Ok");
                  return false;
                }
            };
            return true;

          };


          /**
           * [getIndiceArray Función que s eencarga de obtener el indice de un elemento dentro de un Array]
           * @param  {[type]} mArray [Array en el cual se va a realizar la busqueda]
           * @param  {[type]} key    [$$haskey del elemento a buscar]
           * @return {[type]}        [Indice del elemento encontrado, o en caso de no ser encontrado se
           *                          retorna un null]
           */
          var getIndiceArray = function(mArray,key){
              for(var j = mArray.length -1; j >= 0; j--){
                  if(mArray[j].$$hashKey == key){
                    return j
                  }
                }
                return null;
          };

          self.guardarCuestionario = function(){

            if(verificarNomCuestionario()){
                if(verificaEspecialidad()){
                    if(verificaNombreSeccion()){
                        var aux = cuestionarioService.verificaSeccion();
                        if(!aux.result){
                                mensajes.alerta("ERROR","DEBE AGREGAR POR LO MENOS UNA PREGUNTA EN LA SECCIÓN: " + aux.error,"OK");
                                return false;
                          }

                          var cuestionarioFin = angular.toJson(self.mCuestionario);
                          var cuestionarioResult = cuestionarioService.crearCuestionarioFinal(angular.fromJson(cuestionarioFin));

                           mensajes.mConfirmacion("",
                                        "¿DESEA GUARDAR EL CUESTIONARIO?",
                                        "NO","SI")
                          .then(function() {
                              var resultGuardar = ($state.current.name == "addCuestionario") ? postCuestionario(cuestionarioResult) : editarCuestionarioServer(cuestionarioResult);
                          }).catch(function() {
                              console.error('Error');
                          });


                        }
                    }else{
                      //mensajes.alerta("ERROR","PARA GUARDAR EL CUESTIONARIO DEBE CREAR POR LO MENOS UNA SECCIÓN CON UNA PREGUNTA","Ok");
                      return false;
                    }


            }

          };

        var  postCuestionario = function(mData){
              peticiones.postDatos('cuestionario/',mData)
                .then(function(data) {
                    mensajes.alerta("","EL CUESTIONARIO SE GUARDO DE FORMA EXITOSA" ,"OK");
                    cuestionarioService.reset(self.mCuestionario);
                    activarAcciones(false);


                })
                .catch(function(err) {
                    mensajes.alerta("ERROR","NO SE PUDO GUARDAR EL CUESTIONARIO, POR FAVOR CONTACTE AL ADMINISTRADOR" ,"OK");
                    console.log(err);
                });
        };

        //Solo de prueba
         var editarCuestionarioServer = function(mData,eliminarCuestionario){

          var mensaje = (eliminarCuestionario) ? "EL CUESTIONARIO SE ELIMINO DE FORMA EXITOSA" : "EL CUESTIONARIO SE MODIFICO DE FORMA EXITOSA";
            peticiones.putMethod('cuestionario/',mData)
            .success(function(data){

                   cuestionarioService.reset(mData);
                    activarAcciones(false);

                    mensajes.mConfirmacion("",mensaje,"","Ok")
                          .then(function() {
                             $state.go('home');
                          }).catch(function() {
                             $state.go('home');
                          });

            })
            .error(function(data){
              mensajes.alerta("ERROR","NO SE PUDO MODIFICAR EL CUESTIONARIO, POR FAVOR CONTACTE AL ADMINISTRADOR" ,"OK");

            });
        };







        /***********************
         **********************
         *Sección para Guardar las respuestas de un cuestionario
         ***********************
         */

         self.guardarRespuestas = function(){
           console.log(' lo que se va a guardar  1111', self.mCuestionario)
            var result = cuestionarioService.revisarRespuestas();
            if(result.correcta){
                var cuestionarioFin = angular.toJson(self.mCuestionario);

                console.log(' lo que se va a guardar ', self.mCuestionario)

                var cuestionarioResult = cuestionarioService.crearCuestionarioFinal(angular.fromJson(cuestionarioFin));

                postRespuestas(cuestionarioResult);
            }else{
              mensajes.alerta("ERROR","DEBE CONTESTAR TODAS LAS PREGUNTAS OBLIGATORIAS" ,"OK");
            }

         };


         var  postRespuestas = function(mData){
              peticiones.postDatos('cuestionario/respuestas/',mData)
                .then(function(data) {
                    mensajes.alerta("","DATOS ACTUALIZADOS CORRECTAMENTE" ,"OK");

                })
                .catch(function(err) {
                    console.log(err);
                });
        };

        self.seleccionarRespuesta = function(key_seccion,key_pregunta,key_respuesta){
            var id_seccion = getIndiceArray(self.mCuestionario.SECCION,key_seccion);
            var id_pregunta = getIndiceArray(self.mCuestionario.SECCION[id_seccion].PREGUNTAS, key_pregunta);
            var id_respuesta = (key_respuesta) ? getIndiceArray(self.mCuestionario.SECCION[id_seccion].PREGUNTAS[id_pregunta].RESPUESTAS, key_respuesta) : 0;

            self.mCuestionario.SECCION[id_seccion].PREGUNTAS[id_pregunta].RESPUESTAS[id_respuesta].SELECTED = false;
            if(self.mCuestionario.SECCION[id_seccion].PREGUNTAS[id_pregunta].RESPUESTAS[id_respuesta].RESPUESTA.trim().length > 0){
              self.mCuestionario.SECCION[id_seccion].PREGUNTAS[id_pregunta].RESPUESTAS[id_respuesta].SELECTED = true;
            }
        };

        /********************
         * Sección para Guardar las respuestas de un cuestionario
         **********************
         */


function DialogController($scope, $mdDialog,cuestionarioService) {
      $scope.tiposDePregunta = cuestionarioService.tiposDePregunta;
      $scope.editandoPregunta = angular.fromJson(cuestionarioService.getEditarPregunta());
      $scope.listOpcionesDerivadas = cuestionarioService.listOpcionesDerivadas;

      $scope.nuevaOpcion = "";
      $scope.numeroOpciones = false;
      $scope.pregunta = {};
      $scope.pregunta.TIPO_PREGUNTA = 0;
      $scope.pregunta.derivada = undefined;
      $scope.pregunta.NP_PREGUNTA = 0;
      $scope.pregunta.ESTATUS = true;
      $scope.pregunta.OBLIGATORIA = false;
      $scope.pregunta.RESPUESTAS = [];
      $scope.esHija = false;
      var primerEdicion = false;

      if(!$scope.editandoPregunta.NUEVA){
          $scope.pregunta = angular.extend({}, $scope.editandoPregunta.pregunta);
          $scope.editarPreguntas = true;
          $scope.numeroOpciones = true;
          primerEdicion = true;

      };



      $scope.changeTipoPregunta = function(){
          if (primerEdicion){
            return;
          }
              switch($scope.pregunta.TIPO_PREGUNTA) {
                    case "1":
                        $scope.pregunta.RESPUESTAS = [{NP_RESPUESTA: 0, ETIQUETA: "" ,RESPUESTA: "", SELECTED: false}];
                        $scope.numeroOpciones = true;
                        break;
                    case "6":
                        $scope.pregunta.RESPUESTAS = [{NP_RESPUESTA: 0,  ETIQUETA: "NO" ,RESPUESTA: "", SELECTED: false},
                                                      {NP_RESPUESTA: 0,  ETIQUETA: "SI" ,RESPUESTA: "", SELECTED: false}];
                        $scope.numeroOpciones = true;
                        break;
                    case "7":
                        $scope.pregunta.RESPUESTAS = [{NP_RESPUESTA: 0, ETIQUETA: "" ,RESPUESTA: "", SELECTED: false}];
                        $scope.numeroOpciones = true;
                        break;
                    case "8":
                        $scope.pregunta.RESPUESTAS = [{NP_RESPUESTA: 0, ETIQUETA: "" ,RESPUESTA: "", SELECTED: false}];
                        $scope.numeroOpciones = true;
                        break;
                    default:
                        $scope.numeroOpciones = false;
                        $scope.pregunta.RESPUESTAS = [];
                        break;
                }
                $scope.pregunta.OBLIGATORIA = false;
          };


        $scope.verificaNuevaOpcion = function(){
          if($scope.nuevaOpcion){
            if ($scope.nuevaOpcion.trim().length > 0){
                $scope.opcionValida = true;
            }else{
                $scope.opcionValida = false;
            }
          }else {
            $scope.opcionValida = false;
          }
        };

        $scope.addEtiqueta = function(){
              $scope.pregunta.RESPUESTAS.push({
                                                NP_RESPUESTA: 0,
                                                ETIQUETA: $scope.nuevaOpcion.trim(),
                                                RESPUESTA: "",
                                                ESTATUS: true,
                                                SELECTED: false
                                              });
              $scope.opcionValida = false;
              $scope.nuevaOpcion = "";

              if($scope.pregunta.TIPO_PREGUNTA == "3" | $scope.pregunta.TIPO_PREGUNTA == "5"){
                  if($scope.pregunta.RESPUESTAS.length > 1){
                      $scope.numeroOpciones = true;
                  }
              }else{
                $scope.numeroOpciones = true;
              }
          };

      $scope.cancelar = function() {
        cuestionarioService.resetEditandoPregunta();
          $mdDialog.hide();
      };

      $scope.guardar = function() {
          if($scope.editandoPregunta.NUEVA){
              $scope.pregunta.NODO_RAIZ = true;
              $scope.pregunta.NP_PREGUNTA = 0;
              cuestionarioService.addPregunta($scope.editandoPregunta.indice_seccion, $scope.pregunta,undefined);
              cuestionarioService.resetEditandoPregunta();
              $mdDialog.hide();
               self.verGuardarCuestionario = true;
          }else{
              if(!$scope.editandoPregunta.NUEVA){
                  if($scope.editandoPregunta.pregunta.TIPO_PREGUNTA =="6" & $scope.pregunta.TIPO_PREGUNTA != "6"){
                      mensajes.mConfirmacion("PRECAUCIÓN",
                                            "SI MODIFICA ESTA PREGUNTA SE ELIMINARAN TODAS LAS PREGUNTAS DEPENDIENTES DE ESTA, ¿DESEA CONTINUAR?",
                                            "NO","SI")
                      .then(function() {

                          updatePreguntas();


                      }).catch(function() {
                          console.error('Error');

                      });
              }else{
                      updatePreguntas();
              }
              $mdDialog.hide();
            }
          }
      };

      $scope.eliminarOpcion = function(index){
        if($scope.pregunta.RESPUESTAS[index].NP_RESPUESTA == 0 ){
            $scope.pregunta.RESPUESTAS.splice(index, 1);
        }else{
            $scope.pregunta.RESPUESTAS[index].ESTATUS = false;
            $scope.pregunta.RESPUESTAS[index].MODIFICADA = true;
        }

      };

      var updatePreguntas = function(){
            $scope.pregunta.MODIFICADA = true;
            cuestionarioService.updatePregunta($scope.pregunta);
            cuestionarioService.resetEditandoPregunta();
      };

      $scope.guardarNuevo = function(answer) {
          $mdDialog.hide(answer);
      };

      $scope.preguntaHija = function(){
        $scope.esHija = true;
        $scope.pregunta.OBLIGATORIA = false;
      }

  }



      }]);



})();




