'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:SidenavCtrl
 * @description
 * # SidenavCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('SidenavCtrl',  ['$scope', '$mdSidenav', function($scope, $mdSidenav){
 	$scope.toggleSidenav = function(menuId) {

 		$mdSidenav(menuId).toggle();
 	};
 	
 }]);