
'use strict';
angular.module('expediente')
  	.controller('CajaDetalleserviciosCtrl',['$mdDialog','$state','$scope','$timeout', '$q', '$log','mensajes','carritoCompras','catalogos','pacientes','usuario','buscaPreCitasService','$localStorage',
  		function ($mdDialog,$state,$scope,$timeout, $q, $log,mensajes,carritoCompras,catalogos,pacientes,usuario,buscaPreCitasService,$localStorage) {
  	  var aux;
      // carritoCompras.reset;
      var self                      = this;


      $scope.carrito                = carritoCompras;
      $scope.selected               = [];
      $scope.descuentos             = [];
      $scope.seguro=false;
      self.paciente_id = "";

      self.mUser                    =  usuario.informacion();
      carritoCompras.carrito.cajero = parseInt(self.mUser.NP_EXPEDIENTE);
      self.seguroPopular            = false;

      if($localStorage.paciente !== undefined){
	      carritoCompras.carrito.paciente = $localStorage.paciente.idPaciente;
	      carritoCompras.carrito.nomPaciente = $localStorage.paciente.nombrePaciente;
	  	}else{
	      carritoCompras.carrito.paciente = pacientes.obtenerNpIdPaciente();
	      carritoCompras.carrito.nomPaciente = pacientes.obtenerNombrePaciente();
	  	}

      self.noCache                  = false;
      self.isDisabled               = true;
      self.descuentos               = false;
      self.querySearch              = querySearch;
      self.selectedItemChange       = selectedItemChange;
      self.mCatalogos               = [];
      self.tipos                    = [{desc:'CONVENIO', id:1},{desc:'CONDONACIÓN', id:2}];

        self.paciente_id = carritoCompras.carrito.paciente;
        self.nombrePaciente =  carritoCompras.carrito.nomPaciente;

        self.idEditCobros = (carritoCompras.carrito.tipo_pago != "pre_cita") ? false : true;



      getServicios();


      var cTitle = function(){
      	console.log("==========================");
      	console.log(self.paciente_id);
      	console.log("==========================");
        if(self.paciente_id == undefined){
          $state.go("bienvenido");
        }
        switch(self.paciente_id.substring(0, 1)) {
          case 'D':
              self.mTitle = "COBRO DE SERVICIOS DERECHO HABIENTE";
              break;
          case 'B':
               self.mTitle = "COBRO DE SERVICIOS BENEFICIARIO";
              break;
          default:
              self.mTitle = "COBRO DE SERVICIOS PUBLICO GENERAL";
          }
      }

      cTitle();

      catalogos.getCatalogos(1).success(function(data){
        self.mCatalogos = data;
      });

	    $scope.siguiente = function(){
	    	if($scope.carrito.carrito.servicios.length){
	    		mensajes.confirmacion('CONFIRMACIÓN','¿DESEA PAGAR LOS SERVICIOS?','OK')
	              .then(function() {
	              	$state.transitionTo("realizarPago");
	              }).catch(function() {
	                
	              });
	    		
	    	}else{
	    		mensajes.alerta("Advertencia",'No se han agregado servicios.','Aceptar');
	    	}
	    };

	    function querySearch (query) {
	        var results = query ? self.servicios.filter( createFilterFor(query) ) : self.servicios,
	        	deferred;
		      return results;
	    };

	    function searchTextChange(text) {
	      	switch(text.length){
	    		case 1:
	    		case 2: break;
	    		case 3:

	    		break;
	    	}
	    };

	    function getCuentasCobrar(text) {
	      	switch(text.length){
	    		case 1:
	    		case 2: break;
	    		case 3:

	    		break;
	    	}
	    };

	    function getServicios(){
	    	catalogos.getCatalogos(2)
	      	.success(function(data){
			    if(self.paciente_id.substring(0, 1)=='D' || self.paciente_id.substring(0, 1)=='B'){  // SI ES DERECHO TODOS LOS SERVICIOS MENOS CAUSES
			    	console.log("DERECHO");
			    	$scope.seguro=false;
					self.servicios = _.filter(data , function(e){
	        			return e.categoria != "CAUSES";
	        		});
		      	}else{
		      		console.log("NO ES DERECHO HABIENTE");
		      		//VERIFICAR SI TIENE SEGURO 
		      		//$localStorage.paciente.B_SEGURO_POPULAR
		      		if($localStorage.paciente.CUENTAS_COBRAR){
		      			console.log("TIENE CUENTA PACIENTE");
		      			self.servicios=data;
		      			$scope.seguro=true;
		      		}else{
		      			console.log("NO TIENE SEGURO");
		      			$scope.seguro=false;
		      			self.servicios = _.filter(data , function(e){
	        			return e.categoria != "CAUSES";
	        			});
		      		}
		      	}
		      	
	      		self.isDisabled = false;
	      		self.servicios = cargarServicios(self.servicios);
	      		console.log(self.servicios);
			})
			.error(function(data){
				mensajes.alerta('ERROR','Ha ocurrido un error al intentar cargar los datos','Aceptar');
			});
		};

	    function cargarServicios(data){
	    	return data.map(function (serv){
	      		return {
	      			value : (serv.cClave+" ― "+serv.cDescripcion).toUpperCase(),
	      			display: (serv.cClave+" ― "+serv.cDescripcion).toUpperCase(),
	      			dCosto 	: serv.dCosto,
	            	sEstatusRegistro : serv.sEstatusRegistro,
	            	id : serv.ID,
              		porcentajeDescuento : serv.nPorcentajeCondonacion,
	           		cVariable : serv.nCostoVariable,
	           		categoria: serv.categoria,
	      		};
	      	});
	    };

	    function selectedItemChange(item) {
	    	if(item !== undefined && item.sEstatusRegistro===1){
		    	if($scope.seguro==true){
		    		if(item.categoria!="CAUSES"){
		    			 mensajes.confirmacion('PRECAUCIÓN','EL SERVICIO NO ES CAUSE  YA QUE SE GENERARA UN COSTO','OK')
			              .then(function() {
			              	var desc = item.display.split('―');
					      	$scope.carrito.addServicio({NP_ID:item.id,C_DESCRIPCIPON:desc[1],D_PESO:item.dCosto, cClave:desc[0],
			                                        nPorcentajeCondonacion : item.porcentajeDescuento,cVariable:item.cVariable});
			                 self.searchText = '';
			              }).catch(function() {
			                $scope.selected = [];
			              });
		    		}
		    	}else{
			      	var desc = item.display.split('―');
			      	$scope.carrito.addServicio({NP_ID:item.id,C_DESCRIPCIPON:desc[1],D_PESO:item.dCosto, cClave:desc[0],
	                                        nPorcentajeCondonacion : item.porcentajeDescuento,cVariable:item.cVariable});
	                 self.searchText = '';
                }
		    }
	    };

	    var consulta;
	    function createFilterFor(query) {
	      consulta = angular.uppercase(query);
	      return function filterFn(state) {
	        return (state.value.search(consulta) !== -1);
	      };
	    };

	    $scope.eliminarServicio = function(){

	    	$scope.carrito.delServicio($scope.selected);
	    	$scope.selected = [];
	    };

	    $scope.eliminarDescuento = function(){
	    	for(var i=0 ; i<$scope.descuentos.length ; i++){
	    		$scope.carrito.delDescuento($scope.descuentos[i]);
	    	}
	    	$scope.descuentos = [];
	    };

	    $scope.actualizarCantidad = function(key){
	    	var index = $scope.carrito.getIndiceByKey(key);
	    	$scope.carrito.updateCantidad(index);
	    };

	    $scope.setTipoDesc = function(tipo){
 	    	$scope.carrito.tipoDescuento = tipo;
 	    };


	    $scope.showAdvanced = function(ev) {
	    	$scope.carrito.servSeleccionados = $scope.selected;

		    $mdDialog.show({
			      controller: DialogController,
			      templateUrl: '/application/views/caja/descuento.html',
			      parent: angular.element(document.body),
			      targetEvent: ev,
			      clickOutsideToClose:false
		    })
		    .then(function(answer) {
		    	if(answer==='true'){
			      	//Obtener indexes de objetos seleccionados
			      	var index = $scope.carrito.getIndices2($scope.selected);
			      	var id = generateUUID();
              var fallidos = [];

              fallidos = $scope.carrito.addDescuento($scope.carrito.descuento,index,id);
			      	$scope.carrito.descuento.desc = $scope.selected;

               for(var i = fallidos.length -1 ; i >=0; i-- ){
                $scope.carrito.descuento.desc.splice(fallidos[i].id_selecionado,1);
                  if (i===0){
                    if ($scope.carrito.descuento.TIPO =="2"){
                        mensajes.alerta('ERROR','No se pudo agregar el descuento a uno a más servicios por que el importe del descuento es mayor al importe del servicio o bien ya se ha agregado una condonacion para un servicio','Aceptar');
                     }else{
                     mensajes.alerta('ERROR','No se pudo agregar el descuento a uno a más servicios por que el importe del descuento es mayor al importe del servicio','Aceptar');
                     }

                  }
               }

			      	$scope.carrito.descuento.id = id;

              if( $scope.carrito.descuento.desc.length > 0){
                  $scope.carrito.descuentosAll.push(angular.extend({},$scope.carrito.descuento));
              }

			      	$scope.carrito.descuento.TIPO = '';
			      	$scope.carrito.descuento.NP_ID = '';
			      	$scope.carrito.descuento.FOLIO = '';
			      	$scope.carrito.descuento.descripcion = '';
			      	$scope.carrito.descuento.DESCUENTO = '';
			      	$scope.carrito.descuento.index = [];
			      	$scope.selected = [];
			    }
		    }, function() {
		      //$scope.status = 'You cancelled the dialog.';
		    });
		};

		function generateUUID(){
		    var d = new Date().getTime();
		    var r = (d + Math.random()*10000)%10000 | 0;
		    return r;
 	    };



	    ////////////////////////
	    // Acciones de tabla
	    ////////////////////////
	    $scope.query = {
		    order: 'clave_servicio',
		    limit: 5,
		    page: 1
		};

		$scope.query2 = {
		    order: 'clave_servicio',
		    limit: 5,
		    page: 1
		};

		$scope.columns = [{
		    name: 'CANTIDAD',
		    width : '10%'
		}, {
			descendFirst: true,
		    name: 'CLAVE',
		    orderBy: 'NP_ID',
		    width : '10%'
		}, {
			name: 'DESCRIPCIÓN',
		    orderBy: 'C_DESCRIPCIPON',
		    width : '50%'
		}, {
			name: 'PRECIO UNITARIO',
		    orderBy: 'D_PESO',
		    unit: '$',
		    width : '10%'
		}, {
			name: 'TOTAL',
		    orderBy: 'D_PESO_TOTAL',
		    unit: '$',
		    width : '10%'
		}];

		$scope.colDescuentos = [{
			descendFirst: true,
		    name: 'CLAVE',
		    orderBy: 'NP_ID',
		    width : '10%'
		}, {
			name: 'DESCRIPCIÓN',
		    orderBy: 'C_DESCRIPCIPON',
		    width : '50%'
		}];

		$scope.descAplicados = [{
			name: 'TIPO',
		    // orderBy: 'ctrl.carrito.carrito.servicios.total',
		    unit: '$',
		    width : '10%'
		}, {
		    name: 'CONVENIO',
		    width : '10%'
		}, {
			descendFirst: true,
		    name: 'FOLIO',
		    // orderBy: 'ctrl.carrito.carrito.servicios.clave_servicio',
		    width : '10%'
		}, {
			name: 'DESCUENTO',
		    unit: '$',
		    // orderBy: 'ctrl.carrito.carrito.servicios.descripcion',
		    width : '10%'
		}, {
			name: 'DESCRIPCIÓN',
		    // orderBy: 'ctrl.carrito.carrito.servicios.precio_unitario',
		    //unit: '$',
		    width : '20%'
		},{
			name: 'SERVICIOS',
		    // orderBy: 'ctrl.carrito.carrito.servicios.precio_unitario',
		    width : '30%'
		}];

		$scope.onOrderChange = function(page, limit) {
		    var deferred = $q.defer();

		    $timeout(function () {
		    	deferred.resolve();
		    }, 2000);

		    return deferred.promise;
		};

		$scope.onOrderChange2 = function(page, limit) {
		    var deferred = $q.defer();

		    $timeout(function () {
		    	deferred.resolve();
		    }, 2000);

		    return deferred.promise;
		};


	}]).config( function($mdThemingProvider){
    // Configure a dark theme with primary foreground yellow
    $mdThemingProvider.theme('docs-dark', 'default')
        .primaryPalette('yellow')
        .dark();
  });

function DialogController($scope, $mdDialog, carritoCompras) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
  	if(answer==='true'){
	  	if(carritoCompras.descuento.TIPO===''){
	  		carritoCompras.mensaje = 'Seleccione un tipo de descuento';
	  		return 0;
	  	}else{
	  		if(carritoCompras.descuento.DESCUENTO==='' && carritoCompras.descuento.PORCENTAJE===''){
	  			carritoCompras.mensaje = 'Ingresa un descuento';
	  			return 0;
	  		}else{
	          if(isNaN(carritoCompras.descuento.DESCUENTO && isNaN(carritoCompras.descuento.PORCENTAJE))){
	              carritoCompras.mensaje = 'Ingresa un descuento';
	            return 0;
	          }
	        }
	  		if(carritoCompras.descuento.TIPO===2){
	  			if(carritoCompras.descuento.FOLIO===''){
	  				carritoCompras.mensaje = 'Ingresa una descripción';
	  				return 0;
	  			}
	  		}
	  		if(carritoCompras.descuento.TIPO===1){
	  			if(carritoCompras.descuento.NP_ID===''){
	  				carritoCompras.mensaje = 'Ingresa id de convenio';
	  				return 0;
	  			}
	  			if(carritoCompras.descuento.FOLIO===''){
	  				carritoCompras.mensaje = 'Ingresa un folio';
	  				return 0;
	  			}
	  		}
	  	}
	}
    $mdDialog.hide(answer);
  };
};
