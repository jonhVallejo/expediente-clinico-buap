(function(){
  'use strict';

  /**
   * @ngdoc function
   * @name ECEangular.module('expediente').controller:CajaValecajaCtrl
   * @description
   * # CajaValecajaCtrl
   * Controller of the expediente
   */
  angular.module('expediente')
    .controller('CajaValecajaCtrl',['$scope','$state','cancelarServicio', function ($scope,$state,cancelarServicio) {
      $scope.cs = cancelarServicio;
      $scope.reporte=false;
      $scope.mensaje='';

      $scope.atras = function(){
        $state.transitionTo("cancelarServicios");
      };

      $scope.guardar = function(){
        if($scope.cs.response.NOMBRE === ''){
          $scope.mensaje = "DEBE INTRODUCIR UN NOMBRE";
          return 0;
        }
        if($scope.cs.existIdCatalogo($scope.cs.parentescos,$scope.cs.response.NF_RELACION_PACIENTE)){
          $scope.mensaje = "DEBE INTRODUCIR UN PARENTESCO VÁLIDO";
          return 0;
        }
        if($scope.cs.existIdCatalogo($scope.cs.identificaciones,$scope.cs.response.NF_IDENTIFICACION)){
          $scope.mensaje = "DEBE INTRODUCIR UN TIPO DE IDENTIFICACIÓN VÁLIDA";
          return 0;
        }
        if($scope.cs.response.C_FOLIO_IDENTIFICACION === ''){
          $scope.mensaje = "DEBE INTRODUCIR UN FOLIO DE IDENTIFICACIÓN";
          return 0;
        }
        if($scope.cs.response.C_TELEFONO === ''){
          $scope.mensaje = "DEBE INTRODUCIR UN NÚMERO TELEFÓNICO";
          return 0;
        }


        cancelarServicio.validar().then(function() {
            cancelarServicio.enviar();
          }, function() {
            console.log("Regresar a form de cancelación"); 
          });
      };


      $scope.registrarDatos = function(){
      };
      







    }]);

})();
