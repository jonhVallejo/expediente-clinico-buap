'use strict';

(function(){




angular.module('expediente')
    .controller('reportesDeVentas',['$window','$scope', '$log', '$mdDialog','catalogos','$http','peticiones','mensajes','reportes',
      function ($window,$scope, $log,$mdDialog,catalogos,$http,peticiones,mensajes,reportes) {

      var self = this;
      $scope.folio = '';

      self.tipoDeReporte = [{cDescripcion:'Concentrado de ventas',ID: 1}, {cDescripcion:'Reporte 2',ID: 2}, {cDescripcion:'Reporte 3', ID: 3}];


      var emitirReporte = function(url,data){
      return $http({
            url : url,
            method : 'GET',
            data :data,
            dataType : 'json',
            headers:{
                'Content-Type': 'application/json',
            }
        });
    };


    catalogos.getCatalogos(13).success(function(data){
        // console.log("Debug 1.1");
        // console.log(data);
        self.listTurnos = data;
    });


     catalogos.getCatalogos(6).success(function(data){
        // console.log("Debug 1.1");
        // console.log(data);
        self.listCajas = data;
    });


     self.emiteReporte = function(){
console.log(self.filtrosRecibo);
     }


    $scope.imprimirReporteDeValesCaja = function(){
      peticiones.getMethodPDF('vale_de_Caja/'+$scope.folio)
      .success(function(data){
        var file = new Blob([data], {type: 'application/pdf'});
        var fileURL = URL.createObjectURL(file);
        reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
      })
      .error(function(data){
        mensajes.alerta('ERROR','NO SE HAN ENCONTRADO DATOS CON ESE FOLIO','ACEPTAR');
      });
    };




      }]);

})();
