
(function(){
'use strict';
angular.module('expediente')
    .controller('cuentaPacienteEspecifica',['$localStorage','$scope','$timeout', '$mdSidenav', '$log', '$mdDialog','$http','peticiones','mensajes','cuentaPacienteService','pacientes','$state','carritoCompras','reportes','$q','catalogos',
      function ($localStorage,$scope,$timeout, $mdSidenav, $log,$mdDialog,$http,peticiones,mensajes,cuentaPacienteService, pacientes,$state,carritoCompras,reportes,$q,catalogos) {
        var self = this;
        var url_historico = "cuentaPaciente/historial/";
        self.isDerechoHabiente = false;
        self.isSaldoAFavor = false;
        self.actualizarCauses = false;
        $scope.datosCauses = {};
        self.tieneSeguroPopular = false;
        self.tieneSeguroPrivado = false;
        $scope.habilitarSwitch = true;
        $scope.habilitarSwitchPrivado = false;
        self.eliminarSegPopular = false;

        self.catalogoSeguros = [
              {"nombre_largo":"AXA GOLD", "nombre_corto":"axa", "selected": false},
              {"nombre_largo":"GNP Familiar", "nombre_corto":"gnp", "selected": false},
              {"nombre_largo":"Atlas Básico", "nombre_corto":"atlas", "selected": false},
              {"nombre_largo":"Qualitas TOTAL", "nombre_corto":"qualitas", "selected": false}
        ];
        

        /**
         * [getPaciente Funsión que obtiene los datos de un paciente]
         * @return {[type]} [description]
         */

      console.debug("$LoscalStorage:" , $localStorage);


      var getPaciente = function(){
          pacientes.getPaciente()
          .success(function(data){
            // console.debug("Paciente: ", data);

            if(!data.data.hasOwnProperty('NP_ID')){
              $scope.paciente=data.data[0];
               //tipoPaciente();
               //
               console.log( data.data[0] )

               if( _.isUndefined( $scope.paciente.C_SEGUROS ) ){
                  $scope.paciente.C_SEGUROS = ' ';
               }

               var seguros = $scope.paciente.C_SEGUROS.trim() != '' ? angular.fromJson($scope.paciente.C_SEGUROS) : '';
               if(seguros.length > 0){
                  $localStorage.paciente.seguros = seguros;
                  self.tieneSeguroPrivado = true;
                  $scope.habilitarSwitchPrivado = true;
                  obtenerSeguros(seguros);
               }
            }
          })
          .error(function(data){
            mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
          });
      };
      getPaciente();



catalogos.getCatalogos('39').success(function(data){
            self.tipoCauses= data;
        });

        catalogos.getCatalogos(17).success(function(data){
              self.tipoParentesco = data;
             // console.log(self.tipoParentesco);
        });

    // seguro privado
    $scope.selectedSeguro = function( item ){
        if(self.editSeguros != true){
          item.selected = (item.selected == false) ? true : false;
        }
    }


    $scope.guardarSeguroPrivado = function(){
       var url = "cuentaPaciente/addSeguros/";
       if(obtenerSeguros().length > 0 ){
          var seguros = {
               "NP_CTA_PACIENTE": $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE,
               "C_SEGUROS": obtenerSeguros()
              };
            
           peticiones.putMethod(url, seguros)
            .success(function(data){
                  console.log(" RESPONSE ==============> ", data);
                  //$scope.habilitarSwitchSeguros = true;
                  $localStorage.paciente.seguros = seguros.C_SEGUROS;
                  mensajes.alerta('','DATOS ACTUALIZADOS','Ok');
                   mBuscaCuenta($localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE);
              })
              .error(function(error){
                  console.error(error)
                  mensajes.alerta('ERROR','POR FAVOR CONTACTE AL ADMINISTRADOR','Ok');
              });
       }
       else{
        console.log("No GUARDANDO ...."); 
       }
    }

    function obtenerSeguros( seguros ){

      if(seguros == undefined)
      {
         return _.map(_.filter(self.catalogoSeguros, function( seguro ){ return seguro.selected == true;}), function ( seguro ){
              return seguro.nombre_corto;
         });
      }
      else{
        _.each(seguros, function(item){ 
            _.each(self.catalogoSeguros, function(seguros){
                 if(item == seguros.nombre_corto){
                    seguros.selected = true;
                 } 
            });
        });
      }
    }

    $scope.cancelarSeguroPrivado = function(){
       self.tieneSeguroPrivado = false;
       $scope.habilitarSwitchPrivado = false;
    };


    // end seguro privado
    
    $scope.actualizarSeguro = function(){
      var url = "cuentaPaciente/addCause";
      var datos = {};
      datos.ID_TIPO_CAUSE = self.cause;
      datos.POLIZA_SEGURO = self.polizaSeguro;
      datos.C_OBSERVACIONES = self.C_OBSERVACIONES;
      datos.APOYO_ESTIMADO = self.apoyoEstimado
      datos.PARENTESCO = self.parentesco
      datos.NP_CUENTA = $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
      datos.NP_PERSONAL = $localStorage.usuario.NP_EXPEDIENTE;

      peticiones.putMethod(url,datos)
        .success(function(data){
              self.actualizarCauses = false;
               self.cause = data.CONVENIO_CAUSE.NP_TIPO_CAUSE;
               self.parentesco = data.CONVENIO_CAUSE.NP_RELACION_PACIENTE;
               self.apoyoEstimado = data.CONVENIO_CAUSE.D_MONTO;
               self.C_OBSERVACIONES = data.CONVENIO_CAUSE.C_OBSERVACIONES;
               self.polizaSeguro = data.CONVENIO_CAUSE.C_FOLIO;
               self.tieneSeguroPopular = true;
               $scope.parentescoSeguro = data.CONVENIO_CAUSE.C_RELACION_PACIENTE;
               $scope.tipoConvenio = data.CONVENIO_CAUSE.C_TIPO_CAUSE;
               self.apoyoEstimadoS = (data.CONVENIO_CAUSE.D_MONTO) ?  "$" + data.CONVENIO_CAUSE.D_MONTO : undefined;
              $scope.datosCauses = data.CONVENIO_CAUSE;
              $scope.habilitarSwitch = true;
            mensajes.alerta('','DATOS ACTUALIZADOS','Ok');

          })
          .error(function(error){
              console.error(error)
              mensajes.alerta('ERROR','POR FAVOR CONTACTE AL ADMINISTRADOR','Ok');
          });
    }

    $scope.actualizarSeguroActivar = function(){
      self.actualizarCauses = true;
      $scope.habilitarSwitch = false;
    }

    $scope.actualizarSeguroCancelar = function(){


      if($scope.datosCauses.hasOwnProperty('NP_TIPO_CAUSE')){
        self.actualizarCauses = false;
        self.cause = $scope.datosCauses.NP_TIPO_CAUSE;
        self.parentesco = $scope.datosCauses.NP_RELACION_PACIENTE;
        self.apoyoEstimado = $scope.datosCauses.D_MONTO;
        self.C_OBSERVACIONES = $scope.datosCauses.C_OBSERVACIONES;
        self.polizaSeguro = $scope.datosCauses.C_FOLIO;
        self.tieneSeguroPopular = true;
        $scope.parentescoSeguro = $scope.datosCauses.C_RELACION_PACIENTE;
        $scope.tipoConvenio = $scope.datosCauses.C_TIPO_CAUSE;
        self.apoyoEstimadoS = "$" + $scope.datosCauses.D_MONTO;
      }else{
        self.tieneSeguroPopular = false;
      }
    }


    $scope.activarSeguro = function(){

        if(self.tieneSeguroPopular){
            if($scope.datosCauses.hasOwnProperty('NP_TIPO_CAUSE')){
                self.eliminarSegPopular = true;

                console.debug("entro 333");
             }
        }else{
          console.debug("entro");
          self.eliminarSegPopular = false;
             if($scope.datosCauses.hasOwnProperty('NP_TIPO_CAUSE')){
              self.actualizarCauses = false;
              self.tieneSeguroPopular = true;
               $scope.habilitarSwitch = true;
             }else{
                self.actualizarCauses = true;
             }
        }
    }

    $scope.eliminarSeguro = function(){
      mensajes.confirmacion("", "¿ESTA SEGURO DE ELIMINAR LOS DATOS DEL SEGURO POPULAR?","SI","NO").then(function() {
          console.debug("Se elimino el seguro popular");
          self.tieneSeguroPopular = false;
          $scope.habilitarSwitch = false;
          self.eliminarSegPopular = false;
       }).catch(function(error) {
        console.error("No se elimino el seguro ", error);
      });
    }

    $scope.verificarValido = function(){
      var datoCorrecto = true;
      if(self.cause == undefined){
        datoCorrecto = false;
        console.debug("entro");
      }

      if(self.polizaSeguro == undefined){
        datoCorrecto = false;
        console.debug("entro 1");
      }else{
        if(self.polizaSeguro.trim().length  < 1){
          datoCorrecto = false;
           console.debug("entro 2");
        }
      }

      if(self.C_OBSERVACIONES == undefined){
        datoCorrecto = false;
         console.debug("entro 3");
      }else{
        if(self.C_OBSERVACIONES.trim().length  < 1){
          console.debug(self.C_OBSERVACIONES.trim().length);
          datoCorrecto = false;
           console.debug("entro 4");
        }
      }
      console.debug(self.C_OBSERVACIONES);

      if(self.apoyoEstimado == undefined){
        datoCorrecto = false;
         console.debug("entro 5");
      }else{
        if(self.apoyoEstimado < 1){
          datoCorrecto = false;
           console.debug("entro 6" , self.apoyoEstimado);
        }
      }

      if(self.parentesco == undefined){
        datoCorrecto = false;
         console.debug("entro 7");
      }


      $scope.causeValido = !datoCorrecto;

console.debug(datoCorrecto);
    }


        

    
      // var tipoPaciente = function(){
      //   if($localStorage.paciente.idPaciente == undefined){
      //     $state.go("bienvenido");
      //   }
      //   switch($scope.paciente.C_CLAVE_TIPO_PACIENTE) {
      //     case 'D':
      //         self.isDerechoHabiente = true;
      //         break;
      //     case 'B':
      //          self.isDerechoHabiente = true;
      //         break;
      //     default:
      //         self.isDerechoHabiente = false;
      //     }
      // }



      /**
       * [verificarSate Funsión que se utiliza para verificar de donde viene la aplicación
       * para saber que peticiones se deben realizar]
       * @return {[type]} [description]
       */
      var verificarSate = function(){
        // console.log($state.current.name, $localStorage.stateAnterior);
            if($state.current.name== "detalleCuentaPaciente"){
              
              getCuentasPaciente($localStorage.paciente.idexpediente);
            }else{
              console.debug($localStorage.stateAnterior);
              if($localStorage.stateAnterior == "crearCuentaPaciente"){


                cargarDatosCuenta($localStorage.cuentaAux);
                delete $localStorage.cuentaAux; 
                
               
              }else{
                 mBuscaCuenta($localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE);
              }
              
            }
      }

      /**
       * [getCuentasPaciente  Funsión que s eencarga de traer todo el listado de las cuenta paciente
       * que ha tenido un paciente, activas y las que ya estan cerradas]
       * @param  {[type]} NP_EXPEDIENTE [description]
       * @return {[type]}               [description]
       */
      var getCuentasPaciente = function(NP_EXPEDIENTE){
            peticiones.getDatos(url_historico + NP_EXPEDIENTE)
              .then(function(data) {
                var cuenta;
                  cuenta = _.filter(data.T_CUENTA_PACIENTE, function(e){
                    return e.NF_ESTATUS == 1;
                  });
                  $scope.tCuentaPaciente = data.T_CUENTA_PACIENTE;
                  // if(cuenta.length > 0){
                  //    mBuscaCuenta(cuenta[0].NP_ID);
                  // }
              })
              .catch(function(err) {
                  console.error(err);
              });
        };
        /**
         * [verDetalleCuentaPaciente Funsión que s eencarga de enviar al detalle de una cuenta paciente que fue solicitada]
         * @param  {[type]} ev        [objeto]
         * @param  {[type]} NP_CUENTA [pk de la cuenta a revisar]
         * @return {[type]}           [description]
         */
        self.verDetalleCuentaPaciente = function(ev,NP_CUENTA){
            $localStorage.paciente.cuentaPaciente = {};
            $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE = NP_CUENTA;
            // console.debug($localStorage);
            $state.go("detalleCuentaPacienteEspecifica");
        };

        function getToTalMedicamentos(data){
          var totalMedicamentos=0;
          if(data.hasOwnProperty('TMEDICAMENTOS_RECETA')){
           for (var i = data.TMEDICAMENTOS_RECETA.length - 1; i >= 0; i--) {
              totalMedicamentos += parseFloat(data.TMEDICAMENTOS_RECETA[i].N_COSTO);
            }
          }
          return totalMedicamentos;
        }



        function obtenerCostosSegu( servicio ){

          var precio_auxiliar = carritoCompras.calcularCostosSeg( servicio, 1 );

          

            if( _.isNull( precio_auxiliar ) ){
              console.log( 'Debe ser el precio regular y no tiene seguros' )
              return servicio.D_PESO_TOTAL; // se esta poniendo un valor por defecto
            }else{
              //data['listadoSeg'] = precio_auxiliar;
              
              var mSubTo = 0;

              for (var f = precio_auxiliar.length - 1; f >= 0; f--) {
                mSubTo += precio_auxiliar[f]['aportacion']
              }

              return (servicio.CANTIDAD * servicio.D_PESO) - mSubTo;
              //console.log( precio_auxiliar )
            }

        }


        var calcularTotalCuentaPaciente = function(listServicios,abonos,tipoDePaciente,estatusCuenta){
           abonos = (abonos== undefined) ? 0 : abonos;
           self.isDerechoHabiente = (tipoDePaciente=="D") ? true : false;

            var total = 0.0;
            var restantePagar = 0.0;
            var saldoAFavor = 0.0;
            var totalNeto = 0;
            var prestacionServicios = 0.0;
            var otraFormaPago = 0.0;
            // Estatus 1 = pagado
            

            if(self.isDerechoHabiente){
              for (var i = listServicios.length - 1; i >= 0; i--) {
                  var subTotal =  listServicios[i].D_PESO * listServicios[i].CANTIDAD;
                  total +=  listServicios[i].D_PESO * listServicios[i].CANTIDAD;
                  var descuento = (subTotal * listServicios[i].N_PORCENTAJE_CONDONACION) / 100;
                  prestacionServicios += descuento;
                  // if(estatusCuenta == 1){
                  //   listServicios[i].DESCUENTOS.push({"DESCRIPCION":"D","DESCUENTO": descuento});
                  // }

              }
            }else{
                if(estatusCuenta == 0){
                    for (var i = listServicios.length - 1; i >= 0; i--) {
                      for (var j = listServicios[i].DESCUENTOS.length - 1; j >= 0; j--) {
                        abonos += listServicios[i].DESCUENTOS[j].DESCUENTO;
                      }
                    }
                }
                console.log(listServicios)
                 for (var i = listServicios.length - 1; i >= 0; i--) {
                    //total += listServicios[i].D_PESO * listServicios[i].CANTIDAD;

                  //  console.log( '$localStorage.paciente', $localStorage.paciente )

                    total += obtenerCostosSegu( listServicios[i] )

                    console.log( total )

                  }
            }


      
              // console.debug("listServicios",listServicios);
             


            if(self.isDerechoHabiente){
               if((abonos + prestacionServicios) > total){
                  saldoAFavor = (abonos + prestacionServicios) - total;
                }else{
                  restantePagar =  total - ( abonos + prestacionServicios);
                }
            }else{
               if(abonos > total){
                  saldoAFavor = abonos - total;
                }else{
                  restantePagar = total - abonos;
                }
            }

           
            

          return {"total": total, "abonos": abonos,"saldoAFavor": saldoAFavor,"restantePagar":restantePagar,"prestacionDeServicios":prestacionServicios};
        }



        self.getCuentasCobrar = function(data, total,abonos,restantePagar,saldoAFavor,prestacionServicios){
          
          if(data.hasOwnProperty("B_SEGURO_POPULAR")){
            $localStorage.paciente.cuentaPaciente.TIPO="B_SEGURO_POPULAR";
            $localStorage.paciente.cuentaPaciente.total=total;
            $localStorage.paciente.cuentaPaciente.abonos=abonos;
            $localStorage.paciente.cuentaPaciente.restantePagar=restantePagar;
            $localStorage.paciente.cuentaPaciente.saldoAFavor=saldoAFavor;
            $localStorage.paciente.cuentaPaciente.NF_ESTATUS=data.T_CUENTA_PACIENTE.NF_ESTATUS;
            $localStorage.paciente.cuentaPaciente.prestacionServicios = prestacionServicios;
//            $localStorage.paciente.cuentaPaciente.TIPO_PACIENTE_DERECHO_HABIENTE= (data.T_PACIENTE.C_CLAVE_TIPO_PACIENTE=="D") ? true : false;
            self.isCuentaPacienteActiva = data.T_CUENTA_PACIENTE.NF_ESTATUS;
          }
        };





        /**
         * [mBuscaCuenta Se utiliza para traer toda la información de una cuenta paciente]
         * @param  {[type]} NP_CUENTA [PK de la cuenta paciente a buscar]
         * @return {[type]}           [description]
         */
        
        var cargarDatosCuenta = function(data){
                var aux = calcularTotalCuentaPaciente(data.PRODUCTOS_SERVICIOS, data.T_CUENTA_PACIENTE.D_IMPORTE,data.T_PACIENTE.C_CLAVE_TIPO_PACIENTE,data.T_CUENTA_PACIENTE.NF_ESTATUS);
                  
                  console.log(aux) 


                  aux.total+= getToTalMedicamentos(data);

                  self.getCuentasCobrar(data,aux.total, aux.abonos, aux.restantePagar, aux.saldoAFavor,aux.prestacionDeServicios);
                  self.totalDeCuentaPaciente = "$" + aux.total.toString();
                  self.totalCuenta=aux.total.toString();
                  self.abonosDeCuentaPaciente = "$" + aux.abonos.toString();
                  self.restantePorPagar = "$" + aux.restantePagar.toString();
                  self.tieneSaldoAFavor = "+ $" + aux.saldoAFavor.toString();
                  self.saldoFavor= aux.saldoAFavor.toString();
                  self.prestacionDeServicios = "$" + aux.prestacionDeServicios.toString();

                  self.mCuentaPaciente = data;
                  self.D_FECHA_APERTURA = data.T_CUENTA_PACIENTE.D_FECHA_APERTURA.substring(0,data.T_CUENTA_PACIENTE.D_FECHA_APERTURA.indexOf(" ",0));
                  self.D_HORA_APERTURA = data.T_CUENTA_PACIENTE.D_FECHA_APERTURA.substring(
                                          data.T_CUENTA_PACIENTE.D_FECHA_APERTURA.indexOf(" ",0),data.T_CUENTA_PACIENTE.D_FECHA_APERTURA.length);
                  if(data.T_CUENTA_PACIENTE.NF_ESTATUS==1){
                      self.NF_ESTATUS = "ACTIVA";
                      self.editSeguros = false;
                  }else{
                      if(data.T_CUENTA_PACIENTE.NF_ESTATUS==0){
                        self.NF_ESTATUS = "CERRADA";
                        self.editSeguros = true;
                      }else{
                        if(data.T_CUENTA_PACIENTE.NF_ESTATUS==3){
                          self.NF_ESTATUS = "ABONO PENDIENTE";
                          self.editSeguros = false;
                        }else{
                          if(data.T_CUENTA_PACIENTE.NF_ESTATUS==2){
                          self.NF_ESTATUS = "CANCELADA";
                          self.editSeguros = true;
                        }
                        } 
                      }
                  }

console.debug(data);


        if(data.CONVENIO_CAUSE.hasOwnProperty('NP_TIPO_CAUSE')){
              self.cause = data.CONVENIO_CAUSE.NP_TIPO_CAUSE;
              self.parentesco = data.CONVENIO_CAUSE.NP_RELACION_PACIENTE;
              self.apoyoEstimado = data.CONVENIO_CAUSE.D_MONTO;
              self.C_OBSERVACIONES = data.CONVENIO_CAUSE.C_OBSERVACIONES;
              self.polizaSeguro = data.CONVENIO_CAUSE.C_FOLIO;
              self.tieneSeguroPopular = true;
              $scope.parentescoSeguro = data.CONVENIO_CAUSE.C_RELACION_PACIENTE;
              $scope.tipoConvenio = data.CONVENIO_CAUSE.C_TIPO_CAUSE;
              self.apoyoEstimadoS = (data.CONVENIO_CAUSE.D_MONTO) ?  "$" + data.CONVENIO_CAUSE.D_MONTO : undefined;
              $scope.datosCauses = data.CONVENIO_CAUSE;
              $scope.habilitarSwitch = true;
        }else{
          self.tieneSeguroPopular = false;
          $scope.habilitarSwitch = false;
        }

              


        }
        var mBuscaCuenta = function(NP_CUENTA){
         peticiones.getDatos("cuentaPaciente/" + NP_CUENTA)
          //peticiones.getDatosPruebas("http://www.json-generator.com/api/json/get/bRwhjBXXYi?indent=2")
                .then(function(data) {
                  // console.debug("CuentaPaciente:  ", data);
                  console.log('Se obtienen los servicios de la cuenta paciente')
                  console.log(data)
                  for (var i = data['TMEDICAMENTOS_RECETA'].length - 1; i >= 0; i--) {
                    console.log(data['TMEDICAMENTOS_RECETA'][i])



                    data['TMEDICAMENTOS_RECETA'][i]['C_DESCRIPCION'] = data['TMEDICAMENTOS_RECETA'][i]['C_NOMBRE_MEDICAMENTO']

                    data['TMEDICAMENTOS_RECETA'][i]['B_DERECHO_ABIENTE'] = false;

                    data['TMEDICAMENTOS_RECETA'][i]['CANTIDAD'] = 1;


                    data['TMEDICAMENTOS_RECETA'][i]['D_PESO'] = data['TMEDICAMENTOS_RECETA'][i]['N_COSTO_NORMAL'];


                    data['TMEDICAMENTOS_RECETA'][i]['D_PESO_TOTAL'] = data['TMEDICAMENTOS_RECETA'][i]['N_COSTO_NORMAL'];


                    data['TMEDICAMENTOS_RECETA'][i]['NP_ID'] = 100000 + i;


                    data['TMEDICAMENTOS_RECETA'][i]['NP_PAGO'] = 100000 + i;


                    data['TMEDICAMENTOS_RECETA'][i]['NP_PAGO_SERV'] = 100000 + i;


                    data['TMEDICAMENTOS_RECETA'][i]['N_PORCENTAJE_CONDONACION'] = 100;


                    data['TMEDICAMENTOS_RECETA'][i]['NP_PAGO'] = 100000 + i;


                    data['TMEDICAMENTOS_RECETA'][i]['cClave'] = "100000" + i;

                    data['TMEDICAMENTOS_RECETA'][i]['cVariable'] = 0

                     data['TMEDICAMENTOS_RECETA'][i]['DESCUENTOS'] = []


                    console.log(data['PRODUCTOS_SERVICIOS'])

                    data['PRODUCTOS_SERVICIOS'].push( data['TMEDICAMENTOS_RECETA'][i] )


                   // data['PRODUCTOS_SERVICIOS'].push()

                  }
                 cargarDatosCuenta(data);
            
              }).catch(function(response) {
                console.error(response);
            });
        };
        verificarSate();


        


        self.abonarPagar = function(){
          carritoCompras.carrito.tipo_pago = "abono_cuenta";
         // carritoCompras.carrito.total = $localStorage.paciente.cuentaPaciente.total;
          carritoCompras.carrito.total = 1000;
          $state.go("realizarPagoCuentaPaciente");
        };

        self.detalleServicios = function(){
          console.debug($localStorage.perfil.clave);
           $localStorage.paciente.cuentaPaciente.accion= "buscar_1";
          $localStorage.paciente.cuentaPaciente.servicios = self.mCuentaPaciente.PRODUCTOS_SERVICIOS;
                  // for (var i = $scope.carrito.descuentosAll.length - 1; i >= 0; i--) {
         //    if($scope.carrito.descuentosAll[i].DESC_EXISTENTE){
         //      $scope.carrito.descuentosAll.splice(i,1);
         //    }
         //  }
          // console.debug(carritoCompras.carrito.servicios);
          if(carritoCompras.carrito.servicios){
            for (var i = carritoCompras.carrito.servicios.length - 1; i >= 0; i--) {
                if(carritoCompras.carrito.servicios[i].NUEVO){
                  carritoCompras.carrito.servicios.splice(i,1);
                }
              }
          }
              

              // $localStorage.paciente.contador = 1;
          if($localStorage.perfil.clave=="ADMCAJA"){
              carritoCompras.carrito.tipo_pago = "detalleCuentaAdmin";
              $state.go("ventaDeServiciosCuentaPacienteAdmin");
          }else{
               $state.go("ventaDeServiciosCuentaPaciente");
          }

        }

        self.cancelando=function(ev){
          var defered = $q.defer();
          var promise = defered.promise;
            mensajes.mConfirmacionValida("", "¿ESTÁ SEGURO QUE DESEA CANCELAR LA CUENTA?","CANCELAR","ACEPTAR")
                .then(function() {
                        $mdDialog.show({
                          controller:DialogControllerCancelar,
                          templateUrl: '/application/views/caja/cancelarcuenta.html',
                          parent: angular.element(document.body),
                          targetEvent: ev,
                          clickOutsideToClose:true,
                           locals:{
                             ev:ev,
                         },
                         resolve : {
                                val : function() {
                                    return $scope.val;
                                }
                            }
                        })
                        .then(function(answer) {
                          $scope.status = answer;
                          defered.resolve();
                        }, function() {
                          $scope.status = 'You cancelled the dialog.';
                            defered.resolve();
                        });     
                 }).catch(function(error) {
                  defered.reject();
               })
            return promise;           
        }

        self.imprimirReporteSaldoFavor=function(){
          var url="";
          peticiones.getMethodPDF(url)
             .success(function(data){
                  var file = new Blob([data], {type: 'application/pdf'});
                  var fileURL = URL.createObjectURL(file);
                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
              }).error(function(data){
                  console.log(data);
              });
        }


        self.cancelarCuenta = function(ev){ 
          console.log($localStorage);
          if( self.totalCuenta!=0 || self.mCuentaPaciente.PRODUCTOS_SERVICIOS.length>0){
            mensajes.alerta('ALERTA','NO PUEDE CANCELAR LA CUENTA ','ACEPTAR');
          }else{
                self.cancelando(ev).then(function(){
                  if($scope.status==true){
                    var mensaje="";
                    if(self.saldoFavor!=null ||  self.saldoFavor!=undefined ||  self.saldoFavor!=" ")
                        mensaje=" SU SALDO A FAVOR ES DE  $"+ self.saldoFavor;
                         mensajes.mConfirmacionValida("", "CANCELACIÓN EXITOSA. "+mensaje,"ACEPTAR")
                          .then(function() {
                                  $state.go("bienvenido");
                                  $scope.status==undefined;
                                  //mandar a llamar la impresión de reporte
                                  //self.imprimirReporteSaldoFavor();
                           }).catch(function(error) {
                         });
                  }else{
                      if($scope.status==false)
                        mensajes.alerta("","NO SE CANCELO, INTENTELO NUEVAMENTE","ACEPTAR");
                  }
                });
          }
        };

        function DialogControllerCancelar($scope,ev,$mdDialog,val) {
            $scope.cancelar = function() {
                 $mdDialog.hide();
            };

            $scope.canceladoAutomatico = function() {
                 $mdDialog.hide($scope.val);
            };

            $scope.cancelandoCuenta = function() {
        
              var aux={};
              aux.NP_CTA_PACIENTE=$localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
              aux.C_MOTIVO_CANCELACION=$scope.motivoCancelacion;
              aux.NP_USUARIO_CANCELACION=$localStorage.usuario.NP_EXPEDIENTE;
              var url="cuentaPaciente/cancelar";
                peticiones.putMethod(url, aux)
                   .success(function(data){
                     console.log(data); 
                     $scope.cancelar();
                     $scope.val=true;
                     $scope.canceladoAutomatico();
                  })
                .error(function(data){
                   console.log(data);
                });
               

            };


        };

        self.detalleAbonos = function(ev){
          var url="cuentaPaciente/detalleAbonos/"+$localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
          peticiones.getDatos(url).then(function(data){
           data.SALDO_PENDIENTE="$"+data.SALDO_PENDIENTE;
           data.TOTAL_ABONOS="$"+data.TOTAL_ABONOS;
           data.TOTAL_CUENTA="$"+data.TOTAL_CUENTA;
          $mdDialog.show({
              controller:DialogControllerAbonos,
              templateUrl: '/application/views/caja/detalleAbonos.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
               locals:{
                 ev:ev,
                 data: data
             }
            })
            .then(function(answer) {
              $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.status = 'You cancelled the dialog.';
            });

          }).catch(function(err){
            console.log(err);
          });
        };

        function DialogControllerAbonos($scope,ev,data,$mdDialog,reportes) {
            $scope.datos=data;
            $scope.abonos=data.ABONOS;

            $scope.cancelar = function() {
                 $mdDialog.hide();
            };

            $scope.imprimir = function() {
               // console.log("imprimir");
                var url="cuentaPaciente/reporteAbonos/"+ $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
                peticiones.getMethodPDF(url)
                   .success(function(data){
                        var file = new Blob([data], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);
                        reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
                    }).error(function(data){
                        console.log(data);
                    });
            };

            $scope.onOrderChange = function(page, limit) {
              var deferred = $q.defer();
              $timeout(function () {
                deferred.resolve();
              }, 2000); 
              return deferred.promise;
            };

            $scope.queryAbonos = {
                order: 'fecha',
                limit: 5,
                page: 1
            };
        };

        self.showReporteDetalleCuenta=function(){
          console.log("imprimir reporte");
          var url="cuentaPaciente/reportes/estadoCuenta/"+ $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
              peticiones.getMethodPDF(url)
                 .success(function(data){
                  console.log(data);
                      var file = new Blob([data], {type: 'application/pdf'});
                      var fileURL = URL.createObjectURL(file);
                      reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
                  }).error(function(data){
                      console.log(data);
                  });
        };

        $scope.query = {
            order: 'cDescripcion',
            limit: 15,
            page: 1
        };

        $scope.queryCuenta = {
          filter: '',
          order: '-NP_ID',
          limit: 5,
          page: 1
        };

        $scope.columns = [
        {name: 'FECHA DE INGRESO',
            orderBy: 'dCosto',
            unit: '$',
            width : '10%'
        }, {
            name: 'PISO / UBICACIÓN',
            orderBy: 'cClave',
            width : '10%'
        },
        {descendFirst: true,
          name: 'CAMA / PACIENTE',
            orderBy: 'cDescripcion',
            width : '50%'
        },
         {name: 'SERVICIO OFRECIDO',
            orderBy: 'categoria',
            width : '30%'
        },
        {name: 'USUARIO QUE ATENDIO',
            orderBy: 'dCosto',
            unit: '$',
            width : '10%'
        }];

 }]);
})();
