'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:CajaCompraserviciosCtrl
 * @description
 * # CajaCompraserviciosCtrl
 * Controller of the expediente
 */
var aux;
angular.module('expediente')
  	.controller('CompraserviciosCtrl',['$mdDialog','$state','$scope','$timeout', '$q', '$log','mensajes','carritoCompras','catalogos','pacientes','usuario','buscaPreCitasService','$localStorage','peticiones',
  		function ($mdDialog,$state,$scope,$timeout, $q, $log,mensajes,carritoCompras,catalogos,pacientes,usuario,buscaPreCitasService,$localStorage,peticiones) {



      // carritoCompras.reset;
      var self                      = this;


      $scope.carrito                = carritoCompras;
      $scope.selected               = [];
      $scope.descuentos             = [];
      self.paciente_id = "";

      self.mUser                    =  usuario.informacion();
      carritoCompras.carrito.cajero = parseInt(self.mUser.NP_EXPEDIENTE);

	    if($localStorage.paciente !== undefined){
	      carritoCompras.carrito.paciente = $localStorage.paciente.idPaciente;
	      carritoCompras.carrito.nomPaciente = $localStorage.paciente.nombrePaciente;
	  	}else{
	      carritoCompras.carrito.paciente = pacientes.obtenerNpIdPaciente();
	      carritoCompras.carrito.nomPaciente = pacientes.obtenerNombrePaciente();
	  	}
      self.noCache                  = false;
      self.isDisabled               = true;
      self.descuentos               = false;
      self.querySearch              = querySearch;
      self.selectedItemChange       = selectedItemChange;
      self.mCatalogos               = [];
      self.tipos                    = [{desc:'CONVENIO', id:1},{desc:'CONDONACIÓN', id:2}];
      $scope.esNuevoElemento = false;
      $scope.desabilitarGuardarCambios = false;
      $scope.desabilitarCancelarServicio = false;
      self.paciente_id = carritoCompras.carrito.paciente;
      self.nombrePaciente =  carritoCompras.carrito.nomPaciente;
      $scope.mOrden =  500;

      console.debug("$state:",$state);
      console.debug("$localStorage: ", $localStorage);

var llenarServiciosCarrito = function (){

	for (var i = $scope.carrito.descuentosAll.length - 1; i >= 0; i--) {
		if($scope.carrito.descuentosAll[i].NUEVO ===false){
			console.debug($scope.carrito.descuentosAll[i]);
			$scope.carrito.descuentosAll.splice(i,1);
			}
	}

	// $localStorage.paciente.seguros = [
	// 									 "axa",
	// 									 "atlas",
	// 									 "qualitas",
	// 									 "gnp"
	// 								]

	var orden = 10000;
	for (var i = 0; i < $localStorage.paciente.cuentaPaciente.servicios.length; i++) {
		
		var data = {};	
		data.NP_ID = $localStorage.paciente.cuentaPaciente.servicios[i].NP_ID;
		data.C_DESCRIPCIPON = $localStorage.paciente.cuentaPaciente.servicios[i].C_DESCRIPCION;


		

		data.CANTIDAD = $localStorage.paciente.cuentaPaciente.servicios[i].CANTIDAD; // se esta ponmiendo un valor por defecto, se debve revisar
		data.D_PESO =  $localStorage.paciente.cuentaPaciente.servicios[i].D_PESO;
		//data.D_PESO_TOTAL = $localStorage.paciente.cuentaPaciente.servicios[i].D_PESO_TOTAL; 
		

		var precio_auxiliar = carritoCompras.calcularCostosSeg( $localStorage.paciente.cuentaPaciente.servicios[i], data.CANTIDAD );

		if( _.isNull( precio_auxiliar ) ){
			console.log( 'Debe ser el precio regular y no tiene seguros' )
			data.D_PESO_TOTAL = $localStorage.paciente.cuentaPaciente.servicios[i].D_PESO_TOTAL; // se esta poniendo un valor por defecto
		}else{
			data['listadoSeg'] = precio_auxiliar;
			
			var mSubTo = 0;

			for (var f = precio_auxiliar.length - 1; f >= 0; f--) {
				mSubTo += precio_auxiliar[f]['aportacion']
			}

			data.D_PESO_TOTAL = (data.CANTIDAD * data.D_PESO) - mSubTo;
			console.log( precio_auxiliar )
		}


		data.cClave = $localStorage.paciente.cuentaPaciente.servicios[i].cClave;
		data.cVariable = $localStorage.paciente.cuentaPaciente.servicios[i].cVariable;
		data.PRESTACION_DERECHO_HABIENTE =  ($localStorage.paciente.cuentaPaciente.servicios[i].N_PORCENTAJE_CONDONACION * data.D_PESO)/100;
		data.nPorcentajeCondonacion = $localStorage.paciente.cuentaPaciente.servicios[i].N_PORCENTAJE_CONDONACION;
		data.NP_PAGO_SERV = $localStorage.paciente.cuentaPaciente.servicios[i].NP_PAGO_SERV;
		data.T_FECHA_CMD = $localStorage.paciente.cuentaPaciente.servicios[i].T_FECHA_CMD;
		data.orden = 10000 + i;
		var DESCUENTOS = $localStorage.paciente.cuentaPaciente.servicios[i].DESCUENTOS;
		$scope.carrito.addServicio(data,false);
		DESCUENTOS = (DESCUENTOS) ? DESCUENTOS : [];
		var fallidos = [];
		var indice = [];
		var id = generateUUID();

		for (var k = DESCUENTOS.length - 1; k >= 0; k--) {
			if(DESCUENTOS[k].TIPO ==1 || (DESCUENTOS[k].TIPO ==2 && DESCUENTOS[k].DESCRIPCION !="D")){
				indice.push({ id_selecionado:k, id_servicio: i});
				DESCUENTOS[k].desc = [];
				DESCUENTOS[k].NUEVO = false;
				DESCUENTOS[k].desc.push({"cClave": data.cClave, "C_DESCRIPCIPON": data.C_DESCRIPCIPON,"DESC_EXISTENTE":true});
				fallidos = $scope.carrito.addDescuento(DESCUENTOS[k],indice,id,false);
				$scope.carrito.descuentosAll.push(DESCUENTOS[k]);	
			}
		}
	}


}

// function atualizaCostosSeguros( costos_producto ){

// 	var costo_normal_prod = costos_producto['D_PESO']
// 	var costo_restante = costo_normal_prod;
// 	var suma_seguros = 0;

// 	var listado_seguros = [];


// 	if(_.indexOf( $localStorage.paciente.seguros, "axa" )){
// 		if( costos_producto['N_COSTO_AXA'] >= 0 ){

// 			var m_aportacion = costo_normal_prod - costos_producto['N_COSTO_AXA'];
			
// 			suma_seguros = m_aportacion;

// 			listado_seguros.push( { 'aportacion': m_aportacion, 'seguro': 'axa'} )

// 			if ( costos_producto['N_COSTO_AXA'] == 0 ){
// 				return listado_seguros ;

// 			}else{
				
// 				costo_restante = costos_producto['N_COSTO_AXA']
// 			}  


// 		}else{
// 			console.error('No tiene un costo correcto el producto pero si le agregaron el seguro') 
// 		}
		
// 	}

// 	if(_.indexOf( $localStorage.paciente.seguros, "qualitas" )){

// 		if( costos_producto['N_COSTO_QUALITAS'] >= 0 ){

// 			var m_aportacion = costo_normal_prod - costos_producto['N_COSTO_QUALITAS'];

// 			console.log(  'qualitas 1' , m_aportacion )

// 			if( suma_seguros > 0 ){

// 				suma_seguros += m_aportacion;

// 				if( costo_restante <= m_aportacion){

// 					listado_seguros.push( { 'aportacion': costo_restante , 'seguro': 'qualitas'} )

// 					return listado_seguros ;
// 				}else{

// 					console.log(  'qualitas 1' , m_aportacion )
// 					listado_seguros.push( { 'aportacion': m_aportacion , 'seguro': 'qualitas'} )
					
// 					costo_restante -= m_aportacion;
// 				}

// 			}else{

// 				suma_seguros += m_aportacion;

// 				listado_seguros.push( { 'aportacion': m_aportacion, 'seguro': 'qualitas'} )

// 				if ( costos_producto['N_COSTO_QUALITAS'] == 0 ){
// 						return listado_seguros ;
// 					}else{
// 						costo_restante = costos_producto['N_COSTO_QUALITAS']
// 					}
// 			}

// 		}else{
// 			console.error('No tiene un costo correcto el producto pero si le agregaron el seguro') 
// 		}

// 		//return costos_producto['N_COSTO_QUALITAS'] 
// 	}




// 	if(_.indexOf( $localStorage.paciente.seguros, "gnp" )){

// 		if( costos_producto['N_COSTO_GNP'] >= 0 ){

// 			var m_aportacion = costo_normal_prod - costos_producto['N_COSTO_GNP'];

// 			console.log(  'gnp 2' , m_aportacion )

// 			if( suma_seguros > 0 ){

// 				suma_seguros += m_aportacion;


// 				if( costo_restante <= m_aportacion){

// 					listado_seguros.push( { 'aportacion': costo_restante , 'seguro': 'gnp'} )

// 					return listado_seguros ;
// 				}else{
// 					listado_seguros.push( { 'aportacion': m_aportacion , 'seguro': 'gnp'} )
// 					//suma_seguros += m_aportacion;
// 					costo_restante -= m_aportacion;
// 				}

// 			}else{

// 				suma_seguros += m_aportacion;


// 				listado_seguros.push( { 'aportacion': m_aportacion, 'seguro': 'gnp'} )

// 				if ( costos_producto['N_COSTO_GNP'] == 0 ){
// 						return listado_seguros ;
// 					}else{
// 						costo_restante = costos_producto['N_COSTO_GNP']
// 					}
// 			}

// 		}else{
// 			console.error('No tiene un costo correcto el producto pero si le agregaron el seguro') 
// 		}

// 		//return costos_producto['N_COSTO_QUALITAS'] 
// 	}




// 	// if(_.indexOf( $localStorage.paciente.seguros, "N_COSTO_GNP" )){
// 	// 	return costos_producto['N_COSTO_QUALITAS'] 
// 	// }
// 	// 
	
// 	if(_.indexOf( $localStorage.paciente.seguros, "atlas" )){

// 		if( costos_producto['N_COSTO_ATLAS'] >= 0 ){

// 			var m_aportacion = costo_normal_prod - costos_producto['N_COSTO_ATLAS'];

// 			if( suma_seguros > 0 ){

// 				suma_seguros += m_aportacion;


// 				if( costo_restante <= m_aportacion){

// 					listado_seguros.push( { 'aportacion': costo_restante , 'seguro': 'atlas'} )

// 					return listado_seguros ;
// 				}else{
// 					listado_seguros.push( { 'aportacion': m_aportacion , 'seguro': 'atlas'} )
// 					//suma_seguros += m_aportacion;
// 					costo_restante -= m_aportacion;
// 				}

// 			}else{
// 				suma_seguros += m_aportacion;


// 				listado_seguros.push( { 'aportacion': m_aportacion, 'seguro': 'atlas'} )

// 				if ( costos_producto['N_COSTO_ATLAS'] == 0 ){
// 						return listado_seguros ;
// 					}else{
// 						costo_restante = costos_producto['N_COSTO_ATLAS']
// 					}
// 			}

// 		}else{
// 			console.error('No tiene un costo correcto el producto pero si le agregaron el seguro') 
// 		}

// 		//return costos_producto['N_COSTO_QUALITAS'] 
// 	}

// 	// if(_.indexOf( $localStorage.paciente.seguros, "N_COSTO_ATLAS" )){
// 	// 	return costos_producto['N_COSTO_QUALITAS'] 
// 	// }





// 	return _.size( listado_seguros ) > 0 ? listado_seguros   : null;



// }

var verificarProcedencia =function (){

	console.log(carritoCompras.carrito)

	for (var i = carritoCompras.carrito.servicios.length - 1; i >= 0; i--) {
		if(carritoCompras.carrito.servicios[i].NUEVO==false){
			carritoCompras.carrito.servicios.splice(i,1);
		}
	}
	if($state.current.name == "ventaDeServiciosCuentaPaciente"){
		self.esUserAdmin = false;
		llenarServiciosCarrito();	
	}else{
		if($state.current.name == "ventaDeServiciosCuentaPacienteAdmin"){
			self.esUserAdmin = true;
			self.idEditCobros = true;
			llenarServiciosCarrito();	
		}else{
			self.esUserAdmin = false;
			self.idEditCobros = (carritoCompras.carrito.tipo_pago != "pre_cita") ? false : true;
		}
	}
}
      
    verificarProcedencia(); 
	getServicios();
    self.mTitle = ($localStorage.paciente.DERECHO_HABIENTE) ? "COBRO DE SERVICIOS DERECHO HABIENTE" :"COBRO DE SERVICIOS PUBLICO GENERAL";

      catalogos.getCatalogos(40).success(function(data){
        self.mCatalogos = data;
      });


	    $scope.siguiente = function(){
	    	if($scope.carrito.carrito.servicios.length){
	    		if($state.current.name == "ventaDeServicios"){
	    			carritoCompras.carrito.tipo_pago = "cobro_serv"; 
	    		}
	    		$state.transitionTo("realizarPago");
	    	}else{
	    		mensajes.alerta("Advertencia",'No se han agregado servicios.','Aceptar');
	    	}
	    };

	    function querySearch (query) {
	        var results = query ? self.servicios.filter( createFilterFor(query) ) : self.servicios,
	        	deferred;
		      return results;
	    };

	    function searchTextChange(text) {
	      	switch(text.length){
	    		case 1:
	    		case 2: break;
	    		case 3:
	    		break;
	    	}
	    };

	    function getServicios(){
	    	catalogos.getCatalogos(2)
	      	.success(function(data){
	      		self.isDisabled = false;
	      		self.servicios = cargarServicios(data);

			})
			.error(function(data){
				mensajes.alerta('ERROR','Ha ocurrido un error al intentar cargar los datos','Aceptar');
			});
		};

	function cargarServicios(data){
	    	return data.map(function (serv){
	      		return {
	      			value : (serv.cClave+" ― "+serv.cDescripcion).toUpperCase(),
	      			display: (serv.cClave+" ― "+serv.cDescripcion).toUpperCase(),
	      			dCosto 	: serv.dCosto,
	            sEstatusRegistro : serv.sEstatusRegistro,
	            id : serv.ID,
              porcentajeDescuento : serv.nPorcentajeCondonacion,
	           cVariable : serv.nCostoVariable,
	      		};
	      	});
	    };

	function selectedItemChange(item) {
	    var bandera = false;
	    	if(item !== undefined && item.sEstatusRegistro===1){
		      	var desc = item.display.split('―');

		      	for (var i = carritoCompras.carrito.servicios.length - 1; i >= 0; i--) {
		      		
		      		if(carritoCompras.carrito.servicios[i].NP_ID == item.id && carritoCompras.carrito.servicios[i].NUEVO == true){
		      			bandera = true;
		      		}
		      	}
		      if(!bandera){
		      	$scope.carrito.addServicio({NP_ID:item.id,C_DESCRIPCIPON:desc[1],D_PESO:item.dCosto, cClave:desc[0],D_PESO_TOTAL:item.dCosto,
                                        nPorcentajeCondonacion : item.porcentajeDescuento,cVariable:item.cVariable, "DESC_EXISTENTE":false,"orden": $scope.mOrden},true);
		      	$scope.mOrden -= 1;
		      }else{
		      	mensajes.alerta("ALERTA","EL SERVICIO '" + desc[1] + "' YA FUE SOLICITADO, SI DESEA PUEDE CAMBIAR LA CANTIDAD DEL SERVICIO PERO NO PUEDE SOLICITARLO NUEVAMENTE","Ok");
		      }
		      	
            self.searchText = '';
		    }
	    };

	    function createFilterFor(query) {
	      var consulta = angular.uppercase(query);
	      return function filterFn(state) {
	        return (state.value.search(consulta) !== -1);
	      };
	    };

	    $scope.eliminarServicio = function(){
	    	$scope.carrito.delServicio($scope.selected);
	    	$scope.selected = [];
	    };

	    $scope.toggle = function (item, list) {
	    	list.splice(0,list.length);
	    	for (var i = carritoCompras.carrito.servicios.length - 1; i >= 0; i--) {
	    		carritoCompras.carrito.servicios[i].it = false;
	    	}
	        list.push(item);
	        $scope.esNuevoElemento = (item.NUEVO === true) ? true : false;
	      };

	    $scope.eliminarDescuento = function(){
	    	
	    	for(var i=0 ; i<$scope.descuentos.length ; i++){
	    		$scope.carrito.delDescuento($scope.descuentos[i]);
	    	}
	    	$scope.descuentos = [];
	    };

	    $scope.actualizarCantidad = function(key){
	    	var index = $scope.carrito.getIndiceByKey(key);
	    	$scope.carrito.updateCantidad(index);
	    };

	    $scope.setTipoDesc = function(tipo){
 	    	$scope.carrito.tipoDescuento = tipo;
 	    };


		self.validDescuento = true;
		self.verificarDatosDes = function(folio){
			var aux = undefined;
			if($scope.carrito.descuento.TIPO == 1){
				aux = $scope.carrito.descuento.FOLIO;
			}else{
				aux = $scope.carrito.descuento.DESCRIPCION;	
			}
			if (aux == undefined) {
				self.validDescuento = true;
			}else{
				if(aux.length >1){
					if($scope.carrito.descuento.DESCUENTO>0){
					self.validDescuento = false;
				}else{
					self.validDescuento = true;
					}	
				}
			}	
		}

		self.cambioTipoDescuento = function(dat){
			$scope.carrito.descuento.FOLIO = "";
			$scope.carrito.descuento.DESCRIPCION = "";
			$scope.carrito.descuento.DESCUENTO = 0;
			self.validDescuento = true;
		}


	    $scope.showAdvanced = function(ev) {
	    	$scope.carrito.servSeleccionados = $scope.selected;
		    $mdDialog.show({
			      controller: DialogController,
			      templateUrl: '/application/views/caja/descuento.html',
			      parent: angular.element(document.body),
			      targetEvent: ev,
			      clickOutsideToClose:false
		    })
		    .then(function(answer) {
		    	if(answer==='true'){
			      	var index = $scope.carrito.getIndices2($scope.selected);
			      	var id = generateUUID();
              		var fallidos = [];
              		$scope.carrito.descuento.DESC_EXISTENTE = false;
              		fallidos = $scope.carrito.addDescuento($scope.carrito.descuento,index,id,true);
              		
			      	$scope.carrito.descuento.desc = $scope.selected;

               for(var i = fallidos.length -1 ; i >=0; i-- ){
                	$scope.carrito.descuento.desc.splice(fallidos[i].id_selecionado,1);
                  	if (i===0){
                  		if(fallidos.length > 1){
                  			if ($scope.carrito.descuento.TIPO =="2"){
	                        	mensajes.alerta('ERROR','NO SE PUDO AGREGAR EL DESCUENTO A UNO A MÁS SERVICIOS POR QUE EL DESCUENTO ES MAYOR AL IMPORTE DEL SERVICIO O YA SE HA AGREGADO UNA CONDONACIÓN PARA UN SERVICIO.','ACEPTAR');
	                     	}else{
	                     		mensajes.alerta('ERROR','NO SE PUDO AGREGAR EL DESCUENTO A UNO O MÁS SERVICIOS POR QUE EL IMPORTE DEL DESCUENTO ES MAYOR AL IMPORTE DEL SERVICIO.','ACEPTAR');
	                     	}
                  		}else{
                  			if(fallidos[0].tipoFallo==1){
                  				mensajes.alerta('ERROR','EL DESCUENTO SOLICITADO NO DEBE SER MAYOR AL IMPORTE DEL SERVICIO.','ACEPTAR');
                  			}else{
                  				mensajes.alerta('ERROR','NO PUEDE AGREGAR MÁS DE UN DESCUENTO/CONDONACIÓN A UN MISMO SERVICIO.','ACEPTAR');
                  			}
                  		}
                    	
                  }
               }
			  $scope.carrito.descuento.id = id;
              if( $scope.carrito.descuento.desc.length > 0){
                  $scope.carrito.descuentosAll.push(angular.extend({},$scope.carrito.descuento));
                 // console.debug($scope.carrito.descuentosAll);

              }
			      	$scope.carrito.descuento.TIPO = '';
			      	$scope.carrito.descuento.NP_ID = '';
			      	$scope.carrito.descuento.FOLIO = '';
			      	$scope.carrito.descuento.descripcion = '';
			      	$scope.carrito.descuento.DESCUENTO = '';
			      	$scope.carrito.descuento.index = [];
			      	$scope.selected = [];
			    }
			 
		    }, function(error) {
				console.error(error);
		    });
		};

		function generateUUID(){
		    var d = new Date().getTime();
		    var r = (d + Math.random()*10000)%10000 | 0;
		    return r;
 	    };

/**
 * Sección para guardar los cambios de servicios en una cuenta paciente
 */

self.guardarCambios = function(){
	$scope.desabilitarGuardarCambios = true;
 	mensajes.confirmacion("", "¿ESTA SEGURO DE AGREGAR EL SERVICIO/PRODUCTO A ESTÁ CUENTA PACIENTE?   UNA VEZ AGREGADO YA NO SERÁ POSIBLE ELIMINARLO","SI","NO").then(function() {
				var url = "cuentaPaciente/addServ/" + $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
				carritoCompras.carrito.NP_CUENTA_PACIENTE = $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
				carritoCompras.carrito.tipo_pago = "aumento_servicios";
				var aux = angular.fromJson(angular.toJson(carritoCompras.carrito));

				for (var i = aux.servicios.length - 1; i >= 0; i--) {
					if(aux.servicios[i].NUEVO==false){
						aux.servicios.splice(i,1);
					}else{
						for (var j = aux.servicios[i].DESCUENTOS.length - 1; j >= 0; j--) {
							if(aux.servicios[i].DESCUENTOS[j].DESC_EXISTENTE){
								aux.servicios[i].DESCUENTOS.splice(j,1);
							}
						}
					}
				}

			peticiones.postDatos(url,aux)
                .then(function(data) {
                	solicitarCuentaPaciente($localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE);
                 mensajes.mConfirmacionValida("", "SERVICIO/PRODUCTO AGREGADO A LA CUENTA PACIENTE DE FORMA EXITOSA","Ok")
                      .then(function() {
                          $scope.desabilitarGuardarCambios = false;
                      }).catch(function(error) {
                      	   $scope.desabilitarGuardarCambios = false;
                           console.error(error);
                      });
		    }).catch(function(response) {
		                 mensajes.alerta('ERROR','OCURRIO UN ERROR AL REALIZAR LA TRANSACCIÓN POR FAVOR CONTACTE AL ADMINISTRADOR','Ok');
		                  $scope.desabilitarGuardarCambios = false;
		    });              
    }).catch(function(error) {
        $scope.desabilitarGuardarCambios = false;
    });
}

	self.cancelarServicioCuentaPaciente = function(){
		var cancelarServicios = {};
		$scope.desabilitarCancelarServicio = true;
		cancelarServicios.NP_CUENTA_PACIENTE = $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
		cancelarServicios.servicios = []
 		mensajes.confirmacion("", "¿ESTA SEGURO DE CANCELAR EL SERVICIO/PRODUCTO A ESTÁ CUENTA PACIENTE?   UNA VEZ CANCELADO YA NO APARECERA MÁS EN EL DETALLE DE SERVICIOS","SI","NO")
            .then(function() {
				for (var i = $scope.selected.length - 1; i >= 0; i--) {
					cancelarServicios.servicios.push($scope.selected[i].NP_PAGO_SERV);
				}
		 
			var url= "cuentaPaciente/deleteServ/";
			peticiones.deleteDatosParametro(url, cancelarServicios)
				.success(function (data, status) {
					mensajes.mConfirmacionValida("", "SERVICIO CANCELADO DE FORMA EXITOSA","Ok")
	                      .then(function() {
	                         
			carritoCompras.carrito.servicios.splice(0,carritoCompras.carrito.servicios.length);
	            $scope.carrito.descuentosAll.splice(0,$scope.carrito.descuentosAll.length);

	            $localStorage.paciente.cuentaPaciente.servicios = data.PRODUCTOS_SERVICIOS;
	            llenarServiciosCarrito()
	            $scope.desabilitarCancelarServicio = false;
	        }).catch(function() {
	            $scope.desabilitarCancelarServicio = false;
	        });      
		}).error(function(err) {
			mensajes.alerta('ERROR','OCURRIO UN ERROR AL REALIZAR LA TRANSACCIÓN POR FAVOR CONTACTE AL ADMINISTRADOR','Ok');
	        $scope.desabilitarCancelarServicio = false;
	        console.log(err);
		});
  }).catch(function(error) {
        $scope.desabilitarCancelarServicio = false;
     });      
}


var solicitarCuentaPaciente = function(NP_CUENTA){

//console.log('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj')

	peticiones.getDatos("cuentaPaciente/" + NP_CUENTA)
        .then(function(data) {
            $scope.carrito.descuentosAll.splice(0,$scope.carrito.descuentosAll.length);
            carritoCompras.carrito.servicios.splice(0,carritoCompras.carrito.servicios.length);
            
	      	$localStorage.paciente.cuentaPaciente.servicios = _.sortBy(data.PRODUCTOS_SERVICIOS, function( servicio ){ return -servicio.T_FECHA_MILIS});
	      	//$localStorage.paciente.cuentaPaciente.servicios = data.PRODUCTOS_SERVICIOS;
            llenarServiciosCarrito()
        }).catch(function(response) {
     			console.error(response);
    	});
}

	    ////////////////////////
	    // Acciones de tabla
	    ////////////////////////
	    $scope.query = {
		    order: 'ORDEN',
		    limit: 5,
		    page: 1
		};

		$scope.query2 = {
		    order: 'clave_servicio',
		    limit: 15,
		    page: 1
		};

		$scope.columns = [
			{
		    name: 'CANTIDAD',
		    width : '10%'
		}, {
			descendFirst: true,
		    name: 'CLAVE',
		    orderBy: 'NP_ID',
		    width : '10%'
		}, {
			name: 'DESCRIPCIÓN',
		    orderBy: 'C_DESCRIPCIPON',
		    width : '50%'
		}, {
			name: 'PRECIO UNITARIO',
		    orderBy: 'D_PESO',
		    unit: '$',
		    width : '10%'
		}, {
			name: 'PRESTACIÓN DERECHO HABIENTE',
		    orderBy: 'D_PESO',
		    unit: '$',
		    width : '10%'
		}, {
			name: 'TOTAL',
		    orderBy: 'D_PESO_TOTAL',
		    unit: '$',
		    width : '10%'
		}];
		if($localStorage.paciente.DERECHO_HABIENTE == false){
			$scope.columns.splice(4,1);
		}



		$scope.columnsDerechoHabiente = [{
		    name: '',
		    width : '5%'
		},{
		    name: 'CANTIDAD',
		    width : '5%'
		}, {
			descendFirst: true,
		    name: 'CLAVE',
		    orderBy: 'NP_ID',
		    width : '10%'
		}, {
			name: 'DESCRIPCIÓN',
		    orderBy: 'C_DESCRIPCIPON',
		    width : '50%'
		},
		{
			name: 'FECHA',
		    orderBy: 'T_FECHA_CMD',
		    unit: '',
		    width : '10%'
		}, {
			name: 'PRECIO UNITARIO',
		    orderBy: 'D_PESO',
		    unit: '$',
		    width : '10%'
		},{
			name: 'PRESTACIÓN DERECHO HABIENTE',
		    orderBy: 'D_PESO',
		    unit: '$',
		    width : '10%'
		},
		{
			name: 'SEGUROS',
		    orderBy: 'SEGUROS',
		    unit: '$',
		    width : '10%'
		}, {
			name: 'TOTAL',
		    orderBy: 'D_PESO_TOTAL',
		    unit: '$',
		    width : '10%'
		}];

		if($localStorage.paciente.DERECHO_HABIENTE == false){
			$scope.columnsDerechoHabiente.splice(6,1);
		}
		
		$scope.esDerechoHabiente = $localStorage.paciente.DERECHO_HABIENTE;

		$scope.colDescuentos = [{
			descendFirst: true,
		    name: 'CLAVE',
		    orderBy: 'NP_ID',
		    width : '10%'
		}, {
			name: 'DESCRIPCIÓN',
		    orderBy: 'C_DESCRIPCIPON',
		    width : '50%'
		}];

		$scope.descAplicados = [{
			name: 'TIPO',
		    // orderBy: 'ctrl.carrito.carrito.servicios.total',
		    unit: '$',
		    width : '10%'
		}, {
		    name: 'CONVENIO',
		    width : '10%'
		}, {
			descendFirst: true,
		    name: 'FOLIO',
		    // orderBy: 'ctrl.carrito.carrito.servicios.clave_servicio',
		    width : '10%'
		}, {
			name: 'DESCUENTO',
		    unit: '$',
		    // orderBy: 'ctrl.carrito.carrito.servicios.descripcion',
		    width : '10%'
		}, {
			name: 'DESCRIPCIÓN',
		    // orderBy: 'ctrl.carrito.carrito.servicios.precio_unitario',
		    //unit: '$',
		    width : '20%'
		},{
			name: 'SERVICIOS',
		    // orderBy: 'ctrl.carrito.carrito.servicios.precio_unitario',
		    width : '30%'
		}];

		$scope.onOrderChange = function(page, limit) {
		    var deferred = $q.defer();

		    $timeout(function () {
		    	deferred.resolve();
		    }, 2000);

		    return deferred.promise;
		};

		$scope.onOrderChange2 = function(page, limit) {
		    var deferred = $q.defer();

		    $timeout(function () {
		    	deferred.resolve();
		    }, 2000);

		    return deferred.promise;
		};

	}]).config( function($mdThemingProvider){
    // Configure a dark theme with primary foreground yellow
    $mdThemingProvider.theme('docs-dark', 'default')
        .primaryPalette('yellow')
        .dark();
  });