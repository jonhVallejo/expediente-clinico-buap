'use strict';
(function(){

/**
 * Controlador encargado de realizar las operaciones para el carrito de compra
 * este controlador se comunica con la Factoria [carritoCompras] que es donde se
 * tiene toda la información del carrito.
 */
angular.module('expediente')
.controller('realizarPago',['$scope','$timeout', '$q', '$log','carritoCompras','catalogos','$http','$state','$window','peticiones','$localStorage','reportes','mensajes','$mdDialog',
  function ($scope,$timeout, $q, $log,carritoCompras,catalogos,$http,$state,$window,peticiones,$localStorage, reportes,mensajes,$mdDialog) {

      var self = this;
      self.c_operador = ['VISA', 'MASTERCARD'];
      self.banco = [];
      self.formasPago = [];
      $scope.operacionEsCuentaPaciente = false;

      $scope.tituloTotal = "TOTAL:";
      $scope.tituloAbonar = "CANTIDAD A ABONAR:";
      self.idFolioRecibo = 0; //Folico que se utiliza cuando se realiza un pago.
      self.puedeAbonarPagar = false;
      self.debePagar= true;
      carritoCompras.carrito.formasDePago=[{TIPO_PAGO:"1"}];
      self.catTipoConvenios=[{cDescripcion: 'CONVENIO', ID:0},{cDescripcion: 'SEGURO POPULAR',ID:1}];
    /**
     * Function Realiza petición al servidor para obtener todo el catalogo de parentescos.
     * @param  {[type Int Id del catalogo a solicitar ]}
     */
    catalogos.getCatalogos(17).success(function(data){
        self.catParentesco = data;
    });




    /**
     *  Function, Realiza una petición para obtener el catalogo de formas de pago y se valida
     *  si viene de cuenta paciente mostrar tipo de pago Crédito en caso contrario no mostrarlo
     * @param  {[type id del catalgo a solicitar]}
     */
    function getFormasPago(){
       
       peticiones.getDatos("catalogo/" + 3)
          .then(function(data)
          {
          
            self.catalogoFormasPago =  data;
            console.debug(self.catalogoFormasPago);
            self.listaDePagos();
          }).catch(function(data)
          {
          
            console.error(data);
          });
    }
    getFormasPago();


    /**
     * Function Realiza petición al servidor para tipo de convenios
     * @param  {[type Int Id del catalogo a solicitar ]}
     */
    catalogos.getCatalogos('convenios').success(function(data){
        self.catConvenios = data;
    });

     /**
     * Function Realiza petición al servidor para tipo de convenios seguro popular
     * @param  {[type Int Id del catalogo a solicitar ]}
     */
    catalogos.getCatalogos(39).success(function(data){
        self.catCauses = data;
    });

    /**
     * Function Realiza petición al servidor para obtener todo el catalogo de bancos.
     * @param  {[type Int Id del catalogo a solicitar ]}
     */
    catalogos.getCatalogos(5).success(function(data){
        self.banco = data;
    });







    self.listaDePagos = function(){
      console.debug("tootototot");
        if(self.pagoCuentaPaciente == false){
          self.formasPago=_.filter(self.catalogoFormasPago,function(e){
            return e.cDescripcion != "CRÉDITO";
          });
        }else{
            self.formasPago=_.filter(self.catalogoFormasPago,function(e){
              return (self.tipoOperacionPago == 0) ? ((e.cDescripcion != "CRÉDITO")) : self.catalogoFormasPago;
            });
        }
        console.debug("self.catalogoFormasPago", self.catalogoFormasPago);
    }




    /**
     * [verificarProcedencia Funsión que se utiliza para verificar la procedencia y confirmar el tipo de transacción que se va a realizar]
     * @return {[type]} [description]
     */
    function verificarProcedencia (){
      // if($localStorage.paciente.cuentaPaciente == undefined){
      //         console.error("sE ELIMINO LA CUNETA PACIENTE", "$state.go('bienvenido')");
      // }
      console.debug("Carrito de compras: ", carritoCompras.carrito);
      if($localStorage.stateAnterior == "detalleCuentaPacienteEspecifica"){ // viene del detalle de su cuneta paciente, va a pagar o a abonar
          if(carritoCompras.carrito.tipo_pago  == "abono_cuenta"){
            self.pagoCuentaPaciente=true;
            self.saldoAFavor = false;
            return true;
          }else{
              delete $localStorage.paciente.cuentaPaciente;
              console.error("Ocurrio un error inesperado se va a ejecutar: 1 ", "$state.go('bienvenido')");
          }
      }else{
          if($localStorage.stateAnterior == "ventaDeServicios" ){ // viene del carrito cunado compra los servicios
            if(carritoCompras.carrito.tipo_pago  == "cobro_serv"){
                self.mImporte =0;
                return false;
              }else{
                  delete $localStorage.paciente.cuentaPaciente;
                  console.error("Ocurrio un error inesperado se va a ejecutar: 2 ", "$state.go('bienvenido')");
              }
          }
          else{

            if($localStorage.stateAnterior == "crearCuentaPaciente" ){ 
               console.log($localStorage);
               carritoCompras.carrito.tipo_pago  = "abono_cuenta";
               self.pagoCuentaPaciente=true;
               if(!$localStorage.paciente.cuentaPaciente.autorizacion){
                  self.cantidadAbonar =5000;
                  self.cuantoAbona=5000;
                  self.saldoAFavor = false;

               }
               return true;
            }else{
                delete $localStorage.paciente.cuentaPaciente;
                console.error("Ocurrio un error inesperado se va a ejecutar: 3 ", "$state.go('bienvenido')");
            }
        
          }
      }
     
    }
    $scope.operacionEsCuentaPaciente = verificarProcedencia();
    
    


   self.mRealizarPago = carritoCompras.carrito.formasDePago;
   function realizarAccion(){
    
      if($scope.operacionEsCuentaPaciente){//La operacion es una CUENTA PACIENTE, se hace el llenado de los datos necesarios.
        //$localStorage.paciente.cuentaPaciente.total = 550;
        self.pagoTotalServicios = $localStorage.paciente.cuentaPaciente.total;
        
        if($localStorage.stateAnterior == "crearCuentaPaciente" ){ // viene del carrito cunado compra los servicios
               if(!$localStorage.paciente.cuentaPaciente.autorizacion)
                  self.cantidadAbonar =5000;
        }else{
              self.cantidadAbonar     = 0;
        }
        self.mImporte = 0;
        self.puedeAbonarPagar = true;
      
      }else{ //La operación es un cobro de servicio para una persona que no tiene una CUENTA PACIENTE
       self.pagoTotalServicios = carritoCompras.carrito.total;
       self.puedeAbonarPagar = (self.pagoTotalServicios == 0) ? false : true;

      }

      self.listaDePagos();
   }

   realizarAccion();


/**
 * Área ara las funciones genericas del proceso
 */

 self.abonar = function(){
  console.debug(self.cuantoAbona);
  
     self.cantidadAbonar = (self.cuantoAbona) ? self.cuantoAbona : 0;

     
  };

  /**
   * [importeCliente Se utiliza para actualizar (etiqueta) el importe que esta pagando el cliente]
   * @return {[type]} [description]
   */
  self.importeCliente = function(){
      self.mImporte =0;
      for (var elemento in carritoCompras.carrito.formasDePago){
          if (typeof carritoCompras.carrito.formasDePago[elemento].D_IMPORTE != 'undefined'){
              if(carritoCompras.carrito.formasDePago[elemento].D_IMPORTE != null){
                //carritoCompras.carrito.formasDePago[elemento].D_IMPORTE = importe;
                self.mImporte += parseFloat(carritoCompras.carrito.formasDePago[elemento].D_IMPORTE);
                self.restaDerechoHabiente = (carritoCompras.carrito.total - (self.totalDeAbonos + self.prestacionServicios)) - parseFloat(carritoCompras.carrito.formasDePago[elemento].D_IMPORTE);
                console.debug("Entro a cambiar la forma de pago: " , carritoCompras.carrito.formasDePago);
            }
          }
        }
    };

  /**
   * [actualizarTipoPago Se utiliza para actualizar el tipo de pago ********** creo que ya no se usa ]
   * @param  {[type]} key [description]
   * @return {[type]}     [description]
   */
  self.actualizarTipoPago = function(key){
      var index = carritoCompras.getIndiceByKeyCobro(key);
  }

  self.actualizarTipoOperacion = function(){

     self.listaDePagos();


    //$localStorage.paciente.cuentaPaciente.abonos = 550; //Se hace para emular saldo a favor
    //$localStorage.paciente.cuentaPaciente.total =550;
    carritoCompras.carrito.total = $localStorage.paciente.cuentaPaciente.total;
    self.totalDeAbonos = $localStorage.paciente.cuentaPaciente.abonos;
    self.prestacionServicios = $localStorage.paciente.cuentaPaciente.prestacionServicios; 
    self.soyDerechoHabiente = $localStorage.paciente.DERECHO_HABIENTE;
    self.restaDerechoHabiente = 0;
    self.mImporte = 0;

      carritoCompras.carrito.formasDePago.splice(0,carritoCompras.carrito.formasDePago.length);
      carritoCompras.carrito.formasDePago.push({});
//self.cuantoAbona = 0;
    
    self.cuantoAbona = 0;
console.debug($localStorage);

if(self.soyDerechoHabiente){
  console.debug("++++++++++");
  if((self.totalDeAbonos + self.prestacionServicios) > carritoCompras.carrito.total){
      self.saldoAFavor = true;
      self.debePagar = (self.tipoOperacionPago == 0) ? true : false;
      self.dineroAFavor = (self.totalDeAbonos + self.prestacionServicios) - carritoCompras.carrito.total;
  }else{
     if((self.totalDeAbonos + self.prestacionServicios) == carritoCompras.carrito.total){
          self.debePagar = (self.tipoOperacionPago == 0) ? true : false;
          self.saldoAFavor = false;
     }else{
        self.debePagar = true;
       self.saldoAFavor = false;
       self.restaDerechoHabiente = carritoCompras.carrito.total - (self.totalDeAbonos + self.prestacionServicios);
     }
  }
}else{

  console.debug("esto no se ");

    if(self.totalDeAbonos < carritoCompras.carrito.total){
       self.debePagar = true;
       self.saldoAFavor = false;
    }else{
      if(self.totalDeAbonos == carritoCompras.carrito.total){
                      //aca se manda a cerrar la cuenta del paciente
          self.debePagar = (self.tipoOperacionPago == 0) ? true : false;
          self.saldoAFavor = false;
          console.debug("no debe pagar nada");
        }else{
          //aca se va a hacer el cierre de la cuenta paciente y ademas se debe indicar que se debe hacer una devolución
            self.saldoAFavor = true;
            self.debePagar = (self.tipoOperacionPago == 0) ? true : false;
            self.dineroAFavor = self.totalDeAbonos - carritoCompras.carrito.total;
            
            }
        }


}

 

  }




  /**
   *  Funcion Que se utiliza para agregar una forma de pago.
   * @param {[type]}
   * @param {[type]}
   */
  self.addTipo = function(key,keyEvent){
     if (keyEvent.which == 0 | keyEvent.keyCode == 0){
     // return;
     }
      var index = carritoCompras.getIndiceByKeyCobro(key);
      console.debug("Key a buscar ", key);
      console.debug("Elemento encontrado " , index);
      console.debug(index);
      if( !isNaN(carritoCompras.carrito.formasDePago[index].D_IMPORTE) ) {
          if (carritoCompras.carrito.formasDePago[index].D_IMPORTE <= 0){
                 // mDialog('ERROR','EL IMPORTE NO PUEDE SER MENOR O IGUAL A CERO');
                  return;
          }else{
              if (typeof carritoCompras.carrito.formasDePago[index].TIPO_PAGO == 'undefined'){
                   //   mDialog('ERROR','POR FAVOR SELECCIONE EL TIPO DE PAGO');
                      return;
                  }
                  console.debug("no se actualiza el pago");
                carritoCompras.carrito.formasDePago.push({});
          }
      }else{
           mensajes.alerta('ERROR','POR FAVOR REVISE EL IMPORTE, CONTIENE UN DATO INCORRECTO','Ok');
      }
    };



  /**
   * [delTiposDePago Se borran  un tipo de pago seleccionado]
   * @param  {[type]} key      [id del elemento a eliminar]
   * @param  {[type]} keyEvent [evento realizado]
   * @return {[type]}          [N/D]
   */
  self.delTiposDePago = function(key,keyEvent){
     if (keyEvent.which == 0 | keyEvent.keyCode == 0){
    // return;
     }
      var index = carritoCompras.getIndiceByKeyCobro(key);
      carritoCompras.carrito.formasDePago.splice(index,1);
      self.importeCliente();

  }


  self.verificarTiposDePago = function(ev){
    var bandera = false;
    var pagos = 0;
    var mensaje ="";
    self.cambiCliente = 0;
    var arrayAux = {};
    if(!$scope.operacionEsCuentaPaciente){
        if (carritoCompras.carrito.servicios.length<1 & carritoCompras.carrito.tipo_pago  == "cobro_serv"){
            mensajes.alerta('ERROR','NO HAY ELEMENTOS EN EL CARRITO DE COMPRAS','Ok');
           return;
        }
    }else{
        carritoCompras.carrito.total = $localStorage.paciente.cuentaPaciente.total;
        self.totalDeAbonos = $localStorage.paciente.cuentaPaciente.abonos;
        if(self.tipoOperacionPago == 0){
            if(!isNaN(self.cuantoAbona)){
              if(self.cuantoAbona <= 0){
                mensajes.alerta('ERROR','EL IMPORTE A ABONAR DEBE SER MAYO A CERO','OK');
                return;
              }
            }else{
              mensajes.alerta('ERROR','POR FAVOR INGRESE EL IMPORTE A ABONAR','OK');
              return;
            }
        }
     
    }


    var result = undefined;
    result = verificarArrayPagos();
  console.debug(result);
    if(result == undefined){
      return;
    }
   
    // result = verificarArrayPagos(); // if(!$scope.operacionEsCuentaPaciente){
    //    result = verificarArrayPagos();
    // }else{
    //    if(self.totalDeAbonos < carritoCompras.carrito.total){
    //       result = verificarArrayPagos();
    //    }
    // }

    if(($localStorage.stateAnterior == "detalleCuentaPacienteEspecifica" || $localStorage.stateAnterior == "crearCuentaPaciente") && (carritoCompras.carrito.tipo_pago  == "abono_cuenta" || carritoCompras.carrito.tipo_pago  == "cerrarCuentaPaciente")){
        console.debug("viene del detalle de una cuenta paciente, el tipo de operación es: ", self.tipoOperacionPago);
        carritoCompras.carrito.NP_CTA_PACIENTE = $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
         if(self.tipoOperacionPago == 0 ){
              eliminarInecesarios(false);
              carritoCompras.carrito.total = self.cuantoAbona;
              
              //console.debug(otroTipoPago, pagoConEfectivo, mOtraFormaPago,contadorInputEfectivo, carritoCompras.carrito.total);
              ////(otroTipoPago,pagoConEfectivo,mOtraFormaPago,contadorInputEfectivo, totalAPagar, esAbonoACuenta, dataArray)
              arrayAux = angular.fromJson(angular.toJson(carritoCompras.carrito));
              bandera = verificarPagoTotal(result.otroTipoPago, result.pagoConEfectivo, result.mOtraFormaPago, result.contadorInputEfectivo, carritoCompras.carrito.total, true, arrayAux);
          }else{
               carritoCompras.carrito.tipo_pago  = "cerrarCuentaPaciente";

               if($localStorage.paciente.DERECHO_HABIENTE){

                  self.totalDeAbonos = $localStorage.paciente.cuentaPaciente.abonos;
                  self.prestacionServicios = $localStorage.paciente.cuentaPaciente.prestacionServicios; 
 arrayAux = angular.fromJson(angular.toJson(carritoCompras.carrito));
                  if((self.totalDeAbonos + self.prestacionServicios) >= carritoCompras.carrito.total){

                      bandera = true;
                  }else{
                      eliminarInecesarios(false);
                       //(otroTipoPago,pagoConEfectivo,mOtraFormaPago,contadorInputEfectivo, totalAPagar, esAbonoACuenta, dataArray)
                     
                      var faltaPagarTotal = carritoCompras.carrito.total -self.totalDeAbonos;
                      bandera = verificarPagoTotal(result.otroTipoPago, result.pagoConEfectivo, result.mOtraFormaPago, result.contadorInputEfectivo, faltaPagarTotal - self.prestacionServicios, false, arrayAux);
                  }

               }else{


                  if(self.totalDeAbonos < carritoCompras.carrito.total){
                     eliminarInecesarios(false);
                      //(otroTipoPago,pagoConEfectivo,mOtraFormaPago,contadorInputEfectivo, totalAPagar, esAbonoACuenta, dataArray)
                      arrayAux = angular.fromJson(angular.toJson(carritoCompras.carrito));
                      var faltaPagarTotal = carritoCompras.carrito.total -self.totalDeAbonos;
                      bandera = verificarPagoTotal(result.otroTipoPago, result.pagoConEfectivo, result.mOtraFormaPago, result.contadorInputEfectivo, faltaPagarTotal, false, arrayAux);
                    }else{
                      eliminarInecesarios(true);
                      self.cambiCliente = self.totalDeAbonos - carritoCompras.carrito.total;
                      console.debug("Se debe cerrar cuenta paciente");
                      carritoCompras.carrito.abonos = self.totalDeAbonos;
                      arrayAux = angular.fromJson(angular.toJson(carritoCompras.carrito));
                      bandera = true;
                      //bandera = verificarPagoTotal(otroTipoPago, pagoConEfectivo, mOtraFormaPago,contadorInputEfectivo, carritoCompras.carrito.total, false, arrayAux);
                    }

               }

            
              
          }


      }else{
          if($localStorage.stateAnterior == "ventaDeServicios" && carritoCompras.carrito.tipo_pago  == "cobro_serv"){
            console.debug("viene de la venta de servicios");
            
            console.debug("arrayAux: ", arrayAux );
            //(otroTipoPago,pagoConEfectivo,mOtraFormaPago,contadorInputEfectivo, totalAPagar, esAbonoACuenta, dataArray)
            arrayAux = angular.fromJson(angular.toJson(carritoCompras.carrito));
            bandera = verificarPagoTotal(result.otroTipoPago, result.pagoConEfectivo,result.mOtraFormaPago, result.contadorInputEfectivo,carritoCompras.carrito.total,false, arrayAux);
            
            console.debug("Resultado de verificar el pago: ", bandera);
         }else{
          console.error("No se puede realizar el pago por que la procedencia no esta considerada");
         }
    }


    if (bandera){
      if(arrayAux.formasDePago){
        for (var i = arrayAux.formasDePago.length - 1; i >= 0; i--) {
          if(arrayAux.formasDePago[i].D_IMPORTE == 0){
              arrayAux.formasDePago.splice(i,1);
          }
        }
      }
      generaPago($scope.operacionEsCuentaPaciente,arrayAux,ev);
    }
}



function eliminarInecesarios(eliminarFormasPago){
  delete carritoCompras.carrito.cajero;
  delete carritoCompras.carrito.efectivo;
  delete carritoCompras.carrito.nomPaciente;
  delete carritoCompras.carrito.paciente;
  delete carritoCompras.carrito.servicios;
  delete carritoCompras.carrito.folio_general;
  if(eliminarFormasPago){
    delete carritoCompras.carrito.formasDePago;
  }

}

function saveCuentasCobrarCredito(esCuentaPaciente,mData){
var url="pagosConvenio/";
console.debug(mData);

console.log($localStorage);
 for(var x in mData.formasDePago){
      if(mData.formasDePago[x].TIPO_PAGO ==5){
          console.log("CUENTAS POR COBRAR LO QUE SE VA ALMACENAR");
          console.log(mData);
          console.log( mData.formasDePago[x]);
          mData.formasDePago[x].convenio.N_MONTO=mData.formasDePago[x].D_IMPORTE;
          mData.formasDePago[x].convenio.N_PACIENTE=parseInt($localStorage.paciente.idexpediente);
          mData.formasDePago[x].convenio.N_USUARIO=parseInt($localStorage.usuario.NP_EXPEDIENTE);

      }
      if(mData.formasDePago[x].TIPO_PAGO ==6){
          console.log("TIPO CRÉDITO LO QUE SE VA ALMACENAR");
          mData.formasDePago[x].convenio.N_MONTO=mData.formasDePago[x].D_IMPORTE;
          mData.formasDePago[x].convenio.N_PACIENTE=parseInt($localStorage.paciente.idexpediente);
          mData.formasDePago[x].convenio.N_USUARIO=parseInt($localStorage.usuario.NP_EXPEDIENTE);
        //TIPO CRÉDITO falta poner el servicio
      }
  }
}
  function generarReporteCerrarCuenta(){
      var url="cuentaPaciente/reportes/avisoDeSalida/"+ $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
       peticiones.getMethodPDF(url)
         .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          }).error(function(data){
              console.log(data);
          });
  }

  function saldoFavor(ev){
      $scope.saldoFavor={};
      var defered = $q.defer();
      var promise = defered.promise;
      $mdDialog.show({
        controller:DialogControllerSaldoFavor,
        templateUrl: '/application/views/caja/saldoFavor.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
         locals:{
           ev:ev,
       },
       resolve : {
              val : function() {
                  return $scope.val;
              }
          }
      })
      .then(function(answer) {
        $scope.saldoFavor=answer;
        defered.resolve();
      }, function() {
        $scope.status = 'You cancelled the dialog.';
          defered.resolve();
      }); 

    return promise;        
  };

  function DialogControllerSaldoFavor($scope,ev,$mdDialog,val) {
    $scope.val={};
    $scope.datos={};
      $scope.catParentesco = [];
      catalogos.getCatalogos("17").success(function(data){
        $scope.catParentesco=data;
        console.log( $scope.catParentesco);
      });
      $scope.catIdentificacion = [];
      catalogos.getCatalogos("18").success(function(data){
        $scope.catIdentificacion=data;
        console.log($scope.catIdentificacion);
      });

      $scope.datos.NOMBRE_PACIENTE=$localStorage.paciente.nombrePaciente;
      $scope.datos.importe_devolver=$localStorage.paciente.cuentaPaciente.saldoAFavor;
      $scope.datos.paciente=parseInt($localStorage.paciente.idexpediente);
      $scope.datos.cajero=$localStorage.usuario.NP_EXPEDIENTE;


      $scope.cancelar = function() {
        $scope.val.aux=false;
        $mdDialog.hide($scope.val);
      };

      $scope.canceladoAutomatico = function() {
          $mdDialog.hide($scope.val);
      };

      $scope.devolucion = function() {
        $scope.datos.aux=true;
        $scope.val=$scope.datos;
        var url="cuentaPaciente/reportes/devolucion/"+$localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
          peticiones.putMethod(url,$scope.val)
             .success(function(data){
                $scope.val.aux=true;
                $scope.val.id_vale=data.id_vale;
                $scope.val.id_cancelacion=data.id_cancelacion;
                $scope.canceladoAutomatico();
            })
          .error(function(data){
             console.log(data);
          });
      };
  };

  function generarReporteSaldoFavor(datos){
    datos.NP_CUENTA_PACIENTE=$localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
      var url="cuentaPaciente/reportes/valecuentapaciente";
       peticiones.putMethodPDF(url,datos)
         .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          }).error(function(data){
              console.log(data);
          });
  }

  function generarReporteAbono(){
      var url="cuentaPaciente/reportes/reportepagoabono/"+ $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
       peticiones.getMethodPDF(url)
         .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          }).error(function(data){
              console.log(data);
          });
  }

 function generaPago(esCuentaPaciente, mData,ev){
    //FUNCIÓN PARA GUARDAR CAMPOS DE CUENTAS POR COBRAR Y CRÉDITO
    saveCuentasCobrarCredito(esCuentaPaciente, mData);
    var url = (esCuentaPaciente) ? 'cuentaPaciente/addAbono' : 'pago_serv_productos';
    if(esCuentaPaciente){
          mData.NP_CTA_PACIENTE=$localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE;
          mData.D_IMPORTE = mData.total;
          mData.cajero=$localStorage.usuario.NP_EXPEDIENTE;
          mData.paciente=$localStorage.paciente.idPaciente;
          mData.nomPaciente=$localStorage.paciente.nombrePaciente;
          if(mData.tipo_pago=="cerrarCuentaPaciente"){
            if(self.debePagar==false){
              mData.formasDePago=[];
            }
            mData.total=$localStorage.paciente.cuentaPaciente.restantePagar;

            var url="cuentaPaciente/cerrar";
             peticiones.postDatos(url,mData)
                 .then(function(data) {
                  var mensaje=($localStorage.paciente.cuentaPaciente.saldoAFavor>0)? "USTED CUENTA CON SALDO A FAVOR , INGRESE LOS SIGUIENTES DATOS":"";
                   mensajes.mConfirmacionValida('', "SE CERRRO CORRECTAMENTE LA CUENTA"+mensaje,"Ok")
                   .then(function() {
                      if($localStorage.paciente.cuentaPaciente.saldoAFavor>0){
                        saldoFavor(ev).then(function(){
                              if($scope.saldoFavor.aux){
                                 mensajes.mConfirmacionValida("", "DEVOLUCIÓN EXITOSA. ","ACEPTAR")
                                  .then(function() {
                                          generarReporteSaldoFavor($scope.saldoFavor);
                                          generarReporteCerrarCuenta();
                                          $state.go("bienvenido");
                                   }).catch(function(error) {
                                 });
                              }  
                        });
                      }else{
                         generarReporteCerrarCuenta();
                      }
                    
                   }).catch(function(error) {
                   });
             }).catch(function(response) {
                console.log(response);
             });

          }else{
             console.debug("Se hizo el pago y a continuación se muestra el cambio en caso de que aplique");
              //self.idFolioRecibo = 75516;
              //se valida si el tipo de pago es una liquidación
              peticiones.putMethod(url,mData)
                    .then(function(data) {
                      mensajes.alerta('','PAGO REALIZADO DE FORMA EXITOSA','Ok');
                      self.idFolioRecibo = 75516;  
                      generarReporteAbono();              
              }).catch(function(response) {
                console.error(response);
                     mensajes.alerta('ERROR','OCURRIO UN ERROR AL REALIZAR LA TRANSACCIÓN POR FAVOR CONTACTE AL ADMINISTRADOR','Ok');
              });
          }
         
    }else{
          if (mData.servicios.length>0){
              peticiones.postDatos(url,mData)
                .then(function(data) {
                  //carritoCompras.reset();
                  self.idFolioRecibo = parseInt(data.id);
                   self.impRecibo = function() {
                      reportes.getReporte(baseURL + 'reporte_ReciboDePago/' +  data.id , '_blank', 'width=1000, height=800');
                       $state.go('bienvenido');
                    };
              }).catch(function(response) {
                 mensajes.alerta('ERROR','OCURRIO UN ERROR AL REALIZAR LA TRANSACCIÓN POR FAVOR CONTACTE AL ADMINISTRADOR','Ok');
                });
          }else{
             mensajes.alerta('Error','No hay elementos en el carrito de compras','Ok');
          }
    } 
  }



    /**
   * [verificarPagoTotal Funsión para verificar  el pago total de la cuantea o el servicio,
   * solo se va a utilizar para pagar totales]
   * @param  {[type]} otroTipoPago [parametro para verificar si solo es pago en efectivo o hay diferentes formas de pago]
   * @return {[type]}              [description]
   */
  var verificarPagoTotal = function(otroTipoPago,pagoConEfectivo,mOtraFormaPago,contadorInputEfectivo, totalAPagar, esAbonoACuenta, dataArray){
    var bandera = true;
    var importeNoAlcanzado = (esAbonoACuenta) ? 'EL IMPORTE INGRESADO NO ALCANZA A CUBRIR EL COSTO A ABONAR' :'EL IMPORTE INGRESADO NO ALCANZA A CUBRIR EL COSTO TOTAL';
    var diferentesPagos = (esAbonoACuenta) ?  'LAS FORMAS DE PAGO QUE SON DIFERENTES A “EFECTIVO” DEBEN SUMAR LA CANTIDAD EXACTA A ABONAR, O EN CASO DE TENER UN MONTO MENOR EL RESTO SERÁ PAGADO CON EFECTIVO' :'LAS FORMAS DE PAGO QUE SON DIFERENTES A “EFECTIVO” DEBEN SUMAR LA CANTIDAD EXACTA A PAGAR, O EN CASO DE TENER UN MONTO MENOR EL RESTO SERÁ PAGADO CON EFECTIVO';
    var cantidadExacta = (esAbonoACuenta) ? 'LAS FORMAS DE PAGO QUE SON DIFERENTES A “EFECTIVO” DEBEN SUMAR LA CANTIDAD EXACTA A ABONAR' : 'LAS FORMAS DE PAGO QUE SON DIFERENTES A “EFECTIVO” DEBEN SUMAR LA CANTIDAD EXACTA A PAGAR';

console.debug("++++++++",otroTipoPago,pagoConEfectivo,mOtraFormaPago,contadorInputEfectivo, totalAPagar, esAbonoACuenta, dataArray);

    ///var noAlcanza = (otroTipoPago) ? 'EL IMPORTE INGRESADO NO ALCANZA A CUBRIR EL COSTO TOTAL' : 'EL IMPORTE INGRESADO NO ALCANZA A CUBRIR EL COSTO TOTAL';
    if(otroTipoPago == false){
      console.debug("Entro solo a la opción de pago con efectivo");
        if (parseFloat(pagoConEfectivo) >= parseFloat(totalAPagar)){
            self.cambiCliente =  parseFloat(pagoConEfectivo) - parseFloat(totalAPagar);
            dataArray.formasDePago[0].D_IMPORTE =  parseFloat(pagoConEfectivo) - self.cambiCliente;
             dataArray.formasDePago[0].C_OPERADOR = "";
             dataArray.formasDePago[0].N_AUTORIZACION = "0";
             dataArray.formasDePago[0].N_OPERACION = "0";
             dataArray.formasDePago[0].C_REFERENCIA = "";

             if(dataArray.formasDePago.length > 1){
                dataArray.formasDePago.splice(1,dataArray.formasDePago.length);              
             }
            
        }else{
            bandera = false;
            mensajes.alerta('ERROR',importeNoAlcanzado,'Ok');
        }
    }else{
         if ((parseFloat(pagoConEfectivo) + parseFloat(mOtraFormaPago)) >= parseFloat(totalAPagar)){
              if ((parseFloat(pagoConEfectivo) + parseFloat(mOtraFormaPago)) >= parseFloat(totalAPagar)){
                  self.cambiCliente = (parseFloat(pagoConEfectivo) + parseFloat(mOtraFormaPago))  - parseFloat(totalAPagar);
                  if (contadorInputEfectivo > 0){
                    if (self.cambiCliente > pagoConEfectivo){
                        bandera = false
                        mensajes.alerta('ERROR',diferentesPagos,'Ok');
                    }else{
                        var banC = false;
                        var ids =[];
                          for(var dat = 0; dat < dataArray.formasDePago.length; dat++){
                            if (banC == false && dataArray.formasDePago[dat].TIPO_PAGO == 1 ){
                                dataArray.formasDePago[dat].D_IMPORTE = pagoConEfectivo - self.cambiCliente;
                                banC = true;
                            }else{
                                    if (dataArray.formasDePago[dat].TIPO_PAGO == 1 ){
                                      ids.push(dat);
                                    }
                            }
                          }
                          for (var i = ids.length - 1; i >= 0; i--) {
                            dataArray.formasDePago.splice([ids[i]], 1);
                          };
                    }
                  }else{
                    if(parseFloat(mOtraFormaPago) != parseFloat(totalAPagar)){
                        bandera = false;
                        mensajes.alerta('ERROR',cantidadExacta,'Ok');
                    }
                  }
              }
            }else{
                bandera = false;
                mensajes.alerta('ERROR',importeNoAlcanzado,'OK');
            }
      }
      return bandera;
  }




function verificarArrayPagos(){

  var result = {};
  result.contadorInputEfectivo = 0;
  result.pagoConEfectivo = 0;
  result.otroTipoPago = false;
  result.mOtraFormaPago = 0;

if($localStorage.paciente.cuentaPaciente){
  if($localStorage.paciente.DERECHO_HABIENTE && self.tipoOperacionPago ==  1 && $localStorage.paciente.cuentaPaciente.restantePagar == 0){
      return result;
  }else{
    console.debug($localStorage.paciente.cuentaPaciente.saldoAFavor , $localStorage.paciente.cuentaPaciente.restantePagar  , carritoCompras.carrito.tipo_pago ,"abono_cuenta");
     if($localStorage.paciente.cuentaPaciente.saldoAFavor >=0 && $localStorage.paciente.cuentaPaciente.restantePagar == 0 && self.tipoOperacionPago ==  1){
      return result;
    }
  }
 
}
  


for (var id = 0; id < carritoCompras.carrito.formasDePago.length; id++)
  {
      if(!isNaN(carritoCompras.carrito.formasDePago[id].D_IMPORTE)){
          carritoCompras.carrito.formasDePago[id].D_IMPORTE = parseFloat(carritoCompras.carrito.formasDePago[id].D_IMPORTE);
          if(carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 1){
              result.pagoConEfectivo += parseFloat(carritoCompras.carrito.formasDePago[id].D_IMPORTE);
              result.contadorInputEfectivo += 1;
            }else{
                result.otroTipoPago = true;
                result.mOtraFormaPago += carritoCompras.carrito.formasDePago[id].D_IMPORTE;
            }
      }else{
          if(carritoCompras.carrito.total == 0){
           break;
          }
          mensajes.alerta('ERROR','POR FAVOR REVISE EL IMPORTE, CONTIENE UN DATO INCORRECTO','OK');
          return;
      }
    if( typeof carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 'undefined'){
             mensajes.alerta('POR FAVOR SELECCIONE EL TIPO DE PAGO');
             return;
    }
    carritoCompras.carrito.formasDePago[id].TIPO_PAGO =parseInt(carritoCompras.carrito.formasDePago[id].TIPO_PAGO);
    if (carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 2 || carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 3){
      if( typeof carritoCompras.carrito.formasDePago[id].NP_ID == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR SELECCIONE EL TIPO DE BANCO','OK');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].C_OPERADOR == 'undefined'){

             mensajes.alerta('ERROR','POR FAVOR SELECCIONE EL TIPO DE OPERADOR','OK');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].N_AUTORIZACION == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR INGRESE EL NÚMERO DE AUTORIZACIÓN','OK');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].N_OPERACION == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR INGRESE EL NÚMERO DE OPERACIÓN','OK');
             return;
      }
        carritoCompras.carrito.formasDePago[id].N_AUTORIZACION = carritoCompras.carrito.formasDePago[id].N_AUTORIZACION;
        carritoCompras.carrito.formasDePago[id].N_OPERACION = carritoCompras.carrito.formasDePago[id].N_OPERACION;
    }
    if (carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 4){
      if( typeof carritoCompras.carrito.formasDePago[id].NP_ID == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR SELECCIONE EL TIPO DE BANCO','OK');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].C_REFERENCIA == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR INGRESE EL NÚMERO DE REFERENCIA','OK');
             return;
      }
       if(isNaN(carritoCompras.carrito.formasDePago[id].C_REFERENCIA)){
            mensajes.alerta('ERROR','EL NÚMERO DE REFERENCIA NO ES VALIDO','OK');
            return;
       }
    }
    if (carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 5){
      if( typeof carritoCompras.carrito.formasDePago[id].convenio.C_TIPO_SERVICIO == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR INGRESE EL TIPO DE SERVICIO','OK');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].convenio.N_PARENTESCO == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR SELECCIONE PARENTESCO','OK');
             return;
      }

      if( typeof carritoCompras.carrito.formasDePago[id].convenio.TIPO_CONVENIO == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR SELECCIONE TIPO DE CONVENIO','OK');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].convenio.C_FOLIO == 'undefined'){
             mDialog('ERROR','POR FAVOR INGRESE SEGURO POPULAR','OK');
             return;
      }
      if( typeof carritoCompras.carrito.formasDePago[id].convenio.C_REFERENCIA == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR INGRESE EL DOCUMENTO DE REFERENCIA','OK');
             return;
      }

    }
     if (carritoCompras.carrito.formasDePago[id].TIPO_PAGO == 6){
        if($scope.operacionEsCuentaPaciente){
          if( typeof carritoCompras.carrito.formasDePago[id].convenio.C_FOLIO == 'undefined'){
             mensajes.alerta('ERROR','POR FAVOR INGRESE EL NÚMERO DE DOCUMENTO DE CRÉDITO','OK');
             return;
          }
          //si el tipo de pago crédito
        }
    }
    
  }//Termina el for
    return result;
}





  

    }]);
  })();
