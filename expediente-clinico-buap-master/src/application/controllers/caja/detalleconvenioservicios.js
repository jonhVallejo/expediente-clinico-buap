// 'use strict';

// /**
//  * @ngdoc function
//  * @name ECEangular.module('expediente').controller:CajaDetalleconvenioserviciosCtrl
//  * @description
//  * # CajaDetalleconvenioserviciosCtrl
//  * Controller of the expediente
//  */
// angular.module('expediente')
//   .controller('CajaDetalleconvenioserviciosCtrl', function () {
//     this.awesomeThings = [
//       'HTML5 Boilerplate',
//       'AngularJS',
//       'Karma'
//     ];
//   });

(function(){
'use strict';
angular.module('expediente')
    .controller('CajaDetalleconvenioserviciosCtrl',['$scope','$q', '$mdDialog','$http','$state','pacientes','peticiones','$localStorage','cuentaPacienteService',
      function ($scope,$q, $mdDialog,$http,$state,pacientes,peticiones,$localStorage,cuentaPacienteService) {
     	console.log($localStorage);
     	$scope.paciente=$localStorage.paciente;
     	var self = this;
        var url = "cuentaPaciente/historial/";
        $scope.tCuentaPaciente = [];

     	self.getPaciente = function(){
          pacientes.getPaciente()
          .success(function(data){
            console.debug(data);
            if(!data.data.hasOwnProperty('NP_ID')){
              $scope.paciente=data.data[0];
              console.log($scope.paciente);
              self.getCuentasPaciente($scope.paciente.NP_EXPEDIENTE.substring(0,$scope.paciente.NP_EXPEDIENTE.indexOf("/",0)));
             // self.derechoHabiente = !verificaNP_ID($scope.paciente.NP_ID);
            }
            else{
            //  mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
            }
          })
          .error(function(data){
            mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
          });
        };

        self.getCuentasPaciente = function(NP_EXPEDIENTE){
            peticiones.getDatos(url + NP_EXPEDIENTE)
              .then(function(data) {
                console.log(data.T_CUENTA_PACIENTE);
                 $scope.tCuentaPaciente = _.filter(data.T_CUENTA_PACIENTE , function(e){
                    return e.NF_ESTATUS == 1 || e.NF_ESTATUS == 0;
                  });
                  //$localStorage.paciente.cuentaPaciente={};
                  //$localStorage.paciente.cuentaPaciente=data.T_CUENTA_PACIENTE;
                  data.T_PACIENTE["NP_EXPEDIENTE"]=NP_EXPEDIENTE;
                  console.log(data);
                  //cuentaPacienteService.setNPCuentaPaciente(data);
              })
              .catch(function(err) {
                  // Tratar el error
              });
        };

        self.verDetalle = function(ev,dato){
        	  //$localStorage.paciente.cuentaPaciente={};
        	  $localStorage.paciente.NP_CUENTA=dato;
           // cuentaPacienteService.setNPCuentaPaciente(dato);
            console.debug(dato);
            $state.go("causesServicios");
        };
        self.getPaciente();
        $scope.queryCuenta = {
          filter: '',
          order: '-NP_ID',
          limit: 5,
          page: 1
        };


 }]);
})();
