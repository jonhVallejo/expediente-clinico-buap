'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:CajaReimprimirrecetaCtrl
 * @description
 * # CajaReimprimirrecetaCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('CajaReimprimirrecetaCtrl', function ($scope, $http, mensajes, $window, reportes) {
    
    $scope.obtenReceta = function(folio) {
    	$http.get(baseURL + "receta/" + folio)
  		.success(function(result, response){
  			switch(response){
  				case 200:
  				reportes.getReporte(baseURL + 'getpdf/' + result.C_MODULO + '/' + result.C_FILE_RECETA , '_blank' , 'width=800, height=640');  			
  				break;
  				case 204:
	  			mensajes.alerta('AVISO','NO SE ENCUENTRAN RESULTADOS DE ESE FOLIO','ACEPTAR');	
  				break;
  			}
  		})
  		.error(function(data) {
  			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
  		});
    }

  });
