(function(){
'use strict';


angular.module('expediente')
    .controller('buscaPreCitas',['$scope','$timeout','buscaPreCitasService','$state','carritoCompras','peticiones','pacientes',
      function ($scope,$timeout,buscaPreCitasService,$state,carritoCompras,peticiones,pacientes) {

        $scope.title="CONSULTA DE PRE-CITAS";

        var self = this;
        self.mData = [];

       var  getListPreCitas = function(){
           peticiones.getMethod('precitas/').
             success(function(data){
               self.mData = data;

               console.log(data);
             });
        };

        getListPreCitas();

       self.detalleCita = function(event,mData){
            carritoCompras.reset();
            buscaPreCitasService.setDatosPreCita(mData);

            self.mData.cClave                    =  buscaPreCitasService.getClaveServicio();
            self.mData.NP_ID                     =        buscaPreCitasService.getServicio_id();
            self.mData.C_DESCRIPCIPON            = buscaPreCitasService.getDescripcionService();
            self.mData.D_PESO                    =            buscaPreCitasService.getImporte();
            self.mData.tipo                      = "Cita";
            self.mData.cVariable                 = 0;
            carritoCompras.carrito.tipo_pago     = "pre_cita";
            carritoCompras.carrito.folio_general =   buscaPreCitasService.getFolio();

            var paciente                         = [];
            paciente.NOMBRE                      = buscaPreCitasService.getNomPaciente();
            paciente.NP_ID                       = buscaPreCitasService.getPaciente_id();
            pacientes.cargaPacienteMinimo(paciente);
            carritoCompras.addServicio(self.mData);
            $state.go('ventaDeServiciosPrecita');

        };

        $scope.onOrderChange = function(page, limit) {
            var deferred = $q.defer();
            $timeout(function () {
              deferred.resolve();
            }, 2000);
            return deferred.promise;
        };

        $scope.query = {
            order: 'clave_servicio',
            limit: 15,
            page: 1
        };
  }]);
})();
