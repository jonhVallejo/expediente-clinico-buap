(function(){
'use strict';

/**
 * [Controlador utilizado para que el cajero pueda realizar busqueda de servicios
 * mediante diferentes patrones de busqueda como son (Clave, por nombre, por categoria)]
 * @param  {[type]} $state     [description]
 * @param  {[type]} $scope     [description]
 * @param  {[type]} $q         [description]
 * @param  {[type]} $log       [description]
 * @param  {[type]} $mdDialog  [description]
 * @param  {[type]} catalogos) {                  }] [description]
 * @return {[type]}            [description]
 */
  angular.module('expediente').controller('listaServicios',['$state','$scope','$q', '$log','$mdDialog','catalogos','$timeout',
    function ($state,$scope, $q, $log,$mdDialog,catalogos,$timeout) {

    var self = this;
    self.categoriaServicios = [];
    self.mListaDeServicios = [];
     getServicios();


    function getServicios(){
        catalogos.getCatalogos(2)
          .success(function(data){
        self.mListaDeServicios =  data;
      })
      .error(function(data){
        $mdDialog.show(
          $mdDialog.alert()
          .clickOutsideToClose(true)
          .title('ERROR')
          .content('Ha ocurrido un error al intentar cargar los datos')
          .ok('Aceptar')
        );
      });
    };


    self.click = function(){
      console.log("mi directive ss");
      $scope.alta = true;
      $scope.busqueda = false;
      $scope.data="";
    }

    this.primerPagina = function(){
      $scope.query.page = 1;
    };


    function success(desserts) {
    $scope.desserts = desserts;
  }

$scope.onpagechange = function(page, limit) {
    var deferred = $q.defer();

    $timeout(function () {
      deferred.resolve();
    }, 2000);

    return deferred.promise;
  };

  $scope.onorderchange = function(order) {
    var deferred = $q.defer();

    $timeout(function () {
      deferred.resolve();
    }, 2000);

    return deferred.promise;
  };




    $scope.query = {
        order: 'cDescripcion',
        limit: 15,
        page: 1
    };

     // $scope.query = {
     //    order: 'name',
     //    limit: 5,
     //    page: 1
     //  };

    $scope.columns = [ {

        name: 'CLAVE',
        orderBy: 'cClave',
        width : '10%'
    }, {
      descendFirst: true,
      name: 'DESCRIPCIÓN',
        orderBy: 'cDescripcion',
        width : '50%'
    },
     {
      name: 'CATEGORÍA',
        orderBy: 'categoria',
        width : '30%'
    },
    {
      name: 'PRECIO UNITARIO',
        orderBy: 'dCosto',
        unit: '$',
        width : '10%'
    }];

    }]);

})();
