(function(){
'use strict';
/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:CajaAsignacioncamaCtrl
 * @description
 * # CajaAsignacioncamaCtrl
 * Controller of the expediente
 */
angular.module('expediente')
    .controller('CajaAsignacioncamaCtrl',['$scope','$mdDialog','$state','$localStorage','$q','catalogos','mensajes','usuario','peticiones','$window','pacientes','$location',
      function ($scope, $mdDialog, $state, $localStorage,$q,catalogos,mensajes,usuario,peticiones,$window,pacientes,$location) {
      
      $scope.paciente=$localStorage.paciente;
      $scope.subSeccion=[];
      $scope.arregloSubseccion = [];
      $scope.catCamas=[];
      var idCama=undefined;
      var idServicio= undefined;
      inicializarCamas();

      console.log(usuario);
      console.log($localStorage);

    //  $scope.registros=self.mCuentaPaciente.C_CAMAS;
      $scope.mCuentaPaciente={};

      $scope.query = {
        filter: '',
        order: 'NF_CAMA',
        limit: 5,
        page: 1
      };

     

      catalogos.getCatalogos('32')
      .success(function(data){
        $scope.catServicioMedico = data;
          console.log("DATAAAAAAAAA");
         console.log(data);
       })
      .error(function (err) {
        console.log(err);
      });


      function inicializarCamas(){
         catalogos.getCatalogos('camas/1')
          .success(function(data){
            $scope.catAreas = data;
            console.log(data);
          })
          .error(function (err) {
            console.log(err);
          });
      };

      $scope.onOrderChange = function(page, limit) {
        var deferred = $q.defer();
        $timeout(function () {
          deferred.resolve();
        }, 2000);
        return deferred.promise;
      };

      $scope.hide = function() {
        $mdDialog.hide();
      };

      $scope.close = function() {
        $mdDialog.hide();
      };

      $scope.limpiar = function(){
        $scope.area="";
        $scope.seccion="";
        $scope.subSeccion=[];
        $scope.tipoCama="";
        $scope.cama="";
        $scope.catSeccion = [];
        $scope.catTipoCama={Camas: []};
        $scope.catCamas=[];
      }

      function validarExisteCama(){
        var defered = $q.defer();
        var promise = defered.promise;
        $scope.valor=undefined;
         pacientes.getPacientes($localStorage.paciente.expediente)
          .success(function(data){
            console.log(data.data[0].NP_CAMA!==0);
            if(data.data[0].NP_CAMA!==0){
              $scope.valor=true;
              defered.resolve();
           }
            else{
              $scope.valor=false;
              defered.resolve();
            }
          })
        .error(function(data){
          mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
          valor=false;
          defered.reject();
        });
        return promise;
      };
      
      function setCama (){
        var url="cuentaPaciente/asignarCamaManual"+"/"+$scope.paciente.idexpediente+"/"+idCama+"/"+usuario.usuario.NP_EXPEDIENTE+"/"+idServicio;
              peticiones.getDatos(url)
             .then(function(data) {
                 if(data.CUENTA_PACIENTE==0)
                  mensajes.alerta('ERROR','EL PACIENTE REQUIERE TENER UNA CUENTA DE CUENTA PACIENTE','ACEPTAR');
                 else{
                       inicializarCamas();
                       mensajes.alerta('EXITO','SE ASIGNO CORRECTAMENTE LA CAMA','ACEPTAR');
                       console.log($localStorage.stateAnterior);
                       $state.go($localStorage.stateAnterior);
                      // $location.path("/agenda/consultaPaciente/asignacionCama");
                   }
              })
              .catch(function(err) {
                console.log(err);
              });
      };

      $scope.asignarCama = function(){
        validarExisteCama().then(function() {
          if($scope.valor){
             mensajes.confirmacion("ALERTA","EL PACIENTE YA CUENTA CON UNA CAMA ¿ DESEA CAMBIAR LA CAMA?","OK")
              .then(function() {
                 setCama();
              }).catch(function() {
              });
          }else{
              setCama();
          } 
        }); 
      };



      $scope.changeArea = function(){
        var idCama=undefined;
        $scope.catSeccion = [];
        $scope.catTipoCama=undefined;
        $scope.arregloSubseccion=[];
        $scope.catCamas=[];
        $scope.seccion="";
        $scope.tipoCama="";
        $scope.data={};
        $scope.catSeccion = $scope.catAreas[$scope.area].areas;
        $scope.data.idHospitalizacion=$scope.catAreas[$scope.area].ID;
      }

      $scope.changeSeccion = function(){
        var idCama=undefined;
        $scope.subSeccion[$scope.subSeccion.length-1]="";
        $scope.catTipoCama=undefined;
        $scope.tipoCama="";
        $scope.arregloSubseccion = [];
        $scope.catCamas=[];

        $scope.data.idArea=$scope.catAreas[$scope.area].areas[$scope.seccion].ID; // Obtiene el id de la sección
          if($scope.catAreas[$scope.area].areas[$scope.seccion].hasOwnProperty("subAreas")) // TIENE SUBSECCIONES
            $scope.arregloSubseccion.push($scope.catAreas[$scope.area].areas[$scope.seccion].subAreas);
          else {
            if($scope.catAreas[$scope.area].areas[$scope.seccion].hasOwnProperty("tiposCama")){
                  $scope.catTipoCama=$scope.catAreas[$scope.area].areas[$scope.seccion].tiposCama;
              }
              if($scope.catTipoCama==undefined){//No tiene camas 
                $scope.catTipoCama=[];
              }    
          }    
      }

      $scope.changeSubSeccion = function(key,index){
        $scope.subSeccion.splice(getIndiceCombo(key)+1 ,$scope.subSeccion.length - 1);
        $scope.arregloSubseccion.splice(getIndiceCombo(key) + 1 ,$scope.arregloSubseccion.length - 1);
        $scope.data.idArea=$scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].ID; 
           if ($scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].subAreas!== undefined ){//TIENE SUBSECCIONES
              $scope.arregloSubseccion.push([$scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].subAreas]);
              }
              else{//YA NO EXISTEN SUBSECCIONES VERIFICO SI TIENE CAMAS
                    if($scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].hasOwnProperty("tiposCama")){
                        $scope.catTipoCama=$scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].tiposCama;
                      }
                    if($scope.catTipoCama==undefined){
                        $scope.catTipoCama=[];
                    } 
                  }
      }

      function getIndiceCombo (key) {
        for (var index = 0; index < $scope.arregloSubseccion.length; index++){
          if($scope.arregloSubseccion[index].$$hashKey ===  key){
                return index;
          }
        }
      };

      $scope.changeTipoCama = function(index){
        var aux;
         $scope.identity = angular.identity;
          if($scope.catTipoCama[index].hasOwnProperty("camas")){
            $scope.catCamas=$scope.catTipoCama[index].camas;
            $scope.catCamas.sort(function(a, b){
              return a.cDescripcion-b.cDescripcion
            })
             $scope.data.idTipo=$scope.catTipoCama[index].ID;
          }
          else
              mensajes.alerta('ADVERTENCIA','EL PACIENTE REQUIERE TENER UNA CUENTA DE CUENTA PACIENTE','ACEPTAR');
      };
        

      $scope.changeCama = function(id){
        idCama=id;
      };

      $scope.changeServicio = function(id){
        console.log(id);
        idServicio=id;
      };
 }]);
})();
