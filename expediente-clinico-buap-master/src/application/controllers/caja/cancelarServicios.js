'use strict';
(function(){


/**
 * Controlador que s eva a encargar de manejar toda las cancelaciones de servicios, se le va a pasar como parametro
 * el nombre del paciente o bien su folio del recibo de compra.
 */

angular.module('expediente').controller('cancelarServicios',['$mdDialog','$http','$scope','$timeout', '$q', '$log','cancelarServicio','mensajes','catalogos',
      function ($mdDialog,$http,$scope,$timeout, $q, $log,cancelarServicio,mensajes,catalogos) {

      	var self = this;
      	self.cs = cancelarServicio;

      	$scope.showAdvanced = function(ev,obj,index) {
      		self.cs.index = index;
      		self.cs.setServicio(obj);
		    $mdDialog.show({
			      controller: DialogController2,
			      templateUrl: '/application/views/caja/eliminarservicio.html',
			      parent: angular.element(document.body),
			      targetEvent: ev,
			      clickOutsideToClose:false
		    })
		    .then(function(answer) {
		    	if(answer==='true'){
		    		self.cs.eliminarServicio($scope.query.page,$scope.query.limit);
			    }
		    }, function() {
		      //$scope.status = 'You cancelled the dialog.';
		    });
		};

		$scope.siguiente = function(){
			self.cs.siguiente();
		};

		$scope.calcColores = function(index){
			return ($scope.query.limit* ($scope.query.page-1) )+index;
		}



      	////////////////////////
      	///	FUNCIONES DE TABLA
      	////////////////////////
      	
      	$scope.query = {
		    order: 'clave_servicio',
		    limit: 5,
		    page: 1
		};

		$scope.columns = [{
		    name: 'ELIMINADOS / CANTIDAD',
		    width : '5%'
		}, {
			descendFirst: true,
		    name: 'CLAVE',
		    orderBy: 'C_CLAVE',
		    width : '10%'
		}, {
			name: 'DESCRIPCIÓN',
		    orderBy: 'C_DESCRIPCION',
		    width : '40%'
		}, {
			name: 'PRECIO UNITARIO',
		    orderBy: 'D_IMPORTE',
		    unit: '$',
		    width : '5%'
		}, {
			name: 'TOTAL',
		    orderBy: 'D_IMPORTE_TOTAL',
		    unit: '$',
		    width : '5%'
		},{
			name: 'MODIFICAR',
		    width : '10%'
		}];

		$scope.columnsEliminar = [{
		    name: 'CANTIDAD',
		    width : '10%'
		}, {
			descendFirst: true,
		    name: 'CLAVE',
		    orderBy: 'NP_ID',
		    width : '10%'
		}, {
			name: 'DESCRIPCIÓN',
		    orderBy: 'C_DESCRIPCIPON',
		    width : '40%'
		}, {
			name: 'PRECIO UNITARIO',
		    orderBy: 'D_PESO',
		    unit: '$',
		    width : '10%'
		}];

		$scope.onpagechange = function(page, limit) {
    
		    console.log('Scope Page: ' + $scope.query.page + ' Scope Limit: ' + $scope.query.limit);
		    console.log('Page: ' + page + ' Limit: ' + limit);
		    
		    var deferred = $q.defer();
		    
		    $timeout(function () {
		      deferred.resolve();
		    }, 2000);
		    
		    return deferred.promise;
		  };

		$scope.onOrderChange = function(page, limit) {
		    var deferred = $q.defer();
				    
		    $timeout(function () {
		    	deferred.resolve();
		    }, 2000);
				    
		    return deferred.promise;
		};


      }]);

})();

function DialogController2($scope, $mdDialog,cancelarServicio) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
  	var valor = cancelarServicio.cantidad;
  	var patron = /[0-9]+/;
  	if(answer==='true' && (!patron.test(valor) || valor > cancelarServicio.servicio[0].N_SERVICIOS || valor < 0)){
  		cancelarServicio.mensaje = 'Debe introducir un número entre 1 y '+cancelarServicio.servicio[0].N_SERVICIOS;
  		return 0;
  	}
    $mdDialog.hide(answer);
  };
};


