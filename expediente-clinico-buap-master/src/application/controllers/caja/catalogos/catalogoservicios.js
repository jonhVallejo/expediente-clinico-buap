'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:CajaCatalogosCatalogoserviciosCtrl
 * @description
 * # CajaCatalogosCatalogoserviciosCtrl
 * Controller of the expediente
 */
var app = angular.module('expediente')
		 angular.module('expediente').controller('CajaCatalogosCatalogoserviciosCtrl', function ($scope,$timeout, $q, $log, $mdToast, $mdDialog,$stateParams,catalogos,mensajes,usuario) {
	          	 $scope.idCatalogo = $stateParams.idCatalogo;
	            var id=$scope.idCatalogo;
	            var mUser =  usuario.usuario.NP_EXPEDIENTE;
	          	$scope.data={nfUsuario:mUser};
	          	var band=true;
	            /*Variables para visualizar determinados apartados */
	            $scope.editar = false;
				$scope.alta = false;
				$scope.busqueda = true;
				/*Almacena datos a mostrar en la tabla*/
			 	$scope.registros = [];
				/*Se inicializan valores a la tabla*/
				$scope.query = {
				    filter: '',
				    order: 'id',
				    limit: 5,
				    page: 1
				};
				$scope.selected = [];
				/*"data" es la que permite recuperar los datos del html y se inicializa*/
				$scope.data = {};
				$scope.backData = {};//auxiliar
				var datos={};//auxiliar


			$scope.onOrderChange = function(page, limit) {
			    var deferred = $q.defer();
			    $timeout(function () {
			    	deferred.resolve();
			    }, 2000);

			    return deferred.promise;
			};
			//CLICK EN AGREGAR se oculta el apartado de busqueda y se activa el formulario de alta
			$scope.click = function(){
		  		$scope.alta = true;
		  		$scope.busqueda = false;
		  		$scope.data="";
	        }
		    $scope.cargaTabla = function(urlCat) {
		    	/*INICIALIZACIÓN DE VALORES PONERLO EN UNA FUNCIÓN*/
			    $scope.registros = [];//Cada que cargo una tabla requiere de nuevo valores por tanto limpio registros
			    $scope.editar = false;
				$scope.alta = false;
				$scope.busqueda = true;
				$scope.buscar="";
				$scope.query = {
			    filter: '',
			    order: 'id',
			    limit: 5,
			    page: 1
				};

        console.log("Catalogos " + urlCat);

			    catalogos.getCatalogos(urlCat)

			    .success(function(data){
            console.log("Catalogos " + urlCat);
					$scope.registros=data;
				})
				.error(function(data){
					    $scope.registros = [];
						mensajes.datoNoEncontrado();
				});
			};
		     $scope.addReg = function(registros,urlCat){
		    	$scope.data.nfUsuario=mUser;
		        datos={};
		    	$scope.cDescripcion="";
	        	if($scope.data.categoria!=null){
	        		datos = {nfUsuario:mUser,cClave:$scope.data.cClave ,idCategoria:parseInt($scope.data.categoria.ID),
				  				cDescripcion:$scope.data.cDescripcion,dCosto:$scope.data.dCosto,idProveedor:parseInt($scope.data.proveedor.ID)};
				  	console.log(datos);
	        	}
	        	else{
	        		datos=$scope.data;
	        	}
		       catalogos.guardaCatalogo(urlCat,datos)
					.success(function(data){
						if(data.res==1){
							catalogos.getCatalogos(urlCat).success(function(data){
								$scope.registros=data;
							});
							mensajes.alerta('CONFIRMACIÓN','SE HA AGREGADO EXITOSAMENTE','ACEPTAR');
						}
						else{
							if(data.res==2){
								mensajes.alerta('ALERTA','EL ELEMENTO QUE DESEA AGREGAR YA SE ENCUENTRA EN EL CATÁLOGO','ACEPTAR');
							}
							else{
								mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
							}
						}

						$scope.alta = false;//Se desactiva el formulario para agregar un nuevo registro
						$scope.busqueda = true;
						console.log("entro a sucess"+data.res);

					})
					.error(function(data){
						mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
					});
		    };
		     $scope.editRow = function(){
			  	var alert;
			  	if ($scope.selected.length > 1) {
				  	alert = $mdDialog.alert().title('INFORMACIÓN').content('SÓLO DEBE SELECCIONAR UN REGISTRO').ok('CERRAR');
				    $mdDialog
				        .show( alert )
				        .finally(function() {
				            alert = undefined;
				        });
				    $scope.editar = false;
				    $scope.busqueda = true;
			  	} else {
					$scope.backData = $scope.selected[0];
				    console.log($scope.selected[0]);
					$scope.data = angular.copy($scope.backData);
					$scope.editar = true;
					$scope.busqueda = false;
					$scope.alta = false;
			  	}
			  	$scope.selected = [];
	        };
	        $scope.updateReg = function(registros,urlCat){
	        	console.log("*******MODIFICADO************");
				console.log("todos los datos************");
	         	console.log($scope.data);

	         	console.log("categoria************");
	         	console.log($scope.data.categoria);

	             console.log("proveedor***********");
	             console.log($scope.data.proveedor);
	         	/*$scope.data.nfUsuario=mUser;
	        	var id=$scope.data.ID;
	        	if($scope.data.categoria!=null){
	        		 datos = {nfUsuario:mUser,cClave:$scope.data.cClave ,idCategoria:parseInt($scope.data.categoria.ID),
				  				cDescripcion:$scope.data.cDescripcion,dCosto:$scope.data.dCosto,idProveedor:parseInt($scope.data.proveedor.ID)};
				  	console.log(datos);
				  	$scope.data= {ID:$scope.data.ID,cClave:$scope.data.cClave ,categoria:$scope.data.categoria.cDescripcion,
				  				cDescripcion:$scope.data.cDescripcion,dCosto:$scope.data.dCosto,proveedor:$scope.data.proveedor.cDescripcion};
	        	}
	        	else{
	        		datos=$scope.data;
	        	}
				catalogos.modificaCatalogo(urlCat+"/"+id,datos)
                .success(function(data){
	                	if(data.res==0){
								mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
							}
							else{
								if(data.res==1){
									$scope.registros=registros;
		                            $scope.registros[$scope.registros.indexOf($scope.backData)] = $scope.data;
		                            $scope.data = {};
		                            $scope.editar = false;
		                            $scope.busqueda = true;
		                            mensajes.alerta('CONFIRMACIÓN','SE HA MODIFICADO EXITOSAMENTE','ACEPTAR');
								}
								else{
									if(data.res==2){
										mensajes.alerta('ALERTA','EL ELEMENTO QUE DESEA AGREGAR YA SE ENCUENTRA EN EL CATÁLOGO','ACEPTAR');
									}
									else{
										mensajes.alerta('ALERTA','LA CLAVE INGRESADA YA EXISTE','ACEPTAR');
									}
								}
							}
                })*/
		    };

			$scope.deleteRow = function(registros,urlCat){
				$scope.editar = false;
				band=true;//Auxiiar para mostrar una sola vez el mensaje de "Exito a eliminar"
				$scope.registros=registros;
		 	 	$scope.selected.forEach(function(seleccion) {
			 	 	catalogos.eliminaCatalogo(urlCat+"/"+seleccion.ID+"/"+mUser)
					.success(function(data){
					  	$scope.registros.splice($scope.registros.indexOf(seleccion),1);
					  	$scope.selected = [];
		  		        $scope.busqueda = true;
		  			    $scope.alta = false;
		  			    if(band){
		  			   		 mensajes.alerta('CONFIRMACIÓN','SE HA ELIMINADO EXITOSAMENTE','ACEPTAR');
		  			   		 band=false;
		  				}
		  			})
					.error(function(data){
						mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
					});
			  	});
			};


		    $scope.loadCategorias = function() {
				$scope.catCategoria = [];
				catalogos.getCatalogos('15').success(function(data){
					$scope.catCategoria=data;

				});
		    };
		 	$scope.loadProveedor = function() {
			    $scope.catProveedor = [];
			     catalogos.getCatalogos('14').success(function(data){
					$scope.catProveedor=data;
				});
		 	 };

		 	this.primerPagina = function(){
		      $scope.query.page = 1;
		    };

		  });

