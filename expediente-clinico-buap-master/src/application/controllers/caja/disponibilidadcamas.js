(function(){
'use strict';
angular.module('expediente')
    .controller('CajaDisponibilidadcamasCtrl',['$scope','$mdDialog','$state','$localStorage','$q','catalogos','mensajes','usuario','peticiones','$window','pacientes','$location',
      function ($scope, $mdDialog, $state, $localStorage,$q,catalogos,mensajes,usuario,peticiones,$window,pacientes,$location) {
        $scope.query = {
          filter: '',
          order: 'cDescripcion',
          limit: 5,
          page: 1
        };

        $scope.onOrderChange = function(page, limit) {
          var deferred = $q.defer();
          $timeout(function () {
            deferred.resolve();
          }, 2000);

          return deferred.promise;
        };

      	function getCatalogoOcupacionCamas(){
      		var url="cuentaPaciente/ocupacionCamas";
      		peticiones.getDatos(url).then(function(data){
      			$scope.ocupacionCamas=data;
      			console.log(data);
      		}).catch(function(err){
      			console.log(err);
      		});
      	}
            function getCatalogoAreas(){
                  catalogos.getCatalogos(24)
                      .success(function(data){
                      $scope.catAreas=data; 
                      console.log($scope.catAreas);     
                  })
                      .error(function(data){
                  });
            }

      	$scope.changeArea=function(area){
      		console.log(area);
                  console.log($scope.ocupacionCamas);
                  if(area=="HOSPITALIZACION"){
                        $scope.registros=$scope.ocupacionCamas[0].HO;
                  }else{
                     if(area=="URGENCIAS")
                     $scope.registros=$scope.ocupacionCamas[1].UR; 
                  }
            console.log($scope.registros);

      	};
        getCatalogoOcupacionCamas();
        getCatalogoAreas();
 
 }]);
})();
