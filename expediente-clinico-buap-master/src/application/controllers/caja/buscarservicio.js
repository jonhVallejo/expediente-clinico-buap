
(function(){
	'use strict';

	/**
	 * @ngdoc function
	 * @name ECEangular.module('expediente').controller:CajaBuscarservicioCtrl
	 * @description
	 * # CajaBuscarservicioCtrl
	 * Controller of the expediente
	 */
	angular.module('expediente').controller('buscarServicio',['$http','$scope','$timeout', '$q', '$log','$state','cancelarServicio','mensajes','catalogos','peticiones','$location','$window','$stateParams','reportes',
	      function ($http,$scope,$timeout, $q, $log,$state,cancelarServicio,mensajes,catalogos,peticiones,$location,$window,$stateParams,reportes) {
	    	var catalogoURL = baseURL + 'cancela_serv_productos';
	      	var data;
	      	$scope.tipos = ('FOLIO-ID USUARIO').split('-').map(function (state) { return { desc: state }; });
	      	$scope.tipo = 'FOLIO';
	      	$scope.busqueda = '';
	      	$scope.tikets = [];
	      	$scope.user = '';
	      	$scope.selected = [];

	      	$scope.goCarrito = function(index){

	      		if($location.path() == "/caja/buscarServicio"){
	      			if(index.N_ESTATUS == 3){
	      				mensajes.alerta("ADVERTENCIA","ESTE TICKET YA HA SIDO CANCELADO ANTERIORMENTE","ACEPTAR");
	      				return 0;
	      			}
		      		cancelarServicio.eliminados = [];
	      			cancelarServicio.colores = [];
		      		cancelarServicio.setCompra(index);
		      		cancelarServicio.response.servicios = [];
		      		cancelarServicio.response.importe_devolver;
		      		$state.transitionTo("cancelarServicios");
		      	}else{
		      		peticiones.getMethodPDF('reporte_ReciboDePago_Reimpresion/'+index.NP_ID)
				    .success(function(data){
				        var file = new Blob([data], {type: 'application/pdf'});
				        var fileURL = URL.createObjectURL(file);
				        reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
				    })
				    .error(function(data){
				        mensajes.alerta('ERROR','NO SE HAN ENCONTRADO DATOS CON ESE FOLIO','ACEPTAR');
				    });
		      	}
	      	};


	      	$scope.getTikets = function (){
	      		switch ($scope.tipo) {
	      			case 'FOLIO':
	      				data = {"FOLIO" : $scope.busqueda};
	      			break;
	      			case 'ID USUARIO':
	      				data = {"CP_ID" : $scope.busqueda};
	      			break;
	      			default:
	    				return 0;
	      		}
	      		$scope.tikets = [];

	      		peticiones.postMethod("cancela_serv_productos",data)
				.success(function(data){
					$scope.user = $scope.busqueda;
					if(data[0] == undefined){
						if(data.PACIENTE !== undefined){
							$scope.tikets.push(data);
						}else{
							mensajes.datoNoEncontrado();
						}
					}else{
						$scope.tikets=data;	
					}

					if($scope.tikets.length==1){
						$scope.goCarrito($scope.tikets[0]);
					}
				})
				.error(function(data){
					mensajes.datoNoEncontrado();
				});
	      	};




	      	////////////////////////
		    // Acciones de tabla
		    ////////////////////////
		    $scope.query = {
			    order: 'clave_servicio',
			    limit: 5,
			    page: 1
			};

			$scope.columns = [{
			    name: 'FOLIO',
			    width : '15%',
			    orderBy : 'NP_ID'
			}, {
			    name: 'ID PACIENTE',
			    width : '15%'
			}, {
				name: 'PACIENTE',
			    orderBy: 'PACIENTE',
			    width : '50%'
			}, {
				descendFirst: true,
				name: 'FECHA',
			    orderBy: 'T_FECHA_CMD',
			    width : '20%'
			}];

			$scope.onOrderChange = function(page, limit) {
			    var deferred = $q.defer();
					    
			    $timeout(function () {
			    	deferred.resolve();
			    }, 2000);
					    
			    return deferred.promise;
			};
	  }]);

})();
