
(function(){
'use strict';

angular.module('expediente')
    .controller('CajaCorrespondenciacausesserviciosCtrl',['$scope','$mdDialog','$state','$localStorage','$q','catalogos','mensajes','usuario','peticiones','$window','pacientes','$location',
      function ($scope, $mdDialog, $state, $localStorage,$q,catalogos,mensajes,usuario,peticiones,$window,pacientes,$location) {
        $scope.listaConvenios=[];

        function inicializarVariables(){
	        $scope.listaHU = [];
	      	$scope.listaConvenios = [];//LISTA CONVENIOS
	        $scope.tablaConvenios = [];
	      	$scope.catConvenios = [];//CATEGORÍAS
	      	$scope.catCauses = [];
	     // 	$scope.listCausesCategoria = [];//LISTA DE CONVENIOS EN BASE A LA CATEGORÍA
	      	$scope.convenios = [];//lista de convenios según el tipo
	        $scope.match = [];
	        $scope.selectedMatch=[];
	        $scope.selectedConvenios=[];
	        $scope.selectedHU=[];
	        $scope.showConvenios=false;
	        $scope.showlistaConvenios=false;
	        $scope.showServicios=false;

        };

      	function getCatalogoConvenios(){//CATEGORÍAS
      		$scope.catConvenios = [];
			     catalogos.getCatalogos('39').success(function(data){
					$scope.catConvenios=data;
			        getCatalogoCauses();
			     });
      	};
      	function getCatalogoCauses(){  //CONVENIOS
      		$scope.catCauses = [];
			     catalogos.getCatalogos('38').success(function(data){
					$scope.catCauses=data;
			     });
      	};

      	function getDatosListaHU(){//SERVICIOS Y PRDUCTOS (LISTA HU)
	      var url = "/cuentaPaciente/"+$localStorage.paciente.NP_CUENTA;
	      peticiones.getDatos(url)
	        .then(function(data) {
	            var productosServicios = _.map(data.PRODUCTOS_SERVICIOS, function (el, key) {
                    return{
                    	'NP_PAGO' : el.NP_PAGO,
                    	'NP_SERVICIO_PRODUCTO' : el.NP_PAGO_SERV,
                    	'NP_TMEDICAMENTO_RECETA' : 0,
                    	'NP_ALM_I_CUENTA_ARTICULO' :0,
                    	'C_DESCRIPCION':el.C_DESCRIPCION
                    }
                });
                var medicamentosReceta = _.map(data.TMEDICAMENTOS_RECETA, function (el, key) {
                    return{
                    	'NP_PAGO' : 0,
                    	'NP_SERVICIO_PRODUCTO' : 0,
                    	'NP_TMEDICAMENTO_RECETA' : el.NP_TMEDICAMENTO_RECETA,
                    	'NP_ALM_I_CUENTA_ARTICULO' : 0,
                    	'C_DESCRIPCION':el.C_NOMBRE_MEDICAMENTO
                    }
                });
              $scope.listaHU = productosServicios.concat(medicamentosReceta);
	        })
	        .catch(function(err) {
	          console.log(err);
	        });
        };

;

      	function cargarDatos(){
      		inicializarVariables();
      		getCatalogoConvenios();
      		getCatalogoCauses();
      		getDatosListaHU();
      	}

      	cargarDatos();
      	/*************************************************************************************/
      	$scope.changeCategoriaConvenio=function(idConvenio){//Seleccionar la categoría y mostar los servicios que le correspondan
      		$scope.listaConvenios=[];
      		$scope.showlistaConvenios=true;
  			$scope.listaConvenios = _.filter($scope.catCauses, function(e){
				return	e.idCategoria=idConvenio;
    		});
      	};

      	$scope.changeSelectConvenio=function(){
      		 $scope.showConvenios=true;
      	};
      	/**************************VALIDACIONES PARA VER SI ES UN POST O ES UN UPDATE*****************************************************/

      	/**************************FUNCIONES DEL MATCH*****************************************************/
        function getJson(conveniosMatch){
      		var aux=[];
            for(var x in conveniosMatch){
            	 for(var y in conveniosMatch[x].SERV_MED_ALM){
            	    aux.push({C_DESCRIPCION:conveniosMatch[x].C_DESCRIPCION, NP_ID:conveniosMatch[x].NP_ID ,NP_ID_CAUSE:conveniosMatch[x].NP_ID_CAUSE,C_DESCRIPCION_PRODUCTO:conveniosMatch[x].SERV_MED_ALM[y].NP_SERVICIO_PRODUCTO}); 
            	 }
            }
		};

	    function armarJson(conveniosMatch){
	    	var servicios;
			var datosEnviar=[];
			var convenios;
	            for(var x in conveniosMatch){

	                convenios = _.filter($scope.match , function(e){
        					return e.C_DESCRIPCION== conveniosMatch[x];
        			});
	                servicios = _.map(convenios, function (el, key) {
	                	return{
	                    	'NP_PAGO' : el.NP_PAGO,
	                    	"NP_SERVICIO_PRODUCTO":el.NP_SERVICIO_PRODUCTO,
	                    	"NP_TMEDICAMENTO_RECETA":el.NP_TMEDICAMENTO_RECETA,
	                    	"NP_ALM_I_CUENTA_ARTICULO":el.NP_ALM_I_CUENTA_ARTICULO,	                	
	                    }
                    });
                    datosEnviar.push({NP_ID_CAUSE:convenios[0].NP_ID,NP_ID:0,C_DESCRIPCION:convenios[0].C_DESCRIPCION, SERV_MED_ALM:servicios}); 
	            }
	            console.debug(datosEnviar);
	            getJson(datosEnviar);
	          //  post({MATCH_CAUSE_PRODUCTOS:datosEnviar});
		}

      	$scope.realizarMatch=function(){
      		if($scope.selectedConvenios.length==1){
      			//eliminarlo de la lista de convenios
      			$scope.selectedConvenios.forEach(function(selectedConvenios){
  		            $scope.listaConvenios.splice($scope.listaConvenios.indexOf(selectedConvenios.cClave),1);
	  		    });
	  		    $scope.selectedHU.forEach(function(selectedHU){
  		            $scope.listaHU.splice($scope.listaHU.indexOf(selectedHU),1);
	  		    });

      			for(var x in  $scope.selectedHU){
	       		    $scope.match.push({
		       		    				  NP_ID:$scope.selectedConvenios[0].NP_ID,
		       		    				  NP_ID_CAUSE:$scope.selectedConvenios[0].NP_ID_CAUSE,
		       		    				  C_DESCRIPCION:$scope.selectedConvenios[0].C_DESCRIPCION,
		       		    				  NP_PAGO:$scope.selectedHU[x].NP_PAGO,
		       		    				  NP_SERVICIO_PRODUCTO:$scope.selectedHU[x].NP_SERVICIO_PRODUCTO,
		       		    				  NP_TMEDICAMENTO_RECETA:$scope.selectedHU[x].NP_TMEDICAMENTO_RECETA,
		       		    				  NP_ALM_I_CUENTA_ARTICULO:$scope.selectedHU[x].NP_ALM_I_CUENTA_ARTICULO,
		       		    				  C_DESCRIPCION_PRODUCTO:$scope.selectedHU[x].C_DESCRIPCION
	       		    				  }); 
	            }
	          

	            $scope.selectedConvenios=[];
	            $scope.selectedHU=[];
	            $scope.tablaConvenios = [];
	            $scope.showConvenios=false;

      		}else{
      			mensajes.alerta('ALERTA','SELECCIONE UNICAMENTE UN CONVENIO','ACEPTAR');
      		}
      	};

      	function post(datos){
      		var url="cuasesServicios/";
      		peticiones.postDatos(url,datos)
	        .then(function(data) {
	           console.log(data);
	        })
	        .catch(function(err) {
	          console.log(err);
	        });
      	};
      	function getMatch(){

      	}

      	

      	$scope.guardarMatch=function(registro){
      		  var conveniosMatch=_($scope.match).chain().flatten().pluck('C_DESCRIPCION').unique().value();
	          armarJson(conveniosMatch)

      	};

      	$scope.eliminarMatch=function(registro){
      		//agregar el convenio
      		 $scope.listaConvenios.push({NOMBRE:$scope.selectedMatch[0].CATEGORIA, CLAVE:$scope.selectedMatch[0].CLAVE2});
      		//agregar los servicios
      		  for(var x in  $scope.selectedMatch){
	       		    $scope.listaHU.push({NOMBRE:$scope.selectedMatch[x].NOMBRE, CLAVE:$scope.selectedMatch[x].CLAVE} ); 
	            }
	           $scope.selectedMatch.forEach(function(selectedMatch){
  		            $scope.match.splice($scope.match.indexOf(selectedMatch),1);
	  		    });
      	};


		$scope.deleteRow = function(){
  		 	$scope.selected.forEach(function(selectedMatch) {
  		    $scope.registros.splice($scope.registros.indexOf(selectedMatch),1);
  		    //ELIMINAR CON EL SERVICIO
	 	 // 	catalogos.eliminaCatalogo(urlCat+"/"+seleccion.ID+"/"+mUser)
				// .success(function(data){
				// 	$scope.registros.splice($scope.registros.indexOf(seleccion),1);
				//   	$scope.selected = [];
	  	// 	        $scope.busqueda = true;
	  	// 		    $scope.alta = false;
	  	// 		    if(band){
	  	// 		   		 mensajes.alerta('CONFIRMACIÓN','SE HA ELIMINADO EXITOSAMENTE','ACEPTAR');
	  	// 		   		 band=false;
	  	// 		   		  if(urlCat==23){
	   //                      	catalogos.getCatalogos('camas/1').success(function(data){
				// 	       		$scope.catAreas = data;
				//    				});
	   //                      }
	  	// 			}
	  	// 		})
				// .error(function(data){
				// 	if(data.res !=0 ) 
				// 		mensajes.alerta('ALERTA','EL REGISTRO ESTA ASOCIADO A INFORMACIÓN VIGENTE','ACEPTAR');
				// 	else
				// 		mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
				// 	$scope.selected = [];
				// });
				//CARGAR LISTA HU Y LISTA DE CONVENIOS NUEVAMENTE
	  		});
		};
		/*************************FUNCIONES PARA BUSCAR CONVENIOS*************************************************/
		$scope.querySearch = function(texto) {
		    var defered = $q.defer();
		    var promise = defered.promise;
		  	var arraySearch = $scope.getConvenio($scope.listaConvenios);
		  	defered.resolve(arraySearch);
		  	return promise;
		};

		$scope.getConvenio= function(data){
		    if(data!==undefined){
		      var a = data.map(function (serv){
		        return {
			          display: (serv.cDescripcion + " "+ serv.cClave),
			          C_DESCRIPCION:(serv.cDescripcion ),
			          NP_ID:(serv.ID ),
			          NP_ID_CAUSE:0,
			          cClave:(serv.cClave)
		        };
		      });
		      return a;
		    }
		};

		$scope.addReg = function(clave,nombre,id,idCause){
			$scope.tablaConvenios .push({cClave: clave,C_DESCRIPCION: nombre,NP_ID:id,NP_ID_CAUSE:idCause}); 
			$scope.showConvenios=true;
			$scope.showServicios=true;
	    };

		$scope.selectedItemChange = function(item,tipo) {
		    if(item!==undefined){
		    	if(existMatch(item.NP_ID)){
                    mensajes.alerta('ALERTA','EL CONVENIO YA EXISTE SELECCIONE OTRO','ACEPTAR');
		    	}else{
		    		$scope.addReg(item.cClave,item.C_DESCRIPCION,item.NP_ID,item.NP_ID_CAUSE);
		    	    $scope.searchText = "";
		    	}
		    }
		};

		function existMatch(element){
           var existe = _.filter($scope.match , function(e){
    					return e.NP_ID== element;
    	   });
			if (existe.length)
				return true;
			else
				return false;
		}


      	/***************************FUNCIONES PARA LA TABLA*********************************/
      	$scope.query = {
		    filter: '',
		    order: 'F_FECHA_INICIAL',
		    limit: 5,
		    page: 1
		};
		$scope.queryDos = {
		    filter: '',
		    order: 'F_FECHA_INICIAL',
		    limit: 5,
		    page: 1
		 };
		$scope.queryTres = {
		    filter: '',
		    order: 'F_FECHA_INICIAL',
		    limit: 5,
		    page: 1
		 };

		$scope.onOrderChange = function(page, limit){
	        var deferred = $q.defer();
	        $timeout(function () {
	          deferred.resolve();
			        }, 2000);
			    return deferred.promise
		};

		$scope.onPaginationChange = function (page, limit){
		      if(typeof success !== 'undefined')
		        return $scope.datos.get($scope.query, success).$promise;
		};


 }]);
})();
