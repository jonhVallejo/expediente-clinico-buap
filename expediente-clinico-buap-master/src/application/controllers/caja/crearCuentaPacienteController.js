(function(){
'use strict';


angular.module('expediente')
    .controller('crearCuentaPaciente',['$scope','$timeout','$state','peticiones','pacientes','mensajes','catalogos','blockUI','cuentaPacienteService','$localStorage',
      function ($scope,$timeout,$state,peticiones,pacientes,mensajes,catalogos,blockUI,cuentaPacienteService,$localStorage) {

    $scope.title="CREAR CUENTA PACIENTE";
    var self = this;
    self.camaAsignada = false;
    self.subArea=false;
    self.camaDescripcion = "";
    blockUI.start();

    this.getPaciente = function(){
      pacientes.getPaciente()
      .success(function(data){
        if(!data.data.hasOwnProperty('NP_ID')){
          $scope.paciente=data.data[0];
          self.derechoHabiente = !verificaNP_ID($scope.paciente.NP_ID);
          self.getCuentasPaciente($scope.paciente.NP_EXPEDIENTE.substring(0,$scope.paciente.NP_EXPEDIENTE.indexOf("/",0)));
        }
        else{
        //  mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
        }
      })
      .error(function(data){
        blockUI.stop();
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      });
    };

    self.getCuentasPaciente = function(NP_EXPEDIENTE){
      peticiones.getDatos("cuentaPaciente/activa/" + NP_EXPEDIENTE)
        .then(function(data) {
          console.debug(data);
          if($scope.tCuentaPaciente = data.T_CUENTA_PACIENTE){
            mensajes.mConfirmacion("","YA EXISTE UNA CUENTA ABIERTA PARA ESTE PACIENTE, ¿DESEA IR A LA CUENTA ACTIVA?","NO","SI")
              .then(function() {
                  //Se manda a guardar los dato
                  //cuentaPacienteService.fillCuentaPaciente(data);
                  $localStorage.cuentaAux = data;
                  $localStorage.paciente.cuentaPaciente = {};
                  $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE = data.T_CUENTA_PACIENTE.NP_ID;
                  $state.go("detalleCuentaPacienteEspecifica");
              }).catch(function() {
                  $state.go("bienvenido");
              });
          }

            console.debug(data);
        })
        .catch(function(err) {
             console.log(err);
        })
        .finally(function() {
              blockUI.stop();

         });
     }

    var verificaNP_ID = function(paciente_id){
        switch($scope.paciente.C_CLAVE_TIPO_PACIENTE) {
          case 'D':
              return true;
              break;
          case 'B':
              return true;
              break;
          default:
              return false;
          }
      }

    catalogos.getCatalogos('camas/1').success(function(data){
        self.tipoCama = data;
    });
     catalogos.getCatalogos('39').success(function(data){
        self.tipoCauses= data;
    });

    catalogos.getCatalogos(17).success(function(data){
          self.tipoParentesco = data;
          console.log(self.tipoParentesco);
    });

    self.crearCuentaPaciente = function(){
      var mData  = {};
      var mTipoArea = {};
      mData.NP_PERSONAL = $localStorage.usuario.NP_EXPEDIENTE;
      mData.CP_ID = $scope.paciente.NP_ID;
   

      if (verificaNP_ID($scope.paciente.NP_ID)){
         guardarDatos(mData);
      }else{
          if(self.seguroPopular == true){
                if(typeof self.seguroPopular == 'undefined' | typeof self.apoyoEstimado == 'undefined' | typeof self.idSeguroPopular == 'undefined' | typeof self.polizaSeguro == 'undefined' ){
                //entra si es que tiene seguro popular
                mensajes.alerta('','POR FAVOR INGRESA LOS DATOS DEL SEGURO POPULAR','OK');
                return;
              }else{
                mData.SEGURO_POPULAR = true;
                mData.PARENTESCO     = self.tipoParentesco[self.parentesco].ID;
                mData.APOYO_ESTIMADO = self.apoyoEstimado;
                mData.ID_SEGURO      = self.idSeguroPopular;
                mData.POLIZA_SEGURO  = self.polizaSeguro;
                mData.ID_TIPO_CAUSE  = self.cause;
                mData.C_AUTORIZACION  =self.autorizacionPaciente;
                mData.C_OBSERVACIONES  =self.C_OBSERVACIONES;
              }
          }

          mData.D_IMPORTE = (typeof self.anticipoPaciente == 'undefined') ? 0 : self.anticipoPaciente;
          mData.SEGURO_POPULAR = false; //IMPORTANTE, ESOT SE VA A QUITAR PERO CREO QUE ES REQUERIDO POR EL SERVICIO

          if(self.solicitaAutorizacion == true){

            if(typeof self.autorizacionPaciente == 'undefined'){
                mensajes.alerta('','DEBE INGRESAR UN FOLIO DE AUTORIZACIÓN','OK');
                return;
            }
            mData.C_AUTORIZACION = self.autorizacionPaciente;
          }
          guardarDatos(mData);
      }
    }



    var guardarDatos = function(mData){
     console.log($localStorage);
      peticiones.postMethod('cuentaPaciente/', mData)
      .success(function(data){
            $localStorage.paciente.cuentaPaciente={};
            $localStorage.paciente.cuentaPaciente.TIPO= (mData.SEGURO_POPULAR)?"B_SEGURO_POPULAR":"";
            $localStorage.paciente.cuentaPaciente.total=0;
            $localStorage.paciente.cuentaPaciente.abonos=0;
            $localStorage.paciente.cuentaPaciente.restantePagar=0;
            $localStorage.paciente.cuentaPaciente.saldoAFavor=0;
            $localStorage.paciente.cuentaPaciente.NF_ESTATUS=data.hasOwnProperty('NP_CUENTA_PACIENTE_ACTIVA')? true:false;
            $localStorage.paciente.cuentaPaciente.autorizacion=self.autorizacionPaciente;
            $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE=data.T_CUENTA_PACIENTE.NP_ID;
            $scope.dataCuentaPaciente=undefined;
               if($localStorage.paciente.DERECHO_HABIENTE==true){
                  mensajes.mConfirmacionValida("", "LA CUENTA HA SIDO CREADA EXITOSAMENTE","Ok")
                  .then(function() {
                    //  console.log(data);
                    // $state.go("bienvenido");
                    $localStorage.cuentaAux = data;
                    $localStorage.paciente.cuentaPaciente = {};
                    $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE = data.T_CUENTA_PACIENTE.NP_ID;
                    $state.go("detalleCuentaPacienteEspecifica");
                  }).catch(function(error) {
                  });
               }else{
                  if(self.autorizacionPaciente==true || self.seguroPopular==true){
                    mensajes.mConfirmacion("CONFIRMACIÓN","LA CUENTA HA SIDO CREADA EXITOSAMENTE, DESEA REALIZAR ABONOS","NO","SI")
                    .then(function() {
                        $state.go("realizarPagoCuentaPaciente");
                    }).catch(function() {
                        $localStorage.cuentaAux = data;
                        $localStorage.paciente.cuentaPaciente = {};
                        $localStorage.paciente.cuentaPaciente.NP_CUENTA_PACIENTE = data.T_CUENTA_PACIENTE.NP_ID;
                        $state.go("detalleCuentaPacienteEspecifica");
                    });
                  }else{
                      mensajes.mConfirmacionValida("CONFIRMACIÓN","LA CUENTA HA SIDO CREADA EXITOSAMENTE, INGRESE DATOS PARA ABONAR","CANCELAR","ACEPTAR")
                      .then(function() {
                          $state.go("realizarPagoCuentaPaciente");
                      }).catch(function() {
                           $state.go("realizarPagoCuentaPaciente");
                      });
                  }
              }         
      })
      .error(function(data){
         mensajes.alerta('ERROR','Ocurrio un error en el sistema, por favor contacte al administrador','OK');
      });
    }

    this.getPaciente();

    }]);
})();
