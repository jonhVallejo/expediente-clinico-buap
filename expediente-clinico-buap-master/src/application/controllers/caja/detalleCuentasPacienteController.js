(function(){
  'use strict';
angular.module('expediente')
    .controller('detalleCuentasPacienteController',['$scope','$q', '$mdDialog','$http','$state','pacientes','peticiones','$localStorage','cuentaPacienteService',
      function ($scope,$q, $mdDialog,$http,$state,pacientes,peticiones,$localStorage,cuentaPacienteService) {
        var self = this;
        var url = "cuentaPaciente/historial/";
        $scope.tCuentaPaciente = [];


        self.getPaciente = function(){
          pacientes.getPaciente()
          .success(function(data){
            console.debug(data);
            if(!data.data.hasOwnProperty('NP_ID')){
              $scope.paciente=data.data[0];
              console.log($scope.paciente);
              self.getCuentasPaciente($scope.paciente.NP_EXPEDIENTE.substring(0,$scope.paciente.NP_EXPEDIENTE.indexOf("/",0)));
             // self.derechoHabiente = !verificaNP_ID($scope.paciente.NP_ID);
            }
            else{
            //  mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
            }
          })
          .error(function(data){
            mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
          });
        };

        self.getCuentasPaciente = function(NP_EXPEDIENTE){
            peticiones.getDatos(url + NP_EXPEDIENTE)
              .then(function(data) {
                console.log(data.T_CUENTA_PACIENTE);
                  $scope.tCuentaPaciente = data.T_CUENTA_PACIENTE;
                  data.T_PACIENTE["NP_EXPEDIENTE"]=NP_EXPEDIENTE;
                  console.log(data);
                  cuentaPacienteService.setNPCuentaPaciente(data);
              })
              .catch(function(err) {
                  // Tratar el error
              });
        };
        self.verDetalleCuentaPaciente = function(ev,dato){
          cuentaPacienteService.setNPCuentaPaciente(dato);
            $state.go("detalleCuentaPacienteEspecifica");
        };
        self.getPaciente();
    }]);
})();
