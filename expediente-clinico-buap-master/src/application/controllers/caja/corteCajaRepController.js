(function(){
'use strict';



/**
 * [Controlador encargado de mostrar los diferentes reportes que se pueden solicitar,
 * corte de caja, corte de turno]
 * @param  {[type]} $state     [description]
 * @param  {[type]} $scope     [description]
 * @param  {[type]} $q         [description]
 * @param  {[type]} $log       [description]
 * @param  {[type]} $mdDialog  [description]
 * @param  {[type]} catalogos) {               var self [description]
 * @return {[type]}            [description]
 */
  angular.module('expediente').controller('reportesDeCaja',['$state','$scope','$q', '$log','mensajes','catalogos','corteCajaRepService','$window','blockUI','usuario','peticiones','reportes',
    function ($state,$scope, $q, $log,mensajes,catalogos,corteCajaRepService,$window,blockUI,usuario,peticiones,reportes) {



      blockUI.start();
      $scope.busqueda      = '';
      $scope.simulateQuery = false;
      $scope.isDisabled    = false;
      $scope.seleccionado  = false;
      $scope.imprimir      = true;
      $scope.mDatos        = {caja : '',categorias : '',turno: '',fIni: '',fFin: '',usr:''};
      $scope.query         = {filter: '',order: 'cDescripcion',limit: 5,page: 1};

      var textoBuscado     = '';
      var self             = this;
      self.data = {};
      var mUser =  usuario.usuario;


      $scope.mDataVisibles = {
          numero : true,
          folio : true,
          id : true,
          paciente : true,
          cantidad : true,
          clave :true,
          descripcion : true,
          p_unitario : true,
          condonado : false,
          cancelado : false,
          convenios : false,
          t_credito : false,
          t_debito : false,
          efectivo : false,
          t_electronica : false,
          creditos : false,
          monto : true,
          caja : false,
          turno : false,
          usr : false,
          fecha : false
      };



/*
    Función que va a retornar todos los datos para mostrar los filtros
*/
      catalogos.getCatalogos('cajas/').success(function(data){
          self.cajas = data;
          setUsuarios(self.cajas);
      }).error(function(data){
          console.log(data);
      });


    //Recupera Categorias
      catalogos.getCatalogos(15).success(function(data){
          self.categorias = data;
      }).error(function(data){
          console.log(data);
      });


    catalogos.getCatalogos(21).success(function(data){
          $scope.areas=data;
      }).error(function(data){
          console.log(data);
      });

    $scope.limpiar = function(){
        $scope.seleccionado = false;
        $scope.imprimir = true;
        self.data = {};
    };


   function formarDatos(){
        if(typeof self.fIni != 'undefined'){
            var a=moment(self.fIni).format('L').split('/');
            $scope.mDatos.fIni = a[0]+a[1]+a[2];
        }

        if(typeof self.fFin != 'undefined'){
            var b=moment(self.fFin).format('L').split('/');
            $scope.mDatos.fFin = b[0]+b[1]+b[2];
        }

        if( typeof self.mCategoria != 'undefined'){
             $scope.mDatos.categorias = String(self.categorias[self.mCategoria].ID);
        }

        if( typeof self.mCaja != 'undefined'){
             $scope.mDatos.caja = String(self.cajas[self.mCaja].ID);
        }

        if(typeof self.turno != 'undefined' ){
           $scope.mDatos.turno = String(self.cajas[parseInt(self.mCaja)].turnos[parseInt(self.turno)].ID);
        }

        if(typeof self.cajero != 'undefined' ){
           $scope.mDatos.usr = String(self.cajas[parseInt(self.mCaja)].cajeros[parseInt(self.cajero)].npExpediente);
        }

        if(typeof self.fIni == 'undefined' | typeof self.fFin == 'undefined'){
            mensajes.alerta("ERROR","LOS CAMPOS DE FECHA SON OBLIGATORIOS","OK");
            return false;
        }else{
           return true;
        }

    };

    $scope.buscarReporteDeVentas = function(){
        if(formarDatos()){
            corteCajaRepService.getDatosRepVentas($scope.mDatos)
              .success(function(data){
                  self.reporteDeVentas = data;

                  $scope.seleccionado = true;
                  $scope.imprimir = false;
              }).error(function(data){
                  console.log(data);
            });
        }
    };

    $scope.imprimirReporteDeVentas = function(){
        formarDatos();
        corteCajaRepService.getDatosRepVentasPDF($scope.mDatos)
          .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          }).error(function(data){console.log(fileURL);
              console.log(data);
          });

    };

    $scope.buscarReporte = function(){
       if(formarDatos()){
          corteCajaRepService.getDatosCorteCaja($scope.mDatos)
            .success(function(data){
                self.reporteCorteDeCaja = data;
                $scope.seleccionado = true;
                $scope.imprimir = false;
            }).error(function(data){
                console.log(data);
            });
        }
    };


    $scope.imprimirReporte = function(){
        formarDatos();
        corteCajaRepService.getDatosCorteCajaPDF($scope.mDatos)
          .success(function(data){
              var file = new Blob([data], {type: 'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
          }).error(function(data){console.log(fileURL);
              console.log(data);
          });

    };


   //Funciones para Reporte De Servicios
       $scope.buscarReporteDeServicios = function(){
          if(formarDatos()){
              corteCajaRepService.getDatosRepServicios($scope.mDatos)
                .success(function(data){
                    self.data = data;
                    $scope.seleccionado = true;
                    $scope.imprimir = false;
                }).error(function(data){
                    console.log(data);
              });
            }
        };


    $scope.imprimirReporteDeServicios = function(){
      if(formarDatos()){
           corteCajaRepService.getDatosRepServiciosPDF($scope.mDatos)
             .success(function(data){
                  var file = new Blob([data], {type: 'application/pdf'});
                  var fileURL = URL.createObjectURL(file);
                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
            }).error(function(data){console.log(fileURL);
                  console.log(data);

            });
      }
    };

    //Funciones para Reporte De Concentrado De Ventas
       $scope.buscarReporteDeConcentradoVentas = function(){
            if(formarDatos()){
               corteCajaRepService.getDatosRepConcentradoVentas($scope.mDatos)
                 .success(function(data){
                      self.reporteConventradoVentas = data;
                      $scope.seleccionado = true;
                      $scope.imprimir = false;
                  }).error(function(data){
                      console.log(data);
                  });
            }
        };

       $scope.imprimirReporteDeConcentradoVentas = function(){
           formarDatos();
           corteCajaRepService.getDatosRepConcentradoVentasPDF($scope.mDatos)
             .success(function(data){
                  var file = new Blob([data], {type: 'application/pdf'});
                  var fileURL = URL.createObjectURL(file);
                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
              }).error(function(data){console.log(fileURL);
                  console.log(data);
              });
        };

        var setUsuarios = function(data){
            var aux = null;
            for(var i=0 ; i<data.length ; i++){
                if(data[i].cDescripcion.toUpperCase() === 'TODAS'){
                   aux = data[i].cajeros;
                }
            }
            if(aux!=null){
              _.each(data,function(value){
                _.each(value.cajeros,function(cajero){
                  if(_.indexOf(aux,cajero) === -1){
                    aux.push(angular.extend({},cajero) );
                  }
                });
              });
            }
            blockUI.stop();
          };
          var calcularValores = function(){
          var ingresoTotal = 0;
        };

    /*******************************************
    ****** REPORTE DE VALES CAJA
    *******************************************/
        $scope.imprimirReporteDeValesCaja = function(){
            console.log($scope.datos.caja);
        };
    /*******************************************
    ****** CORTE DE CAJA
    *******************************************/

    var identificarOpcion = function(){
      var NP_CAJA = "2";
           if(usuario.perfil.id==3)
              NP_CAJA=2;
            else
              NP_CAJA=1;
      if ($state.current.name == "corteDeCaja"){
          var url_reporteVentas = "cajacorte/arqueoDatos/ventas/" + NP_CAJA + "/" + usuario.usuario.NP_EXPEDIENTE;
          var url_reporteConventradoVentas = "cajacorte/arqueoDatos/concentradoVentas/" + NP_CAJA + "/" + usuario.usuario.NP_EXPEDIENTE;
          var url_reporteCorteCaja = "cajacorte/arqueoDatos/" + NP_CAJA + "/" + usuario.usuario.NP_EXPEDIENTE;

          peticiones.getDatosStatus(url_reporteVentas).then(function(data)
            {
              if(data.status == 200){
                  self.reporteDeVentas = data.data;
              }else{
                 if(data.status == 204){
                    self.reporteConventradoVentas = {jsonReporteConcentradoDeVentas:[]};
                }
              }
            }).catch(function(err)
            {
                self.reporteConventradoVentas = {jsonReporteConcentradoDeVentas:[]};
                console.log(err);
            });


            peticiones.getDatosStatus(url_reporteConventradoVentas).then(function(data)
            {
              if(data.status == 200){
                  self.reporteConventradoVentas = data.data;
              }else{
                 if(data.status == 204){
                    self.reporteDeVentas = {jsonReporteConcentradoDeVentas :[]};
                }
              }
            }).catch(function(err)
            {
                self.reporteDeVentas = {jsonReporteConcentradoDeVentas :[]};
                console.log(err);
            });


           peticiones.getDatosStatus(url_reporteCorteCaja).then(function(data)
            {
              if(data.status == 200){
                self.reporteCorteDeCaja = data.data;
              }

            }).catch(function(err)
            {
                console.log(err);
            });
      }
    }

    identificarOpcion();






        $scope.imprimirCorteCaja = function(){
          //si idPerfil=3 idCaja=2 else idCaja=1;
           var url="2";
           if(usuario.perfil.id==3)
              url=2;
            else
              url=1;

          url+="/"+usuario.usuario.NP_EXPEDIENTE;

          mensajes.mConfirmacion("PRECAUCIÓN",
                                            "SI REALIZA UN CORTE DE CAJA YA NO PODRA REALIZAR NINGUNA ACCIÓN EN EL SISTEMA Y SU SESIÓN SERÁ CERRADA AUTOMATICAMENTE. ¿DESEA CONTINUAR?",
                                            "NO","SI")
                      .then(function() {
                         imprimirRepCorteCajaVentas(url);
                      }).catch(function(data) {
                          console.error('No realizar corte de caja');
                      });




        };
        function imprimirRepCorteCajaVentas(url){
            corteCajaRepService.getDatosRepCorteCajaVentas(url)
             .success(function(data){
                  var file = new Blob([data], {type: 'application/pdf'});
                  var fileURL = URL.createObjectURL(file);
                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
                  imprimirRepCorteCajaConcentrado(url);
              }).error(function(data){
                  console.log(data);
                  mensajes.alerta('ERROR','LO SENTIMOS, OCURRIÓ UN PROBLEMA','ACEPTAR');
              });

        };
        function imprimirRepCorteCajaConcentrado (url){
             corteCajaRepService.getDatosRepCorteCajaConcentrado(url)
             .success(function(data){
                  // var file = new Blob([data], {type: 'application/pdf'});
                  // var fileURL = URL.createObjectURL(file);
                  // reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
                  imprimirRepCorteCajaPDF(url);
              }).error(function(data){
                  console.log(data);
                  mensajes.alerta('ERROR','LO SENTIMOS, OCURRIÓ UN PROBLEMA','ACEPTAR');
              });

        };
        function imprimirRepCorteCajaPDF(url){
            corteCajaRepService.getDatosRepCorteCajaPDF(url)
             .success(function(data){
                  var file = new Blob([data], {type: 'application/pdf'});
                  var fileURL = URL.createObjectURL(file);
                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
                  usuario.removeLoginInformation();
                  $state.transitionTo('home');
              }).error(function(data){
                  console.log(data);
                  mensajes.alerta('ERROR','LO SENTIMOS, OCURRIÓ UN PROBLEMA','ACEPTAR');
              });

        };

    }]);

})();
