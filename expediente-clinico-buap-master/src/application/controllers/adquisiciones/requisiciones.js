
/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AdquisicionesRequisicionesCtrl
 * @description
 * # AdquisicionesRequisicionesCtrl
 * Controller of the expediente
 */

(function(){
'use strict';
angular.module('expediente')
    .controller('AdquisicionesRequisicionesCtrl',['$scope','usuario','$state','mensajes','$mdDialog','$q', '$http', 'reportes',
    function ($scope,usuario,$state,mensajes,$mdDialog,$q, $http,reportes) {
      $scope.showRequisicion=true;
      $scope.requisiciones=[];

      function cargarDatos(){

      };
    
      $scope.crearRequisicion=function(tipo){
        //tipo=0 REQUISICION  //tipo=1 REQUISICION ESPECIAL
        if(tipo==0){
           cargarDatos();
           $scope.showRequisicion=true;
        }
        if(tipo==1){
           $scope.showRequisicion=false;
         }
      };
      
      $scope.guardar=function(){

      };

      $scope.addReg = function() {
         $scope.requisiciones.push({CANTIDAD: undefined, CLAVE: undefined, UNIDAD: undefined, DESCRIPCION:undefined}); 
      };
      
      $scope.eliminar=function(){

      };

      $scope.imprimir=function(){
        var url="";
         peticiones.getMethodPDF(url)
           .success(function(data){
                var file = new Blob([data], {type: 'application/pdf'});
                var fileURL = URL.createObjectURL(file);
                reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
            }).error(function(data){
                console.log(data);
            });
      };
    

    }]);
})();

