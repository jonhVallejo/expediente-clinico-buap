/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AdquisicionesAdquisicionesCtrl
 * @description
 * # AdquisicionesAdquisicionesCtrl
 * Controller of the expediente
 */


'use strict';
angular.module('expediente')
.controller('AdquisicionesAdquisicionesCtrl', function ($scope,usuario,adquisiciones,$q,$timeout,$location) {



	    $scope.grupos=undefined;



	    $scope.data={};



	    $scope.change = function(){
	          $scope.query.page = 1;
	    };

	    /******************************FUNCIONES CORRESPONDIENTES A GRUPOS*****************************/
	    function cargarDatosGrupos(){
	    	console.log("cargando grupos");
	    	var url= "grupo/cuenta/";
	        adquisiciones.getDatos(url)
	          .then(function(data) {
	          	console.log("grupo cuenta");
	          	console.log(data);
	            $scope.grupos=data.data;
	            console.log(data.data);
	          })
	          .catch(function(err) {
	            console.log(err);
	          });

	        var url= "cuenta/abono/";
	        adquisiciones.getDatos(url)
	          .then(function(data) {
	            $scope.catCuentaAbono=[data.data];
	            console.log(data.data);
	          })
	          .catch(function(err) {
	            console.log(err);
	          });

	        var url= "cuenta/cargo/";
	        adquisiciones.getDatos(url)
	          .then(function(data) {
	            $scope.catCuentaCargo=[data.data];
	            console.log(data.data);
	          })
	          .catch(function(err) {
	            console.log(err);
	          });

	    };
	    /********************************FUNCIONES CORRESPONDIENTES A LOS ARTÍCULOS*********************************/
	    function cargarDatosArticulos(){

	    	console.log("cargando articulos");
	    	$scope.catMaterialAseo=[
		        {cDescripcion: 'ASEO 1',ID:0},
		        {cDescripcion: 'ASEO 2', ID:1}
		        ];
		    $scope.catUnidadArticulos=[
		        {cDescripcion: 'ACTIVO',ID:0},
		        {cDescripcion: 'INACTIVO', ID:1}
		        ];
		    $scope.catMaterialAseo=[
		        {cDescripcion: 'MATERIAL 1',ID:0},
		        {cDescripcion: 'MATERIAL 2', ID:1}
		    ];
	    };
	     function cargaEntradas(){
	     		$scope.entradas=[
		        {cClave: '30413',cDescripcion: 'ASSAY CUP ELECSYS 10 10',ivaPrecio:0,importe:0,caducidad:'10-02-03'},
		       {cClave: '30413',cDescripcion: 'ASSAY CUP ELECSYS 10 10',ivaPrecio:0,importe:0,caducidad:'10-02-03'},
		        ];

	    };

	    /**********************FUNCIONES GENERALES PARA ALMACEN****************************/

	    $scope.click = function(){
	        $scope.alta = true;
	    };

	    $scope.guardar = function(){
	        console.log($scope.data);
	        $scope.alta = true;
	        adquisiciones.postDatos(urlGuardar,$scope.data)
	        .then(function(data) {
	          console.log($scope.data);
	        })
	        .catch(function(err) {
	          console.log(err);
	        });
	    };

	    $scope.editar = function(){
	        console.log($scope.data);
	        $scope.alta = true;
	        adquisiciones.postDatos(urlEditar,$scope.data)
	        .then(function(data) {
	          console.log($scope.data);
	        })
	        .catch(function(err) {
	          console.log(err);
	        });
	    };

	    $scope.eliminar = function(){

	    };


	    $scope.regresar = function(){
	        $scope.alta = false;
	        $scope.data={};
	    };

	    /******************************************************************************/
	    $scope.validarCargar=function(){
	    	console.log("validando el guardar");
	    	console.log($location.path());
	    	var url=$location.path();
	    		console.log(url);
	        switch(url){
	          case "/adquisiciones/grupos":
	          		cargarDatosGrupos();
	          		urlGuardar="grupo/";
	          		urlEditar="grupo/";
	                break;

	          case "/adquisiciones/articulos":
	          		cargarDatosArticulos();
	          		urlGuardar="grupo/";
	          		urlEditar="grupo/";
	                break;

	          case "/adquisiciones/entradas":
	         		cargaEntradas();
	                break;

	          default:
	          			console.log(url);
	                break;
	        };
	    };

	    $scope.validarCargar();


    });
