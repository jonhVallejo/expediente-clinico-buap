

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AdquisicionesEntradasCtrl
 * @description
 * # AdquisicionesEntradasCtrl
 * Controller of the expediente
 */
(function(){
'use strict';
angular.module('expediente')
    .controller('AdquisicionesEntradasCtrl',['$scope','usuario','$state','mensajes','$mdDialog','$q', '$http', 'peticiones', 
    function ($scope,usuario,$state,mensajes,$mdDialog,$q, $http, peticiones) {
    	$scope.datosArticulos=[];
    	$scope.datosProveedor=[];
    	$scope.datosArticulosBusqueda=[];
    	$scope.F_ACTUAL = moment().toDate();
    	
    	$scope.datos={};
    	 $scope.query = {
	        filter: '',
	        order: '!NP_ID',
	        limit: 5,
	        page: 1
    	};
    	$scope.total=0;

    getDatosArticulos();
    getDatosProveedor();
    getTipoPago();

	function getDatosArticulos(){
		//$scope.urlService	= "articulo/";
		$http.get(baseURL + "articulo/")
		.success(function(data){
			$scope.datosArticulosBusqueda = data;
			console.log("Articulos recuperados para la busqueda..");
			console.log($scope.datosArticulosBusqueda);			
		})
		.error(function(error){
			mensajes.alerta('ERROR','NO SE PUDO OBTENER LOS ARTICULOS','ACEPTAR!');
		});
	};

	function getDatosProveedor(){
		//$scope.urlService	= "proveedor/";
		$http.get(baseURL + "proveedor/")
		.success(function(data){
			$scope.datosProveedor = data;
			console.log("Proveedore recuperados..");
			console.log($scope.datosProveedor);			
		})
		.error(function(error){
			mensajes.alerta('ERROR','NO SE PUDO OBTENER LOS PROVEEDORES','ACEPTAR!');
		});
	};

	function getTipoPago() {
		$http.get(baseURL + "factura/pago/")
		.success(function(data){
			$scope.catTipoPagos = data;
			/*console.log("Catalogo de Tipo  de Pago..");
			console.log($scope.catTipoPagos);			*/
		})
		.error(function(error){
			mensajes.alerta('ERROR','NO SE PUDO OBTENER LOS TIPOS DE PAGO','ACEPTAR!');
		});	
	}	
	
	/******************************************AGREGAR******************************************************/
	$scope.addReg = function(idArticulo,cClave,cDescripcion,unidad,iva) {
		console.log("IVA"+iva);
		 $scope.datosArticulos.push({nId: idArticulo, NF_ARTICULO: cClave, CDESCRIPCION: cDescripcion, NFUNIDAD:unidad, DPRECIO:undefined, NIVA: iva, D_PRECIO_NETO:0, N_CANTIDAD: 1, IMPORTE: 0, F_CADUCIDAD: undefined, FCADUCIDAD: undefined, C_LOTE:undefined}); 
    };
	/******************************************ELIMINAR******************************************************/
	$scope.deleteReg = function(index) {
		 mensajes.confirmacion('ADVERTENCIA','DESEA ELIMINAR EL REGISTRO','SI'). then(function() {
          $scope.datosArticulos.splice(index,1);
        }); 
  
    };
	/******************************************EDITAR********************************************************/
	$scope.updateReg = function() {
 
    };
    /*************************FUNCIONES PARA BUSCAR PROVEEDOR*************************************************/
     $scope.querySearch = function(texto,tipo) {
	    var defered = $q.defer();
	    var promise = defered.promise;
	    texto = texto.toUpperCase();
	    var arraySearch = (tipo == 0) 	? $scope.cargarProveedores(texto, $scope.datosProveedor, [{descripcion : "C_DESCRIPCION", validar: false, campo:"" }])
	    								: $scope.cargarArticulos(texto, $scope.datosArticulosBusqueda, [{descripcion : "C_DESCRIPCION", validar: false, campo:"" }]);

	  defered.resolve(arraySearch);
	  return promise;
	}

	$scope.cargarProveedores= function(filtro, data, campos){
		if(data!==undefined){
			var result = peticiones.filtrar(filtro, data, campos);
	      	var a = result.map(function (serv){
						        return {
						          value : (serv.C_DESCRIPCION).toUpperCase(),
						          display: (serv.C_DESCRIPCION ).toUpperCase(),
						          idProve : serv.nId
						        };
	      });
	      return a;
	    }
	};

	$scope.cargarArticulos= function(filtro, data, campos){
		if(data!==undefined){
	    	var result = peticiones.filtrar(filtro, data, campos);	    	
		    var a = result.map(function (serv){
						        return {
						          CCLAVE : (serv.C_CLAVE),
						          CDESCRIPCION : (serv.C_DESCRIPCION),
						          NFUNIDAD:(serv.C_UNIDAD),
						          display: (serv.C_DESCRIPCION ).toUpperCase(),
						          NIVA:(serv.N_IVA),
						          NID:(serv.nId)
						    	};
	      });
	      return a;
	    }
	};


	$scope.selectedItemChange = function(item,tipo) {
	    if(item!==undefined){
	    	console.log("elemento seleccionado");
	    	if(tipo == 0){
	    		$scope.prov = item;
	    	}
	    	if(tipo==1){
	    		$scope.art = item;
	    		console.log(item);
	    		$scope.addReg(item.NID,item.CCLAVE,item.CDESCRIPCION,item.NFUNIDAD,item.NIVA);
	    	}
	    }
	};

    
    /**************************FUNCIONES PARA SUBIR LOS ARCHIVOS**********************************************/
    function downloadArchivo(registro,tipo,ev){
		

	};

	$scope.loadArchivo=function (ev){
	  	$mdDialog.show({
            controller: DialogControllerLoad,
            templateUrl: '/application/views/quirofano/subirarchivo.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
           
          })
          .then(function(answer) {
            $scope.status = 'You said the information was "' + answer + '".';
          }, function() {
            $scope.status = 'You cancelled the dialog.';
          });

	};

	function DialogControllerLoad($scope, $mdDialog,$timeout) {
  		
  	    $scope.cancelar = function() {
	           $mdDialog.hide({result: false});
	    };
	    $scope.guardar = function(bandera) {
	        $mdDialog.hide({result: bandera});
	    };

	    $scope.actualizardatos = function(image, formulario){
	    	$scope.isImage = image;
	    	$scope.errorMsg = false;
	    	$scope.picFile = null;
	    }

	    $scope.eliminarArchivo = function(formulario){
	    	$scope.picFile = null;
	    	formulario.$setPristine();
      		formulario.$setUntouched();
	    }

	    $scope.uploadPic = function(file) {
    	
      	};

 	};

	 /*********************************FUNCIONES PARA TABLA*************************************************/
    $scope.onOrderChange = function(page, limit) {
      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve();
      }, 2000);

      return deferred.promise;
    };

    /*********************************FUNCIONES PARA ENTRADAS***********************************************/
    $scope.getTotal=function(){
    	var t = 0;
    	angular.forEach($scope.datosArticulos, function(reg) {
	       t += reg.IMPORTE;
	    });
	    $scope.total=t;
    };

    $scope.getIva=function(porcentaje,precio,index){
	 	$scope.datosArticulos[index].D_PRECIO_NETO= precio + (porcentaje*precio/100);
		$scope.calcualPrecio(index);	
		$scope.getTotal(); 		 	
		//$scope.datosArticulos[index].IMPORTE=($scope.datosArticulos[index].PIVA*$scope.datosArticulos[index].NCANTIDAD);
    };

    $scope.calcualPrecio = function(index) {
    	$scope.datosArticulos[index].IMPORTE = $scope.datosArticulos[index].D_PRECIO_NETO * $scope.datosArticulos[index].N_CANTIDAD;    
    	$scope.getTotal(); 		 		
    }

    $scope.guardar=function(){
    	
    };

    $scope.guardar = function(tabla) {			
		if (isRegistroValido()) {
			$scope.datos.C_ARTICULOS = $scope.datosArticulos
			console.log("Datos a registrar");
			console.log($scope.datos);

			$http.post(baseURL + "factura/",$scope.datos)
			.success(function(data) {
				mensajes.alerta('','LA FACTURA SE GUARDO EXITOSAMENTE','ACEPTAR!');
				$state.go('bienvenido');
			})
			.error(function(error) {
				mensajes.alerta('ERROR','NO SE PUDO GUARDAR LA FACTURA','ACEPTAR!');
			});
		}				
	}

	function isRegistroValido() {
		var isregValido = true;

		if ($scope.datos.FFECHA == undefined) {
			mensajes.alerta('ERROR','FALTA REGISTRAR LA FECHA DE LA FACTURA','ACEPTAR!');	
		}
		else {
			$scope.datos.F_FECHA = moment($scope.datos.FFECHA).format("DD/MM/YYYY");
			$scope.datos.C_FILEPDF = "ALGO.PDF";
			$scope.datos.C_FILEXML = "ALGO.XML";
			$scope.datos.NF_PROVEEDOR = $scope.prov.idProve;

			if ($scope.datosArticulos.length == 0) {
				isregValido = false;
				mensajes.alerta('ERROR','NO SE HA ESPECIFICADO LOS ARTICULOS DE LA FACTURA','ACEPTAR!');
			}

			angular.forEach($scope.datosArticulos, function(reg) {	
				if (reg.FCADUCIDAD == undefined) {
					isregValido = false;
					mensajes.alerta('ERROR','FALTA REGISTRAR LA FECHA CADUCIDAD','ACEPTAR!');	
				} 
				else
					reg.F_CADUCIDAD = moment(reg.FCADUCIDAD).format("DD/MM/YYYY");
			});
		}	
		return isregValido;			
	}	

    }]);
})();

