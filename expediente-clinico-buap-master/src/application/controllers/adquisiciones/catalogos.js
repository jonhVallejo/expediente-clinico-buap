'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AdquisicionesCatalogosCtrl
 * @description
 * # AdquisicionesCatalogosCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('AdquisicionesCatalogosCtrl', function ($scope, $http, $mdDialog, peticiones, mensajes) {

	$scope.alta = false;
	$scope.edita = false;
	$scope.ver = false;
	$scope.selected = [];
	$scope.selectedAbono = [];
	$scope.selectedCargo = [];
	$scope.datos = {};
	$scope.datos.iva = 16;
	$scope.isPieza = false;
	
	$scope.query = {
    	    filter: '',
    	    order: 'cDescripcion',
    	    limit: 15,
    	    page: 1
	};

	$scope.filtraTabla = function(filtro) {
		$scope.registros = peticiones.filtrar(filtro,$scope.datos,$scope.campos);
	}

	$scope.onOrderChange = function(page, limit) {
		    var deferred = $q.defer();
		    $timeout(function () {
		    	deferred.resolve();
		    }, 2000);

		    return deferred.promise;
	    };

	$scope.actualizaPrecioConIva = function() {
	  	$scope.datos.precioIva = ($scope.datosRegistro.aplicaIVA ? $scope.datosRegistro.N_IVA * $scope.datosRegistro.N_PRECIO : $scope.datosRegistro.N_PRECIO) 
	}

	$scope.obtenRegistros = function(catalogo) {
		$scope.datosRegistro = {};
		switch (catalogo) {
			// Departamentos
			case 'D': 	$scope.urlService	= "almacen/";
						$http.get(baseURL + "almacen/subalmacenes/")
						.success(function(data){
							$scope.datos = data;
							$scope.registros = $scope.datos;
							angular.forEach($scope.registros, function(dep) {
								dep.C_ESTATUS = dep.sEstatusRegistro == 1 ? "ACTIVO" : "BAJA";
								dep.C_CLAVE = dep.C_CLAVE == "S" ? 0 : 1;
							})

							console.log("Subalmacenes recuperados..");
							console.log($scope.registros);			
						})
						.error(function(error){
							mensajes.alerta('ERROR','NO SE PUDO OBTENER LOS SUBALMACENES','ACEPTAR!');
						});
						break;

			// Articulos
			case 'A': 	$scope.urlService	= "articulo/";
						$http.get(baseURL + $scope.urlService)
						.success(function(data){
							$scope.datos = data;
							$scope.registros = _.filter($scope.datos, function(articulo) { return  articulo.S_ESTATUS_REGISTRO; });
							console.log("Articulos recuperados..");
							console.log($scope.datos);
							$http.get(baseURL + 'catalogo/35')  // Catalogo de Unidades de Medida
							.success(function(data) {
								$scope.catUnidadMedida = data;
								console.log("Catalo de Unidades");
								console.log($scope.catUnidadMedida);
								$http.get(baseURL +"grupo/cuenta/")
								.success(function(data){
									$scope.catGrupos = data;
									console.log("Grupos recuperados..");
									console.log($scope.catGrupos);
								})
								.error(function(error){
									mensajes.alerta('ERROR','NO SE PUDO OBTENER LOS GRUPOS','ACEPTAR!');
								});	
							})
							.error(function(data) {
								mensajes.alerta('ERROR',"NO SE PUDO OBTENER LE CATÁLOGO DE UNIDADES DE MEDIDA",'ACEPTAR!');
							});
						})
						.error(function(error){
							mensajes.alerta('ERROR','NO SE PUDO OBTENER LOS ARTICULOS','ACEPTAR!');
						});

						$scope.campos = [{descripcion : "C_CLAVE",		 validar: false, campo:"" },
					 					 {descripcion : "C_DESCRIPCION", validar: false, campo:"" },
					 					 {descripcion : "C_GRUPO", 		 validar: false, campo:"" },];						
					  	break;
			// Grupos
			case 'G': 	$scope.urlService	= "grupo/";
						$http.get(baseURL + $scope.urlService+"cuenta/")
						.success(function(data){
							$scope.datos = data;
							$scope.registros = $scope.datos;
							console.log("Grupos recuperados..");
							console.log($scope.datos);
							$http.get(baseURL + "cuenta/abono")
							.success(function(data) {
								$scope.catCuentaAbono = data;
								console.log("Cuenta Abono");
								console.log($scope.catCuentaAbono);
								$http.get(baseURL + "cuenta/cargo")
								.success(function(data) {
									$scope.catCuentaCargo = data;
									console.log("Cuenta Cargo");
									console.log($scope.catCuentaCargo);
								})
								.error(function(error) {
									mensajes.alerta('ERROR','NO SE PUDO OBTENER LAS CUENTAS DE CARGO','ACEPTAR!');
								});	
							})
							.error(function(error){
								mensajes.alerta('ERROR','NO SE PUDO OBTENER LAS CUENTAS DE ABONO','ACEPTAR!');
							});

						})
						.error(function(error){
							mensajes.alerta('ERROR','NO SE PUDO OBTENER LOS GRUPOS','ACEPTAR!');
						});	
						$scope.campos = [{descripcion : "cDescripcion", validar: false, campo:"" }];						
					  	break;

			// Proveedores		  	
			case 'P': 	$scope.urlService	= "proveedor/";
						$http.get(baseURL + $scope.urlService)
						.success(function(data){
							$scope.datos = data;
							$scope.registros = $scope.datos;
							console.log("Proveedore recuperados..");
							console.log($scope.datos);
							$http.get(baseURL + 'catalogo/7')  // Catalogo de Estados
							.success(function(data) {
								$scope.catEstados = data;
							})
							.error(function(data) {
								mensajes.alerta('ERROR',"NO SE PUDO OBTENER LE CATÁLOGO DE ESTADOS",'ACEPTAR!');
							});
						})
						.error(function(error){
							mensajes.alerta('ERROR','NO SE PUDO OBTENER LOS PROVEEDORES','ACEPTAR!');
						});


						$scope.campos = [{descripcion : "C_DESCRIPCION",		validar: false, campo:"" }];
						
					  	break;
			// Cuentas			
			case 'C': 	$scope.campos = [{descripcion : "cClave",		validar: false, campo:"" },
					 					 {descripcion : "cDescripcion",	validar: false, campo:"" }];
											  	
						//console.log(usuario);
						$scope.urlService	= "cuenta/";
						$http.get(baseURL + $scope.urlService + "abono")
						.success(function(data) {
							console.log("Cuentas de Abono Recuperadas..")
							console.log(data);
							$scope.datosAbono = data;
							$scope.registrosAbono = $scope.datosAbono; 
							$http.get(baseURL + $scope.urlService + "cargo")
							.success(function(data) {
								console.log("Cuentas de Cargo Recuperadas..")
								console.log(data);
								$scope.datosCargo = data;
								$scope.registrosCargo = $scope.datosCargo; 
							})
							.error(function(error) {
								mensajes.alerta('ERROR','NO SE PUDO OBTENER LAS CUENTAS DE CARGO','ACEPTAR!');
							});

						})
						.error(function(error) {
							mensajes.alerta('ERROR','NO SE PUDO OBTENER LAS CUENTAS DE ABONO','ACEPTAR!');
						});
						break;	
		}
	}

  	$scope.editRow = function() {
	  	$scope.edita = true;
	}

	$scope.editRowDepartamento = function() {
		$scope.datosRegistro.C_DESCRIPCION  = $scope.selected[0].C_DESCRIPCION;		
		$scope.datosRegistro.C_CLAVE  = $scope.selected[0].C_CLAVE;		
		$scope.datosRegistro.C_UBICACION  = $scope.selected[0].C_UBICACION;		
		$scope.datosRegistro.sEstatusRegistro  = $scope.selected[0].sEstatusRegistro;		
		console.log("Data seleccionado");
		console.log($scope.datosRegistro);
		$scope.edita = true;
	}

	$scope.editRowGrupo = function() {
		console.log($scope.datos);
		$scope.datosRegistro.cDescripcion  = $scope.selected[0].cDescripcion;
		$scope.datosRegistro.nfCuentaCargo = $scope.selected[0].NFCARGOID;
		$scope.datosRegistro.nfCuentaAbono = $scope.selected[0].NFABONOID;

	  	$scope.edita = true;
	}

	$scope.editRowProveedor = function() {
		//console.log($scope.datos);
		$scope.datosRegistro.C_CLAVE 				= $scope.selected[0].C_CLAVE;
		$scope.datosRegistro.C_DESCRIPCION 			= $scope.selected[0].C_DESCRIPCION;
		$scope.datosRegistro.C_RFC 					= $scope.selected[0].C_RFC;
		$scope.datosRegistro.C_DIRECCION 			= $scope.selected[0].C_DIRECCION;
		$scope.datosRegistro.C_COLONIA 				= $scope.selected[0].C_COLONIA;
		$scope.datosRegistro.N_CODIGO_POSTAL		= $scope.selected[0].N_CODIGO_POSTAL;
		$scope.datosRegistro.C_MUNICIPIO 			= $scope.selected[0].C_MUNICIPIO;
		$scope.datosRegistro.NF_ENTIDAD_FEDERATIVA 	= $scope.selected[0].NF_ENTIDAD_FEDERATIVA;
		$scope.datosRegistro.C_TELEFONO 			= $scope.selected[0].C_TELEFONO;
		$scope.datosRegistro.N_EXTENSION 			= $scope.selected[0].N_EXTENSION;
		$scope.datosRegistro.C_TELEFONO2 			= $scope.selected[0].C_TELEFONO2;
		$scope.datosRegistro.N_EXTENSION2 			= $scope.selected[0].N_EXTENSION2;
		$scope.datosRegistro.C_EMAIL 				= $scope.selected[0].C_EMAIL;

	  	$scope.edita = true;
	}

	$scope.editRowArticulo = function() {
		$scope.datosRegistro.C_CLAVE 					= $scope.selected[0].C_CLAVE;
		$scope.datosRegistro.C_DESCRIPCION 				= $scope.selected[0].C_DESCRIPCION;
		$scope.datosRegistro.NF_UNIDAD 					= $scope.selected[0].NF_UNIDAD;
		$scope.datosRegistro.N_PIEZAS 					= $scope.selected[0].N_PIEZAS;
		$scope.datosRegistro.N_CANTIDAD_UNIDOSIS 		= $scope.selected[0].N_CANTIDAD_UNIDOSIS;
		$scope.datosRegistro.NF_UNIDAD_MEDIDA_UNIDOSIS	= $scope.selected[0].NF_UNIDAD_MEDIDA_UNIDOSIS;
		$scope.datosRegistro.NF_GRUPO 					= $scope.selected[0].NF_GRUPO;
		$scope.datosRegistro.D_PORCENTAJE 				= $scope.selected[0].D_PORCENTAJE;
		$scope.datosRegistro.N_IVA 						= $scope.selected[0].N_IVA;
		$scope.datosRegistro.N_MINIMO 					= $scope.selected[0].N_MINIMO;
		$scope.datosRegistro.N_MAXIMO 					= $scope.selected[0].N_MAXIMO;
		$scope.datosRegistro.N_PUNTO_REORDEN 			= $scope.selected[0].N_PUNTO_REORDEN;
		$scope.datosRegistro.N_ACTIVO_INTERNO 			= $scope.selected[0].N_ACTIVO_INTERNO == undefined ? false:  $scope.selected[0].N_ACTIVO_INTERNO;
		$scope.datosRegistro.N_CUENTA_PACIENTE 			= $scope.selected[0].N_CUENTA_PACIENTE == undefined ? false:  $scope.selected[0].N_CUENTA_PACIENTE;
		$scope.datosRegistro.C_MARCA 					= $scope.selected[0].C_MARCA;
		$scope.datosRegistro.C_MODELO 					= $scope.selected[0].C_MODELO;
		$scope.datosRegistro.C_COLOR 					= $scope.selected[0].C_COLOR;
		$scope.datosRegistro.S_ESTATUS_REGISTRO 		= $scope.selected[0].S_ESTATUS_REGISTRO;
		$scope.datosRegistro.N_EXISTENCIAS_ALMACEN 		= $scope.selected[0].N_EXISTENCIAS_ALMACEN;

	  	$scope.edita = true;
	}
	
	$scope.editRowAbono = function() {
		$scope.datos.claveAbono  = $scope.selectedAbono[0].cClave;
		$scope.datos.nombreAbono = $scope.selectedAbono[0].cDescripcion;		
		$scope.datos.nIdAbono	 = $scope.selectedAbono[0].nId;
		$scope.editaAbono = true;
	}	

	$scope.editRowCargo = function() {
		$scope.datos.claveCargo  = $scope.selectedCargo[0].cClave;
		$scope.datos.nombreCargo = $scope.selectedCargo[0].cDescripcion;		
		$scope.datos.nIdCargo	 = $scope.selectedCargo[0].nId;
	  	$scope.editaCargo = true;
	}

	$scope.actualizar = function(tabla) {
		//console.log("Reg seleccionado");
		//console.log($scope.selected[0]);
		//$scope.datosRegistro.nId = $scope.selected[0].nId;
		var isValido = true;
		isValido = validaRegistro(tabla);		
		if (isValido) {
			$scope.datosRegistro.nfUsuario = parseInt(usuario.NP_EXPEDIENTE);
			if (tabla == "D")
				$scope.datosRegistro.C_CLAVE = $scope.datosRegistro.C_CLAVE == 0 ? "S" : "G";
			console.log("Datos a actualizar");
			console.log($scope.datosRegistro);
			
			var msg = ""
			$http.put(baseURL + $scope.urlService + $scope.selected[0].nId, $scope.datosRegistro)
			.success(function(data) {
				switch (tabla) {
					case 'D':   var iDatos = _.findLastIndex($scope.datos, {nId: $scope.selected[0].nId});
								//$scope.datos[iDatos] = $scope.selected[0];
								$scope.datos[iDatos].C_DESCRIPCION = $scope.datosRegistro.C_DESCRIPCION;
								$scope.datos[iDatos].C_UBICACION = $scope.datosRegistro.C_UBICACION;
								$scope.datos[iDatos].C_TIPO_ALMACEN = $scope.datosRegistro.C_CLAVE == "S" ? "SUBALMACEN" : "ALMACEN";
								$scope.datos[iDatos].C_CLAVE = $scope.datosRegistro.C_CLAVE == "S" ? 0 : 1;
								$scope.datos[iDatos].C_ESTATUS = $scope.datosRegistro.sEstatusRegistro == 1 ? "ACTIVO" : "BAJA";
								var msg = "EL DEPARTAMENTO FUE ACTUALIZADO EXITOSAMENTE";
								break;

					case 'G': 	var iCat = _.findLastIndex($scope.catCuentaAbono, {nId: $scope.datosRegistro.nfCuentaAbono});
								var iDatos = _.findLastIndex($scope.datos, {nId: $scope.selected[0].nId});							
								$scope.datos[iDatos].NFABONOID = $scope.catCuentaAbono[iCat].cClave + "-" + $scope.catCuentaAbono[iCat].cDescripcion;

								iCat = _.findLastIndex($scope.catCuentaCargo, {nId: $scope.datosRegistro.nfCuentaCargo});
								$scope.datos[iDatos].NFCARGODESC = $scope.catCuentaCargo[iCat].cClave + "-" + $scope.catCuentaCargo[iCat].cDescripcion;
								$scope.datos[iDatos].cDescripcion = $scope.datosRegistro.cDescripcion;
								var msg = "EL GRUPO FUE ACTUALIZADO EXITOSAMENTE";
								break;
					case 'P':   var iDatos = _.findLastIndex($scope.datos, {nId: $scope.selected[0].nId});
								$scope.datos[iDatos].C_CLAVE 				= $scope.datosRegistro.C_CLAVE;
								$scope.datos[iDatos].C_DESCRIPCION 			= $scope.datosRegistro.C_DESCRIPCION;
								$scope.datos[iDatos].C_RFC 					= $scope.datosRegistro.C_RFC;
								$scope.datos[iDatos].C_DIRECCION 			= $scope.datosRegistro.C_DIRECCION;
								$scope.datos[iDatos].C_COLONIA 				= $scope.datosRegistro.C_COLONIA;
								$scope.datos[iDatos].N_CODIGO_POSTAL		= $scope.datosRegistro.N_CODIGO_POSTAL;
								$scope.datos[iDatos].C_MUNICIPIO 			= $scope.datosRegistro.C_MUNICIPIO;
								$scope.datos[iDatos].NF_ENTIDAD_FEDERATIVA 	= $scope.datosRegistro.NF_ENTIDAD_FEDERATIVA;
								$scope.datos[iDatos].C_TELEFONO 			= $scope.datosRegistro.C_TELEFONO;
								$scope.datos[iDatos].N_EXTENSION 			= $scope.datosRegistro.N_EXTENSION;
								$scope.datos[iDatos].C_TELEFONO2 			= $scope.datosRegistro.C_TELEFONO2;
								$scope.datos[iDatos].N_EXTENSION2 			= $scope.datosRegistro.N_EXTENSION2;
								$scope.datos[iDatos].C_EMAIL 				= $scope.datosRegistro.C_EMAIL;
								var msg = "EL PROVEEDOR FUE ACTUALIZADO EXITOSAMENTE";
								break;	
					//	Articulos		
					case 'A': 	var iDatos = _.findLastIndex($scope.datos, {nId: $scope.selected[0].nId});
								$scope.datos[iDatos].C_CLAVE 					= $scope.datosRegistro.C_CLAVE;
								$scope.datos[iDatos].C_DESCRIPCION 				= $scope.datosRegistro.C_DESCRIPCION;
								$scope.datos[iDatos].NF_UNIDAD 					= $scope.datosRegistro.NF_UNIDAD;
								var iCat = _.findLastIndex($scope.catUnidadMedida, {ID: $scope.datosRegistro.NF_UNIDAD});
								console.log("indice para unidad de Medida"+iCat);
								$scope.datos[iDatos].C_UNIDAD 					= $scope.catUnidadMedida[iCat].cDescripcion;
								$scope.datos[iDatos].N_PIEZAS 					= $scope.datosRegistro.N_PIEZAS;
								$scope.datos[iDatos].N_CANTIDAD_UNIDOSIS 		= $scope.datosRegistro.N_CANTIDAD_UNIDOSIS;
								$scope.datos[iDatos].NF_UNIDAD_MEDIDA_UNIDOSIS	= $scope.datosRegistro.NF_UNIDAD_MEDIDA_UNIDOSIS;
								iCat = _.findLastIndex($scope.catUnidadMedida, {ID: $scope.datosRegistro.NF_UNIDAD_MEDIDA_UNIDOSIS});
								console.log(iCat);
								$scope.datos[iDatos].C_UNIDAD_MEDIDA_UNIDOSIS	= $scope.catUnidadMedida[iCat].cDescripcion;
								$scope.datos[iDatos].NF_GRUPO 					= $scope.datosRegistro.NF_GRUPO;
								iCat = _.findLastIndex($scope.catGrupos, {nId: $scope.datosRegistro.NF_GRUPO});
								$scope.datos[iDatos].C_GRUPO					= $scope.catGrupos[iCat].cDescripcion;
								$scope.datos[iDatos].D_PORCENTAJE 				= $scope.datosRegistro.D_PORCENTAJE;
								$scope.datos[iDatos].N_IVA						= $scope.datosRegistro.N_IVA;
								$scope.datos[iDatos].N_MINIMO 					= $scope.datosRegistro.N_MINIMO;
								$scope.datos[iDatos].N_MAXIMO					= $scope.datosRegistro.N_MAXIMO;
								$scope.datos[iDatos].N_PUNTO_REORDEN 			= $scope.datosRegistro.N_PUNTO_REORDEN;
								$scope.datos[iDatos].N_ACTIVO_INTERNO 			= $scope.datosRegistro.N_ACTIVO_INTERNO;
								$scope.datos[iDatos].N_CUENTA_PACIENTE 			= $scope.datosRegistro.N_CUENTA_PACIENTE;
								$scope.datos[iDatos].C_MARCA 					= $scope.datosRegistro.C_MARCA;
								$scope.datos[iDatos].C_MODELO 					= $scope.datosRegistro.C_MODELO;
								$scope.datos[iDatos].C_COLOR 					= $scope.datosRegistro.C_COLOR;
								$scope.datos[iDatos].S_ESTATUS_REGISTRO 		= $scope.datosRegistro.S_ESTATUS_REGISTRO;
								var msg = "EL PROVEEDOR FUE ACTUALIZADO EXITOSAMENTE";
								break;
				}			
				mensajes.alerta('',msg,'ACEPTAR!');
				$scope.selected = [];
				$scope.edita = false;
				$state.go('bienvenido');
			})
			.error(function(error) {
				
				switch (tabla) {
					case 'G' : msg = " EL GRUPO"; break;
					case 'P' : msg = " EL PROVEEDOR"; break;
					case 'A' : msg = " EL ARTICULO"; break;
				}
				mensajes.alerta('ERROR','NO SE PUDO ACTUALIZAR'+ msg,'ACEPTAR!');
				$scope.edita = true;
			});
		}
	}
	
	$scope.actualizarAbono = function() {
		var registro = {cDescripcion: $scope.datos.nombreAbono, cClave: $scope.datos.claveAbono, nfUsuario: parseInt(usuario.NP_EXPEDIENTE),  NF_TIPO_CUENTA: 2};
		console.log("Datos a Actualizar..");
		console.log(registro);
		$http.put(baseURL + $scope.urlService + $scope.datos.nIdAbono, registro)
		.success(function(data) {
			$scope.editaAbono = false;
			var actualizado = {cClave: registro.cClave, cDescripcion: registro.cDescripcion, nfUsuario: parseInt(usuario.NP_EXPEDIENTE)};
			var i = _.findLastIndex($scope.datosAbono, {nId: $scope.datos.nIdAbono});			
			$scope.datosAbono[i].cDescripcion = registro.cDescripcion;
			console.log($scope.datosAbono[i]);
			mensajes.alerta('','LA CUENTA FUE ACTUALIZADA EXITOSAMENTE','ACEPTAR!');
			$scope.selectedAbono = [];
		})
		.error(function(error) {
			mensajes.alerta('ERROR','NO SE PUDO ACTUALIZAR LA CUENTA','ACEPTAR!');
		});
	}

	$scope.actualizarCargo = function() {
		var registro = {cDescripcion: $scope.datos.nombreCargo, cClave: $scope.datos.claveCargo, nfUsuario: parseInt(usuario.NP_EXPEDIENTE),  NF_TIPO_CUENTA: 1};
		console.log("Datos a Actualizar..");
		console.log(registro);
		$http.put(baseURL + $scope.urlService + $scope.datos.nIdCargo, registro)
		.success(function(data) {
			$scope.editaCargo = false;
			var actualizado = {cClave: registro.cClave, cDescripcion: registro.cDescripcion, nfUsuario: parseInt(usuario.NP_EXPEDIENTE)};			
			var i = _.findLastIndex($scope.datosCargo, {nId: $scope.datos.nIdCargo});			
			$scope.datosCargo[i].cDescripcion = registro.cDescripcion;
			console.log($scope.datosCargo[i]);
			mensajes.alerta('','LA CUENTA FUE ACTUALIZADA EXITOSAMENTE','ACEPTAR!');
			$scope.selectedCargo = [];
		})
		.error(function(error) {
			mensajes.alerta('ERROR','NO SE PUDO ACTUALIZAR LA CUENTA','ACEPTAR!');
		});
	}

	$scope.guardar = function(tabla) {	
		var isValido = true;		
		isValido = validaRegistro(tabla);		
		if (isValido) {		
			var msg;
			console.log("Datos a registrar");
			$scope.datosRegistro.nfUsuario = parseInt(usuario.NP_EXPEDIENTE);
			if (tabla == "D")
				$scope.datosRegistro.C_CLAVE = $scope.datosRegistro.C_CLAVE == 0 ? "S" : "G";
			console.log($scope.datosRegistro);
			$http.post(baseURL + $scope.urlService, $scope.datosRegistro)
			.success(function(id){
				console.log(id);
		  		$scope.alta = false;		  		

				switch (tabla) {
					case 'D':   $scope.datosRegistro.nId = id;
								$scope.datosRegistro.C_TIPO_ALMACEN = $scope.datosRegistro.C_CLAVE = "S" ? "SUBALMACEN" : "GENERAL"; 
								$scope.datosRegistro.C_CLAVE = $scope.datosRegistro.C_CLAVE = "S" ? 0 : 1; 
								$scope.datosRegistro.C_ESTATUS = $scope.datosRegistro.sEstatusRegistro = 1 ? "ACTIVO" : "BAJA"; 
								msg = "EL DEPARTAMENTO FUE INSERTADO EXITOSAMENTE";
								$scope.datos.push($scope.datosRegistro); 			
								break;
					case 'G': 	var iCatAbono = _.findLastIndex($scope.catCuentaAbono, {nId: $scope.datosRegistro.nfCuentaAbono});
								var iCatCargo = _.findLastIndex($scope.catCuentaCargo, {nId: $scope.datosRegistro.nfCuentaCargo});
								msg = "EL GRUPO FUE INSERTADO EXITOSAMENTE";
								$scope.datos.push({	NFABONODESC : $scope.catCuentaAbono[iCatAbono].cClave + "-" + $scope.catCuentaAbono[iCatAbono].cDescripcion,
												  	NFABONOID 	: $scope.datosRegistro.nfCuentaAbono,
												  	NFCARGODESC	: $scope.catCuentaCargo[iCatCargo].cClave + "-" + $scope.catCuentaCargo[iCatCargo].cDescripcion,
												  	NFCARGOID	: $scope.datosRegistro.nfCuentaCargo,
												  	cDescripcion: $scope.datosRegistro.cDescripcion,
												  	nId 		: id});
								break;
					case 'P': 	$scope.datosRegistro.nId = id;
								$scope.datos.push($scope.datosRegistro); 			
								msg = "EL PROVEEDOR FUE INSERTADO EXITOSAMENTE";
								break;
					case 'A':	$scope.datosRegistro.nId = id;
								$scope.datos.push($scope.datosRegistro); 			
								msg = "EL ARTICULO FUE INSERTADO EXITOSAMENTE";
								break;		
				}			
				mensajes.alerta('',msg,'ACEPTAR!');
			})
			.error(function(error){
				switch (tabla) {
					case 'D': $scope.datosRegistro.C_CLAVE = $scope.datosRegistro.C_CLAVE = "S" ? 0 : 1; 
							  msg = "NO SE REALIZAO EL ALTA DEL ALMACEN"; break;
					case 'G': msg = "NO SE REALIZAO EL ALTA DEL GRUPO"; break;
					case 'P': msg = "NO SE REALIZAO EL ALTA DEL PROVEEDOR"; break;
					case 'A': msg = "NO SE REALIZAO EL ALTA DEL ARTICULO"; break;
				}
				mensajes.alerta('ERROR',msg,'ACEPTAR!');	

			});
		}
	}

	function guardarCuenta(url, registro, tablaDatos) {	
		$http.post(url, registro)
		.success(function(id) { 
			console.log(id);
	  		$scope.altaAbono = false;	
	  		$scope.altaCargo = false;	

			var nuevo = {cClave: registro.cClave, cDescripcion: registro.cDescripcion, nId: id};
			/*console.log("Dato nuevo");
			console.log(nuevo);*/
			tablaDatos.push(nuevo);
			mensajes.alerta('','LA CUENTA FUE INSERTADA EXITOSAMENTE','ACEPTAR!');
		})
		.error(function() {
			mensajes.alerta('ERROR','NO SE PUDO INSERTAR LA CUENTA','ACEPTAR!');
		});
	}	

	$scope.guardarAbono = function() {	
		var registro = {cDescripcion: $scope.datos.nombreAbono, cClave: $scope.datos.claveAbono, nOrden: 0, sEstatusRegistro: 1, nfUsuario: parseInt(usuario.NP_EXPEDIENTE),  NF_TIPO_CUENTA: 2};
		console.log("Datos a Guardar..");
		console.log(registro);
		guardarCuenta(baseURL + $scope.urlService + "abono", registro, $scope.datosAbono);
		/*$scope.altaAbono = false;
	  	$scope.editaAbono = false;	*/
	}

	$scope.guardarCargo = function() {	
		var registro = {cDescripcion: $scope.datos.nombreCargo, cClave: $scope.datos.claveCargo, nOrden: 0, sEstatusRegistro: 1, nfUsuario: parseInt(usuario.NP_EXPEDIENTE), NF_TIPO_CUENTA: 1};
		console.log("Datos a Guardar..");
		console.log(registro);
		guardarCuenta(baseURL + $scope.urlService + "cargo", registro, $scope.datosCargo);
		/*$scope.altaCargo = false;
		$scope.editaCargo = false;	*/
	}
	
	$scope.insertarGrupo = function() {
		$scope.datos.cDescripcion = "";
		$scope.datos.cuentaCargo = "";
		$scope.datos.cuentaAbono = "";
		$scope.alta  = true;	
	}
	
	$scope.insertar = function() {
		$scope.datosRegistro = {};		
		$scope.alta  = true;	
	}

	$scope.insertarCargo = function() {
		$scope.datos.claveCargo= "";
		$scope.datos.nombreCargo = "";		
		$scope.altaCargo  = true;		
	}

	$scope.insertarAbono = function() {
		$scope.datos.claveAbono = "";
		$scope.datos.nombreAbono = "";		
		$scope.altaAbono  = true;		
	}

	$scope.deleteRegistro = function() {
		var i;
		angular.forEach($scope.selected, function(registro) {
			i = _.findLastIndex($scope.datos, {cClave: registro.cClave});			
            datos.splice(i, 1);			
		})
		$scope.selected = [];
	}

	$scope.showConfirm = function(ev,catalogo) {
		console.log("Confirmando..")
    	var confirm = $mdDialog.confirm()
	        .title('PRECAUCIÓN')
	        .content('DESEA ELIMINAR EL(LOS) REGISTRO(S)')
	        .ariaLabel('Lucky day')
	        .targetEvent(ev)
	        .ok('ACEPTAR')
	        .cancel('CANCELAR');

	    $mdDialog.show(confirm).then(function() {
	     	console.log("Eliminar datos");  
	     	var datos = [];  
	     	var i;   
	     	var datos = "";
		    var coma = "";	
		    var msg;
	    	switch (catalogo) {
				case 'C': 	$scope.selectedCargo.forEach(function(seleccion) {
						      datos = datos + coma + seleccion.nId;
						      coma = ",";
						    });

							$http.delete(baseURL + $scope.urlService, {data:datos})
							.success(function(data) {
								angular.forEach(datos, function(registro) {
									i = _.findLastIndex($scope.datosCargo, {cClave: registro.nId});			
					            	$scope.datosCargo.splice(i, 1);			             
								});
								$scope.selectedCargo = [];
								mensajes.alerta('','LA(S) CUENTA(S) FUE(FUERON) ELIMINADA(S) EXITOSAMENTE','ACEPTAR!');
							})
							.error (function(error) {
								mensajes.alerta('ERROR','NO SE PUDO ELIMINAR LA(S) CUENTA(S)','ACEPTAR!');
							});
							break;

				case 'A': 	$scope.selectedAbono.forEach(function(seleccion) {
						      datos = datos + coma + seleccion.nId;
						      coma = ",";
						    });
						    
							/*console.log("Informacion para la eliminacion");
							console.log(datos);*/
							$http.delete(baseURL + $scope.urlService, {data:datos})
							.success(function(data) {
								angular.forEach(datos, function(registro) {
									i = _.findLastIndex($scope.datosAbono, {cClave: registro.nId});			
					            	$scope.datosAbono.splice(i, 1);			             
								});
								$scope.selectedAbono = [];
								mensajes.alerta('','LA(S) CUENTA(S) FUE(FUERON) ELIMINADA(S) EXITOSAMENTE','ACEPTAR!');
							})
							.error (function(error) {
								mensajes.alerta('ERROR','NO SE PUDO ELIMINAR LA(S) CUENTA(S)','ACEPTAR!');
							});
							break;

				default : 	$scope.selected.forEach(function(seleccion) {
						      datos = datos + coma + seleccion.nId;
						      coma = ",";
						    });

							$http.delete(baseURL + $scope.urlService, {data:datos})
							.success(function(data) {
								angular.forEach(datos, function(registro) {
									i = _.findLastIndex($scope.datos, {nId: registro.nId});			
					            	$scope.datos.splice(i, 1);			             
								});
								//$scope.registros = 
								$scope.selected = [];
								switch (catalogo) {
									case 'G': msg = "LOS GRUPOS FUERON ELIMINADOS EXITOSAMENTE"; break
									case 'P': msg = "LOS PROVEEDORES FUERON ELIMINADOS EXITOSAMENTE"; break
									case 'X': msg = "LOS ARTICULOS FUERON ELIMINADOS EXITOSAMENTE"; break
								}
								mensajes.alerta('',msg,'ACEPTAR!');
							})
							.error (function(error) {
								switch (catalogo) {
									case 'G': msg = "NO SE PUDO ELIMINAR EL(LOS) GRUPO(S)"; break
									case 'P': msg = "NO SE PUDO ELIMINAR EL(LOS) PROVEEDOR(ES)"; break
									case 'X': msg = "NO SE PUDO ELIMINAR EL(LOS) ARTICULO(S)"; break
								}
								mensajes.alerta('ERROR',msg,'ACEPTAR!');
							});
						    break;
			}
	    });
 	}  

 	$scope.verProveedor = function() {
 		$scope.editRowProveedor();
 		$scope.ver = true;
 		$scope.edita = false;
 	}

 	$scope.verArticulo = function() {
 		$scope.editRowArticulo();
 		$scope.ver = true;
 		$scope.edita = false;	
 	}

 	$scope.habilitaRegistro = function() {
 		$scope.ver = false;
 		$scope.edita = true;	
 	}

	$scope.cancelar = function() {
		$scope.alta  = false;	
		$scope.edita = false;
		$scope.ver = false;	
		$scope.selected = [];
		//$scope.registros = $scope.datos;
	}

	$scope.cancelarAbono = function() {
		$scope.altaAbono  = false;	
		$scope.editaAbono = false;	
		$scope.selectedAbono = [];
	}

	$scope.cancelarCargo = function() {
		$scope.altaCargo  = false;	
		$scope.editaCargo = false;	
		$scope.selectedCargo = [];
	}

	$scope.actNoPiezas = function() {
		if ($scope.datosRegistro.NF_UNIDAD == 1) {
			$scope.datosRegistro.N_PIEZAS = 1;
			$scope.datosRegistro.N_CANTIDAD_UNIDOSIS = 1;
			$scope.datosRegistro.NF_UNIDAD_MEDIDA_UNIDOSIS = 1;
			$scope.isPieza = true;
		}
		else {
			//$scope.datosRegistro.N_PIEZAS = '';
			$scope.isPieza = false;
		}
	}

	$scope.actStockMax = function() {
		$scope.datosRegistro.N_MAXIMO = undefined;	
	}

	function validaRegistro(tabla) {
		var isValido = true;
		var msg = undefined;
		switch (tabla) {
			case 'A': 	if ($scope.datosRegistro.NF_UNIDAD == undefined) msg = 'ESPECIFIQUE LA UNIDAD DE MEDIDA DEL ARTÍCULO';
						else
							if ($scope.datosRegistro.N_PIEZAS == undefined) msg = 'INGRES EL NÚMERO DE PIEZAS EN LAS QUE SE DIVIDE EL ARTÍCULO';
							else
								if ($scope.datosRegistro.N_CANTIDAD_UNIDOSIS == undefined) msg = 'INGRESE LA CANTIDAD DE POR UNIDOSIS DEL ARTÍCULO';
								else
									if ($scope.datosRegistro.NF_UNIDAD_MEDIDA_UNIDOSIS == undefined) msg = 'ESPECIFIQUE LA UNIDAD DE MEDIDA DE LA UNIDOSIS';
									else
										if ($scope.datosRegistro.NF_GRUPO == undefined) msg = 'ESPECIFIQUE EL GRUPO AL QUE PERTENECE EL ARTÍCULO';
										else
											if ($scope.datosRegistro.D_PORCENTAJE == undefined) msg = 'ESPECIFIQUE EL PORCENTAJE';
											else
												if ($scope.datosRegistro.N_IVA == undefined) msg = 'ESPECIFIQUE EL IVA';
												else
													if ($scope.datosRegistro.N_MINIMO == undefined) msg = 'INGRESE EL STOCK MÍNIMO';
													else
														if ($scope.datosRegistro.N_MAXIMO == undefined) msg = 'INGRESE EL STOCK MÍNIMO';
														else
															if ($scope.datosRegistro.N_PUNTO_REORDEN == undefined) msg = 'INGRESE EL PUNTO DE INFLEXIÓN';
						if (msg == undefined) {							
							var limInf = parseInt($scope.datosRegistro.N_MINIMO);
							var limSup = parseInt($scope.datosRegistro.N_MAXIMO);
							var puntoR = parseInt($scope.datosRegistro.N_PUNTO_REORDEN);
							if (limSup < limInf) msg = "EL STOCK MÁXIMO NO PUEDE SER MENOR AL STOCK MÍNIMO";
							else
								if (puntoR < limInf || puntoR > limSup) msg = "EL PUNTO DE INFLEXIÓN DEBE ESTAR ENTRE EL STOCK MÍNIMO Y EL STOCK MÁXIMO"
						}
						break;

			case 'D':   if ($scope.datosRegistro.C_CLAVE == undefined) msg = 'ESPECIFIQUE TIPO DE DEPARTAMENTO';
						else
							if ($scope.datosRegistro.sEstatusRegistro == undefined) msg = 'ESPECIFIQUE EL ESTATUS DEL DEPARTAMENTO';
						break;

			case 'G': 	if ($scope.datosRegistro.nfCuentaAbono == undefined) msg = 'ESPECIFIQUE LA CUENTA DE ANONO';
						else
							if ($scope.datosRegistro.nfCuentaCargo == undefined) msg = 'ESPECIFIQUE LA CUENTA DE CARGO';
						break;

			case 'P': 	if ($scope.datosRegistro.C_RFC == undefined) msg = 'INGRESE EL RFC O VERIFIQUELO';
						else
							if ($scope.datosRegistro.N_CODIGO_POSTAL == undefined) msg = 'INGRESE EL CODIGO POSTAL O VERIFIQUELO';
							else
								if ($scope.datosRegistro.NF_ENTIDAD_FEDERATIVA == undefined) msg = 'ESPECIFIQUE ES ESTADO';							
						break;
		}
									
		if (msg != undefined) {
			isValido = false;
			mensajes.alerta('ERROR',msg,'ACEPTAR!');
		}
		return isValido;
	}
	
	$scope.validaPuntoReorden = function() {
		if ($scope.datosRegistro.N_MINIMO != undefined && $scope.datosRegistro.N_MAXIMO != undefined && $scope.datosRegistro.N_PUNTO_REORDEN != undefined) {
			var limInf = parseInt($scope.datosRegistro.N_MINIMO);
			var limSup = parseInt($scope.datosRegistro.N_MAXIMO);
			var puntoR = parseInt($scope.datosRegistro.N_PUNTO_REORDEN);
			if ( (puntoR < limInf) || (puntoR > limSup) )
				mensajes.alerta('ERROR',"EL PUNTO DE REORDEN NO PUEDE SER MENOR AL STOCK MÍNIMO, NI MAYOR AL STOCK MÁXIMO",'ACEPTAR!');
		}
	}

	$scope.cambiaTablaArticulos = function(estatus) {
		if (estatus) 
			$scope.registros = _.filter($scope.datos, function(articulo) { return  !articulo.S_ESTATUS_REGISTRO; });			
		else
			$scope.registros = _.filter($scope.datos, function(articulo) { return  articulo.S_ESTATUS_REGISTRO; });			

	}

	$scope.validaExistencia = function(estatus) {
		console.log(estatus);
		if (!estatus) {
			console.log($scope.datosRegistro.N_EXISTENCIAS_ALMACEN);
			if ($scope.datosRegistro.N_EXISTENCIAS_ALMACEN > 0) {
				$scope.datosRegistro.S_ESTATUS_REGISTRO = true;
				mensajes.alerta('ERROR','EL ARTICULO NO SE PUEDE CAMBIAR DE ESTATUS (CUENTA CON EXISTENCIA)','ACEPTAR!');
			}
		}
	}

	$scope.toggleCtaPaciente = function(valor) {
		$scope.datosRegistro.N_CUENTA_PACIENTE = !valor;
	}

	$scope.toggleActivoInterno = function(valor) {
		$scope.datosRegistro.N_ACTIVO_INTERNO = !valor;
	}


  });
