
/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AdquisicionesArticulosCtrl
 * @description
 * # AdquisicionesArticulosCtrl
 * Controller of the expediente
 */

(function(){
'use strict';
angular.module('expediente')
.controller('AdquisicionesArticulosCtrl',['$scope','usuario','adquisiciones','$q','$timeout',



  function ($scope,usuario,adquisiciones,$q,$timeout) {

    $scope.campos = [{descripcion : "C_CLAVE",       validar: false, campo:"" },
                     {descripcion : "C_DESCRIPCION", validar: false, campo:"" }];

      $scope.articulos=undefined;
    	$scope.query = {
    	    filter: '',
    	    order: 'cDescripcion',
    	    limit: 5,
    	    page: 1
    	};
      $scope.data={};
    	$scope.onOrderChange = function(page, limit) {
    			    var deferred = $q.defer();
    			    $timeout(function () {

    			    	deferred.resolve();
    			    }, 2000);

    			    return deferred.promise;
      };

    	$scope.change = function(){
          $scope.query.page = 1;
      };

      function getDatosArticulo(){
        var url= "grupo/";
        adquisiciones.getDatos(url)
          .then(function(data) {
            $scope.grupos=[data.data];
            console.log(data.data);
          })
          .catch(function(err) {
            console.log(err);
          });
      };
      function getCuentaAbono(){
        var url= "cuenta/abono/";
        adquisiciones.getDatos(url)
          .then(function(data) {
            $scope.catCuentaAbono=[data.data];
            console.log(data.data);
          })
          .catch(function(err) {
            console.log(err);
          });
      };
      function getCuentaCargo(){
        var url= "cuenta/cargo/";
        adquisiciones.getDatos(url)
          .then(function(data) {
            $scope.catCuentaCargo=[data.data];
            console.log(data.data);
          })
          .catch(function(err) {
            console.log(err);
          });
      };

      getDatosGrupos();
      getCuentaAbono();
      getCuentaCargo();

      $scope.click = function(){
        $scope.alta = true;
      };

      $scope.guardar = function(){
        console.log($scope.data);
        $scope.alta = true;
        var url= "grupo/cuenta/";
        adquisiciones.postDatos(url,$scope.data)
        .then(function(data) {
          console.log($scope.data);
        })
        .catch(function(err) {
          console.log(err);
        });
      };
      $scope.regresar = function(){
        $scope.alta = false;
      };



    }]);
})();
