/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:AdquisicionesGruposCtrl
 * @description
 * # AdquisicionesGruposCtrl
 * Controller of the expediente
 */
(function(){
'use strict';
angular.module('expediente')
.controller('AdquisicionesGruposCtrl',['$scope','usuario','adquisiciones','$q','$timeout',
  function ($scope,usuario,adquisiciones,$q,$timeout) {

      $scope.grupos=undefined;
    	$scope.query = {
    	    filter: '',
    	    order: 'cDescripcion',
    	    limit: 5,
    	    page: 1
    	};

      $scope.data={};

    	$scope.onOrderChange = function(page, limit) {
    			    var deferred = $q.defer();
    			    $timeout(function () {
    			    	deferred.resolve();
    			    }, 2000);

    			    return deferred.promise;
      };

    	$scope.change = function(){
          $scope.query.page = 1;
      };

      function getDatosGrupos(){
        var url= "grupo/";
        adquisiciones.getDatos(url)
          .then(function(data) {
            console.log("************")
            console.log(data);
            $scope.grupos=[data.data];
            console.log(data.data);
          })
          .catch(function(err) {
            console.log(err);
          });
      };

      function getCuentaAbono(){
        var url= "cuenta/abono/";
        adquisiciones.getDatos(url)
          .then(function(data) {
            $scope.catCuentaAbono=[data.data];
            console.log(data.data);
          })
          .catch(function(err) {
            console.log(err);
          });
      };

      function getCuentaCargo(){
        var url= "cuenta/cargo/";
        adquisiciones.getDatos(url)
          .then(function(data) {
            $scope.catCuentaCargo=[data.data];
            console.log(data.data);
          })
          .catch(function(err) {
            console.log(err);
          });
      };

      getDatosGrupos();
      getCuentaAbono();
      getCuentaCargo();

      $scope.click = function(){
        $scope.alta = true;
      };

      $scope.guardar = function(){
        console.log($scope.data);
        $scope.alta = true;
        var url= "grupo/cuenta/";
        adquisiciones.postDatos(url,$scope.data)
        .then(function(data) {
          console.log($scope.data);
        })
        .catch(function(err) {
          console.log(err);
        });
      };

      $scope.regresar = function(){
        $scope.alta = false;
        $scope.data={};
      };

      $scope.validarCargar=function(){
        var url="";
        switch(estatus){
          case 0:
                break;
           
          case 1:
                break;
        
          case 2:
                break;
           
          default:
                break;
        }
      };


      $scope.validarGuardar=function(){
        var url="";
        switch(estatus){
          case 0:
                break;
           
          case 1:
                break;
        
          case 2:
                break;
           
          default:
                break;
        }
      };

    

    }]);
})();
