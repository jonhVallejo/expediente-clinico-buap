
'use strict';
var app = angular.module('expediente')
		 angular.module('expediente').controller('AdquisicionesCatalogogeneralCtrl', function ($scope,$timeout, $q, $log, $mdToast, $mdDialog,$stateParams,catalogos,mensajes,usuario,$location,$state) {
          	/*******************VARIABLES UTILIZADAS PARA CUALQUIER CATÁLOGO**********************/
          	/*Variable para identificar un catálogo (cuando es un catálogo general) */
            $scope.idCatalogo = $stateParams.idCatalogo;
            /*Variable para identificar el usuario logeado*/
            var mUser =  usuario.usuario.NP_EXPEDIENTE;
          	$scope.data={nfUsuario:mUser};
          	$scope.datos={nfUsuario:mUser};
            /*Variables para visualizar determinados apartados */
            $scope.editar = false;
			$scope.alta = false;
			$scope.busqueda = true;
			/*Almacena datos a mostrar en la tabla*/
		 	$scope.registros = [];
			/*Se inicializan valores a la tabla*/
			/*********PONERLO EN UNA FUNCIÓN HACERLO FUNCIONAL PARA CAMAS Y SECCIONES**********/
			$scope.query = {
		    filter: '',
		    order: 'cDescripcion',
		    limit: 5,
		    page: 1
			};

			$scope.selected = [];
			/*"data" es la que permite recuperar los datos del html y se inicializa*/
			$scope.data = {};
			$scope.backData = {};//auxiliar
			var datos={};//auxiliar
			var band=true;//auxiliar utilizada para la función de eliminar
			/************************VARIABLES UTILIZADAS PARA UN CATÁLOGO EN PARTICULAR*************************/
			/*CATÁLOGO DE CAMAS Y SECCIONES*/
          	$scope.showTable=false;
          	//auxiliar para catálogos de secciones y camas, es el botón de alta
          	$scope.buttonAlta=true;
           	$scope.subSeccion=[];
          	$scope.catTipoCama=[];
	 	 	$scope.idTipo="";
	 	 	$scope.data.idPiso="";
	        $scope.data.idCategoria="";
	        $scope.data.idProducto="";
	        $scope.catSeccion = [];
	 		$scope.seccion="";
	 	 	$scope.arregloSubseccion=[];
          	/*CATÁLOGO DE SERVICIOS*/
          	//auxiliar para el catálogo de servicios el check de costo
          	var check=true;
          	$scope.cancelarAccion = false;
          	$scope.updateDesc = 0;
          	$scope.updateRango = 0;
			/***************************FUNCIONES GENERALES PARA CUALQUIER CATÁLOGO**********************************************/
			/*FUNCIONES PARA LA TABLA*/
			$scope.onOrderChange = function(page, limit) {
			    var deferred = $q.defer();
			    $timeout(function () {
			    	deferred.resolve();
			    }, 2000);

			    return deferred.promise;
			};
			$scope.change = function(){
		      $scope.query.page = 1;
		    };
		    $scope.setIdCatalogo= function(idCat){//Se asigna el idCatalogo cuando es un catalogo diferente a un catálogo general
		  		$scope.idCatalogo=idCat;
	        }
			//CLICK EN AGREGAR se oculta el apartado de busqueda y se activa el formulario de alta
			$scope.click = function(){
				$scope.alta = true;
				$scope.busqueda = false;
				$scope.data = {};
				if($scope.idCatalogo==23 || $scope.idCatalogo==21){
					//INVESTIGAR COMO LIMPIAR FORMULARIO 
		  			$scope.area="";
		  			$scope.showTable=false;
		  			inicializarSecciones();
				}
				inicializarTurnos();
	        }
	        $scope.cancelar = function(){
		  		$scope.busqueda = true;
		  		$scope.alta = false;
		  		$scope.editar = false;
		  		$scope.cancelarAccion = true;
		  		if($scope.idCatalogo==23  || $scope.idCatalogo==21){
		  			$scope.area="";
		  			$scope.registros=[];
		  			$scope.showTable=false;
		  			inicializarSecciones();
		  			inicializarCamas();
		  			$scope.catProductos=[];
		  		}
	        }
		    $scope.cargaTabla = function(urlCat) {
		    	$scope.idCatalogo=urlCat;
			    $scope.registros = [];//Cada que cargo una tabla requiere de nuevo valores por tanto limpio registros
			    $scope.editar = false;
				$scope.alta = false;
				$scope.busqueda = true;
			    catalogos.getCatalogos(urlCat)
			    .success(function(data){
	                $scope.registros=data;          
				})
				.error(function(data){
				    $scope.registros = [];
					mensajes.datoNoEncontrado();
				});
			};
			function inicializarTurnos (){
				$scope.updateDesc = 0;
				$scope.updateRango = 0;	
				$scope.data.cDescripcion = "";
	      		$scope.dHoraInicial = "";
	      		$scope.dHoraFinal = "";
			}
		    function addReg (registros,urlCat){
		    	if (!$scope.cancelarAccion) {
		    		    $scope.data.nfUsuario=parseInt(mUser);
		    		    var existe = _.filter($scope.registros, function(val){
	                          if (!urlCat!=13)
	                          	return $scope.data.cDescripcion === val.cDescripcion;
	                          else {
	                          	var existeRango = $scope.data.dHoraInicial === val.dHoraInicial && $scope.data.dHoraFinal === val.dHoraFinal;
								return ($scope.data.cDescripcion === val.cDescripcion && existeRango) || existeRango;	                                  	
	                          }
	                                  	
	                    });
					   	if (existe.length)
					   		if (urlCat!=13)
					   			mensajes.alerta('ERROR','EL REGISTRO YA EXISTE','ACEPTAR');
					   		else
					   			mensajes.alerta('ERROR','EL TURNO O EL RANGO YA EXISTE','ACEPTAR');
					   	else {
					   	   catalogos.guardaCatalogo(urlCat,$scope.data)
		    					.success(function(data){
		    						console.log("retornando del back");
		    						console.log(data);
		    						console.log(urlCat);
		    						if(data.res==0){
		    							mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
		    						}
		    						else{
		    							if(data.res==1){
		    								mensajes.alerta('CONFIRMACIÓN','SE HA AGREGADO EXITOSAMENTE','ACEPTAR');
		    								if(urlCat==21 ){//secciones
		    									tableSecciones();
		    								}else{
		    									if(urlCat==23 ){//camas
		    										console.log("CAMAS");
		    										tableCamas();
		    									}else{//cualquier otro catálogo
		    										catalogos.getCatalogos(urlCat).success(function(data){
		    											$scope.registros=data;
		    											$scope.showTable=true;
		    									    });
		    									}
		    								}
		    								//si es turnos
		    								inicializarTurnos();
		    								$scope.alta = false;//Se desactiva el formulario para agregar un nuevo registro
		    								$scope.busqueda = true;
		    								
		    							}
		    							else{
		    								if(data.res==2){
		    									mensajes.alerta('ALERTA','EL ELEMENTO QUE DESEA AGREGAR YA SE ENCUENTRA EN EL CATÁLOGO','ACEPTAR');
		    								}
		    								else{
		    									mensajes.alerta('ALERTA','LA CLAVE INGRESADA YA EXISTE','ACEPTAR');
		    								}
		    							}
		    							
		    						}
		    						
		    					})
								.error(function(data){
									mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
								});
					}
		    	} else{
		    		$scope.cancelarAccion = false;
		    		inicializarTurnos();
		    	}
		    };
		    $scope.editRow = function(){
			  	var alert;
			  	if ($scope.selected.length > 1) {
				  	alert = $mdDialog.alert().title('INFORMACIÓN').content('SÓLO DEBE SELECCIONAR UN REGISTRO').ok('CERRAR');
				    $mdDialog
				        .show( alert )
				        .finally(function() {
				            alert = undefined;
				        });
				    $scope.editar = false;
				    $scope.busqueda = true;
			  	} else {
			  		$scope.busqueda = false;
			  		$scope.editar = true;
					$scope.backData = $scope.selected[0];
					if($scope.idCatalogo==21 || $scope.idCatalogo==23){//CATÁLOGO DE SECCIONES O DE CAMAS 
						$scope.data.cDescripcion=$scope.backData.cDescripcion;
						$scope.data.ID=$scope.backData.ID;
						$scope.backData.idHospitalizacion=$scope.data.idHospitalizacion;	
						$scope.backData.idArea=$scope.data.idArea;
						if($scope.idCatalogo==23){
							console.log("1");
							$scope.loadCategorias();
							$scope.loadPiso();
							console.log("2");
							console.log($scope.backData);
							$scope.backData.idTipo=$scope.data.idTipo;
							$scope.data.idPiso=$scope.backData.idPiso;
							$scope.data.idCategoria=$scope.backData.idCategoria;
							$scope.data.idProducto=$scope.backData.idProducto;
							$scope.changeCategoria($scope.data.idCategoria);
							console.log("3");
						}
						else{
							$scope.arregloSubseccion.splice($scope.arregloSubseccion.length - 1,1);
						}		
							
			
					}
					else{//cualquier otro catalogo
						$scope.data = angular.copy($scope.backData);
						$scope.alta = false;
						if($scope.idCatalogo==13){
							getTurnos($scope.selected[0].dHoraInicial,$scope.selected[0].dHoraFinal);
						}
						if($scope.idCatalogo==2){
							$scope.data.idCategoria = $scope.selected[0].idCategoria;
							$scope.data.idProveedor = $scope.selected[0].idProveedor;
							getEstatusCosto($scope.selected[0].nCostoVariableDesc);
						}
					}
			  	}
			  	$scope.selected = [];
	        };

	        function updateReg (registros,urlCat,datos){
	         	if (!$scope.cancelarAccion) {
		        	var id=$scope.data.ID;
		        	$scope.datos=datos;
		        	$scope.datos.nfUsuario=parseInt(mUser);
		        	console.log("Registros");
		        	console.log($scope.registros);
		        	if(urlCat!=23){//DIFERENTE DE CAMAS
		        		var existe = _.filter(registros, function(val){ 
		                if (urlCat!=13)
		                  	return $scope.data.cDescripcion === val.cDescripcion;//
		                else {//CATÁLOGO DE TURNOS
		                  	if ($scope.updateDesc+$scope.updateRango == 1)
		                  		return ($scope.data.cDescripcion === val.cDescripcion);
		                  	else {
		                  		var existeRango = $scope.data.dHoraInicial === val.dHoraInicial && $scope.data.dHoraFinal === val.dHoraFinal;
								return ($scope.data.cDescripcion === val.cDescripcion && existeRango) || existeRango;	                                  		
		                  	}		  	
		                }
		                });
		                console.log(existe.length);
						if (existe.length){
						    if (urlCat!=13)
						   		mensajes.alerta('ERROR','EL REGISTRO YA EXISTE','ACEPTAR');
						   	else
						   		mensajes.alerta('ERROR','EL TURNO O EL RANGO YA EXISTE','ACEPTAR');
						}else{
							actualizar(urlCat,id,registros);
						}
		        	}
		        	else {
		        		actualizar(urlCat,id,registros);
					}
				} else {
					$scope.cancelarAccion = false;	
					$scope.updateDesc = 0;
	          		$scope.updateRango = 0;	
				}
		    };

		    function actualizar(urlCat,id,registros){
	        	catalogos.modificaCatalogo(urlCat+"/"+id,$scope.datos)
                .success(function(data){
                	if(data.res==0){
						mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
					}
					else{
					if(data.res==1){
						$scope.registros=registros;
                        $scope.registros[$scope.registros.indexOf($scope.backData)] = $scope.data;
                        $scope.editar = false;
                        $scope.busqueda = true;
                        mensajes.alerta('CONFIRMACIÓN','SE HA MODIFICADO EXITOSAMENTE','ACEPTAR');
                        $scope.updateDesc = 0;
                        $scope.updateRango = 0;	
                        $scope.showTable=true;	
                        if(urlCat==23){
                        	catalogos.getCatalogos('camas/1').success(function(data){
				       		$scope.catAreas = data;
				       		//console.log($scope.catAreas);
			   				});
                        }
					}
					else{
						if(data.res==2){
							mensajes.alerta('ALERTA','EL ELEMENTO QUE DESEA AGREGAR YA SE ENCUENTRA EN EL CATÁLOGO','ACEPTAR');
						}
						else{
							mensajes.alerta('ALERTA','LA CLAVE INGRESADA YA EXISTE','ACEPTAR');
						}
					}
					}
                })
			.error(function(data){
				mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
			});

		    }

			$scope.deleteRow = function(registros,urlCat){
				 mensajes.confirmacion('PRECAUCIÓN','DESEA ELIMINAR ELIMINAR EL(LOS) REGISTRO(S) SELECCIONADO(S)','OK')
	              .then(function() {
	              		$scope.editar = false;
						band=true;//Auxiliar para mostrar una sola vez el mensaje de "Exito a eliminar"
						$scope.registros=registros;
				 	 	$scope.selected.forEach(function(seleccion) {
					 	 	catalogos.eliminaCatalogo(urlCat+"/"+seleccion.ID+"/"+mUser)
							.success(function(data){
								$scope.registros.splice($scope.registros.indexOf(seleccion),1);
							  	$scope.selected = [];
				  		        $scope.busqueda = true;
				  			    $scope.alta = false;
				  			    if(band){
				  			   		 mensajes.alerta('CONFIRMACIÓN','SE HA ELIMINADO EXITOSAMENTE','ACEPTAR');
				  			   		 band=false;
				  			   		  if(urlCat==23){
			                            	catalogos.getCatalogos('camas/1').success(function(data){
								       		$scope.catAreas = data;
							   				});
			                            }
				  				}
				  			})
							.error(function(data){
								if(data.res !=0 ) 
									mensajes.alerta('ALERTA','EL REGISTRO ESTA ASOCIADO A INFORMACIÓN VIGENTE','ACEPTAR');
								else
									mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
								$scope.selected = [];
							});
					  	});
	              }).catch(function() {
	                $scope.selected = [];
	              });
			};

			/***********************FUNCIONES PARA UN CATÁLOGO EN GENERAL***************************************/
			$scope.updateCatGeneral= function(registros,urlCat) {
		  		datos=$scope.data;
		  		updateReg(registros,urlCat,datos);
		  	};
		  	$scope.addCatGeneral= function(registros,urlCat) {
		  		addReg(registros,urlCat);
		  	};
			/***********************FUNCIONES PARA EL CATÁLOGO DE SERVICIOS**************************************/
			$scope.setServicios= function(registros,idCat,agregar) {
				var categoria = _.filter($scope.catCategoria,function(e){
				   return e.ID+''===$scope.data.idCategoria+'';
				});
				var proveedor = _.filter($scope.catProveedor,function(e){
				   return e.ID+''===$scope.data.idProveedor+'';
				});

				$scope.data.categoria=categoria[0].cDescripcion;
				$scope.data.proveedor=proveedor[0].cDescripcion;
		  		setEstatusCosto();
		  		if(agregar){
		  			$scope.data.dCosto=parseInt($scope.data.dCosto);
				  	addReg(registros,idCat);
		  		}
		  		else{
		  			if($scope.backData.cClave==$scope.data.cClave){
		  				if($scope.backData.cDescripcion==$scope.data.cDescripcion){
		  					if($scope.data.dCosto!=$scope.backData.dCosto || $scope.data.nPorcentajeCondonacion!=$scope.backData.nPorcentajeCondonacion || $scope.data.nCostoVariable!=$scope.backData.nCostoVariable || $scope.data.idCategoria!=$scope.backData.idCategoria || $scope.data.idProveedor!=$scope.backData.idProveedor){
		  						$scope.datos = {ID:$scope.data.ID,dCosto:$scope.data.dCosto,idCategoria:$scope.data.idCategoria,idProveedor:$scope.data.idProveedor,nPorcentajeCondonacion:$scope.data.nPorcentajeCondonacion,proveedor:$scope.data.proveedor,categoria:$scope.data.categoria,nCostoVariable:$scope.data.nCostoVariable,nCostoVariableDesc:$scope.data.nCostoVariableDesc}
		  					}
		  					else{
		  						$scope.datos=$scope.data;
		  					}
		  				}else{
		  						$scope.datos=$scope.data;
		  				}

		  			}
		  			updateReg(registros,idCat,$scope.datos);
		  		}
		  	};
			function getEstatusCosto(nCostoVariableDesc) {
			     if(nCostoVariableDesc =="SI"){
			     	$scope.data.check=true;
			     	$scope.data.nCostoVariable=1;
			      }
			      else{
			     	$scope.data.check=false;
			     	$scope.data.nCostoVariable=0;
			      }
		  	};
		  	function setEstatusCosto() {
			     if($scope.data.check==true){
			     	$scope.data.nCostoVariable=1;
			     	$scope.data.nCostoVariableDesc="SI";
			      }
			      else{
			    	 $scope.data.nCostoVariable=0;
			    	 $scope.data.nCostoVariableDesc="NO";
			      }
		  	};
		   /************************FUNCIONES PARA EL CATÁLOGO DE TURNOS***************************************/
		  	$scope.setTurnos= function(registros,idCat,agregar) {
		  		$scope.data.dHoraInicial = moment($scope.dHoraInicial).format().substring(11, 19);
		  		$scope.data.dHoraFinal = moment($scope.dHoraFinal).format().substring(11, 19);
		  		if(agregar){
		  			addReg(registros,idCat);
		  		}
		  		else{
		  			var modificaciones = $scope.updateDesc + $scope.updateRango;
		  			if (modificaciones == 3)
		  				$scope.datos=$scope.data;
		  			else
		  				if (modificaciones == 1)
		  					$scope.datos = {cDescripcion:$scope.data.cDescripcion}
		  				else
		  					$scope.datos = {dHoraInicial:$scope.data.dHoraInicial,dHoraFinal:$scope.data.dHoraFinal}
		  				updateReg(registros,idCat,$scope.datos);
		  		}
		  		
		  	};
		  	function getTurnos (fInicial,fFinal,registros,id) {
		  		$scope.dHoraInicial=new Date("Sat Sep 26 "+fInicial);
		  		$scope.dHoraFinal=new Date("Sat Sep 26 "+fFinal);

		  	};
		  	$scope.inicializaHorario = function() {
				$scope.dHoraFinal = $scope.dHoraInicial;			
				$scope.updateRango = 2;
			};
		
		  	 /************************FUNCIONES PARA EL CATÁLOGO DE CAMAS**************************************/
		  	$scope.updateCamas= function(registros,idCat) {
		  		$scope.datos["idTipo"]=$scope.data.idTipo;
		  	/*	console.log("DATA");
		  		console.log($scope.data);
		  		console.log("BACKDATA");
		  		console.log($scope.backData);*/
		  		if($scope.data.cDescripcion!=$scope.backData.cDescripcion || $scope.data.idArea!=$scope.backData.idArea || $scope.data.idHospitalizacion!=$scope.backData.idHospitalizacion || $scope.data.idTipo!=$scope.backData.idTipo || $scope.data.idCategoria!=$scope.backData.idCategoria || $scope.data.idProducto!=$scope.backData.idProducto){
		  			$scope.datos=$scope.data;
		  			updateReg(registros,idCat,$scope.datos);
		  		}
		  		else{
		  			mensajes.alerta('ERROR','EL REGISTRO YA EXISTE','ACEPTAR');
		  		}
		  	};
		  	/**************************FUNCIONES PARA CARGAR CATÁLOGOS*****************************************/
		  	$scope.loadCategorias = function() {
				$scope.catCategoria = [];
				catalogos.getCatalogos('15').success(function(data){
					$scope.catCategoria=data;
				});
				
		    };
		 	$scope.loadProveedor = function() {
			    $scope.catProveedor = [];
			     catalogos.getCatalogos('14').success(function(data){
					$scope.catProveedor=data;
				});
		 	 };

		 	$scope.loadSeccion = function() {
			    $scope.catSeccion = [];
			     catalogos.getCatalogos('21').success(function(data){
					$scope.catSeccion=data;
				});
		 	};
		 	$scope.loadArea = function() {
			    $scope.catArea = [];
			     catalogos.getCatalogos('24').success(function(data){
					$scope.catArea=data;
				});
		 	};
		 	$scope.loadPiso = function() {
			    $scope.catPiso = [];
			     catalogos.getCatalogos('22').success(function(data){
					$scope.catPiso=data;
				});
		 	};
		 	$scope.loadTipoCama = function() {
			    $scope.catTiposCama = [];
			     catalogos.getCatalogos('25').success(function(data){
					$scope.catTiposCama=data;
				});
		 	};
		 	catalogos.getCatalogos('camas/1').success(function(data){
		       	$scope.catAreas = data;
		       	console.log("AREAS");
		       	console.log($scope.catAreas);
	   		});
		    /**************************FUNCIONES PARA CATÁLOGOS DE CAMAS Y SECCIONES*****************************************/
	   		function tableSecciones() {
	   			catalogos.getCatalogos('camas/1').success(function(data){
	   				var aux;
	       			$scope.catAreas = data;
	       			if($scope.seccion===""){//ES UNA SECCIÓN
	       				$scope.registros=$scope.catAreas[$scope.area].areas;
			   			$scope.showTable=true;
	       			}
	       			else{
       					aux=$scope.catAreas[$scope.area].areas[$scope.seccion];

	       				 if($scope.subSeccion.length>0){//TIENE SUBSECCIONES
	       				 	for(var i=0 ; i<$scope.subSeccion.length ; i++){
			       				if(aux.subAreas[$scope.subSeccion[i]] === undefined){
			       					aux=aux.subAreas;
			       				}else{
			       					aux=aux.subAreas[$scope.subSeccion[i]];
			       				}
		       				}
	       				 }else{
	       				 	$scope.registros=aux.subAreas;	
	       				 }
	       				 $scope.showTable=true;		
	       			}
       			});
       			console.log("PINTANDO TABLA DE SECCIONES");
       			console.log($scope.registros);
		 	};
		 	
		 	function tableCamas() {
		 		$scope.catProductos=[];
		 		$scope.catCategoria = [];
		 		var aux;
		 		var temporal;
	   			catalogos.getCatalogos('camas/1').success(function(data){
	       			$scope.catAreas = data;
	       			var i="";
	       			aux=$scope.catAreas[$scope.area].areas[$scope.seccion];
	       			if(aux.hasOwnProperty("subAreas")){
	       				for(var i=0 ; i<$scope.subSeccion.length ; i++){
	       					console.log(aux.subAreas[$scope.subSeccion[i]]);
		       				if(aux.subAreas[$scope.subSeccion[i]] === undefined){
		       					aux=aux.subAreas;
		       					$scope.catTipoCama=aux.tiposCama;
		       				}else{
		       					aux=aux.subAreas[$scope.subSeccion[i]];
		       					$scope.catTipoCama=aux.tiposCama;
		       				}
	       				}

	       			}else{
	       				$scope.catTipoCama=aux.tiposCama;
	       			}
	 
	       			if($scope.catTipoCama.length >0){
		       			for(var i=0 ; i<$scope.catTipoCama.length ; i++){
		       				if($scope.catTipoCama[i].ID === $scope.data.idTipo){
		       					$scope.idTipo=i;
		       					break;
		       				}
		   				}
	       			}
       				for(var x in aux.tiposCama){
	       				if(aux.tiposCama[x].ID === $scope.data.idTipo){
	       					$scope.registros =aux.tiposCama[x].camas;
	       					console.log("ALGOOOOOOOOOO");
	       					console.log($scope.registros);
		   					$scope.showTable=true;
		   					break;
	       				}
	       			}

       			});
       		
		 	};
		 
		 	$scope.deleteSeccion = function(registros,urlCat){
		 		var aux;
		 		band=true;
	   			$scope.selected.forEach(function(seleccion) {	
		 	 	aux=$scope.catAreas[$scope.area].areas;
       			if($scope.seccion===""){//ES UNA SECCIÓN DE UN AREA
       				for(var x in aux){
						if(aux[x].ID === seleccion.ID){
						   if(aux[x].tiposCama!==undefined){
	   							mensajes.alerta('ALERTA','NO SE PUEDE ELIMINAR LA SECCIÓN YA QUE TIENE CAMAS','ACEPTAR');
	   							$scope.selected = [];
	   							return;
			       			}else{
			       				aux=$scope.catAreas[$scope.area].areas[x].subAreas;
							    break;
			       			}
							
	       				}
       				}
       			}
       			else{//SUBSECCIONES DE ESA SECCION
       				aux=$scope.catAreas[$scope.area].areas[$scope.seccion].subAreas;
     			}
	       			if(aux==undefined){
						$scope.deleteRow(registros,urlCat);
					}else{
						for(var x in aux){		
	       					if(aux[x].subAreas!==undefined){
								aux=aux.subAreas;
	       					}
	       					else{
	       						if(aux[x].tiposCama!==undefined){
	       							mensajes.alerta('ALERTA','NO SE PUEDE ELIMINAR LA SECCIÓN YA QUE TIENE CAMAS','ACEPTAR');
	       							$scope.selected = [];
	       							break;
	       						}	
	       						else
	       							$scope.deleteRow(registros,urlCat);
	       					}
       					}	
					}
		
				});
			}
			function inicializarSecciones() {
				$scope.catSeccion = [];
		 		$scope.seccion="";
		 	 	$scope.arregloSubseccion=[];
		 	 	$scope.registros=[];
		 	};
		 	function inicializarCamas() {
		 	 	$scope.catTipoCama=[];
		 	 //	$scope.catTiposCama = [];
		 	 	$scope.idTipo="";
		 	 	/*$scope.data.idPiso="";
		        $scope.data.idCategoria="";
		        $scope.data.idProducto="";*/
       			
		 	};
		 		

			$scope.changeArea = function(){
				console.log("Change Area");
				inicializarCamas();
		        inicializarSecciones();		        
				$scope.catSeccion = $scope.catAreas[$scope.area].areas;
				$scope.data.idHospitalizacion=$scope.catAreas[$scope.area].ID;//se requiere para agregar o editar un elemento
				//Antes de mostrar en tabla verificar si se va insertar el elemento, ya que al agregar no se muestra tabla
				if($scope.catAreas[$scope.area].areas!=undefined)
				 	$scope.registros=$scope.catAreas[$scope.area].areas;
				if($scope.alta){
					$scope.showTable=false;
				}
				else{//busqueda o editar
					if($scope.idCatalogo==23){//Catálogo de Camas
						if($scope.editar)
							$scope.showTable=true;
						else
							$scope.showTable=false;
					}else{
						
							$scope.showTable=true;
						
					}	
				}
		    }
		   

		    $scope.changeSeccion = function(){
		    	inicializarCamas();
		        $scope.arregloSubseccion = [];
		        $scope.subSeccion[$scope.subSeccion.length-1]="";
	            $scope.data.idArea=$scope.catAreas[$scope.area].areas[$scope.seccion].ID;
	          		if($scope.catAreas[$scope.area].areas[$scope.seccion].subAreas===undefined){//NO TIENE SUBSECCIONES
				        $scope.catTipoCama=$scope.catAreas[$scope.area].areas[$scope.seccion].tiposCama;
						if(!$scope.alta){//BUSQUEDA O EDITAR
							if($scope.editar)
								$scope.showTable=true;
							else
								$scope.showTable=false;
							if($scope.idCatalogo==23){//Catálogo de Camas
			    				if($scope.catTipoCama===undefined || $scope.catTipoCama.length===0){//NO tiene camas
			    					if(!$scope.editar)
							     		mensajes.alerta('ALERTA','NO SE ENCONTRARON CAMAS','ACEPTAR');	
							    }  
			    			}
		    				else{//Catálogo de Secciones
		    					mensajes.alerta('ALERTA','NO SE ENCONTRARON SECCIONES','ACEPTAR');
		    				}		
						}
						else{//ALTA
							if($scope.catTipoCama!==undefined){//Tiene camas
								if($scope.idCatalogo!=23){//Catálogo de Secciones  si hay camas no puede agregar una seccion
									mensajes.alerta('ALERTA','NO SE PUEDE AGREGAR LA SECCION YA QUE TIENE CAMAS ASIGNADAS','ACEPTAR');
									//FALTA DESACTIVAR BOTON DE ENVIAR HASTA QUE CAMBIE POR UN AREA QUE NO TENGA HIJOS
									$scope.buttonAlta=false;
								}
			    			}else{
			    				$scope.registros=$scope.catAreas[$scope.area].areas;
			    				$scope.buttonAlta=true;
			    			}	
					}
				 		
					}else {//TIENE SUBSECCIONES
							$scope.arregloSubseccion.push($scope.catAreas[$scope.area].areas[$scope.seccion].subAreas);
							if($scope.editar)
								$scope.showTable=true;
							else{
								if($scope.idCatalogo==23){//camas
									 $scope.showTable=false;
								}else{//secciones
									if($scope.alta){
				 						$scope.showTable=false;
									}else{
										
										$scope.registros=$scope.catAreas[$scope.area].areas[$scope.seccion].subAreas;//tabla
										$scope.showTable=true;
									}
								}

							}		
				}
		    }
			$scope.changeSubSeccion = function(key,index){
				inicializarCamas();
				$scope.subSeccion.splice(getIndiceCombo(key)+1 ,$scope.subSeccion.length - 1);
				$scope.arregloSubseccion.splice(getIndiceCombo(key) + 1 ,$scope.arregloSubseccion.length - 1);
			    $scope.data.idArea=$scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].ID; //se requiere para  editar un elemento
		     		   if ($scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].subAreas!== undefined ){//TIENE SUBSECCIONES
			   		   		$scope.arregloSubseccion.push([$scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].subAreas]);
			   		   		if($scope.editar)
								$scope.showTable=true;
							else{
								if($scope.idCatalogo==23){
					    				$scope.showTable=false;
					    		}else{
					    			if($scope.alta){
				   							$scope.showTable=false;
				   					}else{
			   						  $scope.registros=[$scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].subAreas];
						       		  $scope.showTable=true;
				   					}
						    	}
							}
				    		
					    }else{//YA NO EXISTEN SUBSECCIONES VERIFICO SI TIENE CAMAS
					    		$scope.catTipoCama=$scope.arregloSubseccion[$scope.arregloSubseccion.length - 1][index].tiposCama;
								
								if(!$scope.alta){//busqueda
									if($scope.editar){
										$scope.showTable=true;
									}
									else{
										if($scope.idCatalogo==23){//Catálogo de Camas
											if($scope.catTipoCama===undefined || $scope.catTipoCama.length===0 ){
												
										     	mensajes.alerta('ALERTA','NO SE ENCONTRARON CAMAS','ACEPTAR');
										     }
						    			}
					    				else{//catalogo de Secciones
					    					mensajes.alerta('ALERTA','NO SE ENCONTRARON SUBSECCIONES','ACEPTAR');
					    				}

									}
											
								}
								else{//alta
									if($scope.catTipoCama!==undefined){
										if($scope.idCatalogo!=23){//Catálogo de Secciones  si hay camas no puede agregar una seccion
											mensajes.alerta('ALERTA','NO SE PUEDE AGREGAR LA SECCION YA QUE TIENE CAMAS ASIGNADAS','ACEPTAR');
											//FALTA DESACTIVAR BOTON DE ENVIAR HASTA QUE CAMBIE EL AREA ya que tiene camas asignadas
											$scope.buttonAlta=false;
										}
					    			}
					    			else{
					    				$scope.buttonAlta=true;
					    			}
					    		}
					    }
			}
			$scope.changeTipoCama = function(index){
				console.log("TABLA");
				console.log($scope.catTipoCama[index].camas);
		   		$scope.registros=$scope.catTipoCama[index].camas;
		   		$scope.showTable=true;
		   		$scope.data.idTipo=$scope.catTipoCama[index].ID;
			}
			$scope.changeCategoria = function(idCategoria){
				catalogos.getCatalogos('serviciosProductos/'+idCategoria).success(function(data){
					$scope.catProductos=data;
				});
			}
		    function getIndiceCombo (key) {
		  		for (var index = 0; index < $scope.arregloSubseccion.length; index++){
		  			if($scope.arregloSubseccion[index].$$hashKey ===  key){
		  				return index;
		  			}
         		}
		  	};
});



