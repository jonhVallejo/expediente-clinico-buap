(function(){
'use strict';
angular.module('expediente')
    .controller('AdquisicionesCuentapacienteCtrl',['$scope','usuario','$state','mensajes','$mdDialog','$q', '$http', 'reportes','adquisiciones','$localStorage','$location','peticiones','$stateParams',
    function ($scope,usuario,$state,mensajes,$mdDialog,$q, $http,reportes,adquisiciones,$localStorage,$location,peticiones,$stateParams) {

    	$scope.checkCuentaPaciente=false;
    	$scope.C_OBSERVACIONES="";
    	$scope.datosArticulosBusqueda=[];
    	$scope.datosArticulos=[];
    	$scope.catAlmacenes=[];
    	$scope.idProcedencia = $stateParams.idProcedencia;

    	$scope.datos={
    		paciente:{
	        nombre:undefined,
	        ID: undefined,
	        exp: undefined
      	}};

    	getDatosArticulos();
    	validarProcedencia();
    	getAlmacenes();

    	$scope.onOrderChange = function(page, limit) {
	        var deferred = $q.defer();
	        $timeout(function () {
	          deferred.resolve();
	        }, 2000); 
	        return deferred.promise;
	    };

	    $scope.query = {
	        order: 'fecha',
	        limit: 5,
	        page: 1
	    };

	    function setDatosPaciente(nombre, id, expediente){
	      $scope.datos.paciente.nombre=nombre;
	      $scope.datos.paciente.ID=id;
	      $scope.datos.paciente.exp=expediente;   
	    };

	    function getAlmacenes(){
	    	var url="almacen/";
			 adquisiciones.getDatos(url)
	          .then(function(data) {
	            $scope.catAlmacenes=data.data;
	          })
	          .catch(function(err) {
	            console.log(err);
	          });
	    };



    	function getDatosArticulos(){
			var url="articulo/";
			 adquisiciones.getDatos(url)
	          .then(function(data) {
	            $scope.datosArticulosBusqueda = data.data;
				$scope.datosArticulosBusqueda = _.filter($scope.datosArticulosBusqueda , function(e){
        			return e.N_CUENTA_PACIENTE == true;
        		});
	          })
	          .catch(function(err) {
	            console.log(err);
	          });
	    };

	    function validarIsCuentaPaciente(){
	    	var url="paciente/"+$localStorage.paciente.expediente;

			 adquisiciones.getDatos(url)
	          .then(function(data) {
	          	if(data.data.data[0].N_CUENTA_ACTIVA==1){
	          		setDatosPaciente($localStorage.paciente.nombrePaciente,
                          $localStorage.paciente.idexpediente,
                          $localStorage.paciente.expediente
                          );
	          		$scope.checkCuentaPaciente=true;
	          	}else{
	          		   console.log("no es cuenta paciente");
	          		   $state.go($localStorage.stateAnterior);
	          	}
    
	          })
	          .catch(function(err) {
	            console.log(err);
	          });

	    };

	    function validarProcedencia(){
	    	 if($scope.idProcedencia!=2){
	    		 validarIsCuentaPaciente();
	    	 }else
	    		$scope.checkCuentaPaciente=false;
	    };

	    $scope.addReg = function(idArticulo,cClave,cDescripcion,unidad) {
		 $scope.datosArticulos.push({nId: idArticulo, NF_ARTICULO: cClave, CDESCRIPCION: cDescripcion, NFUNIDAD:unidad, N_CANT_SOLICITADA: undefined, N_CANT_SURTIDA: undefined}); 
    	};

    	$scope.cancelar=function(){
    		$scope.datosArticulos=[];
    	};

    	$scope.deleteReg = function(index) {
			 mensajes.confirmacion('ADVERTENCIA','DESEA ELIMINAR EL REGISTRO','OK'). then(function() {
	          $scope.datosArticulos.splice(index,1);
	        }); 
	    };

    	$scope.guardar=function(){
    	 var aux={};
    	 if($scope.checkCuentaPaciente){
    	 	aux.ID_CUENTA_PACIENTE=$localStorage.paciente.idexpediente;
    	 	aux.NF_TIPO_SOLICITUD=3;
    	 }else{
    	 	aux.ID_CUENTA_PACIENTE=0;
    	 	aux.NF_TIPO_SOLICITUD=4;
    	 }
    		
    		aux.NF_ALMACEN=$scope.NF_ALMACEN;
    		//$localStorage.usuario.NP_EXPEDIENTE
    		aux.NF_USUARIO=0;
    		aux.C_OBSERVACIONES=$scope.C_OBSERVACIONES;
    		aux.C_ARTICULOS=$scope.datosArticulos;

    		 var url= "/requisicion/articulos/";
	        adquisiciones.postDatos(url,aux)
	        .then(function(data) {
	          if(data.res!=0)
	          	 mensajes.confirmacion('ALERTA','SE AGREGO EXITOSAMENTE','OK');
	        })
	        .catch(function(err) {
	          console.log(err);
	        });
    		
    	};
    	$scope.changeCantidad=function(index,valor){
    		$scope.datosArticulos[index].N_CANT_SURTIDA=valor;
    	};
       /*************************FUNCIONES PARA BÚSQUEDA***********************************************/
	    $scope.querySearch = function(texto) {
		    var defered = $q.defer();
		    var promise = defered.promise;
		    var arraySearch = $scope.cargarArticulos(texto,$scope.datosArticulosBusqueda, [{descripcion : "C_DESCRIPCION", validar: false, campo:"" }]);

		  defered.resolve(arraySearch);
		  return promise;
		};

    	$scope.cargarArticulos= function(filtro,data,campos){
		    if(data!==undefined){
		      var result = peticiones.filtrar(filtro, data, campos);
		      var a = result.map(function (serv){
		        return {
		          CCLAVE : (serv.C_CLAVE),
		          CDESCRIPCION : (serv.C_DESCRIPCION),
		          NFUNIDAD:(serv.C_UNIDAD),
		          display: (serv.C_DESCRIPCION ).toUpperCase(),
		          NIVA:(serv.N_IVA),
		          NID:(serv.nId)

		        };
		      });
		      return a;
		    }
		};
		$scope.selectedItemChange = function(item) {
		    if(item!==undefined){
	    		$scope.art = item;
	    		$scope.addReg(item.NID,item.CCLAVE,item.CDESCRIPCION,item.NFUNIDAD);
	    		$scope.searchText = "";
		    }
		};

    }]);
})();

