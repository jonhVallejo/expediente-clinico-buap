'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:TablacontrollerCtrl
 * @description
 * # TablacontrollerCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('TablacontrollerCtrl', ['$scope','$timeout', '$q', '$log', '$mdToast', '$mdDialogfunction', function ($scope,$timeout, $q, $log, $mdToast, $mdDialog) {
    $scope.selected = [];

		$scope.query = {
		    filter: '',
		    order: 'id',
		    limit: 5,
		    page: 1
		};			

		

		$scope.registros = [];

    	$scope.registros = [
		  {id: 1,  descripcion: 'Especialidad 1'},
		  {id: 2,  descripcion: 'Especialidad 2'},
		  {id: 3,  descripcion: 'Especialidad 3'},
		  {id: 4,  descripcion: 'Especialidad 4'},
		  {id: 5,  descripcion: 'Especialidad 5'},
		  {id: 6,  descripcion: 'Especialidad 6'},
		  {id: 7,  descripcion: 'Especialidad 7'},
		  {id: 8,  descripcion: 'Especialidad 8'},
		  {id: 9,  descripcion: 'Especialidad 9'},
		  {id: 10,  descripcion: 'Especialidad 10'}
		];

		
		$scope.onOrderChange = function(page, limit) {
		    var deferred = $q.defer();
		    
		    $timeout(function () {
		      deferred.resolve();
		    }, 2000);
		    
		    return deferred.promise;
		  };

		  
		$scope.deleteRowCallback = function(){

		  	$scope.selected.forEach(function(seleccion) {
		  		$scope.registros.splice($scope.registros.indexOf(seleccion),1);	

		  	}); 

			$mdDialog.show({
          template:
            '<md-dialog> <BR>' +
            '  <md-content align="center">Hello!</md-content>' +
            '  <div class="md-actions">' +
            '    <md-button ng-click="$mdDialog.hide()">' +
            '      Close Greeting' +
            '    </md-button>' +
            '  </div>' +
            '</md-dialog>',
          
      	});

		    $scope.selected = [];
            
        };


  	function GreetingController($scope, $mdDialog, employee) {
	    
	    

	    $scope.closeDialog = function() {
	      $mdDialog.hide();
	    };
	  }
  }]);
