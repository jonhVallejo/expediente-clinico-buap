(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:HistclinicaHistoriconotasCtrl
 * @description
 * # HistclinicaHistoriconotasCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('HistclinicaHistoriconotasCtrl',['$scope','consultas', '$localStorage','mensajes','$q','reportes','pacientes','$timeout', 'peticiones',
  function ($scope,consultas,$localStorage,mensajes,$q,reportes,pacientes,$timeout, peticiones) {

	$scope.query = {
	    filter: '',
	    order: 'T_FECHA_CMD',
	    limit: 15,
	    page: 1
    };


    $scope.campos = [{descripcion : "CF_MEDICO"		,validar: false, campo:"" },
					 {descripcion : "C_ESPECIALIDAD",validar: false, campo:"" }];

    $scope.paciente=$localStorage.paciente;
    $scope.paciente.N_EDAD = pacientes.calculaEdad($scope.paciente.f_nacimiento);

    $scope.filtraTabla = function(filtro) {
		$scope.registros = peticiones.filtrar(filtro,$scope.datos,$scope.campos);
	}

	consultas.notasPaciente($localStorage.paciente.expediente)
	//consultas.notasPaciente(10000)
	.success(function(data){
		if(data.length>0){
			_.each(data, function(reg){ reg.FECHA = moment(reg.T_FECHA_CMD).format("YYYYMMDD") });	
			$scope.datos = data;
			$scope.registros = data;
		}
		else {
			mensajes.alerta("AVISO","EL PACIENTE NO CUENTA CON NOTAS MÉDICAS ANTERIORES","ACEPTAR");
		}
	})
	.error(function(err){});

	$scope.abrirNota = function(nota){
		$scope.NF_NOTAMEDICA = nota.NF_NOTA_MEDICA;
		$localStorage.NF_NOTAMEDICA = $scope.NF_NOTAMEDICA;
    	$scope.selectedIndex2 = 0;
    	if(nota.NF_NOTA_MEDICA != undefined)
	    	consultas.getNotaMedicaUrgencias(nota.NF_NOTA_MEDICA) 
	    	.success(function(data){
	    		loadData(data);
		    	$scope.existInfo = true;
		    	$scope.isUrgencias = false;

		    })
	    	.error(function(data){
	    		defered.reject();
	    	});
	    else mensajes.alerta("AVISO","NO SE PUEDE RECUPERERAR LA NOTA MÉDICA","ACEPTAR");
	};

	function loadData(data){
	    
   		$scope.idOrdenMedica = data[0].nota.NF_NOTA_MEDICA;
		var defered = $q.defer();
		var promise = defered.promise;
		data = data[0];
			if(data != undefined){

			if(data.fisicas != undefined){
				$scope.nuevo={
					'EXPLORACION_FSC':{
						'N_TSN_ART_SISTOLICA':data.fisicas.N_TSN_ART_SISTOLICA,
						'N_TSN_ART_DIASTOLICA':data.fisicas.N_TSN_ART_DIASTOLICA,
						'N_FRC_CARDIACA':data.fisicas.N_FRC_CARDIACA,
						'N_FRC_RESPIRATORIA':data.fisicas.N_FRC_RESPIRATORIA,
						'N_TEMPERATURA':data.fisicas.N_TEMPERATURA,
						'N_PESO':data.fisicas.N_PESO,
						'N_TALLA':data.fisicas.N_TALLA,
						'C_DESCRIPCION':data.fisicas.C_DESCRIPCION,
						'N_SATURACION':data.fisicas.N_SATURACION,
						'N_IMC':data.fisicas.N_IMC,
						'C_CLASIFICACION':data.fisicas.C_CLASIFICACION,
						'C_OBS_ENFERMERIA':data.fisicas.C_OBS_ENFERMERIA,
						'EXPLORACION':data.fisicas.C_DESCRIPCION
					}
				};
			}
			$scope.nuevo.C_INTERPRETACION = data.nota.C_INTERPRETACION_LABS;
			$scope.nuevo.RESUMEN_DEL_INTERROGATORIO = {'C_ANAMNESIS':data.nota.C_ANAMNESIS, 'C_MOTIVO_DE_CONSULTA' : data.nota.C_MOTIVO_DE_CONSULTA};
			$scope.nuevo.NOTAS_MEDICAS = {'C_PRONOSTICO':data.nota.C_PRONOSTICO};

			$scope.nuevo.C_PROCEDIMIENTOS_REALIZADOS =  data.nota.C_PROCEDIMIENTOS_REALIZADOS;
			$scope.nuevo.C_PLAN = data.nota.C_PLAN;
			$scope.nuevo.C_DESTINO_PACIENTE = data.nota.C_DESTINO_PACIENTE;
			if(data.fisicas != undefined)
				$scope.nuevo.EXPLORACION_FSC.C_ESTADO_MENTAL = data.fisicas.N_ESTADO_MENTAL;
			$scope.nuevo.EXPLORACION_FSC.C_ANALISIS = data.nota.C_ANALISIS;
			if(data.laboratorio.length>0){ //Datos de laboratorio
				var estudiosL = _.map(data.laboratorio, function (el, key) {
                    return{
                    	'cDescripcion' : el.C_DESCRIPCION,
                    	'ID' : el.NP_SRV_PRODUCTO,
                    	'C_MOTIVO_SOLICITUD' : el.C_MOTIVO_SOLICITUD,
                    	'dCosto' : el.D_COSTO
                    }
                });
				$scope.datosEstudios = estudiosL;
				$scope.isEstudio = true;
				$scope.canDelete = true;
                if($scope.datosEstudios.length > 0){
                	$scope.existLaboratorio = true;
                }
                else{
                	$scope.existLaboratorio = false;
                }
			}
			//DIAGNOSTICO PRIMARIO
			$scope.nuevo.DIAGNOSTICOPRINCIPAL = data.nota.NF_CIE_10_C_DESCRIPCION;
			if($scope.existLaboratorio && $scope.nuevo.DIAGNOSTICOPRINCIPAL != undefined){
				$scope.existSolicitudLaboratorio = true;
			}
			else {
				$scope.existSolicitudLaboratorio = false;
			}

			//Pronóstico
			$scope.NF_ESTATUS1 = false;
			$scope.NF_ESTATUS2 = false;
			$scope.NF_ESTATUS3 = false;
			$scope.NF_ESTATUS4 = false;
			if(data.nota.N_ESTATUS_PRONOSTICO != undefined){
				if(data.nota.N_ESTATUS_PRONOSTICO === 1){$scope.NF_ESTATUS1 = true; $scope.NF_ESTATUS3 = true;}
				else if(data.nota.N_ESTATUS_PRONOSTICO === 2){$scope.NF_ESTATUS1 = true; $scope.NF_ESTATUS4 = true;}
				else if(data.nota.N_ESTATUS_PRONOSTICO === 3){$scope.NF_ESTATUS2 = true; $scope.NF_ESTATUS3 = true;}
				else if(data.nota.N_ESTATUS_PRONOSTICO === 4){$scope.NF_ESTATUS2 = true; $scope.NF_ESTATUS4 = true;}
			}

			//ORDEN MÉDICA
			$scope.C_DIETA = "";
			$scope.C_SOLUCIONES = "";
			$scope.C_MEDIDAS_GENERALES = "";
			$scope.isOrden = false;
			if(data.nota.C_DIETA != undefined){
				$scope.C_DIETA = data.nota.C_DIETA;
				$scope.isOrden = true;
			}
			if(data.nota.C_SOLUCIONES != undefined){
				$scope.C_SOLUCIONES = data.nota.C_SOLUCIONES;
				$scope.isOrden = true;
			}
			if(data.nota.C_MEDIDAS_GENERALES != undefined){
				$scope.C_MEDIDAS_GENERALES = data.nota.C_MEDIDAS_GENERALES;
				$scope.isOrden = true;
			}

			$scope.isDiagnostico = false;
			if(data.cie.length > 0){
				$scope.datosSecundarios = _.map(data.cie, function (el, key) {
                    return{
                    	'C_DESCRIPCION' : el.C_DESCRIPCION,
                    	'NP_ID' : el.NF_CIE10
                    }
                });
				$scope.isDiagnostico = true;
			}
			else{
				$scope.datosSecundarios = [];
				$scope.isDiagnostico = false;
			}
			if($localStorage.paciente.tipo === 'EVOLUCION Y ALTA'){
				consultas.getDiagnosticoIngreso($localStorage.paciente.expediente)
				.success(function(data){
					if(data.length>0){
						$scope.nuevo.C_DIAGNOSTICO_INGRESO = data.C_ANAMNESIS;
					}
				})
				.error(function(data){
					mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
					defered.reject();
				});
			}
			$scope.isIndicacion = false;
			if(data.nota.C_FILE_RECETA != undefined && data.nota.C_MODULO != undefined){
				$localStorage.C_MODULO = data.nota.C_MODULO;
				$localStorage.C_FILE_RECETA = data.nota.C_FILE_RECETA;
				$scope.existReceta = false;
			}
			else{
				$localStorage.C_MODULO = undefined;
				$localStorage.C_FILE_RECETA = undefined;
				$scope.existReceta = false;
			}
			$scope.medicamentos = [];
			if(data.nota.NF_RECETA!=undefined){
				consultas.getIndicacionTereapeutica(data.nota.NF_RECETA)
		    	.success(function(data){
		    		if(data != undefined){
		    			$scope.medicamentos = data;
		    			$scope.isIndicacion = true;
		    			$scope.C_MODULO = $localStorage.C_MODULO;
		    			$scope.C_FILE_RECETA = $localStorage.C_FILE_RECETA;
		    			$scope.isCancel = false;
		    			$scope.existReceta = false;
		    		}
		    		defered.resolve({});
		    	})
		    	.error(function(data){
		    		defered.reject();
		    	});
			}
			
	    	defered.resolve({});
  		}
  		else {
  			defered.reject();
  		}
	}

	$scope.reprintReceta = function(){
    	reportes.getReporte(baseURL + 'getpdf/' + $localStorage.C_MODULO + '/' + $localStorage.C_FILE_RECETA , '_blank' , 'width=800, height=640');
    };

    $scope.reprintNotaMedica = function(){
    	reportes.getReporte(baseURL + 'reportenotasmedicas/' + $localStorage.NF_NOTAMEDICA , '_blank' , 'width=800, height=640');
    };
    $scope.parseFecha = function(date){
         return (date !== undefined) ? moment(date).format("DD/MM/YYYY") : '';
    };
    $scope.onOrderChange = function (page, limit) {
        var deferred = $q.defer();
        $timeout(function () {
            deferred.resolve();
        }, Math.random() * 2000);
        return deferred.promise;
    };

    
  }]);
})();