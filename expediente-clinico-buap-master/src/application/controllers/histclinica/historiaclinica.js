(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:HistclinicaHistoriaclinicactrlCtrl
 * @description
 * # HistclinicaHistoriaclinicactrlCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('HistoriaclinicactrlCtrl',['$scope','$location','historiaClinicaService','$q','cuestionarioService','peticiones','mensajes','medicos','usuario','$localStorage','pacientes','consultas','$state','blockUI','catalogos','$window','reportes',
    function ($scope,$location,historiaClinicaService,$q,cuestionarioService,peticiones,mensajes,medicos,usuario,$localStorage,pacientes,consultas,$state,blockUI,catalogos,$window,reportes) {
    var self                = this;
    var idEspecialidad = 0;
    var datoCita            = {};
    var ant                 = 0;

    $scope.fecha            = new Date();
    $scope.idNota           = "";

    self.observacion        = '';
    self.contesCuestionario = true;
    self.oCuestionarios     = [];
    datoCita                = $localStorage.paciente;
    self.mCuestionario      = {SECCION: []};
    self.mCuestionarioGeneral = {SECCION: []};
    $scope.procedenciaHistoria=$localStorage.procedenciaHistoria;
    $scope.procedenciaExpediente=$localStorage.procedenciaExpediente;
    $scope.secciones = historiaClinicaService.getSecciones();
    $scope.NOTA={};
    $scope.datos={};
    $scope.NP_TCUESTIONARIO=undefined;
    //Variables para el Pronóstico
    $scope.NF_ESTATUS1 = false;
    $scope.NF_ESTATUS2 = false;
    $scope.NF_ESTATUS3 = false;
    $scope.NF_ESTATUS4 = false;
    //blockUI.start();
    getEspecialidad();

      var mNom = ($localStorage.paciente.nombrePaciente == undefined) ? $localStorage.paciente.nombreCompleto: $localStorage.paciente.nombrePaciente;
      if($localStorage.procedenciaUrgencias == true){
         mNom = (mNom == undefined) ? $localStorage.paciente.C_NOMBRE + " " + $localStorage.paciente.C_PRIMER_APELLIDO + " " + $localStorage.paciente.C_SEGUNDO_APELLIDO : mNom;
      }
      var idPaciente= $localStorage.paciente.idPaciente;


    limpiar();



    //$scope.tiposDeSeccion = [{'np'}]

    getInicializar().then(function(){
      if(!$localStorage.procedenciaExpediente){
          if($localStorage.procedenciaUrgencias == true){
              $scope.idNota=$localStorage.notaMedica.NP_NOTA_MEDICA;
              getDatosFijos($scope.idNota);
          }else{
              consultas.getNotaMedica($localStorage.paciente.expediente,$localStorage.paciente.idCita)
             .success(function(data){

                  $scope.nota=data[0];
                  $scope.idNota=$scope.nota.nota.NF_NOTA_MEDICA;
              getDatosFijos($scope.idNota);
            })
            .error(function(data){
                  mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
            });
          }
      }
      else{

        $scope.idNota=$localStorage.idNota;
        getDatosFijos($scope.idNota);

      }
    });

    function getEspecialidad(){
      if(!$localStorage.procedenciaExpediente){
        medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
        .success(function(data){
          if(data === ''){
            mensajes.alerta('ERROR','ERROR INTERNO DEL SISTEMA INTENTE NUEVAMENTE','ACEPTAR');
          }
          else{
            idEspecialidad = data.ESPECIALIDADES.NF_ESPECIALIDAD;
            blockUI.stop();
          }
        })
        .error(function(data){
           blockUI.stop();
          mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR', 3000);
        });
      }else{
        idEspecialidad=$localStorage.idEspecialidad;
      }
    };

    pacientes.getPaciente()
      .success(function(data){
        if(data.success===true){
          $scope.isHombre           = (data.data[0].NF_SEXO == "MASCULINO") ? true : "";
          $scope.datos.genero= (data.data[0].NF_SEXO !== undefined) ? data.data[0].NF_SEXO  : "";
          $scope.datos.expedienteSiu=(data.data[0].NP_EXPEDIENTE_SIU!== undefined) ? data.data[0].NP_EXPEDIENTE_SIU  : " ";
          $scope.datos.fNac=(data.data[0].F_FECHA_DE_NACIMIENTO!== undefined) ? data.data[0].F_FECHA_DE_NACIMIENTO  : " ";
          $scope.datos.edad=(data.data[0].F_FECHA_DE_NACIMIENTO!== undefined) ? pacientes.calculaEdad(data.data[0].F_FECHA_DE_NACIMIENTO) :" ";
          $scope.datos.ficha.direccion += (data.data[0].C_CALLE) ? data.data[0].C_CALLE : "";
          $scope.datos.ficha.direccion += (data.data[0].C_COLONIA) ? data.data[0].C_COLONIA : "";
          $scope.datos.ficha.direccion += (data.data[0].C_NO_INTERIOR) ? data.data[0].C_NO_INTERIOR : "";
          $scope.datos.ficha.origen=(data.data[0].NF_ENTIDAD_FEDERATIVA) ? data.data[0].NF_ENTIDAD_FEDERATIVA :  "";
          $scope.datos.ficha.curp=(data.data[0].C_CURP!== undefined) ? data.data[0].C_CURP : "";
          $scope.datos.ficha.edoCivil=(data.data[0].NF_ESTADO_CIVIL!== undefined) ? data.data[0].NF_ESTADO_CIVIL : "";
          $scope.datos.ficha.ocupacion=(data.data[0].C_OCUPACION!== undefined) ? data.data[0].C_OCUPACION : " ";
          $scope.datos.ficha.telefono1=(data.data[0].C_NO_TELEFONO_UNO!== undefined) ? data.data[0].C_NO_TELEFONO_UNO :"";
          $scope.datos.ficha.telefono2=(data.data[0].C_NO_TELEFONO_DOS!== undefined) ? data.data[0].C_NO_TELEFONO_DOS :"";

          $scope.datos.ficha.religion = (data.data[0].C_RELIGION) ? data.data[0].C_RELIGION : "";
          $scope.datos.ficha.grupoEtnico = (data.data[0].C_ETNIA == " ") ? undefined : data.data[0].C_ETNIA ;


          $scope.secciones = $scope.secciones.map(function(data){
            data.activo = true;
             if(data.id == 5){
                data.activo = ($scope.isHombre) ? true : false;
             }
             if(data.id == 11 || data.id == 12){
                data.activo = !$scope.procedenciaHistoria;
             }
            return data;
          });

        }
        else{
          mensajes.alerta('ERROR','LO SENTIMOS, OCURRIÓ UN PROBLEMA','ACEPTAR');
        }
      })
      .error(function(data){
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR');
      });



    function getDatosFijos(NF_NOTA){
      if(!$localStorage.procedenciaExpediente){
        var url = "notamedica/urgencias/" + NF_NOTA;
      }else{
        var idexpediente=(datoCita.idexpediente.indexOf('/'))?datoCita.idexpediente.split("/")[0]:datoCita.idexpediente;
        var url= "expedienteClinico/historia/primeraNota/"+idexpediente;
      }
        var coma="";
        peticiones.getDatos(url)
        .then(function(data) {
          if(data[0].fisicas!==undefined){
            if(data[0].fisicas!==undefined){
              $scope.datos.exploracionfisica.diastolica = data[0].fisicas.N_TSN_ART_DIASTOLICA;
              $scope.datos.exploracionfisica.sostolica = data[0].fisicas.N_TSN_ART_SISTOLICA;
              $scope.datos.exploracionfisica.cardio = data[0].fisicas.N_FRC_CARDIACA;
              $scope.datos.exploracionfisica.respiratoria = data[0].fisicas.N_FRC_RESPIRATORIA;
              $scope.datos.exploracionfisica.temperatura = data[0].fisicas.N_TEMPERATURA;
              $scope.datos.exploracionfisica.peso = data[0].fisicas.N_PESO;
              $scope.datos.exploracionfisica.talla = data[0].fisicas.N_TALLA;
              $scope.datos.exploracionfisica.imc = data[0].fisicas.N_IMC;
              $scope.datos.exploracionfisica.saturacion=data[0].fisicas.N_SATURACION;
              $scope.datos.exploracionfisica.clasificacion = data[0].fisicas.C_CLASIFICACION;
              $scope.datos.exploracionfisica.observaciones = data[0].fisicas.C_OBS_ENFERMERIA;
              $scope.datos.diagnostico.descripcionPrincipal=data[0].nota.NF_CIE_10_C_DESCRIPCION;
              for(var x in data[0].cie){
                $scope.datos.diagnostico.descripcionSecundario= $scope.datos.diagnostico.descripcionSecundario+'\n'+data[0].cie[x].C_DESCRIPCION;
                coma=",";
              }
             if(data[0].nota.N_ESTATUS_PRONOSTICO != undefined){
                if(data[0].nota.N_ESTATUS_PRONOSTICO === 1){$scope.NF_ESTATUS1 = true; $scope.NF_ESTATUS3 = true;}
                else if(data[0].nota.N_ESTATUS_PRONOSTICO === 2){$scope.NF_ESTATUS1 = true; $scope.NF_ESTATUS4 = true;}
                else if(data[0].nota.N_ESTATUS_PRONOSTICO === 3){$scope.NF_ESTATUS2 = true; $scope.NF_ESTATUS3 = true;}
                else if(data[0].nota.N_ESTATUS_PRONOSTICO === 4){$scope.NF_ESTATUS2 = true; $scope.NF_ESTATUS4 = true;}
                $scope.nota={'C_PRONOSTICO':data[0].nota.C_PRONOSTICO};
              }
          }
        } else
              mensajes.alerta("","NO SE RECUPERARON DATOS DE EXPLORACIÓN FÍSICA ","OK");
        })
        .catch(function(err) {
            console.log(err);
        });
    }
    $scope.salir=function(){
     if($localStorage.procedenciaExpediente){
       $state.go("expedienteClinico");
     }else{
          if(usuario.perfil.clave == 'RESI')
            $state.go("bienvenido");
          else {
            $state.go("notaMedicaIngreso");
          }
      }
    };

    $scope.imprimir=function(){
      reportes.getReporte(baseURL + 'expedienteClinico/historiaPdf/' + $scope.idNota+"/"+ idEspecialidad, '_blank', 'width=1000, height=800');
    };
/**
 * Empieza la sección para los cuestionarios
 */
    var cuestionarioEspecifico = function(NP_TIPO_CUESTIONARIO, ID_ESPECIALIDAD, NF_PACIENTE, ID_NOTA, MEDICINA_GENERAL){
      var url="";
      var auxEspecialidad=(MEDICINA_GENERAL) ? 1020 : ID_ESPECIALIDAD;
      cuestionarioService.setContestarCuestionario(true);
       if($localStorage.procedenciaExpediente || $localStorage.procedenciaUrgencias == true){
             url = "cuestionario/historia/" + NP_TIPO_CUESTIONARIO + "/" +auxEspecialidad+"/"+ ID_NOTA;
       }
       else{
            url = "cuestionario/" + NP_TIPO_CUESTIONARIO + "/" + NF_PACIENTE + "/"+auxEspecialidad;
       }
      blockUI.start();

      peticiones.getDatos(url)
          .then(function(data) {
            blockUI.stop();
             if(data.SECCION){
               if(!$localStorage.procedenciaExpediente){//si procede de historia clinica o de agenda médica los campos siempre serán editables
                  self.contesCuestionarioGeneral = false;
                  self.contesCuestionario = false;
              }else{ //si viene de expediente clínico los campos deben ser "no editales"
                  self.contesCuestionarioGeneral = true;
                  self.contesCuestionario = true;
              }
              cuestionarioService.setCuestionario(data);
              if(MEDICINA_GENERAL){
                self.mCuestionarioGeneral = data

              }else{
                self.mCuestionario = data

              }
            }
        })
        .catch(function(err) {
              console.log(err);
              blockUI.stop();
        });
    };

    var limpiarCuestionario = function(){
      cuestionarioService.reset(self.mCuestionario);
      cuestionarioService.reset(self.mCuestionarioGeneral);
    }

    self.cambiarRadio = function(key_seccion,key_pregunta,bandera,MEDICINA_GENERAL){
        if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
              cuestionarioService.cambiarRadio(key_seccion,key_pregunta, bandera, self.mCuestionarioGeneral,MEDICINA_GENERAL);
            }else{
              cuestionarioService.cambiarRadio(key_seccion,key_pregunta, bandera, self.mCuestionario,MEDICINA_GENERAL);
            }
        };

    self.seleccionarRespuesta = function(key_seccion,key_pregunta,key_respuesta,MEDICINA_GENERAL){
       if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
              cuestionarioService.seleccionarRespuesta(key_seccion,key_pregunta, key_respuesta, self.mCuestionarioGeneral);
        }else{
              cuestionarioService.seleccionarRespuesta(key_seccion,key_pregunta, key_respuesta, self.mCuestionario);
        }
      };

    self.guardarRespuestas = function(MEDICINA_GENERAL){


      var cuestionarioFin = {};

      if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
        cuestionarioFin = self.mCuestionarioGeneral;
      }else{
        cuestionarioFin = self.mCuestionario;
      }
      
      //var cuestionarioFin = (MEDICINA_GENERAL) ? angular.toJson(self.mCuestionarioGeneral) : angular.toJson(self.mCuestionario);



      var result = cuestionarioService.revisarRespuestas(angular.fromJson(cuestionarioFin));
      if(result.correcta){
          var cuestionarioResult = cuestionarioService.crearCuestionarioFinal(angular.fromJson(result.cuestionario));
          cuestionarioResult.NF_MEDICO = usuario.usuario.NP_EXPEDIENTE
          cuestionarioResult.NF_PACIENTE = $localStorage.paciente.expediente.substring(0, $localStorage.paciente.expediente.length - 4)
          cuestionarioResult.NF_NOTA_MEDICA = $scope.idNota;
          if($scope.NP_TCUESTIONARIO!==undefined)
          cuestionarioResult.NP_TCUESTIONARIO = $scope.NP_TCUESTIONARIO;
          postRespuestas(cuestionarioResult,MEDICINA_GENERAL);
      }else{
        mensajes.alerta("ERROR","DEBE CONTESTAR TODAS LAS PREGUNTAS OBLIGATORIAS" ,"OK");
      }
    };

    var  postRespuestas = function(mData,MEDICINA_GENERAL){
     if(mData.CONTESTADO){//Put
        peticiones.putMethod('cuestionario/respuestas/',mData)
          .success(function(data){
            mensajes.alerta("","DATOS ACTUALIZADOS CORRECTAMENTE" ,"OK");
          })
          .error(function(data){
            mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR', 3000);
          });

      }else{//Post
          peticiones.postDatos('cuestionario/respuestas/',mData)
            .then(function(data) {
              $scope.NP_TCUESTIONARIO=data.id;
              if(MEDICINA_GENERAL){
                self.mCuestionarioGeneral.CONTESTADO = true;
              }else{
                self.mCuestionario.CONTESTADO = true;
              }
                mensajes.alerta("","DATOS ACTUALIZADOS CORRECTAMENTE" ,"OK");
            })
            .catch(function(err) {
                console.log(err);
            });
      }
    };

/**
 * Termina la sección de guardar los cuestionarios
 */

    $scope.guardarObs = function(seccion){
        $scope.datos[seccion].observaviones.unshift({descripcion:self.observacion,fecha:new Date()});
        self.observacion = '';
    };

    $scope.seleccionar = function(index){
      //var index = $scope.selectedItemSeccion;
     if (ant == index){
          $scope.secciones[index].visible = ($scope.secciones[index].visible) ? false : true;
      }
      else{
          $scope.secciones[ant].visible = false;
          $scope.secciones[index].visible = true;
      }
      ant = index;

      self.mCuestionario.SECCION.splice(0,self.mCuestionario.SECCION.length)
      self.mCuestionarioGeneral.SECCION.splice(0,self.mCuestionarioGeneral.SECCION.length);
      limpiarCuestionario();
      var NP_EXPEDIENTE_PACIENTE = (datoCita.idexpediente.indexOf('/'))?datoCita.idexpediente.split("/")[0]:datoCita.idexpediente;
      if($scope.secciones[index].buscarCuest && $scope.secciones[index].visible){
          //PETICIÓN PARA MEDICINA GENERAL
          cuestionarioEspecifico($scope.secciones[index].id,idEspecialidad,NP_EXPEDIENTE_PACIENTE, $scope.idNota, true);
          //PETICIÓN PARA EL CUESTIONARIO ESPECIFICO
          cuestionarioEspecifico($scope.secciones[index].id,idEspecialidad,NP_EXPEDIENTE_PACIENTE, $scope.idNota, false);

          self.mfecha = new Date("05/05/2015");
      }

    };


    function limpiar(){
      for(var x in  $scope.secciones)
         $scope.secciones[x].visible=false;
    };

   function getInicializar(){
    var defered = $q.defer();
    var promise = defered.promise;
    $scope.datos = {
      nombre : mNom ,
      ID : idPaciente,
      exp : datoCita.idexpediente,
      edad : undefined,
      fNac : undefined,
      genero : undefined,
      expedienteSiu: "",
      ficha: {
        direccion: "",
        origen: undefined,
        curp : undefined,
        edoCivil: undefined,
        ocupacion: undefined,
        telefono1: undefined,
        telefono2: undefined,
        celular: undefined
      },
      heredofamiliares: {

      },
      nopatologicos: {

      },
      patologicos: {

      },
      ginecobstetricos: {
        observaviones: [{descripcion:'ESTA ES UNA OBSERVACIÓN GINECOBSTETRICA',fecha:new Date()},{descripcion:'ESTA ES OTRA OBSERVACIÓN GINECOBSTETRICA =)',fecha:new Date()}]
      },
      padecimientoactual: {

      },
      aparatosysistemas: {

      },
      exploracionfisica: {
        diastolica: undefined,
        sostolica: undefined,
        cardio: undefined,
        respiratoria: undefined,
        temperatura: undefined,
        peso: undefined,
        talla: undefined,
        imc: undefined,
        clasificacion: undefined,
        observaciones: undefined,
        saturacion:undefined
      },
      laboratoriosygabinete: {

      },
      terapeuticaempleada: {

      },
      diagnostico: {
        descripcionPrincipal:undefined,
        descripcionSecundario:""
      },
      pronostico: {
        estatus1:false,
        estatus2:false,
        estatus3:false,
        descripcion:undefined

      }
    };
    defered.resolve();
    return promise;
  };

    /////////////////////////////////
    /////  MENÚS
    /////////////////////////////////
    $scope.tabs = [
      { title: 'CABEZA', elementos: [{desc:'FORMA Y VOLUMEN DEL CRANEO'},{desc:'PELO'},{desc:'PIEL'},{desc:'ICTERICIA'},{desc:'OJOS'},{desc:'CONJUNTIVAS'},{desc:'PUPILAS, CORNEA'},{desc:'REFLEJOS'},{desc:'FONDO DEL OJO'},{desc:'ENCÍAS'},{desc:'DIENTES'},{desc:'LENGUA'},{desc:'FARINGE'},{desc:'AMIGDALAS'}]},
      { title: 'CUELLO', elementos: [{desc:'VASOS'},{desc:'GANGLIOS'},{desc:'TIROIDES'}]},
      { title: 'TORAX', elementos: [{desc:'FORMA Y VOLUMEN'},{desc:'GLÁNDULAS MAMARIAS'},{desc:'REGIÓN PRECORDIAL',hijos:[{inciso:'A',desc:'INSPECCIÓN'},{inciso:'B',desc:'PALPASIÓN'},{inciso:'C',desc:'AUSCULTACIÓN'}]},{desc:'RESPIRATORIO',hijos:[{inciso:'A',desc:'INSPECCIÓN'},{inciso:'B',desc:'PALPACIÓN'},{inciso:'C',desc:'PERCUSIÓN'},{inciso:'d',desc:'AUSCULTACIÓN'}]}]},
      { title: 'ABDOMEN', elementos: [{desc:'FORMA Y VOLUMEN'},{desc:'PARED'},{desc:'CICATRIZ UMBILICAL'},{desc:'HIGADO'},{desc:'BRAZO'},{desc:'COLON'},{desc:'OTROS ÓRGANOS'},{desc:'DOLOR A LA PRESIÓN'},{desc:'TUMORES'},{desc:'ASCITIS'},{desc:'CIRCULACIÓN COLATERAL'},{desc:'ORIFICIOS INGUINAL'}]},
      { title: 'PELVIS', elementos: [{desc:'GENITALES'},{desc:'DISTRIBUCIÓN DEL VELLO'},{desc:'TACTO RECTAL'},{desc:'TACTO VAGINAL'}]},
      { title: 'DORSO Y EXTREMIDADES', elementos: [{desc:'COLUMNA'},{desc:'VASOS'},{desc:'GANGLIOS'},{desc:'PIEL'},{desc:'ARTICULACIONES'},{desc:'HUESOS'},{desc:'REFLEJOS',hijos:[{inciso:'A',desc:'ROTULIANO'},{inciso:'B',desc:'AQUILIANO'},{inciso:'C',desc:'PLANTAR'}]}]}
    ];


    ////////////////////////
    // Acciones de tabla
    ////////////////////////
    $scope.query = {
        order: 'fecha',
        limit: 5,
        page: 1
    };

    $scope.columns = [{
        name: 'FECHA',
        width : '15%',
        orderBy : 'NP_ID'
        }, {
            name: 'DOCTOR',
            width : '15%'
        }, {
          name: 'DESCRIPCIÓN',
            orderBy: 'PACIENTE',
            width : '70%'
        }
    ];

    $scope.onOrderChange = function(page, limit) {
        var deferred = $q.defer();
        $timeout(function () {
          deferred.resolve();
        }, 2000);

        return deferred.promise;
    };
  }])
  .directive('fichaIdent', ['historiaClinicaService','$parse', function(historiaClinicaService,$parse){
    return {
      scope: true,
      templateUrl: function(elem,attrs){
        return historiaClinicaService.pagina[attrs.fichaIdent];
      }
    };
  }]);

})();
