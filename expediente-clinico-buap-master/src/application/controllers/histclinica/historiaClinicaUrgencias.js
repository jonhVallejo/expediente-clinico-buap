(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:HistclinicaHistoriaclinicactrlCtrl
 * @description
 * # HistclinicaHistoriaclinicactrlCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('HistoriaclinicaUrgenciasCtrl',['$scope','$location','historiaClinicaService','$q','cuestionarioService','peticiones','mensajes','medicos','usuario','$localStorage','pacientes','consultas','$state','blockUI','catalogos',
    function ($scope,$location,historiaClinicaService,$q,cuestionarioService,peticiones,mensajes,medicos,usuario,$localStorage,pacientes,consultas,$state,blockUI,catalogos) {


    var self                  = this;
    var mPaciente             = $localStorage.paciente;
    var ant                   = 0;

    var idEspecialidad        = 0;

    $scope.fecha              = new Date();
    $scope.datos              = {ficha:{}};

    $scope.datos.exp          = mPaciente.idexpediente;
    $scope.datos.ID           = pacientes.obtenerNpIdPaciente();
    $scope.datos.nombre       = ($localStorage.paciente.nombrePaciente == undefined) ? $localStorage.paciente.nombreCompleto: $localStorage.paciente.nombrePaciente;

    self.mCuestionario        = {SECCION: []};
    self.mCuestionarioGeneral = {SECCION: []};


 blockUI.start();


      function getEspecialidad(){

        medicos.getMedicoActual(usuario.usuario.NP_EXPEDIENTE)
        .success(function(data){
          if(data === ''){
            mensajes.alerta('ERROR','ERROR INTERNO DEL SISTEMA INTENTE NUEVAMENTE','ACEPTAR!');
          }
          else{
            idEspecialidad = data.ESPECIALIDADES.NF_ESPECIALIDAD;

          }
          blockUI.stop();
        })
        .error(function(data){
           blockUI.stop();
          mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
        });
      };
       getEspecialidad();

    pacientes.getPaciente()
      .success(function(data){
        if(data.success===true){
          $scope.datos.genero          =  data.data[0].NF_SEXO;
          $scope.isHombre              =  (data.data[0].NF_SEXO == "MASCULINO") ? true : false;
          $scope.datos.fNac            =  data.data[0].F_FECHA_DE_NACIMIENTO;
          $scope.datos.edad            =  pacientes.calculaEdad(data.data[0].F_FECHA_DE_NACIMIENTO);
          $scope.datos.ficha.direccion += (data.data[0].C_CALLE) ? data.data[0].C_CALLE : "";
          $scope.datos.ficha.direccion += (data.data[0].C_COLONIA) ? data.data[0].C_COLONIA : "";
          $scope.datos.ficha.direccion += (data.data[0].C_NO_INTERIOR) ? data.data[0].C_NO_INTERIOR : "";
          $scope.datos.ficha.origen    =  data.data[0].NF_ENTIDAD_FEDERATIVA;
          $scope.datos.ficha.curp      =  data.data[0].C_CURP;
          $scope.datos.ficha.edoCivil  =  data.data[0].NF_ESTADO_CIVIL;
          $scope.datos.ficha.ocupacion =  data.data[0].C_OCUPACION;
          $scope.datos.ficha.telefono1 =  data.data[0].C_NO_TELEFONO_UNO;
          $scope.datos.ficha.telefono2 =  data.data[0].C_NO_TELEFONO_DOS;
          $scope.datos.ID =  data.data[0].NP_ID;

          }
        else if(data.success===false){
        }
        else{
          mensajes.alerta('ERROR','LO SENTIMOS, OCURRIÓ UN PROBLEMA','ACEPTAR!');
        }
      })
      .error(function(data){
        mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      });




    $scope.salir=function(){
      $state.go("notaMedicaIngreso");
    }

/**
 * Empieza la sección para los cuestionarios
 */
    var cuestionarioEspecifico = function(NP_TIPO_CUESTIONARIO, ID_ESPECIALIDAD, NF_PACIENTE, MEDICINA_GENERAL){
        var url = "cuestionario/" + NP_TIPO_CUESTIONARIO + "/" + NF_PACIENTE + "/";
        //url += (MEDICINA_GENERAL) ? 1020 : ID_ESPECIALIDAD;
        url += 1020;
        //url = "/cuestionario/" + 17 + "/" + 17 + "/10061";
        blockUI.start();
        peticiones.getDatos(url)
        .then(function(data) {
            blockUI.stop();
            cuestionarioService.setContestarCuestionario(true);

            if(data.SECCION){

                  self.contesCuestionarioGeneral = false;
                  self.contesCuestionario = false;


                // if(!$localStorage.procedenciaExpediente){//si procede de historia clinica o de agenda médica los campos siempre serán editables
                //   self.contesCuestionarioGeneral = false;
                //   self.contesCuestionario = false;
                // }else{ //si viene de expediente clínico los campos deben ser "no editales"
                //     self.contesCuestionarioGeneral = true;
                //     self.contesCuestionario = true;
                // }

                cuestionarioService.setCuestionario(data);
                if(MEDICINA_GENERAL){
                  self.mCuestionarioGeneral = data

                }else{
                  self.mCuestionario = data

                }
            }
        })
        .catch(function(err) {
              blockUI.stop();
              console.log(err);

        });
    };

    var limpiarCuestionario = function(){
      cuestionarioService.reset(self.mCuestionario);
      cuestionarioService.reset(self.mCuestionarioGeneral);
    }

    self.cambiarRadio = function(key_seccion,key_pregunta,bandera,MEDICINA_GENERAL){
        if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
              cuestionarioService.cambiarRadio(key_seccion,key_pregunta, bandera, self.mCuestionarioGeneral,MEDICINA_GENERAL);
            }else{
              cuestionarioService.cambiarRadio(key_seccion,key_pregunta, bandera, self.mCuestionario,MEDICINA_GENERAL);
            }
        };

    self.seleccionarRespuesta = function(key_seccion,key_pregunta,key_respuesta,MEDICINA_GENERAL){
       if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
              cuestionarioService.seleccionarRespuesta(key_seccion,key_pregunta, key_respuesta, self.mCuestionarioGeneral);
        }else{
              cuestionarioService.seleccionarRespuesta(key_seccion,key_pregunta, key_respuesta, self.mCuestionario);
        }
      };

    self.guardarRespuestas = function(MEDICINA_GENERAL){


      //var cuestionarioFin = (MEDICINA_GENERAL) ? angular.toJson(self.mCuestionarioGeneral) : angular.toJson(self.mCuestionario);

      var cuestionarioFin = {};

      if( !_.isUndefined(self.mCuestionarioGeneral.NP_CUESTIONARIO)){
        cuestionarioFin = self.mCuestionarioGeneral;
      }else{
        cuestionarioFin = self.mCuestionario;
      }


      var result = cuestionarioService.revisarRespuestas(angular.fromJson(cuestionarioFin));
      if(result.correcta){
          var cuestionarioResult = cuestionarioService.crearCuestionarioFinal(angular.fromJson(result.cuestionario));
          cuestionarioResult.NF_MEDICO = usuario.usuario.NP_EXPEDIENTE
          cuestionarioResult.NF_PACIENTE = $localStorage.paciente.expediente.substring(0, $localStorage.paciente.expediente.length - 4)
          cuestionarioResult.NF_NOTA_MEDICA = $localStorage.notaMedica.NP_NOTA_MEDICA;
          postRespuestas(cuestionarioResult,MEDICINA_GENERAL);
      }else{
        mensajes.alerta("ERROR","DEBE CONTESTAR TODAS LAS PREGUNTAS OBLIGATORIAS" ,"OK");
      }
    };

    var  postRespuestas = function(mData,MEDICINA_GENERAL){
      if(mData.CONTESTADO && mData.NOTA_FIRMADA==false){//Put
        peticiones.putMethod('cuestionario/respuestas/',mData)
          .success(function(data){
            mensajes.alerta("","DATOS ACTUALIZADOS CORRECTAMENTE" ,"OK");
          })
          .error(function(data){
            mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
          });
      }else{//Post
          peticiones.postDatos('cuestionario/respuestas/',mData)
            .then(function(data) {
                mensajes.alerta("","DATOS ACTUALIZADOS CORRECTAMENTE" ,"OK");
            })
            .catch(function(err) {
                console.log(err);
            });
      }
    };


/**
 * Termina la sección de guardar los cuestionarios
 */

    $scope.seleccionar = function(index){
     //  self.mCuestionario.SECCION.splice(0,self.mCuestionario.SECCION.length)
     //  self.mCuestionarioGeneral.SECCION.splice(0,self.mCuestionarioGeneral.SECCION.length);
     //  limpiarCuestionario();

     //  var NP_EXPEDIENTE_PACIENTE = $localStorage.paciente.expediente.substring(0, $localStorage.paciente.expediente.length - 4)

     // if($scope.secciones[index].buscarCuest){


     //  if (ant == index){
     //       $scope.secciones[index].visible = ($scope.secciones[index].visible) ? false : true;
     //  }else{
     //     $scope.secciones[ant].visible = false;
     //    $scope.secciones[index].visible = true;


     //  }
     //  ant = index;
     //   cuestionarioEspecifico($scope.secciones[index].id,idEspecialidad,NP_EXPEDIENTE_PACIENTE, false);
     // }

        if (ant == index){
            $scope.secciones[index].visible = ($scope.secciones[index].visible) ? false : true;
        }
        else{
            $scope.secciones[ant].visible = false;
            $scope.secciones[index].visible = true;
        }
        ant = index;

        self.mCuestionario.SECCION.splice(0,self.mCuestionario.SECCION.length)
        self.mCuestionarioGeneral.SECCION.splice(0,self.mCuestionarioGeneral.SECCION.length);
        limpiarCuestionario();
        var NP_EXPEDIENTE_PACIENTE = $localStorage.paciente.expediente.substring(0, $localStorage.paciente.expediente.length - 4);
        if($scope.secciones[index].buscarCuest && $scope.secciones[index].visible){
            //PETICIÓN PARA MEDICINA GENERAL
            cuestionarioEspecifico($scope.secciones[index].id,idEspecialidad,NP_EXPEDIENTE_PACIENTE, $scope.idNota, true);
            //PETICIÓN PARA EL CUESTIONARIO ESPECIFICO
           // cuestionarioEspecifico($scope.secciones[index].id,idEspecialidad,NP_EXPEDIENTE_PACIENTE, $scope.idNota, false);

            self.mfecha = new Date("05/05/2015");
        }



    };


    /////////////////////////////////
    /////  MENÚS
    /////////////////////////////////

    $scope.secciones =  [
     {
                            titulo:'PADECIMIENTO ACTUAL',
                            visible: false,
                            id: 6,
                            buscarCuest: true
                          },{
                            titulo:'ANTECEDENTES PERSONALES PATOLÓGICOS',
                            visible: false,
                            id: 4,
                            buscarCuest: true
                          },
                          {
                            titulo:'ANTECEDENTES GINECO-OBSTÉTRICOS',
                            visible: false,
                            id: 5,
                            buscarCuest: true
                          },

                        ];

  }])
  .directive('historiaClinica', ['historiaClinicaService','$parse', function(historiaClinicaService,$parse){
    return {
      scope: true,
      templateUrl: function(elem,attrs){
        return historiaClinicaService.pagina[attrs.historiaClinica];
      }
    };
  }]);

})();
