(function(){

'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:HistclinicaExpedienteclinicoCtrl
 * @description
 * # HistclinicaExpedienteclinicoCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('HistclinicaExpedienteclinicoCtrl',['$scope','mensajes','$localStorage','pacientes', 'consultas','$q','$timeout','$state','peticiones','$http','catalogos','$window','quirofano','reportes' , "$mdDialog",
  	function ($scope,mensajes,$localStorage,pacientes,consultas,$q,$timeout,$state,peticiones,$http,catalogos, $window,quirofano,reportes,$mdDialog) {
   	var self=this;

    $scope.nuevo={
			'ANAMNESIS':{},
			'EXPLORACION_FSC':{},
			'INTERROGATORIO':{},
			'NOTAS_MEDICAS':{},
			'RESUMEN_DEL_INTERROGATORIO' : {}
		};
  	$scope.queryU = {
	    filter: '',
	    order: '',
	    limit: 10,
	    page: 1
    };
    $scope.queryC = {
	    filter: '',
	    order: '',
	    limit: 5,
	    page: 1
    };
    $scope.queryH = {
	    filter: '',
	    order: '',
	    limit: 5,
	    page: 1
    };

    $scope.queryEstudios = {
	    filter: '',
	    order: '',
	    limit: 5,
	    page: 1
    };
    $scope.queryMedicamentos = {
	    filter: '',
	    order: '',
	    limit: 5,
	    page: 1
    };
    $scope.queryEstudios = {
	    filter: '',
	    order: '',
	    limit: 15,
	    page: 1
    };

	$scope.existReceta = false;
    $scope.consultas = {};
    $scope.urgencias = {};



    $scope.seccionesConsulta = [{titulo : 'CONSULTAS EXTERNAS', visible : true , id: 0 },{titulo : 'DETALLE', visible : false , id : 1}];
    $scope.seccionesUrgencias = [{titulo : 'CONSULTAS URGENCIAS', visible : true , id: 0 },{titulo : 'CICLO URGENCIAS', visible : false , id : 1},{titulo : 'DETALLES', visible : false , id : 2}];
    $scope.seccionesHospitalizacion = [{titulo : 'CONSULTAS HOSPITALIZACIÓN', visible : true , id: 0 },{titulo : 'CICLO HOSPITALIZACIÓN', visible : false , id : 1},{titulo : 'DETALLES', visible : false , id : 2}];
    $scope.isInfo = false;
    //Variables utilizadas para Historioco de Expediente Clínico
    $scope.showExpedienteClinico = false;
    //$scope.showExpedienteClinico = $localStorage.showExpedienteClinico;
    $scope.query = {
	    filter: '',
	    order: '',
	    limit: 15,
	    page: 1
    };
    //Variables utilizadas para Histórico de Historia Clínica
     $scope.queryHC= {
	    filter: '',
	    order: '',
	    limit: 5,
	    page: 1
    };

    getDatosHistoricoExpediente();

    $scope.cargaDatos = function(){
    	pacientes.getPaciente()
		.success(function(data){


			if(data.success===true){
				$scope.paciente=data.data[0];
				$scope.paciente.nombreCompleto = $scope.paciente.C_PRIMER_APELLIDO+' '+$scope.paciente.C_SEGUNDO_APELLIDO+' '+$scope.paciente.C_NOMBRE;
				$scope.paciente.direccion = $scope.paciente.C_CALLE+' '+$scope.paciente.C_NO_EXTERIOR+' '+$scope.paciente.C_COLONIA+' '+$scope.paciente.C_LOCALIDAD+' '+$scope.paciente.C_MUNICIPIO+' '+$scope.paciente.NF_ENTIDAD_FEDERATIVA;
				$scope.paciente.FECHA_NACIMIENTO = new Date($scope.paciente.F_FECHA_DE_NACIMIENTO.split("/")[2], ($scope.paciente.F_FECHA_DE_NACIMIENTO.split("/")[1]) - 1 ),$scope.paciente.F_FECHA_DE_NACIMIENTO.split("/")[0];
				$scope.paciente.N_EDAD =  pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);
				 $scope.calcularEdad();
				}
			else if(data.success===false){
				mensajes.alerta('AVISO',data.Mensaje.toUpperCase(),'ACEPTAR!');
			}
			else{
				mensajes.alerta('ERROR','LO SENTIMOS, OCURRIÓ UN PROBLEMA','ACEPTAR!');
			}
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
    };

    $scope.calcularEdad =function(){
		$scope.paciente.F_FECHA_DE_NACIMIENTO = moment($scope.paciente.FECHA_NACIMIENTO).format('DD/MM/YYYY');
		var values=$scope.paciente.F_FECHA_DE_NACIMIENTO.split("/");
		var dia = values[0];
	    var mes = values[1];
        var ano = values[2];
        var fecha_hoy = new Date();
        var ahora_ano = fecha_hoy.getYear();
        var ahora_mes = fecha_hoy.getMonth()+1;
        var ahora_dia = fecha_hoy.getDate()-1+1;
        var edad = (ahora_ano + 1900) - ano;
        if(ahora_mes<mes){
        	edad--;
        }
        else if(ahora_mes==mes && ahora_dia<dia){
        	edad--;
        }
        $scope.paciente.N_EDAD=edad +' AÑO(S)';
        if(edad<0){
        	mensajes.alerta('AVISO','NO PUEDES PONER UNA FECHA MAYOR AL DÍA DE HOY','ACEPTAR!');
        	$scope.paciente.FECHA_NACIMIENTO = undefined;
        	$scope.paciente.N_EDAD = undefined;
        }
        else if(edad === 0){
        	if(ahora_dia-dia > 0){
        		$scope.paciente.N_EDAD =  mes-ahora_mes +' MES(ES)';
        	}
        	else{
        		$scope.paciente.N_EDAD =mes- ahora_mes - 1 +' MES(ES)';	
        	}
        }
    }




	$scope.detallesConsulta = function(data,tipo){
    	$scope.nuevo = {};
    	$scope.datosEstudios = {};
		$scope.catDatosLaboratorio = {};
		$scope.medicamentos = {};
		$scope.NF_ESTATUS1 = false;
		$scope.NF_ESTATUS2 = false;
		$scope.NF_ESTATUS3 = false;
		$scope.existLaboratorio = false;
		$scope.existSolicitudLaboratorio = false;
		$localStorage.NF_NOTAMEDICA = -1;
		$scope.C_MODULO = undefined;
		$scope.C_FILE_RECETA = undefined;
    	var defered = $q.defer();
    	var promise = defered.promise;
    	if(tipo==1){//tipo 1=CONSULTA EXTERNA
	    	$scope.NF_NOTAMEDICA = $localStorage.NF_NOTAMEDICA;
	    	$scope.selectedIndex2 = 0;
	    	consultas.getNotaMedica($localStorage.paciente.expediente , data.NF_T_AGENDA)
	    	.success(function(data){
	    		$scope.selectedIndex2 = 0;
	    		$localStorage.NF_NOTAMEDICA = data[0].nota.NF_NOTA_MEDICA;
	    		$scope.NF_NOTAMEDICA = $localStorage.NF_NOTAMEDICA;
	    		loadData(data);
	    		$scope.seccionesConsulta[0].visible = false;
		    	$scope.seccionesConsulta[1].visible = true;
		    	$scope.isInfo = true;
		    	$scope.isUrgencias = false;
		    })
	    	.error(function(data){
	    		defered.reject();
	    	});
	    	return promise;
	    }else if(tipo === 2){ //tipo=2 URGENCIAS
	    	$localStorage.NF_NOTAMEDICA=data.NF_NOTA_MEDICA;
	    	$scope.selectedIndex2 = 0;
	    	if (data.NF_TIPO_NOTA == 7) {
	    		$scope.isConsulta = true;
	    		if(data.CF_TIPO_NOTA_EGRESO == "REFERENCIA" || data.CF_TIPO_NOTA_EGRESO == "VOLUNTARIA"){
	    			reportes.getReporte(baseURL + 'notaegreso/datos/'+ data.NF_NOTA_EGRESO+'/1', '_blank', 'width=1000, height=800'); 	
	    		}
                reportes.getReporte(baseURL + 'notaegreso/datos/'+ data.NF_NOTA_EGRESO+'/0', '_blank', 'width=1000, height=400'); 
                $scope.isNotaEgreso = true;
			}
	    	else {
		    	consultas.getNotaMedicaUrgencias(data.NF_NOTA_MEDICA)
		    	.success(function(data){
		    		$scope.isUrgencias = true;
			  		$scope.seccionesUrgencias[2].visible = true;
			    	$scope.isInfo = true;
			    	loadData(data);
		    		$scope.isNotaEgreso = false;					
			    	defered.resolve({});
		    	})
		    	.error(function(data){
		    		defered.reject();
		    	});	    		
	    	}
	    	return promise;
	    }
	    else if(tipo == 3){
	    	$localStorage.NF_NOTAMEDICA=data.NF_NOTA_MEDICA;
	    	$scope.selectedIndex2 = 0;
	    	if (data.NF_TIPO_NOTA == 7) {
	    		$scope.isConsulta = true;
                reportes.getReporte(baseURL + 'notaegreso/datos/'+ data.NF_NOTA_EGRESO, '_blank', 'width=1000, height=800'); 
                $scope.isNotaEgreso = true;
			}
	    	else {
		    	consultas.getNotaMedicaUrgencias(data.NF_NOTA_MEDICA)
		    	.success(function(data){
		    		$scope.isUrgencias = true;
			  		$scope.seccionesHospitalizacion[2].visible = true;
			    	$scope.isInfo = true;
			    	loadData(data);
		    		$scope.isNotaEgreso = false;
			    	defered.resolve({});
		    	})
		    	.error(function(data){
		    		defered.reject();
		    	});	    		
	    	}
	    	return promise;
	    }

	};

	function loadNotaEgreso(data) {
		var defered = $q.defer();
		var promise = defered.promise;
		if (data != undefined) {
			catalogos.getCatalogos(28)
	        .success(function(cat){
	          	$scope.motivos = cat;
				$scope.motivo = 1;
				$scope.alta = {
					ESTANCIA_EN_HOSPITAL : data.C_ESTANCIA_HOSPITALARIA,
					MANEJO_TRATAMIENTO : data.C_PLAN_TRATAMIENTO,
					C_PRONOSTICO : data.C_PRONOSTICO,
					RECOMENDACIONES: data.C_VIGILANCIA_AMBULATORIA,
				}
				defered.resolve({});
	        })
	        .error(function(err){
	          mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
	          defered.reject();
	        }); 			
		}
		else 
  			defered.reject();
	}

	function loadData(data){
   		$scope.idOrdenMedica = data[0].nota.NF_NOTA_MEDICA;
		var defered = $q.defer();
		var promise = defered.promise;
		data = data[0];
			if(data != undefined){
			if(data.fisicas != undefined){
				$scope.nuevo={
					'EXPLORACION_FSC':{
						'N_TSN_ART_SISTOLICA':data.fisicas.N_TSN_ART_SISTOLICA,
						'N_TSN_ART_DIASTOLICA':data.fisicas.N_TSN_ART_DIASTOLICA,
						'N_FRC_CARDIACA':data.fisicas.N_FRC_CARDIACA,
						'N_FRC_RESPIRATORIA':data.fisicas.N_FRC_RESPIRATORIA,
						'N_TEMPERATURA':data.fisicas.N_TEMPERATURA,
						'N_PESO':data.fisicas.N_PESO,
						'N_TALLA':data.fisicas.N_TALLA,
						'C_DESCRIPCION':data.fisicas.C_DESCRIPCION,
						'N_SATURACION':data.fisicas.N_SATURACION,
						'N_IMC':data.fisicas.N_IMC,
						'C_CLASIFICACION':data.fisicas.C_CLASIFICACION,
						'C_OBS_ENFERMERIA':data.fisicas.C_OBS_ENFERMERIA,
						'EXPLORACION':data.fisicas.C_DESCRIPCION
					}
				};
			}
			$scope.nuevo.C_INTERPRETACION = data.nota.C_INTERPRETACION_LABS;
			$scope.nuevo.RESUMEN_DEL_INTERROGATORIO = {'C_ANAMNESIS':data.nota.C_ANAMNESIS, 'C_MOTIVO_DE_CONSULTA' : data.nota.C_MOTIVO_DE_CONSULTA};
			$scope.nuevo.NOTAS_MEDICAS = {'C_PRONOSTICO':data.nota.C_PRONOSTICO};

			$scope.nuevo.C_PROCEDIMIENTOS_REALIZADOS =  data.nota.C_PROCEDIMIENTOS_REALIZADOS;
			$scope.nuevo.C_PLAN = data.nota.C_PLAN;
			$scope.nuevo.C_DESTINO_PACIENTE = data.nota.C_DESTINO_PACIENTE;
			if(data.fisicas != undefined)
				$scope.nuevo.EXPLORACION_FSC.C_ESTADO_MENTAL = data.fisicas.N_ESTADO_MENTAL;
			$scope.nuevo.EXPLORACION_FSC.C_ANALISIS = data.nota.C_ANALISIS;
			if(data.laboratorio.length>0){ //Datos de laboratorio
				var estudiosL = _.map(data.laboratorio, function (el, key) {
                    return{
                    	'cDescripcion' : el.C_DESCRIPCION,
                    	'ID' : el.NP_SRV_PRODUCTO,
                    	'C_MOTIVO_SOLICITUD' : el.C_MOTIVO_SOLICITUD,
                    	'dCosto' : el.D_COSTO
                    }
                });
				$scope.datosEstudios = estudiosL;
				$scope.isEstudio = true;
				$scope.canDelete = true;
                if($scope.datosEstudios.length > 0){
                	$scope.existLaboratorio = true;
                }
                else{
                	$scope.existLaboratorio = false;
                }
			}
			//DIAGNOSTICO PRIMARIO
			$scope.nuevo.DIAGNOSTICOPRINCIPAL = data.nota.NF_CIE_10_C_DESCRIPCION;
			if($scope.existLaboratorio && $scope.nuevo.DIAGNOSTICOPRINCIPAL != undefined){
				$scope.existSolicitudLaboratorio = true;
			}
			else {
				$scope.existSolicitudLaboratorio = false;
			}

			//Pronóstico
			$scope.NF_ESTATUS1 = false;
			$scope.NF_ESTATUS2 = false;
			$scope.NF_ESTATUS3 = false;
			$scope.NF_ESTATUS4 = false;
			if(data.nota.N_ESTATUS_PRONOSTICO != undefined){
				if(data.nota.N_ESTATUS_PRONOSTICO === 1){$scope.NF_ESTATUS1 = true; $scope.NF_ESTATUS3 = true;}
				else if(data.nota.N_ESTATUS_PRONOSTICO === 2){$scope.NF_ESTATUS1 = true; $scope.NF_ESTATUS4 = true;}
				else if(data.nota.N_ESTATUS_PRONOSTICO === 3){$scope.NF_ESTATUS2 = true; $scope.NF_ESTATUS3 = true;}
				else if(data.nota.N_ESTATUS_PRONOSTICO === 4){$scope.NF_ESTATUS2 = true; $scope.NF_ESTATUS4 = true;}
			}

			//ORDEN MÉDICA
			$scope.C_DIETA = "";
			$scope.C_SOLUCIONES = "";
			$scope.C_MEDIDAS_GENERALES = "";
			$scope.isOrden = false;
			if(data.nota.C_DIETA != undefined){
				$scope.C_DIETA = data.nota.C_DIETA;
				$scope.isOrden = true;
			}
			if(data.nota.C_SOLUCIONES != undefined){
				$scope.C_SOLUCIONES = data.nota.C_SOLUCIONES;
				$scope.isOrden = true;
			}
			if(data.nota.C_MEDIDAS_GENERALES != undefined){
				$scope.C_MEDIDAS_GENERALES = data.nota.C_MEDIDAS_GENERALES;
				$scope.isOrden = true;
			}

			$scope.isDiagnostico = false;
			if(data.cie.length > 0){
				$scope.datosSecundarios = _.map(data.cie, function (el, key) {
                    return{
                    	'C_DESCRIPCION' : el.C_DESCRIPCION,
                    	'NP_ID' : el.NF_CIE10
                    }
                });
				$scope.isDiagnostico = true;
			}
			else{
				$scope.datosSecundarios = [];
				$scope.isDiagnostico = false;
			}
			if($localStorage.paciente.tipo === 'EVOLUCION Y ALTA'){
				consultas.getDiagnosticoIngreso($localStorage.paciente.expediente)
				.success(function(data){
					if(data.length>0){
						$scope.nuevo.C_DIAGNOSTICO_INGRESO = data.C_ANAMNESIS;
					}
				})
				.error(function(data){
					mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
					defered.reject();
				});
			}
			$scope.isIndicacion = false;
			if(data.nota.C_FILE_RECETA != undefined && data.nota.C_MODULO != undefined){
				$localStorage.C_MODULO = data.nota.C_MODULO;
				$localStorage.C_FILE_RECETA = data.nota.C_FILE_RECETA;
				$scope.existReceta = true;
			}
			else{
				$localStorage.C_MODULO = undefined;
				$localStorage.C_FILE_RECETA = undefined;
				$scope.existReceta = false;
			}
			$scope.medicamentos = [];
			if(data.nota.NF_RECETA!=undefined){
				consultas.getIndicacionTereapeutica(data.nota.NF_RECETA)
		    	.success(function(data){
		    		if(data != undefined){
		    			$scope.medicamentos = data;
		    			$scope.isIndicacion = true;
		    			$scope.C_MODULO = $localStorage.C_MODULO;
		    			$scope.C_FILE_RECETA = $localStorage.C_FILE_RECETA;
		    			$scope.isCancel = false;
		    			$scope.existReceta = true;
		    		}
		    		defered.resolve({});
		    	})
		    	.error(function(data){
		    		defered.reject();
		    	});
			}
			
	    	defered.resolve({});
  		}
  		else {
  			defered.reject();
  		}
	}


	$scope.seleccionar = function(obj){
	    $scope.seccionesConsulta[obj.id].visible = !obj.visible;
    };

    $scope.seleccionarUrgencias = function(obj){
	    $scope.seccionesUrgencias[obj.id].visible = !obj.visible;
    };

    $scope.parseFecha = function(date){
    	return (date !== undefined) ? moment(date).format("DD/MM/YYYY HH:mm") : '';
    };
    $scope.parseFechaSinHH = function(date){
         return (date !== undefined) ? moment(date).format("DD/MM/YYYY") : '';
    };



    $scope.campos = [{descripcion : "C_ESPECIALIDAD",validar: false, campo:"" },
    				 {descripcion : "C_DESCRIPCION_CIE_10",validar: false, campo:"" }];

   //$scope.dato.buscar='';
    $scope.filtraTabla = function(filtro) {
		$scope.registros = peticiones.filtrar(filtro,$scope.historicoExpediente,$scope.campos);
	}

    $scope.siguiente = function(){
    	$scope.selectedIndex2 += 1;
    };

    $scope.TERMINAR = function(){
    };
    /***********************FUNCIONES PARA HISTORIA CLÍNICA*******************************/
    function getDatosHistoricoExpediente(){
      var url = "expedienteClinico/historia/"+$localStorage.paciente.idexpediente;
      peticiones.getDatos(url)
        .then(function(data) {
        	console.log(data);
        	_.each(data,function(reg){
        		reg.C_DESCRIPCION_CIE_10=reg.C_DESCRIPCION_CIE_10.toUpperCase();
        	});

          $scope.historicoExpediente =data;
	      $scope.registros=data;
	      _.each(data, function(reg){ reg.FECHA = moment(reg.T_FECHA_CMD).format("YYYYMMDD") });
        })
        .catch(function(err) {
          console.log(err);
        });
    };
    //DATOS  PARA VER HISTÓRICO DE HISTORIA CLÍNICA
    function getDatosHistoriaClinica(NP_NOTA){
      var url = "expedienteClinico/historiaNotas/"+NP_NOTA;
      peticiones.getDatos(url)
        .then(function(data) {
          $scope.historicoHistoriaClinica =data;
        })
        .catch(function(err) {
          console.log(err);
        });
    };
    //FUNCIONES PARA TABLA
    $scope.onOrderChange = function(page, limit) {
      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve();
      }, 2000);
    };
    $scope.onOrderChangeHC = function(page, limit) {
      var deferred = $q.defer();
      $timeout(function () {
        deferred.resolve();
      }, 2000);
    };
    //FUNCIONES PARA EXPEDIENTE CLINICO
    $scope.verExpediente=function(data){
    	$scope.showExpedienteClinico=true;
    	$scope.seccionesConsulta[0].visible = true;
    	$scope.seccionesConsulta[1].visible = false;
	    $scope.isInfo = false;
	    $scope.seccionesUrgencias[0].visible = true;
    	$scope.seccionesUrgencias[1].visible = false;
    	$scope.seccionesUrgencias[2].visible = false;
    	$scope.seccionesHospitalizacion[0].visible = true;
    	$scope.seccionesHospitalizacion[1].visible = false;
    	$scope.seccionesHospitalizacion[2].visible = false;
    	$scope.isInfoUrgencias = false;
    	$scope.isInfoHosp = false;
    	$localStorage.showExpedienteClinico = $scope.showExpedienteClinico;
    	$localStorage.NF_CIE_10=data.NP_CIE_10;
    	getDatosHistoriaClinica(data.NP_NOTA_MEDICA);
    	$scope.verNotaMedicaCIE10();
    	$scope.verNotaMedicaUrgencias(data);
    	$scope.getHistoricoNotaQuirofano();
    	$scope.getNotasHosp(data);
    	$scope.getEstudiosPaciente();
    };
    //FUNCIONES PARA HISTORIA CLÍNICA
    $scope.verHistoriaClinica=function(data){
    	$localStorage.procedenciaHistoria=false;
    	$localStorage.procedenciaExpediente=true;
    	$localStorage.idNota=data.NP_NOTA_MEDICA;
    	$localStorage.idEspecialidad=data.NP_ESPECIALIDAD;
	    $state.go("historiaClinica");
    };

    $scope.verNotaMedicaCIE10 = function(){
    	consultas.getNotasCIE10($localStorage.NF_CIE_10 , $localStorage.paciente.expediente)//
		.success(function(data){
			$scope.consultas = (data.length > 0) ? data : [];
		})
		.error(function(data){
			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
		});
    };

    $scope.verNotaMedicaUrgencias = function(datos){
    	consultas.getNotasUrgencias(datos.NP_CIE_10 , $localStorage.paciente.expediente , 2).success(function(data){
    		console.log("datos");
    		console.log(data);
    		$scope.urgencias = (data.length > 0) ? data : [];
    	}).error(function(data){});
    };

    $scope.parseSAEC = function(SAEC){
    	return SAEC === 1 ? 'S' : 'N';
    };

    $scope.reprintReceta = function(){
    	reportes.getReporte(baseURL + 'getpdf/' + $localStorage.C_MODULO + '/' + $localStorage.C_FILE_RECETA , '_blank' , 'width=800, height=640');
    };

    $scope.reprintNotaMedica = function(){
    	reportes.getReporte(baseURL + 'reporteNota/' + $localStorage.NF_NOTAMEDICA , '_blank' , 'width=800, height=640');
    };

     $scope.reprintNotaMedicaUrgencias = function(){
    	reportes.getReporte(baseURL + 'reportenotasmedicas/' + $localStorage.NF_NOTAMEDICA , '_blank' , 'width=800, height=640');
    };

    $scope.reprintDatosLaboratorio = function(){
    	reportes.getReporte(baseURL + 'solicitudGabinete/' + $localStorage.NF_NOTAMEDICA , '_blank' , 'width=1000, height=800');
    };

    $scope.selectedRow = null;
   	$scope.rowSelection = function rowSelection( id ) { $scope.selectedRow = id; }
   	$scope.wasSelected = function wasSelected( id ) { return $scope.selectedRow === id; }
   	$scope.rowSelection2 = function rowSelection( id ) { $scope.selectedRow2 = id; }
   	$scope.wasSelected2 = function wasSelected( id ) { return $scope.selectedRow2 === id; }

   	$scope.detallesUrgencia = function(dato){
   		$scope.urgenciasCiclo = [];
   		var defered = $q.defer();
    	var promise = defered.promise;
   		if(dato.VERDE === 1){ //Comprueba si la nota es de Consulta Urgencias (verdes)
   			$localStorage.NF_NOTAMEDICA = dato.NF_NOTA_MEDICA_INGRESO;
   			$scope.nuevo = {};
	    	$scope.datosEstudios = {};
			$scope.catDatosLaboratorio = {};
			$scope.medicamentos = {};
			$scope.NF_ESTATUS1 = false;
			$scope.NF_ESTATUS2 = false;
			$scope.NF_ESTATUS3 = false;
			$scope.existLaboratorio = false;
			$scope.existSolicitudLaboratorio = false;
			$scope.C_MODULO = undefined;
			$scope.C_FILE_RECETA = undefined;
			if(dato.TRIAGE != undefined){
				if(dato.TRIAGE.C_FILE_TRIAGE != undefined){
					$scope.printTriage(dato);
				}
			}
   			consultas.getNotaMedicaUrgencias(dato.NF_NOTA_MEDICA_INGRESO)
	    	.success(function(data){
	    		console.log(data);
				$scope.seccionesUrgencias[0].visible = false;
		    	$scope.seccionesUrgencias[1].visible = true;
		    	$scope.isInfoUrgencias = true;
				$scope.isUrgencias = true;
		  		$scope.seccionesUrgencias[2].visible = true;
		    	$scope.isInfo = true;
		    	loadData(data);
	    		$scope.isNotaEgreso = false;
		    	defered.resolve({});		    	
	    	})
	    	.error(function(data){
	    		defered.reject();
	    	});
   		}
   		else{
   			consultas.getCicloUrgencias($localStorage.paciente.expediente , dato.NF_NOTA_MEDICA_INGRESO , 2).success(function(data){
	    		if(data.length > 0){
	    			$scope.urgenciasCiclo = data;
		    		$scope.seccionesUrgencias[0].visible = false;
			    	$scope.seccionesUrgencias[1].visible = true;
			    	$scope.isInfoUrgencias = true;
			    	$scope.isTriage = (data[0].C_FILE_TRIAGE != undefined) ? true : false;
			    	defered.resolve();
	    		}
	    		else{
	    			mensajes.alerta("AVISO","NO SE ENCUENTRAN RESULTADOS","ACEPTAR");
	    		}
	    	}).error(function(data){defered.reject();});
   		}
   		
   		
		return promise;
   	};

   	$scope.printTriage = function(dato){
   		reportes.getReporte(baseURL + 'getpdf/' + dato.TRIAGE.C_MODULO_TRIAGE + '/' + dato.TRIAGE.C_FILE_TRIAGE , '_blank' , 'width=800, height=640');
   	};

   	$scope.reprintOrdenMedica = function(){
   		reportes.getReporte(baseURL + 'ordenmedica/' + $scope.idOrdenMedica , '_blank' , 'width=1000, height=800');
   	};

   	/***********************FUNCIONES PARA QURÓFANO*******************************/
    self.historicoQuirofano =[];
   	$scope.getHistoricoNotaQuirofano=function(){
		var url="listaNotasQuirofano/"+$localStorage.paciente.idexpediente;
	    peticiones.getDatos(url)
        .then(function(data) {
        	if(data.quirofanoNotasVarias.length>0){
        		self.historicoQuirofano = _.filter(data.quirofanoNotasVarias , function(e){
        			return e.NOTAPREQUIRURGICA.N_CIE10 == $localStorage.NF_CIE_10;
        		});
        	}
        })
        .catch(function(err) {
          console.log(err);
        });
	};

	$scope.setEstatus=function(estatus,registro,nota){
    	var texto="";
     	switch(estatus){
     		case 0:
     			if(nota==4)
     				texto="CIRUGÍA CANCELADA";
     			else
  					texto="CANCELADA";
  				return texto;
  			case 1:
  				return texto;
  			case 2:
  				texto="FINALIZADA";
  				return texto;
  			default:
  				return texto;
     	}
    };
	$scope.clickNota=function(registro,tipoNota,estatus){
    	quirofano.setNotaActual(registro);
    	switch(tipoNota){
     		case 0: //NOTA PREOPERATORIA
        		quirofano.setTipoNota("PreOperatoria");
     			quirofano.setIdNota(registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA);
  				$state.go("notaPreOperatoria");
  				break;
  			case 1: //NOTA PREANESTESICA
  				$state.go("notaPreAnestesica");
  				break;
  			case 2: //NOTA POSTOPERATORIA
	            quirofano.setIdNota(0);
	            quirofano.setTipoNota("PostOperatoria");
		  		$state.go("notaPostOperatoria");
  				break;
  			case 3: //NOTA POST ANESTESICA
  				$state.go("notaPostAnestesica");
  				break;
  			case 4: //CIRUGÍA SEGURA
  				$state.go("cirugiaSegura");
  				break;
     	}
        
    };

    $scope.getEstatusConsentimiento=function(registro,tipoNota){
      	//  0=PREQUIRURGICA   1=PREANÉSTESICA
    	if(tipoNota==0){//PREQUIRURGICA
    		if(registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA!==0)
    		return (registro.NOTAPREQUIRURGICA.NF_CONSENTIMIENTO==0) ? " ":"CON CONSENTIMIENTO";
    	}else{//PREANÉSTESICA
    		if(registro.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA!==0)
    			return (registro.NOTAPREANESTESICA.NF_CONSENTIMIENTO==0) ? " ":"CON CONSENTIMIENTO";
    	}
    };
    function DialogControllerDownload($scope, $mdDialog,image,ruta) {
    	$scope.cancelar = function() {
	           $mdDialog.hide();
	    };
    	$scope.image=image;
    	$scope.ruta=ruta;
 	};


 	 $scope.clickConsentimiento=function(tipo,registro,ev){
        	$scope.tipo=tipo;
        	$scope.registro=registro;
           	// tipo 0=PREQUIRURGICA  1=PREANÉSTESICA
          var url=(tipo==0) ? "download/consentimientoCirugia/"+registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA:"download/consentimientoAnestesia/"+registro.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA;
        	if(tipo==0){ //PREQUIRÚRGICA
        		if($scope.registro.NOTAPREQUIRURGICA.NF_CONSENTIMIENTO!==0){//Tienen consentimiento
         			if($scope.registro.NOTAPREQUIRURGICA.C_ARCHIVO.indexOf("|image") > -1){
			 		  	download($scope.registro,tipo,ev,url);
			 		}else{
			           	peticiones.getMethodPDF(url)
			            .success(function(data){
			                var file = new Blob([data], {type: 'application/pdf'});
			                var fileURL = URL.createObjectURL(file);
			                reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
			            }).error(function(data){
			                console.log(data);
			            });
			 		}
        		}
        	}else{//PREANÉSTESICA
        		if($scope.registro.NOTAPREANESTESICA.NF_CONSENTIMIENTO!==0){//Tienen consentimiento
		 		    if($scope.registro.NOTAPREANESTESICA.C_ARCHIVO.indexOf("|image") > -1){
		 		    	download($scope.registro,tipo,ev,url);
		 		    }else{
			           	peticiones.getMethodPDF(url)
			            .success(function(data){
			                var file = new Blob([data], {type: 'application/pdf'});
			                var fileURL = URL.createObjectURL(file);
			                reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
			            }).error(function(data){
			                console.log(data);
			            });
		 		    }
        		}
        	}
    };
    function download(registro,tipo,ev,url){
 		   	var url=(tipo==0) ? "download/consentimientoCirugia/"+registro.NOTAPREQUIRURGICA.NP_ID_NOTA_PREQUIRURGICA:"download/consentimientoAnestesia/"+registro.NOTAPREANESTESICA.NP_ID_NOTA_PREANESTESICA;
 		   	url += "?access_token=" + $localStorage.userSecurityInformation.token.value;
			 peticiones.getDatos(url)
		        .then(function(data) {
		          	$scope.image=data;
		        	$scope.ruta=baseURL+url;
		        	$mdDialog.show({
                    controller:DialogControllerDownload,
                    templateUrl: '/application/views/quirofano/descargar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                     locals:{
                     	 ev:ev,
                     	 registro: registro,
                     	 tipo:tipo,
                   		 image: $scope.image,
                   		 ruta:$scope.ruta,
                   		 url:url
                	 }
                  })
                  .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                  }, function() {
                    $scope.status = 'You cancelled the dialog.';
                  });
		        })
		        .catch(function(err) {
		          console.log(err);
		          mensajes.alerta("ALERTA","NO EXISTE ARCHIVO","OK");
		        });

    };
    function DialogControllerDownload($scope, $mdDialog,image,ruta,registro,tipo,ev,url){
	        	$scope.registro=registro;
	        	$scope.tipo=tipo;
	        	$scope.cancelar = function() {
			           $mdDialog.hide();
			    };
	        	$scope.image=image;
	        	$scope.ruta=ruta;
	        	
	        	$scope.modificarArchivo=function(){
		        	mensajes.alerta("ALERTA","NO SE PUEDE MODIFICAR ARCHIVO","OK");	
	 		    };

	 		    $scope.verPdf=function(){
	 		    		peticiones.getMethodPDF(url)
			             .success(function(data){
			                  var file = new Blob([data], {type: 'application/pdf'});
			                  var fileURL = URL.createObjectURL(file);
			                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
			              }).error(function(data){console.log(fileURL);
			                  console.log(data);
			              });
	 		    };
    };
   

    $scope.getNotasHosp = function(datos){
    	consultas.getNotasUrgencias(datos.NP_CIE_10 , $localStorage.paciente.expediente , 1).success(function(data){
    		$scope.hosp = (data.length > 0) ? data : [];
    	}).error(function(err){console.log(err);});
    };

    $scope.detallesHospitalizacion = function(notaHosp){
    	$scope.hospCiclo = [];
   		consultas.getCicloUrgencias($localStorage.paciente.expediente , notaHosp.NF_NOTA_MEDICA_INGRESO , 1).success(function(data){
    		$scope.hospCiclo = data;
    		$scope.seccionesHospitalizacion[0].visible = false;
	    	$scope.seccionesHospitalizacion[1].visible = true;
	    	$scope.isInfoHosp = true;
	    	$scope.isTriage = (data[0].C_FILE_TRIAGE != undefined) ? true : false;
    	}).error(function(data){});
    };

    $scope.seleccionarHosp = function(obj){
	    $scope.seccionesHospitalizacion[obj.id].visible = !obj.visible;
    };


    $scope.downloadConsentimiento = function(obj){
   		reportes.getReporte(baseURL + 'downloadpdf/' + obj.CONSENTIMIENTO.C_MODULO_CONSENTIMIENTO + '/' + obj.NF_NOTA_MEDICA , '_blank' , 'width=800, height=640');
    }

    $scope.downloadEstudio = function(obj){
    	reportes.getReporte(baseURL + 'getpdf/' + obj.ESTUDIO_ECONOMICO.C_MODULO_ESTUDIO + '/' + obj.ESTUDIO_ECONOMICO.C_FILE_ESTUDIO_ECONOMICO , '_blank' , 'width=800, height=640');
    };

    $scope.downloadEnfermeria = function(obj){
    	reportes.getReporte(baseURL + 'downloadpdf/' + obj.C_MODULO_CONSENTIMIENTO + '/' +  obj.NF_NOTA_ENFERMERIA, '_blank' , 'width=800, height=640');
    };

    $scope.downloadMedicina = function(obj){
    	reportes.getReporte(baseURL + 'getpdf/' + obj.LEGAL.C_MODULO_MEDICINA + '/' + obj.LEGAL.C_FILE_MEDICINA , '_blank' , 'width=800, height=640');
    };

    $scope.downloadDiagnosticos = function(obj,ciclo){
    	reportes.getReporte(baseURL + 'getpdf/' + ciclo.C_MODULO_ESTUDIO + '/' + obj.C_NOMBRE_FILE , '_blank' , 'width=800, height=640');
    };

    /***********************FUNCIONES PARA ESTUDIOS DE LABORATORIO Y GABINETE*******************************/
/*
    $scope.EstuLaboGabi=[{
*/

	$scope.getEstudiosPaciente =  function(){
		consultas.getEstudiosPaciente($localStorage.paciente.idexpediente)
		.success(function(data){
			//$scope.EstuLaboGabi = (data.success) ? data : [];
			if (data.success){
				$scope.EstuLaboGabi=data.ESTUDIOS;
				$scope.EstuLaboGabi = _.filter($scope.EstuLaboGabi , function(e){
        			if(e.C_RESULTADO_SERVICIO==undefined){
						e.FECHA_ENTREGA=undefined;
					}	
        			return true;
        		});
				
			}

			else
				$scope.EstuLaboGabi=[];
		})
		.error(function(err){
			mensajes.alerta("ERROR","SERVICIO NO DISPONIBLE","ACEPTAR");
		})

	};

	$scope.verDetalle = function(estudio){
		$mdDialog.show({
			controller: DialogControllerVerDetallesEstudio,
       		templateUrl: '/application/views/histclinica/detalleestudios.html',
      		parent: angular.element(document.body),
      		clickOutsideToClose:true,
      		locals:{
                   		 estudio: estudio
                   
                	 }
	    });
	};




	function DialogControllerVerDetallesEstudio($scope, $mdDialog, estudio) {
        	$scope.estudio=estudio;
        	$scope.cancelar = function() {
		           $mdDialog.hide();
		    };
 		 };






}]);
})();
