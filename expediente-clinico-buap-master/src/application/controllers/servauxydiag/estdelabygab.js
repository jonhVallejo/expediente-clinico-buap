(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:HospitalizacionCalifriesgoobstCtrl
 * @description
 * # HospitalizacionCalifriesgoobstCtrl
 * Controller of the expediente
 * Version al 05/02/2015 8:13
 */
angular.module('expediente')
  .controller('ServauxydiagEstdelabygabCtrl', ['$scope', '$http', 'mensajes', 'pacientes', 'catalogos', '$window', 'usuario', 'medicos', '$state', '$localStorage', function ( $scope, $http, mensajes, pacientes, catalogos, $window, usuario, medicos, $state, $localStorage) {

  	var actionURL = 'laboratorio';
    $scope.tieneEstudios = false;
    //$localStorage.isAgenda = true;

    /*$scope.iniciar = function() {
      //console.log($scope.opcion);
      if ($scope.opcion == "A")
        $localStorage.isAgenda = true;
      else
        $localStorage.isAgenda = false;
      $state.go('agendaConsultaPaciente',{state: 'servAuxiliaresCaptura'});
    }*/

  	$scope.obtenBitacoraDeEstudios = function() {
      //$scope.isAgenda = $localStorage.isAgenda;
      $scope.isAgenda = true;
  		$scope.catSexo = [];
	      pacientes.getCatalogo("12").success(function(data){
	      $scope.catSexo=data;
	    });

	    pacientes.getPaciente()
      	.success(function(data){
	        if(!data.data.hasOwnProperty('NP_ID')){
	        	  $scope.paciente=data.data[0];
              console.log("Datos del Paciente");
              console.log($scope.paciente);

	          	var temp = $scope.paciente.NF_SEXO;
	          	$scope.paciente.NF_SEXO = $scope.paciente.NF_SEXO_ID;
	          	$scope.paciente.NF_SEXO_ID = temp;

	          	temp = $scope.paciente.NF_ENTIDAD_FEDERATIVA;
	          	$scope.paciente.NF_ENTIDAD_FEDERATIVA = $scope.paciente.NF_ENTIDAD_FEDERATIVA_ID;
	          	$scope.paciente.CF_ENTIDAD_FEDERATIVA = temp;

	          	temp = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO;
	          	$scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO_ID;
	          	$scope.paciente.CF_ENTIDAD_DE_NACIMIENTO = temp;

	          	temp = $scope.paciente.NF_ESTADO_CIVIL;
	          	$scope.paciente.NF_ESTADO_CIVIL = $scope.paciente.NF_ESTADO_CIVIL_ID;
	          	$scope.paciente.CF_ESTADO_CIVIL = temp;
	          	$scope.paciente.C_NOMBRE = $scope.paciente.C_NOMBRE + " " + $scope.paciente.C_PRIMER_APELLIDO + " " + $scope.paciente.C_SEGUNDO_APELLIDO;

	          	$scope.paciente.FECHAREGISTRO = moment().toDate();
	          	$scope.paciente.EDAD = pacientes.calculaEdad($scope.paciente.F_FECHA_DE_NACIMIENTO);

	          	// Se obtien los estudios para agendar
              $http.get(baseURL + actionURL + "/" + $scope.paciente.NP_EXPEDIENTE.replace('/', '')+"/"+usuario.usuario.NP_EXPEDIENTE)
          		.success(function(result){
          			$scope.tabEstudiosAgenda = result.NOTAS_MEDICAS;
                $scope.tabEstudiosAgenda = filtraTabla($scope.tabEstudiosAgenda);
                $scope.tieneEstudiosAgenda = ($scope.tabEstudiosAgenda.length > 0) ? true : false;

                if ($scope.tieneEstudiosAgenda) {
                  angular.forEach($scope.tabEstudiosAgenda, function(tabla, index) {
                    angular.forEach(tabla.SERVICIOS, function (estudio, index) {
                      estudio.AGENDAR = false;
                    });
                  });
                  console.log("estudios Agendados ");
            			console.log($scope.tabEstudiosAgenda );
                }
          		})
          		.error(function(data) {
          			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
          		});

              // Se obtien los estudios para registrar
              $http.get(baseURL + actionURL + "/seleccionados/"+ $scope.paciente.NP_EXPEDIENTE.replace('/', '')+"/"+usuario.usuario.NP_EXPEDIENTE)
              .success(function(result){
                $scope.tabEstudiosRegistro = result.NOTAS_MEDICAS;
                angular.forEach($scope.tabEstudiosRegistro, function(tabla, index) {
                  angular.forEach(tabla.SERVICIOS, function (estudio, index) {
                    estudio.CONTROL = estudio.N_CONTROL_INTERNO;
                      estudio.INTERPRETACION = "";
                  });
                });
                $scope.tabEstudiosRegistro = filtraTabla($scope.tabEstudiosRegistro);
                $scope.tieneEstudiosRegistro = ($scope.tabEstudiosRegistro.length > 0) ? true : false;
                console.log("estudios para registro");
                console.log($scope.tabEstudiosRegistro);
              })
              .error(function(data) {
                mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
              });

	        }
	        else
	          mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
	     })
    	.error(function(data){
      	mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
    	});
    }

    function filtraTabla(tabla) {
      if (tabla.length > 0) {
        tabla = _.filter(tabla, function (registro) {
          return registro.SERVICIOS.length > 0
        });
        return tabla;
      }
    }

    $scope.registraResultados = function() {
    	var data = [];
      var registrosSel;
      var regValido = true;
    	angular.forEach($scope.tabEstudiosRegistro, function (tabla, index) {
        angular.forEach(tabla.SERVICIOS, function(subtabla, subindex) {
            if (subtabla.INTERPRETACION != undefined){
              subtabla.INTERPRETACION = trim(subtabla.INTERPRETACION);
              if (subtabla.INTERPRETACION.length > 0)
    			      data.push({NP_ID: subtabla.NP_ID, N_CONTROL_INTERNO: parseInt(subtabla.CONTROL), C_RESULTADO_SERVICIO: subtabla.INTERPRETACION})
            }
            /*else
              regValido = false; */
    		});
      });

      if (regValido)
        if (data.length > 0) {
          $http.put(baseURL + actionURL, data)
      		.success(function(result) {
            var registrosSel;
            var i;
            angular.forEach($scope.tabEstudiosRegistro, function (tabla, index) {
              tabla.SERVICIOS = _.filter(tabla.SERVICIOS, function(estudio){ return estudio.INTERPRETACION.length == 0; });
            });

            // Se eleminiar registros sin estudios
            $scope.tabEstudiosRegistro = filtraTabla($scope.tabEstudiosRegistro);
            console.log();
            $scope.tieneEstudiosRegistro = ($scope.tabEstudiosRegistro.length > 0) ? true : false;

      			mensajes.alerta('','INFORMACION ALMACENADA EXITOSAMENTE','ACEPTAR!');
            //$state.go("servAuxiliares");
      		})
      		.error(function(data) {
      			mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      		});
        }
        else
          mensajes.alerta('ERROR','NO HA SELECCIONADO ESTUDIOS','ACEPTAR!');
      else
        mensajes.alerta('ERROR','VERIFIQUE EL(LOS) NUMERO(S) DE CONTROL','ACEPTAR!');
    }

    $scope.agendarEstudios = function() {
      var data = [];
      var regRespaldo;
      var tablaNuevosAgenda = [];
      var regValido = true;
      var registrosSel, registrosNoSel;

      angular.forEach($scope.tabEstudiosAgenda, function (tabla, index) {
        angular.forEach(tabla.SERVICIOS, function(subtabla, subindex) {
          if (subtabla.AGENDAR == true) {
            if (subtabla.CONTROL == undefined)
              regValido = false;
            else
              // Se genera el JSON para el servicio
              data.push({NP_ID: subtabla.NP_ID, SELECCIONADOS: 1, N_CONTROL_INTERNO : parseInt(subtabla.CONTROL)});
          }
        });
      });

      if (regValido) {
        if (data.length > 0) {
          console.log(JSON.stringify( data ));
          $http.put(baseURL + actionURL + "/seleccionados", data)
          .success(function(result) {
            tablaNuevosAgenda = [];

            angular.forEach($scope.tabEstudiosAgenda, function (tabla, index) {
              // Respalda el Registro Actual
              regRespaldo = JSON.parse( JSON.stringify( tabla ) );
              registrosSel = [];
              registrosNoSel = [];
              angular.forEach(tabla.SERVICIOS, function(subtabla, subindex) {
                if (subtabla.AGENDAR == true)
                  registrosSel.push(subtabla.NP_ID);
                else
                  registrosNoSel.push(subtabla.NP_ID);
              });

              // Se eliminan los estudios no selecciondos del respaldo
              console.log("Registros NO selecciondos");
              console.log(registrosNoSel);
              if (registrosNoSel.length >0) {
                var i;
                angular.forEach(registrosNoSel, function(id) {
                  i = _.findLastIndex(regRespaldo.SERVICIOS, {NP_ID: id});
                  regRespaldo.SERVICIOS.splice(i, 1);
                });
              }

              // Se guarda el medico con lo estudios seleccionado si es que tiene aun eliminando los no seleccionados
              console.log("servicios agendados");
              console.log(regRespaldo.SERVICIOS);
              if (regRespaldo.SERVICIOS.length > 0) {
                tablaNuevosAgenda.push(JSON.parse( JSON.stringify( regRespaldo ) ));
                $scope.tieneEstudiosRegistro = true;
              }

              // Se eliminan de la lista los registros selecionados
              console.log("Registros selecionados");
              console.log(registrosSel);
              if (registrosSel.length > 0) {
                var i;
                angular.forEach(registrosSel, function(id) {
                  i = _.findLastIndex(tabla.SERVICIOS, {NP_ID: id});
                  tabla.SERVICIOS.splice(i, 1);
                });
              }
            });


            // Se actualiza la tabla de Estudios para registro con los agendados
            angular.forEach(tablaNuevosAgenda, function(regNuevo, indexNuevo) {
              var indiceBusqueda = -1;

              /*angular.forEach($scope.tabEstudiosRegistro, function(regActual, indexAct) {
                if (regNuevo.NP_ID == regActual.NP_ID)
                  indiceBusqueda = regNuevo.NP_ID;
              });*/

              indiceBusqueda = _.findLastIndex($scope.tabEstudiosRegistro, {NP_ID: regNuevo.NP_ID });

              console.log("indiceBusqueda");
              console.log(indiceBusqueda);

              if (indiceBusqueda == -1) {
                $scope.tabEstudiosRegistro.push(JSON.parse(JSON.stringify(regNuevo)));
              }
              else{
                  angular.forEach(regNuevo.SERVICIOS,  function(estudio) {
                  $scope.tabEstudiosRegistro[indiceBusqueda].SERVICIOS.push(JSON.parse(JSON.stringify(estudio)));
                });
              }

            });

            // Se eleminian los registros sin estudios
            $scope.tabEstudiosAgenda = filtraTabla($scope.tabEstudiosAgenda);
            $scope.tieneEstudiosAgenda = ($scope.tabEstudiosAgenda.length > 0) ? true : false;

            mensajes.alerta('','INFORMACION ALMACENADA EXITOSAMENTE','ACEPTAR!');
            //$state.go("servAuxiliares");
          })
          .error(function(data) {
              mensajes.alerta('ERROR',data.error,'ACEPTAR!');
          });
        }
        else
          mensajes.alerta('ERROR','NO HA SELECCIONADO ESTUDIOS','ACEPTAR!');
      }
      else
        mensajes.alerta('ERROR','VERIFIQUE EL(LOS) NUMERO(S) DE CONTROL','ACEPTAR!');
    }

    $scope.cancelar = function() {
      $state.go('agendaConsultaPaciente',{state: 'servAuxiliares'});
    }

    function trim(cadena){
      var retorno=cadena.replace(/^\s+/g,'');
      retorno=retorno.replace(/\s+$/g,'');
      return retorno;
    }

  }]);


})();
