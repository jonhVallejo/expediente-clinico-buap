/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:HospitalizacionGenerarrequisicionhospitalizacionCtrl
 * @description
 * # HospitalizacionGenerarrequisicionhospitalizacionCtrl
 * Controller of the expediente
 */

(function(){
'use strict';
angular.module('expediente')
    .controller('HospitalizacionGenerarrequisicionhospitalizacionCtrl',['$scope','usuario','$state','peticiones','$q','mensajes','$location','$timeout','reportes','$localStorage',
    function ($scope,usuario,$state,peticiones,$q,mensajes,$location,$timeout,reportes,$localStorage) {

		$scope.catDatosInsumos=[];
		$scope.insumos=[];
		$scope.medicamentos=[];
		$scope.F_ACTUAL=moment().toDate();
		//$scope.showRequisicion=false;
		$scope.cargarCuentaPaciente=undefined;//VARIABLE QUE ME PERMITE SABER SI SE VA GENERAR REQUISICION(true) O SE VA CARGAR A CUENTA PACIENTE(false)
		$scope.selectedMedicamentos = [];
		$scope.selectedInsumos = [];
		$scope.generarReqImprimir=true;
		$scope.showTables=false;
		$scope.soluciones="";
		$scope.medidasGenerales="";
		$scope.dieta="";
		var self = this;

		$scope.query = {
	        filter: '',
	        order: '',
	        limit: 5,
	        page: 1
		};

		$scope.query2 = {
	        filter: '',
	        order: '',
	        limit: 5,
	        page: 1
		};
		$scope.total=0;

	    getDatosInsumos();
	  
	  



	    function validarProcedencia(){
	    	if($location.path()=="/hospitalizacion/cargarCuentaPaciente"){  //CARGAR A CUENTA PACIENTE LA REQUISICIÓN
	    		$scope.cargarCuentaPaciente=true;
	    		$scope.showTables=false;
	    		getDatosMedicamentos(2);
	    	}
	    	if($location.path()=="/hospitalizacion/generarRequisiciones"){//GENERARÉ LA REQUISICIÓN DE UN PACIENTE
	    		$scope.cargarCuentaPaciente=false;
	    		$scope.showTables=true;
	    	    getDatosMedicamentos(1);
	    	}
	    };

	    validarProcedencia();

	    //Catálogo de Insumos
	    function getDatosInsumos(){
	    	var url="medicamentos/insumos";
	    	 peticiones.getDatos(url)
      			  .then(function(data) {
      			  	if(data!=undefined)
		           		$scope.catDatosInsumos=data;
			        }).catch(function(err) {
			          console.log(err);
			        });

	    };

	    function getRecuperarDatos(data,idEstatus){
	    	$scope.insumos=[];
			$scope.medicamentos=[];
	    	if(data.MEDICAMENTOS .length>0){
    			$scope.insumos= _.filter(data.MEDICAMENTOS , function(e){
    				if(idEstatus==1)
    					return e.C_INDICACIONES == "INSUMO" && e.NF_ESTATUS==2;
    			    else
        				return e.C_INDICACIONES == "INSUMO" && e.NF_ESTATUS==idEstatus;
        		});
        		
        		$scope.medicamentos= _.filter(data.MEDICAMENTOS , function(e){
        		   if(idEstatus==1)
        		   	return e.C_INDICACIONES !== "INSUMO" && (e.NF_ESTATUS==0 || e.NF_ESTATUS==2);   
        		   else
        			return e.C_INDICACIONES !== "INSUMO" && e.NF_ESTATUS==idEstatus;
        		});
        		
        		$scope.insumos = $scope.insumos.map(function (aux){
		            return {NP_ID_INSUMO: aux.NP_ID, NP_MEDICAMENTO: aux.C_NOMBRE_MEDICAMENTO, NP_CVEMED: aux.NP_MEDICAMENTO_CLAVE,C_CANTIDAD:aux.C_CANTIDAD};
		         });
        	}
        	$scope.nombrePaciente=data.DATOS_PACIENTE.C_NOMBRE +" "+data.DATOS_PACIENTE.C_PRIMER_APELLIDO+" "+data.DATOS_PACIENTE.C_SEGUNDO_APELLIDO;
        	$scope.nombreMedico=data.DATOS_MEDICO.C_NOMBRE +" "+data.DATOS_MEDICO.C_PRIMER_APELLIDO+" "+data.DATOS_MEDICO.C_SEGUNDO_APELLIDO;
	    };

	    $scope.buscar=function(idFolio){
	    	$scope.showTables=true;
	    	$localStorage.N_ULTIMARECETAMEDICA=idFolio;
	    	getDatosMedicamentos(2);
	    };

	    function getDatosMedicamentos(idEstatus){
	    	var url="receta/"+$localStorage.N_ULTIMARECETAMEDICA;
	    	 peticiones.getDatos(url)
      			.then(function(data) {
      				if(data=='')
		            	mensajes.alertaTimeout('ALERTA','EL FOLIO DE LA RECETA NO  EXISTE','ACEPTAR', 3000);
		            else{
		            	 $scope.datos=data;
		            	 console.debug(data.NF_NOTA_MEDICA);
		            	 getSoluciones(data.NF_NOTA_MEDICA);
	      				 getRecuperarDatos(data,idEstatus);
	      				 getServicio();
			        	
		        	}
		        }).catch(function(err) {
			          console.log(err);
			          mensajes.alertaTimeout('ALERTA','EL FOLIO DE LA RECETA NO  EXISTE','ACEPTAR', 3000);
			    });

	    };
	    function getSoluciones(idNota){
	    	var url="notamedica/urgencias/"+idNota;
	    	 peticiones.getDatos(url)
      			.then(function(data) {
      				console.debug(data[0]);
		            $scope.soluciones=data[0].nota.C_SOLUCIONES;
		            $scope.medidasGenerales=data[0].nota.C_MEDIDAS_GENERALES;
		            $scope.dieta=data[0].nota.C_DIETA;
		        }).catch(function(err) {
			          console.log(err);
			    });
	    }

	    function getServicio(){
	    	$scope.C_AREA_SERVICIO="";
	    	var id=($scope.datos.DATOS_PACIENTE.NP_EXPEDIENTE.indexOf('/'))?$scope.datos.DATOS_PACIENTE.NP_EXPEDIENTE.split("/")[0]:$scope.datos.DATOS_PACIENTE.NP_EXPEDIENTE;
	    	//($scope.datos.DATOS_PACIENTE.NP_EXPEDIENTE.indexOf('/'))?$scope.datos.DATOS_PACIENTE.NP_EXPEDIENTE.split("/")[0]:$scope.datos.DATOS_PACIENTE.NP_EXPEDIENTE;
	    	var url="cuentaPaciente/cama/"+id;
	    	 peticiones.getDatos(url)
      			.then(function(data) {
      			    if(data=='')
		            	$scope.cama=false;
		            else{
		            	$scope.C_AREA_SERVICIO=data.C_AREA_SERVICIO;
      					$scope.cama=true;
		            }
		        }).catch(function(err) {
		        	console.log("ERROR");
			        console.log(err);
			    });
	    };

	    function cambiarEstatus(){
    	 	  _.each($scope.selectedMedicamentos, function(i) {
	          		i.NF_ESTATUS=2;
	           });
    	 	   _.each($scope.selectedInsumos, function(i) {
	          		i.NF_ESTATUS=2;
	           });
	    };
	    /******************************************FUNCIONES PARA TABLA**********************************************************/
	    $scope.onOrderChange = function(page, limit) {
	      var deferred = $q.defer();
	      $timeout(function () {
	        deferred.resolve();
	      }, 2000);

	      return deferred.promise;
	    };
	     $scope.onOrderChange2 = function(page, limit) {
	      var deferred = $q.defer();
	      $timeout(function () {
	        deferred.resolve();
	      }, 2000);

	      return deferred.promise;
	    };
	  
	    /******************************************FUNCIONES DE REQUISICIONES******************************************************/

		$scope.addReg = function(NP_ID_INSUMO,NP_MEDICAMENTO,NP_CVEMED){
			$scope.insumos.push({NP_ID_INSUMO: 0, NP_MEDICAMENTO: NP_MEDICAMENTO, NP_CVEMED: NP_CVEMED, C_CANTIDAD:undefined}); 
	    };

		$scope.deleteReg = function(index,registro) {
			 mensajes.confirmacion('ADVERTENCIA','DESEA ELIMINAR EL REGISTRO','SI'). then(function() {
	          $scope.catDatosInsumos.splice(index,1);
	          	///requisicionPaciente/idInsumoEliminar
	          	var url="requisicionPaciente/"+registro.NP_ID_INSUMO;
	          	 peticiones.deleteDatos(url)
			        .then(function(data) {
			         if(data!= undefined)
			          	mensajes.alerta("ALERTA",data.message.toUpperCase(),"OK");
			        })
			        .catch(function(err) {
			          console.log(err);
			        });
	        }); 
	    };

	    $scope.guardar = function(){
            /*if($scope.datos.MOTIVO_SUPERVISOR==undefined || $scope.datos.MOTIVO_SUPERVISOR==""){
		           mensajes.confirmacion('ALERTA','INGRESE EL MOTIVO POR EL CUAL LA REFERENCIA HA SIDO RECHAZADA','ACEPTAR')
		                .then(function() {
		                  document.getElementById("MOTIVO_SUPERVISOR").focus();
		                }).catch(function() {
		                  
		                });
		          
		           return;
		      }*/
		     
	    		var datos={};
		    	datos.INSUMOS=$scope.insumos;
		    	datos.MEDICAMENTOS=$scope.datos.MEDICAMENTOS;
		    	var url="requisicionPaciente/"+$localStorage.N_ULTIMARECETAMEDICA ;
		    	peticiones.putMethod(url,datos)
			       .success(function(data){
			            if(data!= undefined)
				          	mensajes.alerta("ALERTA",data.message.toUpperCase(),"OK");
				            $scope.generarReqImprimir=true;
			          })
			          .error(function(data){
			            mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
			          });

	    };

	    $scope.guardarCuentaPaciente = function(){
	    	////requisicionPaciente/cuentaPaciente/{idRecetaMedica}
	    	if($scope.cargarCuentaPaciente){
	    		var datos={};
		    	datos.INSUMOS=$scope.selectedInsumos;
		    	datos.MEDICAMENTOS=$scope.selectedMedicamentos;
		    	var url="/requisicionPaciente/cuentaPaciente/"+$localStorage.N_ULTIMARECETAMEDICA ;
		    	peticiones.putMethod(url,datos)
			       .success(function(data){
			            if(data!= undefined)
				          	mensajes.alerta("ALERTA",data.message.toUpperCase(),"OK");
				            $scope.generarReqImprimir=true;
			          })
			          .error(function(data){
			            mensajes.alertaTimeout('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!', 3000);
			          });
	    	}/*else{
	    		cambiarEstatus();
	    		var datos={};
		    	datos.INSUMOS=$scope.insumos;
		    	datos.MEDICAMENTOS=$scope.datos.MEDICAMENTOS;
	    		//esperar petición para guardar
	    	}*/
	    };


	    $scope.imprimir=function(){
	      var datos={};
		  datos.INSUMOS=$scope.insumos;
		  datos.MEDICAMENTOS=$scope.medicamentos;
		  datos.FOLIO=$localStorage.N_ULTIMARECETAMEDICA;
		  datos.NP_EXPEDIENTE=$scope.datos.DATOS_PACIENTE.NP_EXPEDIENTE;
		  if($scope.FECHA_PROCEDIMIENTO!=undefined)
		  	datos.FECHA_PROCEDIMIENTO=(moment($scope.FECHA_PROCEDIMIENTO).format("DD/MM/YYYY HH:mm"));
		  datos.FECHA_INSUMO=($scope.FECHA_INSUMO!=undefined) ?(moment($scope.FECHA_INSUMO).format("DD/MM/YYYY HH:mm")) :" ";
		  datos.C_AREA_SERVICIO=$scope.C_AREA_SERVICIO;

	    	//put
	      var url="requisicionPaciente/reporte2";
	       peticiones.putMethodPDF(url,datos)
	         .success(function(data){
	              var file = new Blob([data], {type: 'application/pdf'});
	              var fileURL = URL.createObjectURL(file);
	              reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
	          }).error(function(data){
	              console.log(data);
	          });
		 };
	     /*************************FUNCIONES PARA BUSCAR REQUISICIONES*************************************************/
		$scope.querySearch = function(texto) {
			console.log($scope.catDatosInsumos);
		    var defered = $q.defer();
		    var promise = defered.promise;
		  	var arraySearch = $scope.getInsumo(texto,$scope.catDatosInsumos, [{descripcion : "NP_MEDICAMENTO", validar: false, campo:"" }]);
		  	defered.resolve(arraySearch);
		  	return promise;
		};

		$scope.getInsumo= function(texto,data,campos){
		    if(data!==undefined){
		      var result = peticiones.filtrar(texto, data, campos);
		      console.log(result);
		      var a = result.map(function (serv){
		        return {
			          display: (serv.NP_MEDICAMENTO + " "+ serv.DOSIS[0].DESCRIPCION).toUpperCase(),
			          NP_ID_INSUMO:(serv.NP_ID_INSUMO ),
			          NP_MEDICAMENTO:(serv.NP_MEDICAMENTO ).toUpperCase(),
			          NP_CVEMED:(serv.NP_CVEMED),
		        };
		      });
		      return a;
		    }
		};

		$scope.selectedItemChange = function(item,tipo) {
		    if(item!==undefined){
		    	/*NP_ID_INSUMO: 5410, NP_MEDICAMENTO: "AGUJA ESPINAL PUNTA QUINCKE", NP_CVEMED: "5410"*/
		    	$scope.addReg(item.NP_ID_INSUMO,item.NP_MEDICAMENTO,item.NP_CVEMED);
		    	$scope.searchText = "";
		    	$scope.generarReqImprimir=false;

		    }
		};

    }]);
})();

