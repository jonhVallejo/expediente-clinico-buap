'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:HospitalizacionCalifriesgoobstCtrl
 * @description
 * # HospitalizacionCalifriesgoobstCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('HospitalizacionCalifRiesgoObstCtrl', function ( $scope, $http, mensajes, pacientes, catalogos, peticiones, $window, usuario, medicos) {

  	var baseURL = 'http://148.228.103.13:8080/';
  	var actionURL = '';
  	$scope.N_CALIFICACION = 0;
  	$scope.N_CLASIFICACION_ALTO_RIESGO = "";
    $scope.N_CLASIFICACION_BAJO_RIESGO = "";

  	$scope.inicializaPagina = function() {
  		$scope.catEstadoCivil = [];
	      pacientes.getCatalogo("9").success(function(data){
	      $scope.catEstadoCivil=data;
	    })

	    $scope.catSexo = [];
	      pacientes.getCatalogo("12").success(function(data){
	      $scope.catSexo=data;
	    });

	    $scope.tabHistorialClinica = [ 	{ opciones :  [	{descripcion: "EDAD (<15 AÑOS Ó >40 AÑOS)", 	estatus: false, calificacion: 1},
	    												{descripcion: "ANALFABETA", 					estatus: false, calificacion: 0.5} ] } ];



	    $scope.tabAntecedentesPersonales = [ 	{ opciones :  [	{descripcion: "TB PULMONAR", 			estatus: false, calificacion: 1},
	    													 	{descripcion: "CIRUGIA PELVICA/UTERINA",estatus: false, calificacion: 4} ] },
	    										{ opciones :  [	{descripcion: "DIABETES MELLITUS", 		estatus: false, calificacion: 2},
	    													 	{descripcion: "ENF. INMUNOLOGICAS", 	estatus: false, calificacion: 4} ] },
	    										{ opciones :  [	{descripcion: "HAS CRÓNICA", 			estatus: false, calificacion: 3}] }	    										
	    									];

	    $scope.tabAntecedentesGinecoObst = [ 	{ opciones :  [{descripcion: "INFERTILIDAD", 					estatus: false, calificacion: 2},
	    													 	{descripcion: "OBITOS (UNO)", 					estatus: false, calificacion: 2} ] },
	    										{ opciones :  [{descripcion: "PRIMIGESTA O MULTIGESTA", 		estatus: false, calificacion: 2},
	    													 	{descripcion: "PÉRDIDA GESTACIONAL RECURRENTE", estatus: false, calificacion: 3} ] },
	    										{ opciones :  [{descripcion: "ABORTO PREVIO", 					estatus: false, calificacion: 2},
	    													 	{descripcion: "RN MUERTE EN 1er SEMANA DE VIDA",estatus: false, calificacion: 2} ] },
	    										{ opciones :  [{descripcion: "CESÁRIA PREVIA", 				estatus: false, calificacion: 3},
	    													 	{descripcion: "RN CON PESO <2500 gr.", 			estatus: false, calificacion: 2} ] },
	    										{ opciones :  [{descripcion: "2 Ó MÁS CESARIAS", 				estatus: false, calificacion: 4},
	    													 	{descripcion: "RN CON PESO >4000 gr.", 			estatus: false, calificacion: 2} ] },
	    										{ opciones :  [{descripcion: "PERIODO INTERGENÉSICO CORTO 3", 	estatus: false, calificacion: 3} ] },			 	 
	    									];

	    $scope.tabEmbarazoActual = [ 	{ opciones :  [	{descripcion: "PRECLAMSIA EN EMBARAZO ANTERIOR",estatus: false, calificacion: 4},
													 	{descripcion: "GRUPO SANGUINEO RH NEGATIVO",	estatus: false, calificacion: 6} ] },
										{ opciones :  [	{descripcion: "PESO >80 kg", 					estatus: false, calificacion: 2},
													 	{descripcion: "HB<10mg/DL ", 					estatus: false, calificacion: 3} ] },
										{ opciones :  [	{descripcion: "PESO <40 kg", 					estatus: false, calificacion: 3},
													 	{descripcion: "VDRL POSITIVO",					estatus: false, calificacion: 3} ] },
										{ opciones :  [	{descripcion: "TALLA <150 cm", 					estatus: false, calificacion: 0.5},
													 	{descripcion: "VIH POSITIVO", 					estatus: false, calificacion: 6} ] },
										{ opciones :  [	{descripcion: "FUM INCIERTA", 					estatus: false, calificacion: 0.5},
													 	{descripcion: "IVU", 							estatus: false, calificacion: 3} ] },
										{ opciones :  [	{descripcion: "SIN INMUNISACIÓN ANTITETÁNICA", 	estatus: false, calificacion: 0.5},
														{descripcion: "EMBARAZO MULTIPLE", 				estatus: false, calificacion: 6} ] },			 	 
									];

	    $scope.tabExamenClinico = [ 	{ opciones :  [	{descripcion: "EXPLORACIÓN DE MAMAS ANORMAL", estatus: false, calificacion: 3},
	    												{descripcion: "EXPLORACIÓN DE CÉRVIX ANORMAL",estatus: false, calificacion: 4},
	    												{descripcion: "ALTURA UTERINA DISCORDANTE",	  estatus: false, calificacion: 4},
	    												{descripcion: "FCC>160X'", 					  estatus: false, calificacion: 6}
														 ] },

										{ opciones :  [	{descripcion: "EXPLORACIÓN DE PELVIS ANORMAL",estatus: false, calificacion: 6},
														{descripcion: "T/A>130/90 mmhg", 			  estatus: false, calificacion: 6},
													 	{descripcion: "FCC<120X'", 					  estatus: false, calificacion: 6}																								
													 	] }										
									];
	    
	    /*console.log("Id del medico");
	    console.log(usuario.usuario.NP_EXPEDIENTE);*/
	    medicos.getMedicoActual(302) //usuario.usuario.NP_EXPEDIENTE)
	    .success(function(data){
	    	$scope.medico = data;
	    	$scope.medico.NOMBRE = $scope.medico.C_PRIMER_APELLIDO + " " + $scope.medico.C_SEGUNDO_APELLIDO + " " + $scope.medico.C_NOMBRE;
	    	/*console.log("Datos del medico..");
	    	console.log($scope.medico);*/
	    })
	    .error(function(data){
        	mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      	});

  		pacientes.getPaciente()
      	.success(function(data){
	        if(!data.data.hasOwnProperty('NP_ID')){        
	          $scope.paciente=data.data[0];
	          var temp = $scope.paciente.NF_SEXO;
	          $scope.paciente.NF_SEXO = $scope.paciente.NF_SEXO_ID;
	          $scope.paciente.NF_SEXO_ID = temp;

	          temp = $scope.paciente.NF_ENTIDAD_FEDERATIVA;
	          $scope.paciente.NF_ENTIDAD_FEDERATIVA = $scope.paciente.NF_ENTIDAD_FEDERATIVA_ID;
	          $scope.paciente.CF_ENTIDAD_FEDERATIVA = temp;

	          temp = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO;
	          $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO = $scope.paciente.NF_ENTIDAD_DE_NACIMIENTO_ID;
	          $scope.paciente.CF_ENTIDAD_DE_NACIMIENTO = temp;

	          temp = $scope.paciente.NF_ESTADO_CIVIL;
	          $scope.paciente.NF_ESTADO_CIVIL = $scope.paciente.NF_ESTADO_CIVIL_ID;
	          $scope.paciente.CF_ESTADO_CIVIL = temp; 

	          $scope.paciente.FECHAREGISTRO = moment().toDate();
	          $scope.calcularEdad();
	        
	        }
	        else{
	          mensajes.alerta('ERROR',data.data.Mensaje.toUpperCase(),'ACEPTAR!');
	        }
	    })
      	.error(function(data){
        	mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');
      	}); 
    }

    $scope.calcularEdad = function(){
	    var values=$scope.paciente.F_FECHA_DE_NACIMIENTO.split("/");
	    var dia = values[0];
	    var mes = values[1];
	    var ano = values[2];
	    var fecha_hoy = new Date();
	    var ahora_ano = fecha_hoy.getYear();
	    var ahora_mes = fecha_hoy.getMonth()+1;
	    var ahora_dia = fecha_hoy.getDate()-1+1;
	    var edad = (ahora_ano + 1900) - ano;

	    if(ahora_mes<mes){
	      edad--;
	    }
	    else if(ahora_mes==mes && ahora_dia<dia){
	      edad--;
	    }
	    $scope.paciente.EDAD=edad;        
	}

	$scope.toggle = function (item) {
  		item.estatus = !item.estatus;
        if (item.estatus)
        	$scope.N_CALIFICACION += parseFloat(item.calificacion);
        else
        	$scope.N_CALIFICACION -= parseFloat(item.calificacion);  

        if ($scope.N_CALIFICACION > 6) {
        	$scope.N_CLASIFICACION_ALTO_RIESGO = "yellow";
        	$scope.N_CLASIFICACION_BAJO_RIESGO = "";
        } else {
        	$scope.N_CLASIFICACION_ALTO_RIESGO = "";
        	$scope.N_CLASIFICACION_BAJO_RIESGO = "yellow";
        }

    }

});
