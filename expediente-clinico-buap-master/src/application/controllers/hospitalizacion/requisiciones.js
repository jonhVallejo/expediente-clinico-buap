(function(){
'use strict';
	angular.module('expediente')
    	.controller('requisicionesCtrl',['$scope','usuario','$state','peticiones','$mdDialog','mensajes','$localStorage','pacientes',
    		function ($scope,usuario,$state,peticiones,$mdDialog,mensajes,$localStorage,pacientes) {


    	function getAreas(){
			var url="catalogo/32"
		    peticiones.getDatosStatus(url)
	        .then(function(result) {
	        	if(result.status == 200){
	        		$scope.areas = result.data;
	        		console.debug(result.data);
	        	}else{
	        		mensajes.alerta("","NO HAY DATOS EN EL CATALOGO","OK");
	        	}
	        })
	        .catch(function(err) {
	          console.error(err);
	        });
	    };

	    getAreas();

	    function getPaciente(NP_AREA){
			var url="listaNotasQuirofano/pacientes/" + NP_AREA;
		    peticiones.getDatosStatus(url)
	        .then(function(result) {
	        	if(result.status == 200){
	        		$scope.paciente = result.data;
	        		console.debug(result.data);
	        	}else{
	        		if(result.status == 404){
		        		console.debug("DATOS ENCONTRADOS :" , result.data);
	        		}	
	        		mensajes.alerta("","NO HAY DATOS EN EL CATALOGO","OK");
	        	}
	        })
	        .catch(function(err) {
	          console.error(err);
	        });
	    };


	    $scope.cambioArea = function(){
	    	getPaciente($scope.mArea);
	    }

	    $scope.primerPagina = function(){
      		$scope.query.page = 1;
    	};

    	$scope.generaRequisicion = function(dat){
    		$localStorage.N_ULTIMARECETAMEDICA = dat;
    		$state.go("generarRequisicion");
    	}


      $scope.query = {
        order: 'C_SEGUNDO_APELLIDO',
        limit: 5,
        page: 1
      };

      $scope.columns = [ {
            name: 'ID',
            orderBy: 'NP_ID',
            width : '15%'
        },{
          name: 'APELLIDO PATERNO',
            orderBy: 'C_PRIMER_APELLIDO',
            width : '20%'
      },{
          descendFirst: true,
          name: 'APELLIDO MATERNO',
            orderBy: 'C_SEGUNDO_APELLIDO',
            width : '20%'
        },

      {
          name: 'NOMBRE',
            orderBy: 'C_NOMBRE',
            width : '30%'
      },
      {
          name: 'VER/EDITAR',
            orderBy: 'C_NOMBRE',
            width : '15%'
      }];

	}]);
})();