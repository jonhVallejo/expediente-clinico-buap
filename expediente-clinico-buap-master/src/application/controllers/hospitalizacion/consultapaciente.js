(function(){
'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:HospitalizacionConsultapacienteCtrl
 * @description
 * # HospitalizacionConsultapacienteCtrl
 * Controller of the expediente
 */
angular.module('expediente')
  .controller('HospitalizacionConsultapacienteCtrl', ['$scope', '$http', 'pacientes', 'mensajes', 'peticiones', '$localStorage', '$state', 'consultas', '$mdDialog', '$q', 'blockUI','reportes',
  	function ($scope, $http, pacientes, mensajes, peticiones, $localStorage, $state, consultas, $mdDialog, $q, blockUI, reportes) {
    $scope.selected = [];
    $scope.tabla = false;

	$scope.campos = [{descripcion : "C_NOMBRE_COMPLETO",validar: false, campo:"" },
					 {descripcion : "CF_TIPO_NOTA", 	validar: false, campo:"" }];

  	$scope.query = {
  		filter: '',
    	order: 'C_NOMBRE_COMPLETO',
    	limit: 5,
    	page: 1
  	};

  	$scope.loadTable = function(){
  		//console.log(baseURL + "hospitalizacion/" +  usuario.NP_EXPEDIENTE);
  		iniciaConcentimientosInformados();
  		var myBlockUI = blockUI.instances.get('myBlockUI');
    	myBlockUI.start();
  		var defered = $q.defer();
    	var promise = defered.promise;

  		$http.get(baseURL + "hospitalizacion/" +  usuario.NP_EXPEDIENTE)
  		.success(function(data) {
  			//console.log(data);
	        $scope.datos=_.filter(data,function(reg){
				if (reg!==null) {
					if (!reg.hasOwnProperty('CF_TIPO_NOTA')){
						reg.CF_TIPO_NOTA = "ESPERA";
						reg.NF_TIPO_NOTA = 0;
						reg.NF_NOTA_MEDICA = 0;
					}
				}
				reg.expediente = reg.NF_PACIENTE.replace("/","")
				return reg!==null;
			});
/*
			consultas.getSolicitudesInterconsulta()  //INTERCONSULTA
            .success(function(data){
            	var interconsultas = _.map(data, function (el, key) { //SE AGREGA EL TIPO DE NOTA S_INTERCONSULTA = SOLICITUD DE INTERCONSULTA
                  el.CF_TIPO_NOTA = 'S_INTERCONSULTA';
                  el.idOrder = 1;
                  return el;
                });

                interconsultas = _.filter(interconsultas,function(e){ //SE QUITAN LAS SOLICITUDES QUE EL MÉDICO SOLICITÓ
                  return e.NF_MEDICO_ATIENDE+'' === usuario.NP_EXPEDIENTE+'';
                });
                
                interconsultas = _.map(interconsultas, function (el, key) {
                  el.PACIENTE = {};
                  el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
                  el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
                  el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
                  return el;
                });


                $scope.datos = _.union($scope.datos,interconsultas);
                */
                /*consultas.getEvolucionAlta()//EVOLUCIÓN Y ALTA
                .success(function(data){
	                  data = _.map(data, function (el, key) {
	                    el.PACIENTE = {};
	                    el.PACIENTE.C_NOMBRE = el.C_NOMBRE;
	                    el.PACIENTE.C_PRIMER_APELLIDO = el.C_PRIMER_APELLIDO;
	                    el.PACIENTE.C_SEGUNDO_APELLIDO = el.C_SEGUNDO_APELLIDO;
	                    el.idOrder = 4;
	                    return el;
	                  });
	                  console.log(data);
	                  console.log($scope.datos);
	                  $scope.datos = _.union($scope.datos,data);
	              	})
	              	.error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!'); myBlockUI.stop();}); */
              	/*console.log("Datos Recuperados..")
				console.log($scope.datos); */
				$scope.registros = $scope.datos;
				$scope.tabla = true;        
              	defered.resolve();		                  
	        /*})
	        .error(function(data){
	        	mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!'); 
	        	myBlockUI.stop();
	        });  */
        })
        .error(function(data) {
           mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!'); 
           myBlockUI.stop();    
        });	

        myBlockUI.stop();
    	return promise;						
  	}	

  	$scope.filtraTabla = function(filtro) {
		$scope.registros = peticiones.filtrar(filtro,$scope.datos,$scope.campos);
	}

	$scope.abrirAgendaMedicaIngreso = function(dato){
	    $localStorage.selectedIndex = 0;
	    if(dato.NF_ESTATUS == 1){
		    $localStorage.paciente = {
		      idexpediente:   	dato.NF_PACIENTE,
		      //expediente:		dato.NF_PACIENTE.replace("/",""),
		      expediente:		dato.expediente,
		      nombreCompleto: 	dato.C_NOMBRE_COMPLETO,
		      idTriage:     	"",
		      tipo : 			'ingreso',
		    };
		} 
		else{
	      $localStorage.paciente = {
	        idexpediente:   dato.NF_PACIENTE,
	        expediente:		dato.expediente,
		    nombreCompleto: dato.C_NOMBRE_COMPLETO,
		    idTriage:     	"",
	        tipo : 'ingreso',
	      };
      $localStorage.notaMedica = undefined;
    }
  

	    $state.go("consultaNotaMedica");
  	};

  	$scope.abrirEvolutiva = function(paciente){
	    if($localStorage.paciente != undefined){
	      if($localStorage.paciente.expediente !== paciente.expediente){
	        $localStorage.selectedIndex = 0;
	        $localStorage.notaMedica = undefined;
	      }
	    }

	    if(paciente.NF_ESTATUS === 1){
	      $localStorage.notaMedica = {NP_NOTA_MEDICA : paciente.NF_NOTA_MEDICA}
	    }

	    $localStorage.paciente 				= paciente;
	    $localStorage.paciente.tipo 		= 'EVOLUTIVA';
	    $localStorage.paciente.idexpediente = $localStorage.paciente.NF_PACIENTE.split("/")[0];
	    $localStorage.paciente.expediente 	= $localStorage.paciente.NF_PACIENTE.replace("/","");
	    //console.log("Datos para Evolutiva..");
	    //console.log($localStorage.paciente);

	    $state.go('notaMedicaIngreso');
	};

	$scope.abrirSolicitarInterconsulta = function(paciente){
	    if($localStorage.paciente != undefined){
	      if($localStorage.paciente.expediente !== paciente.expediente){
	        $localStorage.selectedIndex = 0;
	        $localStorage.notaMedica = undefined;
	      }
	    }

	    $localStorage.paciente 				= paciente;
	    $localStorage.paciente.idexpediente = $localStorage.paciente.NF_PACIENTE.split("/")[0];
	    $localStorage.paciente.expediente 	= $localStorage.paciente.NF_PACIENTE.replace("/","");
	    //console.log("Datos para Interconsulta");
	    //console.log($localStorage.paciente);
	    $state.go('solicitudInterconsulta');
	};

	$scope.abrirInterconsulta = function(paciente){
	    if($localStorage.paciente != undefined){
	      if($localStorage.paciente.expediente !== paciente.expediente){
	        $localStorage.selectedIndex = 0;
	        $localStorage.notaMedica = undefined;
	      }
	    }
	    if(paciente.NF_ESTATUS === 1){
	      $localStorage.notaMedica = {NP_NOTA_MEDICA : paciente.NF_NOTA_MEDICA}
	    }

	    $localStorage.paciente 				= paciente;
	    $localStorage.paciente.tipo 		= 'INTERCONSULTA';
	    $localStorage.paciente.idexpediente = $localStorage.paciente.NF_PACIENTE.split("/")[0];
	    $localStorage.paciente.expediente 	= $localStorage.paciente.NF_PACIENTE.replace("/","");

	    $state.go('notaMedicaIngreso');
	};

	$scope.abrirEvolucionAlta = function(paciente){
	    if($localStorage.paciente != undefined){
	      if($localStorage.paciente.expediente !== paciente.expediente){
	        $localStorage.selectedIndex = 0;
	        $localStorage.notaMedica = undefined;
	      }
	    }
	    if(paciente.NF_ESTATUS === 1){
	      $localStorage.notaMedica = {NP_NOTA_MEDICA : paciente.NF_NOTA_MEDICA}
	    }
	    $localStorage.paciente 				= paciente;
	    $localStorage.paciente.tipo 		= 'EVOLUCION Y ALTA';
	    $localStorage.paciente.idexpediente = $localStorage.paciente.NF_PACIENTE.split("/")[0];
	    $localStorage.paciente.expediente 	= $localStorage.paciente.NF_PACIENTE.replace("/","");

	    console.log("Datos para Evolucion y Alta");
	    console.log($localStorage.paciente);
	    $state.go('notaMedicaIngreso');
	};

	$scope.abrirCancelar = function(paciente,num){
	    $localStorage.NOTA_CANCELAR = paciente;
      $localStorage.Num=num;
      console.log(num);
    	$mdDialog.show({
	      templateUrl: '/application/views/hospitalizacion/cancelarevolucionalta.html',
	      parent: angular.element(document.body),
	      clickOutsideToClose:true,
	    });
  	};
  $scope.sendCancelacion = function(){
    $scope.cancelNota = {NP_NOTA_MEDICA:$localStorage.NOTA_CANCELAR.NF_NOTA_MEDICA,C_MOTIVO_CANCELACION:$scope.nota.C_MOTIVO_CANCELACION};
    consultas.cancelJustificacionUrgencias($scope.cancelNota).success(function(data){
      if (data.success){
        mensajes.alerta("AVISO!","SE HA CANCELADO LA EVOLUCIÓN Y ALTA","ACEPTAR!");
        //$scope.loadIni();
        $state.reload();
      }
      else{
        mensajes.alerta("AVISO!",data.Mensaje.toUpperCase(),"ACEPTAR!");
      }
    }).error(function(data){mensajes.alerta('ERROR','SERVICIO NO DISPONIBLE','ACEPTAR!');});
    $localStorage.NOTA_CANCELAR = undefined;
    
  };


	$scope.abrirNotaEgreso = function(dato) {
	    //console.log(dato);
	    $localStorage.paciente = {
	    	expediente : dato.NF_PACIENTE.replace("/",""),
	       	NF_NOTA_MEDICA : dato.NF_NOTA_MEDICA
	    };
	    $localStorage.regresarA = 'H';
	    //console.log($localStorage.paciente);
	    $state.go("notaEgreso"); 
   }



    $scope.consentimientosInformados = [];
    function iniciaConcentimientosInformados(){
	    $http.get(baseURL + 'notamedica/pacientesConsentimiento/1/'+usuario.NP_EXPEDIENTE).success(function(data){

	      //console.log(data);
	      $scope.consentimientosInformados = data;

	    }).error(function(err){

	    });
	 }



	 $scope.uploadConsentiminetoInformado = function(consentimientoInformado, ev){
    var registro = {};
    var tipo = {};
    $mdDialog.show({
      controller: uploadConcentimientoCtrl,
      templateUrl: '/application/views/quirofano/subirarchivo.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      locals:{
        registro: consentimientoInformado,
      }
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });





  }

  $scope.downloadConsentiminetoInformado = function(consentimientoInformado){


    var url = consentimientoInformado.C_FILE_CONSENTIMIENTO.toUpperCase().indexOf('PDF') > 0 ? 'downloadpdf/' : 'download/';

    switch (consentimientoInformado.CF_HOSPITALIZACION) {
      case 'HOSPITALIZACION':
      url += 'consentimientoHospitalizacion/'
      break;

      case 'URGENCIAS':
      url += 'consentimientoUrgencias/'

      break;

    }

    url += consentimientoInformado.NF_NOTA_MEDICA;

    if(url.indexOf('pdf') > 0){
      reportes.getReporte(baseURL + url, '_blank', 'width=1000, height=800');
    }else{
      peticiones.getDatos(url)
      .then(function(data) {
        $scope.image=data;
        $scope.ruta=baseURL+url;
        $mdDialog.show({
          controller: DialogControllerDownload,
          templateUrl: '/application/views/quirofano/descargar.html',
          parent: angular.element(document.body),
          clickOutsideToClose:true,
          locals:{
            image: $scope.image,
            ruta:$scope.ruta
          }
        })
        .then(function(answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
          $scope.status = 'You cancelled the dialog.';
        });
      })
      .catch(function(err) {
        mensajes.alerta("ALERTA","NO EXISTE ARCHIVO","OK");
      });
    }


  };
  function uploadConcentimientoCtrl($scope, $mdDialog, registro, $timeout, peticiones) {
    var registro=registro;

    $scope.cancelar = function() {
      $mdDialog.hide();
    };
    $scope.guardar = function() {
      $mdDialog.hide();
    };

    $scope.uploadPic = function(file) {

      var urlSave = baseURL + "consentimiento/"
      var url="upload/";

      switch (registro.CF_HOSPITALIZACION) {
        case 'HOSPITALIZACION':
        url += 'consentimientohospitalizacion/'
        urlSave += "hospitalizacion/";
        break;

        case 'URGENCIAS':
        url += 'consentimientourgencias/'
        urlSave += "urgencias/";

        break;

      }

      url += registro.NF_NOTA_MEDICA;
      urlSave += registro.NF_NOTA_MEDICA;

      $http.get(urlSave).success(function(dataD){

        peticiones.uploadArchivo(url, file).then(function (data) {

          iniciaConcentimientosInformados();

          mensajes.alerta("EXITO","SE SUBIO CORRECTAMENTE","OK");



        }).catch(function(err){
          console.error(err);
        });

      }).error(function(err){

      });






    };

  };


  }]);
})();