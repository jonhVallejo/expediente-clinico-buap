'use strict';

/**
 * @ngdoc function
 * @name ECEangular.module('expediente').controller:ConsultapacienteCtrl
 * @description
 * # ConsultapacienteCtrl
 * Controller of the expediente
 */
angular.module('expediente').controller('ConsultapacienteCtrl', function ($scope,$timeout, $q, $log) {


	var self = this;
    self.simulateQuery = false;
    self.isDisabled    = false;
    // list of `state` value/display objects
    self.names        = loadAll();
    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;
    
    // ******************************
    // Internal methods
    // ******************************
    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      var results = query ? self.names.filter( createFilterFor(query) ) : self.names,
          deferred;
      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }
    function searchTextChange(text) {
      $log.info('Text changed to ' + text);
    }
    function selectedItemChange(item) {
      $log.info('Item changed to ' + JSON.stringify(item));
    }
    /**
     * Build `data` list of key/value pairs
     */
    function loadAll() {
      var allNames = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
              Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
              Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
              Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
              North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
              South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
              Wisconsin, Wyoming';
      return allNames.split(/, +/g).map( function (state) {
        return {
          value: state.toLowerCase(),
          display: state
        };
      });
    }
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(state) {
        return (state.value.indexOf(lowercaseQuery) === 0);
      };
    }

    $scope.loadFilters = function() {
	    // Use timeout to simulate a 2000ms request.
	    $scope.filters = [];
	    return $timeout(function() { //Función para cargar las especialidades de un filtro
	      $scope.filters = [
	        { id: 1, name: 'Especialidad 1' },
	        { id: 2, name: 'Especialidad 2' },
	        { id: 3, name: 'Especialidad 3' },
	        { id: 4, name: 'Especialidad 4' },
	        { id: 5, name: 'Especialidad 5' },
	      ];
	    }, 1000);
	  };

	  $scope.loadFilters2 = function() {
	    // Use timeout to simulate a 2000ms request.
	    $scope.filters2 = [];
	    return $timeout(function() { //Función para cargar las especialidades de un filtro
	      $scope.filters2= [
	        { id: 1, name: 'MATUTINO' },
	        { id: 2, name: 'VESPERTINO' },
	        { id: 3, name: 'NOCTURNO' },
	      ];
	    }, 1000);
	  };



	   $scope.selected = [];

  $scope.query = {
    filter: '',
    order: 'name',
    limit: 5,
    page: 1
  };

  function success(desserts) {
    $scope.desserts = desserts;
  }

  // in the future we may see a few built in alternate headers but in the mean time
  // you can implement your own search header and do something like
  $scope.search = function (predicate) {
    $scope.filter = predicate;
    $scope.deferred = $scope.desserts.get($scope.query, success).$promise;
  };

  $scope.onOrderChange = function (order) {
    return $scope.desserts.get($scope.query, success).$promise; 
  };

  $scope.onPaginationChange = function (page, limit) {
    return $scope.desserts.get($scope.query, success).$promise; 
  };

  $scope.desserts =[];

  $scope.desserts = [
  {name: 'cupcake',  calories: '35kCal', fat: '14', carbs: '10', sodium: '23', calcium: '58', iron: '978'},
  {name: 'cake',  calories: '15kCal', fat: '15', carbs: '9', sodium: '25', calcium: '68', iron: '878'},
  {name: 'nuts',  calories: '25kCal', fat: '16', carbs: '8', sodium: '27', calcium: '78', iron: '778'},
  {name: 'strawberry',  calories: '45kCal', fat: '17', carbs: '7', sodium: '24', calcium: '88', iron: '678'},
  {name: 'pay',  calories: '85kCal', fat: '18', carbs: '6', sodium: '26', calcium: '98', iron: '578'},
  {name: 'cupcake',  calories: '35kCal', fat: '14', carbs: '10', sodium: '23', calcium: '58', iron: '978'},
  {name: 'cake',  calories: '15kCal', fat: '15', carbs: '9', sodium: '25', calcium: '68', iron: '878'},
  {name: 'nuts',  calories: '25kCal', fat: '16', carbs: '8', sodium: '27', calcium: '78', iron: '778'},
  {name: 'strawberry',  calories: '45kCal', fat: '17', carbs: '7', sodium: '24', calcium: '88', iron: '678'},
  {name: 'pay',  calories: '85kCal', fat: '18', carbs: '6', sodium: '26', calcium: '98', iron: '578'}
  ];


  });
