(function(){
'use strict';
angular.module('expediente')
    .controller('hojaEnfermeriaCtrl',['$scope','$localStorage','usuario','$state','peticiones','mensajes','pacientes','$mdDialog','$window','reportes',
      function ($scope,  $localStorage,usuario,$state,peticiones,mensajes,pacientes,$mdDialog,$window,reportes) {

      	var self = this;
      	var url ="hojaenfermeria/";
      	self.verBtn = false;

 		$scope.datos ={
                        nombre: $localStorage.paciente.nombrePaciente,
                        H: $localStorage.paciente.idPaciente,
                        id : usuario.usuario.NP_EXPEDIENTE,
                        exp: $localStorage.paciente.expediente,
                        edad: pacientes.calculaEdad($localStorage.paciente.f_nacimiento),
                        fNac: $localStorage.paciente.f_nacimiento,
                        genero: $localStorage.paciente.genero
        };



        var mNF_PACIENTE = $localStorage.paciente.expediente.substring(0, $localStorage.paciente.expediente.length - 4);
        



         var getHojaList = function(url, NF_PACIENTE){
            peticiones.getDatos(url + NF_PACIENTE)
              .then(function(data)
                {
                  self.hojaEnfermeria = data;
                  if(self.hojaEnfermeria.HOJAS_ENFERMERIA){
                      self.verBtn = true;
                      self.hojaEnfermeria.HOJAS_ENFERMERIA = self.hojaEnfermeria.HOJAS_ENFERMERIA.map(function(data){
                      return {C_CIE_10: data.C_CIE_10, T_FECHA_CMD: new Date(data.T_FECHA_CMD), C_FILE: data.C_FILE, NP_ID: data.NP_ID};
                      });
                      self.NF_NOTA =  self.hojaEnfermeria.NP_ID;
                  }
                  
                 
                }).catch(function(data)
                {
                	self.verBtn = false;
                });
      		};

      		getHojaList(url, mNF_PACIENTE);


		self.load = function(registro,tipo,ev){
 		  	$mdDialog.show({
                    controller: DialogController,
                    templateUrl: '/application/views/quirofano/subirarchivo.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                     locals:{
                   		 NF_NOTA: self.NF_NOTA
                	 }
                  })
                  .then(function(data) {
                   if(data.result == true){
                   		getHojaList(url, mNF_PACIENTE);
                   	 	mensajes.alerta("","EL ARCHIVO SE GUARDO DE FORMA EXITOSA","OK");
                   }
                  }, function(data) {
                    console.log(data);
                  });

 		};

 		 self.download = function(NF_NOTA_ENFERMERIA,NOM_ARCHIVO,ev){
 		    // tipo 1=PREQUIRURGICA  0=PREANÉSTESICA
 		    // 
 		    // 
 		    var url= "download/hojaenfermeria/" + NF_NOTA_ENFERMERIA;
 		    var url_pdf = "downloadpdf/hojaenfermeria/" + NF_NOTA_ENFERMERIA;
 		    if(NOM_ARCHIVO.indexOf("|image") > -1){
 		    	$scope.ruta=baseURL+url;
		        	$mdDialog.show({
                    controller: DialogControllerDownload,
                    templateUrl: '/application/views/quirofano/descargar.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                     locals:{
                   		 ruta:$scope.ruta
                	 }
                  })
                  .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                  }, function() {
                    $scope.status = 'You cancelled the dialog.';
                  });
 		    }else{

		           peticiones.getMethodPDF(url_pdf)
		             .success(function(data){
		                  var file = new Blob([data], {type: 'application/pdf'});
		                  var fileURL = URL.createObjectURL(file);
		                  reportes.getReporte(fileURL, '_blank', 'width=1000, height=800');
		              }).error(function(data){
		                  console.log(data);
		              });
		       

 		    }
 		};


 		function DialogControllerDownload($scope, $mdDialog,ruta) {
        	$scope.cancelar = function() {
		           $mdDialog.hide();
		    };
        	$scope.ruta=ruta;
 		 };

      	function DialogController($scope, $mdDialog, NF_NOTA,$timeout) {
      		
      		var url = "upload/hojaenfermeria/" + NF_NOTA;
      		$scope.determinateValue = 0;
      		$scope.error = "LO SENTIMOS, OCURRIO UN ERROR INTERNO EN EL SERVIDOR";
 
      	    $scope.cancelar = function() {
		           $mdDialog.hide({result: false});
		    };
		    $scope.guardar = function(bandera) {
		        $mdDialog.hide({result: bandera});
		    };

		    $scope.actualizardatos = function(image, formulario){
		    	$scope.isImage = image;
		    	$scope.errorMsg = false;
		    	$scope.picFile = null;
		    	// formulario.$setPristine();
       			// formulario.$setUntouched();
          		// formulario.fileConsentimiento.$error.maxSize = false;
		    }

		    $scope.eliminarArchivo = function(formulario){
		    	$scope.picFile = null;
		    	// formulario.fileConsentimiento.$error.maxSize = false;
		    	formulario.$setPristine();
          		formulario.$setUntouched();
		    }

		    $scope.uploadPic = function(file) {
		    	peticiones.uploadArchivo(url, file)
				    .then(function (response) {
				          $timeout(function () {
				            file.result = response.data;
				             $scope.guardar(true);
				          });
				}, function (response) {
				      if (response.status > 0){
				        	$scope.errorMsg = response.status + ': ' + response.data;
					      $scope.errorMsg = true;
				  		}
				}, function (evt) {
				      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total)); //20140 ultima nota de teo
				     
				});
    		};

 		 };

/**
 * [query Datos para el historico "archivos de enfermeria"]
 * @type {Object}
 */
  $scope.query = {order: 'cFecha', limit: 5,page: 1};
  $scope.notaMedica = [
    {
      name: 'FECHA',
        orderBy: 'idEspecialidad',
        width : '20%'
    },
    {
        name: 'DIAGNOSTICO',
        orderBy: 'display',
        width : '60%'
    }, 
     {
      name: 'ARCHIVO',
        orderBy: 'idEspecialidad',
        width : '20%'
  }];

      }]);
})();