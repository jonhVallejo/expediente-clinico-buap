angular.module('textareaChips', ['ngAnimate', 'ngMaterial', 'ngMessages'])
  .controller('testController', function($scope) {
    $scope.time = new Date();
  })
  .directive('mdSimpleTimePickerTimeInput', function() {
    return {
      restrict: 'A',
      require: ['ngModel'],
      link: function($scope, $element, $attrs, $ctrls) {
        var ngModelCtrl = $ctrls[0];
        var min = Number($attrs.mdSimpleTimePickerTimeInput.split(',')[0]);
        var max = Number($attrs.mdSimpleTimePickerTimeInput.split(',')[1]);

        ngModelCtrl.$parsers.push(function(value) {
          value = value < min ? max : value;
          value = value > max ? max : value;
          return Number(value);
        });

        ngModelCtrl.$formatters.unshift(function(value) {
          //console.log(value, ('00' + value).substr(-2, 2));
          return ('00' + value).substr(-2, 2);
        });

        $element.on('blur', function() {
          var value = $element.val() < min ? max : $element.val();
          value = value > max ? max : value;
          $element.val(('00' + value).substr(-2, 2));
        });
      }
    };
  })
  .directive('mdSimpleTimePicker', function($mdUtil, $window, $mdAria, $filter) {

    var inputTemplate =
      '<div style="max-width:360px;" class="md-display-2">' +
        '<div layout="row">' +
          '<div id="md_hourPicker" flex layout="column" layout-align="center">' +
            '<md-button flex type="button" class="clock-button md-primary md-hue-1" ng-click="incrementHour(); cambioHora(this);" ng-disabled="isReadOnly"><md-icon class="ffa ffa-chevron-up" aria-label="up"></md-icon></md-button>' +
            '<md-input-container class="clock-container" flex="auto" style="width:100px;"><input md-simple-time-picker-time-input="1,12" class="clock-input" ng-model="hourPickerModel" type="text" min="1" max="12" style="text-align:center" ng-keydown="acceptsOnlyTimeNumbers($event)" ng-change="updateModel(); cambioHora(this);" maxlength="2" autocomplete="off" aria-label="HORA"/></md-input-container>' +
            '<md-button flex type="button" class="clock-button md-primary md-hue-1" ng-click="decrementHour(); cambioHora(this);" ng-disabled="isReadOnly"><md-icon class="ffa ffa-chevron-down" aria-label="down"></md-icon></md-button>' +
          '</div>' +
          '<div flex layout="row"  style=" max-width: 10px;" layout-align="center center">' +
            '<div>:</div>' +
          '</div>' +
          '<div id="md_minutePicker" flex layout="column" layout-align="center">' +
            '<md-button type="button" class="clock-button md-primary md-hue-1" ng-click="incrementMinute(); cambioMinuto(this);" ng-disabled="isReadOnly"><md-icon class="ffa ffa-chevron-up"  aria-label="up"></md-icon></md-button>' +
            '<md-input-container class="clock-container" flex="auto" style="width:100px;"><input md-simple-time-picker-time-input="0,59" class="clock-input" ng-model="minutePickerModel" type="text" min="0" max="59" style="text-align:center;" ng-keydown="acceptsOnlyTimeNumbers($event)" ng-change="updateModel(); cambioMinuto(this);" maxlength="2" autocomplete="off"  aria-label="MINUTOS"/></md-input-container>' +
            '<md-button type="button" class="clock-button md-primary md-hue-1" ng-click="decrementMinute(); cambioMinuto(this);" ng-disabled="isReadOnly"><md-icon class="ffa ffa-chevron-down" aria-label="down"></md-icon></md-button>' +
          '</div>' +
          '<div layout="row" layout-align="center center">' +
            '<md-button class="md-primary md-hue-1 md-raised md-cornered" style="min-width:25px;" ng-click="toggleAMPM(); cambioHora(this);" >{{AMPM}}</md-button>' +
          '</div>' +
        '</div>' +
      '</div>';

    function postLink($scope, $element, $attrs, $ctrls) {

      /**
       * VARIABLES DEFINED FOR ANGULAR MATERIAL
       */
      var containerCtrl = $ctrls[0];
      var hasNgModel = !$ctrls[1];
      var ngModelCtrl = $ctrls[1] || $mdUtil.fakeNgModel();
      var isReadonly = angular.isDefined($attrs.readonly);
      var date = new Date();

      $scope.isReadOnly = isReadonly;
      /*---------------------------*/

      var hourPicker = $element.find('input[type=text]:first-child');
      var minutePicker = $element.find('input[type=text]:last-child');
      var hourPickerUp = $element.find('div#hourPicker button:first-child');
      var hourPickerDown = $element.find('div#hourPicker button:last-child');
      var minutePickerUp = $element.find('div#minutePicker button:first-child');
      var minutePickerDown = $element.find('div#minutePicker button:last-child');
      if(date.getHours() > 12){
        $scope.hourPickerModel = date.getHours();
        $scope.AMPM = 'PM';
      }else{
        $scope.hourPickerModel = date.getHours()-12;
        $scope.AMPM = 'AM';  
      }
      $scope.minutePickerModel = date.getMinutes();
      
      $scope.id = $element[0].id;

      $scope.acceptsOnlyTimeNumbers = function($event) {
        if ($.inArray($event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
          // Allow: Ctrl+A, Command+A
          ($event.keyCode == 65 && ($event.ctrlKey === true || $event.metaKey === true)) ||
          // Allow: home, end, left, right, down, up
          ($event.keyCode >= 35 && $event.keyCode <= 40)) {
          // let it happen, don't do anything
          return;
        }
        // Ensure that it is a number and stop the keypress
        if (($event.shiftKey || ($event.keyCode < 48 || $event.keyCode > 57)) && ($event.keyCode < 96 || $event.keyCode > 105)) {
          $event.preventDefault();
        }
      };

      $scope.updateModel = function() {
        var date = ngModelCtrl.$modelValue;
        date.setHours(Number($scope.hourPickerModel) + ($scope.AMPM === 'PM' ? 12 : 0));
        date.setMinutes(Number($scope.minutePickerModel));
        ngModelCtrl.$setViewValue(date);
        ngModelCtrl.$render();
      };

      $scope.incrementHour = function() {
        $scope.hourPickerModel = $scope.hourPickerModel < 12 ? $scope.hourPickerModel + 1 : 1;
        $scope.updateModel();
      };
      $scope.decrementHour = function() {
        $scope.hourPickerModel = $scope.hourPickerModel > 1 ? $scope.hourPickerModel - 1 : 12;
        $scope.updateModel();
      };

      $scope.incrementMinute = function() {
        $scope.minutePickerModel = $scope.minutePickerModel < 59 ? $scope.minutePickerModel + 1 : 0;
        $scope.updateModel();
      };
      $scope.decrementMinute = function() {
        $scope.minutePickerModel = $scope.minutePickerModel > 0 ? $scope.minutePickerModel - 1 : 59;
        $scope.updateModel();
      };
      $scope.toggleAMPM = function() {
        $scope.AMPM = $scope.AMPM === 'AM' ? 'PM' : 'AM';
        $scope.updateModel();
      };

      /**
       * BASIC SETUP FOR INPUT ELEMENT IN ANGULAR MATERIAL
       */
      /*---------------------------*/

      /**
       *
       */
      function ngModelPipelineCheckValue(arg) {
        // arg.setHours(Number($scope.hourPickerModel));
        // containerCtrl.setHasValue(!ngModelCtrl.$isEmpty(arg));
        return arg;
      }

      function ngModelSettingInitialValue(arg) {
        //colocar hora por defecto
        var hours = arg.getHours();
        $scope.hourPickerModel = hours > 12 ? hours - 12 : (hours == 0 ? 12 : hours);
        $scope.minutePickerModel = arg.getMinutes();
        $scope.AMPM = hours >= 12 ? 'PM' : 'AM';

        return arg;
      }

      function inputCheckValue() {
        // An input's value counts if its length > 0,
        // or if the input's validity state says it has bad input (eg string in a number input)
        containerCtrl.setHasValue(true || ($element[0].validity || {}).badInput);
      }

      ngModelCtrl.$formatters.push(ngModelSettingInitialValue);

      if (!containerCtrl) {
        return;
      }
      if (containerCtrl.input) {
        throw new Error('<md-input-container> can only have *one* <input>, <textarea>, <md-select> or <md-textarea-chips>  child element!');
      }
      containerCtrl.input = $element;

      if (!containerCtrl.label) {
        $mdAria.expect($element, 'aria-label', $element.attr('placeholder'));
      } else {
        containerCtrl.label.text(containerCtrl.label.text());
      }

      $element.addClass('md-input');
      if (!$element.attr('id')) {
        $element.attr('id', 'input_' + $mdUtil.nextUid());
      }

      if (!hasNgModel) {
        inputCheckValue();
      }

      var isErrorGetter = containerCtrl.isErrorGetter || function() {
        return ngModelCtrl.$invalid && ngModelCtrl.$touched;
      };
      $scope.$watch(isErrorGetter, containerCtrl.setInvalid);

      ngModelCtrl.$parsers.push(ngModelPipelineCheckValue);
      ngModelCtrl.$formatters.push(ngModelPipelineCheckValue);

      hourPicker.on('keypress', inputCheckValue);
      minutePicker.on('keypress', inputCheckValue);

      if (!isReadonly) {
        hourPicker
          .on('focus', function(ev) {
            containerCtrl.setFocused(true);
          })
          .on('blur', function(ev) {
            containerCtrl.setFocused(false);
            inputCheckValue();
          });
        minutePicker
          .on('focus', function(ev) {
            containerCtrl.setFocused(true);
          })
          .on('blur', function(ev) {
            containerCtrl.setFocused(false);
            inputCheckValue();
          });
      } else {
        hourPicker.attr('readonly', 'readonly');
        minutePicker.attr('readonly', 'readonly');
      }

      $scope.$on('$destroy', function() {
        containerCtrl.setFocused(false);
        containerCtrl.setHasValue(false);
        containerCtrl.input = null;
      });

      /*---------------------------*/

    };

    return {
      restrict: 'E',
      replace: true,
      scope: true,
      require: ['^?mdInputContainer', 'ngModel'],
      link: postLink,
      template: inputTemplate
    };
  });